<div align="center">
![Magma Logo](extra/image/magma-logo.png "Magma Logo")
</div>

# Magma for Android

An Android non-custodial wallet for Tezos developed by [camlCase Inc.](https://camlcase.io/). Uses [KotlinTezos](https://gitlab.com/camlcase-dev/kotlin-tezos) as bridge to communicate with the blockchain.

The app lets you manage your funds and delegation status, has support for [FA1.2](https://assets.tqtezos.com/docs/token-contracts/fa12/1-fa12-intro/) token standard and integrates [Dexter](https://gitlab.com/camlcase-dev/dexter) natively.

Magma is [completely open source](https://gitlab.com/camlcase-dev/magmawallet-android/-/blob/develop/LICENSE.md) to allow users verify its contents. If you encounter an issue, please contact [support@camlcase.io](mailto:supprt@camlcase.io).

On April 2021, camlCase closed down. The app will be available in the store for at least another protocol turn to support users with their move to new apps.

## Dependencies

Name | License | Use
--- | --- | ---
[TzKt API](https://api.tzkt.io/) | Query API and indexer for Tezos
[CoinGecko API](https://www.coingecko.com/en) | XTZ to currency exchange rates
[Baking Bad API](https://baking-bad.org/docs) | Baker information
[Better Call Dev API](https://api.better-call.dev/v1/docs/index.html) | Smart contract errors info provider

Check out the full [list of third-party libraries and source code attribution](https://gitlab.com/camlcase-dev/magmawallet-android/-/wikis/Dependencies) the app uses.

## Security

To ensure the user seed phrase is protected, it's stored using [Android's Keystore system](https://developer.android.com/training/articles/keystore) to generate a master key (AES 256) to be used wih an [EncryptedSharedPreferences](https://developer.android.com/reference/kotlin/androidx/security/crypto/EncryptedSharedPreferences?hl=en) file.

Furthermore, the app enforces pin and biometric authentication every time it's launched. Pin information is saved as a salted hash to be compared against.

## Design

Magma uses a dual approach to its architecture. The app is separated in two modules:
- `app`: UI logic. A standard MVVM application.
- `data`: Business logic. A flexible approach to Clean architecture where use cases fetch data from sources.

Entities used in `data` may be passed to `app`'s view models, but they are always mapped to UI oriented entities so screens are detached of that information.

## Getting started

The app runs in two flavors:
- `mainnet`
- `devnet` (Testnet)

You'll need to edit your `local.properties` file and add 

#### Mainnet configuration

- `sentry-magma-api-key`: DSN to send reports to the [Sentry API](https://docs.sentry.io/platforms/android/) for `mainnet` flavor.
- `moonpay-api-key`: To match with any incoming transactions on the [MoonPay dashboard](https://www.moonpay.com/dashboard/).

#### Testnet configuration

- `sentry-magma-dev-api-key`: DSN to send reports to the [Sentry API](https://docs.sentry.io/platforms/android/) for `devnet` flavor.
- `moonpay-api-key-staging`: To match with any the incoming test transactions on the [MoonPay dashboard](https://www.moonpay.com/dashboard/).

Before submitting a PR, remember to import the CodeStyle settings in `extra/settings/` to maintain consistency.
