/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.test

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.res.Resources
import android.provider.Settings
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.platform.app.InstrumentationRegistry.getInstrumentation
import androidx.test.uiautomator.By
import androidx.test.uiautomator.UiDevice
import androidx.test.uiautomator.Until
import io.camlcase.kotlintezos.model.Address
import io.camlcase.smartwallet.core.Migration
import io.camlcase.smartwallet.data.core.Wallet
import io.camlcase.smartwallet.data.model.AppWallet
import io.camlcase.smartwallet.data.model.OnboardingStep
import io.camlcase.smartwallet.data.model.UserTezosAddress
import io.camlcase.smartwallet.data.model.exchange.CurrencyEntity
import io.camlcase.smartwallet.data.model.exchange.CurrencyType
import io.camlcase.smartwallet.data.source.local.AppPreferences.Companion.AUTH_CANCELLED
import io.camlcase.smartwallet.data.source.local.AppPreferences.Companion.AUTH_TIMESTAMP
import io.camlcase.smartwallet.data.source.local.AppPreferences.Companion.BAKERS_DATA_TIMESTAMP
import io.camlcase.smartwallet.data.source.local.AppPreferences.Companion.BIOMETRIC_LOGIN_ENABLED
import io.camlcase.smartwallet.data.source.local.AppPreferences.Companion.META_DATA_TIMESTAMP
import io.camlcase.smartwallet.data.source.local.AppPreferences.Companion.ONBOARDING_STEP
import io.camlcase.smartwallet.data.source.local.SecurePreferences
import io.camlcase.smartwallet.data.source.local.UserPreferences.Companion.CURRENCY_EXCHANGE
import io.camlcase.smartwallet.data.source.local.UserPreferences.Companion.DELEGATE_ADDRESS
import io.camlcase.smartwallet.data.source.local.UserPreferences.Companion.WALLET_PUBLIC_ADDRESS
import io.camlcase.smartwallet.data.source.local.UserPreferences.Companion.WALLET_VERSION_CREATED
import io.camlcase.smartwallet.data.source.user.PinLocalSource
import kotlinx.serialization.json.Json
import java.io.InputStream
import java.lang.ref.WeakReference
import java.util.*

/**
 * Init a standard user session
 *
 * - test-user-mnemonic
 * - test-user-tokens-mnemonic
 * - test-user-empty-mnemonic
 */
fun initUser(
    activity: Activity? = null,
    user: String = "test-user-mnemonic",
    appVersion: Int = Migration.SECURE_WALLET_CREATION_VERSION,
    currency: CurrencyEntity? = CurrencyEntity(
        Wallet.DEFAULT_CURRENCY,
        CurrencyType.FIAT,
        Wallet.DEFAULT_CURRENCY_SYMBOL,
        1.0,
        Date().time
    )
): AppWallet {
    clearAllPrefs()
    setAppPreferences(activity)
    val wallet = getTestWallet(user)!!
    setUserPreferences(
        activity,
        user = UserTezosAddress(wallet.address),
        appVersion = appVersion,
        currency = currency
    )
    setEncryptedPrefences(activity, wallet = wallet)
    return wallet
}

fun clearAllPrefs() {
    clearPrefs("preferences")
    clearPrefs("app_preferences")
}

fun clearPrefs(name: String) {
    // targetContext is device specific so should be initialized with Instrumentation()
    val context = InstrumentationRegistry.getInstrumentation().targetContext
    val prefs = context.getSharedPreferences(name, Context.MODE_PRIVATE)
    prefs.edit().clear().commit()
}

fun setAppPreferences(
    activity: Activity? = null,
    step: OnboardingStep = OnboardingStep.COMPLETE,
    lastAuth: Long = Date().time,
    biometric: Boolean = false,
    authCancelled: Boolean = false,
    bakersTime: Long = Date().time,
    metadataTime: Long = Date().time
) {
    val context = activity ?: InstrumentationRegistry.getInstrumentation().targetContext
    val prefs = context.getSharedPreferences("app_preferences", Context.MODE_PRIVATE)

    prefs.edit()
        .putString(ONBOARDING_STEP, step.name)
        .putLong(AUTH_TIMESTAMP, lastAuth)
        .putBoolean(BIOMETRIC_LOGIN_ENABLED, biometric)
        .putBoolean(AUTH_CANCELLED, authCancelled)
        .putLong(BAKERS_DATA_TIMESTAMP, bakersTime)
        .putLong(META_DATA_TIMESTAMP, metadataTime)
        .commit()
}

fun setUserPreferences(
    activity: Activity? = null,
    user: UserTezosAddress,
    delegate: Address? = null,
    appVersion: Int = Migration.SECURE_WALLET_CREATION_VERSION,
    currency: CurrencyEntity? = CurrencyEntity(
        Wallet.DEFAULT_CURRENCY,
        CurrencyType.FIAT,
        Wallet.DEFAULT_CURRENCY_SYMBOL,
        1.0,
        Date().time
    )
) {
    val context = activity ?: InstrumentationRegistry.getInstrumentation().targetContext
    val prefs = context.getSharedPreferences("preferences", Context.MODE_PRIVATE)
    val currencySerialised = currency?.let {
        Json.encodeToString(CurrencyEntity.serializer(), it)
    }
    prefs.edit()
        .putString(WALLET_PUBLIC_ADDRESS, user.serialize())
        .putInt(WALLET_VERSION_CREATED, appVersion)
        .putString(DELEGATE_ADDRESS, delegate)
        .putString(CURRENCY_EXCHANGE, currencySerialised)
        .commit()
}

fun setEncryptedPrefences(
    activity: Activity? = null,
    wallet: AppWallet
) {
    val context = activity ?: InstrumentationRegistry.getInstrumentation().targetContext
    val securePreferences = SecurePreferences(WeakReference(context))
    securePreferences.store(SecurePreferences.KEY_WALLET, wallet.serialize())
    securePreferences.store(SecurePreferences.KEY_PUBLIC_KEY, wallet.wallet.publicKey.bytes)

    val pinUseCase = PinLocalSource(securePreferences)
    pinUseCase.storePin("1234")
}

fun getTestWallet(propertyName: String = "test-user-mnemonic"): AppWallet? {
    val properties = ContextTest().getLocalProperties()
    val mnemonic = properties.getProperty(propertyName).split(", ")
    return AppWallet(mnemonic, null, null)
}

var resources: Resources = InstrumentationRegistry.getInstrumentation().targetContext.resources

class ContextTest {
    fun getLocalProperties(): Properties {
        val properties = Properties()
        val inputStream: InputStream = this.javaClass.classLoader.getResourceAsStream("local_test.properties")
        properties.load(inputStream)

        return properties
    }
}

/**
 * [See](https://stackoverflow.com/a/58359193/4328579)
 */
fun setAirplaneMode(enable: Boolean) {
    if ((if (enable) 1 else 0) == Settings.System.getInt(
            getInstrumentation().context.contentResolver,
            Settings.Global.AIRPLANE_MODE_ON, 0
        )
    ) {
        return
    }
    val device = UiDevice.getInstance(getInstrumentation())
    device.openQuickSettings()
    // Find the text of your language
    val description = By.desc("Airplane mode")
    // Need to wait for the button, as the opening of quick settings is animated.
    device.wait(Until.hasObject(description), 500)
    device.findObject(description).click()
    getInstrumentation().context.sendBroadcast(Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS))
}

/**
 * Faucet address to send everything to.
 * Should also be registered as baker
 */
const val mainTestnetAddress = "tz1YCYVCQqFCtjQAQH5sTkkCkPkdMBQTfEn5"

/**
 * Testnet address that is used for regular testing
 */
const val secondaryTestnetAddress = "tz1becfeFKJy1ZM9SdH2Hq8NM6PXgFm5TFTt"
