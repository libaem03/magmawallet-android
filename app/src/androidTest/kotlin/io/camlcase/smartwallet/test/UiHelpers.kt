/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.test

import android.Manifest
import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.platform.app.InstrumentationRegistry
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.data.model.AppWallet
import io.camlcase.smartwallet.ui.main.MainActivity

/**
 * Waits for balance
 */
fun launchMainActivity(
    userProperty: String = "test-user-mnemonic",
    getWallet: (AppWallet) -> Unit = {}
): ActivityScenario<MainActivity> {
    clearAllPrefs()
    val user = initUser(null, userProperty)
    getWallet(user)

    val activityScenario: ActivityScenario<MainActivity> = ActivityScenario.launch(MainActivity::class.java)

    onView(withId(R.id.bottom_navigation))
        .check(matches(isDisplayed()))

    Thread.sleep(5 * 1000) // Wait for balances to be retrieved

    return activityScenario
}

fun grantContactsPermission() {
    InstrumentationRegistry.getInstrumentation().uiAutomation.grantRuntimePermission(
        InstrumentationRegistry.getInstrumentation().targetContext.packageName,
        Manifest.permission.WRITE_CONTACTS
    )
    InstrumentationRegistry.getInstrumentation().uiAutomation.grantRuntimePermission(
        InstrumentationRegistry.getInstrumentation().targetContext.packageName,
        Manifest.permission.READ_CONTACTS
    )
    InstrumentationRegistry.getInstrumentation().uiAutomation.grantRuntimePermission(
        InstrumentationRegistry.getInstrumentation().targetContext.packageName,
        Manifest.permission.CAMERA
    )
}

