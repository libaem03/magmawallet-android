/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.test

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.UiController
import androidx.test.espresso.ViewAction
import androidx.test.espresso.matcher.BoundedMatcher
import androidx.test.espresso.matcher.ViewMatchers.*
import io.camlcase.smartwallet.data.core.Logger
import io.camlcase.smartwallet.ui.delegate.DelegateListAdapter
import io.camlcase.smartwallet.ui.main.activity.OperationListAdapter
import io.camlcase.smartwallet.ui.main.settings.SettingsAdapter
import org.hamcrest.CoreMatchers.allOf
import org.hamcrest.Description
import org.hamcrest.Matcher

class RecyclerViewChildActions {
    companion object {

        /**
         * Performs an action on a view with a given id inside a RecyclerView's item
         */
        fun actionOnChild(action: ViewAction, childId: Int): ViewAction {
            return object : ViewAction {
                override fun getDescription(): String {
                    return "Perform action on the view whose id is passed in"
                }

                override fun getConstraints(): Matcher<View> {
                    return allOf(isDisplayed(), isAssignableFrom(View::class.java))
                }

                override fun perform(uiController: UiController?, view: View?) {
                    view?.let {
                        val child: View = it.findViewById(childId)
                        action.perform(uiController, child)
                    }
                }
            }
        }

        /**
         * checks that the matcher childMatcher matches a view having a given id
         * inside a RecyclerView's item (given its position)
         */
        fun childOfViewAtPositionWithMatcher(childId: Int, position: Int, childMatcher: Matcher<View>): Matcher<View> {
            return object : BoundedMatcher<View, RecyclerView>(RecyclerView::class.java) {
                override fun describeTo(description: Description?) {
                    description?.appendText(
                        "Checks that the matcher childMatcher matches" +
                                " with a view having a given id inside a RecyclerView's item (given its position)"
                    )
                }

                override fun matchesSafely(recyclerView: RecyclerView?): Boolean {
                    val viewHolder = recyclerView?.findViewHolderForAdapterPosition(position)
                    val matcher = hasDescendant(allOf(withId(childId), childMatcher))
                    log(viewHolder)
                    return viewHolder != null && matcher.matches(viewHolder.itemView)
                }

                private fun log(viewHolder: RecyclerView.ViewHolder?) {
                    val logText = when (viewHolder) {
                        is SettingsAdapter.ViewHolder -> {
                            "SETTINGS title: ${viewHolder.title.text} | detail: ${viewHolder.detail.text}"
                        }
                        is OperationListAdapter.ViewHolder -> {
                            "OPERATION type: ${viewHolder.type.text} | fees: ${viewHolder.fees.text}"
                        }
                        is DelegateListAdapter.ViewHolder ->{
                            "DELEGATE name: ${viewHolder.name.text} | detail: ${viewHolder.detail.text}"
                        }
                        else -> {
                            viewHolder.toString()
                        }
                    }
                    Logger.i("matchesSafely $logText")
                }
            }
        }
    }
}
