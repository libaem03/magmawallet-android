/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.onboarding

import androidx.annotation.StringRes
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.test.containsText
import io.camlcase.smartwallet.test.getTextViewValue
import org.hamcrest.CoreMatchers.allOf
import org.hamcrest.CoreMatchers.not
import org.hamcrest.Matchers

class SeedPhraseUITest {
    companion object {
        fun checkSeedPhrase(
            @StringRes titleId: Int = R.string.onb_recovery_phrase_info_title,
            timerVisible: Boolean = false,
            withButton: Boolean = true,
            derivationPath: Boolean = false
        ) {
            checkCollapsingToolbarTitle(titleId)

            if (timerVisible) {
                onView(withId(R.id.timer_tag))
                    .check(matches(isDisplayed()))
            } else {
                onView(withId(R.id.timer_tag))
                    .check(matches(not(isDisplayed())))
            }

            if (withButton) {
                onView(
                    allOf(
                        withId(R.id.action_next),
                        isDescendantOfA(withId(R.id.container_seed_phrase))
                    )
                )
                    .check(matches(isDisplayed()))
                    .check(matches(isEnabled()))
            } else {
                onView(
                    allOf(
                        withId(R.id.action_next),
                        isDescendantOfA(withId(R.id.container_seed_phrase))
                    )
                )
                    .check(matches(not(isDisplayed())))
            }

            onView(withId(R.id.mnemonics_container))
                .check(matches(isDisplayed()))

            onView(withId(R.id.mnemonic_right_row))
                .check(matches(isDisplayed()))

            onView(withId(R.id.mnemonic_left_row))
                .check(matches(isDisplayed()))

            if (derivationPath) {
                onView(withId(R.id.derivation_path))
                    .check(matches(isDisplayed()))
            } else {
                onView(withId(R.id.derivation_path))
                    .check(matches(not(isDisplayed())))
            }
        }

        fun getMnemonic(): List<String> {
            val list = ArrayList<String>()
            list.add(getLeftWord(1).trimMnemonic(1))
            list.add(getLeftWord(2).trimMnemonic(2))
            list.add(getLeftWord(3).trimMnemonic(3))
            list.add(getLeftWord(4).trimMnemonic(4))
            list.add(getLeftWord(5).trimMnemonic(5))
            list.add(getLeftWord(6).trimMnemonic(6))
            list.add(getRightWord(7).trimMnemonic(7))
            list.add(getRightWord(8).trimMnemonic(8))
            list.add(getRightWord(9).trimMnemonic(9))
            list.add(getRightWord(10).trimMnemonic(10))
            list.add(getRightWord(11).trimMnemonic(11))
            list.add(getRightWord(12).trimMnemonic(12))
            return list
        }

        internal fun String.trimMnemonic(number: Int): String {
            return this.split("$number")[1].trim()
        }

        fun getLeftWord(number: Int): String {
            val tvMnemonic = onView(
                Matchers.allOf(
                    containsText("$number  "),
                    withParent(
                        Matchers.allOf(
                            withId(R.id.mnemonic_left_row),
                            withParent(withId(R.id.mnemonics_container))
                        )
                    ),
                    isDisplayed()
                )
            )
            return getTextViewValue(tvMnemonic)
        }

        fun getRightWord(number: Int): String {
            val tvMnemonic = onView(
                Matchers.allOf(
                    containsText("$number  "),
                    withParent(
                        Matchers.allOf(
                            withId(R.id.mnemonic_right_row),
                            withParent(withId(R.id.mnemonics_container))
                        )
                    ),
                    isDisplayed()
                )
            )
            return getTextViewValue(tvMnemonic)
        }

        fun getFirstWord(): String {
            return getLeftWord(1)
        }

        fun getLastWord(): String {
            return getRightWord(12)
        }
    }
}

fun checkCollapsingToolbarTitle(@StringRes titleId: Int) {
    onView(
        allOf(
            withId(R.id.expanded_title),
            withParent(
                Matchers.allOf(
                    withId(R.id.toolbar_layout),
                    withParent(withId(R.id.app_bar_container))
                )
            )
        )
    ).check(matches(withText(titleId)))
}
