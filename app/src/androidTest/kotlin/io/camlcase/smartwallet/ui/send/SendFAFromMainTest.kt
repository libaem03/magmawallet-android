/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.send

import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.ViewInteraction
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.uiautomator.UiDevice
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.WaitForOperationTest
import io.camlcase.smartwallet.core.extension.ellipsize
import io.camlcase.smartwallet.data.core.Tokens.Testnet.TZBTC
import io.camlcase.smartwallet.data.core.extension.formatAsToken
import io.camlcase.smartwallet.data.model.AppWallet
import io.camlcase.smartwallet.data.model.Token
import io.camlcase.smartwallet.test.*
import io.camlcase.smartwallet.ui.ActivityTabTest
import io.camlcase.smartwallet.ui.FAAmountInputTest
import io.camlcase.smartwallet.ui.TezosAddressInputTest
import org.hamcrest.CoreMatchers.allOf
import org.hamcrest.CoreMatchers.not
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.math.BigDecimal
import java.util.*

/**
 * Preconditions:
 * - User has XTZ & tzBTC
 * - Network available
 */
@LargeTest
@RunWith(AndroidJUnit4::class)
class SendFAFromMainTest : TezosAddressInputTest, FAAmountInputTest, WaitForOperationTest, ActivityTabTest {
    lateinit var wallet: AppWallet
    private lateinit var device: UiDevice

    @Before
    fun setUp() {
        device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation())
    }

    @Test
    fun testSendFAToken() {
        val activityScenario = launchMainActivity("test-user-tokens-mnemonic") {
            wallet = it
        }

        onView(
            allOf(
                withId(R.id.list),
                isDisplayed()
            )
        )
            .check(matches(withViewAtPosition(1, hasDescendant(allOf(withId(R.id.token_balance), isDisplayed())))))

        onView(withId(R.id.action_send))
            .check(matches(isDisplayed()))
            .check(matches(isEnabled()))
            .perform(click())

        // DESTINATION
        buttonAtStartCheck()

        val destination = mainTestnetAddress
        testInputTezosAddress(
            wallet.address,
            destination = destination
        )

        onView(withId(R.id.action_next))
            .perform(click())

        // CHOOSE TOKEN
        onView(
            allOf(
                withId(R.id.list),
                isDisplayed()
            )
        )
            .check(matches(isDisplayed()))
            .perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(1, click()))

        // AMOUNT
        onView(withId(R.id.amount_field))
            .check(matches(isDisplayed()))
            .check(matches(withText("")))

        onView(withId(R.id.action_enter_max))
            .check(matches(isDisplayed()))
            .check(matches(isEnabled()))

        buttonAtStartCheck()

        val sendAmount = getRandomTokenAmount()
        val token = TZBTC
        testInputFA(
            sendAmount,
            token
        )

        buttonInteraction()
            .perform(click())

        Thread.sleep(4 * 1000) // Wait for network call
        // CONFIRM
        val (feesText, formattedSendAmount) = checkConfirmationForToken(sendAmount, token, destination)
        // FEES DETAILS
        onView(withId(R.id.fees_section))
            .check(matches(isDisplayed()))
            .check(matches(isEnabled()))
            .perform(click())
        SendXTZFromMainTest.checkFeeDetailSimple(device, feesText)
        device.pressBack()

        onView(
            allOf(
                withId(R.id.action_next),
                withEffectiveVisibility(Visibility.VISIBLE),
                withText(R.string.action_confirm)
            )
        )
            .check(matches(isDisplayed()))
            .check(matches(isEnabled()))
            .perform(click())

        Thread.sleep(3 * 1000) // Wait for network call

        // WAIT FOR INJECTION

        waitForSendOperation(formattedSendAmount)

        // MAIN SCREEN - WALLET

        onView(withId(R.id.bottom_navigation))
            .check(matches(isDisplayed()))

        onView(
            allOf(
                withId(R.id.nav_activity), withContentDescription(R.string.wlt_navigation_activity)
            )
        ).check(matches(isDisplayed()))
            .perform(click())

        Thread.sleep(10 * 1000)

        // ACTIVITY TAB
        testActivityRow(
            resources.getString(R.string.act_type_send),
            "- $formattedSendAmount",
            feesText
        )
        testSendDestination(destination.ellipsize())

        activityScenario.close()
    }

    companion object {
        /**
         * For [Testnet.TZBTC], with 8 decimals
         */
        fun getRandomTokenAmount(): BigDecimal {
            val number = Random().nextInt(8)
            return BigDecimal("0.0000000${number + 1}")
        }

        fun checkConfirmationForToken(
            sendAmount: BigDecimal,
            token: Token,
            destination: String
        ): Pair<String, String> {
            onView(withId(R.id.send_section))
                .check(matches(isDisplayed()))

            val formattedSendAmount = sendAmount.formatAsToken(token)
            onView(allOf(withId(R.id.section_main_value), isDescendantOfA(withId(R.id.send_section))))
                .check(matches(isDisplayed()))
                .check(matches(withText(formattedSendAmount)))

            onView(allOf(withId(R.id.section_main_value), isDescendantOfA(withId(R.id.fees_section))))
                .check(matches(isDisplayed()))
                .check(matches(not(withText(""))))

            val feesView: ViewInteraction =
                onView(allOf(withId(R.id.section_main_value), isDescendantOfA(withId(R.id.fees_section))))
            val feesText = getTextViewValue(feesView)
            println(feesText)

            val total = "$formattedSendAmount\n+ $feesText"
            onView(allOf(withId(R.id.section_main_value), isDescendantOfA(withId(R.id.total_section))))
                .check(matches(isDisplayed()))
                .check(matches(withText(total)))

            onView(withId(R.id.recipient))
                .check(matches(isDisplayed()))
                .check(matches(withText(destination)))

            return Pair(feesText, formattedSendAmount)
        }
    }
}
