/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.exchange

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.uiautomator.UiDevice
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.WaitForOperationTest
import io.camlcase.smartwallet.data.core.Tokens.Testnet.TZBTC
import io.camlcase.smartwallet.data.core.XTZ
import io.camlcase.smartwallet.data.core.extension.format
import io.camlcase.smartwallet.data.model.AppWallet
import io.camlcase.smartwallet.data.model.amount.TokenBalance
import io.camlcase.smartwallet.test.*
import io.camlcase.smartwallet.ui.ActivityTabTest
import org.hamcrest.CoreMatchers.allOf
import org.junit.Before
import org.junit.Test
import java.math.BigDecimal

class XTZToTokenUITest : WaitForOperationTest, ActivityTabTest {
    private lateinit var wallet: AppWallet
    private lateinit var device: UiDevice

    @Before
    fun setUp() {
        device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation())
    }

    @Test
    fun test_Swap_XtzToToken() {
        val activityScenario = launchMainActivity("test-user-tokens-mnemonic") {
            wallet = it
        }
        goAndCheckToExchange()

        // Change slippage to force warning screen
        val slippage = 0.02
        ValidatePriceImpactUITest.changeSlippage(device, slippage)

        val fromAmount = ValidatePriceImpactUITest.getRandomTezAmount()
        val (toAmount, conversionRate) = extractValuesFromExchange(fromAmount.signedDecimalRepresentation)

        buttonInteraction()
            .check(matches(isEnabled()))
            .perform(click())

//        val priceImpact = ValidatePriceImpactUITest.checkPriceImpactWarningScreen()

        Thread.sleep(7 * 1000) // Wait for network call

        val fees = ExchangeConfirmationUITest.checkScreenXTZToToken(
            device,
            fromAmount,
            toAmount,
            TZBTC,
            null,
            conversionRate
        )

        buttonInteraction()
            .check(matches(isDisplayed()))
            .check(matches(isEnabled()))
            .perform(click())

        Thread.sleep(7 * 1000) // Wait for network call
        // WAIT FOR INJECTION

        waitForExchangeOperation(XTZ.symbol, TZBTC.symbol)

        // MAIN SCREEN - EXCHANGE

        onView(withId(R.id.bottom_navigation))
            .check(matches(isDisplayed()))
        onView(
            allOf(
                withId(R.id.nav_activity), withContentDescription(R.string.wlt_navigation_activity)
            )
        ).check(matches(isDisplayed()))
            .perform(click())

        Thread.sleep(8 * 1000)

        // ACTIVITY TAB
        testActivityRow(
            resources.getString(R.string.act_type_exchange),
            "- ${fromAmount.format(XTZ.symbol)}\n",
            " ${TokenBalance(toAmount, TZBTC.decimals).formattedValue} ${TZBTC.symbol}",
            fees.format(XTZ.symbol)
        )

        activityScenario.close()
    }

    companion object {
        fun goAndCheckToExchange() {
            // Has tokens
            onView(
                allOf(
                    withId(R.id.list),
                    isDisplayed()
                )
            )
                .check(
                    matches(
                        withViewAtPosition(
                            1,
                            hasDescendant(
                                allOf(
                                    withId(R.id.token_balance),
                                    isDisplayed()
                                )
                            )
                        )
                    )
                )

            ValidateExchangeTabUITest.navigateToExchange()
            ExchangeScreenUITest.firstCheck()
        }

        fun extractValuesFromExchange(fromAmount: BigDecimal): Pair<Double, String> {
            ExchangeScreenUITest.fromAmount
                .perform(ViewActions.replaceText(fromAmount.toPlainString()))
            Thread.sleep(1000)
            val toAmount = getTextViewValue(ExchangeScreenUITest.toAmount).toDouble()
            val conversionRate = getTextViewValue(onView(withId(R.id.exchange_rate)))
            return Pair(toAmount, conversionRate)
        }
    }
}
