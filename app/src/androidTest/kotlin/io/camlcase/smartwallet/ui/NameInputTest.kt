/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui

import androidx.annotation.IdRes
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.ViewInteraction
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.test.buttonAtStartCheck
import io.camlcase.smartwallet.test.buttonInteraction
import org.hamcrest.Matchers.not

interface NameInputTest {
    fun testNameField(
        name: String,
        editTextInteraction: ViewInteraction,
        clearTextInteraction: ViewInteraction,
        @IdRes buttonId: Int = R.id.action_next
    ) {
        buttonAtStartCheck(buttonId)

        editTextInteraction
            .perform(click())
            .perform(replaceText("12345678"))

        buttonInteraction(buttonId)
            .check(matches(isDisplayed()))
            .check(matches(isEnabled()))

        editTextInteraction
            .perform(click())
            .perform(replaceText(""))

        buttonAtStartCheck()

        editTextInteraction
            .perform(click())
            .perform(replaceText("J"))

        clearNameText(editTextInteraction, clearTextInteraction, buttonId)

        // Name should force upper case for words
       buttonAtStartCheck(buttonId)

        editTextInteraction
            .perform(click())
            .perform(typeText(name))
    }

    fun clearNameText(
        editTextInteraction: ViewInteraction,
        clearTextInteraction: ViewInteraction,
        @IdRes buttonId: Int = R.id.action_next
    ) {
        clearTextInteraction
            .check(matches(isDisplayed()))
            .perform(click())

        editTextInteraction
            .check(matches(withText("")))

        onView(withId(buttonId))
            .check(matches(not(isEnabled())))
    }
}
