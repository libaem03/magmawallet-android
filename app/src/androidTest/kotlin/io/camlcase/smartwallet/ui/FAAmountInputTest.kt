/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui

import androidx.annotation.IdRes
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.ViewInteraction
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.data.model.Token
import io.camlcase.smartwallet.test.buttonInteraction
import io.camlcase.smartwallet.test.getEditTextValue
import org.hamcrest.Matchers.not
import java.math.BigDecimal

/**
 * Test an [AmountEditText] with XTZ
 */
interface FAAmountInputTest {
    fun testInputFA(
        sendAmount: BigDecimal,
        token: Token,
        @IdRes editTextId: Int = R.id.amount_field,
        @IdRes errorViewId: Int = R.id.amount_error,
        @IdRes buttonId: Int = R.id.action_next,
        @IdRes maxBtnId: Int = R.id.action_enter_max
    ) {
        onView(withId(errorViewId))
            .check(matches(not(isDisplayed())))

        // Input not numbers
        onView(withId(editTextId))
            .check(matches(isDisplayed()))
            .perform(replaceText("tz1XSXBe"))

        buttonInteraction(buttonId)
            .check(matches(not(isEnabled())))

        // Input 0.0
        onView(withId(editTextId))
            .perform(replaceText("0"))

        buttonInteraction(buttonId)
            .check(matches(not(isEnabled())))

        // Input max+1 decimals
        var typed = "0"
        if (token.decimals > 0) {
            typed += "."
            for (i in 1..token.decimals) {
                typed += "0"
            }
        }
        val newTyped = typed + "1"

        onView(withId(editTextId))
            .perform(click())
            .perform(typeText(newTyped))

        onView(withId(editTextId))
            .check(matches(withText(typed)))

        // Set max+1
        onView(withId(maxBtnId))
            .check(matches(isDisplayed()))
            .perform(click())

        val numberResult: ViewInteraction = onView(withId(editTextId))
        var text = getEditTextValue(numberResult)
        val maxValue = text.toDouble()

        onView(withId(editTextId))
            .check(matches(isDisplayed()))
            .perform(replaceText((maxValue + 1.0).toString()))

        validateError()

        // Input amount
        onView(withId(editTextId))
            .perform(replaceText(sendAmount.toPlainString()))

        onView(withId(errorViewId))
            .check(matches(not(isDisplayed())))

        buttonInteraction(buttonId)
            .check(matches(isEnabled()))
    }

    fun validateError(
        @IdRes errorViewId: Int = R.id.amount_error,
        @IdRes buttonId: Int = R.id.action_next
    ) {
        onView(withId(errorViewId))
            .check(matches(isDisplayed()))

        buttonInteraction(buttonId)
            .check(matches(not(isEnabled())))
    }
}
