/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.test.withDrawable
import io.camlcase.smartwallet.ui.base.info.InfoType
import org.hamcrest.CoreMatchers.not

interface InfoViewUITest {
    fun checkInfoView(
        type: InfoType
    ) {
        checkInfoView(
            type.imageId!!,
            type.title,
            type.detail,
            type.actionText,
            type.secondaryActionText
        )
    }

    fun checkInfoView(
        @DrawableRes imageId: Int,
        @StringRes title: Int?,
        @StringRes detail: Int?,
        @StringRes actionText: Int? = R.string.action_continue,
        @StringRes secondaryActionText: Int? = null,
    ) {

        onView(withId(R.id.image))
            .check(matches(isDisplayed()))
            .check(matches(withDrawable(imageId)))

        if (title != null) {
            onView(withId(R.id.title))
                .check(matches(isDisplayed()))
                .check(matches(withText(title)))
        } else {
//            onView(withId(R.id.title))
//                .check(matches(isDisplayed()))
        }

        if (detail != null) {
            onView(withId(R.id.detail))
                .check(matches(isDisplayed()))
                .check(matches(withText(detail)))
        }

        if (actionText != null) {
            onView(withId(R.id.action_primary))
                .check(matches(isDisplayed()))
                .check(matches(withText(actionText)))
        } else {
            onView(withId(R.id.action_primary))
                .check(matches(not(isDisplayed())))
        }

        if (secondaryActionText != null) {
            onView(withId(R.id.action_secondary))
                .check(matches(isDisplayed()))
                .check(matches(withText(secondaryActionText)))
        } else {
            onView(withId(R.id.action_secondary))
                .check(matches(not(isDisplayed())))
        }
    }
}
