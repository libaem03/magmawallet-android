/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.contact

import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.uiautomator.UiDevice
import androidx.test.uiautomator.UiSelector
import io.camlcase.kotlintezos.model.Address
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.test.*
import org.hamcrest.CoreMatchers.allOf
import org.hamcrest.CoreMatchers.not
import org.hamcrest.Matchers
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Preconditions:
 * - Device has a Google account set
 * - Google account has no testnet// contacts
 *
 * Steps:
 * - Check empty contact list
 * - Add a contact
 * - Check new list
 * - Pick a contact
 * - Check new list
 * - Delete both
 */
@LargeTest
@RunWith(AndroidJUnit4::class)
class AddTwoContactsAndDeleteTest : AddContactTest, ContactDetailTest, DeleteContactTest {

    lateinit var userAddress: Address

    private lateinit var device: UiDevice

    @Before
    fun setUp() {
        grantContactsPermission()
        device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation())
    }

    @Test
    fun test_AddTwoContactsAndDelete() {
        val activityScenario = launchMainActivity {
            userAddress = it.address
        }

        // Navigate to CONTACTS
        AddAndEditContactTest.navigateToContacts()
        // Empty state
        onView(
            allOf(
                withId(R.id.empty_status),
                isDisplayed()
            )
        )
            .check(matches(isDisplayed()))

        onView(withId(R.id.action_add_contact))
            .check(matches(isDisplayed()))
            .check(matches(isEnabled()))
            // Navigate
            .perform(click())

        // ADD CONTACT screen

        checkAddContactScreen()

        val insertedName = "Jane Doe"
        val insertedAddress = mainTestnetAddress
        addContact(insertedName, insertedAddress, userAddress)

        // CONTACT LIST
        checkContactList(insertedName, 0)

        onView(withId(R.id.action_add_contact))
            .check(matches(isDisplayed()))
            .check(matches(isEnabled()))
            // Navigate
            .perform(click())

        // PICK contact
        checkAddContactScreen()

        val contactName = "Margaret Atwood"
        pickAContact(device, contactName)

        onView(
            allOf(
                withId(R.id.input),
                isDescendantOfA(withId(R.id.address))
            )
        )
            .check(matches(isDisplayed()))
            .check(matches(withText("")))

        val destination2 = secondaryTestnetAddress
        testInputTezosAddress(
            userAddress,
            destination = destination2,
            editTextInteraction = onView(allOf(withId(R.id.input), isDescendantOfA(withId(R.id.address)))),
            clearTextInteraction = onView(
                Matchers.allOf(
                    withId(R.id.text_input_end_icon),
                    isDescendantOfA(withId(R.id.address)),
                    withContentDescription(R.string.icon_clear_action)
                )
            )
        )

        // Save contact
        buttonInteraction()
            .check(matches(isDisplayed()))
            .check(matches(isEnabled()))
            .perform(click())

        // CONTACT LIST
        checkContactList(contactName, 1)

        /**
         * DELETE
         */

        onView(allOf(withId(R.id.contact_list), isDisplayed()))
            .perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(0, click()))

        checkContactDetailScreen(device, insertedName, insertedAddress)

        // Delete dialog
        deleteContact()

        checkContactList(contactName, 0)
        //DELETE inserted
        onView(allOf(withId(R.id.contact_list), isDisplayed()))
            .perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(0, click()))

        checkContactDetailScreen(device, contactName, destination2)

        deleteContact()

        onView(
            allOf(
                withId(R.id.toolbar_title),
                withText(R.string.wlt_navigation_contacts)
            )
        )
            .check(matches(isDisplayed()))
        // Empty state
        onView(
            allOf(
                withId(R.id.empty_status),
                isDisplayed()
            )
        )
            .check(matches(isDisplayed()))

        activityScenario.close()
    }

    fun checkContactList(name: String, index: Int) {
        onView(
            allOf(
                withId(R.id.toolbar_title),
                withText(R.string.wlt_navigation_contacts)
            )
        )
            .check(matches(isDisplayed()))

        onView(allOf(withId(R.id.contact_list), isDisplayed()))
            .check(
                matches(
                    RecyclerViewChildActions.childOfViewAtPositionWithMatcher(
                        R.id.contact_name,
                        index,
                        withText(name)
                    )
                )
            )
    }

    companion object{
        fun pickAContact(device: UiDevice, name: String){
            onView(
                allOf(
                    withId(R.id.input_icon),
                    withContentDescription(R.string.con_add_device_button)
                )
            )
                .check(matches(isDisplayed()))
                .check(matches(isEnabled()))
                .perform(click())

            // Pick contact
            device.findObject(UiSelector().text(name)).click()

            onView(
                allOf(
                    withId(R.id.input),
                    isDescendantOfA(withId(R.id.name))
                )
            )
                .check(matches(isDisplayed()))
                .check(matches(not(isEnabled())))
                .check(matches(withText(name)))
        }
    }


}
