/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.test.RecyclerViewChildActions.Companion.childOfViewAtPositionWithMatcher
import io.camlcase.smartwallet.test.resources
import org.hamcrest.CoreMatchers.allOf
import org.hamcrest.CoreMatchers.not

interface ActivityTabTest {
    /**
     * Check most recent row for the newest operation information
     */
    fun testActivityRow(
        operationType: String,
        operationAmount: String = resources.getString(R.string.act_empty),
        operationFees: String = resources.getString(R.string.act_empty)
    ) {

        onView(withText(R.string.act_screen_title))
            .check(matches(isDisplayed()))

        onView(allOf(withId(R.id.list), isDisplayed()))
            .check(
                matches(
                    childOfViewAtPositionWithMatcher(
                        R.id.operation_type,
                        1,
                        withText(operationType)
                    )
                )
            )
            .check(
                matches(
                    childOfViewAtPositionWithMatcher(
                        R.id.operation_amount,
                        1,
                        withText(operationAmount)
                    )
                )
            )
            .check(
                matches(
                    childOfViewAtPositionWithMatcher(
                        R.id.operation_amount_extra,
                        1,
                        not(isDisplayed())
                    )
                )
            )
            .check(
                matches(
                    childOfViewAtPositionWithMatcher(
                        R.id.operation_fees,
                        1,
                        withText(resources.getString(R.string.act_network_fee, operationFees))
                    )
                )
            )
    }

    fun testActivityRow(
        operationType: String,
        operationAmount: String = resources.getString(R.string.act_empty),
        operationAmountExtra: String?,
        operationFees: String = resources.getString(R.string.act_empty)
    ) {

        onView(withText(R.string.act_screen_title))
            .check(matches(isDisplayed()))

        onView(allOf(withId(R.id.list), isDisplayed()))
            .check(
                matches(
                    childOfViewAtPositionWithMatcher(
                        R.id.operation_type,
                        1,
                        withText(operationType)
                    )
                )
            )
            .check(
                matches(
                    childOfViewAtPositionWithMatcher(
                        R.id.operation_amount,
                        1,
                        withText(operationAmount)
                    )
                )
            )
            .check(
                matches(
                    childOfViewAtPositionWithMatcher(
                        R.id.operation_fees,
                        1,
                        withText(resources.getString(R.string.act_network_fee, operationFees))
                    )
                )
            )

        if (operationAmountExtra != null) {
            onView(allOf(withId(R.id.list), isDisplayed()))
                .check(
                    matches(
                        childOfViewAtPositionWithMatcher(
                            R.id.operation_amount_extra,
                            1,
                            withText(operationAmountExtra)
                        )
                    )
                )
        } else {
            onView(allOf(withId(R.id.list), isDisplayed()))
                .check(
                    matches(
                        childOfViewAtPositionWithMatcher(
                            R.id.operation_amount_extra,
                            1,
                            not(isDisplayed())
                        )
                    )
                )
        }
    }

    fun testSendDestination(destination: String) {
        onView(allOf(withId(R.id.list), isDisplayed()))
            .check(
                matches(
                    childOfViewAtPositionWithMatcher(
                        R.id.operation_detail,
                        1,
                        withText(resources.getString(R.string.act_destination_to, destination))
                    )
                )
            )
    }
}
