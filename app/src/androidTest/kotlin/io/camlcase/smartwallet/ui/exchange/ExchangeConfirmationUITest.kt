/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.exchange

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.ViewInteraction
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.uiautomator.UiDevice
import io.camlcase.kotlintezos.model.Tez
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.data.core.XTZ
import io.camlcase.smartwallet.data.core.extension.format
import io.camlcase.smartwallet.data.model.Token
import io.camlcase.smartwallet.data.model.amount.TokenBalance
import io.camlcase.smartwallet.test.buttonInteraction
import io.camlcase.smartwallet.test.getTextViewValue
import io.camlcase.smartwallet.test.resources
import org.hamcrest.CoreMatchers
import org.hamcrest.CoreMatchers.allOf

object ExchangeConfirmationUITest {
    fun checkScreenXTZToToken(
        device: UiDevice,
        fromAmount: Tez,
        toAmount: Double,
        toToken: Token,
        priceImpact: String?,
        conversionRate: String
    ): Tez {
        generalCheck(priceImpact, conversionRate)

        val formattedFromAmount = fromAmount.format(XTZ.symbol)
        onView(allOf(withId(R.id.section_main_value), isDescendantOfA(withId(R.id.exchange_section))))
            .check(matches(isDisplayed()))
            .check(matches(withText(formattedFromAmount)))

        val formattedToAmount = TokenBalance(toAmount, toToken.decimals).formattedValue + " " + toToken.symbol
        onView(allOf(withId(R.id.section_main_value), isDescendantOfA(withId(R.id.receive_section))))
            .check(matches(isDisplayed()))
            .check(matches(withText(formattedToAmount)))

        // Save fees
        onView(allOf(withId(R.id.section_main_value), isDescendantOfA(withId(R.id.network_section))))
            .check(matches(isDisplayed()))
            .check(matches(CoreMatchers.not(withText(""))))

        val feesView: ViewInteraction =
            onView(allOf(withId(R.id.section_main_value), isDescendantOfA(withId(R.id.network_section))))
        val feesText = getTextViewValue(feesView)
        val fees = Tez(feesText.split(" XTZ")[0].toDouble())

        buttonInteraction()
            .check(matches(isEnabled()))


        return fees
    }

    fun generalCheck(
        priceImpact: String?,
        conversionRate: String
    ) {
        onView(
            allOf(
                withId(R.id.expanded_title),
                withText(R.string.exc_confirm_title)
            )
        ).check(matches(isDisplayed()))

        onView(withId(R.id.price_impact))
            .check(matches(isDisplayed()))
        priceImpact?.apply {
            onView(withId(R.id.price_impact))
                .check(matches(withText(resources.getString(R.string.exc_confirm_price_impact_info, priceImpact))))
        }

        onView(withId(R.id.conversion_rate))
            .check(matches(isDisplayed()))
            .check(matches(withText(conversionRate)))
    }

    fun checkScreenTokenToXTZ(
        fromAmount: Double,
        toAmount: Double,
        fromToken: Token,
        slippage: String?,
        conversionRate: String
    ): Tez {
        generalCheck(slippage, conversionRate)

        val formattedFromAmount = TokenBalance(fromAmount, fromToken.decimals).formattedValue + " " + fromToken.symbol
        onView(allOf(withId(R.id.section_main_value), isDescendantOfA(withId(R.id.exchange_section))))
            .check(matches(isDisplayed()))
            .check(matches(withText(formattedFromAmount)))

        val formattedToAmount = Tez(toAmount).format(XTZ.symbol)
        onView(allOf(withId(R.id.section_main_value), isDescendantOfA(withId(R.id.receive_section))))
            .check(matches(isDisplayed()))
            .check(matches(withText(formattedToAmount)))

        // Save fees
        onView(allOf(withId(R.id.section_main_value), isDescendantOfA(withId(R.id.network_section))))
            .check(matches(isDisplayed()))
            .check(matches(CoreMatchers.not(withText(""))))

        val feesView: ViewInteraction =
            onView(allOf(withId(R.id.section_main_value), isDescendantOfA(withId(R.id.network_section))))
        val feesText = getTextViewValue(feesView)
        val fees = Tez(feesText.split(" XTZ")[0].toDouble())

        buttonInteraction()
            .check(matches(isEnabled()))

        return fees
    }
}
