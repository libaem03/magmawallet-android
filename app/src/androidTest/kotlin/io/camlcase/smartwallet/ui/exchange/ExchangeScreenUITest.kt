/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.exchange

import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.ViewInteraction
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.uiautomator.UiDevice
import androidx.test.uiautomator.UiSelector
import io.camlcase.kotlintezos.model.Tez
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.core.URL.DEXTER_URL
import io.camlcase.smartwallet.data.core.Tokens.Testnet.TZBTC
import io.camlcase.smartwallet.data.core.Tokens.Testnet.USDTZ
import io.camlcase.smartwallet.data.core.XTZ
import io.camlcase.smartwallet.data.model.Token
import io.camlcase.smartwallet.test.RecyclerViewChildActions
import io.camlcase.smartwallet.test.clickEvenIfPartiallyVisible
import io.camlcase.smartwallet.test.getTextViewValue
import org.hamcrest.CoreMatchers.*

object ExchangeScreenUITest {
    /**
     * MAIN
     * Run it with no animations! Or you'll need to Thread.sleep every step
     */
    fun checkExchangeScreen(device: UiDevice) {
        firstCheck()

        checkDexterModal(device)

        val fromBalanceText: ViewInteraction = onView(withId(R.id.from_balance))
        val xtzBalance = getTextViewValue(fromBalanceText).split("Balance: ", " XTZ")[1].toDouble()
        checkFromXtzAmount(xtzBalance)

        // Change to USDTez
        onView(withId(R.id.to_token))
            .check(matches(isDisplayed()))
            .check(matches(isEnabled()))
            .check(matches(withText(TZBTC.symbol)))
            .perform(click())
        checkChooseToToken(USDTZ)

        switchThroughButtons()

        switchWithButton()
    }

    fun firstCheck() {
        onView(
            allOf(
                withText(R.string.wlt_navigation_exchange),
                isDescendantOfA(withId(R.id.toolbar))
            )
        )
            .check(matches(isDisplayed()))

        onView(withId(R.id.action_extra))
            .check(matches(isDisplayed()))
            .check(matches(isEnabled()))
            .check(matches(withText(R.string.exc_action_advanced)))

        checkButtonDisabled()

        onView(
            allOf(
                withId(R.id.amount_input),
                isDescendantOfA(withId(R.id.from_amount))
            )
        )
            .check(matches(isDisplayed()))
            .check(matches(isEnabled()))

        toAmount

        onView(withId(R.id.exchange_rate))
            .check(matches(isDisplayed()))
            .check(matches(isClickable()))
    }

    private fun switchWithButton() {
        onView(withId(R.id.exchange_rate))
            .check(matches(isDisplayed()))
            .check(matches(isClickable()))
            .perform(click())

        onView(withId(R.id.from_token))
            .check(matches(isDisplayed()))
            .check(matches(isEnabled()))
            .check(matches(withText(USDTZ.symbol)))

        onView(withId(R.id.to_token))
            .check(matches(isDisplayed()))
            .check(matches(isEnabled()))
            .check(matches(withText(XTZ.symbol)))

        checkResetFields()
        Thread.sleep(1000) // wait for throttled click

        onView(withId(R.id.exchange_rate))
            .check(matches(isDisplayed()))
            .check(matches(isClickable()))
            .perform(click())

        onView(withId(R.id.from_token))
            .check(matches(isDisplayed()))
            .check(matches(isEnabled()))
            .check(matches(withText(XTZ.symbol)))

        onView(withId(R.id.to_token))
            .check(matches(isDisplayed()))
            .check(matches(isEnabled()))
            .check(matches(withText(USDTZ.symbol)))

        checkResetFields()
    }

    fun switchThroughButtons() {
        // Change FROM to USDTez
        onView(withId(R.id.from_token))
            .check(matches(isDisplayed()))
            .check(matches(isEnabled()))
            .check(matches(withText(XTZ.symbol)))
            .perform(click())


        onView(allOf(withId(R.id.list), isDisplayed()))
            .check(
                matches(
                    RecyclerViewChildActions.childOfViewAtPositionWithMatcher(
                        R.id.token_symbol,
                        0,
                        withText(XTZ.symbol)
                    )
                )
            )
            .check(
                matches(
                    RecyclerViewChildActions.childOfViewAtPositionWithMatcher(
                        R.id.token_symbol,
                        1,
                        withText(TZBTC.symbol)
                    )
                )
            )
            .check(
                matches(
                    RecyclerViewChildActions.childOfViewAtPositionWithMatcher(
                        R.id.token_symbol,
                        2,
                        withText(USDTZ.symbol)
                    )
                )
            ).perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(1, click()))

        checkResetFields()

        onView(withId(R.id.from_token))
            .check(matches(isDisplayed()))
            .check(matches(isEnabled()))
            .check(matches(withText(TZBTC.symbol)))
        checkExchangeRate(TZBTC)

        // Changed automatically to XTZ
        // USDTez -> XTZ
        onView(withId(R.id.to_token))
            .check(matches(isDisplayed()))
            .check(matches(isEnabled()))
            .check(matches(withText(XTZ.symbol)))

        // Change TO to USDTez
        // Change to FA1-2 to FA1.2 to force error
        onView(withId(R.id.to_token))
            .perform(click())
        onView(allOf(withId(R.id.list), isDisplayed()))
            .perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(1, click()))
        onView(withId(R.id.to_token))
            .check(matches(isDisplayed()))
            .check(matches(isEnabled()))
            .check(matches(withText(USDTZ.symbol)))
        onView(withText(R.string.error_token_to_token))
            .check(matches(isDisplayed()))
        Thread.sleep(5 * 1000)
        checkButtonDisabled()

        // Go back to XTZ
        onView(withId(R.id.from_token))
            .check(matches(isDisplayed()))
            .check(matches(isEnabled()))
            .check(matches(withText(TZBTC.symbol)))
            .perform(click())

        onView(allOf(withId(R.id.list), isDisplayed()))
            .perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(0, click()))

        onView(withId(R.id.from_token))
            .check(matches(isDisplayed()))
            .check(matches(isEnabled()))
            .check(matches(withText(XTZ.symbol)))
        checkResetFields()
        checkExchangeRate(XTZ)
    }

    fun checkExchangeRate(token: Token) {
        onView(withId(R.id.exchange_rate))
            .check(matches(isDisplayed()))
            .check(matches(isClickable()))
            .check(matches(withText(startsWith("1 ${token.symbol} = "))))
    }

    fun checkChooseToToken(token: Token) {
        onView(
            allOf(
                withId(R.id.toolbar),
                isDescendantOfA(withId(R.id.container))
            )
        )
            .check(matches(isDisplayed()))
            .check(matches(hasDescendant(withText(R.string.exc_select_title))))

        val position = when (token) {
            TZBTC,
            USDTZ -> 1
            else -> 0 // XTZ
        }

        // All except tzBTC
        onView(allOf(withId(R.id.list), isDisplayed()))
            .check(
                matches(
                    RecyclerViewChildActions.childOfViewAtPositionWithMatcher(
                        R.id.token_symbol,
                        0,
                        withText(XTZ.symbol)
                    )
                )
            )
            .check(
                matches(
                    RecyclerViewChildActions.childOfViewAtPositionWithMatcher(
                        R.id.token_symbol,
                        1,
                        withText(USDTZ.symbol)
                    )
                )
            ).perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(position, click()))

        Thread.sleep(1000)
        onView(withId(R.id.to_token))
            .check(matches(isDisplayed()))
            .check(matches(isEnabled()))
            .check(matches(withText(token.symbol)))

        // When fromAmount is cleared, it triggers an onAmountChanged and forces a 0.00 value on toAmount
        checkResetFields("0.00")
        onView(withId(R.id.exchange_rate))
            .check(matches(isDisplayed()))
            .check(matches(isClickable()))
            .check(matches(withText(endsWith(token.symbol))))
    }

    fun checkResetFields(toAmountText : String = "") {
        fromAmount
            .check(matches(withText("")))

        toAmount
            .check(matches(withText(toAmountText)))
        checkButtonDisabled()
    }

    fun checkFromXtzAmount(balance: Double) {
        changeFromAmount("1")
        Thread.sleep(1000)
        onView(withId(R.id.from_amount_error))
            .check(matches(not(isDisplayed())))
        checkButtonEnabled()

        changeFromAmount("0")
        Thread.sleep(1000)
        onView(withId(R.id.from_amount_error))
            .check(matches(not(isDisplayed())))
        checkButtonDisabled()

        changeFromAmount("Lorem ipsum")
        Thread.sleep(1000)
        onView(withId(R.id.from_amount_error))
            .check(matches(not(isDisplayed()))) // Will be parsed as 0.0
        checkButtonDisabled()

        val aboveMax = Tez(balance) + Tez("1")
        changeFromAmount(aboveMax.signedDecimalRepresentation.toPlainString())
        Thread.sleep(1000)
        onView(withId(R.id.from_amount_error))
            .check(matches(isDisplayed()))
            .check(matches(withText(R.string.error_insufficient_funds)))

        changeFromAmount("")
        Thread.sleep(1000)
        onView(withId(R.id.from_amount_error))
            .check(matches(not(isDisplayed())))
        checkButtonDisabled()

        fromAmount
            .perform(typeText("1.0000056"))
        fromAmount
            .check(matches(withText("1.000005"))) // Typing should be limited to 6 decimals
        Thread.sleep(1000)
        onView(withId(R.id.from_amount_error))
            .check(matches(not(isDisplayed())))
        checkButtonEnabled()

        onView(
            allOf(
                withId(R.id.action_enter_max),
                isDescendantOfA(withId(R.id.from_amount))
            )
        )
            .check(matches(isDisplayed()))
            .check(matches(isEnabled()))
            .perform(clickEvenIfPartiallyVisible())
        fromAmount
            .check(matches(withText(Tez(balance).signedDecimalRepresentation.toPlainString())))
        onView(withId(R.id.from_amount_error))
            .check(matches(not(isDisplayed())))
        checkButtonEnabled()
    }

    val fromAmount: ViewInteraction
        get() {
            return onView(
                allOf(
                    withId(R.id.amount_input),
                    isDescendantOfA(withId(R.id.from_amount))
                )
            )
                .check(matches(isDisplayed()))
                .check(matches(isEnabled()))
        }

    val toAmount: ViewInteraction
        get() {
            return onView(
                allOf(
                    withId(R.id.amount_input),
                    isDescendantOfA(withId(R.id.to_amount))
                )
            )
                .check(matches(isDisplayed()))
                .check(matches(not(isEnabled())))
        }

    fun changeFromAmount(input: String) {
        fromAmount
            .perform(replaceText(input))
    }

    fun checkButtonEnabled() {
        onView(withId(R.id.action_next))
            .check(matches(isDisplayed()))
            .check(matches(isEnabled()))
            .check(matches(withText(R.string.exc_action_review)))
    }

    fun checkButtonDisabled() {
        onView(withId(R.id.action_next))
            .check(matches(isDisplayed()))
            .check(matches(not(isEnabled())))
    }

    fun checkDexterModal(device: UiDevice) {
        onView(withId(R.id.action_sponsor))
            .check(matches(isDisplayed()))
            .check(matches(isEnabled()))
            .perform(click())

        onView(withId(R.id.detail))
            .check(matches(isDisplayed()))
            .check(matches(withText(R.string.exc_dexter_info_description)))

        onView(withId(R.id.action_continue))
            .check(matches(isDisplayed()))
            .check(matches(isEnabled()))
            .check(matches(withText(R.string.exc_dexter_info_link)))
            .perform(click())

        // Check website is correct
        device.findObject(UiSelector().text(DEXTER_URL))
        device.pressBack()

        // Won't ask for pin
    }
}
