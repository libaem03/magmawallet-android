/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.currency

import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.Espresso.pressBack
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.*
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.data.core.Wallet
import io.camlcase.smartwallet.test.initUser
import io.camlcase.smartwallet.test.resources
import io.camlcase.smartwallet.test.setAirplaneMode
import io.camlcase.smartwallet.ui.currency.ChangeLocalCurrencyUITest.Companion.checkLocalCurrencyScreen
import io.camlcase.smartwallet.ui.delegate.DelegateTest
import io.camlcase.smartwallet.ui.migration.MigrationFlowUITest
import org.hamcrest.CoreMatchers
import org.hamcrest.CoreMatchers.not
import org.junit.Before
import org.junit.Test

/**
 * If a user onboards with no connection, the list of currencies has not been loaded so nothing can be set.
 * Once connection is given, coming back should load that list
 */
class ChangeCurrencyNoConnectionUITest {

    @Before
    fun setUp() {
        setAirplaneMode(true)
    }

    @Test
    fun test_ChangeCurrency_NoConnection() {
        val activityScenario = MigrationFlowUITest.launchMainActivityWithUser(
            getWallet = {},
            initTestUser = {
                initUser(null, "test-user-tokens-mnemonic", currency = null)
            })

        // make time for airplane mode
        Thread.sleep(800)

        checkSnackBarText(resources.getString(R.string.error_no_connection))

        onView(withId(R.id.action_buy))
            .check(matches(not(isDisplayed())))
        onView(withId(R.id.action_receive))
            .check(matches(isDisplayed()))
            .check(matches(isEnabled()))
        onView(withId(R.id.action_send))
            .check(matches(isDisplayed()))
            .check(matches(not(isEnabled())))
            .perform(click())

        Thread.sleep(1000)

        DelegateTest.navigateToSettings()
        DelegateTest.checkSettings()

        ChangeLocalCurrencyUITest.checkSettingsRow(Wallet.DEFAULT_CURRENCY)
            .perform(
                RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(
                    LOCAL_CURRENCY_POSITION,
                    click()
                )
            )

        // LOCAL CURRENCY SCREEN
        onView(
            CoreMatchers.allOf(
                withId(R.id.expanded_title),
                isDescendantOfA(withId(R.id.app_bar))
            )
        ).check(matches(isDisplayed()))
            .check(matches(withText(R.string.set_currency_title)))

        onView(withId(R.id.list))
            .check(matches(not(isDisplayed())))

        onView(withId(R.id.empty_status))
            .check(matches(isDisplayed()))
            .check(
                matches(withText(R.string.set_currency_empty))
            )
        checkSnackBarText(resources.getString(R.string.error_generic) + " " + resources.getString(R.string.error_try_again))

        setAirplaneMode(false)
        // make time for airplane mode
        Thread.sleep(500)
        pressBack()

        ChangeLocalCurrencyUITest.checkSettingsRow(Wallet.DEFAULT_CURRENCY)
            .perform(
                RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(
                    LOCAL_CURRENCY_POSITION,
                    click()
                )
            )

        checkLocalCurrencyScreen()
            .perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(2, click()))

        Thread.sleep(10 * 1000)
        // Autonavigate to settings
        DelegateTest.checkSettings()

        val newCurrencyCode = "EUR"
        ChangeLocalCurrencyUITest.checkSettingsRow(newCurrencyCode)

        activityScenario.close()
    }

    companion object {
        const val LOCAL_CURRENCY_POSITION = 14

        fun checkSnackBarText(text: String) {
            onView(withId(com.google.android.material.R.id.snackbar_text))
                .check(matches(withText(text)))
        }
    }
}
