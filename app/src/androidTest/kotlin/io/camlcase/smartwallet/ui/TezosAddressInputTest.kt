package io.camlcase.smartwallet.ui

import androidx.annotation.IdRes
import androidx.annotation.StringRes
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.ViewInteraction
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.action.ViewActions.replaceText
import androidx.test.espresso.assertion.ViewAssertions.doesNotExist
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import io.camlcase.kotlintezos.model.Address
import io.camlcase.smartwallet.R
import org.hamcrest.CoreMatchers.allOf
import org.hamcrest.Matchers
import org.hamcrest.Matchers.not

interface TezosAddressInputTest {

    fun testInputTezosAddress(
        userAddress: Address,
        destination: Address,
        editTextInteraction: ViewInteraction = onView(
            allOf(
                withId(R.id.input),
                isDescendantOfA(withId(R.id.input_address))
            )
        ),
        clearTextInteraction: ViewInteraction = onView(
            Matchers.allOf(
                withId(R.id.text_input_end_icon),
                isDescendantOfA(withId(R.id.input_address)),
                withContentDescription(R.string.icon_clear_action)
            )
        ),
        @IdRes buttonId: Int = R.id.action_next
    ) {

        // Input short address
        editTextInteraction
            .check(matches(isDisplayed()))
            .perform(replaceText("tz1XSXBeWqdupm73qWAJkxJkxRzH16y77i1"))

        validateTezosAddressInputError(buttonId)

        // Click on clear
        clearText(editTextInteraction, clearTextInteraction)

        // Input user address
        editTextInteraction
            .perform(replaceText(userAddress))

        validateTezosAddressInputError(buttonId, R.string.error_own_address)

        clearText(editTextInteraction, clearTextInteraction)

        editTextInteraction
            .perform(replaceText(destination))

        onView(withId(buttonId))
            .check(matches(isEnabled()))

        onView(withText(R.string.error_invalid_address))
            .check(doesNotExist())
    }

    fun clearText(
        editTextInteraction: ViewInteraction,
        clearTextInteraction: ViewInteraction,
        @IdRes buttonId: Int = R.id.action_next
    ) {
        clearTextInteraction
            .check(matches(isDisplayed()))
            .perform(ViewActions.click())

        editTextInteraction
            .check(matches(withText("")))

        onView(withId(buttonId))
            .check(matches(not(isEnabled())))

        onView(withText(R.string.error_invalid_address))
            .check(doesNotExist())
    }

    fun validateTezosAddressInputError(
        @IdRes buttonId: Int,
        @StringRes message : Int = R.string.error_invalid_address
    ) {
        onView(withId(buttonId))
            .check(matches(not(isEnabled())))

        onView(withText(message))
            .check(matches(isDisplayed()))
    }
}
