package io.camlcase.smartwallet.ui.onboarding

import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.replaceText
import androidx.test.espresso.assertion.ViewAssertions.doesNotExist
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.uiautomator.UiDevice
import androidx.test.uiautomator.UiSelector
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.core.URL
import io.camlcase.smartwallet.test.*
import io.camlcase.smartwallet.ui.LaunchActivity
import io.camlcase.smartwallet.ui.currency.ChangeCurrencyNoConnectionUITest
import io.camlcase.smartwallet.ui.onboarding.SeedPhraseUITest.Companion.checkSeedPhrase
import io.camlcase.smartwallet.ui.onboarding.SeedPhraseUITest.Companion.getMnemonic
import org.hamcrest.Matchers.allOf
import org.hamcrest.Matchers.not
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

/**
 * ONBOARDING
 */
@LargeTest
@RunWith(AndroidJUnit4::class)
class CreateUserTest : OnboardingTest {
    private lateinit var device: UiDevice

    @Before
    fun setUp() {
        clearAllPrefs()
        device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation())
    }

    @Test
    fun createUserNoPassword() {
        createUser(null)
    }

    @Test
    fun createUserWithPassword() {
        val password = "1234"
        createUser(password)
    }

    private fun createUser(password: String?) {
        val activityScenario = ActivityScenario.launch(LaunchActivity::class.java)
        Thread.sleep(500)

        onView(withId(R.id.action_create))
            .check(matches(isDisplayed()))
            .perform(click())

        // WALLET SETUP
        checkWalletSetupScreen(device)

        onView(
            allOf(
                withId(R.id.advanced),
                withParent(withId(R.id.setup_container))
            )
        ).perform(click())

        // PASSPHRASE
        checkPassphrase()

        if (password != null) {
            onView(withId(R.id.input))
                .check(matches(isDisplayed()))
                .perform(replaceText(password))

            buttonInteraction()
                // NEXT
                .perform(click())
        } else {
            device.pressBack()
            onView(
                allOf(
                    withId(R.id.common),
                    withParent(withId(R.id.setup_container))
                )
            ).perform(click())
        }

        /**
         * RECOVERY PHRASE
         */
        Thread.sleep(500)
        onView(withId(R.id.title))
            .check(matches(withText(R.string.onb_recovery_phrase_display_title)))

        onView(withId(R.id.action_primary))
            .check(matches(isDisplayed()))
            // NEXT
            .perform(click())

        onView(
            allOf(
                withContentDescription("Navigate back"),
                withParent(
                    allOf(
                        withId(R.id.toolbar),
                        withParent(withId(R.id.toolbar_layout))
                    )
                )
            )
        ).check(doesNotExist())
        checkSeedPhrase()

        val mnemonic = getMnemonic()
        buttonInteraction()
            .check(matches(isDisplayed()))
            .check(matches(isEnabled()))
            .perform(click())

        // SEED PHRASE CHECK
        checkConfirmSeedPhrase(mnemonic, password)

        Thread.sleep(5 * 1000) // Wait for snackbars to disappear
        buttonInteraction()
            .check(matches(isDisplayed()))
            .check(matches(isEnabled()))
            .perform(click())

        /**
         * PIN
         */

        testPin()

        activityScenario.close()
    }

    companion object {
        internal fun checkWalletSetupScreen(device: UiDevice) {
            onView(
                allOf(
                    withId(R.id.title),
                    withParent(
                        allOf(
                            withId(R.id.expanded_title),
                            withParent(withId(R.id.toolbar_layout))
                        )
                    )
                )
            )
                .check(matches(isDisplayed()))
                .check(matches(withText(R.string.onb_wallet_setup_title)))

            onView(withId(R.id.common))
                .check(matches(isDisplayed()))

            onView(withId(R.id.advanced))
                .check(matches(isDisplayed()))

            // Help modal
            onView(
                allOf(
                    withId(R.id.action_help),
                    withContentDescription(R.string.icon_help)
                )
            )
                .check(matches(isDisplayed()))
                .perform(click())

            onView(
                allOf(
                    withId(R.id.close),
                    withContentDescription("Close"),
                    withParent(withParent(withId(R.id.toolbar)))
                )
            ).check(matches(isDisplayed()))

            onView(
                allOf(
                    withId(R.id.detail),
                    withParent(withParent(withId(R.id.design_bottom_sheet))),
                    isDisplayed()
                )
            ).check(matches(withText(R.string.onb_wallet_passphrase_info_detail)))

            onView(
                allOf(
                    withId(R.id.action_out),
                    withParent(withParent(withId(R.id.design_bottom_sheet))),
                    isDisplayed()
                )
            ).check(matches(isDisplayed()))
                .check(matches(withText(R.string.onb_wallet_passphrase_info_action)))
                .check(matches(isEnabled()))
                .perform(click())

            // Check website is correct
            device.findObject(UiSelector().text(URL.WALLET_SETUP_URL))
            device.pressBack()

            Thread.sleep(500)
            // Modal will appear closed

            onView(
                allOf(
                    withId(R.id.common),
                    withParent(withId(R.id.setup_container))
                )
            ).check(matches(isDisplayed()))

            onView(
                allOf(
                    withId(R.id.advanced),
                    withParent(withId(R.id.setup_container))
                )
            ).check(matches(isDisplayed()))

            // Back button
            onView(
                allOf(
                    withContentDescription("Navigate back"),
                    withParent(
                        allOf(
                            withId(R.id.toolbar),
                            withParent(withId(R.id.toolbar_layout))
                        )
                    )
                )
            ).check(matches(isDisplayed()))
                .check(matches(isClickable()))
                .check(matches(isEnabled()))
        }

        internal fun checkPassphrase() {
            Thread.sleep(500)
            onView(
                allOf(
                    withId(R.id.title),
                    withParent(
                        allOf(
                            withId(R.id.expanded_title),
                            withParent(withId(R.id.toolbar_layout))
                        )
                    ),
                )
            ).check(matches(isDisplayed()))
                .check(matches(withText(R.string.onb_wallet_passphrase_title)))

            onView(
                allOf(
                    withId(R.id.action_help),
                    withContentDescription(R.string.icon_help)
                )
            ).check(matches(not(isDisplayed())))

            onView(
                allOf(
                    withId(R.id.info_highlight),
                    withParent(
                        allOf(
                            withId(R.id.passphrase_container),
                            withParent(withId(R.id.switcher))
                        )
                    )
                )
            ).check(matches(isDisplayed()))
                .check(matches(withText(R.string.onb_wallet_passphrase_field_info_highlight)))

            actionIsEnabled()

            onView(
                allOf(
                    withContentDescription("Navigate back"),
                    withParent(
                        allOf(
                            withId(R.id.toolbar),
                            withParent(withId(R.id.toolbar_layout))
                        )
                    )
                )
            ).check(matches(isDisplayed()))
                .perform(click())

            onView(
                allOf(
                    withId(R.id.title),
                    withParent(
                        allOf(
                            withId(R.id.expanded_title),
                            withParent(withId(R.id.toolbar_layout))
                        )
                    )
                )
            )
                .check(matches(isDisplayed()))
                .check(matches(withText(R.string.onb_wallet_setup_title)))
            onView(
                allOf(
                    withId(R.id.advanced),
                    withParent(withId(R.id.setup_container))
                )
            ).perform(click())

            onView(withId(R.id.input))
                .check(matches(isDisplayed()))
                .perform(replaceText("Lorem ipsum dolor sit amet, consectetur adipiscing elit"))
                // 50 char limit
                .check(matches(withText("Lorem ipsum dolor sit amet, consectetur adipiscing")))

            actionIsEnabled()

            onView(
                allOf(
                    withId(R.id.text_input_end_icon),
                    withContentDescription("Toggle password visibility"),
                )
            )
                .check(matches(isDisplayed()))

            onView(
                allOf(
                    withContentDescription("Navigate back"),
                    withParent(
                        allOf(
                            withId(R.id.toolbar),
                            withParent(withId(R.id.toolbar_layout))
                        )
                    )
                )
            ).check(matches(isDisplayed()))
        }

        private fun actionIsEnabled() {
            onView(
                allOf(
                    withId(R.id.action_next),
                    withParent(
                        allOf(
                            withId(R.id.button_container),
                            withParent(withId(R.id.container))
                        )
                    )
                )
            ).check(matches(isDisplayed()))
                .check(matches(withText(R.string.action_continue)))
                .check(matches(isClickable()))
        }

        internal fun checkConfirmSeedPhrase(mnemonic: List<String>, password: String?) {
            onView(
                allOf(
                    withId(R.id.expanded_title),
                    isDisplayed()
                )
            ).check(matches(isDisplayed()))
                .check(matches(withText(R.string.onb_recovery_phrase_backup_title)))

            // Back button
            onView(
                allOf(
                    withContentDescription("Navigate back"),
                    withParent(
                        allOf(
                            withId(R.id.toolbar),
                            withParent(withId(R.id.toolbar_layout))
                        )
                    )
                )
            ).check(matches(isDisplayed()))

            buttonInteraction()
                .check(matches(isDisplayed()))
                .check(matches(not(isEnabled())))

            buttonInteraction(R.id.action_skip)
                .check(matches(isDisplayed()))
                .check(matches(isEnabled()))

            checkConfirmMnemonic(mnemonic)
            checkConfirmPassphrase(password)
        }

        private fun checkConfirmPassphrase(password: String?) {
            if (password != null) {
                buttonInteraction()
                    .perform(click())
                ChangeCurrencyNoConnectionUITest.checkSnackBarText(resources.getString(R.string.onb_recovery_phrase_backup_error))
                Thread.sleep(2 * 1000)

                simpleEditTextWith(R.id.input_passphrase)
                    .perform(replaceText("4321"))

                buttonInteraction()
                    .perform(click())

                ChangeCurrencyNoConnectionUITest.checkSnackBarText(resources.getString(R.string.onb_recovery_phrase_backup_error))
                Thread.sleep(2 * 1000)

                simpleEditTextWith(R.id.input_passphrase)
                    .perform(replaceText(password))
            } else {
                simpleEditTextWith(R.id.input_passphrase)
                    .check(matches(not(isDisplayed())))
            }
        }

        fun checkConfirmMnemonic(mnemonic: List<String>) {
            simpleEditTextWith(R.id.input_mnemonic)
                .check(matches(isDisplayed()))
                .perform(replaceText("Lorem ipsum dolor 1sit amet consectetur adipiscing elit In mollis aliquet erat"))

            simpleEditTextErrorWith(R.id.input_mnemonic)
                .check(matches(isDisplayed()))
                .check(matches(withText(R.string.error_invalid_seed_phrase)))

            buttonInteraction()
                .check(matches(isDisplayed()))
                .check(matches(not(isEnabled())))

            simpleEditTextWith(R.id.input_mnemonic)
                .perform(replaceText("Lorem ipsum dolor sit amet consectetur adipiscing elit In mollis aliquet erat"))
                // No upper caps
                .check(matches(withText("lorem ipsum dolor sit amet consectetur adipiscing elit in mollis aliquet erat")))

            buttonInteraction()
                .check(matches(isDisplayed()))
                .check(matches(isEnabled()))
                .perform(click())

            ChangeCurrencyNoConnectionUITest.checkSnackBarText(resources.getString(R.string.error_invalid_seed_phrase))
            Thread.sleep(2 * 1000)

            simpleEditTextWith(R.id.input_mnemonic)
                .perform(replaceText("     " + mnemonic.joinToString(" ")))

            buttonInteraction()
                .check(matches(isDisplayed()))
                .check(matches(isEnabled()))
        }
    }
}
