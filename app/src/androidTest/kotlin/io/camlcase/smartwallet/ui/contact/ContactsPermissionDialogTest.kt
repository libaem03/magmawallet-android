/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.contact

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.ViewInteraction
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.uiautomator.UiDevice
import androidx.test.uiautomator.UiSelector
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.test.assertViewWithTextIsVisible

interface ContactsPermissionDialogTest {
    fun testDialog(
        device: UiDevice,
        buttonInteraction: ViewInteraction = onView(withId(R.id.action_next))

    ) {
        // FIRST DIALOG
        device.findObject(UiSelector().text(DIALOG_MESSAGE))
        assertViewWithTextIsVisible(device, DIALOG_ALLOW)
        assertViewWithTextIsVisible(device, DIALOG_DENY)

        device.findObject(UiSelector().text(DIALOG_DENY)).click()

        buttonInteraction
            .check(matches(isDisplayed()))
            .check(matches(isEnabled()))
            .perform(click())

        // SECOND DIALOG
        device.findObject(UiSelector().text(DIALOG_MESSAGE))
        assertViewWithTextIsVisible(device, DIALOG_ALLOW)
        assertViewWithTextIsVisible(device, DIALOG_DENY)
//        assertViewWithTextIsVisible(device, DIALOG_DONT_ASK)

        // Deny & don't ask again seems not to be recognized
        device.findObject(UiSelector().index(2)).click()

        buttonInteraction
            .check(matches(isDisplayed()))
            .check(matches(isEnabled()))
            .perform(click())

        // SETTINGS SCREEN
        device.findObject(UiSelector().text(SETTINGS_TITLE))
        device.findObject(UiSelector().text(SETTINGS_TITLE_APP))
        device.findObject(UiSelector().text(SETTINGS_PERMISSIONS)).click()

        device.findObject(UiSelector().text(SETTINGS_PERMISSIONS_CONTACTS)).click()

        device.findObject(UiSelector().text(SETTINGS_PERMISSIONS_CONTACTS_TITLE))
        device.findObject(UiSelector().text(DIALOG_ALLOW)).click()

        device.pressBack()
        device.pressBack()
        device.pressBack()

        // PIN Will be asked again
//        onView(withId(R.id.pin_input))
//            .check(matches(isDisplayed()))
//            .perform(click())
//            .perform(ViewActions.typeText("1234"))

    }

    companion object {
        const val DIALOG_MESSAGE = "Allow Magma Testnet to access your contacts?"
        const val DIALOG_ALLOW = "Allow"
        const val DIALOG_DENY = "Deny"
        const val DIALOG_DONT_ASK = "Deny & don't ask again"
        const val SETTINGS_TITLE = "App Info"
        const val SETTINGS_TITLE_APP = "Magma Testnet"
        const val SETTINGS_PERMISSIONS = "Permissions"
        const val SETTINGS_PERMISSIONS_CONTACTS = "Contacts"
        const val SETTINGS_PERMISSIONS_CONTACTS_TITLE = "CONTACTS ACCESS FOR THIS APP"
        const val APP_BACK = "Navigate up"
    }
}
