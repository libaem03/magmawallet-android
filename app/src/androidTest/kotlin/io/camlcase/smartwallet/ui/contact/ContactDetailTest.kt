/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.contact

import androidx.annotation.StringRes
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.ViewInteraction
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.replaceText
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.uiautomator.UiDevice
import io.camlcase.kotlintezos.model.Address
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.test.RecyclerViewChildActions
import io.camlcase.smartwallet.test.buttonInteraction
import io.camlcase.smartwallet.test.resources
import org.hamcrest.CoreMatchers.allOf
import org.hamcrest.CoreMatchers.not

/**
 * Preconditions:
 * User only has XTZ balance
 */
interface ContactDetailTest: ContactSendFlow {

    fun checkContactDetailScreen(
        device: UiDevice,
        name: String,
        address: Address
    ) {
        onView(allOf(withId(R.id.contact_name), isDescendantOfA(withId(R.id.expanded_title))))
            .check(matches(isDisplayed()))
            .check(matches(withText(name)))

        onView(allOf(withId(R.id.contact_address), isDescendantOfA(withId(R.id.expanded_title))))
            .check(matches(isDisplayed()))
            .check(matches(withText(address)))

        // SEND
        checkListOption(0, R.string.con_details_send)
        onView(allOf(withId(R.id.list), isDisplayed()))
            .perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(0, click()))
        testContactSendFlow(device, name, address)

        // ACTIVITY
        checkListOption(1, R.string.con_details_history)
        onView(allOf(withId(R.id.list), isDisplayed()))
            .perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(1, click()))
        onView(withId(R.id.toolbar_title))
            .check(matches(not(isDisplayed())))

        onView(withId(R.id.contact_operation_title))
            .check(matches(isDisplayed()))
            .check(matches(withText(R.string.con_details_history)))
        device.pressBack()

        // EDIT
        checkListOption(2, R.string.con_details_edit)
        onView(allOf(withId(R.id.list), isDisplayed()))
            .perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(2, click()))
        onView(withId(R.id.expanded_title))
            .check(matches(isDisplayed()))
            .check(matches(withText(R.string.con_edit_title)))
        device.pressBack()

        // DELETE
        checkListOption(3, R.string.con_details_delete)
        onView(allOf(withId(R.id.list), isDisplayed()))
            .perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(3, click()))
        onView(withText(R.string.con_delete_title))
            .check(matches(isDisplayed()))
        onView(withText(R.string.action_cancel))
            .check(matches(isDisplayed()))
            .check(matches(isEnabled()))
            .perform(click())
    }

    fun checkListOption(position: Int, @StringRes optionName: Int): ViewInteraction? {
        return onView(allOf(withId(R.id.list), isDisplayed()))
            .check(
                matches(
                    RecyclerViewChildActions.childOfViewAtPositionWithMatcher(
                        R.id.option_name,
                        position,
                        withText(optionName)
                    )
                )
            )
    }
}

interface ContactSendFlow {
    fun testContactSendFlow(
        device: UiDevice,
        name: String,
        address: Address
    ) {
        onView(withId(R.id.expanded_title_text))
            .check(matches(withText("Send XTZ")))
            .check(matches(isDisplayed()))
        onView(withId(R.id.amount_field))
            .check(matches(isDisplayed()))
            .perform(replaceText("1"))
        buttonInteraction().perform(click())
        Thread.sleep(10 * 1000)
        val recipient = resources.getString(R.string.snd_address_contact_section, name)
        onView(withId(R.id.recipient_title))
            .check(matches(isDisplayed()))
            .check(matches(withText(recipient)))
        onView(withId(R.id.recipient))
            .check(matches(isDisplayed()))
            .check(matches(withText(address)))
        device.pressBack()
        device.pressBack()
    }
}
