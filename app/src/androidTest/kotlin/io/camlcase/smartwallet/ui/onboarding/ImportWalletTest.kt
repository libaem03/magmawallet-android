package io.camlcase.smartwallet.ui.onboarding

import androidx.recyclerview.widget.RecyclerView
import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.replaceText
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.core.Onboarding
import io.camlcase.smartwallet.core.extension.ellipsize
import io.camlcase.smartwallet.test.*
import io.camlcase.smartwallet.ui.LaunchActivity
import org.hamcrest.CoreMatchers
import org.hamcrest.Matchers.allOf
import org.hamcrest.Matchers.not
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@LargeTest
@RunWith(AndroidJUnit4::class)
class ImportWalletTest : OnboardingTest {

    @Before
    fun setUp() {
        clearAllPrefs()
    }

    @Test
    fun importWalletTest() {
        val activityScenario: ActivityScenario<LaunchActivity> = ActivityScenario.launch(LaunchActivity::class.java)

        Thread.sleep(500)

        onView(withId(R.id.action_import))
            .check(matches(isDisplayed()))
            .perform(click())

        val wallet = getTestWallet()!!

        /**
         * IMPORT SEED PHRASE
         */
        checkImportScreen(wallet.mnemonic, wallet.passphrase)

        buttonInteraction()
            .perform(click())

        /**
         * BALANCE CHECK
         */
        Thread.sleep(5 * 1000) // wait for network fetch of balances
        checkBalanceCheck()

        onView(CoreMatchers.allOf(withId(R.id.list), isDisplayed()))
            .check(
                matches(
                    RecyclerViewChildActions.childOfViewAtPositionWithMatcher(
                        R.id.balance_xtz,
                        1,
                        withText(resources.getString(R.string.onb_import_balance_subtitle, "0 XTZ"))
                    )
                )
            )
            .check(
                matches(
                    RecyclerViewChildActions.childOfViewAtPositionWithMatcher(
                        R.id.balance_tokens,
                        1,
                        withText(resources.getString(R.string.onb_import_balance_other_tokens, "No"))
                    )
                )
            )
            .check(
                matches(
                    RecyclerViewChildActions.childOfViewAtPositionWithMatcher(
                        R.id.address,
                        3,
                        withText(wallet.address.ellipsize())
                    )
                )
            )
            .check(
                matches(
                    RecyclerViewChildActions.childOfViewAtPositionWithMatcher(
                        R.id.balance_tokens,
                        3,
                        withText(resources.getString(R.string.onb_import_balance_other_tokens, "No"))
                    )
                )
            ).perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(3, click()))

        /**
         * Migration (Android) selection
         */

        onView(withId(R.id.migration_import))
            .check(matches(isDisplayed()))
            .check(matches(isEnabled()))

        onView(withId(R.id.regular_import))
            .check(matches(isDisplayed()))
            .check(matches(isEnabled()))
            .perform(click())

        /**
         * SUCCESS
         */

        onView(withId(R.id.title))
            .check(matches(withText(R.string.onb_success_title)))

        onView(withId(R.id.action_primary))
            .check(matches(isDisplayed()))
            .perform(click())

        /**
         * PIN
         */
        testPin()

        activityScenario.close()
    }

    companion object {
        fun checkImportScreen(mnemonic: List<String>, password: String?) {
            onView(
                allOf(
                    withId(R.id.expanded_title),
                    isDisplayed()
                )
            ).check(matches(isDisplayed()))
                .check(matches(withText(R.string.onb_import_seed_phrase_title)))

            // Back button
            onView(
                allOf(
                    withContentDescription("Navigate back"),
                    withParent(
                        allOf(
                            withId(R.id.toolbar),
                            withParent(withId(R.id.toolbar_layout))
                        )
                    )
                )
            ).check(matches(isDisplayed()))

            // Help modal
            onView(
                allOf(
                    withId(R.id.help),
                    withContentDescription(R.string.icon_help)
                )
            )
                .check(matches(isDisplayed()))
                .perform(click())

            onView(
                allOf(
                    withId(R.id.detail),
                    withParent(withParent(withId(R.id.design_bottom_sheet))),
                    isDisplayed()
                )
            ).check(matches(withText(R.string.onb_import_seed_phrase_info_modal_detail_android)))

            onView(
                allOf(
                    withId(R.id.detail_highlight),
                    withParent(withParent(withId(R.id.design_bottom_sheet))),
                    isDisplayed()
                )
            ).check(matches(withText(R.string.onb_import_seed_phrase_info_modal_highlight)))

            onView(
                allOf(
                    withId(R.id.close),
                    withContentDescription("Close"),
                    withParent(withParent(withId(R.id.toolbar)))
                )
            ).check(matches(isDisplayed()))
                .perform(click())

            // Fields
            simpleEditTextWith(R.id.input_mnemonic)
                .check(matches(isDisplayed()))

            simpleEditTextWith(R.id.input_passphrase)
                .check(matches(isDisplayed()))
            onView(
                allOf(
                    withId(R.id.text_input_end_icon),
                    withContentDescription("Toggle password visibility"),
                )
            )
                .check(matches(isDisplayed()))

            onView(withId(R.id.advanced_options_container))
                .check(matches(not(isDisplayed())))

            onView(withId(R.id.action_show))
                .check(matches(isDisplayed()))
                .check(matches(isEnabled()))
                .check(matches(withText(R.string.onb_import_seed_phrase_advanced_title)))
                .perform(click())
            onView(withId(R.id.advanced_options_container))
                .check(matches(isDisplayed()))

            checkDerivationPath()

            Thread.sleep(500)

            onView(withId(R.id.action_show))
                .perform(click())
            onView(withId(R.id.advanced_options_container))
                .check(matches(not(isDisplayed())))

            buttonInteraction()
                .check(matches(isDisplayed()))
                .check(matches(not(isEnabled())))

            CreateUserTest.checkConfirmMnemonic(mnemonic)
            if (!password.isNullOrBlank()) {
                simpleEditTextWith(R.id.input_passphrase)
                    .perform(replaceText(password))
            }
        }

        private fun checkDerivationPath() {
            simpleEditTextWith(R.id.input_derivation_path)
                .check(matches(isDisplayed()))
                .perform(replaceText("Lorem ipsum"))
            simpleEditTextErrorWith(R.id.input_derivation_path)
                .check(matches(isDisplayed()))
                .check(matches(withText(R.string.onb_import_seed_phrase_advanced_derivation_error)))

            simpleEditTextWith(R.id.input_derivation_path)
                .perform(replaceText("0/0/0"))
            simpleEditTextErrorWith(R.id.input_derivation_path)
                .check(matches(not(isDisplayed())))

            simpleEditTextWith(R.id.input_derivation_path)
                .perform(replaceText("489/1'/489"))
            simpleEditTextErrorWith(R.id.input_derivation_path)
                .check(matches(not(isDisplayed())))

            simpleEditTextWith(R.id.input_derivation_path)
                .perform(replaceText("1'/200\'"))
            simpleEditTextErrorWith(R.id.input_derivation_path)
                .check(matches(isDisplayed()))
                .check(matches(withText(R.string.onb_import_seed_phrase_advanced_derivation_error)))

            simpleEditTextWith(R.id.input_derivation_path)
                .perform(replaceText(Onboarding.BIP44_DEFAULT))
            simpleEditTextErrorWith(R.id.input_derivation_path)
                .check(matches(not(isDisplayed())))
        }

        internal fun checkBalanceCheck() {
            onView(
                allOf(
                    withId(R.id.expanded_title),
                    isDisplayed()
                )
            ).check(matches(isDisplayed()))
                .check(matches(withText(R.string.onb_import_seed_phrase_title)))

            // Help modal
            onView(
                allOf(
                    withId(R.id.help),
                    withContentDescription(R.string.icon_help),
                    isDisplayed()
                )
            )
                .perform(click())

            onView(
                allOf(
                    withId(R.id.detail),
                    withParent(withParent(withId(R.id.design_bottom_sheet))),
                    isDisplayed()
                )
            ).check(matches(withText(R.string.onb_import_balance_info_modal_detail_android)))

            onView(
                allOf(
                    withId(R.id.detail_highlight),
                    withParent(withParent(withId(R.id.design_bottom_sheet))),
                )
            ).check(matches(not(isDisplayed())))

            onView(
                allOf(
                    withId(R.id.close),
                    withContentDescription("Close"),
                    withParent(withParent(withId(R.id.toolbar)))
                )
            ).check(matches(isDisplayed()))
                .perform(click())
        }
    }
}
