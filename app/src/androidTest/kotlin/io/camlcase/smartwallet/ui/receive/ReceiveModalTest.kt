/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.receive

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import io.camlcase.kotlintezos.model.Address
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.test.launchMainActivity
import org.hamcrest.CoreMatchers.*
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Preconditions
 * - User has no balance
 */
@LargeTest
@RunWith(AndroidJUnit4::class)
class ReceiveModalTest {
    lateinit var userAddress: Address

    @Test
    fun testShowReceiveModal() {
        val activityScenario = launchMainActivity("test-user-empty-mnemonic") {
            userAddress = it.address
        }

        onView(withId(R.id.bottom_navigation))
            .check(matches(isDisplayed()))

        Thread.sleep(15 * 1000) // Wait for balances to be retrieved

        onView(withId(R.id.total_balance_currency))
            .check(matches(isDisplayed()))
            .check(matches(withText(containsString("0.00"))))

        onView(withId(R.id.balance_xtz_container))
            .check(matches(isDisplayed()))

        onView(
            allOf(
                withId(R.id.empty_status),
                isDisplayed()
            )
        )
            .check(matches(isDisplayed()))

        onView(withId(R.id.action_send))
            .check(matches(isDisplayed()))
            .check(matches(not(isEnabled())))

        onView(withId(R.id.action_receive))
            .check(matches(isDisplayed()))
            .check(matches(isEnabled()))
            .perform(click())

        Thread.sleep(300)
        // MODAL
        onView(withId(R.id.subtitle))
            .check(matches(isDisplayed()))
            .check(matches(withText(R.string.rec_subtitle)))

        onView(withId(R.id.qr_code))
            .check(matches(isDisplayed()))

        onView(withId(R.id.tz_address))
            .check(matches(isDisplayed()))
            .check(matches(withText(userAddress)))

        onView(withId(R.id.action_copy))
            .check(matches(isDisplayed()))
            .check(matches(isEnabled()))
            .perform(click())

        onView(withText(R.string.action_copy_success))
            .check(matches(isDisplayed()))

        activityScenario.close()
    }
}
