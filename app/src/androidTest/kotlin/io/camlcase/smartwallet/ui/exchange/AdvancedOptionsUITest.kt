/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.exchange

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.ViewInteraction
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.uiautomator.UiDevice
import io.camlcase.smartwallet.R
import org.hamcrest.CoreMatchers.allOf
import org.hamcrest.CoreMatchers.not

/**
 * @see AdvancedExchangeOptionsFragment
 */
object AdvancedOptionsUITest {
    fun checkOptionsModal(
        device: UiDevice,
        slippage: Double,
        timeout: Int
    ) {
        onView(
            allOf(
                withId(R.id.toolbar),
                isDescendantOfA(withId(R.id.container))
            )
        )
            .check(matches(isDisplayed()))
            .check(matches(hasDescendant(withText(R.string.exc_slippage_title))))

        onView(withId(R.id.slippage_info))
            .check(matches(isDisplayed()))
            .check(matches(withText(R.string.exc_slippage_description)))

        onView(withId(R.id.timeout_info))
            .check(matches(isDisplayed()))
            .check(matches(withText(R.string.exc_timeout_detail)))

        onView(withId(R.id.input_slippage_error))
            .check(matches(not(isDisplayed())))

        timeoutInput
            .check(matches(isDisplayed()))
            .check(matches(isEnabled()))
            .check(matches(withText("")))

        errorTimeoutInput
            .check(matches(not(isDisplayed())))

        /**
         * SLIPPAGE
         */

        buttonsEnabled()

        changeSlippageInput("1")
        onView(withId(R.id.input_slippage_error))
            .check(matches(not(isDisplayed())))
        buttonsEnabledNotSelected()

        changeSlippageInput("50")
        onView(withId(R.id.input_slippage_error))
            .check(matches(isDisplayed()))
            .check(matches(withText(R.string.error_slippage_exceed)))
        buttonsEnabledNotSelected()

        changeSlippageInput("Lorem ipsum")
        onView(withId(R.id.input_slippage_error))
            .check(matches(isDisplayed()))
            .check(matches(withText(R.string.error_generic)))
        buttonsEnabledNotSelected()
        // Check it doesn't save it
        device.pressBack()
        onView(withId(R.id.action_extra))
            .perform(click())
        buttonsEnabled()
        slippageInput
            .check(matches(withText("")))

        changeSlippageInput("0.00005")
        onView(withId(R.id.input_slippage_error))
            .check(matches(not(isDisplayed())))
        buttonsEnabledNotSelected()

        changeSlippageInput("")
        onView(withId(R.id.input_slippage_error))
            .check(matches(not(isDisplayed())))
        buttonsEnabled()

        onView(
            allOf(
                withId(R.id.input),
                isDescendantOfA(withId(R.id.input_slippage))
            )
        )
            .perform(typeText("0.04567"))
            .check(matches(withText("0.04"))) // Accepts only 2 decimals
        onView(withId(R.id.input_slippage_error))
            .check(matches(not(isDisplayed())))
        buttonsEnabledNotSelected()
        // Check it saves it
        device.pressBack() // hide keyboard
        device.pressBack() // leave modal
        onView(withId(R.id.action_extra))
            .perform(click())
        buttonsEnabledNotSelected()
        slippageInput
            .check(matches(withText("0.04")))

        changeSlippageInput("")
        onView(withId(R.id.input_slippage_error))
            .check(matches(not(isDisplayed())))
        buttonsEnabled()

        changeOptionButtons(device)

        changeSlippageInput(slippage.toString())
        onView(withId(R.id.input_slippage_error))
            .check(matches(not(isDisplayed())))
        buttonsEnabledNotSelected()

        /**
         * TIMEOUT
         */
        timeoutInput
            .perform(replaceText("0"))
        errorTimeoutInput
            .check(matches((isDisplayed())))
            .check(matches(withText(R.string.error_exchange_timeout)))

        timeoutInput
            .perform(replaceText("1441"))
        errorTimeoutInput
            .check(matches((isDisplayed())))
            .check(matches(withText(R.string.error_exchange_timeout)))
        // Check it doesn't save it
        device.pressBack()
        onView(withId(R.id.action_extra))
            .perform(click())
        timeoutInput
            .check(matches(isDisplayed()))
            .check(matches(isEnabled()))
            .check(matches(withText("")))

        timeoutInput
            .perform(replaceText("1440"))
        errorTimeoutInput
            .check(matches(not(isDisplayed())))

        timeoutInput
            .perform(replaceText("1"))
        errorTimeoutInput
            .check(matches(not(isDisplayed())))

        timeoutInput
            .perform(replaceText(""))
            .perform(typeText("12345"))
            .check(matches(withText("1234"))) // Only 4 digits
        errorTimeoutInput
            .check(matches(not(isDisplayed())))
        // Check it saves it
        device.pressBack()
        device.pressBack()
        onView(withId(R.id.action_extra))
            .perform(click())
        timeoutInput
            .check(matches(isDisplayed()))
            .check(matches(isEnabled()))
            .check(matches(withText("1234")))
        slippageInput
            .check(matches(withText(slippage.toString())))

        timeoutInput
            .perform(replaceText(timeout.toString()))
        errorTimeoutInput
            .check(matches(not(isDisplayed())))
        device.pressBack()
    }

    fun changeOptionButtons(device: UiDevice) {
        onView(withId(R.id.slippage_option1))
            .check(matches(isDisplayed()))
            .check(matches(isEnabled()))
            .perform(click())
            .check(matches(isChecked()))

        onView(withId(R.id.slippage_option2))
            .check(matches(isDisplayed()))
            .check(matches(isEnabled()))
            .check(matches(not(isChecked())))
        onView(withId(R.id.slippage_option3))
            .check(matches(isDisplayed()))
            .check(matches(isEnabled()))
            .check(matches(not(isChecked())))

        device.pressBack()

        onView(withId(R.id.action_extra))
            .perform(click())

        // Check selection was saved
        onView(withId(R.id.slippage_option1))
            .check(matches(isDisplayed()))
            .check(matches(isEnabled()))
            .check(matches(isChecked()))
        onView(withId(R.id.slippage_option2))
            .check(matches(isDisplayed()))
            .check(matches(isEnabled()))
            .check(matches(not(isChecked())))
        onView(withId(R.id.slippage_option3))
            .check(matches(isDisplayed()))
            .check(matches(isEnabled()))
            .check(matches(not(isChecked())))

        // Go back to default
        onView(withId(R.id.slippage_option2))
            .check(matches(isDisplayed()))
            .check(matches(isEnabled()))
            .perform(click())
            .check(matches(isChecked()))

        buttonsEnabled()
    }

    val slippageInput: ViewInteraction
        get() {
            return onView(
                allOf(
                    withId(R.id.input),
                    isDescendantOfA(withId(R.id.input_slippage))
                )
            )
        }

    val timeoutInput: ViewInteraction
        get() {
            return onView(
                allOf(
                    withId(R.id.input),
                    isDescendantOfA(withId(R.id.input_timeout))
                )
            )
        }
    val errorTimeoutInput: ViewInteraction
        get() {
            return onView(
                allOf(
                    withId(R.id.input_error),
                    isDescendantOfA(withId(R.id.input_timeout))
                )
            )
        }

    fun changeSlippageInput(input: String) {
        slippageInput
            .perform(replaceText(input))
    }

    fun buttonsEnabled() {
        onView(withId(R.id.slippage_option1))
            .check(matches(isDisplayed()))
            .check(matches(isEnabled()))
            .check(matches(not(isChecked())))

        onView(withId(R.id.slippage_option2))
            .check(matches(isDisplayed()))
            .check(matches(isEnabled()))
            .check(matches(isChecked()))

        onView(withId(R.id.slippage_option3))
            .check(matches(isDisplayed()))
            .check(matches(isEnabled()))
            .check(matches(not(isChecked())))
    }

    fun buttonsEnabledNotSelected() {
        onView(withId(R.id.slippage_option1))
            .check(matches(isDisplayed()))
            .check(matches(isEnabled()))
            .check(matches(not(isChecked())))

        onView(withId(R.id.slippage_option2))
            .check(matches(isDisplayed()))
            .check(matches(isEnabled()))
            .check(matches(not(isChecked())))

        onView(withId(R.id.slippage_option3))
            .check(matches(isDisplayed()))
            .check(matches(isEnabled()))
            .check(matches(not(isChecked())))
    }
}
