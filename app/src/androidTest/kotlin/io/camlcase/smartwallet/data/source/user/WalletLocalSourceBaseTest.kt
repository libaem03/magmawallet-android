/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.source.user

import androidx.test.platform.app.InstrumentationRegistry
import io.camlcase.kotlintezos.wallet.SDWallet
import io.camlcase.kotlintezos.wallet.SimulatedSignatureProvider
import io.camlcase.smartwallet.data.SecurePreferencesTest
import io.camlcase.smartwallet.data.core.AppContext
import io.camlcase.smartwallet.data.model.AppWallet
import io.camlcase.smartwallet.data.source.local.SecurePreferences
import io.camlcase.smartwallet.data.source.local.UserPreferences
import io.mockk.*
import org.junit.After
import java.lang.ref.WeakReference

open class WalletLocalSourceBaseTest {
    internal val context = InstrumentationRegistry.getInstrumentation().targetContext
    internal var appContext: AppContext = WeakReference(context)

    internal val securePreferences: SecurePreferences =
        spyk(SecurePreferences(appContext, SecurePreferencesTest.TEST_FILENAME), recordPrivateCalls = true)
    val flagPreferences = mockk<UserPreferences>(relaxed = true)

    internal fun initLocalSource(): WalletLocalSource {
        return WalletLocalSource(securePreferences, flagPreferences)
    }

    // Mocked to avoid using native library
    internal var wallet = mockk<SDWallet>(relaxed = true) {
        every { mnemonic } returns mnemonics
        every { publicKey } returns fakeKey
    }
    internal var appWallet = spyk(AppWallet(wallet))

    @After
    fun tearDown() {
        securePreferences.clear()

        verify {
            securePreferences.preferences
            securePreferences.clear()
        }
        confirmVerified(securePreferences)
    }

    companion object {
        internal val fakeKey = SimulatedSignatureProvider().publicKey
        // BIP44 tz1NJVmgfWnFaia3UnCFsZezahcf75gAF9iU
        // SD tz1RsT5cZru4Q5iCQ7FdfSrPfuNJF3m6yew6
        val mnemonics: List<String> =
            "zone north mass twist smile bullet afford grab kick spell fresh defy"
                .split(" ")
    }
}
