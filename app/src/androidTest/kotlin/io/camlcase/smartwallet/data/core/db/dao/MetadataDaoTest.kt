/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.core.db.dao

import androidx.test.espresso.matcher.ViewMatchers.assertThat
import io.camlcase.kotlintezos.model.blockchain.BlockchainMetadata
import io.camlcase.kotlintezos.model.blockchain.NetworkConstants
import io.camlcase.smartwallet.data.core.db.MagmaDatabaseTest
import io.camlcase.smartwallet.data.core.db.entity.toEntity
import io.camlcase.smartwallet.data.core.db.entity.toTezosModel
import junit.framework.Assert.assertEquals
import junit.framework.Assert.assertNull
import org.hamcrest.CoreMatchers
import org.junit.Before
import org.junit.Test

class MetadataDaoTest : MagmaDatabaseTest() {
    lateinit var dao: MetadataDao

    @Before
    fun before() {
        dao = db.metadataDao()
    }

    @Test
    @Throws(Exception::class)
    fun writeDataAndRetrieve() {
        val data = BlockchainMetadata(
            "hash", "protocol", "chainId", 1, "manager_key", testNetworkConstants
        )
        dao.insert(data.toEntity())
        val result = dao.querySync()
        assertThat(result?.toTezosModel(1), CoreMatchers.equalTo(data))
    }

    @Test
    fun writeTwice() {
        val data = BlockchainMetadata(
            "hash", "protocol", "chainId", 1, "manager_key", testNetworkConstants
        )
        val input = data.toEntity()
        dao.insert(input)
        dao.insert(input)
        val result = dao.querySync()
        assertEquals(result?.id, 1)
        assertThat(result?.toTezosModel(1), CoreMatchers.equalTo(data))
    }

    @Test
    fun testDelete() {
        writeDataAndRetrieve()
        db.clearAllTables()

        val result = dao.querySync()
        assertNull(result)
    }

    companion object {
        val testNetworkConstants = NetworkConstants(
            "1040000",
            "60000",
            "10400000",
            257,
            "250"
        )
    }
}
