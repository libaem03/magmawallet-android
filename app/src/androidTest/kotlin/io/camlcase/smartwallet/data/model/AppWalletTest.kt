/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.model

import androidx.annotation.RequiresApi
import androidx.test.ext.junit.runners.AndroidJUnit4
import io.camlcase.kotlintezos.model.TezosError
import io.camlcase.kotlintezos.wallet.SDWallet
import org.junit.Assert
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@RequiresApi(26)
class AppWalletTest {
    private fun testInitWallet(): AppWallet {
        val appWallet = AppWallet(bakerWallet1!!)
        Assert.assertNotNull(appWallet)
        assertEquals(bakerWallet1.mnemonic, appWallet.mnemonic)
        return appWallet
    }

    @Test(expected = TezosError::class)
    fun testBadInit() {
        val badWallet = SDWallet(listOf("something", "not", "quite", "right"))
        AppWallet(badWallet!!)
    }

    @Test
    fun testSerializeWallet() {
        val appWallet = testInitWallet()
        val serialize = appWallet.serialize()
        assertEquals(JSON_WALLET, serialize)
    }

    @Test
    fun testDeserialize() {
        val deserialize = AppWallet.deserialize(JSON_WALLET)
        Assert.assertNotNull(deserialize)
        val mnemonic = deserialize!!.mnemonic
        Assert.assertNotNull(mnemonic)
        assertTrue(mnemonic!![0] == "bronze")
        assertTrue(mnemonic[mnemonic.size - 1] == "soon")
    }

    companion object {
        val bakerWallet1 = SDWallet(
            "bronze, post, shove, leader, style, enforce, van, orphan, blossom, shiver, year, soon".split(", ")
        )

        const val JSON_WALLET =
            "{\"mnemonic\":\"bronze, post, shove, leader, style, enforce, van, orphan, blossom, shiver, year, soon\"}"
    }
}
