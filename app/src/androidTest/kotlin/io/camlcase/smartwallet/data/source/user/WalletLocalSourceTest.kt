/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 */
package io.camlcase.smartwallet.data.source.user

import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SdkSuppress
import io.camlcase.smartwallet.data.model.AppWallet
import io.camlcase.smartwallet.data.source.local.SecurePreferences
import io.camlcase.smartwallet.data.source.local.UserPreferences
import io.mockk.every
import io.mockk.mockkObject
import io.mockk.verify
import io.mockk.verifySequence
import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith

/**
 * Run with API 29: https://stackoverflow.com/questions/53391611/mockk-missing-calls-inside-every-block
 */
@RunWith(AndroidJUnit4::class)
@SdkSuppress(minSdkVersion = 29)
class WalletLocalSourceTest : WalletLocalSourceBaseTest() {

    @Test
    fun testInit() {
        assertNotNull(securePreferences)
        assertNotNull(flagPreferences)
        assertNotNull(initLocalSource())
    }

    @Test
    fun testSaveWallet() {
        initLocalSource().storeWallet(appWallet)

        verify {
            securePreferences.store(SecurePreferences.KEY_WALLET, any())
            securePreferences.store(SecurePreferences.KEY_PUBLIC_KEY, fakeKey.bytes)
            flagPreferences.store(UserPreferences.WALLET_PUBLIC_ADDRESS, any())
        }
    }

    @Test
    fun testGetEmptyWallet() {
        val storedWallet = initLocalSource().getWallet()
        assertNull(storedWallet)

        verifySequence {
            securePreferences.getString(SecurePreferences.KEY_WALLET)
            securePreferences.preferences
        }
    }

    @Test
    fun testSaveAndGetWallet() {
        mockkObject(AppWallet)
        every { AppWallet.deserialize(any()) } returns appWallet

        val localSource = initLocalSource()
        localSource.storeWallet(appWallet)
        val storedWallet = localSource.getWallet()
        assertNotNull(storedWallet)
        assertEquals(mnemonics, storedWallet!!.mnemonic)

        verify {
            wallet.mainAddress // Instantiation
            // Save wallet
            wallet.mnemonic
            securePreferences.store(SecurePreferences.KEY_WALLET, any())
            securePreferences.store(SecurePreferences.KEY_PUBLIC_KEY, fakeKey.bytes)
            flagPreferences.store(UserPreferences.WALLET_PUBLIC_ADDRESS, any())
            securePreferences.preferences
            // Get wallet
            securePreferences.getString(SecurePreferences.KEY_WALLET)
            securePreferences.preferences
            AppWallet.deserialize(any())
//            wallet.mnemonic // For the assert
        }
    }

}
