/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.source.user

import io.camlcase.kotlintezos.wallet.SDWallet
import io.camlcase.kotlintezos.wallet.SimulatedSignatureProvider
import io.camlcase.smartwallet.data.model.AppWallet
import io.camlcase.smartwallet.data.source.local.SecurePreferences
import io.camlcase.smartwallet.data.source.local.UserPreferences
import io.mockk.every
import io.mockk.mockk
import io.mockk.spyk
import io.mockk.verify
import org.junit.Assert
import org.junit.Test

class MigrationWalletTest : WalletLocalSourceBaseTest() {
    var migrationWallet = mockk<SDWallet>(relaxed = true) {
        every { mnemonic } returns migrationMnemonics
        every { publicKey } returns migrationKey
    }
    val migrationAppWallet = spyk(AppWallet(migrationWallet))

    @Test
    fun test_Init_Swap_MigrationWallet() {
        val initLocalSource = initLocalSource()
        initLocalSource.storeWallet(appWallet)
        initLocalSource.storeMigrationWallet(migrationAppWallet)

        var initialWallet = initLocalSource.getWallet()
        Assert.assertNotNull(initialWallet)
        Assert.assertTrue(initialWallet!!.mnemonic == mnemonics)

        var newWallet = initLocalSource.getMigrationWallet()
        Assert.assertNotNull(newWallet)
        Assert.assertTrue(newWallet!!.mnemonic == migrationMnemonics)

        // SWAP
        initLocalSource.swapMigrationWalletToStandardWallet()

        initialWallet = initLocalSource.getWallet()
        Assert.assertNotNull(initialWallet)
        Assert.assertTrue(initialWallet!!.mnemonic == migrationMnemonics)

        newWallet = initLocalSource.getMigrationWallet()
        Assert.assertNull(newWallet)


        verify {
            securePreferences.store(SecurePreferences.KEY_WALLET, any())
            securePreferences.store(SecurePreferences.KEY_PUBLIC_KEY, fakeKey.bytes)
            flagPreferences.store(UserPreferences.WALLET_PUBLIC_ADDRESS, any())

            securePreferences.getString(SecurePreferences.KEY_WALLET)
            securePreferences.preferences

            securePreferences.store(SecurePreferences.KEY_WALLET_MIGRATION, any())

            securePreferences.getString(SecurePreferences.KEY_WALLET_MIGRATION)
            securePreferences.preferences

            // SWAP
            securePreferences.getString(SecurePreferences.KEY_WALLET_MIGRATION)
            // Store
            securePreferences.store(SecurePreferences.KEY_WALLET, any())
            securePreferences.store(SecurePreferences.KEY_PUBLIC_KEY, any())
            flagPreferences.store(UserPreferences.WALLET_PUBLIC_ADDRESS, any())

            securePreferences.getString(SecurePreferences.KEY_WALLET)
            securePreferences.getString(SecurePreferences.KEY_WALLET_MIGRATION)
        }
    }

    companion object {
        const val migrationAddress = "tz1_migrate"
        internal val migrationKey = SimulatedSignatureProvider().publicKey
        val migrationMnemonics: List<String> =
            "fetch casino jewel track occur planet lesson wait embark people hurdle canal"
                .split(" ")
    }
}
