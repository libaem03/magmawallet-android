/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 */
package io.camlcase.smartwallet.data

import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import io.camlcase.smartwallet.data.core.AppContext
import io.camlcase.smartwallet.data.source.local.Preferences
import io.camlcase.smartwallet.data.source.local.SecurePreferences
import io.mockk.impl.annotations.SpyK
import io.mockk.spyk
import junit.framework.TestCase.*
import org.junit.After
import org.junit.Test
import org.junit.runner.RunWith
import java.io.File
import java.lang.ref.WeakReference

/**
 * Instrumentation since it needs Context
 */
@RunWith(AndroidJUnit4::class)
class SecurePreferencesTest {
    private val context = InstrumentationRegistry.getInstrumentation().targetContext
    private var appContext: AppContext = WeakReference(context)
    @SpyK
    private var preferences: Preferences = spyk(SecurePreferences(appContext,
        TEST_FILENAME
    ))

    @Test
    fun testInitSecurePreferences() {
        assertNotNull(preferences)
        assertNotNull(preferences.preferences)

        val preferencesFile = File("${context.applicationInfo.dataDir}/shared_prefs/$TEST_FILENAME.xml")
        assertTrue(preferencesFile.exists())
        assertNotNull(preferencesFile.readText())
    }

    @Test
    fun testSaveAndRetrieveString() {
        val key = "TestString"
        val data = "SomethingToStore"

        var result = preferences.getString(key)
        assertNull(result)

        preferences.store(key, data)

        result = preferences.getString(key)
        assertNotNull(result)
        assertEquals(data, result)
    }

    @Test
    fun testSaveAndRetrieveBoolean() {
        val key = "TestBoolean"
        val data = true

        var result = preferences.getBoolean(key)
        assertFalse(result)

        preferences.store(key, data)

        result = preferences.getBoolean(key)
        assertEquals(data, result)
    }

    @Test
    fun testSaveAndRetrieveInt() {
        val key = "TestInt"
        val data = 1234

        var result = preferences.getInt(key)
        assertEquals(-1, result)

        preferences.store(key, data)

        result = preferences.getInt(key)
        assertEquals(data, result)
    }

    @Test
    fun testSaveAndRetrieveLong() {
        val key = "TestLong"
        val data = 1234L

        var result = preferences.getLong(key)
        assertEquals(-1, result)

        preferences.store(key, data)

        result = preferences.getLong(key)
        assertEquals(data, result)
    }

    @Test
    fun testSaveAndRetrieveFloat() {
        val key = "TestFloat"
        val data = 123.4f

        var result = preferences.getFloat(key)
        assertEquals(-1f, result)

        preferences.store(key, data)

        result = preferences.getFloat(key)
        assertEquals(data, result)
    }

    @After
    fun tearDown() {
        preferences.clear()
    }

    companion object {
        const val TEST_FILENAME = "test_encrypted_preferences"
    }
}
