/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import io.camlcase.smartwallet.test.buttonInteraction
import io.camlcase.smartwallet.test.resources
import io.camlcase.smartwallet.ui.base.info.InfoType
import org.hamcrest.CoreMatchers.*

interface WaitForOperationTest {
    fun waitForSendOperation(
        amount: String
    ) {
        onView(withId(R.id.progress_title))
            .check(matches(isDisplayed()))
            .check(matches(withText(containsString(amount))))

        buttonInteraction(R.id.action_progress_main)
            .check(matches(isDisplayed()))
            .check(matches(isEnabled()))

        onView(withId(R.id.action_progress_secondary))
            .check(matches(not(isDisplayed())))

        Thread.sleep(60 * 1000) // Wait for TzKt
        // SUCCESS SCREEN

        onView(withId(R.id.title))
            .check(matches(isDisplayed()))
            .check(matches(withText(InfoType.SEND_INCLUSION_SUCCESS.title!!)))

        buttonInteraction(R.id.action_primary)
            .check(matches(isDisplayed()))
            .check(matches(isEnabled()))
            .check(matches(withText(InfoType.SEND_INCLUSION_SUCCESS.actionText!!)))
            .perform(ViewActions.click())
    }

    fun waitForDelegateOperation() {
        onView(withId(R.id.progress_title))
            .check(matches(isDisplayed()))
            .check(matches(withText(resources.getString(R.string.del_wait_title))))

        buttonInteraction(R.id.action_progress_main)
            .check(matches(isDisplayed()))
            .check(matches(isEnabled()))
            .check(matches(withText(resources.getString(R.string.action_return_to_settings))))

        onView(withId(R.id.action_progress_secondary))
            .check(matches(not(isDisplayed())))

        Thread.sleep(60 * 1000) // Wait for TzKt
        // SUCCESS SCREEN

        onView(withId(R.id.title))
            .check(matches(isDisplayed()))
            .check(matches(withText(InfoType.DELEGATE_INCLUSION_SUCCESS.title!!)))

        buttonInteraction(R.id.action_primary)
            .check(matches(isDisplayed()))
            .check(matches(isEnabled()))
            .check(matches(withText(InfoType.DELEGATE_INCLUSION_SUCCESS.actionText!!)))
            .perform(ViewActions.click())
    }

    fun waitForExchangeOperation(
        fromToken: String,
        toToken: String
    ) {
        onView(withId(R.id.progress_title))
            .check(matches(isDisplayed()))
            .check(matches(withText(resources.getString(R.string.exc_progress_title, fromToken, toToken))))

        buttonInteraction(R.id.action_progress_main)
            .check(matches(isDisplayed()))
            .check(matches(isEnabled()))
            .check(matches(withText(R.string.exc_progress_action)))

        onView(withId(R.id.action_progress_secondary))
            .check(matches(isDisplayed()))
            .check(matches(withText(R.string.exc_progress_secondary_action)))

        Thread.sleep(60 * 1000) // Wait for TzKt
        // SUCCESS SCREEN

        onView(
            allOf(
                withId(R.id.title),
                isDisplayed()
            )
        )
            .check(matches(isDisplayed()))
            .check(matches(withText(InfoType.EXCHANGE_INCLUSION_SUCCESS.title!!)))

        buttonInteraction(R.id.action_primary)
            .check(matches(isDisplayed()))
            .check(matches(isEnabled()))
            .check(matches(withText(InfoType.EXCHANGE_INCLUSION_SUCCESS.actionText!!)))
            .perform(ViewActions.click())
    }
}
