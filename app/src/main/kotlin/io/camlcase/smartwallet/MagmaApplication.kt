/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet

import android.app.Application
import androidx.lifecycle.ProcessLifecycleOwner
import io.camlcase.smartwallet.data.analytic.AnalyticsTracker
import io.camlcase.smartwallet.data.core.Logger
import io.camlcase.smartwallet.data.core.db.MagmaDatabase
import io.camlcase.smartwallet.data.core.extension.tezos.isMainnet
import io.camlcase.smartwallet.data.core.toAppContext
import io.camlcase.smartwallet.data.di.AndroidKotlinTezosSourceModule
import io.camlcase.smartwallet.data.di.DataSourceModule
import io.camlcase.smartwallet.data.di.KotlinTezosSourceModule
import io.camlcase.smartwallet.data.usecase.operation.DexterCalculationsUseCase
import io.camlcase.smartwallet.di.ApplicationComponent
import io.camlcase.smartwallet.di.ApplicationModule
import io.camlcase.smartwallet.di.DaggerApplicationComponent
import io.camlcase.smartwallet.ui.base.ApplicationObserver
import io.camlcase.smartwallet.ui.base.navigate.background.AppBackgroundDelegate
import io.reactivex.rxjava3.exceptions.UndeliverableException
import io.reactivex.rxjava3.plugins.RxJavaPlugins
import javax.inject.Inject

/**
 * @test [TestMagmaApplication]
 */
open class MagmaApplication : Application() {
    private lateinit var database: MagmaDatabase

    @Inject
    lateinit var backgroundDelegate: AppBackgroundDelegate
    @Inject
    lateinit var dexterCalculations: DexterCalculationsUseCase
    @Inject
    lateinit var analyticsTracker: AnalyticsTracker

    val component: ApplicationComponent by lazy {
        initDagger()
    }

    open fun initDagger(): ApplicationComponent {
        database = MagmaDatabase.invoke(this.toAppContext())
        return DaggerApplicationComponent.builder()
            .applicationModule(ApplicationModule(this))
            .kotlinTezosSourceModule(
                KotlinTezosSourceModule(
                    debug = BuildConfig.DEBUG,
                    flavor = BuildConfig.FLAVOR,
                )
            )
            .androidKotlinTezosSourceModule(
                AndroidKotlinTezosSourceModule(this)
            )
            .dataSourceModule(
                DataSourceModule(
                    debug = BuildConfig.DEBUG,
                    isMainnet = BuildConfig.FLAVOR.isMainnet(),
                    database = database,
                    moonPayApiKey = BuildConfig.MOONPAY_API_KEY,
                    moonPayApiUrl = BuildConfig.MOONPAY_API_URL,
                    versionCode = BuildConfig.VERSION_CODE,
                    sentryApiKey = BuildConfig.SENTRY_API_KEY,
                )
            )
            .build()
    }

    override fun onCreate() {
        super.onCreate()
        component.inject(this)
        initApplicationObserver()
        setRxErrorHandler()
    }

    /**
     * One important design requirement for RxJava 2.x is that no Throwable errors should be swallowed.
     * This means errors that can't be emitted because the downstream's lifecycle already reached its terminal state
     * or the downstream cancelled a sequence which was about to emit an error.
     *
     * [See docs](https://github.com/ReactiveX/RxJava/wiki/What's-different-in-2.0#error-handling)
     */
    private fun setRxErrorHandler() {
        RxJavaPlugins.setErrorHandler {
            // The exception could not be delivered to the consumer because it has already canceled/disposed
            // the flow or the exception has nowhere to go to begin with.
            // We don't make the app crash, we report the error
            if (it is UndeliverableException) {
//                it.report()
                Logger.e("UNDELIVERABLE ERROR: ${it.cause}")
                return@setErrorHandler
            }
        }
    }

    private fun initApplicationObserver() {
        ProcessLifecycleOwner
            .get()
            .lifecycle
            .addObserver(ApplicationObserver(backgroundDelegate, dexterCalculations, analyticsTracker))
    }
}
