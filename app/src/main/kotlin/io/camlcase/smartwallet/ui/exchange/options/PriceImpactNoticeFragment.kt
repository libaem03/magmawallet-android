/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.exchange.options

import android.content.Context
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import androidx.coordinatorlayout.widget.CoordinatorLayout
import by.kirich1409.viewbindingdelegate.viewBinding
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.core.extension.*
import io.camlcase.smartwallet.databinding.FragmentPriceImpactWarningBinding
import io.camlcase.smartwallet.model.operation.PriceImpactInfo
import io.camlcase.smartwallet.ui.base.BindBaseFragment
import io.camlcase.smartwallet.ui.custom.AdvancedScrollOffsetListener
import io.camlcase.smartwallet.ui.custom.ScrollOffsetListener

/**
 * Warning screen
 */
class PriceImpactNoticeFragment : BindBaseFragment(R.layout.fragment_price_impact_warning) {
    interface SlippageWarningListener {
        fun onSlippageWarningContinue()
        fun onSlippageWarningBacktrack()
    }

    private val binding by viewBinding(FragmentPriceImpactWarningBinding::bind)
    private var priceImpact: Double? = null
    private var info: PriceImpactInfo = PriceImpactInfo.NOTICE
    private var listener: SlippageWarningListener? = null

    private fun manageIntent(bundle: Bundle?) {
        bundle?.getDouble(ARG_PRICE_IMPACT)?.let {
            priceImpact = it
        } ?: throw IllegalStateException("Screen needs an ARG_PRICE_IMPACT argument")

        bundle.getString(ARG_INFO)?.let {
            info = PriceImpactInfo.valueOf(it)
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        manageIntent(arguments)
        listener = context as? SlippageWarningListener
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initAppbar()
        populateSections(priceImpact!!)
        binding.info.setText(info.info)
        bindViews()
    }

    private fun bindViews() {
        binding.actionNext.throttledClick()
            .subscribe { listener?.onSlippageWarningContinue() }
            .addToDisposables(disposables)

        binding.actionSecondary.throttledClick()
            .subscribe { listener?.onSlippageWarningBacktrack() }
            .addToDisposables(disposables)
    }

    private fun populateSections(priceImpact: Double) {
        val priceImpactFormat = priceImpact.formatPercentage() + "%"
        binding.priceImpactSection.setValues(priceImpactFormat)
    }

    private fun initAppbar() {
        setupToolbar(binding.appBarContainer.toolbar, true)

        val showWarning = info == PriceImpactInfo.BIG_WARNING
        val title = getString(info.title)
        binding.appBarContainer.expandedTitle.text = title

        if (showWarning) {
            binding.appBarContainer.icWarning.show()
            val param = CoordinatorLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                resources.getDimension(R.dimen.exchange_price_impact_warning_bar_height).toInt()
            )
            binding.appBarContainer.appBar.layoutParams = param
            binding.appBarContainer.appBar.addOnOffsetChangedListener(
                AdvancedScrollOffsetListener { _, state ->
                    when (state) {
                        AdvancedScrollOffsetListener.State.EXPANDED -> {
                            binding.appBarContainer.expandedTitle.show()
                            binding.appBarContainer.icWarning.show()
                            binding.appBarContainer.toolbar.title = null
                        }
                        AdvancedScrollOffsetListener.State.COLLAPSED -> {
                            binding.appBarContainer.expandedTitle.hide()
                            binding.appBarContainer.icWarning.hide()
                            binding.appBarContainer.toolbar.title = title
                        }
                        AdvancedScrollOffsetListener.State.IDLE -> {
                        }
                    }
                }
            )
        } else {
            binding.appBarContainer.appBar.addOnOffsetChangedListener(
                ScrollOffsetListener(
                    binding.appBarContainer.expandedTitle,
                    binding.appBarContainer.toolbar,
                    title
                )
            )
        }
    }

    companion object {
        private const val ARG_PRICE_IMPACT = "args.price_impact"
        private const val ARG_INFO = "args.price_impact.info"

        fun init(priceImpact: Double, info: PriceImpactInfo): PriceImpactNoticeFragment {
            val fragment = PriceImpactNoticeFragment()
            val args = Bundle()
            args.putDouble(ARG_PRICE_IMPACT, priceImpact)
            args.putString(ARG_INFO, info.name)
            fragment.arguments = args
            return fragment
        }
    }
}
