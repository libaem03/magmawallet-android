/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.main.balance

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.LinearLayoutManager
import by.kirich1409.viewbindingdelegate.viewBinding
import io.camlcase.kotlintezos.model.Tez
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.core.extension.*
import io.camlcase.smartwallet.data.core.XTZ
import io.camlcase.smartwallet.data.core.extension.format
import io.camlcase.smartwallet.data.core.extension.formatAsMoney
import io.camlcase.smartwallet.data.model.Token
import io.camlcase.smartwallet.data.model.amount.CoinBalance
import io.camlcase.smartwallet.data.model.exchange.CurrencyBundle
import io.camlcase.smartwallet.data.model.exchange.TokenInfoBundle
import io.camlcase.smartwallet.databinding.FragmentBalancesBinding
import io.camlcase.smartwallet.model.DataWrapper
import io.camlcase.smartwallet.model.handleResults
import io.camlcase.smartwallet.model.operation.OperationForList
import io.camlcase.smartwallet.model.operation.OperationResultBundle
import io.camlcase.smartwallet.model.token.BalancesForView
import io.camlcase.smartwallet.ui.base.*
import io.camlcase.smartwallet.ui.base.navigate.AppNavigator
import io.camlcase.smartwallet.ui.base.navigate.RequestCode
import io.camlcase.smartwallet.ui.base.navigate.navigateToModal
import io.camlcase.smartwallet.ui.delegate.IncludedOperation
import io.camlcase.smartwallet.ui.exchange.amount.MainToBalance
import io.camlcase.smartwallet.ui.main.activity.OperationListViewModel
import io.camlcase.smartwallet.ui.main.token.TokenListViewModel
import io.camlcase.smartwallet.ui.migration.ShouldMigrateViewModel
import io.camlcase.smartwallet.ui.money.BuyTezFragment
import io.camlcase.smartwallet.ui.money.BuyTezViewModel
import io.camlcase.smartwallet.ui.operation.TokenListAdapter
import io.camlcase.smartwallet.ui.operation.WaitForInclusionActivity
import io.camlcase.smartwallet.ui.permissions.ReadContactsPermission
import io.camlcase.smartwallet.ui.receive.ReceiveFragment
import io.camlcase.smartwallet.ui.security.system.SystemSafetyChecker
import io.reactivex.rxjava3.disposables.CompositeDisposable
import java.lang.ref.WeakReference
import java.math.BigDecimal

/**
 * Screen for Token Balances
 *
 * Fetches operations needed for [TokenDetailFragment] and [OperationListFragment]
 */
class BalancesFragment : BindBaseFragment(R.layout.fragment_balances), MainToBalance {
    interface BalanceListener {
        fun openTokenDetail(token: TokenInfoBundle, currencyBundle: CurrencyBundle)
    }

    private val binding by viewBinding(FragmentBalancesBinding::bind)
    private var clickDisposable: CompositeDisposable = CompositeDisposable()

    private lateinit var viewModel: TokenListViewModel
    private lateinit var operationsViewModel: OperationListViewModel
    private lateinit var migrationViewModel: ShouldMigrateViewModel
    private lateinit var buyViewModel: BuyTezViewModel
    private var listener: BalanceListener? = null
    private var migrationDialog: DialogFragment? = null

    /**
     * Marks the first part of an operation is finished (E.g: a send has been sent to the chain).
     * Linked with the [RequestCode.WAIT_FOR_INJECTION] binding we can time when to refresh.
     *
     * If a user leaves from an operation before finishing injection, we can trigger a refresh after Auth login is
     * finished.
     *
     * @see bindScreenResults
     */
    private var operationFinished: Boolean = false

    /**
     * Two main processes we should be showing feedback for: balance and operations.
     * We run them at the same time but they can finish at different. Use the flag to ensure we still show loading for
     * both.
     * first = loadingBalance
     * second = loadingOperations
     *
     * See [hideBalanceLoading] & [hideOperationLoading]
     */
    private var loadingStatus = Pair(first = false, second = false)

    /**
     * Balances is the first screen that is shown on startup after auth login.
     * It's important to only show the user this warning when it's already logged in or a malicious party could cancel
     * a potential update.
     *
     * We don't use MainActivity as it's the parent for AuthLoginActivity and would show the dialogs there.
     */
    private lateinit var safetyChecker: SystemSafetyChecker

    override fun onAttach(context: Context) {
        super.onAttach(context)
        listener = context as? BalanceListener
        safetyChecker = object : SystemSafetyChecker() {
            override var globalActivityRef: WeakReference<BaseActivity> = WeakReference(context as BaseActivity)
            override var disposablesRef: WeakReference<CompositeDisposable> = WeakReference(disposables)
        }
        safetyChecker.init()
        safetyChecker.run()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bindViewModel()
        checkMigrationProcess()
        initList()
        initPullToRefresh()
        bindViews()
        bindScreenResults()
        refresh()
        checkBuyButton()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        if (migrationDialog?.isVisible == true) {
            migrationDialog?.dismissAllowingStateLoss()
            migrationDialog = null
        }
        outState.putBoolean(ARG_OPERATION_FINISHED_FLAG, operationFinished)
        super.onSaveInstanceState(outState)
    }

    override fun onResume() {
        super.onResume()
        safetyChecker.onResume()
    }

    private fun bindViewModel() {
        // Share view model with other screens
        baseActivity?.apply {
            buyViewModel = viewModel(viewModelFactory) {}
            viewModel = this.viewModel(viewModelFactory) {
                observe(balances, { handleTokens(it) })
                observe(totalBalance, { handleTotalBalanceInCurrency(it.first, it.second) })
            }

            operationsViewModel = viewModel(viewModelFactory) {
                observe(data, { handleOperationsStatus(it) })
            }
        }
        migrationViewModel = viewModel(viewModelFactory) {}
    }

    /**
     * For updated/imported users, we'll need to navigate them directly to the Migration screen
     */
    private fun checkMigrationProcess() {
        if (migrationViewModel.shouldMigrate()) {
            migrationDialog = AppNavigator.navigateToMigrateWarning(activity as? AppCompatActivity)
        }
    }

    /**
     * Operations are not needed to interact with the screen but we should show some feedback to the user so they
     * know tokens are not clickable yet.
     */
    private fun handleOperationsStatus(wrapper: DataWrapper<List<OperationForList>>) {
        wrapper.handleResults(
            onValidData = {
                hideOperationLoading()
            },
            onError = {
                hideOperationLoading()
                showError(it)
            },
            onEmpty = {
                hideOperationLoading()
            },
            onLoading = {
                loadingStatus = Pair(loadingStatus.first, true)
                // Don't deactivate send button, that's a balance prerogative
                showRefreshLoading()
            }
        )
    }

    private fun hideOperationLoading() {
        loadingStatus = Pair(loadingStatus.first, false)
        hideLoading()
    }

    private fun hideBalanceLoading() {
        loadingStatus = Pair(false, loadingStatus.second)
        hideLoading()
    }

    private fun bindScreenResults() {
        activityResult.get().bind(
            listOf(
                RequestCode.DELEGATE_BAKER,
                RequestCode.SEND,
                RequestCode.EXCHANGE
            ),
            disposable = disposables,
            onResult = { result ->
                result.data?.getParcelableExtra<OperationResultBundle>(WaitForInclusionActivity.ARG_OPERATION_BUNDLE)
                    ?.let {
                        operationFinished = true
                        AppNavigator.navigateToWaitForInclusionActivity(activity, it)
                    } ?: refresh()
            },
            onError = { showError(it) })

        activityResult.get().bind(
            RequestCode.WAIT_FOR_INJECTION,
            disposable = disposables,
            onResult = { result ->
                result.data?.getStringExtra(WaitForInclusionActivity.ARG_OPERATION_TYPE)?.let {
                    when (IncludedOperation.valueOf(it)) {
                        IncludedOperation.SEND -> navigateToSend()
                        IncludedOperation.DELEGATE,
                        IncludedOperation.UNDELEGATE -> AppNavigator.navigateToDelegate(activity)
                        else -> {
                        }
                    }
                }
                operationFinished = false
                refresh()
            },
            onError = { showError(it) })

        activityResult.get().bind(
            RequestCode.AUTH_LOGIN,
            disposable = disposables,
            onResult = {
                if (operationFinished) {
                    refresh()
                    operationFinished = false
                }
            },
            onError = { showError(it) })

        activityResult.get().bind(
            RequestCode.LOCAL_CURRENCIES,
            disposable = disposables,
            onResult = {
                // No need to forceRefresh FA1.2 balances
                refresh()
            },
            onError = { showError(it) })

        activityResult.get().bind(
            RequestCode.MIGRATE_WALLET,
            disposable = disposables,
            onResult = {
                handleTotalBalanceInCurrency(BigDecimal.ZERO, viewModel.fetchCurrencyBundle())
                binding.balanceXtzContainer.hide()
                binding.list.hide()
                refresh()
            },
            onError = { showError(it) })
    }

    private fun initList() {
        binding.list.apply {
            layoutManager = LinearLayoutManager(activity)
            setHasFixedSize(true)
        }
    }

    private fun initPullToRefresh() {
        binding.swipeRefresh.setOnRefreshListener {
            refresh()
        }
    }

    private fun showRefreshLoading() {
        if (!binding.swipeRefresh.isRefreshing) {
            binding.swipeRefresh.isRefreshing = true
        }
    }

    private fun showLoading() {
        showRefreshLoading()
        binding.actionSend.isEnabled = false
    }

    private fun hideLoading() {
        if (loadingStatus == Pair(first = false, second = false)) {
            if (binding.swipeRefresh.isRefreshing) {
                binding.swipeRefresh.isRefreshing = false
            }
        }
    }

    /**
     * Loading discard is handled in [handleOperationsStatus] when [operationsViewModel] has finished fetching data.
     */
    private fun handleTokens(wrapper: DataWrapper<BalancesForView>) {
        wrapper.handleResults(
            onValidData = {
                // Called from an Activity's view model, view could be null
                if (view != null) {
                    hideBalanceLoading()
                    binding.balanceXtzContainer.show()
                    binding.balanceXtz.tokenBalance.text = (it.xtz.balance as CoinBalance).value.format(null)
                    binding.balanceXtz.tokenSymbol.text = XTZ.symbol
                    binding.balanceXtz.tokenBalanceCurrency.show()
                    binding.balanceXtz.tokenBalanceCurrency.text = it.xtz.formattedCurrencyBalance
                    binding.balanceXtz.currencyExchange.text = it.xtz.exchange
                    binding.balanceXtz.currencyExchange.show()
                    binding.actionSend.isEnabled = it.canSend

                    if (it.faTokens.isEmpty()) {
                        emptyStatusVisibility(it.xtz.balance.compareTo(Tez.zero) == 0)
                        binding.list.hide()
                    } else {
                        binding.emptyStatus.hide()
                        binding.list.show()
                        // Avoid queueing subscriptions every time the adapter is recreated on refresh
                        clickDisposable.clear()
                        clickDisposable = CompositeDisposable()
                        // Populate with everything except XTZ
                        binding.list.adapter = TokenListAdapter(it.faTokens) { tokenForView, view ->
                            view.throttledClick()
                                .subscribe { onTokenSelected(tokenForView.token) }
                                .addToDisposables(clickDisposable)
                        }
                    }
                    // On first load after onboarding, data won't be saved and button would be hidden.
                    if (!binding.actionBuy.isVisible()) {
                        checkBuyButton()
                    }
                }
            },
            onError = {
                if (view != null) {
                    hideBalanceLoading()
                    it?.apply {
                        showSnackbar(viewModel.errorMessageParser.parseMessage(this))
                    }
                }
            },
            onLoading = {
                if (view != null) {
                    loadingStatus = Pair(true, loadingStatus.second)
                    showLoading()
                }
            }
        )
    }

    private fun emptyStatusVisibility(conditionToShow: Boolean) {
        if (conditionToShow) {
            // Show "Your wallet is empty" only when all tokens == 0
            binding.emptyStatus.show()
        } else {
            binding.emptyStatus.hide()
        }
    }

    private fun handleTotalBalanceInCurrency(totalBalance: BigDecimal, bundle: CurrencyBundle) {
        if (!isRemoving) {
            binding.totalBalanceCurrencyInfo.text = getString(R.string.wlt_total_currency_value, bundle.code)
            binding.totalBalanceCurrencyInfo.show()
            binding.totalBalanceCurrency.text = totalBalance.formatAsMoney(bundle)
        }
    }

    private fun onTokenSelected(token: Token) {
        viewModel.getTokenInfoBundle(token)?.apply {
            listener?.openTokenDetail(this, viewModel.currencyBundle)
        }
    }

    private fun bindViews() {
        binding.balanceXtzContainer.throttledClick()
            .subscribe { onTokenSelected(XTZ) }
            .addToDisposables(disposables)

        binding.actionReceive.throttledClick()
            .subscribe {
                navigateToModal(ReceiveFragment.init(), RequestCode.RECEIVE)
            }
            .addToDisposables(disposables)

        binding.actionSend.throttledClick()
            .subscribe { navigateToSend() }
            .addToDisposables(disposables)

        binding.actionBuy.throttledClick()
            .subscribe {
                navigateToModal(BuyTezFragment.init(), RequestCode.BUY_TEZ)
            }
            .addToDisposables(disposables)
    }

    private fun checkBuyButton() {
        if (buyViewModel.showBuyOption(context.getDeviceCountryCode()) /*|| BuildConfig.DEBUG*/) {
            binding.actionBuy.show()
        }
    }

    override fun refresh() {
        viewModel.getTokens()
        operationsViewModel.getOperations(ReadContactsPermission.hasPermission(context))
    }

    private fun navigateToSend() {
        AppNavigator.navigateToSend(activity, viewModel.getSendSource(), viewModel.getSendToken())
    }

    companion object {

        private const val ARG_OPERATION_FINISHED_FLAG = "args.operation.finished"
    }
}
