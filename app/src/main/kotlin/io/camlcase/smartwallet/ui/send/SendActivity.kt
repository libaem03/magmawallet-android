/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.send

import android.content.Context
import android.content.Intent
import android.os.Bundle
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.core.extension.*
import io.camlcase.smartwallet.data.core.Logger
import io.camlcase.smartwallet.data.core.XTZ
import io.camlcase.smartwallet.data.model.amount.Balance
import io.camlcase.smartwallet.data.model.amount.EmptyBalance
import io.camlcase.smartwallet.data.model.contact.MagmaContact
import io.camlcase.smartwallet.data.model.exchange.TokenInfoBundle
import io.camlcase.smartwallet.data.model.send.SendRequest
import io.camlcase.smartwallet.model.navigation.SendNavigation
import io.camlcase.smartwallet.model.operation.OperationFeesBundle
import io.camlcase.smartwallet.model.operation.finishWithBundleResult
import io.camlcase.smartwallet.ui.base.BaseActivity
import io.camlcase.smartwallet.ui.base.ItemSelector
import io.camlcase.smartwallet.ui.base.info.InfoType
import io.camlcase.smartwallet.ui.base.info.InfoView
import io.camlcase.smartwallet.ui.base.navigate.AppNavigator
import io.camlcase.smartwallet.ui.delegate.IncludedOperation
import io.camlcase.smartwallet.ui.operation.WaitForInclusionFragment
import io.camlcase.smartwallet.ui.operation.fees.FeesDetailFragment
import io.camlcase.smartwallet.ui.qr.QRScanType
import io.camlcase.smartwallet.ui.send.confirmation.SendConfirmationFragment
import io.camlcase.smartwallet.ui.send.token.SendChooseTokenFragment
import kotlinx.android.synthetic.main.activity_fragment.*

/**
 * Send token amount flow
 *
 * @see SendToFragment
 * @see SendChooseTokenFragment
 * @see SendAmountFragment
 * @see SendConfirmationFragment
 * @see WaitForInclusionFragment
 */
class SendActivity : BaseActivity(R.layout.activity_fragment),
    SendToFragment.AddressSetListener,
    SendAmountFragment.SendAmountListener,
    SendConfirmationFragment.SendConfirmationListener,
    InfoView.InfoEventsListener,
    ItemSelector<TokenInfoBundle> {

    private var sendRequest: SendRequest? = null
    private var info: TokenInfoBundle? = null
    private var contact: MagmaContact? = null

    @SendNavigation.ScreenSource
    private var source: Int = SendNavigation.SOURCE_MAIN

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        listenToBackground()
        if (savedInstanceState == null) {
            manageIntent(intent.extras)
            showInitialStep()
        }
    }

    private fun manageIntent(extras: Bundle?) {
        extras?.let {
            source = it.getInt(ARG_SOURCE)
            it.getParcelable<TokenInfoBundle>(ARG_TOKEN)?.apply {
                info = this
            }
            it.getParcelable<MagmaContact>(ARG_CONTACT)?.apply {
                contact = this
            }
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putParcelable(ARG_SEND_FIELDS, sendRequest)
        outState.putParcelable(ARG_TOKEN, info)
        outState.putParcelable(ARG_CONTACT, contact)
        super.onSaveInstanceState(outState)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        sendRequest = savedInstanceState.getParcelable(ARG_SEND_FIELDS)
        info = savedInstanceState.getParcelable(ARG_TOKEN)
        contact = savedInstanceState.getParcelable(ARG_CONTACT)
        super.onRestoreInstanceState(savedInstanceState)
    }

    private fun showInitialStep() {
        info?.apply {
            sendRequest = SendRequest(EmptyBalance(), "", this.token)
        }
        val address = contact?.address

        if (source == SendNavigation.SOURCE_CONTACT_DETAIL && address != null) {
            onAddressSet(address)
        } else {
            replaceFragmentSafely(SendToFragment(), R.id.container_fragment)
        }
    }

    override fun scanQR() {
        AppNavigator.navigateToScanQR(this, QRScanType.SEND)
    }

    override fun onAddressSet(to: String) {
        sendRequest = SendRequest(EmptyBalance(), to, sendRequest?.token ?: XTZ)

        val fragment = if (info != null) {
            SendAmountFragment.init(info)
        } else {
            SendChooseTokenFragment.init()
        }

        if (source == SendNavigation.SOURCE_CONTACT_DETAIL) {
            replaceFragmentSafely(fragment, R.id.container_fragment)
        } else {
            displayFragment(fragment, R.id.container_fragment)
        }
    }

    override fun onContactSelected(contact: MagmaContact) {
        this.contact = contact
        onAddressSet(contact.address!!)
    }

    override fun onAmountSet(amount: Balance<*>) {
        sendRequest?.let {
            val copy = SendRequest(amount, it.to, it.token)
            sendRequest = copy
            displayFragment(SendConfirmationFragment.init(copy, contact), R.id.container_fragment)
        } ?: throw IllegalStateException("Cannot navigate to confirmation without To and From address")
    }

    override fun navigateToFeesDetail(fees: OperationFeesBundle) {
        displayFragment(FeesDetailFragment.init(fees), R.id.container_fragment)
    }

    override fun onSendOperationFinished(amount: Balance<*>, operationHash: String) {
        sendRequest = sendRequest?.copy(amount)
        val title = getString(R.string.snd_wait_title, amount.format(sendRequest!!.token))
        finishWithBundleResult(
            IncludedOperation.SEND,
            operationHash,
            title
        )
    }

    override fun onBackPressed() {
        container_fragment.hideKeyboard()
        super.onBackPressed()
    }

    override fun onItemSelected(item: TokenInfoBundle) {
        info = item
        sendRequest = SendRequest(EmptyBalance(), sendRequest!!.to, item.token)
        displayFragment(SendAmountFragment.init(info), R.id.container_fragment)
    }

    override fun onActionClick(infoType: InfoType) {
        when (infoType) {
            InfoType.SEND_ERROR_INVALID_ADDRESS,
            InfoType.SEND_GENERAL_ERROR -> {
                // Go back to first screen
                popFullBackstack()
                sendRequest = null
                showInitialStep()
            }
            InfoType.SEND_ERROR_INSUFFICIENT_XTZ,
            InfoType.OPERATION_COUNTER -> {
                finishScreen()
            }
            else -> {
                Logger.e("InfoFragment of type not supported $infoType")
            }
        }
    }

    private fun finishScreen() {
        finishAfterTransition()
    }

    companion object {
        internal const val ARG_SEND_FIELDS = "arg.send.fields"
        private const val ARG_SOURCE = "arg.screen.source"
        private const val ARG_TOKEN = "arg.token"
        internal const val ARG_CONTACT = "arg.contact"

        fun init(
            context: Context,
            @SendNavigation.ScreenSource source: Int = SendNavigation.SOURCE_MAIN,
            tokenInfo: TokenInfoBundle? = null,
            contact: MagmaContact? = null
        ): Intent {
            val intent = Intent(context, SendActivity::class.java)
            intent.putExtra(ARG_SOURCE, source)
            intent.putExtra(ARG_TOKEN, tokenInfo)
            intent.putExtra(ARG_CONTACT, contact)
            return intent
        }
    }
}
