/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.qr

import android.content.Context
import android.util.AttributeSet
import com.google.zxing.BarcodeFormat
import com.google.zxing.ResultPoint
import com.journeyapps.barcodescanner.BarcodeCallback
import com.journeyapps.barcodescanner.BarcodeResult
import com.journeyapps.barcodescanner.DecoratedBarcodeView
import com.journeyapps.barcodescanner.DefaultDecoderFactory
import io.camlcase.smartwallet.core.extension.hide
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.processors.PublishProcessor

/**
 * Custom view to add to the fragment that sends a [scanResult] when QR code is read
 */
class QRView(context: Context, attrs: AttributeSet) : DecoratedBarcodeView(context, attrs) {
    private val callback = QRReadCallback()
    val scanResult: Flowable<BarcodeResult>
    var paused: Boolean = false

    init {
        decodeContinuous(callback)
        scanResult = callback.customViewfinder.onBackpressureBuffer().filter { !this.paused }
        val formats = listOf(BarcodeFormat.QR_CODE)
        barcodeView.decoderFactory = DefaultDecoderFactory(formats)
        statusView.hide()
    }

    override fun pause() {
        this.paused = true
        super.pause()
    }

    override fun pauseAndWait() {
        this.paused = true
        super.pauseAndWait()
    }

    override fun resume() {
        this.paused = false
        super.resume()
    }
}

internal class QRReadCallback : BarcodeCallback {
    internal val customViewfinder: PublishProcessor<BarcodeResult> = PublishProcessor.create()

    override fun barcodeResult(result: BarcodeResult?) {
        result?.also {
            customViewfinder.onNext(result)
        }
    }

    override fun possibleResultPoints(resultPoints: MutableList<ResultPoint>?) {}
}
