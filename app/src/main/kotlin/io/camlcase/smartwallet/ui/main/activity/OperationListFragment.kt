/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.main.activity

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import by.kirich1409.viewbindingdelegate.viewBinding
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.core.extension.*
import io.camlcase.smartwallet.data.analytic.report
import io.camlcase.smartwallet.data.model.contact.MagmaContact
import io.camlcase.smartwallet.databinding.FragmentOperationListBinding
import io.camlcase.smartwallet.model.DataWrapper
import io.camlcase.smartwallet.model.handleResults
import io.camlcase.smartwallet.model.operation.OperationForList
import io.camlcase.smartwallet.ui.base.*
import io.camlcase.smartwallet.ui.base.navigate.AppNavigator
import io.camlcase.smartwallet.ui.base.navigate.RequestCode
import io.camlcase.smartwallet.ui.exchange.amount.MainToActivity
import io.camlcase.smartwallet.ui.permissions.ReadContactsPermission

/**
 * Activity screen that shows the operations related to the user.
 *
 * @see MainActivity
 */
class OperationListFragment : BindBaseFragment(R.layout.fragment_operation_list), MainToActivity {
    private val binding by viewBinding(FragmentOperationListBinding::bind)
    private lateinit var viewModel: OperationListViewModel
    private var filterContact: MagmaContact? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        manageIntent(arguments)
    }

    private fun manageIntent(bundle: Bundle?) {
        bundle?.getParcelable<MagmaContact>(ARG_CONTACT)?.let {
            filterContact = it
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupToolbar(binding.toolbar, filterContact != null)
        binding.list.apply {
            layoutManager = LinearLayoutManager(activity)
            setHasFixedSize(true)
        }
        initPullToRefresh()
        bindScreenResults()
        bindSharedViewModel()

        if (filterContact != null) {
            binding.toolbarTitle.text = null
            binding.contactOperationTitle.show()
            binding.emptyStatus.setMarginTop(R.dimen.activity_vertical_margin)

            refresh()
            // It's set by default to be shown with the main bottom navigation
            binding.list.setLinearLayoutMargin(0, 0, 0, 0)
        }
    }

    private fun bindScreenResults() {
        activityResult.get()?.bind(
            code = RequestCode.ADD_CONTACT,
            disposable = disposables,
            onResult = { _ -> refreshContacts() },
            onError = { it.report() }
        )

        activityResult.get().bindAny(
            RequestCode.MIGRATE_WALLET,
            disposable = disposables,
            onResult = {
                // Clean slate for operations
                viewModel.clearLocalOperations()
                showEmpty()
            },
            onError = { showError(it) })
    }

    private fun initPullToRefresh() {
        binding.swipeRefresh.setOnRefreshListener {
            refresh()
        }
    }

    /**
     * First screen [BalancesFragment] will already call [viewModel.getTokens] and data will be observed directly on [handleOperations]
     */
    private fun bindSharedViewModel() {
        // Share view model with other screens
        (activity as? BaseActivity)?.apply {
            viewModel = viewModel(viewModelFactory) {
                observe(data, { handleOperations(it) })
            }
        }
    }

    private fun handleOperations(wrapper: DataWrapper<List<OperationForList>>) {
        wrapper.handleResults(
            onValidData = {
                hideLoading()
                populateList(it)
            },
            onError = {
                hideLoading()
                if (!binding.list.isVisible()) {
                    showEmpty()
                }
                showError(it)
            },
            onLoading = {
                showLoading()
            },
            onEmpty = {
                hideLoading()
                showEmpty()
            }
        )
    }

    private fun showLoading() {
        // BalanceFragment could call this from its refresh when this view has been destroyed
        if (view != null) {
            binding.swipeRefresh.isRefreshing = true
        }
    }

    private fun hideLoading() {
        binding.swipeRefresh.isRefreshing = false
    }

    private fun populateList(operations: List<OperationForList>) {
        binding.emptyStatus.hide()
        binding.list.show()
        binding.list.adapter = OperationListAdapter(operations) { operation, view ->
            view.setOnClickListener {
                AppNavigator.openOperationExplorer(activity, operation.hash)
            }
        }
    }

    private fun showEmpty() {
        binding.emptyStatus.show()
        binding.list.hide()
    }

    fun refresh() {
        viewModel.getOperations(filterContact, ReadContactsPermission.hasPermission(context))
    }

    companion object {
        private const val ARG_CONTACT = "args.activity.contact"

        fun init(contact: MagmaContact? = null): OperationListFragment {
            val fragment = OperationListFragment()
            val args = Bundle()
            args.putParcelable(ARG_CONTACT, contact)
            fragment.arguments = args
            return fragment
        }
    }

    override fun refreshContacts() {
        // Fetching operations again is too expensive. Just update the ones locally with the new contacts
        viewModel.getOperations(filterContact, ReadContactsPermission.hasPermission(context), true)
    }
}
