/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.base.navigate

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import io.camlcase.smartwallet.data.model.contact.MagmaContact
import io.camlcase.smartwallet.data.model.exchange.TokenInfoBundle
import io.camlcase.smartwallet.model.navigation.MainSource
import io.camlcase.smartwallet.model.navigation.PinNavigation
import io.camlcase.smartwallet.model.navigation.SendNavigation
import io.camlcase.smartwallet.model.navigation.WelcomeNavigation
import io.camlcase.smartwallet.model.operation.OperationResultBundle
import io.camlcase.smartwallet.ui.LaunchActivity
import io.camlcase.smartwallet.ui.base.BaseFragment
import io.camlcase.smartwallet.ui.base.BindBaseFragment
import io.camlcase.smartwallet.ui.qr.QRScanType

interface Navigator {
    fun navigateToWelcome(
        context: Context?,
        flags: Int? = null,
        @WelcomeNavigation.ScreenSource source: Int = WelcomeNavigation.SOURCE_LAUNCH
    )

    fun navigateToMain(context: Context?, flags: Int? = null, source: MainSource = MainSource.DEFAULT)
    fun navigateToMain(context: Context?, deeplinkUri: String?, source: MainSource = MainSource.DEFAULT)
    fun navigateToAuthLogin(context: Context?)
    fun navigateToLogin(context: Context?)
    fun navigateToOnboarding(context: Context?)
    fun navigateToDelegate(context: Context?)
    fun navigateToSend(
        context: Context?,
        @SendNavigation.ScreenSource source: Int = SendNavigation.SOURCE_MAIN,
        tokenInfo: TokenInfoBundle? = null,
        contact: MagmaContact? = null
    )

    fun navigateToScanQR(context: Context?, type: QRScanType)
    fun navigateToChangePin(
        context: Context?,
        @PinNavigation.ScreenSource source: Int = PinNavigation.SOURCE_SETTINGS
    )

    fun navigateToAddContact(
        context: Context?,
        contact: MagmaContact? = null
    )

    fun navigateToMigrateWallet(context: Context?)

    fun navigateToLocalCurrencies(context: Context?)
    fun navigateToAttributions(context: Context?)
    fun navigateToNetworkConfig(context: Context?)
    fun navigateToContactActivity(context: Context?, contact: MagmaContact)
    fun navigateToWaitForInclusionActivity(context: Context?, bundle: OperationResultBundle)

    fun navigateTo(context: Context?, intentToLaunch: Intent, requestCode: RequestCode, bundle: Bundle? = null) {
        context?.also {
            when (it) {
                is Activity -> startActivityForResult(it, intentToLaunch, requestCode)
                else -> startActivity(it, intentToLaunch, bundle)
            }
        }
    }

    fun startActivity(context: Context?, intentToLaunch: Intent, bundle: Bundle? = null) {
        context?.startActivity(intentToLaunch, bundle)
    }

    fun startActivityForResult(activity: Activity, intent: Intent, requestCode: RequestCode) {
        activity.startActivityForResult(intent, requestCode.code)
    }

    fun startActivityForResult(fragment: Fragment, intent: Intent, requestCode: RequestCode) {
        fragment.startActivityForResult(intent, requestCode.code)
    }

    /**
     * Clear all previous activities and navigate to launcher.
     */
    fun navigateToLaunchActivity(context: Context?) {
        val intent = Intent(context, LaunchActivity::class.java)
        intent.flags = CLEAR_FLAGS
        startActivity(context, intent)
    }

    fun navigateToPermissionSettings(activity: Activity) {
        val myAppSettings = Intent(
            Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
            Uri.parse("package:" + activity.packageName)
        )
        myAppSettings.addCategory(Intent.CATEGORY_DEFAULT)
        myAppSettings.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivityForResult(activity, myAppSettings, RequestCode.SYSTEM_SETTINGS)
    }

    /**
     * Opens an external browser
     */
    fun navigateToUrl(context: Context?, url: String) {
        context?.apply {
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
            browserIntent.addCategory(Intent.CATEGORY_BROWSABLE)
            if (browserIntent.resolveActivity(this.packageManager) != null) {
                startActivity(context, browserIntent)
            }
        }
    }
}

@Deprecated("Use BindBaseFragment and migrate to view binding",
    ReplaceWith("BindBaseFragment?.navigateToModal")
)
fun BaseFragment?.navigateToModal(
    modalFragment: BottomSheetDialogFragment, requestCode: RequestCode = RequestCode.UNKNOWN
) {
    this?.apply {
        parentFragmentManager.navigateToModal(modalFragment, this, requestCode)
    }
}

fun BindBaseFragment?.navigateToModal(
    modalFragment: BottomSheetDialogFragment, requestCode: RequestCode = RequestCode.UNKNOWN
) {
    this?.apply {
        parentFragmentManager.navigateToModal(modalFragment, this, requestCode)
    }
}

fun FragmentManager?.navigateToModal(
    modalFragment: BottomSheetDialogFragment, parent: Fragment, requestCode: RequestCode
) {
    this?.let {
        modalFragment.isCancelable = true
        modalFragment.setTargetFragment(parent, requestCode.code)
        modalFragment.show(it, modalFragment.tag)
    }
}

const val CLEAR_FLAGS =
    Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK

/**
 * Max code 65_535
 * Use day-month-last digit of year
 */
enum class RequestCode(val code: Int) {
    UNKNOWN(0),
    DELEGATE_BAKER(5_02_0),
    WITHDRAWAL_LIMIT(25_02_0),
    SYSTEM_SETTINGS(27_02_0),
    SCAN_QR(28_02_0),
    AUTH_LOGIN(20_04_0),
    RECEIVE(28_04_0),
    EXCHANGE_CHOOSE_TOKEN(6_05_0),
    SEND(4_06_0),
    EXCHANGE(7_05_0),
    EXCHANGE_SLIPPAGE(13_05_0),
    DELEGATE_CHOOSE_BAKER(1_05_0),
    INFO_MODAL(11_06_0),
    ADD_CONTACT(31_08_0),
    MIGRATE_WALLET(20_10_0),
    LOCAL_CURRENCIES(10_12_0),
    BUY_TEZ(9_12_0),
    WAIT_FOR_INJECTION(5_02_1),

    // Android system
    PROVIDER_ERROR_DIALOG_CODE(3_08_0),
    CONTACTS_PICKER(1_09_0),
    ACCOUNTS_PICKER(3_09_0);

    companion object {
        fun get(code: Int): RequestCode {
            return values().firstOrNull { it.code == code } ?: UNKNOWN
        }
    }
}

enum class ResultCode {
    OK,
    ERROR;

    companion object {
        fun get(code: Int): ResultCode {
            return if (code == Activity.RESULT_OK) {
                OK
            } else {
                ERROR
            }
        }
    }
}
