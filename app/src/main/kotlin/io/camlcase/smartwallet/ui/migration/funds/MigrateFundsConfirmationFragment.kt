/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.migration.funds

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.core.extension.*
import io.camlcase.smartwallet.model.DataWrapper
import io.camlcase.smartwallet.model.handleResults
import io.camlcase.smartwallet.ui.base.BaseFragment
import io.camlcase.smartwallet.ui.custom.ScrollOffsetListener
import kotlinx.android.synthetic.main.fragment_funds_confirmation.*
import kotlinx.android.synthetic.main.view_bottom_action_button.*
import kotlinx.android.synthetic.main.view_collapsing_toolbar.*
import kotlinx.android.synthetic.main.view_funds_confirm.*

class MigrateFundsConfirmationFragment : BaseFragment() {
    interface FundsMigrationListener {
        fun onMigratedFunds(operationHash: String)
        fun onConfirmationError(error: Throwable?)
    }

    private var listener: FundsMigrationListener? = null
    private lateinit var viewModel: MigrateFundsViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_funds_confirmation, container, false)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        listener = context as? FundsMigrationListener
        if (listener == null) {
            throw ClassCastException("$context must implement MigrateFundsConfirmFragment")
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = viewModel(viewModelFactory) {
            observe(data, { handleDetails(it) })
            observe(operation, { handleOperation(it) })
        }
        viewModel.getConfirmationDetails()
    }

    private fun handleDetails(wrapper: DataWrapper<FundsMigrationForView>) {
        wrapper.handleResults(
            onValidData = {
                hideLoading()
                funds_section.setValues(it.toSend, it.toSendInCurrency)
                fees_section.setValues(it.fees, it.feesInCurrency)
            },
            onError = {
                hideLoading()
                listener?.onConfirmationError(it)
            },
            onLoading = {
                showLoading()
            }
        )
    }

    private fun handleOperation(wrapper: DataWrapper<String>) {
        wrapper.handleResults(
            onValidData = {
                listener?.onMigratedFunds(it)
            },
            onError = {
                hideLoading()
                listener?.onConfirmationError(it)
            },
            onLoading = {
                showLoading()
            }
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initAppBar()
        action_next.setText(R.string.action_confirm)
        bindViews()
    }

    private fun bindViews() {
        action_next.throttledClick()
            .subscribe {
                viewModel.migrateFunds()
            }
            .addToDisposables(disposables)
    }

    private fun initAppBar() {
        setupToolbar(toolbar)
        val title = getString(R.string.mig_funds_confirm_title)
        expanded_title.text = title
        app_bar.addOnOffsetChangedListener(
            ScrollOffsetListener(
                expanded_title,
                toolbar,
                title
            )
        )
    }

    fun showLoading() {
        migration_loading.show()
        action_next.isEnabled = false
    }

    fun hideLoading() {
        migration_loading.hide()
        action_next.isEnabled = true
    }

    companion object {
        fun init(): MigrateFundsConfirmationFragment {
            return MigrateFundsConfirmationFragment()
        }
    }
}
