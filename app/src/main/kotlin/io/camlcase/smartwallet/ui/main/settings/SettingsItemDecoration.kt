/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.main.settings

import android.graphics.Canvas
import android.view.View
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.ui.custom.DecorateRow

/**
 * Adds line divider only on certain rows
 */
class SettingsItemDecoration(
    private val headerType: Int = SettingsAdapter.HEADER
) : RecyclerView.ItemDecoration(), DecorateRow {
    @DrawableRes
    override val dividerRes: Int = R.drawable.divider

    override fun onDrawOver(c: Canvas, parent: RecyclerView, state: RecyclerView.State) {
        ContextCompat.getDrawable(parent.context, dividerRes)?.let {
            val left = parent.paddingLeft
            val right = parent.width - parent.paddingRight

            val childCount = parent.childCount
            for (i in 0 until childCount) {
                val child = parent.getChildAt(i)

                if (hasDivider(parent, child, i, (childCount - 1))) {
                    decorateRow(c, child, it, left, right)
                }
            }
        }
    }

    private fun hasDivider(parent: RecyclerView, child: View, index: Int, max: Int): Boolean {
        val position: Int = parent.getChildAdapterPosition(child)
        parent.adapter?.getItemViewType(position)?.apply {
            // Only show divider if:
            // - Settings row
            // - It's not the last row of a section
            return if (headerType < 0) {
                index < max
            } else {
                val lastOfGroup = index == max || getItemType(parent, index + 1) == headerType
                this != headerType && !lastOfGroup
            }
        }

        return true
    }

    fun getItemType(parent: RecyclerView, index: Int): Int {
        val child = parent.getChildAt(index)
        val position: Int = parent.getChildAdapterPosition(child)
        parent.adapter?.getItemViewType(position)?.apply {
            return this
        }
        return SettingsAdapter.HEADER
    }

    companion object {
        internal const val NO_HEADERS = -1
    }
}
