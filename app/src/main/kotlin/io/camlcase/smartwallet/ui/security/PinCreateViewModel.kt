/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.security

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import io.camlcase.smartwallet.core.architecture.BaseViewModel
import io.camlcase.smartwallet.core.extension.addToDisposables
import io.camlcase.smartwallet.core.extension.subscribeForUI
import io.camlcase.smartwallet.data.source.user.PinLocalSource
import io.camlcase.smartwallet.model.DataWrapper
import io.camlcase.smartwallet.model.navigation.PinNavigation
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

/**
 * Handles type and verification of a PIN for accessing the app.
 */
class PinCreateViewModel @Inject constructor(
    private val pinLocalSource: PinLocalSource
) : BaseViewModel() {

    private val _step = MutableLiveData<DataWrapper<PinStep>>()
    val step: LiveData<DataWrapper<PinStep>> = _step

    val bundle: CreatePinForView = CreatePinForView()

    @PinNavigation.ScreenSource
    var source: Int = PinNavigation.SOURCE_SETTINGS

    fun next(pin: String) {
        when (bundle.step) {
            is PinStep.TypePin -> {
                bundle.pin = pin
                bundle.step = PinStep.ConfirmPin(source)
                _step.value = DataWrapper.Success(bundle.step)
            }
            is PinStep.ConfirmPin -> {
                _step.value = DataWrapper.Loading()
                saveAsyncPin(pin)
            }
            PinStep.AllOK -> {
                /*Nothing to do*/
            }
        }
    }

    private fun saveAsyncPin(pin: String) {
        Single.fromCallable { pin }
            .map {
                savePin(it)
                it
            }
            .subscribeForUI()
            .subscribe(
                {
                    bundle.step = PinStep.AllOK
                    _step.value = DataWrapper.Success(bundle.step)
                },
                {
                    // Should never happen
                    _step.value = DataWrapper.Error(it)
                    back()
                })
            .addToDisposables(disposables)
    }

    fun valid(typedPin: String): Boolean {
        return if (bundle.step is PinStep.ConfirmPin) {
            typedPin == bundle.pin
        } else {
            typedPin.length in MIN_PIN_DIGITS..MAX_PIN_DIGITS
        }
    }

    /**
     * @return true if [typedPin] matches the previous pin first digits OR is empty
     */
    fun correctType(typedPin: String): Boolean {
        if (typedPin.isNotEmpty()) {
            return typedPin == bundle.pin?.take(typedPin.length)
        }
        return true
    }

    fun savePin(pin: String) {
        pinLocalSource.storePin(pin)
    }

    fun back(): Boolean {
        if (bundle.step is PinStep.ConfirmPin) {
            bundle.step = PinStep.TypePin(source)
            _step.value = DataWrapper.Success(bundle.step)
            return true
        }
        return false
    }

    companion object {
        const val MIN_PIN_DIGITS = 4
        const val MAX_PIN_DIGITS = 6
    }
}

data class CreatePinForView(
    var pin: String? = null,
    var step: PinStep = PinStep.TypePin(PinNavigation.SOURCE_SETTINGS)
)

sealed class PinStep() {
    class TypePin(@PinNavigation.ScreenSource val source: Int) : PinStep()
    class ConfirmPin(@PinNavigation.ScreenSource val source: Int) : PinStep()
    object AllOK : PinStep()
}
