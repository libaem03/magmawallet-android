/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.main.contact.edit

import android.accounts.AccountManager
import android.content.Context
import android.os.Bundle
import android.text.InputType
import android.text.InputType.TYPE_TEXT_FLAG_CAP_WORDS
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.core.widget.doOnTextChanged
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.core.extension.*
import io.camlcase.smartwallet.data.model.WrappedUri
import io.camlcase.smartwallet.data.model.contact.MagmaContact
import io.camlcase.smartwallet.model.DataWrapper
import io.camlcase.smartwallet.model.handleResults
import io.camlcase.smartwallet.ui.base.*
import io.camlcase.smartwallet.ui.base.navigate.AppNavigator
import io.camlcase.smartwallet.ui.base.navigate.RequestCode
import io.camlcase.smartwallet.ui.custom.ScrollOffsetListener
import io.camlcase.smartwallet.ui.custom.setForTezosAddress
import io.camlcase.smartwallet.ui.qr.QRScanType
import io.camlcase.smartwallet.ui.qr.ScanQRActivity
import io.camlcase.smartwallet.ui.send.GetUserAddressViewModel
import kotlinx.android.synthetic.main.fragment_add_contact.*
import kotlinx.android.synthetic.main.view_add_contact.*
import kotlinx.android.synthetic.main.view_bottom_action_button.*
import kotlinx.android.synthetic.main.view_collapsing_toolbar.*
import kotlinx.android.synthetic.main.view_icon_clear_input.view.*

/**
 * Form screen to populate a device contact with a tz1 address.
 *
 * Works for ADD and EDIT
 * @see [ContactDetailFragment]
 */
class AddContactFragment : BaseFragment() {
    interface AddContactListener {
        fun onContactSaved(contact: MagmaContact)
    }

    private lateinit var viewModel: AddContactViewModel
    private lateinit var addressviewModel: GetUserAddressViewModel
    private var listener: AddContactListener? = null
    private var editContact: MagmaContact? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        manageIntent(arguments)
        return inflater.inflate(R.layout.fragment_add_contact, container, false)
    }

    private fun manageIntent(bundle: Bundle?) {
        bundle?.getParcelable<MagmaContact>(ARG_CONTACT)?.let {
            editContact = it
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        listener = context as? AddContactListener

        viewModel = viewModel(viewModelFactory) {
            observe(picked, { onPickedContact(it) })
            observe(save, { onContactSaved(it) })
        }
        addressviewModel = viewModel(viewModelFactory) {}
        addressviewModel.getUserAddress()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        editContact?.apply {
            viewModel.populateEditContact(this)
        }

        initAppbar()
        keyboardListener = initKeyboardListener(action_next) { visible ->
            if (visible) {
                app_bar.setExpanded(false, true)
            } else {
                app_bar.setExpanded(true, true)
            }
        }

        action_next.setText(R.string.con_add_save_action)
        name.textField()?.setHint(R.string.con_add_name_placeholder)
        name.textField()?.inputType =
            InputType.TYPE_CLASS_TEXT or TYPE_TEXT_FLAG_CAP_WORDS or InputType.TYPE_TEXT_VARIATION_PERSON_NAME
        name.textField()?.imeOptions = EditorInfo.IME_ACTION_NEXT
        name.setMaxLength(120)
        address.input.setHint(R.string.snd_recipient_hint)
        address.endIcon()?.contentDescription = getString(R.string.snd_scan_qr)
        name.endIcon()?.contentDescription = getString(R.string.con_add_device_button)

        if (viewModel.isEdit()) {
            name.endIcon().hide()
        }

        bindViews()
        bindScreenResults()
    }

    private fun initAppbar() {
        setupToolbar(toolbar, true)
        val title = if (viewModel.isEdit()) {
            getString(R.string.con_edit_title)
        } else {
            getString(R.string.con_add_title)
        }
        expanded_title.text = title
        app_bar.addOnOffsetChangedListener(
            ScrollOffsetListener(
                expanded_title,
                toolbar,
                title
            )
        )
    }

    private fun bindViews() {
        if (viewModel.isAdd()) {
            name.bindIcon(disposables) {
                AppNavigator.openContactsPicker(activity)
            }
        }

        name.textField()?.doOnTextChanged { text, _, _, _ ->
            when (viewModel.onNameChanged(text?.toString())) {
                AddContactViewModel.RESET -> {
                    address.textField()?.text = null
                    address.dismissError()
                    name.textField()?.isEnabled = true
                    if (viewModel.isAdd()) {
                        name.endIcon().show() // Show again "pick"
                    }
                }
                else -> {
                    // Validate text
                    action_next.isEnabled = viewModel.valid()
                }
            }
        }

        address.setForTezosAddress(
            disposables,
            isUserAddress = { isUserAddress() },
            onAddressOK = {
                viewModel.onAddressChanged(it)
                action_next.isEnabled = viewModel.valid()
            },
            onAddressKO = {
                viewModel.onAddressChanged(it)
                action_next.isEnabled = false
            },
            onQRClick = {
                AppNavigator.navigateToScanQR(activity, QRScanType.SEND)
            }
        )
//
//        address.bindIcon(disposables) {
//            AppNavigator.navigateToScanQR(activity, QRScanType.SEND)
//        }
//
//        address.textField()?.doOnTextChanged { text, _, _, _ ->
//            val input = text.toString()
//            viewModel.onAddressChanged(input)
//
//            if (input.isEmpty() || (input.validTezosAddress() && !isUserAddress())) {
//                address.dismissError()
//                action_next.isEnabled = viewModel.valid()
//            } else {
//                address.showError(getString(R.string.error_invalid_address))
//                action_next.isEnabled = false
//            }
//        }

        action_next.throttledClick()
            .subscribe {
                if (viewModel.isUpdate()) {
                    viewModel.saveContact("", name.textField()?.text.toString(), address.textField()?.text.toString())
                } else {
                    AppNavigator.openAccountPicker(activity)
                }
            }
            .addToDisposables(disposables)
    }

    private fun bindScreenResults() {
        activityResult.get().bind(
            RequestCode.CONTACTS_PICKER,
            disposable = disposables,
            onResult = {
                it.data?.data?.apply {
                    viewModel.pickContact(WrappedUri(this))
                }
            },
            onError = { showError(it) }
        )

        activityResult.get().bind(
            RequestCode.ACCOUNTS_PICKER,
            disposable = disposables,
            onResult = {
                it.data?.apply {
                    this.getStringExtra(AccountManager.KEY_ACCOUNT_NAME)?.let { accountName ->
                        viewModel.saveContact(
                            accountName,
                            name.textField()?.text.toString(),
                            address.textField()?.text.toString()
                        )
                    }
                }
            },
            onError = { showError(it) }
        )

        activityResult.get().bind(
            RequestCode.SCAN_QR,
            disposable = disposables,
            onResult = {
                it.data?.getStringExtra(ScanQRActivity.ARG_ADDRESS_SCANNED)?.apply {
                    address.textField()?.setText(this)
                }
            },
            onError = { showError(it) }
        )
    }

    private fun onPickedContact(wrapper: DataWrapper<MagmaContact>) {
        wrapper.handleResults(
            onValidData = {
                hideLoading()
                name.textField()?.setText(it.name)
                name.textField()?.isEnabled = viewModel.isNameEditable()
                if (!viewModel.showPicker()) {
                    name.endIcon().hide()
                }
                if (viewModel.hideClear()) {
                    name.hideClear()
                }
                if (viewModel.hideQR()) {
                    address.endIcon().hide()
                }

                if (it.tezosAddresses.isNotEmpty()) {
                    address.textField()?.setText(it.tezosAddresses[0].address)
                }
            },
            onError = {
                hideLoading()
                // ADD: Contact already has a Tezos address for that network
                if (it is IllegalArgumentException) {
                    context?.apply {
                        singleAnswerDialog(
                            this,
                            message = getString(R.string.con_error_already_exists)
                        )
                    }
                } else {
                    showError(it)
                }
            },
            onLoading = {
                showLoading()
            }
        )
    }

    private fun onContactSaved(wrapper: DataWrapper<Pair<Boolean, MagmaContact>>) {
        wrapper.handleResults(
            onValidData = {
                hideLoading()
                if (it.first) {
                    // Leave screen
                    listener?.onContactSaved(it.second)
                } else {
                    showSnackbar(getString(R.string.error_generic) + " " + getString(R.string.error_try_again))
                }
            },
            onError = {
                hideLoading()
                showError(it)
            },
            onLoading = {
                showLoading()
            }
        )
    }

    private fun isUserAddress(): Boolean {
        return addressviewModel.isUserAddress(address.input.text.toString())
    }

    private fun showLoading() {
        loading.show()
        activity.disableTouch()
    }

    private fun hideLoading() {
        loading.hide()
        activity.enableTouch()
    }

    companion object {
        private const val ARG_CONTACT = "args.edit.contact"

        /**
         * @param contact EDIT mode
         */
        fun init(contact: MagmaContact? = null): AddContactFragment {
            val fragment = AddContactFragment()
            val args = Bundle()
            args.putParcelable(ARG_CONTACT, contact)
            fragment.arguments = args
            return fragment
        }
    }
}
