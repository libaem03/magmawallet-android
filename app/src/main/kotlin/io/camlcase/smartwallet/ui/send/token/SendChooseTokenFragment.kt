/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.send.token

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.core.extension.*
import io.camlcase.smartwallet.data.model.exchange.TokenInfoBundle
import io.camlcase.smartwallet.model.DataWrapper
import io.camlcase.smartwallet.model.handleResults
import io.camlcase.smartwallet.model.token.BalancesForView
import io.camlcase.smartwallet.ui.base.BaseFragment
import io.camlcase.smartwallet.ui.base.ItemSelector
import io.camlcase.smartwallet.ui.base.showError
import io.camlcase.smartwallet.ui.main.token.TokenListViewModel
import kotlinx.android.synthetic.main.fragment_send_choose_token.*
import kotlinx.android.synthetic.main.view_collapsing_toolbar.*
import kotlinx.android.synthetic.main.view_full_loading.*

/**
 * If [ScreenSource] on [SendActivity] == [SendActivity.SOURCE_MAIN], make the user choose the token they want to send.
 */
class SendChooseTokenFragment : BaseFragment() {
    private lateinit var viewModel: TokenListViewModel
    private var listener: ItemSelector<TokenInfoBundle>? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_send_choose_token, container, false)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        listener = context as? ItemSelector<TokenInfoBundle>
        if (listener == null) {
            throw ClassCastException("$context must implement ItemSelector<TokenInfoBundle>")
        }
        viewModel = this.viewModel(viewModelFactory) {
            observe(balances, { handleData(it) })
        }
        viewModel.fallbackTokens()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initAppBar()
        initList()
    }

    private fun initAppBar() {
        setupToolbar(toolbar, true)
        expanded_title.text = getString(R.string.snd_token_asset_title)
    }

    private fun initList() {
        list?.apply {
            layoutManager = LinearLayoutManager(activity)
            setHasFixedSize(true)
        }
    }

    private fun handleData(wrapper: DataWrapper<BalancesForView>) {
        wrapper.handleResults(
            onValidData = {
                hideLoading()
                val fullList = arrayListOf(it.xtz)
                fullList.addAll(it.faTokens)
                list.adapter = ChooseTokenListAdapter(fullList) { token, view ->
                    view.setOnClickListener {
                        viewModel.getTokenInfoBundle(token.token)?.apply {
                            listener?.onItemSelected(this)
                        }
                    }
                }
            },
            onError = {
                hideLoading()
                showError(it)
            },
            onLoading = {
                showLoading()
            }
        )
    }

    private fun showLoading() {
        container_loading.show()
    }

    private fun hideLoading() {
        container_loading.hide()
    }

    companion object {
        fun init(): SendChooseTokenFragment {
            return SendChooseTokenFragment()
        }
    }
}
