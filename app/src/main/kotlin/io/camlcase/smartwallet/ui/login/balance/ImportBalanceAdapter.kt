/**
 * # Released under MIT License
 *
 * Copyright (c) 2021 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.login.balance

import android.text.SpannableString
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.core.extension.ellipsize
import io.camlcase.smartwallet.core.extension.hide
import io.camlcase.smartwallet.core.extension.spanFontType
import io.camlcase.smartwallet.databinding.RowImportBalanceBinding
import io.camlcase.smartwallet.databinding.RowImportBalanceHeaderBinding

class ImportBalanceAdapter(
    private val values: List<ImportBalanceListItem>,
    private val onItemClick: (ImportBalance, View) -> Unit
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    inner class ViewHolder(val binding: RowImportBalanceBinding) : RecyclerView.ViewHolder(binding.root)
    inner class HeaderViewHolder(val binding: RowImportBalanceHeaderBinding) : RecyclerView.ViewHolder(binding.root)

    override fun getItemCount(): Int {
        return values.size
    }

    override fun getItemViewType(position: Int): Int {
        return if (values[position] is ImportBalance) {
            TYPE_ITEM
        } else {
            TYPE_HEADER
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            TYPE_HEADER -> {
                val binding = RowImportBalanceHeaderBinding.inflate(LayoutInflater.from(parent.context), parent, false)
                HeaderViewHolder(binding)
            }
            else -> {
                val binding = RowImportBalanceBinding.inflate(LayoutInflater.from(parent.context), parent, false)
                ViewHolder(binding)
            }
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is HeaderViewHolder -> {
                holder.binding.header.setText(
                    if (position == 0) {
                        R.string.onb_import_balance_hdwallet
                    } else {
                        R.string.onb_import_balance_non_hdwallet
                    }
                )
            }
            is ViewHolder -> {
                val context = holder.itemView.context
                val item = values[position] as ImportBalance
                holder.binding.address.text = item.address.ellipsize()

                if (item.xtzBalance != null) {
                    val balanceXTZText = context.getString(R.string.onb_import_balance_subtitle, item.xtzBalance)
                    val balanceXtzSpannable =
                        context.spanFontType(SpannableString(balanceXTZText), R.font.barlow_regular, item.xtzBalance)
                    holder.binding.balanceXtz.text = balanceXtzSpannable
                } else {
                    holder.binding.balanceXtz.hide()
                }

                if (item.faTokenBalance != null) {
                    val otherTokens = if (item.faTokenBalance) {
                        context.getString(R.string.onb_import_balance_other_tokens_yes)
                    } else {
                        context.getString(R.string.onb_import_balance_other_tokens_no)
                    }
                    val balanceFAText =
                        context.getString(R.string.onb_import_balance_other_tokens, otherTokens)
                    val balanceFASpannable = context.spanFontType(
                        SpannableString(balanceFAText),
                        R.font.barlow_regular,
                        otherTokens
                    )
                    holder.binding.balanceTokens.text = balanceFASpannable
                } else {
                    holder.binding.balanceTokens.hide()
                }

                onItemClick(item, holder.itemView)
            }
        }
    }

    companion object {
        private const val TYPE_HEADER = 0
        private const val TYPE_ITEM = 1
    }
}
