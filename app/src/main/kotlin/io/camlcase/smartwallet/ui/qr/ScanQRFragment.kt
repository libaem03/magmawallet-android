/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.qr

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import com.journeyapps.barcodescanner.CaptureManager
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.core.extension.addToDisposables
import io.camlcase.smartwallet.core.extension.show
import io.camlcase.smartwallet.core.extension.subscribeForUI
import io.camlcase.smartwallet.core.extension.throttledClick
import io.camlcase.smartwallet.data.core.extension.tezos.validTezosAddress
import io.camlcase.smartwallet.ui.base.BaseFragment
import io.camlcase.smartwallet.ui.base.navigate.AppNavigator
import io.camlcase.smartwallet.ui.base.showError
import io.camlcase.smartwallet.ui.base.showSnackbar
import io.camlcase.smartwallet.ui.permissions.CameraPermission
import io.camlcase.smartwallet.ui.permissions.CameraPermissionDelegate
import io.camlcase.smartwallet.ui.permissions.PermissionDelegate
import io.camlcase.smartwallet.ui.qr.ScanQRActivity.Companion.ARG_ADDRESS_SCANNED
import kotlinx.android.synthetic.main.fragment_scan_qr.*
import java.lang.ref.WeakReference

class ScanQRFragment : BaseFragment(), PermissionDelegate.PermissionsListener {
    private var capture: CaptureManager? = null
    private var cameraPermissionsDelegate: PermissionDelegate? = null
    private var type: QRScanType = QRScanType.SEND

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_scan_qr, container, false)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        manageIntent(activity?.intent?.extras)
        cameraPermissionsDelegate = CameraPermissionDelegate(this, context)
    }

    private fun manageIntent(activityBundle: Bundle?) {
        activityBundle?.getString(ScanQRActivity.ARG_TYPE, QRScanType.SEND.name)?.let {
            type = QRScanType.valueOf(it)
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        bindViews()
        bindScreenResults()
    }

    private fun bindScreenResults() {
        activityResult.get()?.apply {
            cameraPermissionsDelegate?.listenToPermissionsSettings(this)
        }
        permissionResult.let {
            cameraPermissionsDelegate?.listenToPermissionsDialog(it)
        }
    }

    private fun bindViews() {
        action_back.throttledClick()
            .subscribe { activity?.onBackPressed() }
            .addToDisposables(disposables)

        qr_view.scanResult
            .subscribeForUI()
            .filter { it.text.isNotEmpty() }
            .map { it.text }
            .doOnNext {
                pauseQRCapture()
            }
            .doOnSubscribe { startQRCapture() }
            .subscribe(
                { manageQRResult(it) },
                { showError(it) }
            ).addToDisposables(disposables)
    }

    private fun pauseQRCapture() {
        capture?.also {
            it.onPause()
        }
    }

    private fun startQRCapture() {
        capture?.also {
            it.onResume()
        }
    }

    override fun onResume() {
        super.onResume()
        // permissions
        if (CameraPermission.hasPermission(activity)) {
            initQRCapture()
        } else {
            cameraPermissionsDelegate?.onPermissionButtonClick()
        }
    }

    override fun onPause() {
        super.onPause()
        pauseQRCapture()
    }

    private fun manageQRResult(result: String) {
        if (result.validTezosAddress()) {
            // Navigate back with address
            activity?.apply {
                val intent = Intent()
                intent.putExtra(ARG_ADDRESS_SCANNED, result)
                this.setResult(Activity.RESULT_OK, intent)
                this.finishAfterTransition()
            }
        } else {
            showError(IllegalArgumentException(getString(R.string.error_invalid_address)))
            qr_view.post {
                startQRCapture()
            }
        }
    }

    override fun activityReference(): WeakReference<AppCompatActivity?> {
        return WeakReference(activity as? AppCompatActivity)
    }

    override fun navigateToSettings() {
        activity?.apply {
            AppNavigator.navigateToPermissionSettings(this)
        }
    }

    override fun onPermission(hasPermission: Boolean) {
        if (hasPermission) {
            initQRCapture()
        }
    }

    private fun initQRCapture() {
        activity?.apply {
            qr_view.show()
            if (capture == null) {
                capture = CaptureManager(this, qr_view)
            }
            startQRCapture()
        }
    }

    override fun showPermissionsError(error: Throwable?) {
        view.showSnackbar(getString(R.string.scan_qr_camera_permission))
        view?.postDelayed({
            activity?.onBackPressed()
        }, 1 * 1000)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        capture?.onDestroy()
        cameraPermissionsDelegate?.unbind()
    }
}
