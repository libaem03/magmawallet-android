/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.currency

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.core.extension.*
import io.camlcase.smartwallet.model.DataWrapper
import io.camlcase.smartwallet.model.handleResults
import io.camlcase.smartwallet.ui.base.BaseFragment
import io.camlcase.smartwallet.ui.base.showError
import io.camlcase.smartwallet.ui.custom.ScrollOffsetListener
import io.camlcase.smartwallet.ui.main.settings.SettingsItemDecoration
import kotlinx.android.synthetic.main.fragment_local_currencies.*
import kotlinx.android.synthetic.main.fragment_operation_list.list
import kotlinx.android.synthetic.main.view_collapsing_toolbar.*
import kotlinx.android.synthetic.main.view_collapsing_toolbar.toolbar

/**
 * Shows a list of supported currencies to compare with the XTZ value
 */
class LocalCurrencyFragment : BaseFragment() {
    interface LocalCurrencyListener {
        fun onCurrencyChanged()
    }

    private lateinit var viewModel: LocalCurrencyViewModel
    private var listener: LocalCurrencyListener? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_local_currencies, container, false)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        listener = context as? LocalCurrencyListener
        viewModel = viewModel(viewModelFactory) {
            observe(data, { handleData(it) })
            observe(operation, { handleOperation(it) })
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initAppbar()
        list?.apply {
            layoutManager = LinearLayoutManager(activity)
            setHasFixedSize(true)
            addItemDecoration(SettingsItemDecoration())
        }
        viewModel.getAvailableCurrencies()
    }

    private fun handleData(wrapper: DataWrapper<List<CurrencyListItem>>) {
        wrapper.handleResults(
            onValidData = {
                hideLoading()
                init(it)
            },
            onError = {
                hideLoading()
                showError(it)
                if (list.adapter?.itemCount == null) {
                    showEmptyStatus()
                }
            },
            onLoading = {
                showLoading()
            }
        )
    }

    private fun init(currencies: List<CurrencyListItem>) {
        if (currencies.isNullOrEmpty() || currencies.size == 2) {
            showEmptyStatus()
        } else {
            list.show()
            list?.adapter = LocalCurrencyAdapter(currencies) { currencyBundle, view ->
                view.setOnClickListener {
                    viewModel.changeCurrency(currencyBundle)
                }
            }
        }
    }

    private fun showEmptyStatus() {
        empty_status.show()
        list.hide()
    }

    private fun handleOperation(wrapper: DataWrapper<Double>) {
        wrapper.handleResults(
            onValidData = {
                hideLoading()
                listener?.onCurrencyChanged()
            },
            onError = {
                hideLoading()
                showError(it)
            },
            onLoading = {
                showLoading()
            }
        )
    }

    private fun showLoading() {
        currencies_loading?.show()
        list?.isEnabled = false
    }

    private fun hideLoading() {
        currencies_loading?.hide()
        list?.isEnabled = true
    }

    private fun initAppbar() {
        setupToolbar(toolbar, true)

        val title = getString(R.string.set_currency_title)
        expanded_title.text = title
        app_bar.addOnOffsetChangedListener(
            ScrollOffsetListener(
                expanded_title,
                toolbar,
                title
            )
        )
    }
}
