/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.main.contact.edit

import androidx.annotation.IntDef
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import io.camlcase.kotlintezos.model.Address
import io.camlcase.kotlintezos.model.validTezosAddress
import io.camlcase.smartwallet.core.architecture.BaseViewModel
import io.camlcase.smartwallet.core.extension.subscribeForUI
import io.camlcase.smartwallet.data.model.WrappedUri
import io.camlcase.smartwallet.data.model.contact.MagmaContact
import io.camlcase.smartwallet.data.usecase.analytics.AnalyticEvent
import io.camlcase.smartwallet.data.usecase.analytics.SendAnalyticUseCase
import io.camlcase.smartwallet.data.usecase.contact.WriteContactUseCase
import io.camlcase.smartwallet.model.DataWrapper
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

/**
 * Handles ADD / EDIT
 */
class AddContactViewModel @Inject constructor(
    private val useCase: WriteContactUseCase,
    private val analytics: SendAnalyticUseCase
) : BaseViewModel() {
    /**
     * Contact picked from Contacts screen OR an edition
     * @see [pickContact]
     * @see [populateEditContact]
     */
    private val _picked = MutableLiveData<DataWrapper<MagmaContact>>()
    val picked: LiveData<DataWrapper<MagmaContact>> = _picked

    /**
     * Results of the SAVE operation
     * @see [saveContact]
     */
    private val _save = MutableLiveData<DataWrapper<Pair<Boolean, MagmaContact>>>()
    val save: LiveData<DataWrapper<Pair<Boolean, MagmaContact>>> = _save

    @ScreenMode
    var mode: Int = ADD_CONTACT

    private var pickedContact: MagmaContact? = null
    private var typedName: String? = null
    private var typedAddress: String? = null

    /**
     * @param account Gmail account which to save the contact on
     * @param name Name of the contact
     * @param address Tz1 Address for that contact
     */
    fun saveContact(account: String, name: String, address: Address) {
        _save.value = DataWrapper.Loading()
        if (pickedContact != null) {
            editContact(name, address)
        } else {
            addContact(account, name, address)
        }
    }

    private fun addContact(account: String, name: String, address: Address) {
        Single.fromCallable {
            useCase.addContact(account, name, address)
        }
            .subscribeForUI()
            .subscribe(
                {
                    _save.value = DataWrapper.Success(Pair(true, it))
                    analytics.reportEvent(AnalyticEvent.ADD_CONTACT_SUCCESS)
                },
                {
                    _save.value = DataWrapper.Error(it)
                    analytics.reportEvent(AnalyticEvent.ADD_CONTACT_ERROR)
                }
            )
    }

    private fun editContact(name: String, address: Address) {
        pickedContact?.apply {
            Single.fromCallable {
                useCase.editContact(this, Pair(name, address))
            }.subscribeForUI()
                .subscribe(
                    {
                        _save.value = DataWrapper.Success(it)
                        if (mode == PICKED_CONTACT) { // Only when we are picking a new contact
                            if (it.first) {
                                analytics.reportEvent(AnalyticEvent.ADD_CONTACT_SUCCESS)
                            } else {
                                analytics.reportEvent(AnalyticEvent.ADD_CONTACT_ERROR)
                            }
                        }
                    },
                    {
                        _save.value = DataWrapper.Error(it)
                        if (mode == PICKED_CONTACT) {
                            analytics.reportEvent(AnalyticEvent.ADD_CONTACT_ERROR)
                        }
                    }
                )
        }
    }

    fun populateEditContact(contact: MagmaContact) {
        if (contact.id == 0L) {
            mode = ADD_FROM_SEND
            _picked.value = DataWrapper.Success(contact)
        } else {
            mode = EDIT_CONTACT

            _picked.value = DataWrapper.Loading()
            Single.fromCallable {
                useCase.isMagmaContact(contact)
            }.subscribeForUI()
                .subscribe(
                    {
                        mode = if (it) {
                            EDIT_MAGMA_CONTACT
                        } else {
                            EDIT_CONTACT
                        }
                        pickedContact = contact
                        _picked.value = DataWrapper.Success(contact)
                    },
                    {
                        _picked.value = DataWrapper.Error(it)
                    }
                )
        }
    }

    fun pickContact(bundle: WrappedUri) {
        _picked.value = DataWrapper.Loading()
        Single.fromCallable {
            useCase.pickContact(bundle)
        }.subscribeForUI()
            .subscribe(
                {
                    if (!it.tezosAddresses.isNullOrEmpty()) {
                        _picked.value = DataWrapper.Error(IllegalArgumentException())
                    } else {
                        mode = PICKED_CONTACT
                        pickedContact = it
                        _picked.value = DataWrapper.Success(it)
                    }
                },
                {
                    _picked.value = DataWrapper.Error(it)
                }
            )
    }

    @InputState
    fun onNameChanged(newName: String?): Int {
        return if (newName.isNullOrBlank() && mode == PICKED_CONTACT) {
            // User picked a contact, tapped clear. Screen should reset.
            mode = ADD_CONTACT
            pickedContact = null
            RESET
        } else {
            typedName = newName
            NORMAL_ADD
        }
    }

    fun onAddressChanged(newAddress: String?) {
        typedAddress = newAddress
    }

    fun valid(): Boolean {
        val isValid = !typedName.isNullOrBlank() && typedAddress.validTezosAddress()
        val hasChanged = pickedContact?.let {
            val addressChanged = if (!it.tezosAddresses.isNullOrEmpty()) {
                typedAddress != it.tezosAddresses[0].address
            } else {
                true
            }
            typedName != it.name || addressChanged
        } ?: true
        return isValid && hasChanged
    }

    fun isUpdate(): Boolean {
        return pickedContact != null
    }

    fun isNameEditable(): Boolean {
        return mode == ADD_CONTACT || mode == EDIT_MAGMA_CONTACT || mode == ADD_FROM_SEND
    }

    fun showPicker(): Boolean {
        return mode == ADD_CONTACT || mode == ADD_FROM_SEND
    }

    fun hideClear(): Boolean {
        return mode == EDIT_CONTACT
    }

    fun hideQR(): Boolean {
        return mode == ADD_FROM_SEND
    }

    fun isEdit(): Boolean {
        return mode == EDIT_CONTACT || mode == EDIT_MAGMA_CONTACT
    }

    fun isAdd(): Boolean {
        return mode == ADD_CONTACT || mode == PICKED_CONTACT || mode == ADD_FROM_SEND
    }

    companion object {
        @IntDef(
            RESET,
            NORMAL_ADD
        )
        @Retention(AnnotationRetention.SOURCE)
        annotation class InputState

        const val RESET = 0
        const val NORMAL_ADD = 1

        @IntDef(
            ADD_CONTACT,
            PICKED_CONTACT,
            EDIT_MAGMA_CONTACT,
            EDIT_CONTACT,
            ADD_FROM_SEND
        )
        @Retention(AnnotationRetention.SOURCE)
        annotation class ScreenMode

        const val ADD_CONTACT = 0
        const val PICKED_CONTACT = 1
        const val EDIT_MAGMA_CONTACT = 2
        const val EDIT_CONTACT = 3
        const val ADD_FROM_SEND = 4
    }
}
