/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.security.pin

import android.content.Context
import android.content.Intent
import android.os.Bundle
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.core.Arguments
import io.camlcase.smartwallet.core.extension.markSecure
import io.camlcase.smartwallet.core.extension.replaceFragmentSafely
import io.camlcase.smartwallet.model.navigation.PinNavigation
import io.camlcase.smartwallet.ui.base.BaseActivity
import io.camlcase.smartwallet.ui.security.PinCreateFragment

/**
 * Change User PIN
 *
 * @see PinConfirmFragment
 * @see PinCreateFragment
 */
class PinChangeActivity : BaseActivity(R.layout.activity_fragment),
    PinConfirmFragment.PinConfirmListener, PinCreateFragment.PinCreateListener {
    @PinNavigation.ScreenSource
    private var source: Int = PinNavigation.SOURCE_SETTINGS

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        markSecure()
        if (savedInstanceState == null) {
            replaceFragmentSafely(PinConfirmFragment(), R.id.container_fragment)
            intent.extras?.getInt(Arguments.ARG_SOURCE)?.let {
                source = it
            }

            if (source == PinNavigation.SOURCE_SETTINGS) {
                overridePendingTransition(R.anim.slide_in_right, R.anim.nothing)
            }
        }
    }

    override fun onPinConfirmed() {
        replaceFragmentSafely(PinCreateFragment.init(PinNavigation.SOURCE_SETTINGS), R.id.container_fragment)
    }

    override fun onPinSet() {
        finishAfterTransition()
    }

    override fun onBackPressed() {
        overrideBackPress()
        if (source == PinNavigation.SOURCE_SETTINGS) {
            overridePendingTransition(R.anim.nothing, R.anim.slide_out_right)
        }
    }

    companion object {

        fun init(context: Context, @PinNavigation.ScreenSource source: Int = PinNavigation.SOURCE_SETTINGS): Intent {
            val intent = Intent(context, PinChangeActivity::class.java)
            intent.putExtra(Arguments.ARG_SOURCE, source)
            return intent
        }
    }
}
