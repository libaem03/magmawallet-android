/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.main.settings.attributions

import android.content.Context
import android.content.Intent
import android.os.Bundle
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.core.extension.listenToBackground
import io.camlcase.smartwallet.core.extension.replaceFragmentSafely
import io.camlcase.smartwallet.ui.base.BaseActivity

class AttributionsActivity: BaseActivity(R.layout.activity_fragment) {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        listenToBackground()
        if (savedInstanceState == null) {
            replaceFragmentSafely(AttributionsFragment(), R.id.container_fragment)
            overridePendingTransition(R.anim.slide_in_right, R.anim.nothing)
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(R.anim.nothing, R.anim.slide_out_right)
    }

    companion object {
        fun init(context: Context): Intent {
            return Intent(context, AttributionsActivity::class.java)
        }
    }
}
