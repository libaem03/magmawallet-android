/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.receive

import android.graphics.Paint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.core.extension.*
import io.camlcase.smartwallet.data.core.XTZ
import io.camlcase.smartwallet.databinding.FragmentReceiveBinding
import io.camlcase.smartwallet.model.DataWrapper
import io.camlcase.smartwallet.model.QR
import io.camlcase.smartwallet.model.handleResults
import io.camlcase.smartwallet.ui.base.BaseBottomSheetFragment
import io.camlcase.smartwallet.ui.base.navigate.AppNavigator
import io.camlcase.smartwallet.ui.base.showError
import io.camlcase.smartwallet.ui.base.showSnackbar

class ReceiveFragment : BaseBottomSheetFragment() {
    private val binding: FragmentReceiveBinding
        get() = _binding as FragmentReceiveBinding
    private lateinit var viewModel: ReceiveViewModel
    private var symbol: String = XTZ.symbol

    override fun onCreateViewBind(inflater: LayoutInflater, container: ViewGroup?): FragmentReceiveBinding {
        manageIntent(arguments)
        return FragmentReceiveBinding.inflate(inflater, container, false)
    }

    private fun manageIntent(bundle: Bundle?) {
        bundle?.getString(ARG_TOKEN_SYMBOL, null)?.apply {
            symbol = this
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initAppbar()
        binding.actionCopy.paintFlags = binding.actionCopy.paintFlags or Paint.UNDERLINE_TEXT_FLAG
        binding.title.text = getString(R.string.rec_title_token, symbol)
    }

    private fun initAppbar() {
        setupToolbar(binding.toolbar, true)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        bindViews()
        viewModel = viewModel(viewModelFactory) {
            observe(address, { handleAddress(it) })
            observe(qr, { handleQR(it) })
        }
        viewModel.getUserData()
    }

    private fun handleQR(wrapper: DataWrapper<QR>) {
        wrapper.handleResults(
            onValidData = {
                hideLoading()
                binding.qrView.qrCode.setImageBitmap(it.bitmap)
            },
            onError = {
                hideLoading()
                showError(it)
            },
            onLoading = {
                showLoading()
            }
        )
    }

    private fun showLoading() {
        binding.receiveLoading.root.show()
        binding.actionCopy.isEnabled = false
        binding.actionShare.isEnabled = false
    }

    private fun hideLoading() {
        binding.receiveLoading.root.hide()
        binding.actionCopy.isEnabled = true
        binding.actionShare.isEnabled = true
    }

    private fun handleAddress(wrapper: DataWrapper<String>) {
        wrapper.handleResults(
            onValidData = { binding.qrView.tzAddress.text = it },
            onError = { showError(it) }
        )
    }

    private fun bindViews() {
        binding.actionCopy.throttledClick()
            .subscribe {
                val tezosAddress = binding.qrView.tzAddress.text.toString()
                activity.copyToClipBoard(
                    tezosAddress,
                    { binding.root.showSnackbar(R.string.action_copy_success) })
            }
            .addToDisposables(disposables)

        binding.actionShare.throttledClick()
            .subscribe {
                val tezosAddress = binding.qrView.tzAddress.text.toString()
                AppNavigator.openShare(activity, getString(R.string.rec_action_share_subject), tezosAddress)
            }
            .addToDisposables(disposables)
    }

    companion object {
        private const val ARG_TOKEN_SYMBOL = "args.token.symbol"

        fun init(symbol: String? = null): ReceiveFragment {
            val fragment = ReceiveFragment()
            val args = Bundle()
            args.putString(ARG_TOKEN_SYMBOL, symbol)
            fragment.arguments = args
            return fragment
        }
    }
}
