/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.migration.funds

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import io.camlcase.kotlintezos.model.Tez
import io.camlcase.smartwallet.core.architecture.BaseViewModel
import io.camlcase.smartwallet.core.extension.addToDisposables
import io.camlcase.smartwallet.core.extension.subscribeForUI
import io.camlcase.smartwallet.data.core.Migration
import io.camlcase.smartwallet.data.model.amount.CoinBalance
import io.camlcase.smartwallet.data.model.exchange.TokenInfoBundle
import io.camlcase.smartwallet.data.model.isXTZ
import io.camlcase.smartwallet.model.DataWrapper
import io.reactivex.rxjava3.core.Single
import java.math.BigDecimal
import javax.inject.Inject

class BalanceCheckViewModel @Inject constructor() : BaseViewModel() {
    /**
     * Balances to transfer
     */
    private val _data = MutableLiveData<DataWrapper<BalanceCheckForView>>()
    val data: LiveData<DataWrapper<BalanceCheckForView>> = _data

    var transferType: MigrationTransferFundsType = MigrationTransferFundsType.NO_TRANSFER

    fun getBalanceCheck(balances: List<TokenInfoBundle>) {
        Single.fromCallable { parseBalances(balances) }
            .subscribeForUI()
            .subscribe(
                { _data.value = DataWrapper.Success(it) },
                { _data.value = DataWrapper.Error(it) }
            ).addToDisposables(disposables)
    }

    private fun parseBalances(balances: List<TokenInfoBundle>): BalanceCheckForView {
        var totalBalance = BigDecimal.ZERO
        var xtzBalance: Tez = Tez.zero
        for (balance in balances) {
            if (balance.token.isXTZ()) {
                xtzBalance = (balance.balance as CoinBalance).value
            }
            totalBalance = totalBalance.add(balance.balance.bigDecimal)
        }

        transferType = if (totalBalance > BigDecimal.ZERO) {
            if (xtzBalance > Migration.XTZ_BUFFER) {
                MigrationTransferFundsType.SHOULD_TRANSFER
            } else {
                MigrationTransferFundsType.NO_XTZ_BALANCE
            }
        } else {
            MigrationTransferFundsType.NO_TRANSFER
        }

        val leftOverTez = if (transferType == MigrationTransferFundsType.NO_XTZ_BALANCE) {
            Tez(xtzBalance.signedDecimalRepresentation.subtract(Migration.XTZ_BUFFER.signedDecimalRepresentation))
        } else {
            Tez.zero
        }

        return BalanceCheckForView(transferType, leftOverTez)
    }
}

data class BalanceCheckForView(
    val transferFundsType: MigrationTransferFundsType,
    val neededXTZ: Tez
)
