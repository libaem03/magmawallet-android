package io.camlcase.smartwallet.ui.permissions

import android.Manifest
import android.app.Activity
import android.content.Context
import androidx.core.app.ActivityCompat
import androidx.core.content.PermissionChecker

/**
 * Representation of an Android Permission (e.g. [CameraPermission] )
 */
interface AndroidPermission {
    val permissionCode: Int
    val permissionName: String

    fun hasPermission(context: Context?): Boolean {
        return context?.let {
            PermissionChecker.checkSelfPermission(context, permissionName) == PermissionChecker.PERMISSION_GRANTED
        } ?: false
    }

    /**
     * @return true if this is not the first time the user sees and rejects the permission dialog and needs more info
     * about why this is needed.
     */
    fun userNeedsMoreInfo(activity: Activity?): Boolean {
        return activity?.let {
            ActivityCompat.shouldShowRequestPermissionRationale(activity, permissionName)
        } ?: false
    }

    fun showPermissionDialog(activity: Activity?) {
        activity?.let {
            ActivityCompat.requestPermissions(it, arrayOf(permissionName), permissionCode)
        }
    }
}

object CameraPermission : AndroidPermission {
    override val permissionCode: Int
        get() = PermissionCode.CAMERA.code
    override val permissionName: String
        get() = Manifest.permission.CAMERA
}

object ReadContactsPermission : AndroidPermission {
    override val permissionCode: Int
        get() = PermissionCode.READ_CONTACTS.code
    override val permissionName: String
        get() = Manifest.permission.READ_CONTACTS
}

object WriteContactsPermission : AndroidPermission {
    override val permissionCode: Int
        get() = PermissionCode.WRITE_CONTACTS.code
    override val permissionName: String
        get() = Manifest.permission.WRITE_CONTACTS
}

enum class PermissionCode(val code: Int) {
    CAMERA(14517),
    READ_CONTACTS(19299),
    WRITE_CONTACTS(19239);

    companion object {
        fun get(code: Int): PermissionCode {
            return values().firstOrNull { it.code == code } ?: CAMERA
        }
    }
}
