/**
 * # Released under MIT License
 *
 * Copyright (c) 2021 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.exchange.confirmation

import android.content.Context
import android.os.Bundle
import android.view.View
import by.kirich1409.viewbindingdelegate.viewBinding
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.core.extension.addToDisposables
import io.camlcase.smartwallet.core.extension.setupToolbar
import io.camlcase.smartwallet.core.extension.slideInRight
import io.camlcase.smartwallet.core.extension.throttledClick
import io.camlcase.smartwallet.databinding.FragmentLiquidityDetailBinding
import io.camlcase.smartwallet.ui.base.BindBaseFragment
import io.camlcase.smartwallet.ui.base.info.InfoModalFragment
import io.camlcase.smartwallet.ui.base.info.InfoModalType
import io.camlcase.smartwallet.ui.base.navigate.navigateToModal
import io.camlcase.smartwallet.ui.custom.ScrollOffsetListener

/**
 * Show liquidity fee information to the user.
 * @see SwapConfirmationFragment
 */
class LiquidityFeeDetailFragment : BindBaseFragment(R.layout.fragment_liquidity_detail) {
    private val binding by viewBinding(FragmentLiquidityDetailBinding::bind)
    private lateinit var liquidityFee: String
    private lateinit var liquidityFeeInCurrency: String

    override fun onAttach(context: Context) {
        super.onAttach(context)
        manageIntent(arguments)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        slideInRight()
    }

    private fun manageIntent(bundle: Bundle?) {
        bundle?.getString(ARG_LIQUIDITY_FEE)?.let {
            liquidityFee = it
        } ?: throw IllegalStateException("Screen needs an ARG_LIQUIDITY_FEE argument")

        bundle.getString(ARG_LIQUIDITY_FEE_CURRENCY)?.let {
            liquidityFeeInCurrency = it
        } ?: throw IllegalStateException("Screen needs an ARG_LIQUIDITY_FEE_CURRENCY argument")
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initAppBar()
        bindViews()
        binding.feesSection.setValues(
            liquidityFee,
            liquidityFeeInCurrency
        )
    }

    private fun bindViews() {
        binding.infoButton.throttledClick()
            .subscribe {
                navigateToModal(InfoModalFragment.init(InfoModalType.LIQUIDITY_FEE_INFO))
            }.addToDisposables(disposables)
    }

    private fun initAppBar() {
        setupToolbar(binding.appBarContainer.toolbar, true)
        val title = getString(R.string.liquidity_fees_title)
        binding.appBarContainer.expandedTitle.text = title
        binding.appBarContainer.appBar.addOnOffsetChangedListener(
            ScrollOffsetListener(
                binding.appBarContainer.expandedTitle,
                binding.appBarContainer.toolbar,
                title
            )
        )
    }

    companion object {
        private const val ARG_LIQUIDITY_FEE = "arg.fee.liquidity"
        private const val ARG_LIQUIDITY_FEE_CURRENCY = "arg.fee.liquidity.currency"

        fun init(liquidityFee: String, liquidityFeeInCurrency: String): LiquidityFeeDetailFragment {
            val fragment = LiquidityFeeDetailFragment()
            val args = Bundle()
            args.putString(ARG_LIQUIDITY_FEE, liquidityFee)
            args.putString(ARG_LIQUIDITY_FEE_CURRENCY, liquidityFeeInCurrency)
            fragment.arguments = args
            return fragment
        }
    }
}
