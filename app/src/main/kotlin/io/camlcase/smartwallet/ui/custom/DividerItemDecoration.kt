package io.camlcase.smartwallet.ui.custom

import android.graphics.Canvas
import android.graphics.drawable.Drawable
import android.view.View
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import io.camlcase.smartwallet.R

/**
 * RecyclerView's Line Divider
 */
class DividerItemDecoration(@DrawableRes override val dividerRes: Int = R.drawable.divider) :
    RecyclerView.ItemDecoration(), DecorateRow {

    override fun onDrawOver(c: Canvas, parent: RecyclerView, state: RecyclerView.State) {
        decorateList(c, parent, state)
    }
}

interface DecorateRow {
    val dividerRes: Int

    fun decorateList(c: Canvas, parent: RecyclerView, state: RecyclerView.State) {
        ContextCompat.getDrawable(parent.context, dividerRes)?.let {
            val left = parent.paddingLeft
            val right = parent.width - parent.paddingRight

            val childCount = parent.childCount
            for (i in 0 until childCount) {
                val child = parent.getChildAt(i)

                decorateRow(c, child, it, left, right)
            }
        }
    }

    fun decorateRow(c: Canvas, child: View, divider: Drawable, left: Int, right: Int) {
        val params = child.layoutParams as RecyclerView.LayoutParams

        val top = child.bottom + params.bottomMargin
        val bottom = top + divider.intrinsicHeight

        divider.setBounds(left, top, right, bottom)
        divider.draw(c)
    }
}
