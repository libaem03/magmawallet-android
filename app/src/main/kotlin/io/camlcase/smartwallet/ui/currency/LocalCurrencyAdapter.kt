/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.currency

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.data.model.exchange.CurrencyBundle
import kotlinx.android.synthetic.main.row_currency.view.*
import kotlinx.android.synthetic.main.row_setting_header.view.*

class LocalCurrencyAdapter(
    private val values: List<CurrencyListItem>,
    private val onItemClick: (CurrencyBundle, View) -> Unit
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    inner class HeaderViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val header: TextView = view.header
    }

    internal class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val symbol: TextView = view.currency_symbol
        val name: TextView = view.currency_name
    }

    override fun getItemCount(): Int {
        return values.size
    }

    override fun getItemViewType(position: Int): Int {
        val item = values[position]
        return when (item) {
            is CurrencyForView -> CURRENCY
            else -> HEADER
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            HEADER -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.row_setting_header, parent, false)
                HeaderViewHolder(view)
            }
            else -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.row_currency, parent, false)
                return ViewHolder(view)
            }
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = values[position]
        when (holder) {
            is HeaderViewHolder -> {
                bindHeader(position, holder)
            }
            is ViewHolder -> {
                bindCurrency(item as CurrencyForView, holder)
            }
        }
    }

    private fun bindCurrency(item: CurrencyForView, holder: ViewHolder) {
        holder.symbol.text = item.bundle.code
        holder.name.text = item.name
        onItemClick(item.bundle, holder.itemView)
    }

    private fun bindHeader(position: Int, holder: HeaderViewHolder) {
        val context = holder.itemView.context
        val title: String = if (position == 0) {
            context.getString(R.string.set_currency_popular)
        } else {
            context.getString(R.string.set_currency_all)
        }
        holder.header.text = title
    }

    companion object {
        internal const val HEADER = 0
        internal const val CURRENCY = 2
    }
}
