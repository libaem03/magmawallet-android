/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.migration

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.camlcase.kotlintezos.model.Address
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.core.extension.addToDisposables
import io.camlcase.smartwallet.core.extension.copyToClipBoard
import io.camlcase.smartwallet.core.extension.throttledClick
import io.camlcase.smartwallet.ui.base.BaseFragment
import io.camlcase.smartwallet.ui.base.info.InfoType
import io.camlcase.smartwallet.ui.base.info.InfoView
import io.camlcase.smartwallet.ui.base.showSnackbar
import kotlinx.android.synthetic.main.fragment_migration_success.*

class MigrationSuccessFragment : BaseFragment() {
    private var listener: InfoView.InfoEventsListener? = null
    private var address: Address? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        manageIntent(arguments)
        return inflater.inflate(R.layout.fragment_migration_success, container, false)
    }

    private fun manageIntent(bundle: Bundle?) {
        bundle?.getString(ARG_MIGRATION_ADDRESS)?.let {
            address = it
        } ?: throw IllegalStateException("Screen needs an ARG_MIGRATION_ADDRESS argument")
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        listener = context as? InfoView.InfoEventsListener
        if (listener == null) {
            throw ClassCastException("$context must implement InfoListener")
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        listener?.apply {
            container_info.init(InfoType.MIGRATION_SUCCESS, this, null)
        }
        migration_address.text = address
        bindViews()
    }

    private fun bindViews() {
        action_copy.throttledClick()
            .subscribe {
                address?.apply {
                    activity.copyToClipBoard(this, { it.showSnackbar(R.string.action_copy_success) })
                }
            }.addToDisposables(disposables)
    }

    companion object {
        private const val ARG_MIGRATION_ADDRESS = "args.migration_address"

        fun init(address: Address): MigrationSuccessFragment {
            val fragment = MigrationSuccessFragment()
            val args = Bundle()
            args.putString(ARG_MIGRATION_ADDRESS, address)
            fragment.arguments = args
            return fragment
        }
    }
}
