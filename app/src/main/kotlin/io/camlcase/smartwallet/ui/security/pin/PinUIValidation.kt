/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.security.pin

import android.widget.TextView
import com.google.android.material.button.MaterialButton
import io.camlcase.smartwallet.core.extension.hide
import io.camlcase.smartwallet.core.extension.show
import io.camlcase.smartwallet.ui.security.PinCreateViewModel
import io.camlcase.smartwallet.ui.security.lock.AuthLoginViewModel
import java.lang.ref.WeakReference

/**
 * Extracted UI behaviour for validating PIN on screens
 *
 * @see AuthLoginActivity
 * @see PinConfirmFragment
 */
interface PinUIValidation {
    val viewModel: AuthLoginViewModel
    val buttonRef: WeakReference<MaterialButton>
    val errorViewRef: WeakReference<TextView>
    val generalError: String

    /**
     * @param fromInput Validation came from text input, not button
     */
    fun onCorrectAuthentication(fromInput: Boolean)

    /**
     * Show a snackbar for a high-priority error
     */
    fun showAlgorithmError()

    /**
     * The input won't show any error, only activate the button when size is the minimum.
     * Will autonavigate if pin validation is correct
     */
    fun validatePinFromInput(input: String) {
        if (input.length >= PinCreateViewModel.MIN_PIN_DIGITS) {
            when (viewModel.validPin(input)) {
                AuthLoginViewModel.VALID_PIN -> {
                    onCorrectAuthentication(true)
                }
                AuthLoginViewModel.INVALID_PIN -> {
                    buttonRef.get()?.isEnabled = true
                }
                AuthLoginViewModel.PIN_ERROR -> {
                    showAlgorithmError()
                    buttonRef.get()?.isEnabled = false
                }
            }
        } else {
            buttonRef.get()?.isEnabled = false
        }
        errorViewRef.get()?.hide()
    }

    /**
     * If input is at least minimum PIN size, will let the user validate.
     */
    fun validatePinFromButton(input: String) {
        when (viewModel.validPin(input)) {
            AuthLoginViewModel.VALID_PIN -> {
                errorViewRef.get()?.hide()
                onCorrectAuthentication(false)
            }
            AuthLoginViewModel.INVALID_PIN -> {
                errorViewRef.get()?.show()
                errorViewRef.get()?.text = generalError
            }
            AuthLoginViewModel.PIN_ERROR -> {
                errorViewRef.get()?.hide()
                showAlgorithmError()
            }
        }
    }
}
