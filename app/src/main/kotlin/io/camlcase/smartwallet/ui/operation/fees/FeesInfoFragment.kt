/**
 * # Released under MIT License
 *
 * Copyright (c) 2021 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.operation.fees

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.core.extension.setupToolbar
import io.camlcase.smartwallet.core.extension.spanFontType
import io.camlcase.smartwallet.databinding.FragmentLongInfoModalBinding
import io.camlcase.smartwallet.ui.base.BaseBottomSheetFragment

/**
 * Since description is so long, we need a way to scroll with only one element.
 * TODO Convert to a LongInfoModal?
 */
class FeesInfoFragment : BaseBottomSheetFragment() {
    private val binding: FragmentLongInfoModalBinding
        get() = _binding as FragmentLongInfoModalBinding

    override fun onCreateViewBind(inflater: LayoutInflater, container: ViewGroup?): FragmentLongInfoModalBinding {
        return FragmentLongInfoModalBinding.inflate(inflater, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupToolbar(binding.toolbar)

        binding.toolbar.setTitle(R.string.fees_modal_title)

        val detail = StringBuilder()
        detail.append(getString(R.string.fees_modal_subtitle))
        detail.append("\n\n")
        val bakerFeeHeading = getString(R.string.fees_modal_baker_heading)
        detail.append(bakerFeeHeading)
        detail.append("\n")
        detail.append(getString(R.string.fees_modal_baker_details))
        detail.append("\n\n")
        val revealFeeHeading = getString(R.string.fees_modal_reveal_heading)
        detail.append(revealFeeHeading)
        detail.append("\n")
        detail.append(getString(R.string.fees_modal_reveal_details))
        detail.append("\n\n")
        val extraFeesHeading = getString(R.string.fees_modal_allocation_heading)
        detail.append(extraFeesHeading)
        detail.append("\n")
        detail.append(getString(R.string.fees_modal_allocation_details))

        val spannable = context.spanFontType(
            detail.toString(),
            R.font.barlow_bold,
            listOf(
                bakerFeeHeading,
                revealFeeHeading,
                extraFeesHeading,
            )
        )

        binding.detail.text = spannable
    }
}
