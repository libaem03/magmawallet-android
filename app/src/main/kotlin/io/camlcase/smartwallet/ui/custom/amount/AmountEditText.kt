/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.custom.amount

import android.content.Context
import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.util.AttributeSet
import com.google.android.material.textfield.TextInputEditText
import java.math.BigDecimal
import java.text.NumberFormat
import java.util.*

/**
 * Custom class for TextInputEditText that handles XTZ/FA1.2 token amounts
 */
class AmountEditText(context: Context, attrs: AttributeSet?) : TextInputEditText(context, attrs) {
    private val decimalInputFilter = DecimalInputFilter()

    init {
        this.inputType =
            InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_DECIMAL or InputType.TYPE_NUMBER_FLAG_SIGNED
        this.hint = "0"
        this.filters = arrayOf(decimalInputFilter)
    }

    /**
     * By default the EditText handles numberDecimal. We can change it to number if we are working with no decimal FA1.2 tokens.
     */
    fun changeInputType(decimal: Boolean) {
        if (decimal) { // XTZ & Decimal point Tokens
            this.inputType =
                InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_DECIMAL or InputType.TYPE_NUMBER_FLAG_SIGNED
        } else { // Tokens
            this.inputType =
                InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_SIGNED
        }
    }

    /**
     * [!] Remember to add any other filters apart from the max value one
     */
    fun setFilters(maxValue: BigDecimal, decimals: Int) {
        val maxFilter = MaxValueInputFilter(maxValue)
        val digitsAfterZero = if (decimals < 2) {
            2
        } else {
            decimals
        }
        this.filters = arrayOf(DecimalInputFilter(digitsAfterZero = digitsAfterZero), maxFilter)
    }

    private fun getAmount(text: String): BigDecimal {
        return try {
            try {
                BigDecimal(text)
            } catch (e: NumberFormatException) {
                // When "Enter Max" button, the formatting is based by the Locale and it might not be parseable by Double
                val numerical = NumberFormat.getInstance(Locale.getDefault()).parse(text)
                BigDecimal.ZERO
            }
        } catch (e: Throwable) {
            BigDecimal.ZERO
        }
    }

    fun getAmount(): BigDecimal {
        return getAmount(text.toString())
    }

    fun listenToChanges(onNewValue: (BigDecimal) -> Unit) {
        this.addTextChangedListener(
            object : TextWatcher {
                override fun afterTextChanged(s: Editable?) {
                    onNewValue(getAmount(s.toString()))
                }

                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                    // Nothing to do
                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                    // Nothing to do
                }
            }
        )
    }
}
