/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.exchange.amount

import io.camlcase.smartwallet.core.architecture.BaseViewModel
import io.camlcase.smartwallet.data.model.Token
import io.camlcase.smartwallet.data.model.exchange.CurrencyBundle
import io.camlcase.smartwallet.data.model.exchange.TokenInfoBundle
import io.camlcase.smartwallet.data.model.exchange.showInExchange
import io.camlcase.smartwallet.data.usecase.info.GetBCDBalanceUseCase
import io.camlcase.smartwallet.model.navigation.ExchangeChooseTokenNavigation
import javax.inject.Inject

class ChooseTokenViewModel @Inject constructor(
    private val useCase: GetBCDBalanceUseCase
) : BaseViewModel() {

    private fun getBalances(): List<TokenInfoBundle> {
        val localBalances = useCase.getLocalBalances()
        return localBalances
            .filter { it.showInExchange() }
            .sortedBy { it.token }
    }

    fun getBalances(
        @ExchangeChooseTokenNavigation.ScreenSource source: Int,
        selectedToken: Token?
    ): List<TokenInfoBundle> {
        return if (source == ExchangeChooseTokenNavigation.SOURCE_FROM) {
            // List order (owned tokens only): XTZ followed by alphabetically
            getBalances().filter { !it.balance.isZero }
        } else {
            // List order: XTZ followed by alphabetical; exclude the token type being exchanged from the receive list
            getBalances().filter { it.token != selectedToken }
        }
    }

    fun getCurrencyBundle(): CurrencyBundle {
        return useCase.getCurrencyBundle()
    }
}
