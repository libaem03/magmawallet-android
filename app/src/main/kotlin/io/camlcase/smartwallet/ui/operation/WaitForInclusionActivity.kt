/**
 * # Released under MIT License
 *
 * Copyright (c) 2021 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.operation

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.core.extension.listenToBackground
import io.camlcase.smartwallet.core.extension.loadFragment
import io.camlcase.smartwallet.core.extension.onOperationInclusion
import io.camlcase.smartwallet.core.extension.replaceFragmentSafely
import io.camlcase.smartwallet.data.core.Logger
import io.camlcase.smartwallet.data.model.error.ExchangeDeadlineTimeout
import io.camlcase.smartwallet.data.model.error.ExchangeInvalidTokens
import io.camlcase.smartwallet.data.model.error.ExchangeZeroTokens
import io.camlcase.smartwallet.data.model.operation.MagmaOperationResult
import io.camlcase.smartwallet.model.navigation.MainNavigation
import io.camlcase.smartwallet.model.operation.OperationResultBundle
import io.camlcase.smartwallet.ui.base.BaseActivity
import io.camlcase.smartwallet.ui.base.info.InfoFragment
import io.camlcase.smartwallet.ui.base.info.InfoType
import io.camlcase.smartwallet.ui.base.info.InfoView
import io.camlcase.smartwallet.ui.delegate.IncludedOperation
import io.camlcase.smartwallet.ui.main.MainActivity

/**
 * Separate operating in two. Second process is waiting for it to be injected in the chain.
 */
class WaitForInclusionActivity : BaseActivity(R.layout.activity_fragment),
    WaitForInclusionFragment.WaitForInclusionListener,
    InfoView.InfoEventsListener {
    private lateinit var bundle: OperationResultBundle

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        listenToBackground()
        if (savedInstanceState == null) {
            bundle = manageIntent(intent.extras)
            replaceFragmentSafely(
                WaitForInclusionFragment.init(
                    bundle.hash,
                    bundle.type,
                    bundle.title,
                    bundle.actionText,
                    bundle.secondaryActionText
                ),
                R.id.container_fragment
            )
            overridePendingTransition(R.anim.slide_in_right, R.anim.nothing)
        }
    }

    private fun manageIntent(extras: Bundle?): OperationResultBundle {
        val bundle = extras?.getParcelable<OperationResultBundle>(ARG_OPERATION_BUNDLE)
        return bundle ?: throw IllegalStateException("Screen should have $ARG_OPERATION_BUNDLE")
    }

    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(R.anim.nothing, R.anim.slide_out_right)
    }

    override fun onProgressAction() {
        when (bundle.type) {
            IncludedOperation.SEND,
            IncludedOperation.DELEGATE,
            IncludedOperation.UNDELEGATE,
            IncludedOperation.EXCHANGE -> finishWithResult()
            else -> {
                // Nothing to do
                // Transfer funds integrates the wait for injection flow in its Activity
            }
        }
    }

    override fun onProgressExtraAction() {
        if (bundle.type == IncludedOperation.EXCHANGE) {
            returnToWallet()
        }
    }

    override fun onInclusion(info: MagmaOperationResult) {
        when (bundle.type) {
            IncludedOperation.SEND -> {
                onOperationInclusion(
                    info,
                    InfoType.SEND_INCLUSION_SUCCESS,
                    InfoType.SEND_INCLUSION_ERROR
                )
            }
            IncludedOperation.EXCHANGE -> {
                val errorInfoType = when (info.parsedError) {
                    is ExchangeInvalidTokens -> InfoType.EXCHANGE_ERROR_INVALID_TOKENS
                    is ExchangeZeroTokens -> InfoType.EXCHANGE_ERROR_ZERO_TOKENS
                    is ExchangeDeadlineTimeout -> InfoType.EXCHANGE_ERROR_INVALID_DEADLINE
                    else -> InfoType.EXCHANGE_INCLUSION_ERROR
                }
                onOperationInclusion(
                    info,
                    InfoType.EXCHANGE_INCLUSION_SUCCESS,
                    errorInfoType
                )
            }
            IncludedOperation.DELEGATE -> {
                onOperationInclusion(
                    info,
                    InfoType.DELEGATE_INCLUSION_SUCCESS,
                    InfoType.DELEGATE_INCLUSION_ERROR
                )
            }
            IncludedOperation.UNDELEGATE -> {
                onOperationInclusion(
                    info,
                    InfoType.UNDELEGATE_INCLUSION_SUCCESS,
                    InfoType.DELEGATE_INCLUSION_ERROR
                )
            }
            else -> {
                // Nothing to do
                // Transfer funds integrates the wait for injection flow in its Activity
            }
        }
    }

    override fun onInclusionError(error: Throwable?) {
        val info = when (error) {
            is ExchangeInvalidTokens -> InfoType.EXCHANGE_ERROR_INVALID_TOKENS
            is ExchangeZeroTokens -> InfoType.EXCHANGE_ERROR_ZERO_TOKENS
            is ExchangeDeadlineTimeout -> InfoType.EXCHANGE_ERROR_INVALID_DEADLINE
            else -> InfoType.TZKT_UNAVAILABLE
        }
        loadFragment(InfoFragment.init(info, error?.message), R.id.container_fragment)
    }

    override fun onActionClick(infoType: InfoType) {
        when (infoType) {
            InfoType.SEND_INCLUSION_SUCCESS,
            InfoType.UNDELEGATE_INCLUSION_SUCCESS,
            InfoType.DELEGATE_INCLUSION_SUCCESS,
            InfoType.EXCHANGE_INCLUSION_SUCCESS -> {
                finishWithResult()
            }
            InfoType.DELEGATE_INCLUSION_ERROR,
            InfoType.SEND_INCLUSION_ERROR -> {
                finishWithType()
            }

            InfoType.OPERATION_COUNTER -> {
                finishScreen()
            }
            InfoType.EXCHANGE_INCLUSION_ERROR,
            InfoType.EXCHANGE_INVALID_PRICE_IMPACT,
            InfoType.EXCHANGE_ERROR_INVALID_DEADLINE,
            InfoType.EXCHANGE_ERROR_INVALID_TOKENS,
            InfoType.EXCHANGE_ERROR_ZERO_TOKENS,
            InfoType.EXCHANGE_ERROR_INSUFFICIENT_XTZ -> {
                finishScreen()
            }
            InfoType.TZKT_UNAVAILABLE -> {
                if (bundle.type == IncludedOperation.DELEGATE || bundle.type == IncludedOperation.UNDELEGATE) {
                    finishWithType()
                } else {
                    finishWithResult()
                }
            }
            else -> {
                Logger.e("InfoFragment of type not supported $infoType")
                finishScreen()
            }
        }
    }

    override fun onSecondaryActionClick(infoType: InfoType) {
        when (infoType) {
            InfoType.EXCHANGE_INCLUSION_SUCCESS -> {
                // Go to my wallet
                returnToWallet()
            }
            InfoType.DELEGATE_INCLUSION_ERROR -> {
                // Return to settings
                finishScreen()
            }
            else -> {
                Logger.e("InfoFragment of type not supported for onSecondaryActionClick $infoType")
                finishScreen()
            }
        }
    }

    private fun finishScreen() {
        finishAfterTransition()
    }

    private fun finishWithType() {
        val intent = Intent()
        intent.putExtra(ARG_OPERATION_TYPE, bundle.type.name)
        setResult(Activity.RESULT_OK, intent)
        finishAfterTransition()
    }

    private fun finishWithResult() {
        setResult(Activity.RESULT_OK, intent)
        finishAfterTransition()
    }

    private fun returnToWallet() {
        val intent = Intent()
        intent.putExtra(MainActivity.ARG_OPERATION_TAB, MainNavigation.TAB_WALLET)
        setResult(Activity.RESULT_OK, intent)
        finishAfterTransition()
    }

    companion object {
        const val ARG_OPERATION_BUNDLE = "args.operation.bundle"
        const val ARG_OPERATION_TYPE = "args.operation.type"

        fun init(context: Context, bundle: OperationResultBundle): Intent {
            val intent = Intent(context, WaitForInclusionActivity::class.java)
            intent.putExtra(ARG_OPERATION_BUNDLE, bundle)
            return intent
        }
    }
}
