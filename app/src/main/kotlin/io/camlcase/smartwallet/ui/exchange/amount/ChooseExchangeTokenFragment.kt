/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.exchange.amount

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewbinding.ViewBinding
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.core.extension.init
import io.camlcase.smartwallet.core.extension.setupToolbar
import io.camlcase.smartwallet.core.extension.viewModel
import io.camlcase.smartwallet.data.model.Token
import io.camlcase.smartwallet.data.model.exchange.TokenInfoBundle
import io.camlcase.smartwallet.databinding.FragmentExchangeChangeTokenBinding
import io.camlcase.smartwallet.model.navigation.ExchangeChooseTokenNavigation
import io.camlcase.smartwallet.ui.base.BaseBottomSheetFragment

/**
 * Select token to be part of the exchange
 */
class ChooseExchangeTokenFragment : BaseBottomSheetFragment() {
    interface ChooseExchangeTokenListener {
        fun onTokenSelected(tokenInfo: TokenInfoBundle, @ExchangeChooseTokenNavigation.ScreenSource source: Int)
    }
    private val binding: FragmentExchangeChangeTokenBinding
        get() = _binding as FragmentExchangeChangeTokenBinding
    private lateinit var viewModel: ChooseTokenViewModel
    private var selectedToken: Token? = null

    @ExchangeChooseTokenNavigation.ScreenSource
    private var source: Int = ExchangeChooseTokenNavigation.SOURCE_FROM
    private var listener: ChooseExchangeTokenListener? = null

    override fun onCreateViewBind(inflater: LayoutInflater, container: ViewGroup?): ViewBinding {
        manageIntent(arguments)
        return FragmentExchangeChangeTokenBinding.inflate(inflater, container, false)
    }

    private fun manageIntent(bundle: Bundle?) {
        bundle?.getParcelable<Token>(ARG_SELECTED_TOKEN)?.let {
            selectedToken = it
        }
        bundle?.getInt(ARG_SELECTED_SOURCE)?.let {
            source = it
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        listener = context as? ChooseExchangeTokenListener
        viewModel = viewModel(viewModelFactory) {}
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupToolbar(binding.toolbar)
        binding.toolbar.setTitle(R.string.exc_select_title)
        binding.list.init(activity)

        val balances = viewModel.getBalances(source, selectedToken)
        populateList(balances)
    }

    private fun populateList(balances: List<TokenInfoBundle>) {
        binding.list.adapter = ExchangeTokenAdapter(
            viewModel.getCurrencyBundle(),
            balances,
            source == ExchangeChooseTokenNavigation.SOURCE_FROM
        ) { item, view ->
            view.setOnClickListener {
                listener?.onTokenSelected(item, source)
                dismiss()
            }
        }
    }

    companion object {
        private const val ARG_LIST_TOKENS = "arg.info.tokens.balance"
        private const val ARG_SELECTED_TOKEN = "arg.info.token.selected"
        private const val ARG_SELECTED_SOURCE = "arg.info.source.selected"

        const val FROM = "from"
        const val TO = "to"

        fun init(
            selected: Token,
            @ExchangeChooseTokenNavigation.ScreenSource source: Int
        ): ChooseExchangeTokenFragment {
            val fragment = ChooseExchangeTokenFragment()
            val args = Bundle()
            args.putParcelable(ARG_SELECTED_TOKEN, selected)
            args.putInt(ARG_SELECTED_SOURCE, source)
            fragment.arguments = args
            return fragment
        }
    }
}
