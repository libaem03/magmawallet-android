/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.money

import android.app.Activity
import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.browser.customtabs.CustomTabColorSchemeParams
import androidx.browser.customtabs.CustomTabsIntent
import androidx.viewbinding.ViewBinding
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.core.customtabs.CustomTabActivityHelper
import io.camlcase.smartwallet.core.customtabs.CustomTabFallback
import io.camlcase.smartwallet.core.customtabs.extensions.launchWithFallback
import io.camlcase.smartwallet.core.extension.*
import io.camlcase.smartwallet.databinding.FragmentBuyXtzBinding
import io.camlcase.smartwallet.ui.base.BaseActivity
import io.camlcase.smartwallet.ui.base.BaseBottomSheetFragment
import io.camlcase.smartwallet.ui.base.navigate.AppNavigator
import io.camlcase.smartwallet.ui.base.showToast

/**
 * Access to the BUY XTZ external flow.
 */
class BuyTezFragment : BaseBottomSheetFragment() {

    private val binding: FragmentBuyXtzBinding
        get() = _binding as FragmentBuyXtzBinding

    override fun onCreateViewBind(inflater: LayoutInflater, container: ViewGroup?): ViewBinding {
        return FragmentBuyXtzBinding.inflate(inflater, container, false)
    }

    private lateinit var buyViewModel: BuyTezViewModel
    private lateinit var customTabActivityHelper: CustomTabActivityHelper

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupToolbar(binding.toolbar, true)
        bindViews()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        // Share view model with other screens
        (activity as? BaseActivity)?.apply {
            buyViewModel = viewModel(viewModelFactory) {}
        }

        customTabActivityHelper = CustomTabActivityHelper(context, lifecycle)
        customTabActivityHelper.mayLaunchUrl(Uri.parse(getUrl()))
    }

    private fun getUrl(): String {
        return buyViewModel.getBuyUrl(context.getDeviceLanguageCode())
    }

    private fun bindViews() {
        binding.actionBuy.throttledClick()
            .subscribe {
                context?.apply {
                    // LAUNCH URL
                    launchBuyTezProvider()
                    // COPY ADDRESS TO CLIPBOARD
                    activity.copyToClipBoard(
                        buyViewModel.getUserAddress(),
                        {
                            // Show a toast with a delay so the info appears to the user at the right time
                            binding.actionBuy.postDelayed({
                                showToast(getString(R.string.buy_copy_info))
                                closeModal()
                            }, COPY_NOTIFICATION_DELAY)
                        })
                }
            }
            .addToDisposables(disposables)
    }

    /**
     * Will open an external intent.
     */
    private fun launchBuyTezProvider() {
        activity?.apply {
            val builder: CustomTabsIntent.Builder = CustomTabsIntent.Builder(customTabActivityHelper.session)

            val colorScheme = CustomTabColorSchemeParams.Builder()
                .setToolbarColor(getColor(R.color.richGradientStart))
                .build()
            builder.setDefaultColorSchemeParams(colorScheme)
                .setShareState(CustomTabsIntent.SHARE_STATE_OFF)
                .setShowTitle(true)

            val customTabsIntent: CustomTabsIntent = builder.build()

            val moonPay = getUrl()
            customTabsIntent.launchWithFallback(this, Uri.parse(moonPay), object :
                CustomTabFallback {
                override fun openUri(activity: Activity, uri: Uri) {
                    AppNavigator.navigateToUrl(activity, moonPay)
                }
            })
        }
    }

    companion object {
        private const val COPY_NOTIFICATION_DELAY: Long = 3 * 1000

        fun init(): BuyTezFragment {
            return BuyTezFragment()
        }
    }
}
