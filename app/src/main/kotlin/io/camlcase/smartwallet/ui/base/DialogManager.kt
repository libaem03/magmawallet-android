/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.base

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import androidx.annotation.ColorInt
import androidx.annotation.StringRes
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.core.extension.getThemeColor

fun okOrCancelDialog(
    context: Context,
    title: String? = null,
    message: String,
    cancelable: Boolean = true,
    @ColorInt okColor: Int,
    @StringRes okString: Int = R.string.ok,
    @StringRes cancelString: Int = R.string.cancel,
    onOK: () -> Unit,
    onCancel: () -> Unit = {}
) {
    okOrCancelDialog(
        context, title, message, cancelable, okColor, context.getString(okString), cancelString, onOK, onCancel
    )
}

fun okOrCancelDialog(
    context: Context,
    title: String? = null,
    message: String,
    cancelable: Boolean = true,
    @ColorInt okColor: Int,
    okString: String,
    @StringRes cancelString: Int = R.string.cancel,
    onOK: () -> Unit,
    onCancel: () -> Unit = {}
) {
    val builder = AlertDialog.Builder(context, R.style.AlertDialogStyle)
    title?.apply { builder.setTitle(title) }
    val alertDialog = builder.setMessage(message)
        .setCancelable(cancelable)
        .setPositiveButton(okString) { _, _ -> onOK() }
        .setNegativeButton(cancelString) { _, _ -> onCancel() }
        .show()

    val positiveButton = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE)
    positiveButton.setTextColor(okColor)

    val negativeButton = alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE)
    negativeButton.setTextColor(context.getThemeColor(R.attr.alertNegativeTextColor))
    negativeButton.setPadding(0, 0, 40, 0)
}

fun singleAnswerDialog(
    context: Context,
    title: String? = null,
    message: String,
    cancelable: Boolean = true,
    okString: String = context.getString(R.string.ok),
    @ColorInt okColor: Int = context.getThemeColor(R.attr.alertNegativeTextColor),
    onOK: (DialogInterface) -> Unit = { it.dismiss() }
) {
    val builder = AlertDialog.Builder(context, R.style.AlertDialogStyle)
    title?.apply { builder.setTitle(title) }
    val alertDialog = builder.setMessage(message)
        .setCancelable(cancelable)
        .setPositiveButton(okString) { dialog, _ -> onOK(dialog) }
        .show()

    val button = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE)
    button.setTextColor(okColor)
}
