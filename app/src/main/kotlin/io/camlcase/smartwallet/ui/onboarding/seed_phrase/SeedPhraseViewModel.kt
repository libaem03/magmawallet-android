/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.onboarding.seed_phrase

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import io.camlcase.smartwallet.core.architecture.BaseViewModel
import io.camlcase.smartwallet.core.extension.addToDisposables
import io.camlcase.smartwallet.core.extension.subscribeForUI
import io.camlcase.smartwallet.data.analytic.report
import io.camlcase.smartwallet.data.usecase.user.CreateWalletUseCase
import io.camlcase.smartwallet.data.usecase.user.GetSeedPhraseUseCase
import io.camlcase.smartwallet.model.DataWrapper
import io.camlcase.smartwallet.model.navigation.SeedPhraseConfirm
import io.camlcase.smartwallet.model.navigation.SeedPhraseNavigation
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

/**
 * Provides a list of words separated in two columns
 */
class SeedPhraseViewModel @Inject constructor(
    private val useCase: GetSeedPhraseUseCase,
    private val createWalletUseCase: CreateWalletUseCase
) : BaseViewModel() {
    private val _data = MutableLiveData<DataWrapper<TwoRowMnemonic>>()
    val data: LiveData<DataWrapper<TwoRowMnemonic>> = _data

    private val _validation = MutableLiveData<DataWrapper<Boolean>>()
    val validation: LiveData<DataWrapper<Boolean>> = _validation

    @SeedPhraseConfirm.ScreenSource
    var source: Int = SeedPhraseConfirm.SOURCE_ONBOARDING

    fun getSeedPhrase(@SeedPhraseNavigation.ScreenSource source: Int) {
        _data.value = DataWrapper.Loading()

        if (source == SeedPhraseNavigation.SOURCE_MIGRATION) {
            Single.fromCallable { createWalletUseCase.getMigrationWallet() }
                .map { Pair(it.mnemonic, it.derivationPath) }
        } else {
            useCase.getSeedPhraseData()
        }
            .map { TwoRowMnemonic(it.first, it.second) }
            .subscribeForUI()
            .subscribe(
                {
                    _data.value = DataWrapper.Success(it)
                    clearOperationSubscription()
                },
                {
                    _data.value = DataWrapper.Error(it)
                    clearOperationSubscription()
                }
            ).addToDisposables(operationDisposables)
    }

    fun hasPassphrase(onPassphrase: (Boolean) -> Unit) {
        if (source == SeedPhraseConfirm.SOURCE_MIGRATION) {
            onPassphrase(false)
        } else {
            useCase.hasPassphrase()
                .subscribeForUI()
                .subscribe(
                    {
                        onPassphrase(it)
                        clearOperationSubscription()
                    },
                    {
                        it.report()
                        onPassphrase(false)
                        clearOperationSubscription()
                    }
                ).addToDisposables(operationDisposables)
        }
    }

    fun confirmMnemonic(mnemonic: List<String>, passphrase: String?) {
        if (source == SeedPhraseConfirm.SOURCE_ONBOARDING) {
            useCase.confirmMnemonic(mnemonic, passphrase)
        } else {
            Single.fromCallable { useCase.confirmMigrationMnemonic(mnemonic) }
        }
            .subscribeForUI()
            .subscribe(
                {
                    _validation.value = DataWrapper.Success(it)
                    clearOperationSubscription()
                },
                {
                    _validation.value = DataWrapper.Error(it)
                    clearOperationSubscription()
                }
            ).addToDisposables(operationDisposables)
    }
}

data class TwoRowMnemonic(
    val left: List<String>,
    val right: List<String>,
    val middle: Int,
    val derivationPath: String?
) {
    companion object {
        operator fun invoke(list: List<String>, derivationPath: String?) = run {
            val middle = list.size / 2
            val leftRow = list.subList(0, middle)
            val rightRow = list.subList(middle, list.size)
            TwoRowMnemonic(leftRow, rightRow, middle, derivationPath)
        }
    }
}
