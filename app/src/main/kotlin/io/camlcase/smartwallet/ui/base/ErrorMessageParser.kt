/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.base

import android.content.Context
import io.camlcase.kotlintezos.core.ext.isTypeOfError
import io.camlcase.kotlintezos.core.ext.listCauses
import io.camlcase.kotlintezos.model.RPCErrorCause
import io.camlcase.kotlintezos.model.TezosError
import io.camlcase.kotlintezos.model.TezosErrorType
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.data.core.api.ConnectionInterceptor
import io.camlcase.smartwallet.data.core.extension.tezos.parseMessage
import io.camlcase.smartwallet.data.model.error.*
import java.io.IOException
import java.net.ConnectException
import java.net.UnknownHostException
import javax.inject.Inject

class ErrorMessageParser @Inject constructor(
    private val context: Context
) {

    val genericMessage = context.getString(R.string.error_generic)

    fun parseMessage(error: Throwable): String {
        return when {
            error is TezosError -> parseTezosErrorMessage(error)
            error.cause is TezosError -> parseTezosErrorMessage(error.cause as TezosError)
            error is MagmaError -> parseMagmeErrorMessage(error)
            else -> parseGenericMessage(error)
        }
    }

    fun parseAppErrorMessage(error: AppError): String {
        return when (error.type) {
            ErrorType.BLOCKED_OPERATION -> context.getString(R.string.error_transaction_insufficient_funds_title)
            ErrorType.NO_WALLET -> context.getString(R.string.error_wallet)
            ErrorType.WALLET_CREATE_ERROR -> context.getString(R.string.error_invalid_seed_phrase)
            ErrorType.TEZOS_ERROR -> error.message ?: parseGenericMessage(error)
            else -> parseGenericMessage(error)
        }
    }

    fun parseTezosErrorMessage(error: TezosError): String {
        if (error.exception is ConnectionInterceptor.NoConnectivityException
            || error.exception is ConnectionInterceptor.NoInternetException
            || error.exception is ConnectException
        ) {
            return context.getString(R.string.error_no_connection)
        }

        if (error.type == TezosErrorType.COUNTER) {
            return context.getString(R.string.error_transaction_counter)
        }

        if (!error.rpcErrors.isNullOrEmpty()) {
            if (error.rpcErrors?.listCauses().isTypeOfError(RPCErrorCause.INSUFFICIENT_XTZ_BALANCE)) {
                return context.getString(R.string.error_transaction_insufficient_funds)
            }
            // RPC Errors are the priority as are the most self-explanatory
            val rpcError = error.rpcErrors?.firstOrNull { it.message != null } ?: error.rpcErrors?.get(0)

            return rpcError.parseMessage()
        }

        if (error.exception is IOException || error.exception is UnknownHostException) {
            // Connectivity problem, unexpected end of stream, okhttp specific errors
            return context.getString(R.string.error_generic) + " " + context.getString(R.string.error_try_again)
        }
        return parseGenericMessage(error)
    }

    fun parseMagmeErrorMessage(error: MagmaError): String {
        if (error is AppError) {
            return parseAppErrorMessage(error)
        }

        val message: String? = when (error) {
            is OperationInProgress -> context.getString(R.string.error_transaction_counter)
            is InvalidAddressError -> context.getString(R.string.error_transaction_invalid_address_title)
            is InsufficientXTZ -> context.getString(R.string.error_transaction_insufficient_funds_title)
            is BakerCantDelegate -> context.getString(R.string.error_transaction_baker_cant_delegate_title)
            is UnchangedDelegate -> context.getString(R.string.error_transaction_delegation_unchanged)
            is ExchangeZeroTokens -> context.getString(R.string.error_transaction_exchange_higher_than_zero)
            is ExchangeInvalidTokens -> context.getString(R.string.error_transaction_exchange_out_of_sync)
            is ExchangeError -> error.message
            else -> null
        }
        return message ?: parseGenericMessage(null)
    }

    fun parseGenericMessage(error: Throwable?): String {
        val message = error?.message ?: genericMessage
        return message
    }
}
