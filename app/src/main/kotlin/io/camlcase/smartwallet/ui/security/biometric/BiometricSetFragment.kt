/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.security.biometric

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import by.kirich1409.viewbindingdelegate.viewBinding
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.core.extension.reportView
import io.camlcase.smartwallet.core.extension.addToDisposables
import io.camlcase.smartwallet.core.extension.show
import io.camlcase.smartwallet.core.extension.throttledClick
import io.camlcase.smartwallet.core.extension.viewModel
import io.camlcase.smartwallet.data.usecase.analytics.ScreenEvent
import io.camlcase.smartwallet.databinding.ViewInfoBinding
import io.camlcase.smartwallet.ui.base.BindBaseFragment
import io.camlcase.smartwallet.ui.base.showSnackbar
import io.camlcase.smartwallet.ui.security.initBiometrics
import io.camlcase.smartwallet.ui.security.userCancelledBiometricPrompt

/**
 * Shows a generic Info view but with biometric functionality on primary action.
 *
 * @see AppBiometricManager
 */
class BiometricSetFragment : BindBaseFragment(R.layout.view_info) {
    interface BiometricSetListener {
        fun onBiometricSet()
        fun skipBiometric()
    }

    private val binding by viewBinding(ViewInfoBinding::bind)
    private var listener: BiometricSetListener? = null
    private lateinit var viewModel: BiometricSetViewModel

    override fun onAttach(context: Context) {
        super.onAttach(context)
        reportView(ScreenEvent.ONB_BIOMETRIC_PROMPT)
        listener = context as? BiometricSetListener
        if (listener == null) {
            throw ClassCastException("$context must implement BiometricSetListener")
        }
        viewModel = viewModel(viewModelFactory) {}
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        populate()
    }

    private fun populate() {
        val detailText = getString(R.string.android_onb_biometric_prompt)
        binding.detail.show()
        binding.detail.text = detailText

        binding.animationView.show()
        val layoutParams: ConstraintLayout.LayoutParams =
            binding.animationView.layoutParams as ConstraintLayout.LayoutParams
        layoutParams.topMargin = resources.getDimensionPixelSize(R.dimen.spacing_medium_x)
        binding.animationView.layoutParams = layoutParams
        binding.animationView.imageAssetsFolder = "lottie_anim_biometric/"
        binding.animationView.setAnimation(R.raw.anim_biometric)

        binding.actionPrimary.text = getString(R.string.android_onb_biometric_prompt_yes)
        binding.actionPrimary.show()
        binding.actionPrimary.throttledClick()
            .subscribe { initBiometricAuth() }
            .addToDisposables(disposables)

        binding.actionSecondary.text = getString(R.string.onb_biometric_prompt_no)
        binding.actionSecondary.show()
        binding.actionSecondary.throttledClick()
            .subscribe { listener?.skipBiometric() }
            .addToDisposables(disposables)
    }

    private fun initBiometricAuth() {
        val biometricAuth = initBiometrics(
            onAuthSuccess = {
                analyticsUseCase.get()?.reportScreen(ScreenEvent.ONB_BIOMETRIC_CAPTURE)
                viewModel.updateBiometricLogin()
                listener?.onBiometricSet()
            },
            onAuthError = { code, message ->
                if (!code.userCancelledBiometricPrompt()) {
                    view.showSnackbar(message.toString())
                }
            })
        if (!biometricAuth) {
            view.showSnackbar(getString(R.string.error_generic))
        }
    }
}
