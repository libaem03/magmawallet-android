/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.exchange

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.core.extension.*
import io.camlcase.smartwallet.data.core.Exchange.INVALID_PRICE_IMPACT_THRESHOLD
import io.camlcase.smartwallet.data.core.Exchange.bigWarningPriceImpactRange
import io.camlcase.smartwallet.data.core.Exchange.noticePriceImpactRange
import io.camlcase.smartwallet.data.core.Exchange.warningPriceImpactRange
import io.camlcase.smartwallet.data.core.Logger
import io.camlcase.smartwallet.data.model.error.*
import io.camlcase.smartwallet.data.model.swap.SwapRequest
import io.camlcase.smartwallet.model.navigation.MainNavigation
import io.camlcase.smartwallet.model.operation.OperationFeesBundle
import io.camlcase.smartwallet.model.operation.PriceImpactInfo
import io.camlcase.smartwallet.model.operation.finishWithBundleResult
import io.camlcase.smartwallet.ui.base.BaseActivity
import io.camlcase.smartwallet.ui.base.info.InfoFragment
import io.camlcase.smartwallet.ui.base.info.InfoType
import io.camlcase.smartwallet.ui.base.info.InfoView
import io.camlcase.smartwallet.ui.delegate.IncludedOperation
import io.camlcase.smartwallet.ui.exchange.confirmation.LiquidityFeeDetailFragment
import io.camlcase.smartwallet.ui.exchange.confirmation.SwapConfirmationFragment
import io.camlcase.smartwallet.ui.exchange.options.PriceImpactNoticeFragment
import io.camlcase.smartwallet.ui.main.MainActivity
import io.camlcase.smartwallet.ui.operation.fees.FeesDetailFragment

/**
 * DEXTER Exchange flow
 *
 * First screen comes from [MainActivity]: [ExchangeAmountFragment]
 * @see PriceImpactNoticeFragment
 * @see SwapConfirmationFragment
 */
class ExchangeActivity : BaseActivity(R.layout.activity_fragment),
    PriceImpactNoticeFragment.SlippageWarningListener,
    SwapConfirmationFragment.SwapConfirmationListener,
    InfoView.InfoEventsListener {
    private var swapRequest: SwapRequest? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        listenToBackground()
        manageIntent(intent.extras)
        if (savedInstanceState == null) {
            navigateTo(swapRequest!!)
        }
    }

    private fun navigateTo(swapRequest: SwapRequest) {
        val priceImpact = swapRequest.priceImpactRounded
        // Order matters
        val fragment = when {
            priceImpact >= INVALID_PRICE_IMPACT_THRESHOLD -> {
                InfoFragment.init(InfoType.EXCHANGE_INVALID_PRICE_IMPACT)
            }
            priceImpact in bigWarningPriceImpactRange -> {
                PriceImpactNoticeFragment.init(swapRequest.priceImpactPercentage, PriceImpactInfo.BIG_WARNING)
            }
            priceImpact in warningPriceImpactRange -> {
                PriceImpactNoticeFragment.init(swapRequest.priceImpactPercentage, PriceImpactInfo.WARNING)
            }
            priceImpact in noticePriceImpactRange -> {
                PriceImpactNoticeFragment.init(swapRequest.priceImpactPercentage, PriceImpactInfo.NOTICE)
            }
            else -> {
                SwapConfirmationFragment.init(swapRequest)
            }
        }

        replaceFragmentSafely(fragment, R.id.container_fragment)
    }

    private fun manageIntent(extras: Bundle?) {
        extras?.let {
            it.getParcelable<SwapRequest>(ARG_SWAP_FIELDS)?.apply {
                swapRequest = this
            }
        } ?: throw IllegalStateException("ExchangeActivity needs an ARG_SWAP_FIELDS argument")
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putParcelable(ARG_SWAP_FIELDS, swapRequest)
        super.onSaveInstanceState(outState)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        swapRequest = savedInstanceState.getParcelable(ARG_SWAP_FIELDS)
        super.onRestoreInstanceState(savedInstanceState)
    }

    override fun onSlippageWarningContinue() {
        displayFragment(SwapConfirmationFragment.init(swapRequest!!), R.id.container_fragment)
    }

    override fun onSlippageWarningBacktrack() {
        finishAfterTransition()
    }

    override fun navigateToLiquidityFeeDetail(liquidityFee: String, inCurrency: String) {
        displayFragment(LiquidityFeeDetailFragment.init(liquidityFee, inCurrency), R.id.container_fragment)
    }

    override fun navigateToFeesDetail(fees: OperationFeesBundle) {
        displayFragment(FeesDetailFragment.init(fees), R.id.container_fragment)
    }

    override fun onSwapOperationFinished(fromToken: String, toToken: String, operationHash: String) {
        val title = getString(R.string.exc_progress_title, fromToken, toToken)

        finishWithBundleResult(
            IncludedOperation.EXCHANGE,
            operationHash,
            title
        )
    }

    /**
     * Operation Success Main Action click "Return to Exchange"
     * Slippage error Invalid Threshold click "Back to Exchange"
     */
    override fun onActionClick(infoType: InfoType) {
        when (infoType) {
            InfoType.EXCHANGE_INVALID_PRICE_IMPACT,
            InfoType.EXCHANGE_ERROR_INVALID_DEADLINE,
            InfoType.EXCHANGE_ERROR_INVALID_TOKENS,
            InfoType.EXCHANGE_ERROR_ZERO_TOKENS,
            InfoType.OPERATION_COUNTER,
            InfoType.EXCHANGE_ERROR_INSUFFICIENT_XTZ -> {
                // User will end up in Exchange tab with all data set up
                finishScreen()
            }
            else -> {
                Logger.w("InfoFragment of type not supported $infoType")
            }
        }
    }

    private fun finishScreen() {
        finishAfterTransition()
    }

    private fun successAndReturn(@MainNavigation.Tab tab: Int = MainNavigation.TAB_EXCHANGE) {
        val intent = Intent()
        intent.putExtra(MainActivity.ARG_OPERATION_TAB, tab)
        setResult(Activity.RESULT_OK, intent)
    }

    private fun returnToWallet() {
        successAndReturn(MainNavigation.TAB_WALLET)
        finishScreen()
    }

    companion object {
        internal const val ARG_SWAP_FIELDS = "arg.swap.fields"

        fun init(context: Context, swapRequest: SwapRequest): Intent {
            val intent = Intent(context, ExchangeActivity::class.java)
            intent.putExtra(ARG_SWAP_FIELDS, swapRequest)
            return intent
        }
    }
}
