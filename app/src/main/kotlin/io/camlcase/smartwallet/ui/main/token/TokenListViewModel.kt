/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.main.token

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import io.camlcase.smartwallet.core.architecture.BaseViewModel
import io.camlcase.smartwallet.core.extension.addToDisposables
import io.camlcase.smartwallet.core.extension.subscribeForUI
import io.camlcase.smartwallet.data.core.Wallet
import io.camlcase.smartwallet.data.core.extension.formatAsMoney
import io.camlcase.smartwallet.data.model.Token
import io.camlcase.smartwallet.data.model.amount.Balance
import io.camlcase.smartwallet.data.model.exchange.CurrencyBundle
import io.camlcase.smartwallet.data.model.exchange.TokenInfoBundle
import io.camlcase.smartwallet.data.model.exchange.showInExchange
import io.camlcase.smartwallet.data.model.isXTZ
import io.camlcase.smartwallet.data.usecase.info.GetBCDBalanceUseCase
import io.camlcase.smartwallet.model.DataWrapper
import io.camlcase.smartwallet.model.navigation.SendNavigation
import io.camlcase.smartwallet.model.token.BalancesForView
import io.camlcase.smartwallet.model.token.TokenForView
import io.camlcase.smartwallet.ui.base.ErrorMessageParser
import io.reactivex.rxjava3.core.Single
import java.math.BigDecimal
import javax.inject.Inject

/**
 * Retrieves trade and balance info of all supported tokens
 */
class TokenListViewModel @Inject constructor(
    private val useCase: GetBCDBalanceUseCase,
    val errorMessageParser: ErrorMessageParser
) : BaseViewModel() {
    /**
     * List of supported [Token] with balance and exchange
     */
    private val _data = MutableLiveData<DataWrapper<List<TokenForView>>>()
    val data: LiveData<DataWrapper<List<TokenForView>>> = _data

    /**
     * Pair of:
     * - Accumulated USD balance of all XTZ/Tokens in user-friendly format
     * - Currency bundle
     */
    val totalBalance = MutableLiveData<Pair<BigDecimal, CurrencyBundle>>()

    private val _balances = MutableLiveData<DataWrapper<BalancesForView>>()
    val balances: LiveData<DataWrapper<BalancesForView>> = _balances

    var tokens: List<TokenInfoBundle> = emptyList()
    var currencyBundle = Wallet.defaultCurrency

    /**
     * If no local value, we fetch remotely
     */
    fun fallbackTokens() {
        getTokens(provider = useCase.fallbackBalances())
    }

    /**
     * @param provider By default remotely
     */
    fun getTokens(
        provider: Single<List<Result<TokenInfoBundle>>> = useCase.getBalances()
    ) {
        _data.value = DataWrapper.Loading()
        _balances.value = DataWrapper.Loading()

        fetchCurrencyBundle()
        provider
            .map { parseTokens(it) }
            .subscribeForUI()
            .subscribe(
                { triple ->
                    if (triple.first.isEmpty()) {
                        _data.value = DataWrapper.Empty()
                    } else {
                        val xtz = triple.first[0]
                        val faTokens = triple.first.drop(1).filter { !it.balance.isZero }
                        val canSend = !xtz.balance.isZero || faTokens.isNotEmpty()
                        _balances.value = DataWrapper.Success(BalancesForView(xtz, faTokens, canSend))

                        _data.value = DataWrapper.Success(triple.first)
                    }

                    totalBalance.value = Pair(triple.second, fetchCurrencyBundle())
                    triple.third?.also { error ->
                        _data.value = DataWrapper.Error(error)
                        _balances.value = DataWrapper.Error(error)
                    }
                },
                {
                    _data.value = DataWrapper.Error(it)
                    _balances.value = DataWrapper.Error(it)
                }
            ).addToDisposables(disposables)
    }

    /**
     * @return List of token balances, total USD balance and an optional error if any of the retrievals have failed.
     */
    private fun parseTokens(list: List<Result<TokenInfoBundle>>): Triple<List<TokenForView>, BigDecimal, Throwable?> {
        val resultList: ArrayList<TokenInfoBundle> = ArrayList()
        val parsedList = ArrayList<TokenForView>()
        var totalUSDBalance = BigDecimal.ZERO
        var error: Throwable? = null
        for (item in list) {
            if (item.isFailure) {
                error = item.exceptionOrNull()
            }
            val tokenItem = item.getOrNull()
            tokenItem?.also {
                val inCurrency = it.balance.inCurrency(it.exchange.currencyExchange)
                resultList.add(it)
                parsedList.add(
                    createTokenForView(
                        it.showInExchange(),
                        it.token,
                        it.balance,
                        it.exchange.currencyExchange,
                        inCurrency
                    )
                )
                if (/*it.showInExchange()*/it.token.isXTZ()) {
                    totalUSDBalance += inCurrency
                }
            }
        }
        tokens = resultList
        return Triple(parsedList.sortedBy { it.token }, totalUSDBalance, error)
    }

    private fun createTokenForView(
        showInExchange: Boolean,
        token: Token,
        balance: Balance<*>,
        dollarExchange: Double,
        balanceInUSD: BigDecimal
    ): TokenForView {
        var exchange: String? = null
        var formattedCurrencyBalance: String? = null
        // Since Dexter rates are going to jump dramatically, hide the currency value to avoid confusion
        if (showInExchange && /*dollarExchange > 0.0 && */token.isXTZ()) {
            exchange = dollarExchange.formatAsMoney(currencyBundle, flexibleDecimals = true)
            formattedCurrencyBalance = balanceInUSD.formatAsMoney(currencyBundle)
        }
        return TokenForView(
            token,
            balance,
            exchange,
            formattedCurrencyBalance,
            showInExchange
        )
    }

    fun getTokenInfoBundle(token: Token): TokenInfoBundle? {
        return tokens.firstOrNull { it.token == token }
    }

    /**
     * If user has balance of only one token, it will return it
     *
     * If it has more than one or none at all, null
     */
    fun getSendToken(): TokenInfoBundle? {
        val localBalances = tokens.filter { !it.balance.isZero }
        if (localBalances.isNotEmpty() && localBalances.size == 1) {
            return localBalances[0]
        }

        return null
    }

    /**
     * When navigating to SEND, we show "Choose token" screen if:
     * - User navigated from the Wallet
     * AND
     * - Have multiple tokens in their wallet
     */
    @SendNavigation.ScreenSource
    fun getSendSource(): Int {
        val faTokens = tokens.filter { !it.balance.isZero && !it.token.isXTZ() }
        return if (faTokens.isNullOrEmpty()) {
            SendNavigation.SOURCE_MAIN_ONLY_XTZ
        } else {
            SendNavigation.SOURCE_MAIN
        }
    }

    fun fetchCurrencyBundle(): CurrencyBundle {
        val bundle = useCase.getCurrencyBundle()
        currencyBundle = bundle
        return bundle
    }
}

