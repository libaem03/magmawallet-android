/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui

import io.camlcase.smartwallet.core.architecture.BaseViewModel
import io.camlcase.smartwallet.data.model.LoginStep
import io.camlcase.smartwallet.data.model.OnboardingStep
import io.camlcase.smartwallet.data.source.local.AppPreferences
import io.camlcase.smartwallet.data.source.local.AppPreferences.Companion.AUTH_CANCELLED
import io.camlcase.smartwallet.data.source.local.UserPreferences
import io.camlcase.smartwallet.model.navigation.LaunchState
import javax.inject.Inject

class LaunchViewModel @Inject constructor(
    private val userPreferences: UserPreferences,
    private val appPreferences: AppPreferences
) : BaseViewModel() {
//    private val _data = MutableLiveData<LaunchState>()
//    val data: LiveData<LaunchState> = _data

    fun getUserLaunchState(deeplinkUri: String? = null): LaunchState {
        // Has wallet, onboarding completed
        if (activeUser()) {
            // Is not logged
            return when {
                appPreferences.getBoolean(AUTH_CANCELLED) -> LaunchState.UNLOGGED
                deeplinkUri != null -> LaunchState.DEEPLINK
                else -> LaunchState.ACTIVE
            }
        }

        // Import in process
        if (appPreferences.getLoginStep() != LoginStep.UNKNOWN) {
            return LaunchState.PENDING_LOGIN
        }

        // Onboarding is on first step
        if (appPreferences.getOnboardingStep() == OnboardingStep.CREATE_WALLET) {
            return LaunchState.FIRST_TIME
        }

        return LaunchState.PENDING_ONBOARDING
    }

    fun activeUser(): Boolean {
        return userPreferences.userLoggedIn() && appPreferences.getOnboardingStep() == OnboardingStep.COMPLETE
    }

    fun checkUserOnboardingStep() {
        if (getUserLaunchState() == LaunchState.UNLOGGED) {
            // If a user has been unlogged and clicks on create wallet, it means they want to reset their status
            // Preferences and data will be deleted during the wallet creation.
            appPreferences.storeOnboardingStep(OnboardingStep.CREATE_WALLET)
        }
    }

    fun showPairingWarning(): Boolean {
        return getUserLaunchState() == LaunchState.UNLOGGED
    }
}
