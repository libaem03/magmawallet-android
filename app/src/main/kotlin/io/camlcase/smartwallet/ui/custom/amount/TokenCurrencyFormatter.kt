/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.custom.amount

import io.camlcase.smartwallet.data.core.Logger
import java.text.NumberFormat
import java.util.*

/**
 * Given a string, parses it to a formatteable number.
 * - Calls [TokenChangeListener] with both the double value and the formatted string
 * - Calls [TokenChangeListener] with the last digit position to set the cursor there
 * - If there's numbers inside a mixed string, it will extract them and use them as number
 */
class TokenCurrencyFormatter(private val listener: TokenChangeListener) {

    fun onTextChanged(typedText: String) {
        if (typedText.isEmpty() || typedText == ZERO) {
            listener.onNewValue(0.0, "")
        } else {
            formatNumberToToken(typedText)
        }
    }

    fun formatNumberToToken(typedText: String) {
        try {
            val decimalValue = try {
                typedText.toDouble()
            } catch (e: NumberFormatException) {
                // When "Enter Max" button, the formatting is based by the Locale and it might not be parseable by Double
                val numerical = NumberFormat.getInstance(Locale.getDefault()).parse(typedText)
                numerical?.toDouble() ?: 0.0
            }

            listener.onNewValue(decimalValue, typedText)

            // CURSOR
            listener.indexOfLastDigit(typedText.length)
        } catch (e: Exception) {
            Logger.w("Error formatting $typedText : $e")
            listener.onNewValue(0.0, typedText)
        }
    }

    companion object {
        const val ZERO = "0.00000" // 6-1 zeroes
    }
}

/**
 * To connect a [TokenCurrencyFormatter] with the visual component
 */
interface TokenChangeListener {
    fun onNewValue(numerical: Double, formatted: String)
    fun getNumberLocale(): Locale
    fun indexOfLastDigit(position: Int)
}
