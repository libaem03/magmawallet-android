/**
 * # Released under MIT License
 *
 * Copyright (c) 2021 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.main.settings.network

import android.content.Context
import android.os.Bundle
import android.text.InputType
import android.view.View
import android.view.inputmethod.EditorInfo
import by.kirich1409.viewbindingdelegate.viewBinding
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.core.extension.*
import io.camlcase.smartwallet.data.model.NetworkProviders
import io.camlcase.smartwallet.databinding.FragmentNetworkConfigBinding
import io.camlcase.smartwallet.model.DataWrapper
import io.camlcase.smartwallet.model.handleResults
import io.camlcase.smartwallet.ui.base.BindBaseFragment
import io.camlcase.smartwallet.ui.base.showSnackbar
import io.camlcase.smartwallet.ui.custom.SimpleEditText

class NetworkConfigFragment : BindBaseFragment(R.layout.fragment_network_config) {
    interface NetworkConfigListener {
        fun onNetworkChange()
    }

    private val binding by viewBinding(FragmentNetworkConfigBinding::bind)
    private lateinit var viewModel: NetworkConfigViewModel
    private var listener: NetworkConfigListener? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        listener = context as? NetworkConfigListener
        if (listener == null) {
            throw ClassCastException("$context must implement NetworkConfigListener")
        }
        viewModel = viewModel(viewModelFactory) {
            observe(operation, { handleRefreshConfig(it) })
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initAppbar()
        initViews()
        bindViews()
        populateUrls(viewModel.getNetworkConfig())
        keyboardListener = activity?.listenToKeyboard { visible ->
            if (visible) {
                binding.actionDefault.hide()
            } else {
                binding.actionDefault.show()
            }
        }
    }

    private fun initViews() {
        binding.mainViewContainer.inputNode.textField()?.inputType = InputType.TYPE_TEXT_VARIATION_URI
        binding.mainViewContainer.inputNodeVerifier.textField()?.inputType = InputType.TYPE_TEXT_VARIATION_URI
        binding.mainViewContainer.inputTzkt.textField()?.inputType = InputType.TYPE_TEXT_VARIATION_URI
        binding.mainViewContainer.inputBcd.textField()?.inputType = InputType.TYPE_TEXT_VARIATION_URI
    }

    private fun initAppbar() {
        val title = getString(R.string.set_dynamic_urls_title_2)
        setupAppBar(binding.appBarContainer, title, true)
    }

    private fun bindViews() {
        binding.mainViewContainer.inputNode.textField()?.listenToChanges {
            validateTezosNodeInput(binding.mainViewContainer.inputNode, it)
        }

        binding.mainViewContainer.inputNodeVerifier.textField()?.listenToChanges {
            validateTezosNodeInput(binding.mainViewContainer.inputNodeVerifier, it)
        }

        binding.mainViewContainer.inputTzkt.textField()?.listenToChanges {
            validateInput(binding.mainViewContainer.inputTzkt, it)
        }

        binding.mainViewContainer.inputBcd.textField()?.listenToChanges {
            validateInput(binding.mainViewContainer.inputBcd, it)
        }

        binding.actionNext.throttledClick()
            .subscribe {
                hideKeyboard()
                viewModel.changeProviders(
                    binding.mainViewContainer.inputNode.text,
                    binding.mainViewContainer.inputNodeVerifier.text,
                    binding.mainViewContainer.inputTzkt.text,
                    binding.mainViewContainer.inputBcd.text,
                )
            }
            .addToDisposables(disposables)

        binding.actionDefault.throttledClick()
            .subscribe {
                populateUrls(viewModel.getDefaultNetworkConfig())
            }
            .addToDisposables(disposables)
    }

    private fun validateInput(editText: SimpleEditText, text: String) {
        if (viewModel.validUrl(text)) {
            dismissInputError(editText)
        } else {
            showInputError(editText, getString(R.string.set_dynamic_urls_invalid_url))
        }
    }

    private fun validateTezosNodeInput(editText: SimpleEditText, text: String) {
        if (viewModel.validUrl(text)) {
            if (binding.mainViewContainer.inputNode.text == binding.mainViewContainer.inputNodeVerifier.text) {
                showInputError(editText, getString(R.string.set_dynamic_urls_verifier_error))
            } else {
                dismissInputError(editText)
            }
        } else {
            showInputError(editText, getString(R.string.set_dynamic_urls_invalid_url))
        }
    }

    private fun showInputError(editText: SimpleEditText, message: String) {
        binding.actionNext.isEnabled = false
        editText.showError(message)
    }

    private fun dismissInputError(editText: SimpleEditText) {
        binding.actionNext.isEnabled = validateUrls()
        editText.dismissError()
    }

    private fun handleRefreshConfig(wrapper: DataWrapper<Boolean>) {
        wrapper.handleResults(
            onValidData = {
                hideLoading()
                listener?.onNetworkChange()
            },
            onError = {
                hideLoading()
                showSnackbar(getString(R.string.set_dynamic_urls_check_error))
            },
            onLoading = {
                showLoading()
            }
        )
    }

    private fun validateUrls(): Boolean {
        return viewModel.validateUrls(
            binding.mainViewContainer.inputNode.text,
            binding.mainViewContainer.inputNodeVerifier.text,
            binding.mainViewContainer.inputTzkt.text,
            binding.mainViewContainer.inputBcd.text,
        )
    }

    private fun populateUrls(data: NetworkProviders) {
        binding.mainViewContainer.inputNode.textField()?.apply {
            setText(data.nodeUrl)
            imeOptions = EditorInfo.IME_ACTION_NEXT
        }
        binding.mainViewContainer.inputNodeVerifier.textField()?.apply {
            setText(data.verifierNodeUrl)
            imeOptions = EditorInfo.IME_ACTION_NEXT
        }
        binding.mainViewContainer.inputTzkt.textField()?.apply {
            setText(data.tzktUrl)
            imeOptions = EditorInfo.IME_ACTION_NEXT
        }
        binding.mainViewContainer.inputBcd.textField()?.apply {
            setText(data.bcdUrl)
            imeOptions = EditorInfo.IME_ACTION_DONE
        }
    }

    override fun onStop() {
        super.onStop()
        hideKeyboard()
    }

    private fun hideKeyboard() {
        binding.mainViewContainer.inputNode.textField()?.hideKeyboard()
    }

    private fun showLoading() {
        binding.loading.containerLoading.show()
        binding.actionNext.isEnabled = false
    }

    private fun hideLoading() {
        binding.loading.containerLoading.hide()
    }
}
