/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.main.balance

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import by.kirich1409.viewbindingdelegate.viewBinding
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.core.extension.*
import io.camlcase.smartwallet.data.core.Wallet
import io.camlcase.smartwallet.data.core.extension.formatAsMoney
import io.camlcase.smartwallet.data.model.Token
import io.camlcase.smartwallet.data.model.exchange.CurrencyBundle
import io.camlcase.smartwallet.data.model.exchange.TokenInfoBundle
import io.camlcase.smartwallet.data.model.exchange.showInExchange
import io.camlcase.smartwallet.data.model.isXTZ
import io.camlcase.smartwallet.databinding.FragmentTokenDetailBinding
import io.camlcase.smartwallet.model.DataWrapper
import io.camlcase.smartwallet.model.handleResults
import io.camlcase.smartwallet.model.navigation.SendNavigation
import io.camlcase.smartwallet.model.operation.OperationForList
import io.camlcase.smartwallet.model.operation.TokenFilteredOperations
import io.camlcase.smartwallet.model.token.TokenForView
import io.camlcase.smartwallet.ui.base.BindBaseFragment
import io.camlcase.smartwallet.ui.base.navigate.AppNavigator
import io.camlcase.smartwallet.ui.base.navigate.RequestCode
import io.camlcase.smartwallet.ui.base.navigate.navigateToModal
import io.camlcase.smartwallet.ui.base.showError
import io.camlcase.smartwallet.ui.custom.AdvancedScrollOffsetListener
import io.camlcase.smartwallet.ui.main.activity.OperationListAdapter
import io.camlcase.smartwallet.ui.main.activity.OperationListViewModel
import io.camlcase.smartwallet.ui.main.balance.TokenDetailFragment.Companion.ARG_TOKEN_INFO
import io.camlcase.smartwallet.ui.main.token.TokenListViewModel
import io.camlcase.smartwallet.ui.receive.ReceiveFragment

/**
 * Show balance + operations of one specific Token
 * We pass the info via [ARG_TOKEN_INFO].
 *
 * Any successful operation done from this screen triggers [BalancesFragment]'s refresh and we are connected to
 * it via [operationsViewModel] and [viewModel]
 */
class TokenDetailFragment : BindBaseFragment(R.layout.fragment_token_detail) {
    interface TokenDetailListener {
        fun onSwapToken(token: Token)
    }

    private val binding by viewBinding(FragmentTokenDetailBinding::bind)
    private var selectedToken: TokenInfoBundle? = null
    private var currencyBundle: CurrencyBundle = Wallet.defaultCurrency
    private var listener: TokenDetailListener? = null

    private val noExchange: Boolean
        get() {
            return selectedToken?.showInExchange() == false
        }

    private lateinit var viewModel: TokenListViewModel
    private lateinit var operationsViewModel: OperationListViewModel

    override fun onAttach(context: Context) {
        super.onAttach(context)
        manageIntent(arguments)
        listener = context as? TokenDetailListener
    }

    private fun manageIntent(bundle: Bundle?) {
        bundle?.getParcelable<TokenInfoBundle>(ARG_TOKEN_INFO)?.let {
            selectedToken = it
        } ?: throw IllegalStateException("Screen needs an ARG_TOKEN_INFO argument")

        bundle.getParcelable<CurrencyBundle>(ARG_CURRENCY)?.let {
            currencyBundle = it
        }
    }

    private fun handleOperations(wrapper: DataWrapper<TokenFilteredOperations>) {
        wrapper.handleResults(
            onValidData = {
                hideLoading()
                populateData(it)
            },
            onError = {
                hideLoading()
                showError(it)
            }
        )
    }

    /**
     * We listen on changes on the general operation list. Every time there's a change, we trigger [viewModel.filterTokenOperations]
     */
    private fun handleTokensRefresh(wrapper: DataWrapper<List<OperationForList>>) {
        wrapper.handleResults(
            onValidData = {
                onOperationDataFetched()
            },
            onEmpty = {
                onOperationDataFetched()
            },
            onError = {
                hideLoading()
                showError(it)
            },
            onLoading = {
                showLoading()
            }
        )
    }

    private fun onOperationDataFetched() {
        selectedToken?.apply {
            operationsViewModel.filterTokenOperations(this)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initToolbar(selectedToken)
        if (noExchange) {
            binding.actionExchange.hide()
        }
        binding.mainViewContainer.list?.apply {
            layoutManager = LinearLayoutManager(activity)
            setHasFixedSize(true)
        }
        bindViews()

        // Share view model with other screens
        baseActivity?.apply {
            operationsViewModel = viewModel(viewModelFactory) {}
            viewModel = this.viewModel(viewModelFactory) {}

            // Cannot use extension since LiveData calls immediately to viewModel when it's still uninitialized
            viewModel.data.observe(viewLifecycleOwner, Observer { handleTokenBalance(it) })
            operationsViewModel.data.observe(viewLifecycleOwner, Observer { handleTokensRefresh(it) })
            operationsViewModel.tokenOperations.observe(viewLifecycleOwner, Observer { handleOperations(it) })
        }
    }

    private fun handleTokenBalance(wrapper: DataWrapper<List<TokenForView>>) {
        wrapper.handleResults(
            onValidData = { list ->
                hideLoading()
                list.firstOrNull { it.token == selectedToken?.token }?.apply {
                    populateBalanceHeader(this)
                }
            },
            onError = {
                showError(it)
            }
        )
    }

    private fun populateData(filteredInfo: TokenFilteredOperations) {
        if (filteredInfo.operations.isNullOrEmpty()) {
            binding.mainViewContainer.emptyStatus.show()
            binding.mainViewContainer.list.hide()
        } else {
            binding.mainViewContainer.emptyStatus.hide()
            binding.mainViewContainer.list.show()
            binding.mainViewContainer.list.adapter = OperationListAdapter(filteredInfo.operations) { operation, view ->
                view.setOnClickListener {
                    AppNavigator.openOperationExplorer(activity, operation.hash)
                }
            }
        }
    }

    private fun initToolbar(info: TokenInfoBundle?) {
        setupToolbar(binding.toolbar, true)
        info?.apply {
            binding.tokenTitle.text = this.token.name
            populateBalanceHeader(this)

            binding.appBar.addOnOffsetChangedListener(
                AdvancedScrollOffsetListener { _, state ->
                    when (state) {
                        AdvancedScrollOffsetListener.State.EXPANDED -> {
                            binding.expandedTitle.root.show()
                            binding.actionReceive.show()
                            binding.actionSend.show()
                            if (!noExchange) {
                                binding.actionExchange.show()
                            }
                        }
                        AdvancedScrollOffsetListener.State.COLLAPSED -> {
                            binding.expandedTitle.root.hide()
                            binding.actionReceive.invisible()
                            binding.actionSend.invisible()
                            if (!noExchange) {
                                binding.actionExchange.invisible()
                            }
                        }
                        AdvancedScrollOffsetListener.State.IDLE -> {
                        }
                    }
                }
            )
        }
    }

    private fun populateBalanceHeader(info: TokenInfoBundle) {
        binding.expandedTitle.tokenBalance.text = info.balance.formattedValue
        binding.expandedTitle.tokenSymbol.text = info.token.symbol
        if (/*info.showInExchange()*/info.token.isXTZ()) {
            binding.expandedTitle.tokenBalanceCurrency.show()
            binding.expandedTitle.tokenBalanceCurrency.text =
                info.balance.inCurrency(info.exchange.currencyExchange).formatAsMoney(currencyBundle)
            binding.expandedTitle.currencyExchange.show()
            binding.expandedTitle.currencyExchange.text = info.exchange.currencyExchange.formatAsMoney(currencyBundle)
        }

        binding.actionSend.isEnabled = !info.balance.isZero
    }

    private fun populateBalanceHeader(info: TokenForView) {
        binding.expandedTitle.tokenBalance.text = info.balance.formattedValue
        binding.expandedTitle.tokenSymbol.text = info.token.symbol
        if (/*info.showExchange*/info.token.isXTZ()) {
            binding.expandedTitle.tokenBalanceCurrency.show()
            binding.expandedTitle.tokenBalanceCurrency.text = info.formattedCurrencyBalance
            binding.expandedTitle.currencyExchange.show()
            binding.expandedTitle.currencyExchange.text = info.exchange
        }
        binding.actionSend.isEnabled = !info.balance.isZero
    }

    private fun bindViews() {
        binding.actionExchange.throttledClick()
            .subscribe {
                selectedToken?.apply {
                    activity?.onBackPressed()
                    listener?.onSwapToken(this.token)
                }
            }
            .addToDisposables(disposables)

        binding.actionReceive.throttledClick()
            .subscribe {
                parentFragmentManager.navigateToModal(
                    ReceiveFragment.init(selectedToken?.token?.symbol),
                    this,
                    RequestCode.RECEIVE
                )
            }
            .addToDisposables(disposables)

        binding.actionSend.throttledClick()
            .subscribe { AppNavigator.navigateToSend(activity, SendNavigation.SOURCE_TOKEN, selectedToken) }
            .addToDisposables(disposables)
    }

    private fun showLoading() {
        binding.loading.root.show()
        binding.actionExchange.isEnabled = false
        binding.actionReceive.isEnabled = false
        binding.actionSend.isEnabled = false
    }

    private fun hideLoading() {
        binding.loading.root.hide()
        binding.actionExchange.isEnabled = true
        binding.actionSend.isEnabled = true
    }

    companion object {
        const val ARG_TOKEN_INFO = "arg.token.info"
        const val ARG_CURRENCY = "arg.currency"

        fun init(arg: TokenInfoBundle, currency: CurrencyBundle): TokenDetailFragment {
            val fragment = TokenDetailFragment()
            val args = Bundle()
            args.putParcelable(ARG_TOKEN_INFO, arg)
            args.putParcelable(ARG_CURRENCY, currency)
            fragment.arguments = args
            return fragment
        }
    }
}
