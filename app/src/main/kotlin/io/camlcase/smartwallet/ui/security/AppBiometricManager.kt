/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.security

import android.content.Context
import android.view.View
import androidx.biometric.BiometricManager
import androidx.biometric.BiometricPrompt
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.core.extension.canShow
import io.camlcase.smartwallet.data.core.Logger

/**
 * @return Code:
 *  BiometricManager.BIOMETRIC_SUCCESS -> App can authenticate using biometrics.
 *  BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE -> No biometric features available on this device.
 *  BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE -> Biometric features are currently unavailable.
 *  BiometricManager.BIOMETRIC_ERROR_NONE_ENROLLED -> The user hasn't associated any biometric credentials with their account.
 *
 *  [See docs](https://developer.android.com/training/sign-in/biometric-auth)
 */
internal fun Context?.usesBiometric(): Int {
    return this?.let {
        val manager = BiometricManager.from(it)
        return manager.canAuthenticate(BiometricManager.Authenticators.BIOMETRIC_WEAK)
    } ?: BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE
}

internal fun Context.createBiometricPromptDialog(): BiometricPrompt.PromptInfo {
    return BiometricPrompt.PromptInfo.Builder()
        .setTitle(this.getString(R.string.android_biometric_prompt_android_title))
        .setDescription(this.getString(R.string.android_biometric_prompt_description))
        .setConfirmationRequired(false)
        // Can't call setNegativeButtonText() and
        // setAllowedAuthenticators(... or DEVICE_CREDENTIAL) at the same time.
        .setNegativeButtonText(this.getString(R.string.cancel))
        .build()
}

/**
 * [See docs](https://developer.android.com/training/sign-in/biometric-auth)
 *
 * @param onAuthSuccess A biometric is recognized.
 * @param onAuthError An error occurred
 * @param onAuthFailed A biometric is valid but not recognized.
 */
internal fun Fragment?.initBiometrics(
    onAuthSuccess: (result: BiometricPrompt.AuthenticationResult) -> Unit,
    onAuthError: (code: Int, message: CharSequence) -> Unit,
    onAuthFailed: () -> Unit = {}
): Boolean {
    this?.apply {
        if (activity.usesBiometric() == BiometricManager.BIOMETRIC_SUCCESS) {
            activity?.let {
                val executor = ContextCompat.getMainExecutor(it)
                val biometricPrompt =
                    BiometricPrompt(this, executor, promptCallback(onAuthSuccess, onAuthError, onAuthFailed))

                this.view?.postDelayed({
                    if (!this.isRemoving) {
                        biometricPrompt.authenticate(it.createBiometricPromptDialog())
                    }
                }, 50)
                return true
            }
        }
    }
    return false
}

/**
 * [See docs](https://developer.android.com/training/sign-in/biometric-auth)
 *
 * @param onAuthSucces A biometric is recognized.
 * @param onAuthError An error occurred
 * @param onAuthFailed A biometric is valid but not recognized.
 */
internal fun FragmentActivity?.initBiometrics(
    view: View,
    onAuthSuccess: (result: BiometricPrompt.AuthenticationResult) -> Unit,
    onAuthError: (code: Int, message: CharSequence) -> Unit,
    onAuthFailed: () -> Unit = {}
): Boolean {
    this?.apply {
        if (usesBiometric() == BiometricManager.BIOMETRIC_SUCCESS) {
            val executor = ContextCompat.getMainExecutor(this)
            val biometricPrompt =
                BiometricPrompt(this, executor, promptCallback(onAuthSuccess, onAuthError, onAuthFailed))

            view.postDelayed({
                if (this.canShow()) {
                    biometricPrompt.authenticate(createBiometricPromptDialog())
                }
            }, 50)
            return true
        }
    }
    return false
}

private fun promptCallback(
    onAuthSuccess: (result: BiometricPrompt.AuthenticationResult) -> Unit,
    onAuthError: (code: Int, message: CharSequence) -> Unit,
    onAuthFailed: () -> Unit = {}
): BiometricPrompt.AuthenticationCallback {
    return object : BiometricPrompt.AuthenticationCallback() {
        override fun onAuthenticationError(errorCode: Int, errString: CharSequence) {
            super.onAuthenticationError(errorCode, errString)
            Logger.e("Authentication error: $errString")
            onAuthError(errorCode, errString)
        }

        override fun onAuthenticationSucceeded(result: BiometricPrompt.AuthenticationResult) {
            super.onAuthenticationSucceeded(result)
            Logger.d("BIOMETRIC Authentication succeeded!")
            onAuthSuccess(result)
        }

        override fun onAuthenticationFailed() {
            super.onAuthenticationFailed()
            Logger.w("BIOMETRIC Authentication failed")
            onAuthFailed()
        }
    }
}

fun Int.userCancelledBiometricPrompt(): Boolean {
    return this == BiometricPrompt.ERROR_USER_CANCELED || this == BiometricPrompt.ERROR_NEGATIVE_BUTTON
}

