/**
 * # Released under MIT License
 *
 * Copyright (c) 2021 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.main.settings.network

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import io.camlcase.smartwallet.core.architecture.BaseViewModel
import io.camlcase.smartwallet.core.extension.addToDisposables
import io.camlcase.smartwallet.core.extension.subscribeForUI
import io.camlcase.smartwallet.data.model.NetworkProviders
import io.camlcase.smartwallet.data.usecase.info.GetNetworkConfigUseCase
import io.camlcase.smartwallet.model.DataWrapper
import io.reactivex.rxjava3.core.Single
import java.util.regex.Pattern
import javax.inject.Inject

class NetworkConfigViewModel @Inject constructor(
    private val useCase: GetNetworkConfigUseCase
) : BaseViewModel() {

    private val _operation: MutableLiveData<DataWrapper<Boolean>> = MutableLiveData()
    val operation: LiveData<DataWrapper<Boolean>> = _operation

    private var data: NetworkProviders? = null
    private val webUrlPattern = Pattern.compile(URL_REGEXP)

    fun getNetworkConfig(): NetworkProviders {
        return useCase.getNetworkConfig().also {
            data = it
        }
    }

    fun getDefaultNetworkConfig(): NetworkProviders {
        return useCase.getDefaultNetworkConfig()
    }

    /**
     * - Do a sanity check on the urls
     * - Save the new urls
     * - (After) Reset app
     */
    fun changeProviders(
        nodeUrl: String,
        verifierNodeUrl: String,
        tzktUrl: String,
        bcdUrl: String
    ) {
        _operation.value = DataWrapper.Loading()
        Single.fromCallable {
            NetworkProviders(nodeUrl, verifierNodeUrl, tzktUrl, bcdUrl)
        }
            .flatMap({ useCase.checkUrls(it) }, { providers, _ -> providers })
            .map { useCase.setNetworkConfig(it) }
            .subscribeForUI()
            .subscribe(
                {
                    _operation.value = DataWrapper.Success(true)
                },
                {
                    _operation.value = DataWrapper.Error(it)
                }
            ).addToDisposables(disposables)
    }

    fun validUrl(url: String): Boolean {
        return webUrlPattern.matcher(url).matches()
    }

    /**
     * URL are valid if:
     * - If any of them are different from the ones already set in place
     * - Verifier != main node url
     * - Match an url pattern
     */
    fun validateUrls(nodeUrl: String, verifierNodeUrl: String, tzktUrl: String, bcdUrl: String): Boolean {
        val changed = nodeUrl != data?.nodeUrl
                || verifierNodeUrl != data?.verifierNodeUrl
                || tzktUrl != data?.tzktUrl
                || bcdUrl != data?.bcdUrl
        val validUrls = validUrl(nodeUrl)
                && validUrl(verifierNodeUrl)
                && validUrl(tzktUrl)
                && validUrl(bcdUrl)
        return changed && validUrls && nodeUrl != verifierNodeUrl
    }

    companion object {
        /**
         * - Forces https (Magma doesn't let you make http connections)
         */
        private const val URL_REGEXP =
            "((https:\\/\\/)[-a-zA-Z0-9:@;?&=\\/%\\+\\.\\*!'\\(\\),\\\$_\\{\\}\\^~\\[\\]`#|]+)"
    }
}
