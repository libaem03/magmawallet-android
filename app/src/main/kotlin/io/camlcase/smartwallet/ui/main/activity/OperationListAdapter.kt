/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.main.activity

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import io.camlcase.kotlintezos.model.Address
import io.camlcase.kotlintezos.model.OperationResultStatus
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.core.extension.hide
import io.camlcase.smartwallet.core.extension.show
import io.camlcase.smartwallet.data.model.operation.TezosOperation
import io.camlcase.smartwallet.model.operation.DateHeader
import io.camlcase.smartwallet.model.operation.OperationForList
import io.camlcase.smartwallet.model.operation.OperationForView
import kotlinx.android.synthetic.main.row_operation.view.*
import kotlinx.android.synthetic.main.row_operation_header.view.*

class OperationListAdapter(
    private val values: List<OperationForList>,
    private val onItemClick: (OperationForView, View) -> Unit
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun getItemCount(): Int {
        return values.size
    }

    inner class HeaderViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val header: TextView = view.header
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val errorIcon: ImageView = view.icon_error
        val type: TextView = view.operation_type
        val detail: TextView = view.operation_detail
        val amount: TextView = view.operation_amount
        val amountExtra: TextView = view.operation_amount_extra
        val fees: TextView = view.operation_fees
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            HEADER -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.row_operation_header, parent, false)
                HeaderViewHolder(view)
            }
            else -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.row_operation, parent, false)
                ViewHolder(view)
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (values[position]) {
            is DateHeader -> HEADER
            else -> OPERATION
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (val item = values[position]) {
            is DateHeader -> {
                bindHeader(holder as HeaderViewHolder, item)
            }
            else -> bindOperation(holder as ViewHolder, item as OperationForView)
        }
    }

    private fun bindOperation(holder: ViewHolder, item: OperationForView) {
        val context = holder.itemView.context

        val failed = item.status != OperationResultStatus.APPLIED
        holder.type.text = getTitle(context, item, failed)
        if (failed) {
            holder.errorIcon.show()
            holder.amount.text = context.getString(R.string.act_empty)
        } else {
            holder.amount.text = item.formattedAmount ?: context.getString(R.string.act_empty)
            if (item.formattedAmountExtra != null) {
                holder.amountExtra.show()
                holder.amountExtra.text = item.formattedAmountExtra
            } else {
                holder.amountExtra.hide()
            }
            holder.errorIcon.hide()
        }

        getDetail(context, item.type, item.source, item.destination, item.detail)?.let {
            holder.detail.show()
            holder.detail.text = it
        } ?: holder.detail.hide()

        holder.fees.text =
            context.getString(R.string.act_network_fee, item.formattedFees ?: context.getString(R.string.act_empty))

        onItemClick(item, holder.itemView)
    }

    private fun bindHeader(holder: HeaderViewHolder, item: DateHeader) {
        holder.header.text = item.formattedDate
    }

    private fun getTitle(context: Context, item: OperationForView, failed: Boolean): String {
        val typeTitle = when (item.type) {
            TezosOperation.Type.SEND -> context.getString(R.string.act_type_send)
            TezosOperation.Type.RECEIVE -> context.getString(R.string.act_type_receive)
            TezosOperation.Type.SWAP -> context.getString(R.string.act_type_exchange)
            TezosOperation.Type.DELEGATE -> {
                if (item.destination.isBlank()) {
                    context.getString(R.string.act_type_delegation_remove)
                } else {
                    context.getString(R.string.act_type_delegation)
                }
            }
            TezosOperation.Type.ORIGINATION -> context.getString(R.string.act_type_origination)
            TezosOperation.Type.REVEAL -> context.getString(R.string.act_type_reveal)
            TezosOperation.Type.UNKNOWN -> context.getString(R.string.act_type_unknown)
        }
        return if (failed) {
            context.getString(R.string.act_type_failed, typeTitle)
        } else {
            typeTitle
        }
    }

    private fun getDetail(
        context: Context,
        type: TezosOperation.Type,
        source: Address,
        destination: Address,
        detail: String?
    ): String? {
        when (type) {
            TezosOperation.Type.SEND -> {
                return context.getString(R.string.act_destination_to, destination)
            }
            TezosOperation.Type.DELEGATE -> {
                return if (destination.isBlank()) {
                    detail
                } else {
                    context.getString(R.string.act_destination_to, destination)
                }
            }
            TezosOperation.Type.RECEIVE -> {
                return context.getString(R.string.act_destination_from, source)
            }
            TezosOperation.Type.REVEAL -> {
                return context.getString(R.string.act_from_my_wallet)
            }
            TezosOperation.Type.SWAP -> {
                return "$detail\n${context.getString(R.string.act_liquidity_fee)}"
            }
            TezosOperation.Type.ORIGINATION -> {
                return context.getString(R.string.act_destination_contract, destination)
            }
            TezosOperation.Type.UNKNOWN -> {
                return detail
            }
        }
    }

    companion object {
        private const val HEADER = 1
        private const val OPERATION = 2
    }
}
