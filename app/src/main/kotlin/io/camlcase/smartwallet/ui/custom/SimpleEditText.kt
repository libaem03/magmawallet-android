/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.custom

import android.content.Context
import android.text.InputFilter
import android.text.InputFilter.LengthFilter
import android.util.AttributeSet
import android.util.TypedValue
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.core.extension.hide
import io.camlcase.smartwallet.core.extension.show
import io.camlcase.smartwallet.databinding.ViewInputBinding

/**
 * An TextInputEditText that follows design guidelines
 */
class SimpleEditText(context: Context, attrs: AttributeSet?) : LinearLayout(context, attrs) {

    private val binding = ViewInputBinding.inflate(LayoutInflater.from(context), this, true)

    val text: String
        get() {
            return binding.input.text.toString()
        }

    val inputLayout: TextInputLayout
        get() = binding.inputLayout

    fun textField(): TextInputEditText? = binding.input

    /**
     * Change programmatically for white modals
     */
    fun initForModal() {
        binding.inputError.setCompoundDrawablesWithIntrinsicBounds(
            ContextCompat.getDrawable(context, R.drawable.ic_error_modal),
            null,
            null,
            null
        )
    }

    fun showError(message: String?) {
        binding.inputContainer.setBackgroundResource(R.drawable.light_alpha_border_background)
        message?.apply {
            binding.inputError.show()
            binding.inputError.text = message
        }
    }

    fun dismissError() {
        binding.inputContainer.setBackgroundResource(R.drawable.light_alpha_background)
        binding.inputError.hide()
        binding.inputError.text = null
    }

    fun setMaxLength(length: Int) {
        val filters = arrayOfNulls<InputFilter>(1)
        filters[0] = LengthFilter(length)
        binding.input.filters = filters
    }

    fun setAsPasswordToggle() {
        inputLayout.endIconMode = TextInputLayout.END_ICON_PASSWORD_TOGGLE
        val typedValue = TypedValue()
        context.theme.resolveAttribute(R.attr.disabledPrimary, typedValue, true)
        inputLayout.setEndIconTintList(ContextCompat.getColorStateList(context, typedValue.resourceId))
        inputLayout.endIconContentDescription = context.getString(R.string.icon_password)
    }
}
