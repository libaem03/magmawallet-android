/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.delegate

import android.graphics.Paint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.button.MaterialButton
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.core.extension.ellipsize
import io.camlcase.smartwallet.core.extension.hide
import io.camlcase.smartwallet.core.extension.show
import io.camlcase.smartwallet.core.extension.showImage
import kotlinx.android.synthetic.main.row_delegate.view.*

class DelegateListAdapter(
    private val values: List<DelegateForView>,
    private val onItemClick: (DelegateForView, View) -> Unit
) : RecyclerView.Adapter<DelegateListAdapter.ViewHolder>() {
    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val image: ImageView = view.baker_logo
        val name: TextView = view.baker_name
        val detail: TextView = view.baker_address
        val removeAction: MaterialButton = view.action_remove
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.row_delegate, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return values.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = values[position]

        holder.image.showImage(item.imageUrl, R.drawable.circle_blue_bg)

        item.name?.let {
            holder.name.show()
            holder.name.text = item.name
        } ?: holder.name.hide()

        holder.detail.text = item.address.ellipsize()

        if (item.removable) {
            holder.removeAction.show()
            holder.removeAction.paintFlags = holder.removeAction.paintFlags or Paint.UNDERLINE_TEXT_FLAG
        } else {
            holder.removeAction.hide()
        }

        onItemClick(item, holder.itemView)
    }
}
