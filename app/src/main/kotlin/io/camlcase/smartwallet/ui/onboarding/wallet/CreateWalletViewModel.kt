/**
 * # Released under MIT License
 *
 * Copyright (c) 2021 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.onboarding.wallet

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import io.camlcase.smartwallet.core.architecture.BaseViewModel
import io.camlcase.smartwallet.core.extension.addToDisposables
import io.camlcase.smartwallet.core.extension.subscribeForUI
import io.camlcase.smartwallet.data.analytic.report
import io.camlcase.smartwallet.data.model.AppWallet
import io.camlcase.smartwallet.data.model.valid
import io.camlcase.smartwallet.data.usecase.analytics.AnalyticEvent
import io.camlcase.smartwallet.data.usecase.analytics.SendAnalyticUseCase
import io.camlcase.smartwallet.data.usecase.analytics.StateEvent
import io.camlcase.smartwallet.data.usecase.user.CreateWalletUseCase
import io.camlcase.smartwallet.model.DataWrapper
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

class CreateWalletViewModel @Inject constructor(
    private val useCase: CreateWalletUseCase,
    private val analytic: SendAnalyticUseCase
) : BaseViewModel() {
    private val _data: MutableLiveData<DataWrapper<String>> = MutableLiveData()
    val data: LiveData<DataWrapper<String>> = _data

    fun createWallet(passphrase: String? = null) {
        _data.value = DataWrapper.Loading()

        var sanitisedPassphrase: String? = if (passphrase.isNullOrBlank()) {
            null
        } else {
            analytic.reportState(StateEvent.ONB_WALLET_PASSPHRASE)
            passphrase.trim()
        }

        Single.fromCallable { useCase.createNewWallet(sanitisedPassphrase) }
            .map {
                sanitisedPassphrase = null
                it
            }
            .subscribeForUI()
            .subscribe(
                { onWalletCreated(it) },
                {
                    it.report()
                    handleError(it)
                }
            ).addToDisposables(disposables)
    }

    private fun onWalletCreated(appWallet: AppWallet) {
        if (appWallet.valid()) {
            analytic.reportEvent(AnalyticEvent.WALLET_CREATED)
            _data.value = DataWrapper.Success(appWallet.address)
        } else {
            handleError(invalidWalletError)
        }
    }

    private fun handleError(error: Throwable?) {
        _data.value = DataWrapper.Error(error, _data.value?.data)
    }

    companion object {
        internal val invalidWalletError = IllegalStateException("Error creating wallet")
    }
}
