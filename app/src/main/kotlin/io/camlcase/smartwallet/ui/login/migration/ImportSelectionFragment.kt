/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.login.migration

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.core.content.res.ResourcesCompat
import by.kirich1409.viewbindingdelegate.viewBinding
import io.camlcase.smartwallet.BuildConfig
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.core.extension.reportView
import io.camlcase.smartwallet.core.extension.addToDisposables
import io.camlcase.smartwallet.core.extension.setupAppBar
import io.camlcase.smartwallet.core.extension.throttledClick
import io.camlcase.smartwallet.core.extension.viewModel
import io.camlcase.smartwallet.data.usecase.analytics.ScreenEvent
import io.camlcase.smartwallet.data.usecase.analytics.StateEvent
import io.camlcase.smartwallet.databinding.FragmentImportSelectionBinding
import io.camlcase.smartwallet.ui.base.BindBaseFragment
import io.camlcase.smartwallet.ui.login.balance.ImportBalanceMenuFragment
import io.camlcase.smartwallet.ui.migration.DismissMigrationViewModel

/**
 * Import Migration or NOT (For SD wallets)
 */
class ImportSelectionFragment : BindBaseFragment(R.layout.fragment_import_selection) {
    interface LoginSelectionListener {
        fun onMigrationStatusSaved()
    }

    private val binding by viewBinding(FragmentImportSelectionBinding::bind)
    private var listener: LoginSelectionListener? = null
    private lateinit var viewModel: DismissMigrationViewModel

    override fun onAttach(context: Context) {
        super.onAttach(context)
        reportView(ScreenEvent.ONB_RECOVERY_MENU_NONHD)
        listener = context as? LoginSelectionListener
        viewModel = viewModel(viewModelFactory) {}
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initAppbar()
        binding.regularImport.contactName.setText(R.string.onb_import_selection_regular)
        context?.apply {
            binding.regularImport.contactName.typeface = ResourcesCompat.getFont(this, R.font.barlow_bold)
        }
        bindViews()
    }

    private fun bindViews() {
        binding.regularImport.root.throttledClick()
            .subscribe {
                analyticsUseCase.get()?.reportState(StateEvent.ONB_RECOVERY_NONHD_REGULAR)
                viewModel.updateVersionCreated(BuildConfig.VERSION_CODE)
                listener?.onMigrationStatusSaved()
            }
            .addToDisposables(disposables)

        binding.migrationImport.onClick()
            .subscribe {
                analyticsUseCase.get()?.reportState(StateEvent.ONB_RECOVERY_NONHD_MIGRATION)
                listener?.onMigrationStatusSaved()
            }
            .addToDisposables(disposables)
    }

    private fun initAppbar() {
        val backNavigation = arguments?.getBoolean(ARG_BACK_NAVIGATION) == true
        val title = getString(R.string.onb_import_seed_phrase_title)
        setupAppBar(binding.appBarContainer, title, backNavigation)
    }

    companion object {
        private const val ARG_BACK_NAVIGATION = "args.navigation"

        /**
         * @param backNavigation True if it's part of the import flow, false if user starts session directly to this screen.
         */
        fun init(backNavigation: Boolean): ImportSelectionFragment {
            val fragment = ImportSelectionFragment()
            val args = Bundle()
            args.putBoolean(ARG_BACK_NAVIGATION, backNavigation)
            fragment.arguments = args
            return fragment
        }
    }
}
