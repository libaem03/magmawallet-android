/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.delegate

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import io.camlcase.kotlintezos.model.Address
import io.camlcase.kotlintezos.model.Tez
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.core.extension.*
import io.camlcase.smartwallet.data.core.Logger
import io.camlcase.smartwallet.data.model.delegate.TezosBakerResponse
import io.camlcase.smartwallet.data.model.error.*
import io.camlcase.smartwallet.model.navigation.MainNavigation
import io.camlcase.smartwallet.model.operation.finishWithBundleResult
import io.camlcase.smartwallet.ui.base.BaseActivity
import io.camlcase.smartwallet.ui.base.info.InfoFragment
import io.camlcase.smartwallet.ui.base.info.InfoType
import io.camlcase.smartwallet.ui.base.info.InfoView
import io.camlcase.smartwallet.ui.base.navigate.AppNavigator
import io.camlcase.smartwallet.ui.main.MainActivity
import io.camlcase.smartwallet.ui.qr.QRScanType

/**
 * Delegate to baker flow
 *
 * @see DelegateFragment
 * @see DelegateListFragment
 */
class DelegateActivity : BaseActivity(R.layout.activity_delegate),
    DelegateFragment.DelegateListListener,
    DelegateListFragment.ChooseDelegateListener,
    InfoView.InfoEventsListener,
    MinimalDelegationWarningFragment.MinimalDelegationListener {

    private var bakerAddress: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        listenToBackground()
        if (savedInstanceState == null) {
            replaceFragmentSafely(DelegateFragment(), R.id.container_fragment)
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putString(ARG_BAKER_ADDRESS, bakerAddress)
        super.onSaveInstanceState(outState)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        bakerAddress = savedInstanceState.getString(ARG_BAKER_ADDRESS)
        super.onRestoreInstanceState(savedInstanceState)
    }

    override fun scanQR() {
        AppNavigator.navigateToScanQR(this, QRScanType.DELEGATE)
    }

    override fun onUndelegate() {
        communicateWithFragment<MainToDelegate> { it.onUndelegation() }
    }

    override fun onBakerSelected(baker: TezosBakerResponse) {
        communicateWithFragment<MainToDelegate> { it.onBakerSelected(baker) }
    }

    override fun onDelegateSuccess(delegateAddress: String, operationHash: String) {
        bakerAddress = delegateAddress
        finishWithBundleResult(
            IncludedOperation.DELEGATE,
            operationHash,
            ""
        )
    }

    override fun onUndelegateSuccess(operationHash: String) {
        bakerAddress = null
        finishWithBundleResult(
            IncludedOperation.UNDELEGATE,
            operationHash,
            ""
        )
    }

    override fun onMinimalAmountWarning(delegateAddress: String, minAmount: Tez, xtzBalance: Tez) {
        displayFragment(
            MinimalDelegationWarningFragment.init(delegateAddress, minAmount, xtzBalance),
            R.id.container_fragment
        )
    }

    override fun onOperationError(error: AppError) {
        val type = when (error.exception) {
            is InsufficientXTZ -> {
                InfoType.SEND_ERROR_INSUFFICIENT_XTZ
            }
            is InvalidAddressError -> {
                InfoType.DELEGATE_ERROR_INVALID_ADDRESS
            }
            is OperationInProgress -> {
                InfoType.OPERATION_COUNTER
            }
            is BakerCantDelegate -> {
                InfoType.DELEGATE_ERROR_BAKER_CANT_DELEGATE
            }
            is UnchangedDelegate -> {
                InfoType.DELEGATE_ERROR_UNCHANGED_DELEGATED
            }
            else -> {
                InfoType.DELEGATE_INCLUSION_ERROR
            }
        }
        popFullBackstack()
        loadFragment(InfoFragment.init(type), R.id.container_fragment)
    }

    override fun onActionClick(infoType: InfoType) {
        when (infoType) {
            InfoType.DELEGATE_INCLUSION_ERROR,
            InfoType.OPERATION_COUNTER,
            InfoType.DELEGATE_ERROR_INVALID_ADDRESS,
            InfoType.DELEGATE_ERROR_BAKER_CANT_DELEGATE,
            InfoType.DELEGATE_ERROR_UNCHANGED_DELEGATED -> {
                // Inclusion failed "Try again"
                bakerAddress = null
                popFullBackstack()
                replaceFragmentSafely(DelegateFragment(), R.id.container_fragment)
            }
            InfoType.SEND_ERROR_INSUFFICIENT_XTZ -> {
                // Go back to Wallet
                returnToWallet()
            }
            else -> {
                Logger.i("InfoFragment of type not supported $infoType")
            }
        }
    }

    private fun returnToWallet() {
        val intent = Intent()
        intent.putExtra(MainActivity.ARG_OPERATION_TAB, MainNavigation.TAB_WALLET)
        setResult(Activity.RESULT_CANCELED, intent)
        finishAfterTransition()
    }

    /**
     * Minimal Amount Warning: User choose to delegate anyways
     */
    override fun onMinAmounContinue(bakerAddress: Address) {
        onBackPressed()
        communicateWithFragment<MainToDelegate> { it.forceDelegate(bakerAddress) }
    }

    companion object {
        private const val ARG_BAKER_ADDRESS = "arg.address.baker"

        fun init(context: Context): Intent {
            return Intent(context, DelegateActivity::class.java)
        }
    }
}
