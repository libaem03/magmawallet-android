/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.migration

import android.content.Context
import android.graphics.Color
import android.graphics.Paint
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.text.SpannableString
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.core.extension.*
import io.camlcase.smartwallet.ui.base.BaseDialogFragment
import io.reactivex.rxjava3.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.fragment_migration_warning.*
import kotlinx.android.synthetic.main.view_informative_option.view.*
import kotlinx.android.synthetic.main.view_informative_option.view.title
import kotlinx.android.synthetic.main.view_migration_warning_info.view.*
import kotlinx.android.synthetic.main.view_migration_warning_options.view.*
import kotlinx.android.synthetic.main.view_migration_warning_options.view.options_skip_temporary

/**
 * Informs the user about the migration cause
 */
class MigrationWarningFragment : BaseDialogFragment() {
    interface MigrationWarningListener {
        fun goToMigration()
        fun dismissMigrationWarningPermanently()
    }

    var disposables: CompositeDisposable = CompositeDisposable()
    private var listener: MigrationWarningListener? = null
    private val viewModel = MigrationWarningViewModel()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        return inflater.inflate(R.layout.fragment_migration_warning, container, false)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        listener = context as? MigrationWarningListener
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val width = (resources.displayMetrics.widthPixels * 0.85).toInt()
        dialog?.window?.setLayout(width, ViewGroup.LayoutParams.WRAP_CONTENT)
        dialog?.setCancelable(false)

        initInfoViews()
        initOptionsViews()
        bindViews()
        viewModel.sendPopupEvent()
    }

    private fun initInfoViews() {
        switcher.action_dismiss.paintFlags = switcher.action_dismiss.paintFlags or Paint.UNDERLINE_TEXT_FLAG
        val boldSection = getString(R.string.mig_warning_bold)
        val fullText = switcher.warning_message.text.toString()

        val spannable = context.spanFontType(SpannableString(fullText), R.font.barlow_bold, boldSection)
        switcher.warning_message.text = spannable
    }

    private fun initOptionsViews() {
        switcher.action_back.paintFlags = switcher.action_dismiss.paintFlags or Paint.UNDERLINE_TEXT_FLAG
        switcher.action_back.text = "< ${switcher.action_back.text}"

        switcher.options_skip_temporary.title.setText(R.string.mig_expert_options_skip_title)
        switcher.options_skip_temporary.message.setText(R.string.mig_expert_options_skip_message)
        switcher.options_skip_temporary.message.setTextSize(
            TypedValue.COMPLEX_UNIT_PX,
            resources.getDimension(R.dimen.text_size_subtitle2)
        )
        // Warning icons
        val warningIcon = String(Character.toChars(0x26A0))
        switcher.options_skip_temporary.extra.text = "$warningIcon ${getString(R.string.mig_expert_options_risk)}"

        switcher.options_skip_permanent.title.setText(R.string.mig_expert_options_never_title)
        switcher.options_skip_permanent.message.setText(R.string.mig_expert_options_never_message)
        switcher.options_skip_permanent.message.setTextSize(
            TypedValue.COMPLEX_UNIT_PX,
            resources.getDimension(R.dimen.text_size_subtitle2)
        )
        switcher.options_skip_permanent.extra.hide()
    }

    private fun bindViews() {
        switcher.action_migrate.throttledClick()
            .subscribe {
                dismiss()
                listener?.goToMigration()
            }
            .addToDisposables(disposables)

        switcher.action_dismiss.throttledClick()
            .subscribe {
                icon.hide()
                switcher.showNext()
            }
            .addToDisposables(disposables)

        switcher.action_back.throttledClick()
            .subscribe {
                icon.show()
                switcher.showPrevious()
            }
            .addToDisposables(disposables)

        switcher.options_skip_temporary.throttledClick()
            .subscribe {
                viewModel.sendPopupSkipEvent()
                dismiss()
            }
            .addToDisposables(disposables)

        switcher.options_skip_permanent.throttledClick()
            .subscribe {
                viewModel.sendPopupSkipPermanentEvent()
                listener?.dismissMigrationWarningPermanently()
                dismiss()
            }
            .addToDisposables(disposables)
    }

    override fun onDestroyView() {
        disposables.dispose()
        super.onDestroyView()
    }
}
