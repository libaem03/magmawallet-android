/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.send.confirmation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import io.camlcase.kotlintezos.model.Tez
import io.camlcase.kotlintezos.model.TezosError
import io.camlcase.kotlintezos.model.operation.Operation
import io.camlcase.kotlintezos.model.operation.fees.OperationFees
import io.camlcase.smartwallet.core.architecture.BaseViewModel
import io.camlcase.smartwallet.core.extension.addToDisposables
import io.camlcase.smartwallet.core.extension.subscribeForUI
import io.camlcase.smartwallet.data.core.Wallet
import io.camlcase.smartwallet.data.core.XTZ
import io.camlcase.smartwallet.data.core.extension.format
import io.camlcase.smartwallet.data.core.extension.formatAsMoney
import io.camlcase.smartwallet.data.core.extension.formatAsToken
import io.camlcase.smartwallet.data.core.extension.tezos.convertToCurrency
import io.camlcase.smartwallet.data.core.extension.tezos.parseError
import io.camlcase.smartwallet.data.model.UserTezosAddress
import io.camlcase.smartwallet.data.model.amount.Balance
import io.camlcase.smartwallet.data.model.amount.CoinBalance
import io.camlcase.smartwallet.data.model.amount.TokenBalance
import io.camlcase.smartwallet.data.model.amount.compareTo
import io.camlcase.smartwallet.data.model.error.AppError
import io.camlcase.smartwallet.data.model.error.ErrorType
import io.camlcase.smartwallet.data.model.error.InsufficientXTZ
import io.camlcase.smartwallet.data.model.exchange.CurrencyBundle
import io.camlcase.smartwallet.data.model.exchange.TokenInfoBundle
import io.camlcase.smartwallet.data.model.exchange.XTZTokenInfo
import io.camlcase.smartwallet.data.model.isXTZ
import io.camlcase.smartwallet.data.model.send.SendRequest
import io.camlcase.smartwallet.data.source.local.UserPreferences
import io.camlcase.smartwallet.data.usecase.OperationBundle
import io.camlcase.smartwallet.data.usecase.info.GetBCDBalanceUseCase
import io.camlcase.smartwallet.data.usecase.operation.SendTokenUseCase
import io.camlcase.smartwallet.model.DataWrapper
import io.camlcase.smartwallet.model.operation.OperationFeesBundle
import io.camlcase.smartwallet.model.operation.parseFees
import javax.inject.Inject

class SendConfirmationViewModel @Inject constructor(
    private val useCase: GetBCDBalanceUseCase,
    private val userPreferences: UserPreferences,
    private val sendUseCase: SendTokenUseCase
) : BaseViewModel() {
    /**
     * Calculate fees
     */
    private val _data = MutableLiveData<DataWrapper<ConfirmationForView>>()
    val data: LiveData<DataWrapper<ConfirmationForView>> = _data

    /**
     * SEND operation
     * @return operation hash, final amount sent
     * @see [SendRequest.transactionAmount]
     */
    private val _operation = MutableLiveData<DataWrapper<Pair<String, Balance<*>>>>()
    val operation: LiveData<DataWrapper<Pair<String, Balance<*>>>> = _operation

    private var sendRequest: SendRequest = SendRequest.empty
    private var tokenInfo: TokenInfoBundle = TokenInfoBundle.empty
    private var xtzInfo: XTZTokenInfo = XTZTokenInfo(0.0)
    private var sendOperations: List<Operation> = emptyList()
    private var currencyBundle: CurrencyBundle = Wallet.defaultCurrency

    private val user: UserTezosAddress by lazy {
        userPreferences.getUserAddress()
    }

    /**
     * Fetch data again to ensure we work with the latest info
     */
    fun getConfirmationDetails(request: SendRequest) {
        _data.value = DataWrapper.Loading()
        this.sendRequest = request
        currencyBundle = useCase.getCurrencyBundle()

        if (request.token.isXTZ()) {
            useCase.getTokenInfo(request.token)
        } else {
            useCase.getTokenInfo(request.token)
                .flatMap({
                    // Fetch XTZ info to calculate currency exchange for fees
                    useCase.getXTZExchange().map {
                        xtzInfo = it
                        it
                    }
                }, { balance, _ -> balance })
        }
            .map { updateRequest(it) }
            .flatMap { sendUseCase.createSendOperation(it) }
            .map { updateOperations(it) }
            .map { populateViewItems(it) }
            .subscribeForUI()
            .subscribe(
                {
                    _data.value = DataWrapper.Success(it)
                    clearOperationSubscription()
                },
                {
                    _data.value = DataWrapper.Error(parseError(it))
                    clearOperationSubscription()
                }
            ).addToDisposables(operationDisposables)
    }

    private fun updateRequest(info: TokenInfoBundle): SendRequest {
        if (info.balance.compareTo(sendRequest.amount) < 0) {
            throw AppError(ErrorType.EXPIRED_DATA)
        }
        tokenInfo = info
        sendRequest.balance = info.balance
        sendRequest.delegated =
            !userPreferences.preferences?.getString(UserPreferences.DELEGATE_ADDRESS, null).isNullOrEmpty()

        // Handle delegated accounts with 1 mutez balance
        if (sendRequest.token.isXTZ() && sendRequest.xtzValue <= Tez.zero) {
            throw AppError(ErrorType.BLOCKED_OPERATION, exception = InsufficientXTZ())
        }
        return sendRequest
    }

    private fun updateOperations(bundle: OperationBundle): OperationFees {
        this.sendOperations = bundle.operations
        return bundle.accumulatedFee
    }

    private fun populateViewItems(totalFees: OperationFees): ConfirmationForView {
        return if (sendRequest.token.isXTZ()) {
            populateXTZ(totalFees)
        } else {
            populateToken(totalFees)
        }
    }

    /**
     * XTZ
     * - If amount == balance -> total = amount - fees
     * - If amount < balance -> total = amount + fees
     */
    private fun populateXTZ(totalFees: OperationFees): ConfirmationForView {
        val transactionAmount: Tez = (sendRequest.transactionAmount as CoinBalance).value
        val currencyPrice = tokenInfo.exchange.currencyExchange
        val feesBundle = totalFees.parseFees(sendOperations, currencyPrice, currencyBundle)

        val amount: Tez
        val total: Tez
        if (sendUseCase.operationOverXTZFunds(sendRequest, feesBundle.totalFees)) {
            // Fees will be substracted from amount instead of added
            amount = transactionAmount - feesBundle.totalFees
            total = transactionAmount
        } else {
            amount = transactionAmount
            total = transactionAmount + feesBundle.totalFees
        }
        val symbol = tokenInfo.token.symbol
        val amountAsUSD = amount.convertToCurrency(currencyPrice).formatAsMoney(currencyBundle)
        val totalInUSD = total.convertToCurrency(currencyPrice).formatAsMoney(currencyBundle)
        return ConfirmationForView(
            amountInTez = amount.format(symbol),
            amountInCurrency = amountAsUSD,
            addressTo = sendRequest.to,
            totalfeesFormatted = feesBundle.totalFees.format(symbol),
            fees = feesBundle,
            total = total.format(symbol),
            totalInUSD = totalInUSD
        )
    }

    /**
     * FA1.2 token
     * - Fees(in xtz) are shown separately from total
     */
    private fun populateToken(totalFees: OperationFees): ConfirmationForView {
        val tezSymbol = XTZ.symbol
        val amount = (sendRequest.transactionAmount as TokenBalance).value
        val feesBundle = totalFees.parseFees(sendOperations, xtzInfo.currencyExchange, currencyBundle)

        val formattedAmount = amount.formatAsToken(tokenInfo.token)
        val formattedFees = feesBundle.totalFees.format(tezSymbol)
        val total = "$formattedAmount\n+ $formattedFees"

        // Since Dexter rates are going to jump dramatically, hide the currency value to avoid confusion
//        val currencyPrice = tokenInfo.exchange.currencyExchange
//        val amountAsUSD = if (tokenInfo.showInExchange()) {
//            amount.convertTo(currencyPrice)
//        } else {
//            null
//        }
        val feesAsUSD = feesBundle.totalFees.convertToCurrency(xtzInfo.currencyExchange)

        val totalAsUSD = /*if (amountAsUSD == null) {*/
            feesAsUSD.formatAsMoney(currencyBundle)
      /*  } else {
            (amountAsUSD + feesAsUSD).formatAsMoney(currencyBundle)
        }*/
        return ConfirmationForView(
            amountInTez = formattedAmount,
            amountInCurrency = /*amountAsUSD?.formatAsMoney(currencyBundle)*/null,
            addressTo = sendRequest.to,
            totalfeesFormatted = formattedFees,
            fees = feesBundle,
            total = total,
            totalInUSD = totalAsUSD
        )
    }

    private fun parseError(error: Throwable?): Throwable? {
        if (error is TezosError) {
            error.parseError()
        }

        return error
    }

    fun send() {
        _operation.value = DataWrapper.Loading()

        sendUseCase.send(sendOperations)
            .subscribeForUI()
            .subscribe(
                {
                    _operation.value = DataWrapper.Success(Pair(it, sendRequest.amount))
                    clearOperationSubscription()
                },
                {
                    _operation.value = DataWrapper.Error(parseError(it))
                    clearOperationSubscription()
                }
            ).addToDisposables(operationDisposables)
    }

    fun getCurrencyBundle(): CurrencyBundle {
        return useCase.getCurrencyBundle()
    }
}

/**
 * Formatted amounts for view
 */
data class ConfirmationForView(
    val amountInTez: String,
    val amountInCurrency: String?,
    val addressTo: String,
    val totalfeesFormatted: String,
    val fees: OperationFeesBundle,
    val total: String,
    val totalInUSD: String?
)
