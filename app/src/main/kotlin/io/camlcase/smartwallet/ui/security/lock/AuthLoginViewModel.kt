/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.security.lock

import androidx.annotation.IntDef
import io.camlcase.smartwallet.core.architecture.BaseViewModel
import io.camlcase.smartwallet.data.model.error.AppError
import io.camlcase.smartwallet.data.source.local.AppPreferences
import io.camlcase.smartwallet.data.source.user.PinLocalSource
import java.util.*
import javax.inject.Inject

class AuthLoginViewModel @Inject constructor(
    private val pinLocalSource: PinLocalSource,
    private val appPreferences: AppPreferences
) : BaseViewModel() {

    @PinCompare
    fun validPin(typedPin: String): Int {
        return try {
            if (pinLocalSource.comparePin(typedPin)) {
                VALID_PIN
            } else {
                INVALID_PIN
            }
        } catch (e: AppError) {
            PIN_ERROR
        }
    }

    fun biometricEnabled(): Boolean {
        return appPreferences.getBoolean(AppPreferences.BIOMETRIC_LOGIN_ENABLED)
    }

    /**
     * Update last auth timestamp
     */
    fun onValidAuthentication() {
        appPreferences.store(AppPreferences.AUTH_TIMESTAMP, Date().time)
        appPreferences.store(AppPreferences.AUTH_CANCELLED, false)
    }

    /**
     * This flag will force the welcome screen on active user so they can import their wallet again or create a new one.
     */
    fun unloggedUser() {
        appPreferences.store(AppPreferences.AUTH_CANCELLED, true)
    }

    companion object {
        @IntDef(VALID_PIN, PIN_ERROR, INVALID_PIN)
        @Retention(AnnotationRetention.SOURCE)
        annotation class PinCompare

        /**
         * Pin is as stored
         */
        const val VALID_PIN = 0

        /**
         * Something went wrong when hashing the pin
         */
        const val PIN_ERROR = 1

        /**
         * Pin is not as stored
         */
        const val INVALID_PIN = 3
    }
}
