/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.security.system

import android.content.Intent
import com.google.android.gms.common.GoogleApiAvailability
import com.google.android.gms.common.internal.ConnectionErrorMessages
import com.google.android.gms.security.ProviderInstaller
import io.camlcase.smartwallet.BuildConfig
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.data.analytic.report
import io.camlcase.smartwallet.data.core.Logger
import io.camlcase.smartwallet.data.core.extension.multiLet
import io.camlcase.smartwallet.ui.base.BaseActivity
import io.camlcase.smartwallet.ui.base.navigate.RequestCode
import io.camlcase.smartwallet.ui.base.singleAnswerDialog
import java.lang.ref.WeakReference

/**
 * Android relies on a security Provider to provide secure network communications. However, from time to time,
 * vulnerabilities are found in the default security provider. To protect against these vulnerabilities, Google Play
 * services provides a way to automatically update a device's security provider to protect against known exploits
 *
 * @see https://developer.android.com/training/articles/security-gms-provider
 */
abstract class SecurityProviderUpdater {

    abstract var activityRef: WeakReference<BaseActivity>
    private var retryProviderInstall: Boolean = false

    /**
     * Notify that Android Security Provider is Up-to-date
     */
    abstract var onProviderOk: () -> Unit

    private var listener = object : ProviderInstaller.ProviderInstallListener {
        /**
         * This method is called if updating fails; the error code indicates
         * whether the error is recoverable.
         */
        override fun onProviderInstallFailed(errorCode: Int, recoveryIntent: Intent?) {
            GoogleApiAvailability.getInstance().apply {
                if (isUserResolvableError(errorCode)) {
                    // Recoverable error. Show a dialog prompting the user to
                    // install/update/enable Google Play services.
                    activityRef.get()?.let { activity ->
                        val playServicesErrorCode: Int = this.isGooglePlayServicesAvailable(activity)
                        val errorIntent: Intent? =
                            this.getErrorResolutionIntent(activity, playServicesErrorCode, null)

                        singleAnswerDialog(
                            context = activity,
                            message = ConnectionErrorMessages.getErrorMessage(
                                activity,
                                playServicesErrorCode
                            ),
                            okString = ConnectionErrorMessages.getErrorDialogButtonMessage(
                                activity,
                                playServicesErrorCode
                            ),
                            cancelable = BuildConfig.DEBUG,
                            onOK = {
                                multiLet(activityRef.get(), errorIntent) { activity, intent ->
                                    activity.startActivityForResult(
                                        intent,
                                        RequestCode.PROVIDER_ERROR_DIALOG_CODE.code
                                    )
                                }
                                it.dismiss()
                            }

                        )
                    }
                } else {
                    onProviderInstallerNotAvailable(errorCode)
                }
            }
        }

        /**
         * This method is only called if the provider is successfully updated
         * (or is already up-to-date).
         */
        override fun onProviderInstalled() {
            // Provider is up-to-date, app can make secure network calls.
            Logger.i("✓️ Android Security Provider is Up-to-date")
            onProviderOk()
        }
    }

    fun init() {
        activityRef.get()?.apply {
            ProviderInstaller.installIfNeededAsync(this, listener)
        }
    }

    /**
     * On resume, check to see if we flagged that we need to reinstall the
     * provider.
     */
    fun onProviderPostResume() {
        if (retryProviderInstall) {
            // We can now safely retry installation.
            init()
        }
        retryProviderInstall = false
    }

    private fun onProviderInstallerNotAvailable(errorCode: Int) {
        // This is reached if the provider cannot be updated for some reason.
        // App should consider all HTTP communication to be vulnerable, and take
        // appropriate action.
        activityRef.get()?.apply {
            singleAnswerDialog(
                this,
                this.getString(R.string.error_transaction_generic),
                this.getString(R.string.android_security_provider_dialog),
                false
            )
        }
        // Report to analytics so we can register unsafe situations
        IllegalStateException("SecurityProviderUpdater couldn't update (${errorCode}").report()
    }

    fun onProviderDialogResponse() {
        // Adding a fragment via GoogleApiAvailability.showErrorDialogFragment
        // before the instance state is restored throws an error. So instead,
        // set a flag here, which will cause the fragment to delay until
        // onPostResume.
        retryProviderInstall = true
    }
}
