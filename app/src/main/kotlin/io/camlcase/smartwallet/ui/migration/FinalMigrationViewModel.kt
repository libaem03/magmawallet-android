/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.migration

import io.camlcase.kotlintezos.model.Address
import io.camlcase.kotlintezos.model.Tez
import io.camlcase.smartwallet.core.Migration
import io.camlcase.smartwallet.core.architecture.BaseViewModel
import io.camlcase.smartwallet.core.extension.addToDisposables
import io.camlcase.smartwallet.core.extension.subscribeForUI
import io.camlcase.smartwallet.data.source.local.UserPreferences
import io.camlcase.smartwallet.data.usecase.analytics.AnalyticEvent
import io.camlcase.smartwallet.data.usecase.analytics.SendAnalyticUseCase
import io.camlcase.smartwallet.data.usecase.migration.GetMigrationWalletBalance
import io.camlcase.smartwallet.data.usecase.user.CreateWalletUseCase
import javax.inject.Inject

class FinalMigrationViewModel @Inject constructor(
    private val createWalletUseCase: CreateWalletUseCase,
    private val balanceUseCase: GetMigrationWalletBalance,
    private val userPreferences: UserPreferences,
    private val sendAnalyticUseCase: SendAnalyticUseCase
) : BaseViewModel() {

    fun swapMigrationWalletToStandardWallet() {
        createWalletUseCase.swapMigrationWalletToStandardWallet()
    }

    fun checkMigrationWalletBalance(onPositiveBalance: () -> Unit, onEmptyBalance: () -> Unit) {
        balanceUseCase.getBalance()
            .subscribeForUI()
            .subscribe(
                {
                    if (it > Tez.zero) {
                        onPositiveBalance()
                    } else {
                        onEmptyBalance()
                    }
                    clearOperationSubscription()
                },
                {
                    onEmptyBalance()
                    clearOperationSubscription()
                }
            ).addToDisposables(operationDisposables)
    }

    /**
     * When it has reached this point, the wallets have already been swapped
     */
    fun getNewStandardAddress(): Address {
        return userPreferences.getUserAddress().address
    }

    fun sendEvent(event: AnalyticEvent) {
        sendAnalyticUseCase.reportEvent(event)
    }
}

/**
 * If we should show the UI options for migrations
 */
class ShouldMigrateViewModel @Inject constructor(
    private val userPreferences: UserPreferences
) : BaseViewModel() {
    fun shouldMigrate(): Boolean {
        return userPreferences.versionCreated() < Migration.SECURE_WALLET_CREATION_VERSION
    }
}

/**
 * If the user chooses to never see the migration dialog again
 */
class DismissMigrationViewModel @Inject constructor(
    private val userPreferences: UserPreferences
) : BaseViewModel() {

    fun updateVersionCreated(versionCode: Int) {
        userPreferences.updateVersionCreated(versionCode)
    }
}
