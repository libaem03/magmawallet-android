/**
 * # Released under MIT License
 *
 * Copyright (c) 2021 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.onboarding.seed_phrase

import android.content.Context
import android.os.Bundle
import android.view.View
import by.kirich1409.viewbindingdelegate.viewBinding
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.core.Arguments
import io.camlcase.smartwallet.core.extension.*
import io.camlcase.smartwallet.data.model.error.AppError
import io.camlcase.smartwallet.data.model.error.ErrorType
import io.camlcase.smartwallet.data.usecase.analytics.ScreenEvent
import io.camlcase.smartwallet.data.usecase.analytics.StateEvent
import io.camlcase.smartwallet.databinding.FragmentConfirmSeedPhraseBinding
import io.camlcase.smartwallet.model.DataWrapper
import io.camlcase.smartwallet.model.handleResults
import io.camlcase.smartwallet.model.navigation.SeedPhraseConfirm
import io.camlcase.smartwallet.ui.base.BindBaseFragment
import io.camlcase.smartwallet.ui.base.showError
import io.camlcase.smartwallet.ui.base.showSnackbar
import java.lang.ref.WeakReference

/**
 * ONB: Make the user write the mnemonic again.
 */
class SeedPhraseConfirmFragment : BindBaseFragment(R.layout.fragment_confirm_seed_phrase) {
    interface SeedPhraseConfirmListener {
        fun onSeedPhraseConfirmed()
    }

    private val binding by viewBinding(FragmentConfirmSeedPhraseBinding::bind)
    private lateinit var viewModel: SeedPhraseViewModel
    private lateinit var seedPhraseValidation: SeedPhraseImportValidation
    private var listener: SeedPhraseConfirmListener? = null

    @SeedPhraseConfirm.ScreenSource
    private var source: Int = SeedPhraseConfirm.SOURCE_ONBOARDING

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        slideInRight()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        reportView(ScreenEvent.ONB_CONFIRM_BACKUP)
        manageIntent(arguments)
        activity?.markSecure()
        listener = context as? SeedPhraseConfirmListener
        if (listener == null) {
            throw ClassCastException("$context must implement SelectWalletFragment")
        }
        viewModel = viewModel(viewModelFactory) {
            observe(validation, { handleValidation(it) })
        }
        viewModel.source = source
    }

    private fun manageIntent(bundle: Bundle?) {
        bundle?.getInt(Arguments.ARG_SOURCE)?.let {
            source = it
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initAppbar()
        seedPhraseValidation = object : SeedPhraseImportValidation {
            override var seedPhraseBinding = WeakReference(binding.mainContainer)

            override fun invalidMnemonic() {
                binding.actionNext.isEnabled = false
            }

            override fun validMnemonic() {
                binding.actionNext.isEnabled = true
            }
        }
        seedPhraseValidation.init(context)
        seedPhraseValidation.bind()
        bindViews()
        viewModel.hasPassphrase {
            if (it) {
                binding.mainContainer.titlePassphrase.show()
                binding.mainContainer.inputPassphrase.show()
            }
        }

        keyboardListener = activity?.listenToKeyboard { visible ->
            if (visible) {
                binding.actionSkip.hide()
            } else {
                binding.actionSkip.show()
            }
        }
    }

    private fun initAppbar() {
        val title = getString(R.string.onb_recovery_phrase_backup_title)
        setupAppBar(binding.appBarContainer, title, true)
    }

    private fun bindViews() {
        binding.actionNext.throttledClick()
            .subscribe {
                analyticsUseCase.get()?.reportState(StateEvent.ONB_CONFIRM_CONFIRM_BUTTON)
                binding.mainContainer.inputMnemonic.textField()?.hideKeyboard()
                viewModel.confirmMnemonic(seedPhraseValidation.getMnemonic(), seedPhraseValidation.getPassphrase())
            }.addToDisposables(disposables)

        binding.actionSkip.throttledClick()
            .subscribe {
                analyticsUseCase.get()?.reportState(StateEvent.ONB_CONFIRM_SKIP_BUTTON)
                onConfirm()
            }
            .addToDisposables(disposables)
    }

    private fun handleValidation(wrapper: DataWrapper<Boolean>) {
        wrapper.handleResults(
            onValidData = { confirmedData ->
                hideLoading()
                if (confirmedData) {
                    onConfirm()
                } else {
                    binding.actionNext.isEnabled = true
                    showSnackbar(getString(R.string.onb_recovery_phrase_backup_error))
                }
            },
            onError = {
                hideLoading()
                binding.actionNext.isEnabled = true
                if (it is AppError && it.type == ErrorType.WALLET_CREATE_ERROR) {
                    showSnackbar(getString(R.string.error_invalid_seed_phrase))
                } else {
                    showError(it)
                }
            },
            onLoading = {
                showLoading()
            }
        )
    }

    private fun showLoading() {
        binding.loading.containerLoading.show()
        binding.actionNext.isEnabled = false
        binding.actionSkip.isEnabled = false
    }

    private fun hideLoading() {
        binding.loading.containerLoading.hide()
        binding.actionSkip.isEnabled = true
    }

    private fun onConfirm() {
        listener?.onSeedPhraseConfirmed()
    }

    override fun onDestroyView() {
        binding.mainContainer.inputMnemonic.textField()?.hideKeyboard()
        super.onDestroyView()
    }

    companion object {
        fun init(@SeedPhraseConfirm.ScreenSource source: Int): SeedPhraseConfirmFragment {
            val fragment = SeedPhraseConfirmFragment()
            val args = Bundle()
            args.putInt(Arguments.ARG_SOURCE, source)
            fragment.arguments = args
            return fragment
        }
    }
}
