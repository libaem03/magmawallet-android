/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.main.activity

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import io.camlcase.smartwallet.core.architecture.BaseViewModel
import io.camlcase.smartwallet.core.extension.OPERATION_DATE_FORMAT
import io.camlcase.smartwallet.core.extension.addToDisposables
import io.camlcase.smartwallet.core.extension.formatDate
import io.camlcase.smartwallet.core.extension.subscribeForUI
import io.camlcase.smartwallet.data.core.extension.tezos.getDate
import io.camlcase.smartwallet.data.model.Token
import io.camlcase.smartwallet.data.model.contact.MagmaContact
import io.camlcase.smartwallet.data.model.delegate.TezosBakerResponse
import io.camlcase.smartwallet.data.model.exchange.TokenInfoBundle
import io.camlcase.smartwallet.data.model.operation.DelegateOperation
import io.camlcase.smartwallet.data.model.operation.OriginateOperation
import io.camlcase.smartwallet.data.model.operation.SwapOperation
import io.camlcase.smartwallet.data.model.operation.TezosOperation
import io.camlcase.smartwallet.data.usecase.activity.GetOperationsUseCase
import io.camlcase.smartwallet.data.usecase.contact.ListContactsUseCase
import io.camlcase.smartwallet.data.usecase.delegate.GetDelegateUseCase
import io.camlcase.smartwallet.model.DataWrapper
import io.camlcase.smartwallet.model.operation.DateHeader
import io.camlcase.smartwallet.model.operation.OperationForList
import io.camlcase.smartwallet.model.operation.OperationForViewBundle
import io.camlcase.smartwallet.model.operation.TokenFilteredOperations
import io.reactivex.rxjava3.core.Single
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

/**
 * Fetch and filter Conseil operations of a user
 */
class OperationListViewModel @Inject constructor(
    private val useCase: GetOperationsUseCase,
    private val contactsUseCase: ListContactsUseCase,
    private val delegateUseCase: GetDelegateUseCase
) : BaseViewModel() {
    /**
     * List of transactions/operations injected on the blockchain
     */
    private val _data = MutableLiveData<DataWrapper<List<OperationForList>>>()
    val data: LiveData<DataWrapper<List<OperationForList>>> = _data

    /**
     * Operations of a token sorted desc by date
     */
    private val _tokenOperations = MutableLiveData<DataWrapper<TokenFilteredOperations>>()
    val tokenOperations: LiveData<DataWrapper<TokenFilteredOperations>> = _tokenOperations

    /**
     * All operations sorted desc by date
     */
    var operations: List<OperationForList> = emptyList()

    /**
     * All operations grouped in a map of Date
     */
    private var operationsMap: Map<TransactionMapKey, List<OperationForViewBundle>> = emptyMap()

    /**
     * Fetch all operations for this user and, if [hasContactPermission], match them with its contacts.
     */
    fun getOperations(hasContactPermission: Boolean) {
        getOperations(null, hasContactPermission, false)
    }

    /**
     * @param contact Filter operations by this
     * @param hasContactPermission It can retrieve contacts
     * @param localOps Don't go to remote
     */
    fun getOperations(contact: MagmaContact? = null, hasContactPermission: Boolean, localOps: Boolean = false) {
        _data.value = DataWrapper.Loading()

        Single.zip<List<TezosOperation>, List<MagmaContact>, List<TezosBakerResponse>, List<OperationForList>>(
            listOperations(contact, localOps),
            listContacts(contact, hasContactPermission),
            delegateUseCase.supportedDelegates(),
            { operations, contacts, delegates -> parseOperations(operations, contacts, delegates) }
        )
            .subscribeForUI()
            .subscribe(
                {
                    this.operations = it
                    if (it.isNullOrEmpty()) {
                        _data.value = DataWrapper.Empty()
                    } else {
                        _data.value = DataWrapper.Success(it)
                    }
                },
                { _data.value = DataWrapper.Error(it) }
            ).addToDisposables(disposables)
    }

    private fun listOperations(contact: MagmaContact?, localOps: Boolean): Single<List<TezosOperation>> {
        return if (contact == null) {
            if (localOps) {
                Single.fromCallable { useCase.localOperations() }
            } else {
                useCase.operations()
            }
        } else {
            Single.fromCallable { useCase.filterOperations(contact.tezosAddresses) }
        }
    }

    private fun listContacts(contact: MagmaContact?, hasContactPermission: Boolean): Single<List<MagmaContact>> {
        return if (contact == null) {
            if (hasContactPermission) {
                Single.fromCallable { contactsUseCase.getMagmaContacts() }
            } else {
                Single.fromCallable { emptyList<MagmaContact>() }
            }
        } else {
            Single.fromCallable { listOf(contact) }
        }
    }

    private fun parseOperations(
        result: List<TezosOperation>,
        contacts: List<MagmaContact>,
        supportedDelegates: List<TezosBakerResponse>
    ): List<OperationForList> {
        val parser = OperationForViewParser(contacts, supportedDelegates)
        val map = HashMap<TransactionMapKey, List<OperationForViewBundle>>()
        for (tezosOp in result) {
            val timestamp = (tezosOp.tzKtOperations[0].getDate() ?: Date()).time
            val date = timestamp.formatDate(OPERATION_DATE_FORMAT)
            val key = TransactionMapKey(timestamp, date)

            val element = parser.parseOperation(tezosOp)

            val datedOperations = ArrayList(map[key] ?: emptyList())
            datedOperations.add(OperationForViewBundle(tezosOp, element))
            if (datedOperations.size > 1) {
                datedOperations.sortByDescending { Date(it.opForView.timestamp) }
            }
            map[key] = datedOperations
        }

        operationsMap = map.toSortedMap(compareByDescending { Date(it.timestamp) })
        val operations = ArrayList<OperationForList>()
        for (entries in operationsMap) {
            operations.add(DateHeader(entries.key.formattedDate))
            operations.addAll(entries.value.map { it.opForView })
        }

        return operations
    }

    fun filterTokenOperations(tokenInfo: TokenInfoBundle) {
        _tokenOperations.value = DataWrapper.Loading()
        fun filterByToken(token: Token, operation: TezosOperation): Boolean {
            // Return only direct operations for this token (send, exchange)
            if (operation is OriginateOperation || operation is DelegateOperation) {
                return false
            }

            if (operation.token.symbol == token.symbol) {
                return true
            }
            if (operation is SwapOperation && operation.tokenFrom.symbol == token.symbol) {
                return true
            }

            return false
        }

        Single.fromCallable { operationsMap }
            .map {
                val operations = ArrayList<OperationForList>()
                for (entries in operationsMap) {
                    val tokenOperations = entries.value.filter { filterByToken(tokenInfo.token, it.operation) }
                    if (tokenOperations.isNotEmpty()) {
                        operations.add(DateHeader(entries.key.formattedDate))
                        operations.addAll(tokenOperations.map { it.opForView })
                    }
                }
                operations
            }
            .subscribeForUI()
            .subscribe(
                {
                    _tokenOperations.value = DataWrapper.Success(TokenFilteredOperations(tokenInfo, it))
                },
                { _tokenOperations.value = DataWrapper.Error(it) }
            ).addToDisposables(disposables)
    }

    fun clearLocalOperations() {
        useCase.clear()
        operations = emptyList()
        operationsMap = emptyMap()
    }
}

/**
 * Helper class to sort the map to list by descending Date.
 * @param timestamp So we can sort the map before turning into a list
 * @param formattedDate So we can separate by key on the map
 */
private data class TransactionMapKey(
    val timestamp: Long,
    val formattedDate: String
) {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as TransactionMapKey

        if (formattedDate != other.formattedDate) return false

        return true
    }

    override fun hashCode(): Int {
        return formattedDate.hashCode()
    }
}
