/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.main.settings

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import io.camlcase.smartwallet.core.Migration
import io.camlcase.smartwallet.core.architecture.BaseViewModel
import io.camlcase.smartwallet.core.extension.addToDisposables
import io.camlcase.smartwallet.core.extension.subscribeForUI
import io.camlcase.smartwallet.data.analytic.report
import io.camlcase.smartwallet.data.model.delegate.TezosBaker
import io.camlcase.smartwallet.data.source.local.AppPreferences
import io.camlcase.smartwallet.data.source.local.UserPreferences
import io.camlcase.smartwallet.data.usecase.LogOutUseCase
import io.camlcase.smartwallet.data.usecase.analytics.AnalyticEvent
import io.camlcase.smartwallet.data.usecase.analytics.SendAnalyticUseCase
import io.camlcase.smartwallet.data.usecase.delegate.GetDelegateUseCase
import io.camlcase.smartwallet.data.usecase.info.GetXTZCurrencyExchange
import io.camlcase.smartwallet.model.DataWrapper
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

class SettingsViewModel @Inject constructor(
    private val delegateUseCase: GetDelegateUseCase,
    private val appPreferences: AppPreferences,
    private val userPreferences: UserPreferences,
    private val logOutUseCase: LogOutUseCase,
    private val analyticUseCase: SendAnalyticUseCase,
    private val currencyRatesUseCase: GetXTZCurrencyExchange
) : BaseViewModel() {

    /**
     * List of Settings rows
     */
    private val _data = MutableLiveData<DataWrapper<List<SettingsItem>>>()
    val data: LiveData<DataWrapper<List<SettingsItem>>> = _data


    fun initSettings(vararg texts: String) {
        _data.value = DataWrapper.Loading()
        val usesBiometric = texts.firstOrNull { it == AppPreferences.BIOMETRIC_LOGIN_ENABLED } != null

        delegateUseCase.listenToDelegate()
            .doOnSubscribe {
                delegateUseCase.getDelegate()
                    .subscribeForUI()
                    .subscribe(
                        { populateSettings(it.getOrNull(), usesBiometric) },
                        { populateSettings(null, usesBiometric) }
                    ).addToDisposables(disposables)
            }
            .subscribeForUI()
            .subscribe(
                { populateSettings(it.getOrNull(), usesBiometric) },
                { populateSettings(null, usesBiometric) }
            ).addToDisposables(disposables)
    }

    fun updateDelegate() {
        delegateUseCase.pingLocalDelegate()
            .subscribeForUI()
            .subscribe(
                {
                    // Nothing to do. Stream will go through initSettings
                },
                {
                    it.report()
                }).addToDisposables(disposables)
    }

    private fun populateSettings(currentBaker: TezosBaker?, usesBiometric: Boolean) {
        val list = ArrayList<SettingsItem>()

        // MIGRATE WALLET
        if (userPreferences.versionCreated() < Migration.SECURE_WALLET_CREATION_VERSION) {
            list.add(HeaderSettings(SettingType.HEADER_UPDATE))
            list.add(SettingsForView(SettingType.MIGRATE_WALLET, null))
        }

        list.add(HeaderSettings(SettingType.HEADER_DELEGATE))
        // DELEGATE
        val delegate = currentBaker?.name ?: currentBaker?.address
        list.add(SettingsForView(SettingType.DELEGATE, delegate))

        populateSecuritySection(list, usesBiometric)

        populateAboutSection(list)

        list.add(HeaderSettings(SettingType.HEADER_OTHER))

        list.add(SettingsForView(SettingType.NETWORK_CONFIG, null))
        // CURRENCIES
        val currencyCode = currencyRatesUseCase.getCurrencyCode()
        list.add(SettingsForView(SettingType.LOCAL_CURRENCY, currencyCode))
        // VERSION
        list.add(SettingsForView(SettingType.VERSION, null))
        // DELETE WALLET
        list.add(SettingsForView(SettingType.DELETE_WALLET, null))

        _data.value = DataWrapper.Success(list)
    }

    private fun populateSecuritySection(list: ArrayList<SettingsItem>, usesBiometric: Boolean) {
        list.add(HeaderSettings(SettingType.HEADER_SECURITY))
        // BIOMETRIC LOGIN
        if (usesBiometric) {
            val biometricEnabled = appPreferences.getBoolean(AppPreferences.BIOMETRIC_LOGIN_ENABLED)
            list.add(SettingsForView(SettingType.BIOMETRIC_LOGIN, null, biometricEnabled))
        }
        // EDIT PIN
        list.add(SettingsForView(SettingType.EDIT_PIN, null))
        // RECOVERY PHRASE
        list.add(SettingsForView(SettingType.RECOVERY_PHRASE, null))
    }

    private fun populateAboutSection(list: ArrayList<SettingsItem>) {
        list.add(HeaderSettings(SettingType.HEADER_ABOUT))
        // FEEDBACK
        list.add(SettingsForView(SettingType.FEEDBACK, null, external = true))
        // ABOUT
        list.add(SettingsForView(SettingType.ABOUT, null, external = true))
        // PRIVACY POLICY
        list.add(SettingsForView(SettingType.PRIVACY_POLICY, null, external = true))
        // TERMS
        list.add(SettingsForView(SettingType.TERMS, null, external = true))
        // NEWSLETTER
        list.add(SettingsForView(SettingType.NEWSLETTER, null, external = true))
        // ATTRIBUTIONS
        list.add(SettingsForView(SettingType.ATTRIBUTIONS, null))
    }

    fun deleteWallet(onFinished: () -> Unit) {
        Single.fromCallable {
            analyticUseCase.reportEvent(AnalyticEvent.WALLET_UNPAIRED)
            logOutUseCase.logOut()
            true
        }.subscribeForUI()
            .subscribe(
                { onFinished() },
                { onFinished() }
            ).addToDisposables(disposables)
    }

    fun updateBiometricLogin(enabled: Boolean) {
        appPreferences.store(AppPreferences.BIOMETRIC_LOGIN_ENABLED, enabled)
    }
}

interface SettingsItem {
    val type: SettingType
}

class HeaderSettings(
    override val type: SettingType
) : SettingsItem

/**
 * @param external Link to outside of the app
 */
class SettingsForView(
    override val type: SettingType,
    val detail: String?,
    val checked: Boolean = false, // If switch
    val external: Boolean = false
) : SettingsItem

enum class SettingType {
    /**
     * Headers
     */
    HEADER_UPDATE,
    HEADER_DELEGATE,
    HEADER_SECURITY,
    HEADER_ABOUT,
    HEADER_OTHER,

    /**
     * Actual sections
     */
    BIOMETRIC_LOGIN,
    EDIT_PIN,
    DELEGATE,
    RECOVERY_PHRASE,
    ABOUT,
    FEEDBACK,
    PRIVACY_POLICY,
    TERMS,
    NEWSLETTER,
    ATTRIBUTIONS,
    MIGRATE_WALLET,
    NETWORK_CONFIG,
    LOCAL_CURRENCY,
    DELETE_WALLET,
    VERSION
}
