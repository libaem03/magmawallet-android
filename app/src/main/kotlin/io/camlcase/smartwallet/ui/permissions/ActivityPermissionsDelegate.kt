package io.camlcase.smartwallet.ui.permissions

import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.processors.PublishProcessor

class ActivityPermissionsDelegate {
    private val processor: PublishProcessor<PermissionsRequestBundle> = PublishProcessor.create()
    val stream: Flowable<PermissionsRequestBundle> = processor.onBackpressureBuffer()

    fun onPermissionsResponse(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        processor.onNext(PermissionsRequestBundle(requestCode, permissions, grantResults))
    }
}

data class PermissionsRequestBundle(
    val requestCode: Int,
    val permissions: Array<String>,
    val grantResults: IntArray
) {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as PermissionsRequestBundle

        if (requestCode != other.requestCode) return false
        if (!permissions.contentEquals(other.permissions)) return false
        if (!grantResults.contentEquals(other.grantResults)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = requestCode
        result = 31 * result + permissions.contentHashCode()
        result = 31 * result + grantResults.contentHashCode()
        return result
    }
}
