/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.exchange.amount

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import io.camlcase.kotlintezos.model.Tez
import io.camlcase.smartwallet.core.architecture.BaseViewModel
import io.camlcase.smartwallet.core.extension.addToDisposables
import io.camlcase.smartwallet.core.extension.formatExchangeRate
import io.camlcase.smartwallet.core.extension.subscribeForUI
import io.camlcase.smartwallet.data.core.Exchange
import io.camlcase.smartwallet.data.core.Wallet
import io.camlcase.smartwallet.data.core.XTZ
import io.camlcase.smartwallet.data.core.extension.formatAsMoney
import io.camlcase.smartwallet.data.core.extension.formatAsToken
import io.camlcase.smartwallet.data.core.extension.formatWithDecimals
import io.camlcase.smartwallet.data.core.extension.tezos.convertToCurrency
import io.camlcase.smartwallet.data.model.Token
import io.camlcase.smartwallet.data.model.amount.Balance
import io.camlcase.smartwallet.data.model.amount.CoinBalance
import io.camlcase.smartwallet.data.model.amount.EmptyBalance
import io.camlcase.smartwallet.data.model.amount.TokenBalance
import io.camlcase.smartwallet.data.model.exchange.*
import io.camlcase.smartwallet.data.model.isXTZ
import io.camlcase.smartwallet.data.model.swap.SwapRequest
import io.camlcase.smartwallet.data.source.local.AppPreferences
import io.camlcase.smartwallet.data.usecase.info.GetBCDBalanceUseCase
import io.camlcase.smartwallet.data.usecase.operation.DexterCalculationsUseCase
import io.camlcase.smartwallet.model.DataWrapper
import io.reactivex.rxjava3.core.Single
import java.math.BigDecimal
import java.math.BigInteger
import java.math.RoundingMode
import javax.inject.Inject
import javax.inject.Named

class ExchangeAmountViewModel @Inject constructor(
    private val useCase: GetBCDBalanceUseCase,
    @Named("SupportedFATokens")
    private val supportedFATokens: List<Token>,
    private val appPreferences: AppPreferences,
    private val calculationsUseCase: DexterCalculationsUseCase
) : BaseViewModel() {
    private val _data = MutableLiveData<DataWrapper<ExchangeForView>>()
    val data: LiveData<DataWrapper<ExchangeForView>> = _data

    /**
     * Once user clicks "Exchange", calculate values and go to the next screen
     */
    private val _operation = MutableLiveData<DataWrapper<SwapRequest>>()
    val operation: LiveData<DataWrapper<SwapRequest>> = _operation

    private var balances: List<TokenInfoBundle> = emptyList()
    private var error: Throwable? = null
    private var totalBalance = BigDecimal.ZERO
    private var xtzBalance: TokenInfoBundle = TokenInfoBundle.empty

    var fromToken: TokenInfoBundle = TokenInfoBundle.empty
    var toToken: TokenInfoBundle = TokenInfoBundle.empty
    private var exchange: DexterExchange? = null
    var selectedSlippage: Double = Exchange.DEFAULT_SLIPPAGE
    var timeoutMinutes: Int = Exchange.DEFAULT_TIMEOUT_MINUTES
    private var currencyBundle = Wallet.defaultCurrency

    private var validationsFrom: List<AmountValidation> =
        listOf(AmountValidation.OVER_ZERO, AmountValidation.BALANCE)

    private var validationsTo: List<AmountValidation> =
        listOf(
            AmountValidation.OVER_ZERO,
            AmountValidation.TOKEN_POOL,
            AmountValidation.TOKEN_BALANCE
        )

    /**
     * Check if the user changed the slippage/timeout values or use defaults
     */
    fun init() {
        val storedSlippage = appPreferences.getDouble(AppPreferences.EXCHANGE_SLIPPAGE)
        selectedSlippage = if (storedSlippage == -1.0) {
            Exchange.DEFAULT_SLIPPAGE
        } else {
            storedSlippage
        }

        val storedTimeout = appPreferences.getInt(AppPreferences.EXCHANGE_TIMEOUT)
        timeoutMinutes = if (storedTimeout == -1) {
            Exchange.DEFAULT_TIMEOUT_MINUTES
        } else {
            storedTimeout
        }
    }

    fun getTokenBalances() {
        _data.value = DataWrapper.Loading()

        currencyBundle = getCurrencyBundle()
        if (supportedFATokens.isEmpty()) {
            _data.value = DataWrapper.Empty()
        } else {
            Single.fromCallable { useCase.getLocalBalances().map { Result.success(it) } }
                .map { parseBalances(it, true) }
                .flatMap { calculateConversion() }
                .map { parseInfoForView(it) }
                .subscribeForUI()
                .subscribe(
                    {
                        handleSuccess(it)
                    },
                    { _data.value = DataWrapper.Error(it) }
                ).addToDisposables(disposables)
        }
    }

    private fun handleSuccess(data: ExchangeForView) {
        _data.value = DataWrapper.Success(data)
        error?.apply {
            // Balances could come with some error
            _data.value = DataWrapper.Error(this)
        }
    }

    fun refreshTokenBalances(clear: Boolean = false) {
        _data.value = DataWrapper.Loading()
        useCase.getBalances()
            .map { parseBalances(it, clear) }
            .flatMap { calculateConversion() }
            .map { parseInfoForView(it) }
            .subscribeForUI()
            .subscribe(
                { handleSuccess(it) },
                { _data.value = DataWrapper.Error(it) }
            ).addToDisposables(disposables)
    }

    /**
     * @param clear If fromToken/toToken should be filled for the first time
     */
    private fun parseBalances(results: List<Result<TokenInfoBundle>>, clear: Boolean): List<TokenInfoBundle> {
        val resultList: ArrayList<TokenInfoBundle> = ArrayList()

        for (result in results) {
            if (result.isFailure) {
                error = result.exceptionOrNull()
            }
            val tokenItem = result.getOrNull()
            tokenItem?.also {
                if (tokenItem.showInExchange()) {
                    resultList.add(it)
                    totalBalance = totalBalance.add(it.balance.bigDecimal)
                }
            }
        }
        balances = resultList.sortedBy { it.token }
        updateTokens(clear)
        return resultList
    }

    private fun updateTokens(clear: Boolean) {
        // We only need this if clear == true
        val firstFaToken = if (clear) {
            firstFAToken()
        } else {
            null
        }
        // Since it'd be always a small list, we loop it again when all balances have been parsed.
        balances.forEach { infoBundle ->
            updateTokens(infoBundle, firstFaToken, clear)
        }
    }

    private fun firstFAToken(): TokenInfoBundle? {
        val faTokens = balances.filter { !it.token.isXTZ() }
        return if (faTokens.isNullOrEmpty()) {
            null
        } else {
            faTokens[0]
        }
    }

    private fun updateTokens(info: TokenInfoBundle, firstFaToken: TokenInfoBundle?, clear: Boolean = false) {
        // When clear, we set up the tokens for the first time
        if (clear) {
            if (info.token.isXTZ()) {
                xtzBalance = info
                fromToken = info
            } else if (supportedFATokens.contains(info.token) && toToken != firstFaToken) {
                // Update if the info has been updated too
                toToken = info
            }
            // If not, we update the already selected ones
        } else {
            if (info.token == fromToken.token) {
                fromToken = info
            } else if (info.token == fromToken.token) {
                toToken = info
            }
        }
    }

    private fun parseInfoForView(conversionRate: String): ExchangeForView {
        val maxBalance =
            balances.firstOrNull { it.token.symbol == fromToken.token.symbol }?.balance?.bigDecimal ?: BigDecimal.ZERO

        return ExchangeForView(
            fromToken.token,
            toToken.token,
            maxBalance,
            fromToken.balance.formattedValue,
            conversionRate
        )
    }

    /**
     * To be able to exchange:
     * - Total Balance > 0
     * - There should be FA1.2 tokens to exchange
     * - At least one of them should have balance
     * @return true if fields should be enabled
     */
    fun exchangeEnabled(): Boolean {
        return totalBalance.compareTo(BigDecimal.ZERO) != 0 && supportedFATokens.isNotEmpty() && balances.size > 1
    }

    /**
     * [amount] Token = ?? Token
     */
    private fun calculateConversion(amount: BigDecimal = BigDecimal.ZERO): Single<String> {
        fun getConversionRate(): Single<BigDecimal> {
            return when {
                amount.compareTo(BigDecimal.ZERO) == 0 -> {
                    calculationsUseCase.calculateMarketRate(fromToken, toToken)
                }
                // If convertAmount called just before, it would already be calculated and set in exchange.
                // No need to call getExchangeRate again.
                exchange?.conversionRate != null -> {
                    Single.just(exchange?.conversionRate!!)
                }
                else -> {
                    calculationsUseCase.getExchangeRate(amount, fromToken, toToken)
                }
            }
        }

        return if (fromToken.token != toToken.token) {
            getConversionRate()
                .map { conversionRate ->
                    val formatted = conversionRate.formatExchangeRate(fromToken.token.symbol, toToken.token.symbol)
                    formatted
                }
        } else {
            Single.just("")
        }
    }

    fun switch() {
        val swappedFrom = toToken
        val swappedTo = fromToken

        toToken = swappedTo
        fromToken = swappedFrom
        update()
    }

    private fun update() {
        calculateConversion()
            .map { parseInfoForView(it) }
            .subscribeForUI()
            .subscribe(
                { _data.value = DataWrapper.Success(it) },
                { _data.value = DataWrapper.Error(it) }
            ).addToDisposables(disposables)
    }

    /**
     * Every time user types some value in the FROM field. Asynchronous method, will return the validation of
     * the [amount] on [onValidate].
     */
    fun onAmountFromChanged(amount: BigDecimal, onValidate: (ExchangeValidation) -> Unit) {
        val validationFrom = validateFromAmount(amount)

        convertAmount(amount)
            .flatMap({ calculateConversion(BigDecimal(amount.toString())) },
                { (toAmount, toInCurrency, validationTo), conversionRate ->
                    ExchangeValidation(
                        convertToCurrency(amount, token = fromToken.token),
                        validationFrom,
                        conversionRate,
                        toAmount,
                        toInCurrency,
                        validationTo,
                        validationFrom.valid && validationTo.valid && oneIsXTZ()
                    )
                })
            .subscribeForUI()
            .subscribe(
                {
                    onValidate(it)
                },
                { _data.value = DataWrapper.Error(it) }
            ).addToDisposables(disposables)
    }

    private fun validateFromAmount(amount: BigDecimal): ExchangeAmountValidation {
        val dexterBalance = if (fromToken.token.isXTZ()) {
            (toToken.exchange as? FATokenInfo)?.dexterBalance
        } else {
            (fromToken.exchange as? FATokenInfo)?.dexterBalance
        }
        return dexterBalance?.let { runValidations(amount, validationsFrom, fromToken, dexterBalance) }
            ?: ExchangeAmountValidation.BaseValidation(false)
    }

    private fun validateToAmount(amount: BigDecimal): ExchangeAmountValidation {
        val dexterBalance = if (fromToken.token.isXTZ()) {
            (toToken.exchange as? FATokenInfo)?.dexterBalance
        } else {
            (fromToken.exchange as? FATokenInfo)?.dexterBalance
        }
        return dexterBalance?.let {
            runValidations(amount, validationsTo, toToken, dexterBalance)
        } ?: ExchangeAmountValidation.BaseValidation(false)
    }

    /**
     * @param validations Group of [validateFromAmount] and [validateToAmount]
     */
    private fun runValidations(
        amount: BigDecimal,
        validations: List<AmountValidation>,
        info: TokenInfoBundle,
        dexterBalance: DexterBalance
    ): ExchangeAmountValidation {
        for (validation in validations) {
            val runned = runValidation(validation, amount, info, dexterBalance)
            if (!runned.valid) {
                // No need to keep looping if there's already a non-valid verification
                return runned
            }
        }
        return ExchangeAmountValidation.BaseValidation(true)
    }

    private fun runValidation(
        validation: AmountValidation,
        amount: BigDecimal,
        info: TokenInfoBundle,
        dexterBalance: DexterBalance
    ): ExchangeAmountValidation {
        val valid = ExchangeAmountValidation.BaseValidation(true)
        return when (validation) {
            AmountValidation.OVER_ZERO -> ExchangeAmountValidation.ZeroValidation(amount > BigDecimal.ZERO)
            AmountValidation.BALANCE -> ExchangeAmountValidation.BaseValidation(info.balance.bigDecimal >= amount)
            AmountValidation.TOKEN_POOL -> {
                if (info.token.isXTZ()) {
                    valid
                } else {
                    val tokenPool = dexterBalance.tokens
                    ExchangeAmountValidation.TokenPoolBalance(
                        tokenPool.toBigDecimal() >= amount,
                        tokenPool
                    )
                }
            }
            AmountValidation.TOKEN_BALANCE -> {
                if (info.token.isXTZ()) {
                    val tezBalance = dexterBalance.balance
                    ExchangeAmountValidation.TokenTezBalance(
                        tezBalance >= Tez(amount),
                        tezBalance
                    )
                } else {
                    valid
                }
            }
            AmountValidation.NONE -> valid
        }
    }

    private fun convertToCurrency(
        amount: BigDecimal,
        currencyExchange: Double = fromToken.exchange.currencyExchange,
        token: Token
    ): String {
        // Since Dexter rates are going to jump dramatically, hide the currency value to avoid confusion
        return if (token.isXTZ()) {
            Tez(amount).convertToCurrency(currencyExchange).formatAsMoney(currencyBundle)
        } else {
            ""
        }
    }

    /**
     * Second step when user types a value: Calculate the TO and other related values.
     *
     * @return The TO amount formatted, the TO amount converted to currency and the Validation of TO amount
     */
    private fun convertAmount(amount: BigDecimal): Single<Triple<String, String, ExchangeAmountValidation>> {
        return if (amount.compareTo(BigDecimal.ZERO) == 0) {
            Single.just(
                Triple(
                    parseSetAmount(BigDecimal.ZERO, toToken.token),
                    BigDecimal.ZERO.formatAsMoney(currencyBundle),
                    validateToAmount(amount)
                )
            )
        } else {
            calculationsUseCase.calculate(fromToken, toToken, amount)
                .map {
                    exchange = it
                    val toAmount = exchange!!.valueToBuy
                    Triple(
                        parseSetAmount(toAmount, toToken.token),
                        convertToCurrency(toAmount, toToken.exchange.currencyExchange, toToken.token),
                        validateToAmount(exchange!!.amountTo.bigDecimal)
                    )
                }
        }
    }

    private fun parseSetAmount(amount: BigDecimal, token: Token): String {
        return when {
            token.isXTZ() -> {
                amount.formatWithDecimals(token)
            }
            token.decimals > 0 -> {
                amount.formatAsToken(token.decimals)
            }
            else -> {
                amount.roundTokenValue().formatAsToken()
            }
        }
    }

    private fun BigDecimal.roundTokenValue(): BigInteger {
        val rounded = if (this.toBigInteger().compareTo(BigInteger.ZERO) == 0) {
            this.setScale(0, RoundingMode.UP)
        } else {
            this.setScale(0, RoundingMode.DOWN)
        }
        return rounded.toBigInteger()
    }

    fun oneIsXTZ(): Boolean {
        return fromToken.token.isXTZ() || toToken.token.isXTZ()
    }

    fun setTokenFrom(token: Token) {
        if (fromToken.token != token) {
            balances.firstOrNull { it.token == token }?.apply {
                fromToken = this
            }

            // Avoid the situation where we have XTZ to XTZ
            if (token.isXTZ() && toToken.token.isXTZ()) {
                balances.firstOrNull { it.token == supportedFATokens[0] }?.apply {
                    toToken = this
                }
                // Avoid the situation where we have FA1.2 to FA1.2
            } else if (!token.isXTZ() && !toToken.token.isXTZ()) {
                balances.firstOrNull { it.token == XTZ }?.apply {
                    toToken = this
                }
            }

            update()
        }
    }

    /**
     * Get max value of trading token
     */
    fun getBalanceFrom(): String {
        return fromToken.balance.bigDecimal.toPlainString()
    }

    fun setTokenTo(token: Token) {
        if (toToken.token != token) {
            balances.firstOrNull { it.token == token }?.apply {
                toToken = this
            }

            // Avoid the situation where we have XTZ to XTZ
            if (token.isXTZ() && fromToken.token.isXTZ()) {
                balances.firstOrNull { it.token == supportedFATokens[0] }?.apply {
                    fromToken = this
                }
            }
            update()
        }
    }

    fun getRequest(fromAmount: BigDecimal) {
        _operation.value = DataWrapper.Loading()

        calculationsUseCase.calculatePriceImpact(
            fromAmount,
            fromToken,
            toToken
        ).map {
            val balance: Balance<*> = if (fromToken.token.isXTZ()) {
                CoinBalance(Tez(fromAmount))
            } else {
                TokenBalance(
                    fromAmount,
                    fromToken.token.decimals
                )
            }

            SwapRequest(
                fromToken.token,
                toToken.token,
                balance,
                EmptyBalance(),
                selectedSlippage,
                it,
                timeoutMinutes
            )
        }
            .subscribeForUI()
            .subscribe(
                { _operation.value = DataWrapper.Success(it) },
                { _operation.value = DataWrapper.Error(it) }
            ).addToDisposables(disposables)
    }

    fun getCurrencyBundle(): CurrencyBundle {
        return useCase.getCurrencyBundle()
    }
}

data class ExchangeForView(
    val fromToken: Token,
    val toToken: Token,
    val maxFrom: BigDecimal,
    val fromBalance: String,
    val conversionRate: String
)

/**
 * @param enableExchange Enable button to continue with operation
 */
data class ExchangeValidation(
    val amountFromInCurrency: String,
    val fromValidation: ExchangeAmountValidation,
    val conversionRate: String,
    val amountTo: String,
    val amountToInCurrency: String,
    val toValidation: ExchangeAmountValidation,
    val enableExchange: Boolean
)
