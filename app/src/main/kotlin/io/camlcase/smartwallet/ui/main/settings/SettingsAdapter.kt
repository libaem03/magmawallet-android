/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.main.settings

import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.style.ImageSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.switchmaterial.SwitchMaterial
import io.camlcase.smartwallet.BuildConfig
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.core.extension.addEndExternalLink
import io.camlcase.smartwallet.core.extension.hide
import io.camlcase.smartwallet.core.extension.show
import io.camlcase.smartwallet.core.extension.throttledClick
import io.camlcase.smartwallet.data.core.extension.tezos.isMainnet
import io.camlcase.smartwallet.ui.base.showToast
import kotlinx.android.synthetic.main.row_setting_header.view.*
import kotlinx.android.synthetic.main.row_settings.view.*

/**
 * List of settings options
 */
class SettingsAdapter(
    private val values: List<SettingsItem>,
    private val onItemClick: (SettingsForView, View) -> Unit
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    inner class HeaderViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val header: TextView = view.header
    }

    inner class DeleteWalletViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val container: ConstraintLayout = view.setting_container
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val container: ConstraintLayout = view.setting_container
        val title: TextView = view.setting_title
        val detail: TextView = view.setting_detail
        val switch: SwitchMaterial = view.btn_switch
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            HEADER -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.row_setting_header, parent, false)
                HeaderViewHolder(view)
            }
            DELETE_WALLET -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.row_settings_delete, parent, false)
                DeleteWalletViewHolder(view)
            }
            else -> {
                val view = LayoutInflater.from(parent.context).inflate(R.layout.row_settings, parent, false)
                ViewHolder(view)
            }
        }
    }

    override fun getItemCount(): Int {
        return values.size
    }

    override fun getItemViewType(position: Int): Int {
        val item = values[position]
        return when (item.type) {
            SettingType.HEADER_UPDATE,
            SettingType.HEADER_DELEGATE,
            SettingType.HEADER_SECURITY,
            SettingType.HEADER_ABOUT,
            SettingType.HEADER_OTHER -> HEADER
            SettingType.DELETE_WALLET -> DELETE_WALLET
            else -> SETTINGS
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = values[position]
        when (holder) {
            is HeaderViewHolder -> {
                bindHeader(item as HeaderSettings, holder)
            }
            is ViewHolder -> {
                bindSettings(item as SettingsForView, holder)
            }
            else -> {
                // DELETE WALLET
                setItemClick(holder.itemView, item as SettingsForView)
            }
        }
    }

    private fun bindHeader(header: HeaderSettings, holder: HeaderViewHolder) {
        val context = holder.itemView.context
        val title: String = when (header.type) {
            SettingType.HEADER_UPDATE -> context.getString(R.string.set_security_title)
            SettingType.HEADER_DELEGATE -> context.getString(R.string.set_group_staking)
            SettingType.HEADER_SECURITY -> context.getString(R.string.set_group_security)
            SettingType.HEADER_ABOUT -> context.getString(R.string.set_group_about)
            SettingType.HEADER_OTHER -> context.getString(R.string.set_group_other)
            else -> ""
        }
        holder.header.text = title
    }

    private fun bindSettings(item: SettingsForView, holder: ViewHolder) {
        val context = holder.itemView.context

        // TITLE
        val title: String? = when (item.type) {
            SettingType.BIOMETRIC_LOGIN -> context.getString(
                R.string.set_biometric,
                context.getString(R.string.android_onb_quick_unlock)
            )
            SettingType.EDIT_PIN -> context.getString(R.string.set_edit_pin)
            SettingType.DELEGATE -> context.getString(R.string.set_baker)
            SettingType.RECOVERY_PHRASE -> context.getString(R.string.set_recovery)
            SettingType.ABOUT -> context.getString(R.string.set_about)
            SettingType.FEEDBACK -> context.getString(R.string.set_share_feedback)
            SettingType.PRIVACY_POLICY -> context.getString(R.string.set_privacy)
            SettingType.TERMS -> context.getString(R.string.set_terms)
            SettingType.NEWSLETTER -> context.getString(R.string.set_newsletter)
            SettingType.ATTRIBUTIONS -> context.getString(R.string.set_attributions_title)
            SettingType.MIGRATE_WALLET -> context.getString(R.string.set_migration_title)
            SettingType.NETWORK_CONFIG -> context.getString(R.string.set_dynamic_urls_title)
            SettingType.LOCAL_CURRENCY -> context.getString(R.string.set_currency)
            SettingType.DELETE_WALLET -> null
            SettingType.VERSION -> context.getString(
                R.string.set_version,
                "${BuildConfig.VERSION_NAME} (${BuildConfig.VERSION_CODE})"
            )
            else -> null
        }
        title?.apply {
            holder.title.text = title
            if (item.external) {
                holder.title.addEndExternalLink()
            }
        }

        // DETAIL
        if (item.type == SettingType.MIGRATE_WALLET) {
            val text = "   ${context.getString(R.string.set_migration_detail)}"
            val builder = SpannableStringBuilder(text)
            val imageSpan = ImageSpan(context, R.drawable.ic_warning)
            builder.setSpan(imageSpan, 0, 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
            holder.detail.show()
            holder.detail.text = builder
        } else {
            val detail: String? = when (item.type) {
                SettingType.VERSION -> if (BuildConfig.FLAVOR.isMainnet()) {
                    "Mainnet"
                } else {
                    "Testnet"
                }
                else -> item.detail
            }
            setDetailText(detail, holder)
        }

        // SWITCH
        val showSwitch = item.type == SettingType.BIOMETRIC_LOGIN
        if (showSwitch) {
            showSwitch(item, holder)
            // CLICK
            holder.itemView.throttledClick {
                holder.switch.toggle()
            }
        } else {
            holder.switch.hide()
            // CLICK
            setItemClick(holder.itemView, item)
        }

        if (item.type == SettingType.VERSION) {
            holder.itemView.setOnLongClickListener {
                val winkySmile =
                    String(Character.toChars(0x1F30B) + Character.toChars(0x2764) + Character.toChars(0x1F42B))
                context.showToast(winkySmile)
                true
            }
        } else {
            holder.itemView.setOnLongClickListener(null)
        }
    }

    private fun setItemClick(view: View, item: SettingsForView) {
        view.throttledClick {
            onItemClick(item, view)
        }
    }

    private fun showSwitch(item: SettingsForView, holder: ViewHolder) {
        holder.switch.show()
        holder.switch.isChecked = item.checked
        holder.switch.setOnCheckedChangeListener { buttonView, isChecked ->
            val toggleItem = SettingsForView(item.type, item.detail, isChecked)
            onItemClick(toggleItem, buttonView)
        }
    }

    private fun setDetailText(text: String?, holder: ViewHolder) {
        text?.let {
            holder.detail.show()
            holder.detail.text = it
        } ?: holder.detail.hide()
    }

    companion object {
        const val HEADER = 0
        const val SETTINGS = 1
        const val DELETE_WALLET = 2
    }
}
