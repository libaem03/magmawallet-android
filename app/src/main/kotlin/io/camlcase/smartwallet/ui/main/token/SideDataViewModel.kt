/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.main.token

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import io.camlcase.smartwallet.core.architecture.BaseViewModel
import io.camlcase.smartwallet.core.extension.addToDisposables
import io.camlcase.smartwallet.core.extension.subscribeForUI
import io.camlcase.smartwallet.data.analytic.report
import io.camlcase.smartwallet.data.core.DataRxThread
import io.camlcase.smartwallet.data.core.Logger
import io.camlcase.smartwallet.data.usecase.delegate.GetDelegateUseCase
import io.camlcase.smartwallet.data.usecase.info.GetCurrencyRatesUseCase
import io.camlcase.smartwallet.data.usecase.money.GetMoonPayDataUseCase
import javax.inject.Inject

/**
 * Retrieve extra useful data and store it
 */
class SideDataViewModel @Inject constructor(
    private val delegateUseCase: GetDelegateUseCase,
    private val currenciesUseCase: GetCurrencyRatesUseCase,
    private val moonPayDataUseCase: GetMoonPayDataUseCase
) : BaseViewModel() {
    /**
     * If we retrieved our curated delegates list to the network, stream a signal
     */
    private val _bakersUpdate = MutableLiveData<Boolean>()
    val bakersUpdate: LiveData<Boolean> = _bakersUpdate

    fun getData() {
        saveDelegate()
        saveCuratedBakers()
        updateAvailableCurrencies()
        updateMoonPaySupportedCountries()
    }

    private fun saveCuratedBakers() {
        delegateUseCase.updateDelegates()
            .subscribeForUI()
            .subscribe(
                {
                    Logger.d("Bakers List Update: $it")
                    if (it) {
                        _bakersUpdate.value = it
                    }
                },
                { logAndReport(it) }
            ).addToDisposables(disposables)
    }

    private fun saveDelegate() {
        delegateUseCase.fetchDelegate()
            .subscribeOn(DataRxThread.newThread())
            .subscribe(
                { Logger.d("DELEGATE: $it") },
                { Logger.e(it.toString()) }
            ).addToDisposables(disposables)
    }

    private fun updateMoonPaySupportedCountries() {
        moonPayDataUseCase.updateMoonPayXTZData()
            .subscribeOn(DataRxThread.newThread())
            .subscribe(
                { Logger.d("MoonPay updated supported countries/currencies: $it") },
                {
                    logAndReport(it)
                }
            ).addToDisposables(disposables)
    }

    private fun logAndReport(error: Throwable) {
        error.report()
        Logger.e(error.toString())
    }

    private fun updateAvailableCurrencies() {
        currenciesUseCase.updateAvailableCurrencies()
            .subscribeOn(DataRxThread.newThread())
            .subscribe(
                { Logger.d("CoinGecko currencies list updated: $it") },
                {   logAndReport(it) }
            ).addToDisposables(disposables)
    }
}
