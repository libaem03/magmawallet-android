/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.delegate

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import io.camlcase.kotlintezos.model.Address
import io.camlcase.smartwallet.core.architecture.BaseViewModel
import io.camlcase.smartwallet.core.extension.isMainnet
import io.camlcase.smartwallet.core.extension.subscribeForUI
import io.camlcase.smartwallet.data.model.delegate.TezosBaker
import io.camlcase.smartwallet.data.model.delegate.TezosBakerResponse
import io.camlcase.smartwallet.data.usecase.delegate.GetDelegateUseCase
import io.camlcase.smartwallet.model.DataWrapper
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

class DelegateListViewModel @Inject constructor(
    private val useCase: GetDelegateUseCase
) : BaseViewModel() {
    /**
     * Bakers list
     */
    private val _data = MutableLiveData<DataWrapper<DelegateData>>()
    val data: LiveData<DataWrapper<DelegateData>> = _data

    private var bakers: List<TezosBakerResponse> = emptyList()

    fun getDelegates(undelegated: Boolean) {
        _data.value = DataWrapper.Loading()
        Single.zip(
            useCase.supportedDelegates(),
            useCase.getDelegate(),
            { list, current -> parseDelegates(list, current.getOrNull(), undelegated) }
        )
            .map { DelegateData(it, isMainnet()) }
            .subscribeForUI()
            .subscribe(
                { _data.value = DataWrapper.Success(it) },
                { _data.value = DataWrapper.Error(it) }
            )
    }

    private fun parseDelegates(
        bakers: List<TezosBakerResponse>,
        currentDelegate: TezosBaker?,
        undelegated: Boolean
    ): List<DelegateForView> {
        var currentInList = false
        var removed = false
        this.bakers = bakers.sortedByDescending { it.estimatedRoi }
        val list = ArrayList<DelegateForView>()
        for (baker in this.bakers) {
            if (baker.address == currentDelegate?.address) {
                // Mark once to be used later on
                currentInList = true
                removed = undelegated
            }
            list.add(
                DelegateForView(
                    baker.name,
                    baker.address,
                    baker.logo,
                    baker.address == currentDelegate?.address,
                    removed
                )
            )
        }

        if (!currentInList && currentDelegate != null) {
            list.add(0, DelegateForView(currentDelegate.name, currentDelegate.address, null, true, undelegated))
        }
        list.sortByDescending { it.current }
        return list
    }

    /**
     * Testnet bakers are repeated. We need to compare by both fields so we show the correct one.
     */
    fun getBaker(address: Address, name: String? = null): TezosBakerResponse {
        return bakers.firstOrNull { it.address == address && it.name == name } ?: TezosBakerResponse("", address)
    }
}

data class DelegateData(
    val delegates: List<DelegateForView>,
    val isMainnet: Boolean
)

data class DelegateForView(
    val name: String?,
    val address: Address,
    val imageUrl: String?,
    val current: Boolean = false,
    /**
     * User is [current] but has been selected to be removed
     */
    val removed: Boolean = false
) {
    val removable
        get() = current && !removed
}
