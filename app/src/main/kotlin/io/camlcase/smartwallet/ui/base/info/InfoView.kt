/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.base.info

import android.content.Context
import android.util.AttributeSet
import android.util.TypedValue
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.core.extension.addToDisposables
import io.camlcase.smartwallet.core.extension.show
import io.camlcase.smartwallet.core.extension.throttledClick
import io.camlcase.smartwallet.databinding.ViewInfoBinding
import io.camlcase.smartwallet.ui.base.FontCache
import io.reactivex.rxjava3.disposables.CompositeDisposable

/**
 * Custom view to show information populated with an [InfoType] object
 */
class InfoView(context: Context, attrs: AttributeSet?) : ConstraintLayout(context, attrs) {
    interface InfoEventsListener {
        /**
         * Since an activity can show several info fragments, we pass the [infoType] to the listener
         * so it knows whom to react to
         */
        fun onActionClick(infoType: InfoType)

        fun onSecondaryActionClick(infoType: InfoType) {}
    }

    var disposables: CompositeDisposable = CompositeDisposable()
    private val binding = ViewInfoBinding.inflate(LayoutInflater.from(context), this, true)

    fun unbind() {
        disposables.dispose()
    }

    fun init(info: InfoType, listener: InfoEventsListener?, extras: List<String>? = null) {
        setTitle(info)
        setDetail(info, extras)

        val imageId = info.imageId
        if (imageId != null && imageId > 0) {
            binding.image.show()
            binding.image.setImageDrawable(ContextCompat.getDrawable(context, imageId))
        }

        info.animationId?.apply {
            binding.animationView.show()
            binding.animationView.imageAssetsFolder = "lottie_anim_success/"
            binding.animationView.setAnimation(this)
        }

        info.actionText?.apply {
            binding.actionPrimary.setText(this)
            binding.actionPrimary.show()
            binding.actionPrimary.throttledClick()
                .subscribe { listener?.onActionClick(info) }
                .addToDisposables(disposables)
        }
        info.secondaryActionText?.apply {
            binding.actionSecondary.setText(this)
            binding.actionSecondary.show()
            binding.actionSecondary.throttledClick()
                .subscribe { listener?.onSecondaryActionClick(info) }
                .addToDisposables(disposables)
        }
    }

    private fun setTitle(info: InfoType) {
        info.title?.apply {
            binding.title.show()
            binding.title.setText(this)

            if (info.type == InfoTypeKind.ERROR) {
                // Error message titles are smaller
                binding.title.typeface = FontCache["barlow-medium", context]
                binding.title.setTextSize(TypedValue.COMPLEX_UNIT_PX, resources.getDimension(R.dimen.text_size_body))
            }
        }
    }

    private fun setDetail(info: InfoType, extras: List<String>?) {
        info.detail?.apply {
            binding.detail.show()

            if (info.type == InfoTypeKind.ERROR) {
                if (extras.isNullOrEmpty()) {
                    binding.detail.text = context.getString(this)
                } else {
                    binding.detail.text = context.getString(this, extras?.get(0))
                }
            } else {
                binding.detail.setText(this)
            }
        }
        info.detailHighlight?.apply {
            binding.detailHighlight.show()
            binding.detailHighlight.setText(this)
        }
    }
}
