/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.exchange.confirmation

import android.content.Context
import android.os.Bundle
import android.view.*
import by.kirich1409.viewbindingdelegate.viewBinding
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.core.extension.*
import io.camlcase.smartwallet.data.core.Exchange
import io.camlcase.smartwallet.data.core.extension.format
import io.camlcase.smartwallet.data.model.error.*
import io.camlcase.smartwallet.data.model.swap.SwapRequest
import io.camlcase.smartwallet.databinding.FragmentExchangeConfirmationBinding
import io.camlcase.smartwallet.databinding.ViewFullExchangeConfirmationBinding
import io.camlcase.smartwallet.model.DataWrapper
import io.camlcase.smartwallet.model.handleResults
import io.camlcase.smartwallet.model.operation.OperationFeesBundle
import io.camlcase.smartwallet.ui.base.BindBaseFragment
import io.camlcase.smartwallet.ui.base.info.InfoModalFragment
import io.camlcase.smartwallet.ui.base.info.InfoModalType
import io.camlcase.smartwallet.ui.base.info.InfoType
import io.camlcase.smartwallet.ui.base.info.InfoView
import io.camlcase.smartwallet.ui.base.navigate.navigateToModal
import io.camlcase.smartwallet.ui.base.showError
import io.camlcase.smartwallet.ui.custom.ScrollOffsetListener
import io.camlcase.smartwallet.ui.exchange.ExchangeActivity

/**
 * Confirmation for EXCHANGE
 * - General error view
 * - Confirmation view as ViewStub
 */
class SwapConfirmationFragment : BindBaseFragment(R.layout.fragment_exchange_confirmation) {
    interface SwapConfirmationListener {
        fun navigateToLiquidityFeeDetail(liquidityFee: String, inCurrency: String)
        fun navigateToFeesDetail(fees: OperationFeesBundle)
        fun onSwapOperationFinished(fromToken: String, toToken: String, operationHash: String)
    }

    private val binding by viewBinding(FragmentExchangeConfirmationBinding::bind)
    private var confirmViewBinding: ViewFullExchangeConfirmationBinding? = null
    private lateinit var viewModel: SwapConfirmationViewModel
    private var swapRequest: SwapRequest? = null
    private var listener: SwapConfirmationListener? = null
    private var errorListener: InfoView.InfoEventsListener? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        manageIntent(arguments)
        listener = context as? SwapConfirmationListener
        if (listener == null) {
            throw ClassCastException("$context must implement SwapConfirmationListener")
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = viewModel(viewModelFactory) {
            observe(data, { handleDetails(it) })
            observe(operation, { handleOperation(it) })
        }
        viewModel.getConfirmationDetails(swapRequest!!)
    }

    private fun manageIntent(bundle: Bundle?) {
        bundle?.getParcelable<SwapRequest>(ExchangeActivity.ARG_SWAP_FIELDS)?.let {
            swapRequest = it
        } ?: throw IllegalStateException("Screen needs an ARG_SWAP_FIELDS argument")
        errorListener = context as? InfoView.InfoEventsListener
    }

    private fun initGeneralErrorView() {
        confirmViewBinding?.root.hide()
        binding.viewError.show()
    }

    private fun initConfirmationView() {
        binding.viewError.hide()
        confirmViewBinding =
            ViewFullExchangeConfirmationBinding.inflate(LayoutInflater.from(context), binding.root, true)
        initAppbar()
    }

    private fun initAppbar() {
        confirmViewBinding?.apply {
            setupToolbar(this.appBarContainer.toolbar, true)
            setHasOptionsMenu(true)

            val title = getString(R.string.exc_confirm_title)
            this.appBarContainer.expandedTitle.text = title
            this.appBarContainer.appBar.addOnOffsetChangedListener(
                ScrollOffsetListener(
                    this.appBarContainer.expandedTitle,
                    this.appBarContainer.toolbar,
                    title
                )
            )
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        if (viewModel.showHelpOption()) {
            inflater.inflate(R.menu.menu_help, menu)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.help -> {
                val fragment =
                    InfoModalFragment.init(InfoModalType.EXCHANGE_MAX_BALANCE, Exchange.minimumXTZBalance.format(null))
                navigateToModal(fragment)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun handleOperation(wrapper: DataWrapper<String>) {
        wrapper.handleResults(
            onValidData = {
                hideLoading()
                val (fromToken, toToken) = viewModel.getTokens()
                listener?.onSwapOperationFinished(fromToken, toToken, it)
            },
            onError = {
                hideLoading()

                if (it is AppError && it.exception is OperationInProgress) {
                    handleOperationError(it)
                } else {
                    showError(it)
                }
            },
            onLoading = { showLoading() }
        )
    }

    private fun handleDetails(wrapper: DataWrapper<SwapConfirmationForView>) {
        wrapper.handleResults(
            onValidData = {
                hideLoading()
                initConfirmationView()
                bindViews()
                populateData(it)
                confirmViewBinding?.actionNext?.isEnabled = true
            },
            onErrorWithData = { error, data ->
                hideLoading()
                handleInitialError(error, data)
            },
            onLoading = { showLoading() }
        )
    }

    private fun handleInitialError(error: Throwable?, data: SwapConfirmationForView?) {
        if (error is AppError && error.exception is OperationError) {
            handleOperationError(error)
            return
        } else {
            initConfirmationView()

            data?.apply {
                populateData(data)
                confirmViewBinding?.actionNext?.isEnabled = false
            }

            showError(error)
        }
    }

    private fun handleOperationError(error: AppError) {
        initGeneralErrorView()
        val info = when (error.exception) {
            is InsufficientXTZ -> InfoType.EXCHANGE_ERROR_INSUFFICIENT_XTZ
            is ExchangeInvalidTokens -> InfoType.EXCHANGE_ERROR_INVALID_TOKENS
            is ExchangeZeroTokens -> InfoType.EXCHANGE_ERROR_ZERO_TOKENS
            is OperationInProgress -> InfoType.OPERATION_COUNTER
            is ExchangeDeadlineTimeout -> InfoType.EXCHANGE_ERROR_INVALID_DEADLINE
            else -> InfoType.EXCHANGE_INCLUSION_ERROR
        }

        val args = if (info == InfoType.EXCHANGE_INCLUSION_ERROR) {
            listOf(error.exception?.message ?: getString(R.string.error_generic))
        } else {
            null
        }

        binding.viewError.init(info, errorListener, args)
    }

    private fun populateData(data: SwapConfirmationForView) {
        confirmViewBinding?.apply {
            this.sectionsContainer.exchangeSection.setValues(data.formattedFromValue, data.usdFromValue)
            this.sectionsContainer.receiveSection.setValues(data.formattedToValue, data.usdToValue)

            this.sectionsContainer.liquiditySection.setValues(data.formattedLiquidityFee, data.currencyLiquidityValue)
            this.sectionsContainer.liquiditySection.makeClickable()
                .subscribe {
                    listener?.navigateToLiquidityFeeDetail(
                        data.formattedLiquidityFee,
                        data.currencyLiquidityValue
                    )
                }
                .addToDisposables(disposables)

            this.sectionsContainer.networkSection.setValues(data.totalFeesFormatted, data.fees.totalFeesInCurrency)
            this.sectionsContainer.networkSection.makeClickable()
                .subscribe { listener?.navigateToFeesDetail(data.fees) }
                .addToDisposables(disposables)

            this.conversionRate.show()
            this.conversionRate.text = data.exchangeRate
            this.sectionsContainer.priceImpact.show()
            this.sectionsContainer.priceImpact.text = getString(R.string.exc_confirm_price_impact_info, data.priceImpact)
        }

        activity?.invalidateOptionsMenu()
    }

    private fun bindViews() {
        confirmViewBinding?.actionNext?.throttledClick()
            ?.subscribe {
                viewModel.swap()
            }
            ?.addToDisposables(disposables)
    }

    private fun isStubInflated(): Boolean {
        return confirmViewBinding != null
    }

    private fun showLoading() {
        if (isStubInflated()) {
            confirmViewBinding?.swapLoading?.containerLoading.show()
            confirmViewBinding?.actionNext?.isEnabled = false
        } else {
            binding.parentLoading.containerLoading.show()
        }
    }

    private fun hideLoading() {
        if (isStubInflated()) {
            confirmViewBinding?.swapLoading?.containerLoading.hide()
        } else {
            binding.parentLoading.containerLoading.hide()
        }
    }

    companion object {
        fun init(request: SwapRequest): SwapConfirmationFragment {
            val fragment = SwapConfirmationFragment()
            val args = Bundle()
            args.putParcelable(ExchangeActivity.ARG_SWAP_FIELDS, request)
            fragment.arguments = args
            return fragment
        }
    }
}
