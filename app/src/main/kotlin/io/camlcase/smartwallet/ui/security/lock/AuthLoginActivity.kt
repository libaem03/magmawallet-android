/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.security.lock

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.ViewTreeObserver
import android.widget.TextView
import androidx.core.widget.doOnTextChanged
import by.kirich1409.viewbindingdelegate.viewBinding
import com.google.android.material.button.MaterialButton
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.core.extension.*
import io.camlcase.smartwallet.data.usecase.analytics.ScreenEvent
import io.camlcase.smartwallet.databinding.ActivityAuthLoginBinding
import io.camlcase.smartwallet.model.navigation.WelcomeNavigation
import io.camlcase.smartwallet.ui.base.BaseActivity
import io.camlcase.smartwallet.ui.base.navigate.AppNavigator
import io.camlcase.smartwallet.ui.base.navigate.CLEAR_FLAGS
import io.camlcase.smartwallet.ui.base.showSnackbar
import io.camlcase.smartwallet.ui.custom.ScrollOffsetListener
import io.camlcase.smartwallet.ui.security.initBiometrics
import io.camlcase.smartwallet.ui.security.pin.PinUIValidation
import io.camlcase.smartwallet.ui.security.userCancelledBiometricPrompt
import java.lang.ref.WeakReference

class AuthLoginActivity : BaseActivity(R.layout.activity_auth_login) {
    private val binding by viewBinding(ActivityAuthLoginBinding::bind)
    private lateinit var authViewModel: AuthLoginViewModel
    private var pinValidation: PinUIValidation? = null
    private var keyboardListener: ViewTreeObserver.OnGlobalLayoutListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        markSecure()
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        reportView(ScreenEvent.LOGIN_AUTH_PIN)
        initToolbar()
        binding.mainButton.actionNext.text = getString(R.string.action_continue)
        binding.actionBiometric.text =
            getString(R.string.log_biometric_button, getString(R.string.android_onb_quick_unlock))
        initPinInput()
        keyboardListener = initKeyboardListener(binding.mainButton.actionNext)
        bindViews()
        authViewModel = viewModel(viewModelFactory) {}
        // Instantiate now that we have context and views are created
        pinValidation = object : PinUIValidation {
            override val viewModel: AuthLoginViewModel = authViewModel
            override val buttonRef: WeakReference<MaterialButton> = WeakReference(binding.mainButton.actionNext)
            override val errorViewRef: WeakReference<TextView> = WeakReference(binding.pin.pinError)
            override val generalError: String = getString(R.string.error_invalid_pin)

            override fun onCorrectAuthentication(fromInput: Boolean) {
                viewModel.onValidAuthentication()
                binding.pin.pinInput.hideKeyboard()
                setResult(Activity.RESULT_OK)
                finishAfterTransition()
            }

            override fun showAlgorithmError() {
                val message = getString(R.string.error_generic) + " " + getString(R.string.error_try_again)
                binding.containerAuth.showSnackbar(message)
                binding.pin.pinInput.setText("")
            }
        }
    }

    private fun bindViews() {
        binding.mainButton.actionNext.throttledClick()
            .subscribe { pinValidation?.validatePinFromButton(binding.pin.pinInput.text.toString()) }
            .addToDisposables(disposables)
    }

    override fun onResume() {
        super.onResume()
        initBiometric()
    }

    private fun initToolbar() {
        setSupportActionBar(binding.collapsingToolbar.toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        setBackNavigation(binding.collapsingToolbar.toolbar, true)

        binding.collapsingToolbar.expandedTitle.text = getString(R.string.log_title)
        binding.collapsingToolbar.appBar.addOnOffsetChangedListener(
            ScrollOffsetListener(
                binding.collapsingToolbar.expandedTitle,
                binding.collapsingToolbar.toolbar,
                getString(R.string.log_title)
            )
        )
    }

    private fun initPinInput() {
        binding.pin.pinInput.doOnTextChanged { text, _, _, _ ->
            val input = text.toString()
            pinValidation?.validatePinFromInput(input)
        }
    }

    private fun initBiometric() {
        val biometricAuth = authViewModel.biometricEnabled() && promptBiometric()
        if (!biometricAuth) {
            focusPinInput()
            binding.actionBiometric.hide()
        } else {
            binding.actionBiometric.throttledClick()
                .subscribe {
                    promptBiometric()
                }
                .addToDisposables(disposables)
        }
    }

    private fun promptBiometric(): Boolean {
        return initBiometrics(binding.scrollView,
            onAuthSuccess = {
                analyticsUseCase.reportScreen(ScreenEvent.LOGIN_AUTH_BIOMETRIC)
                pinValidation?.onCorrectAuthentication(false)
            },
            onAuthError = { code, message ->
                if (code.userCancelledBiometricPrompt()) {
                    focusPinInput()
                } else {
                    binding.scrollView.showSnackbar(message.toString())
                }
            })
    }

    private fun focusPinInput() {
        binding.pin.pinInput.showInitKeyboard()
    }

    /**
     * If user forgot PIN, they can re-import their wallet by tapping back press and re-importing their wallet.
     */
    override fun onBackPressed() {
        super.onBackPressed()
        authViewModel.unloggedUser()
        AppNavigator.navigateToWelcome(this, CLEAR_FLAGS, WelcomeNavigation.SOURCE_LOGIN)
    }

    override fun onDestroy() {
        clearKeyboardListener(keyboardListener)
        super.onDestroy()
    }

    companion object {
        fun init(context: Context): Intent {
            return Intent(context, AuthLoginActivity::class.java)
        }
    }
}

