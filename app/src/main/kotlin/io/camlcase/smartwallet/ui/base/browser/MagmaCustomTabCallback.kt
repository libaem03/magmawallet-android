/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.base.browser

import android.os.Bundle
import androidx.browser.customtabs.CustomTabsCallback
import io.camlcase.smartwallet.core.customtabs.CustomTabActivityHelper

/**
 * To be used on [CustomTabActivityHelper] init
 * @see BuyTezFragment
 */
class MagmaCustomTabCallback(
    val listener: CustomTabNavigationEventListener
) : CustomTabsCallback() {
    override fun onNavigationEvent(navigationEvent: Int, extras: Bundle?) {
        listener.onNavigationEvent(CustomTabNavigationEvent.get(navigationEvent))
    }
}

interface CustomTabNavigationEventListener {
    fun onNavigationEvent(event: CustomTabNavigationEvent)
}

/**
 * @see CustomTabsCallback attributes
 */
enum class CustomTabNavigationEvent(val code: Int) {
    /**
     * Sent when the tab has started loading a page.
     * NAVIGATION_STARTED
     */
    LOADING_START(CustomTabsCallback.NAVIGATION_STARTED),

    /**
     * Sent when the tab has finished loading a page.
     * NAVIGATION_FINISHED
     */
    LOADING_FINISHED(CustomTabsCallback.NAVIGATION_FINISHED),

    /**
     * Sent when the tab couldn't finish loading due to a failure.
     * NAVIGATION_FAILED
     */
    LOADING_FAILURE(CustomTabsCallback.NAVIGATION_FAILED),

    /**
     * Sent when loading was aborted by a user action before it finishes like clicking on a link
     * or refreshing the page.
     * NAVIGATION_ABORTED
     */
    LOADING_ABORTED(CustomTabsCallback.NAVIGATION_ABORTED),

    /**
     * Sent when the tab becomes visible.
     */
    TAB_SHOWN(CustomTabsCallback.TAB_SHOWN),

    /**
     * Sent when the tab becomes hidden.
     */
    TAB_HIDDEN(CustomTabsCallback.TAB_HIDDEN);

    companion object {
        fun get(code: Int): CustomTabNavigationEvent {
            return values().firstOrNull { it.code == code } ?: TAB_HIDDEN
        }
    }
}
