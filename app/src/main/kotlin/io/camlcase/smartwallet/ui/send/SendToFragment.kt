/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.send

import android.content.Context
import android.graphics.Typeface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.widget.doOnTextChanged
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.tabs.TabLayout
import io.camlcase.kotlintezos.model.Address
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.core.extension.*
import io.camlcase.smartwallet.data.core.extension.tezos.validTezosAddress
import io.camlcase.smartwallet.data.model.contact.MagmaContact
import io.camlcase.smartwallet.model.DataWrapper
import io.camlcase.smartwallet.model.handleResults
import io.camlcase.smartwallet.ui.base.BaseFragment
import io.camlcase.smartwallet.ui.base.bind
import io.camlcase.smartwallet.ui.base.navigate.AppNavigator
import io.camlcase.smartwallet.ui.base.navigate.RequestCode
import io.camlcase.smartwallet.ui.base.showError
import io.camlcase.smartwallet.ui.custom.AdvancedScrollOffsetListener
import io.camlcase.smartwallet.ui.main.contact.ContactListAdapter
import io.camlcase.smartwallet.ui.main.contact.ContactListViewModel
import io.camlcase.smartwallet.ui.main.contact.edit.AddContactActivity
import io.camlcase.smartwallet.ui.permissions.ReadContactsPermission
import io.camlcase.smartwallet.ui.qr.ScanQRActivity
import kotlinx.android.synthetic.main.fragment_send_to.*
import kotlinx.android.synthetic.main.view_bottom_action_button.*
import kotlinx.android.synthetic.main.view_collapsing_toolbar_contact.*
import kotlinx.android.synthetic.main.view_collapsing_toolbar_contact.view.*
import kotlinx.android.synthetic.main.view_send_to_address.*
import kotlinx.android.synthetic.main.view_send_to_contact.*
import kotlinx.android.synthetic.main.view_toggle_tab.view.*

class SendToFragment : BaseFragment() {
    interface AddressSetListener {
        fun scanQR()
        fun onAddressSet(to: String)
        fun onContactSelected(contact: MagmaContact)
    }

    private var listener: AddressSetListener? = null
    private lateinit var viewModel: GetUserAddressViewModel
    private lateinit var contactsViewModel: ContactListViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_send_to, container, false)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        listener = context as? AddressSetListener
        if (listener == null) {
            throw ClassCastException("$context must implement AddressSetListener")
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initAppbar()
        list.apply {
            layoutManager = LinearLayoutManager(activity)
            setHasFixedSize(true)
        }
        keyboardListener = initKeyboardListener(action_next)
        action_next.setText(R.string.action_continue)
        input_address.textField()?.showInitKeyboard()
        input_address.textField()?.setHint(R.string.snd_recipient_hint)
        input_address.endIcon()?.contentDescription = getString(R.string.snd_scan_qr)
        input_address.endIcon()?.show()
    }

    private fun initAppbar() {
        setupToolbar(toolbar, true)
        val title = getString(R.string.snd_recipient_title)
        app_bar.addOnOffsetChangedListener(
            AdvancedScrollOffsetListener { _, state ->
                when (state) {
                    AdvancedScrollOffsetListener.State.EXPANDED -> {
                        expanded_title_layout.show()
                        if (switcher.displayedChild == 1) {
                            expanded_title_layout.expanded_add_contact.show()
                        }
                        toolbar.title = null
                        action_add_contact.hide()
                    }
                    AdvancedScrollOffsetListener.State.COLLAPSED -> {
                        expanded_title_layout.hide()
                        toolbar.title = title
                        if (switcher.displayedChild == 1) {
                            action_add_contact.show()
                        }
                    }
                    AdvancedScrollOffsetListener.State.IDLE -> {
                    }
                }
            }
        )
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        bindViews()
        bindScreenResults()
        viewModel = viewModel(viewModelFactory) {}
        viewModel.getUserAddress()
        contactsViewModel = viewModel(viewModelFactory) {
            observe(data, { handleContacts(it) })
        }
        refreshContacts()
    }

    private fun refreshContacts() {
        contactsViewModel.getContacts(ReadContactsPermission.hasPermission(context))
    }

    private fun handleContacts(wrapper: DataWrapper<List<MagmaContact>>) {
        wrapper.handleResults(
            onValidData = {
                hideLoading()
                empty_status.hide()
                list.show()
                // Populate list
                list.adapter = ContactListAdapter(it) { contact, view ->
                    view.setOnClickListener {
                        listener?.onContactSelected(contact)
                    }
                }
            },
            onError = {
                hideLoading()
                showError(it)
                if (list.adapter?.itemCount == 0) {
                    showEmpty()
                }
            },
            onEmpty = {
                hideLoading()
                showEmpty()
            },
            onLoading = {
                showLoading()
            }
        )
    }

    private fun bindViews() {
        bindAddressViews()

        action_add_contact.throttledClick()
            .mergeWith(expanded_add_contact.throttledClick())
            .subscribe {
                navigateToAddContact(null)
            }
            .addToDisposables(disposables)

        bindToggle()

        action_next.throttledClick()
            .subscribe {
                input_address.textField()?.hideKeyboard()
                val address = input_address.text
                val contact = contactsViewModel.existsInContacts(address)
                if (contact != null) {
                    listener?.onContactSelected(contact)
                } else {
                    listener?.onAddressSet(address)
                }
            }
            .addToDisposables(disposables)
    }

    /**
     * - Moves the ViewSwitcher to the correct view
     * - Sets unselected to NORMAL, selected to BOLD
     */
    private fun bindToggle() {
        fun styleText(tabView: TabLayout.TabView, style: Int, action: (TextView) -> Unit = {}) {
            for (i in 0..tabView.childCount) {
                val childAt = tabView.getChildAt(i)
                if (childAt is TextView) {
                    childAt.setTypeface(null, style)
                    action(childAt)
                }
            }
        }

        // Set bottom margin 1dp & style init typeface
        select_to.toggle_tab_layout.getTabAt(0)?.view?.apply {
            styleText(this, Typeface.BOLD) {
                val layoutParams: LinearLayout.LayoutParams = it.layoutParams as LinearLayout.LayoutParams
                layoutParams.setMargins(0, 0, 0, 1)
                it.layoutParams = layoutParams
            }
        }
        select_to.toggle_tab_layout.getTabAt(1)?.view?.apply {
            styleText(this, Typeface.NORMAL) {
                val layoutParams: LinearLayout.LayoutParams = it.layoutParams as LinearLayout.LayoutParams
                layoutParams.setMargins(0, 0, 0, 1)
                it.layoutParams = layoutParams
            }
        }

        // On tab selected listener
        select_to.toggle_tab_layout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab?) {
                if (tab?.position == 0) {
                    switcher.showPrevious()
                    button_container.show()
                    if (app_bar.expanded_title_layout.isVisible()) {
                        app_bar.expanded_title_layout.expanded_add_contact.invisible()
                    } else {
                        app_bar.action_add_contact.hide()
                    }
                }

                if (tab?.position == 1) {
                    input_address.hideKeyboard()
                    switcher.showNext()
                    button_container.hide()
                    if (app_bar.expanded_title_layout.isVisible()) {
                        app_bar.expanded_title_layout.expanded_add_contact.show()
                    } else {
                        app_bar.action_add_contact.show()
                    }
                }

                tab?.view?.apply {
                    styleText(this, Typeface.BOLD)
                }
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
                tab?.view?.apply {
                    styleText(this, Typeface.NORMAL)
                }
            }

            override fun onTabReselected(tab: TabLayout.Tab?) {
                // Nothing to do
            }
        })
    }

    private fun bindAddressViews() {
        input_address.bindIcon(disposables) {
            listener?.scanQR()
        }

        input_address.textField()?.doOnTextChanged { text, _, _, _ ->
            val input = text.toString()

            val isUserAddress = isUserAddress()
            val validAddress = input.validTezosAddress() && !isUserAddress
            if (input.isEmpty() || validAddress) {
                input_address.dismissError()
                if (validAddress) {
                    action_next.isEnabled = true
                    if (contactsViewModel.existsInContacts(input) == null) {
                        action_add.show()
                    } else {
                        action_add.hide()
                    }
                } else {
                    action_next.isEnabled = false
                    action_add.hide()
                }
            } else {
                val error = if (isUserAddress) {
                    getString(R.string.error_own_address)
                } else {
                    getString(R.string.error_invalid_address)
                }
                input_address.showError(error)
                action_next.isEnabled = false
                action_add.hide()
            }
        }

        action_add.throttledClick()
            .subscribe {
                val typedAddress = input_address.text
                navigateToAddContact(typedAddress)
            }
            .addToDisposables(disposables)
    }

    private fun bindScreenResults() {
        activityResult.get().bind(
            RequestCode.SCAN_QR,
            disposable = disposables,
            onResult = {
                it.data?.getStringExtra(ScanQRActivity.ARG_ADDRESS_SCANNED)?.apply {
                    input_address.textField()?.setText(this)
                }
            },
            onError = { showError(it) }
        )

        activityResult.get().bind(
            RequestCode.ADD_CONTACT,
            disposable = disposables,
            onResult = {
                it.data?.getParcelableExtra<MagmaContact>(AddContactActivity.ARG_CONTACT)?.apply {
                    showLoading()
                    // Show contacts side
                    input_address.textField()?.text = null
                    refreshContacts()
                    select_to.toggle_tab_layout.selectTab(select_to.toggle_tab_layout.getTabAt(1))
                    // Wait for screen to commit the transaction and then navigate
                    switcher.postDelayed({
                        hideLoading()
                        listener?.onContactSelected(this)
                    }, 800)
                }
            },
            onError = { showError(it) }
        )
    }

    private fun isUserAddress(): Boolean {
        return viewModel.isUserAddress(input_address.textField()?.text.toString())
    }

    private fun navigateToAddContact(address: Address?) {
        val fakeContact = if (address != null) {
            contactsViewModel.createMagmaContact(address)
        } else {
            null
        }
        AppNavigator.navigateToAddContact(activity, fakeContact)
    }

    private fun showEmpty() {
        empty_status?.show()
        list?.hide()
    }

    private fun showLoading() {
        loading.show()
    }

    private fun hideLoading() {
        loading.hide()
    }
}
