/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.delegate

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.button.MaterialButton
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.core.extension.*
import io.camlcase.smartwallet.data.model.delegate.TezosBakerResponse
import io.camlcase.smartwallet.databinding.FragmentDelegateListBinding
import io.camlcase.smartwallet.model.DataWrapper
import io.camlcase.smartwallet.model.handleResults
import io.camlcase.smartwallet.ui.base.BaseBottomSheetFragment
import io.camlcase.smartwallet.ui.base.bind
import io.camlcase.smartwallet.ui.base.navigate.RequestCode
import io.camlcase.smartwallet.ui.base.showError
import io.camlcase.smartwallet.ui.custom.setForTezosAddress
import io.camlcase.smartwallet.ui.qr.ScanQRActivity
import io.camlcase.smartwallet.ui.send.GetUserAddressViewModel

/**
 * Baker delegation of funds
 * - Paste and address
 * - Scan an address
 * - Select an address from curated list
 */
class DelegateListFragment : BaseBottomSheetFragment() {
    interface ChooseDelegateListener {
        fun scanQR()

        /**
         * User tapped "Remove" action
         */
        fun onUndelegate()
        fun onBakerSelected(baker: TezosBakerResponse)
    }

    private val binding: FragmentDelegateListBinding
        get() = _binding as FragmentDelegateListBinding
    private lateinit var viewModel: DelegateListViewModel
    private lateinit var addressviewModel: GetUserAddressViewModel
    private var listener: ChooseDelegateListener? = null

    override fun onCreateViewBind(inflater: LayoutInflater, container: ViewGroup?): FragmentDelegateListBinding {
        return FragmentDelegateListBinding.inflate(inflater, container, false)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        listener = context as? ChooseDelegateListener
        viewModel = viewModel(viewModelFactory) {
            observe(data, { handleData(it) })
        }
        viewModel.getDelegates(arguments?.getBoolean(ARG_UNDELEGATED) == true)
        addressviewModel = viewModel(viewModelFactory) {}
        addressviewModel.getUserAddress()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupToolbar(binding.toolbar)
        binding.toolbar.setTitle(R.string.del_title)
        binding.inputBaker.initForModal()
        binding.list.init(activity)
        binding.list.post {
            // Show a bit of the delegate list
            setPeekHeight(resources.getDimension(R.dimen.delegation_list_modal_padding_bottom).toInt())
        }
        bindViews()
        bindScreenResults()
    }

    private fun handleData(wrapper: DataWrapper<DelegateData>) {
        wrapper.handleResults(
            onValidData = {
                fun setBakerSelectionListener(delegate: DelegateForView, view: View) {
                    view.setOnClickListener {
                        listener?.onBakerSelected(viewModel.getBaker(delegate.address, delegate.name))
                        dismiss()
                    }
                }
                binding.list.adapter = DelegateListAdapter(it.delegates) { delegate, view ->
                    if (delegate.removable) {
                        val removeButton = view.findViewById<MaterialButton>(R.id.action_remove)
                        removeButton.setOnClickListener {
                            listener?.onUndelegate()
                            view.alpha = .5f
                            removeButton.hide()
                            setBakerSelectionListener(delegate, view)
                        }
                        view.setOnClickListener(null)
                    } else {
                        setBakerSelectionListener(delegate, view)
                    }
                }

            },
            onError = {
                showError(it)
            },
            onLoading = {
                // Nothing to do
            }
        )
    }

    private fun bindViews() {
        binding.inputBaker.setForTezosAddress(
            disposables,
            isUserAddress = { isUserAddress() },
            // Listen to text input and dismiss the moment a valid address is set
            onAddressOK = {
                if (it.isNotEmpty()) {
                    // Delay for a better user experience
                    binding.inputBaker.postDelayed({
                        listener?.onBakerSelected(viewModel.getBaker(binding.inputBaker.text))
                        dismiss()
                    }, 200)
                }
            },
            onQRClick = {
                listener?.scanQR()
            }
        )
    }

    /**
     * Don't let the user self-delegate/register as delegate
     */
    private fun isUserAddress(): Boolean {
        return addressviewModel.isUserAddress(binding.inputBaker.text)
    }

    private fun bindScreenResults() {
        activityResult.get().bind(
            RequestCode.SCAN_QR,
            disposable = disposables,
            onResult = {
                it.data?.getStringExtra(ScanQRActivity.ARG_ADDRESS_SCANNED)?.apply {
                    binding.inputBaker.textField()?.setText(this)
                }
            },
            onError = { showError(it) }
        )
    }

    override fun dismiss() {
        binding.inputBaker.textField()?.hideKeyboard()
        super.dismiss()
    }

    companion object {
        private const val ARG_UNDELEGATED = "arg.undelegated_baker"
        fun init(undelegated: Boolean = false): DelegateListFragment {
            val fragment = DelegateListFragment()
            val args = Bundle()
            args.putBoolean(ARG_UNDELEGATED, undelegated)
            fragment.arguments = args
            return fragment
        }
    }
}
