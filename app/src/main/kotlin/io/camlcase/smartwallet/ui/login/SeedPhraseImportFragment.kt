/**
 * # Released under MIT License
 *
 * Copyright (c) 2021 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.login

import android.content.Context
import android.content.res.ColorStateList
import android.os.Bundle
import android.text.InputType
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import androidx.core.content.ContextCompat
import by.kirich1409.viewbindingdelegate.viewBinding
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.core.Onboarding
import io.camlcase.smartwallet.core.extension.*
import io.camlcase.smartwallet.data.model.error.AppError
import io.camlcase.smartwallet.data.model.error.ErrorType
import io.camlcase.smartwallet.data.usecase.analytics.ScreenEvent
import io.camlcase.smartwallet.databinding.FragmentImportSeedPhraseBinding
import io.camlcase.smartwallet.model.DataWrapper
import io.camlcase.smartwallet.model.handleResults
import io.camlcase.smartwallet.ui.base.BindBaseFragment
import io.camlcase.smartwallet.ui.base.info.InfoModalFragment
import io.camlcase.smartwallet.ui.base.info.InfoModalType
import io.camlcase.smartwallet.ui.base.navigate.navigateToModal
import io.camlcase.smartwallet.ui.base.showError
import io.camlcase.smartwallet.ui.base.showSnackbar
import io.camlcase.smartwallet.ui.onboarding.seed_phrase.SeedPhraseImportValidation
import kotlinx.android.synthetic.main.view_bottom_action_button.*
import java.lang.ref.WeakReference

/**
 * Import a mnemonic to generate a wallet. Supports linear and hierarchical deterministic
 */
class SeedPhraseImportFragment : BindBaseFragment(R.layout.fragment_import_seed_phrase) {
    interface SeedPhraseImportListener {
        /**
         * If user
         */
        fun onStandardWalletImported(derivedAddress: String, linearAddress: String)

        /**
         * User added a custom derivation path, so we know it's an HD wallet
         */
        fun onHDWalletImported()
    }

    private val binding by viewBinding(FragmentImportSeedPhraseBinding::bind)
    private var listener: SeedPhraseImportListener? = null
    private lateinit var seedPhraseValidation: SeedPhraseImportValidation
    private lateinit var viewModel: SeedPhraseImportViewModel

    override fun onAttach(context: Context) {
        super.onAttach(context)
        reportView(ScreenEvent.ONB_RECOVERY_ENTRY)
        listener = context as? SeedPhraseImportListener
        if (listener == null) {
            throw ClassCastException("$context must implement SeedPhraseImportListener")
        }
        viewModel = viewModel(viewModelFactory) {
            observe(data, { handleImport(it) })
        }
    }

    private fun handleImport(wrapper: DataWrapper<ImportForView>) {
        wrapper.handleResults(
            onValidData = {
                if (it.sdAddress == null) {
                    listener?.onHDWalletImported()
                } else {
                    listener?.onStandardWalletImported(it.hdAddress, it.sdAddress)
                }
            },
            onError = {
                if (it is AppError && it.type == ErrorType.WALLET_CREATE_ERROR) {
                    showSnackbar(getString(R.string.error_invalid_seed_phrase))
                } else {
                    showError(it)
                }
            }
        )
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_help, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.help -> {
                analyticsUseCase.get()?.reportScreen(ScreenEvent.ONB_RECOVERY_ENTRY_HELP)
                val fragment = InfoModalFragment.init(InfoModalType.IMPORT_SEED_PHRASE)
                navigateToModal(fragment)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initAppbar()
        bindViews()
        keyboardListener = initKeyboardListener(action_next)
        binding.buttonContainer.actionNext.setText(R.string.action_continue)
        initForm()
    }

    private fun initAppbar() {
        val title = getString(R.string.onb_import_seed_phrase_title)
        setupAppBar(binding.appBarContainer, title, true)
        setHasOptionsMenu(true)
    }

    private fun initForm() {
        // SEED PHRASE
        seedPhraseValidation = object : SeedPhraseImportValidation {
            override var seedPhraseBinding = WeakReference(binding.mainViewContainer.importView)

            override fun invalidMnemonic() {
                viewModel.validSeedPhrase = false
                checkButton()
            }

            override fun validMnemonic() {
                viewModel.validSeedPhrase = true
                checkButton()
            }
        }
        seedPhraseValidation.init(context)
        seedPhraseValidation.bind()

        // PASSPHRASE
        binding.mainViewContainer.importView.titlePassphrase.show()
        binding.mainViewContainer.importView.titlePassphrase.setText(R.string.onb_import_seed_phrase_passphrase_title)
        binding.mainViewContainer.importView.inputPassphrase.show()

        // DERIVATION PATH
        binding.mainViewContainer.inputDerivationPath.inputLayout.prefixText = Onboarding.BIP44_PREFIX
        context?.apply {
            binding.mainViewContainer.inputDerivationPath.inputLayout.setPrefixTextColor(
                ColorStateList.valueOf(
                    ContextCompat.getColor(this, R.color.white_transparent_50)
                )
            )
        }
        binding.mainViewContainer.inputDerivationPath.textField()?.inputType =
            (InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS or InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD)
        binding.mainViewContainer.inputDerivationPath.textField()?.setText(Onboarding.BIP44_DEFAULT)
    }

    private fun bindViews() {
        binding.mainViewContainer.actionShow.throttledClick(500)
            .subscribe {
                if (binding.mainViewContainer.advancedOptionsContainer.isVisible()) {
                    binding.mainViewContainer.advancedOptionsContainer.hide()
                } else {
                    binding.mainViewContainer.advancedOptionsContainer.show()
                    binding.mainViewContainer.advancedOptionsContainer.postDelayed({
                        binding.scrollView.scrollToBottom()
                    }, 100)
                }
            }.addToDisposables(disposables)

        binding.buttonContainer.actionNext.throttledClick()
            .subscribe {
                binding.mainViewContainer.importView.inputMnemonic.textField()?.hideKeyboard()
                viewModel.importWallet(
                    seedPhraseValidation.getMnemonic(),
                    seedPhraseValidation.getPassphrase(),
                    binding.mainViewContainer.inputDerivationPath.text
                )
            }.addToDisposables(disposables)

        binding.mainViewContainer.inputDerivationPath.textField()?.listenToChanges {
            if (viewModel.validDerivationPath(it)) {
                binding.mainViewContainer.inputDerivationPath.dismissError()
            } else {
                binding.mainViewContainer.inputDerivationPath.showError(getString(R.string.onb_import_seed_phrase_advanced_derivation_error))
            }
            checkButton()
        }
    }

    private fun checkButton() {
        binding.buttonContainer.actionNext.isEnabled = viewModel.validForm()
    }

    companion object {
        fun init(): SeedPhraseImportFragment {
            return SeedPhraseImportFragment()
        }
    }
}
