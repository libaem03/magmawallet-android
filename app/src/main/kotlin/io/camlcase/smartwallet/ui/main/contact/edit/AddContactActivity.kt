/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.main.contact.edit

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.core.extension.getActiveFragmentTag
import io.camlcase.smartwallet.core.extension.listenToBackground
import io.camlcase.smartwallet.core.extension.replaceFragmentSafely
import io.camlcase.smartwallet.data.model.contact.MagmaContact
import io.camlcase.smartwallet.model.navigation.AddContactNavigation
import io.camlcase.smartwallet.ui.base.BaseActivity
import io.camlcase.smartwallet.ui.permissions.ReadContactsPermission
import io.camlcase.smartwallet.ui.permissions.WriteContactsPermission

/**
 * @see ContactPermissionFragment
 * @see AddContactFragment
 */
class AddContactActivity : BaseActivity(R.layout.activity_fragment), ContactPermissionFragment.ContactPermissionListener,
    AddContactFragment.AddContactListener {
    private val step: Int
        get() {
            val canRead = ReadContactsPermission.hasPermission(this)
            val canWrite = WriteContactsPermission.hasPermission(this)
            return if (canRead && canWrite) {
                STEP_ADD
            } else {
                STEP_PERMISSION
            }
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        listenToBackground()
    }

    override fun onStart() {
        super.onStart()
        showStep()
    }

    /**
     * We call this in onStart in case the user has revoked permission and came back.
     * This makes the showStep method to be called more times than necessary, since the dialogs and screens both
     * fragments call force the screens to be recreated. We need to filter if they are already set so we don't do it
     * again needlessly.
     */
    private fun showStep() {
        val tag = when (step) {
            STEP_PERMISSION -> ContactPermissionFragment::class.qualifiedName
            else -> AddContactFragment::class.qualifiedName
        }
        if (tag != getActiveFragmentTag()) {
            val fragment = when (step) {
                STEP_PERMISSION -> ContactPermissionFragment()
                else -> {
                    val editContact = intent.extras?.getParcelable<MagmaContact>(ARG_CONTACT)
                    AddContactFragment.init(editContact)
                }
            }
            replaceFragmentSafely(fragment, R.id.container_fragment)
        }
    }

    override fun onPermissionGiven() {
        showStep()
    }

    override fun onContactSaved(contact: MagmaContact) {
        val intent = Intent()
        intent.putExtra(ARG_CONTACT, contact)
        setResult(Activity.RESULT_OK, intent)
        finishAfterTransition()
    }

    companion object {
        internal const val ARG_SOURCE = "args.add_contact.type"
        internal const val ARG_CONTACT = "args.add_contact.contact"

        private const val STEP_PERMISSION = 0
        private const val STEP_ADD = 1

        fun init(
            context: Context,
            contact: MagmaContact? = null,
            @AddContactNavigation.ScreenSource source: Int = AddContactNavigation.SOURCE_ADD
        ): Intent {
            val intent = Intent(context, AddContactActivity::class.java)
            intent.putExtra(ARG_SOURCE, source)
            intent.putExtra(ARG_CONTACT, contact)
            return intent
        }
    }
}
