/**
 * # Released under MIT License
 *
 * Copyright (c) 2021 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.login

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import io.camlcase.kotlintezos.model.Address
import io.camlcase.kotlintezos.wallet.HDWallet
import io.camlcase.smartwallet.core.Onboarding
import io.camlcase.smartwallet.core.architecture.BaseViewModel
import io.camlcase.smartwallet.core.extension.addToDisposables
import io.camlcase.smartwallet.core.extension.subscribeForUI
import io.camlcase.smartwallet.data.analytic.getCause
import io.camlcase.smartwallet.data.analytic.report
import io.camlcase.smartwallet.data.core.Wallet
import io.camlcase.smartwallet.data.model.AppWallet
import io.camlcase.smartwallet.data.model.valid
import io.camlcase.smartwallet.data.usecase.analytics.AnalyticEvent
import io.camlcase.smartwallet.data.usecase.analytics.SendAnalyticUseCase
import io.camlcase.smartwallet.data.usecase.analytics.StateEvent
import io.camlcase.smartwallet.data.usecase.user.CreateWalletUseCase
import io.camlcase.smartwallet.model.DataWrapper
import io.camlcase.smartwallet.ui.onboarding.wallet.CreateWalletViewModel.Companion.invalidWalletError
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

class SeedPhraseImportViewModel @Inject constructor(
    private val useCase: CreateWalletUseCase,
    private val analytic: SendAnalyticUseCase
) : BaseViewModel() {

    private val _data: MutableLiveData<DataWrapper<ImportForView>> = MutableLiveData()
    val data: LiveData<DataWrapper<ImportForView>> = _data

    var validSeedPhrase: Boolean = false
    var validDerivationPath: Boolean = false

    fun validDerivationPath(pathSuffix: String): Boolean {
        validDerivationPath = Wallet.validDerivationPath(Onboarding.BIP44_PREFIX + pathSuffix)
        return validDerivationPath
    }

    fun validForm(): Boolean {
        return validSeedPhrase && validDerivationPath
    }

    fun importWallet(mnemonic: List<String>, passphrase: String?, pathSuffix: String) {
        val derivationPath = sanitiseDerivationPath(pathSuffix)
        sendAnalytics(passphrase, derivationPath)
        Single.fromCallable { useCase.recoverHDWallet(mnemonic, passphrase, derivationPath) }
            .map {
                if (!it.valid()) {
                    throw invalidWalletError
                }
                parseData(it, derivationPath)
            }
            .subscribeForUI()
            .subscribe(
                {
                    analytic.reportEvent(AnalyticEvent.WALLET_RECOVERED)
                    analytic.reportState(StateEvent.ONB_RECOVERY_ENTRY_SUCCESS)
                    _data.value = DataWrapper.Success(it)
                },
                {
                    analytic.reportState(StateEvent.ONB_RECOVERY_ENTRY_FAILED, it.getCause())
                    it.report()
                    handleError(it)
                }
            ).addToDisposables(disposables)
    }

    private fun sanitiseDerivationPath(pathSuffix: String): String {
        val derivationPath = Onboarding.BIP44_PREFIX + pathSuffix
        return if (!derivationPath.contains("\'")) {
            derivationPath.replace("'", "\"")
        } else {
            derivationPath
        }
    }

    private fun parseData(appWallet: AppWallet, derivationPath: String): ImportForView {
        // If user has changed the derivation path, they have implicitly selected an HD wallet. No need for a balance check.
        val sdAddress = if (derivationPath == Wallet.DEFAULT_BIP44_DERIVATION_PATH) {
            (appWallet.wallet as HDWallet).linearAddress
        } else {
            null
        }
        return ImportForView(appWallet.address, sdAddress)
    }

    private fun sendAnalytics(passphrase: String?, derivationPath: String) {
        analytic.reportState(StateEvent.ONB_RECOVERY_ENTRY_SEED_PHRASE)
        if (!passphrase.isNullOrBlank()) {
            analytic.reportState(StateEvent.ONB_RECOVERY_ENTRY_PASSPHRASE)
        }
        if (derivationPath != Wallet.DEFAULT_BIP44_DERIVATION_PATH) {
            analytic.reportState(StateEvent.ONB_RECOVERY_ENTRY_DERIVATION_PATH)
        }
    }

    private fun handleError(error: Throwable?) {
        _data.value = DataWrapper.Error(error)
    }
}

data class ImportForView(
    val hdAddress: Address,
    val sdAddress: Address?
)
