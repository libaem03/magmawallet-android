/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.base

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import io.camlcase.smartwallet.data.analytic.AnalyticsTracker
import io.camlcase.smartwallet.data.core.Logger
import io.camlcase.smartwallet.data.usecase.operation.DexterCalculationsUseCase
import io.camlcase.smartwallet.ui.base.navigate.background.AppBackgroundDelegate

/**
 * Listens to Application general events.
 * @param backgroundDelegate Handles flags for when the app comes and goes from background
 * @param dexterCalculationsUseCase Loads the JS file to make calculations. We link it to app lifecycle so it
 * stays loaded and calls are faster.
 */
class ApplicationObserver(
    private val backgroundDelegate: AppBackgroundDelegate,
    private val dexterCalculationsUseCase: DexterCalculationsUseCase,
    private val analyticsTracker: AnalyticsTracker
) : LifecycleObserver {
    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun onBackground() {
        dexterCalculationsUseCase.close()
            .subscribe(
                { Logger.i("JS Context closed") },
                { Logger.e("JS Context closed Error: $it") }
            )
        backgroundDelegate.onBackground()
        analyticsTracker.unbind()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun onForeground() {
        dexterCalculationsUseCase.init()
            .subscribe(
                { Logger.i("JS File loaded") },
                { Logger.e("JS File load Error: $it") }
            )
        // Nothing to do. Activities will handle this on the onResume
        // This event happens sooner than the Activity creation so to listen to it, we'd need to add some delay.
    }
}
