package io.camlcase.smartwallet.ui.main

import io.camlcase.smartwallet.core.architecture.BaseViewModel
import io.camlcase.smartwallet.data.source.local.AppPreferences
import io.camlcase.smartwallet.ui.base.navigate.background.BackgroundMode
import java.util.*
import javax.inject.Inject
import kotlin.math.abs

class BackgroundViewModel @Inject constructor(
    private val appPreferences: AppPreferences
) : BaseViewModel() {
    fun showAuthentication(mode: BackgroundMode): Boolean {
        return mode == BackgroundMode.COME_FROM_BACKGROUND && sessionAuthExpired()
    }

    /**
     * Last AppPreferences.AUTH_TIMESTAMP > AUTHENTICATION_WINDOW
     */
    private fun sessionAuthExpired(): Boolean {
        val lastAuthenticationMillis = appPreferences.getLong(AppPreferences.AUTH_TIMESTAMP)
        if (lastAuthenticationMillis > 0) {
            val now: Date = Date()
            val lastAuthentication: Date = Date(lastAuthenticationMillis)
            val diff = abs(lastAuthentication.time - now.time)
            val overTime = diff >= AUTHENTICATION_WINDOW
            return overTime
        }
        return true
    }

    companion object {
        const val AUTHENTICATION_WINDOW = 5 * 1000
    }
}
