/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.main.settings.attributions

import io.camlcase.smartwallet.core.architecture.BaseViewModel
import javax.inject.Inject

class AttributionsViewModel @Inject constructor() : BaseViewModel() {
    fun getAttributionList(): List<AttributionForView> {
        val list = ArrayList<AttributionForView>()
        // Alphabetically!
        list.add(
            AttributionForView(
                "Baking Bad",
                AttributionType.BAKER_DATA,
                "baking-bad.org",
                "https://baking-bad.org/"
            )
        )
        list.add(
            AttributionForView(
                "Better Call Dev",
                AttributionType.OPERATION_DATA,
                "better-call.dev",
                "https://better-call.dev/"
            )
        )
        list.add(
            AttributionForView(
                "CoinGecko",
                AttributionType.PRICING_DATA,
                "coingecko.com",
                "https://www.coingecko.com/en"
            )
        )
        list.add(
            AttributionForView(
                "Lokalise",
                AttributionType.LOCALIZATION,
                "lokalise.com",
                "https://lokalise.com/"
            )
        )
        list.add(
            AttributionForView(
                "TzKT",
                AttributionType.OPERATION_DATA,
                "tzkt.io",
                "https://tzkt.io/"
            )
        )
        return list
    }
}

data class AttributionForView(
    val name: String,
    val type: AttributionType,
    val vanityUrl: String,
    val url: String
)

enum class AttributionType {
    BAKER_DATA,
    PRICING_DATA,
    OPERATION_DATA,
    LOCALIZATION
}
