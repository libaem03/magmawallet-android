/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.exchange.amount

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.core.extension.getDeviceLocale
import io.camlcase.smartwallet.core.extension.getTokenLogoImage
import io.camlcase.smartwallet.data.model.exchange.CurrencyBundle
import io.camlcase.smartwallet.data.model.exchange.TokenInfoBundle
import io.camlcase.smartwallet.data.model.exchange.formattedCurrencyBalance
import kotlinx.android.synthetic.main.row_exchange_token.view.*

class ExchangeTokenAdapter(
    private val currencyBundle: CurrencyBundle,
    private val values: List<TokenInfoBundle>,
    private val showBalance: Boolean,
    private val onItemClick: (TokenInfoBundle, View) -> Unit
) : RecyclerView.Adapter<ExchangeTokenAdapter.ViewHolder>() {

    override fun getItemCount(): Int {
        return values.size
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val image: ImageView = view.token_image
        val symbol: TextView = view.token_symbol
        val name: TextView = view.token_name
        val balance: TextView = view.token_balance
        val currencyBalance: TextView = view.token_balance_currency
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.row_exchange_token, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = values[position]

        val context = holder.itemView.context
        holder.image.setImageResource(context.getTokenLogoImage(item.token.symbol.toLowerCase(context.getDeviceLocale())))
        holder.symbol.text = item.token.symbol
        holder.name.text = item.token.name
        if (showBalance) {
            holder.balance.text = item.balance.formattedValue
            holder.currencyBalance.text = item.formattedCurrencyBalance(currencyBundle)
        }
        onItemClick(item, holder.itemView)
    }
}
