/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.send

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.os.Parcelable
import android.view.View
import by.kirich1409.viewbindingdelegate.viewBinding
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.core.extension.*
import io.camlcase.smartwallet.data.model.amount.Balance
import io.camlcase.smartwallet.data.model.exchange.TokenInfoBundle
import io.camlcase.smartwallet.data.model.exchange.formattedBalance
import io.camlcase.smartwallet.data.model.isXTZ
import io.camlcase.smartwallet.databinding.FragmentSendAmountBinding
import io.camlcase.smartwallet.model.DataWrapper
import io.camlcase.smartwallet.model.handleResults
import io.camlcase.smartwallet.ui.base.BindBaseFragment
import io.camlcase.smartwallet.ui.base.showError
import io.camlcase.smartwallet.ui.custom.ScrollOffsetListener

/**
 * Send XTZ/Token Amount
 */
class SendAmountFragment : BindBaseFragment(R.layout.fragment_send_amount) {
    interface SendAmountListener {
        fun onAmountSet(amount: Balance<*>)
    }

    private val binding by viewBinding(FragmentSendAmountBinding::bind)
    private lateinit var viewModel: SendAmountViewModel
    private var listener: SendAmountListener? = null
    private var info: TokenInfoBundle? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        listener = context as? SendAmountListener
        if (listener == null) {
            throw ClassCastException("$context must implement SendAmountListener")
        }

        manageIntent(arguments)
        bindViewModel()
    }

    private fun manageIntent(bundle: Bundle?) {
        bundle?.getParcelable<TokenInfoBundle>(ARGS_SEND_TOKEN)?.let {
            info = it
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initToolbar()
        keyboardListener = initKeyboardListener(binding.buttonContainer.actionNext)
        binding.buttonContainer.actionNext.setText(R.string.action_continue)
        binding.mainViewContainer.amountField.showInitKeyboard()
    }

    private fun initToolbar() {
        setupToolbar(binding.appBarContainer.toolbar, true)
    }

    private fun initAppBar(tokenInfo: TokenInfoBundle) {
        val title = getString(R.string.snd_amount_title, tokenInfo.token.symbol)
        binding.appBarContainer.expandedTitleText.text = title
        binding.appBarContainer.expandedTitleDetail.text =
            getString(R.string.snd_amount_balance, tokenInfo.formattedBalance())

        binding.appBarContainer.appBar.addOnOffsetChangedListener(
            ScrollOffsetListener(
                binding.appBarContainer.expandedTitle,
                binding.appBarContainer.toolbar,
                title
            )
        )
    }

    private fun bindViewModel() {
        viewModel = viewModel(viewModelFactory) {
            observe(balance, { handleBalance(it) })
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        bindViews()
        viewModel.getBalanceInfo(info)
    }

    private fun handleBalance(wrapper: DataWrapper<TokenInfoBundle>) {
        wrapper.handleResults(
            onValidData = { exchange ->
                initAppBar(exchange)
                hideLoading()
                bindAmountTextInput(exchange)
            },
            onError = {
                hideLoading()
                showError(it)
            },
            onLoading = { showLoading() }
        )
    }

    private fun bindAmountTextInput(tokenInfo: TokenInfoBundle) {
        binding.mainViewContainer.amountField.changeInputType(tokenInfo.token.decimals > 0)
        binding.mainViewContainer.amountField.setFilters(tokenInfo.balance.bigDecimal, tokenInfo.token.decimals)
        binding.mainViewContainer.amountField.listenToChanges {
            if (/*tokenInfo.showInExchange()*/tokenInfo.token.isXTZ()) {
                binding.mainViewContainer.amountInCurrency.text = viewModel.convertToCurrency(it)
            } else {
                viewModel.saveAmount(it)
            }

            when (viewModel.validAmount(it)) {
                SendAmountViewModel.CORRECT -> {
                    binding.mainViewContainer.amountError.hide()
                    binding.buttonContainer.actionNext.isEnabled = true
                }
                SendAmountViewModel.OVER_BALANCE -> {
                    binding.mainViewContainer.amountError.text = getString(R.string.error_insufficient_funds)
                    binding.mainViewContainer.amountError.show()
                    binding.buttonContainer.actionNext.isEnabled = false
                }
                SendAmountViewModel.IS_ZERO -> {
                    binding.mainViewContainer.amountError.hide()
                    binding.buttonContainer.actionNext.isEnabled = false
                }
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun bindViews() {
        binding.buttonContainer.actionNext.throttledClick()
            .subscribe {
                binding.mainViewContainer.amountField.hideKeyboard()
                listener?.onAmountSet(viewModel.getAmount())
            }
            .addToDisposables(disposables)

        binding.mainViewContainer.actionEnterMax.throttledClick()
            .subscribe {
                val all = viewModel.result.balance.bigDecimal.toPlainString()
                binding.mainViewContainer.amountField.setText(all)
                // Move cursor to the end
                binding.mainViewContainer.amountField.setSelection(all.length)
            }
            .addToDisposables(disposables)
    }

    private fun showLoading() {
        binding.sendLoading.root.show()
        binding.buttonContainer.actionNext.isEnabled = false
    }

    private fun hideLoading() {
        binding.sendLoading.root.hide()
    }

    companion object {
        const val ARGS_SEND_TOKEN = "arg.send.token"

        fun init(info: TokenInfoBundle? = null): SendAmountFragment {
            val fragment = SendAmountFragment()
            info?.apply {
                val args = Bundle()
                args.putParcelable(ARGS_SEND_TOKEN, this as Parcelable)
                fragment.arguments = args
            }
            return fragment
        }
    }
}
