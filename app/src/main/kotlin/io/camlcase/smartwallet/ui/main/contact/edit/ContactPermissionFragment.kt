/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.main.contact.edit

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.core.extension.addToDisposables
import io.camlcase.smartwallet.core.extension.setupToolbar
import io.camlcase.smartwallet.core.extension.throttledClick
import io.camlcase.smartwallet.ui.base.BaseFragment
import io.camlcase.smartwallet.ui.base.navigate.AppNavigator
import io.camlcase.smartwallet.ui.base.showSnackbar
import io.camlcase.smartwallet.ui.permissions.*
import kotlinx.android.synthetic.main.view_bottom_action_button.*
import kotlinx.android.synthetic.main.view_collapsing_toolbar.*
import java.lang.ref.WeakReference

/**
 * Explains the user why we need the WRITE_CONTACTS permission to store tz1 addresses
 *
 * Needs to ask for two permissions, in order:
 * - WRITE_CONTACTS (To add a contact)
 * - READ_CONTACTS (To show the contact list)
 *
 * It won't let the user pass until it has given both
 */
class ContactPermissionFragment : BaseFragment(), PermissionDelegate.PermissionsListener {
    interface ContactPermissionListener {
        fun onPermissionGiven()
    }

    private var listener: ContactPermissionListener? = null
    private var writeContactPermissionsDelegate: PermissionDelegate? = null
    private var readContactPermissionsDelegate: PermissionDelegate? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_contact_permission, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupToolbar(toolbar, true)
        action_next.setText(R.string.con_permission_approve_action)
        action_next.isEnabled = true
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        listener = context as? ContactPermissionListener
        if (listener == null) {
            throw ClassCastException("$context must implement AddressSetListener")
        }
        writeContactPermissionsDelegate = WriteContactsPermissionDelegate(this, context)
        readContactPermissionsDelegate = ReadContactsPermissionDelegate(this, context)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        bindViews()
        bindScreenResults()
    }

    private fun bindViews() {
        action_next.throttledClick()
            .subscribe {
                if (WriteContactsPermission.hasPermission(context)) {
                    readContactPermissionsDelegate?.onPermissionButtonClick()
                } else {
                    writeContactPermissionsDelegate?.onPermissionButtonClick()
                }
            }
            .addToDisposables(disposables)
    }

    private fun bindScreenResults() {
        activityResult.get()?.apply {
            writeContactPermissionsDelegate?.listenToPermissionsSettings(this)
        }
        permissionResult.let {
            writeContactPermissionsDelegate?.listenToPermissionsDialog(it)
            readContactPermissionsDelegate?.listenToPermissionsDialog(it)
        }
    }

    override fun activityReference(): WeakReference<AppCompatActivity?> {
        return WeakReference(activity as? AppCompatActivity)
    }

    override fun navigateToSettings() {
        activity?.apply {
            AppNavigator.navigateToPermissionSettings(this)
        }
    }

    override fun onPermission(hasPermission: Boolean) {
        val canRead = ReadContactsPermission.hasPermission(context)
        val canWrite = WriteContactsPermission.hasPermission(context)

        when {
            canRead && canWrite -> listener?.onPermissionGiven()
            // Write has been given, now show read dialog
            canWrite && !canRead -> readContactPermissionsDelegate?.onPermissionButtonClick()
        }
    }

    override fun showPermissionsError(error: Throwable?) {
        view.showSnackbar(getString(R.string.error_generic))
    }

    override fun onDestroyView() {
        super.onDestroyView()
        writeContactPermissionsDelegate?.unbind()
        readContactPermissionsDelegate?.unbind()
    }
}
