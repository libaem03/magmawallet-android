/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.custom

import android.content.Context
import android.graphics.drawable.Drawable
import android.text.InputFilter
import android.text.InputFilter.LengthFilter
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.ImageButton
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import androidx.core.widget.doOnTextChanged
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.core.extension.addToDisposables
import io.camlcase.smartwallet.core.extension.hide
import io.camlcase.smartwallet.core.extension.show
import io.camlcase.smartwallet.core.extension.throttledClick
import io.camlcase.smartwallet.data.core.extension.tezos.validTezosAddress
import io.reactivex.rxjava3.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.view_icon_clear_input.view.*

/**
 * An TextInputEditText that follows design guidelines
 */
class IconClearEditText(context: Context, attrs: AttributeSet?) : LinearLayout(context, attrs) {

    val text: String
        get() {
            return input?.text.toString()
        }

    init {
        LayoutInflater.from(context).inflate(R.layout.view_icon_clear_input, this)
        attrs?.apply {
            initWithAttrs(context, this)
        }
    }

    private fun initWithAttrs(context: Context, attrs: AttributeSet?) {
        context.theme.obtainStyledAttributes(attrs, R.styleable.IconClearEditText, 0, 0).apply {
            val icon: Drawable?
            val iconDesc: String?
            try {
                icon = getDrawable(R.styleable.IconClearEditText_iconSrc)
                iconDesc = getString(R.styleable.IconClearEditText_iconDescription)
            } finally {
                recycle()
            }

            setIcon(icon, iconDesc)
        }
    }

    fun textField(): TextInputEditText? = input
    fun endIcon(): ImageButton? = input_icon

    /**
     * Change programmatically for white modals
     */
    fun initForModal() {
        input_error.setCompoundDrawablesWithIntrinsicBounds(
            ContextCompat.getDrawable(context, R.drawable.ic_error_modal),
            null,
            null,
            null
        )
    }

    fun setIcon(drawable: Drawable?, contentDescription: String?) {
        drawable?.apply {
            input_icon.show()
            input_icon.setImageDrawable(this)
            contentDescription?.let {
                input_icon.contentDescription = it
            }
        }
    }

    fun showError(message: String) {
        input_container.setBackgroundResource(R.drawable.light_alpha_border_background)
        input_error.show()
        input_error.text = message
    }

    fun dismissError() {
        input_container.setBackgroundResource(R.drawable.light_alpha_background)
        input_error.hide()
        input_error.text = null
    }

    fun bindIcon(disposables: CompositeDisposable, action: () -> Unit) {
        input_icon.throttledClick()
            .subscribe { action() }
            .addToDisposables(disposables)
    }

    fun hideClear() {
        input_layout.endIconMode = TextInputLayout.END_ICON_NONE
    }

    fun setMaxLength(length: Int) {
        val filters = arrayOfNulls<InputFilter>(1)
        filters[0] = LengthFilter(length)
        input.filters = filters
    }
}

/**
 * Assumes tezos address should be anything else than the user address (for sending, delegate, etc)
 */
fun IconClearEditText?.setForTezosAddress(
    disposables: CompositeDisposable,
    addressValidation: (String) -> Boolean = { it.validTezosAddress() },
    isUserAddress: (String) -> Boolean,
    onAddressOK: (String) -> Unit = {},
    onAddressKO: (String) -> Unit = {},
    onQRClick: () -> Unit
) {
    this?.apply {
        textField()?.setHint(R.string.snd_recipient_hint)

        textField()?.doOnTextChanged { text, _, _, _ ->
            val input = text.toString()
            val validAddress = addressValidation(input) && !isUserAddress(input)
            if (input.isEmpty() || validAddress) {
                dismissError()
                onAddressOK(input)
            } else if (isUserAddress(input)) {
                showError(context.getString(R.string.error_own_address))
                onAddressKO(input)
            } else {
                showError(context.getString(R.string.error_invalid_address))
                onAddressKO(input)
            }
        }

        bindIcon(disposables) {
            onQRClick()
        }
    }
}
