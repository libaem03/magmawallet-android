/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.base.navigate

import android.content.Context
import android.content.Intent
import android.provider.ContactsContract
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.DialogFragment
import com.google.android.gms.common.AccountPicker
import io.camlcase.smartwallet.core.URL
import io.camlcase.smartwallet.core.extension.isMainnet
import io.camlcase.smartwallet.data.core.Contacts
import io.camlcase.smartwallet.data.model.contact.MagmaContact
import io.camlcase.smartwallet.data.model.exchange.TokenInfoBundle
import io.camlcase.smartwallet.data.model.swap.SwapRequest
import io.camlcase.smartwallet.model.navigation.MainSource
import io.camlcase.smartwallet.model.navigation.PinNavigation
import io.camlcase.smartwallet.model.navigation.SendNavigation
import io.camlcase.smartwallet.model.navigation.WelcomeNavigation
import io.camlcase.smartwallet.model.operation.OperationResultBundle
import io.camlcase.smartwallet.ui.currency.LocalCurrencyActivity
import io.camlcase.smartwallet.ui.delegate.DelegateActivity
import io.camlcase.smartwallet.ui.exchange.ExchangeActivity
import io.camlcase.smartwallet.ui.login.LoginActivity
import io.camlcase.smartwallet.ui.main.MainActivity
import io.camlcase.smartwallet.ui.main.activity.OperationListActivity
import io.camlcase.smartwallet.ui.main.contact.edit.AddContactActivity
import io.camlcase.smartwallet.ui.main.settings.attributions.AttributionsActivity
import io.camlcase.smartwallet.ui.main.settings.network.NetworkConfigActivity
import io.camlcase.smartwallet.ui.migration.MigrationActivity
import io.camlcase.smartwallet.ui.migration.MigrationWarningFragment
import io.camlcase.smartwallet.ui.onboarding.OnboardingActivity
import io.camlcase.smartwallet.ui.onboarding.WelcomeActivity
import io.camlcase.smartwallet.ui.operation.WaitForInclusionActivity
import io.camlcase.smartwallet.ui.qr.QRScanType
import io.camlcase.smartwallet.ui.qr.ScanQRActivity
import io.camlcase.smartwallet.ui.security.lock.AuthLoginActivity
import io.camlcase.smartwallet.ui.security.pin.PinChangeActivity
import io.camlcase.smartwallet.ui.send.SendActivity

object AppNavigator : Navigator {

    override fun navigateToWelcome(context: Context?, flags: Int?, @WelcomeNavigation.ScreenSource source: Int) {
        context?.also {
            val intent = WelcomeActivity.init(it, source)
            flags?.apply { intent.flags = this }
            startActivity(it, intent)
        }
    }

    override fun navigateToMain(context: Context?, flags: Int?, source: MainSource) {
        context?.also {
            val intent = MainActivity.init(it, source)
            flags?.apply { intent.flags = this }
            startActivity(it, intent)
        }
    }

    override fun navigateToMain(context: Context?, deeplinkUri: String?, source: MainSource) {
        context?.also {
            val intent = MainActivity.init(it, deeplinkUri, source)
            startActivity(it, intent)
        }
    }

    override fun navigateToAuthLogin(context: Context?) {
        context?.also {
            val intent = AuthLoginActivity.init(it)
            // Show at the top of the task
            intent.flags = Intent.FLAG_ACTIVITY_REORDER_TO_FRONT
            navigateTo(it, intent, RequestCode.AUTH_LOGIN)
        }
    }

    override fun navigateToLogin(context: Context?) {
        context?.also {
            startActivity(it, LoginActivity.init(it))
        }
    }

    override fun navigateToOnboarding(context: Context?) {
        context?.also {
            val intent = OnboardingActivity.init(it)
            intent.flags = CLEAR_FLAGS
            startActivity(it, intent)
        }
    }

    fun navigateToOnboarding(context: Context?, clear: Boolean) {
        context?.also {
            val intent = OnboardingActivity.init(it)
            if (clear) {
                intent.flags = CLEAR_FLAGS
            }
            startActivity(it, intent)
        }
    }

    override fun navigateToDelegate(context: Context?) {
        context?.also {
            navigateTo(it, DelegateActivity.init(context), RequestCode.DELEGATE_BAKER)
        }
    }

    override fun navigateToSend(
        context: Context?,
        @SendNavigation.ScreenSource source: Int,
        tokenInfo: TokenInfoBundle?,
        contact: MagmaContact?
    ) {
        context?.also {
            navigateTo(it, SendActivity.init(it, source, tokenInfo, contact), RequestCode.SEND)
        }
    }

    override fun navigateToScanQR(context: Context?, type: QRScanType) {
        context?.also {
            navigateTo(it, ScanQRActivity.init(context, type), RequestCode.SCAN_QR)
        }
    }

    override fun navigateToChangePin(
        context: Context?,
        @PinNavigation.ScreenSource source: Int
    ) {
        context?.also {
            startActivity(it, PinChangeActivity.init(it, source))
        }
    }

    override fun navigateToAddContact(
        context: Context?,
        contact: MagmaContact?
    ) {
        context?.also {
            navigateTo(it, AddContactActivity.init(it, contact), RequestCode.ADD_CONTACT)
        }
    }

    override fun navigateToMigrateWallet(context: Context?) {
        context?.also {
            navigateTo(it, MigrationActivity.init(context), RequestCode.MIGRATE_WALLET)
        }
    }

    override fun navigateToLocalCurrencies(context: Context?) {
        context?.also {
            navigateTo(it, LocalCurrencyActivity.init(context), RequestCode.LOCAL_CURRENCIES)
        }
    }

    override fun navigateToAttributions(context: Context?) {
        context?.also {
            startActivity(it, AttributionsActivity.init(context))
        }
    }

    override fun navigateToNetworkConfig(context: Context?) {
        context?.also {
            startActivity(it, NetworkConfigActivity.init(context))
        }
    }

    fun navigateToMigrateWarning(activity: AppCompatActivity?): DialogFragment? {
        return activity?.let {
            val dialog = MigrationWarningFragment()
            dialog.show(it.supportFragmentManager, "MigrationDialog")
            dialog
        }
    }

    override fun navigateToContactActivity(context: Context?, contact: MagmaContact) {
        context?.also {
            startActivity(it, OperationListActivity.init(it, contact))
        }
    }

    override fun navigateToWaitForInclusionActivity(context: Context?, bundle: OperationResultBundle) {
        context?.also {
            navigateTo(it, WaitForInclusionActivity.init(it, bundle), RequestCode.WAIT_FOR_INJECTION)
        }
    }

    fun navigateToExchange(context: Context?, swapRequest: SwapRequest) {
        context?.also {
            navigateTo(it, ExchangeActivity.init(it, swapRequest), RequestCode.EXCHANGE)
        }
    }

    fun openShare(context: Context?, title: String?, textToShare: String) {
        context?.apply {
            val sendIntent: Intent = Intent().apply {
                action = Intent.ACTION_SEND
                title?.apply {
                    // (Optional)
                    putExtra(Intent.EXTRA_TITLE, title)
                    putExtra(Intent.EXTRA_SUBJECT, title)
                }
                putExtra(Intent.EXTRA_TEXT, textToShare)
                type = "text/plain"
            }

            val shareIntent = Intent.createChooser(sendIntent, null)
            startActivity(this, shareIntent)
        }
    }

    fun openOperationExplorer(context: Context?, operationHash: String) {
        val url = if (isMainnet()) {
            URL.OPERATION_DETAIL_URL
        } else {
            URL.OPERATION_DETAIL_TESTNET_URL
        }
        navigateToUrl(context, url + operationHash)
    }

    fun openContactsPicker(context: Context?) {
        val intent = Intent(Intent.ACTION_PICK).apply {
            type = ContactsContract.Contacts.CONTENT_TYPE // Show all
        }
        context?.also {
            navigateTo(it, intent, RequestCode.CONTACTS_PICKER)
        }
    }

    /**
     * Opens a system dialog to choose a Google account which to operate with
     * @see https://developers.google.com/android/reference/com/google/android/gms/common/AccountPicker
     */
    fun openAccountPicker(context: Context?) {
        val intent = AccountPicker.newChooseAccountIntent(
            null,
            null,
            arrayOf(Contacts.GOOGLE_ACCOUNT_TYPE),
            false,
            null,
            null,
            null,
            null
        )
        context?.also {
            navigateTo(it, intent, RequestCode.ACCOUNTS_PICKER)
        }
    }
}
