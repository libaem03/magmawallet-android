/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.ui.main.settings.attributions

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.databinding.RowAttributionBinding

class AttributionsAdapter(
    private val values: List<AttributionForView>,
    private val onItemClick: (String, View) -> Unit
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    inner class ViewHolder(val binding: RowAttributionBinding) : RecyclerView.ViewHolder(binding.root)

    override fun getItemCount(): Int {
        return values.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding = RowAttributionBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item: AttributionForView = values[position]
        val context = holder.itemView.context

        (holder as ViewHolder).binding.attributionName.text = item.name
        holder.binding.attributionDescription.text = when (item.type) {
            AttributionType.BAKER_DATA -> context.getString(R.string.set_attributions_baker_data)
            AttributionType.PRICING_DATA -> context.getString(R.string.set_attributions_pricing_data)
            AttributionType.OPERATION_DATA -> context.getString(R.string.set_attributions_activity_data)
            AttributionType.LOCALIZATION -> context.getString(R.string.set_attributions_strings)
        }
        holder.binding.attributionUrl.text = item.vanityUrl
        onItemClick(item.url, holder.itemView)
    }
}
