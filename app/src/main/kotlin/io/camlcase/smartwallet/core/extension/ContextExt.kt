package io.camlcase.smartwallet.core.extension

import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.util.TypedValue
import android.view.View
import android.view.ViewTreeObserver
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.FrameLayout
import androidx.annotation.AttrRes
import androidx.annotation.ColorInt
import androidx.annotation.IdRes
import androidx.annotation.Px
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.button.MaterialButton
import io.camlcase.kotlintezos.model.OperationResultStatus
import io.camlcase.smartwallet.BuildConfig
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.data.analytic.report
import io.camlcase.smartwallet.data.core.extension.tezos.isMainnet
import io.camlcase.smartwallet.data.model.operation.MagmaOperationResult
import io.camlcase.smartwallet.ui.base.*
import io.camlcase.smartwallet.ui.base.info.InfoFragment
import io.camlcase.smartwallet.ui.base.info.InfoType
import io.camlcase.smartwallet.ui.base.navigate.AppNavigator
import java.util.*

inline fun <reified T : ViewModel> BaseActivity.viewModel(factory: ViewModelProvider.Factory, body: T.() -> Unit): T {
    val viewModel = ViewModelProvider(this, factory).get(T::class.java)
    viewModel.body()
    return viewModel
}

@Deprecated("Use Use BindBaseFragment and migrate to view binding")
inline fun <reified T : ViewModel> BaseFragment.viewModel(factory: ViewModelProvider.Factory, body: T.() -> Unit): T {
    val viewModel = ViewModelProvider(this, factory).get(T::class.java)

    viewModel.body()
    return viewModel
}

inline fun <reified T : ViewModel> BindBaseFragment.viewModel(
    factory: ViewModelProvider.Factory,
    body: T.() -> Unit
): T {
    val viewModel = ViewModelProvider(this, factory).get(T::class.java)

    viewModel.body()
    return viewModel
}

inline fun <reified T : ViewModel> BaseBottomSheetFragment.viewModel(
    factory: ViewModelProvider.Factory,
    body: T.() -> Unit
): T {
    val viewModel = ViewModelProvider(this, factory).get(T::class.java)

    viewModel.body()
    return viewModel
}

inline fun <reified T : ViewModel> BaseDialogFragment.viewModel(
    factory: ViewModelProvider.Factory,
    body: T.() -> Unit
): T {
    val viewModel = ViewModelProvider(this, factory).get(T::class.java)
    viewModel.body()
    return viewModel
}

/**
 * Returns a color value of an [attributeId] depending on the current theme.
 * If it doesn't exist returns [defaultValue]
 */
@ColorInt
fun Context?.getThemeColor(@AttrRes attributeId: Int, @ColorInt defaultValue: Int = -1): Int {
    return this?.let {
        val typedValue = TypedValue()
        it.theme.resolveAttribute(attributeId, typedValue, true)
        val attributes = it.obtainStyledAttributes(typedValue.data, intArrayOf(attributeId))
        val color = attributes.getColor(0, defaultValue)
        attributes.recycle()
        color
    } ?: defaultValue
}

@Px
fun Context?.getThemeDimension(@AttrRes attributeId: Int): Int {
    return this?.let {
        val typedValue = TypedValue()
        it.theme.resolveAttribute(attributeId, typedValue, true)
        val attributes = it.obtainStyledAttributes(typedValue.data, intArrayOf(attributeId))
        val dimension: Int = attributes.getDimensionPixelSize(0, -1)
        attributes.recycle()
        dimension
    } ?: -1
}

fun EditText?.showKeyboard() {
    this?.apply {
        if (this.requestFocus()) {
            val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.showSoftInput(this, 0)
        }
    }
}

/**
 * Show keyboard at the start of a screen leaving enough time for ImeFocusController to set the current root view.
 */
fun EditText?.showInitKeyboard() {
    this?.apply {
        if (this.requestFocus()) {
            this.postDelayed({
                val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.showSoftInput(this, InputMethodManager.SHOW_IMPLICIT)
            }, 400)
        }
    }
}

fun EditText?.hideKeyboard() {
    this?.apply {
        val imm = context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(rootView?.windowToken, 0)
        this.clearFocus()
    }
}

fun View?.hideKeyboard() {
    this?.apply {
        val imm = context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(rootView?.windowToken, 0)
    }
}

fun Context?.copyToClipBoard(message: String, onCopy: () -> Unit, onError: (error: Throwable) -> Unit = {}) {
    this?.also { context ->
        try {
            val clipboard = context.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
            val clip = ClipData.newPlainText(context.getString(R.string.app_name), message)
            clipboard.setPrimaryClip(clip)
            onCopy()
        } catch (e: NullPointerException) {
            // We shouldn't crash when copying text
            onError(e)
        }
    }
}

/**
 * Given a [name] (instead of an id) will try to find the resource
 * @return The resource id, 0 if could not be found.
 */
fun Context?.getDrawableResourceIdByName(name: String?): Int {
    return this?.resources?.getIdentifier(name, "drawable", this.packageName) ?: 0
}

/**
 * Bottom action button are shown with a listPreferredItemHeight margin bottom but that shouldn't be maintained when
 * the soft keyboards appears.
 */
@Deprecated("")
fun BaseFragment?.initKeyboardListener(
    button: MaterialButton,
    onVisibilityChange: (Boolean) -> Unit = {}
): ViewTreeObserver.OnGlobalLayoutListener? {
    val params = button.layoutParams as FrameLayout.LayoutParams
    val margin = this?.context.getThemeDimension(android.R.attr.listPreferredItemHeight)
    return this?.activity?.listenToKeyboard { visible ->
        if (visible) {
            params.bottomMargin = 0
        } else {
            params.bottomMargin = margin
        }
        button.layoutParams = params
        onVisibilityChange(visible)
    }
}


/**
 * Bottom action button are shown with a listPreferredItemHeight margin bottom but that shouldn't be maintained when
 * the soft keyboards appears.
 */
fun BindBaseFragment?.initKeyboardListener(
    button: MaterialButton,
    onVisibilityChange: (Boolean) -> Unit = {}
): ViewTreeObserver.OnGlobalLayoutListener? {
    val params = button.layoutParams as FrameLayout.LayoutParams
    val margin = this?.context.getThemeDimension(android.R.attr.listPreferredItemHeight)
    return this?.activity?.listenToKeyboard { visible ->
        if (visible) {
            params.bottomMargin = 0
        } else {
            params.bottomMargin = margin
        }
        button.layoutParams = params
        onVisibilityChange(visible)
    }
}

fun AppCompatActivity?.initKeyboardListener(
    button: MaterialButton,
    onVisibilityChange: (Boolean) -> Unit = {}
): ViewTreeObserver.OnGlobalLayoutListener? {
    val params = button.layoutParams as FrameLayout.LayoutParams
    val margin = getThemeDimension(android.R.attr.listPreferredItemHeight)
    return this?.listenToKeyboard { visible ->
        if (visible) {
            params.bottomMargin = 0
        } else {
            params.bottomMargin = margin
        }
        button.layoutParams = params
        onVisibilityChange(visible)
    }
}

fun BindBaseFragment?.setBackNavigation(
    toolbar: Toolbar,
    backNavigation: Boolean = false,
    backAction: () -> Unit = { this?.activity?.onBackPressed() }
) {
    (this?.activity as AppCompatActivity).setBackNavigation(toolbar, backNavigation, backAction)
}

fun AppCompatActivity?.setBackNavigation(
    toolbar: Toolbar,
    backNavigation: Boolean = false,
    backAction: () -> Unit = { this?.onBackPressed() }
) {
    this?.apply {
        if (backNavigation) {
            this.supportActionBar?.setDisplayHomeAsUpEnabled(true)
            this.supportActionBar?.setHomeActionContentDescription(getString(R.string.icon_back_action))
            toolbar.setNavigationOnClickListener { backAction() }
        } else {
            this.supportActionBar?.setDisplayHomeAsUpEnabled(false)
        }
    }
}

fun Fragment?.setupToolbar(
    toolbar: Toolbar,
    backNavigation: Boolean = false,
    backAction: () -> Unit = { this?.activity?.onBackPressed() }
): ActionBar? {
    return this?.let {
        val appCompatActivity = activity as AppCompatActivity
        appCompatActivity.setSupportActionBar(toolbar)
        appCompatActivity.supportActionBar?.setDisplayShowTitleEnabled(false)
        if (backNavigation) {
            appCompatActivity.setBackNavigation(toolbar, backNavigation, backAction)
        }
        return appCompatActivity.supportActionBar
    }
}

/**
 * For flows that will show an "Uh-oh, something went wrong\n(Error: ??)" message
 */
fun BaseActivity?.onOperationInclusion(
    info: MagmaOperationResult,
    successInfoType: InfoType,
    errorInfoType: InfoType,
    @IdRes containerId: Int = R.id.container_fragment
) {
    this?.apply {
        if (info.status == OperationResultStatus.APPLIED) {
            loadFragment(InfoFragment.init(successInfoType), containerId)
        } else {
            val parser = ErrorMessageParser(applicationContext)
            val errorMessage = info.parsedError?.let { parser.parseMagmeErrorMessage(it) } ?: parser.genericMessage
            loadFragment(InfoFragment.init(errorInfoType, errorMessage), containerId)
        }
    }
}

/**
 * Helper method for Logging/Testing purposes
 */
fun Context?.getDensityName(): String? {
    return this?.let {
        val density = it.resources.displayMetrics.density
        if (density >= 4.0) {
            return "xxxhdpi"
        }
        if (density >= 3.0) {
            return "xxhdpi"
        }
        if (density >= 2.0) {
            return "xhdpi"
        }
        if (density >= 1.5) {
            return "hdpi"
        }
        return if (density >= 1.0) {
            "mdpi"
        } else "ldpi"
    }
}

fun isMainnet(): Boolean {
    return BuildConfig.FLAVOR.isMainnet()
}

fun Context?.getDeviceCurrencyCode(): String {
    return try {
        Currency.getInstance(this?.getDeviceLocale() ?: Locale.US).currencyCode
    } catch (ex: RuntimeException) {
        Currency.getInstance(Locale("en", "US")).currencyCode
    }
}

fun Context?.getDeviceLanguageCode(): String {
    return this.getDeviceLocale().language
}

/**
 * Tries to find the SIM card country, else the device's.
 * @return An uppercase ISO 3166 2-letter code or a UN M.49 3-digit code.
 */
fun Context?.getDeviceCountryCode(): String {
    val locale = getDeviceLocale()
//    this?.apply {
//        val tel = ContextCompat.getSystemService(this, TelephonyManager::class.java)
//        val networkOperator = tel!!.networkOperator
//
//        // getNetworkOperator() will return an empty string if a SIM card is not inserted.
//        if (!TextUtils.isEmpty(networkOperator)) {
//            return tel.simCountryIso.toUpperCase(locale)
//        }
//    }
    return locale.country
}

/**
 * Get device Locale or default (US)
 */
fun Context?.getDeviceLocale(): Locale {
    return this?.let {
        it.resources.configuration.locales.get(0)
    } ?: Locale.US
}

fun Activity?.disableTouch() {
    this?.window
        ?.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
}

fun Activity?.enableTouch() {
    this?.window?.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
}

fun Activity?.canShow(): Boolean {
    return this?.isFinishing == false && !this.isDestroyed
}

/**
 * Handles devices with no browser when showing an url from a Fragment
 */
fun Fragment?.navigateToUrl(url: String) {
    this?.apply {
        try {
            AppNavigator.navigateToUrl(this.activity, url)
        } catch (e: ActivityNotFoundException) {
            e.report()
            showSnackbar(this.getString(R.string.error_no_browser))
        }
    }
}

/**
 * Handles devices with no browser when showing an url from an Activity
 */
fun Activity?.navigateToUrl(url: String) {
    this?.apply {
        try {
            AppNavigator.navigateToUrl(this, url)
        } catch (e: ActivityNotFoundException) {
            e.report()
            showSnackbar(this.getString(R.string.error_no_browser))
        }
    }
}

/**
 * Window flag: treat the content of the window as secure, preventing
 * it from appearing in screenshots or from being viewed on non-secure
 * displays.
 */
fun Activity.markSecure() {
    if (BuildConfig.FLAVOR.isMainnet()) {
        window.setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE)
    }
}

fun Activity.clearSecure() {
    window.clearFlags(WindowManager.LayoutParams.FLAG_SECURE)
}
