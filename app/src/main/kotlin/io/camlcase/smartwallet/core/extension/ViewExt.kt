/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.core.extension

import android.app.Activity
import android.content.Context
import android.content.res.Resources
import android.content.res.TypedArray
import android.graphics.drawable.Drawable
import android.os.SystemClock
import android.text.*
import android.text.style.ImageSpan
import android.text.style.UpdateAppearance
import android.util.TypedValue
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.ScrollView
import android.widget.TextView
import androidx.annotation.DimenRes
import androidx.annotation.DrawableRes
import androidx.annotation.FontRes
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.widget.NestedScrollView
import androidx.core.widget.doOnTextChanged
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.transition.TransitionInflater
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import io.camlcase.smartwallet.R
import io.camlcase.smartwallet.core.MILLIS_BETWEEN_EVENTS
import io.camlcase.smartwallet.core.binding.clicks
import io.camlcase.smartwallet.data.core.extension.tezos.validTezosAddress
import io.camlcase.smartwallet.databinding.ViewCollapsingToolbarBinding
import io.camlcase.smartwallet.databinding.ViewCollapsingToolbarSmallTitleBinding
import io.camlcase.smartwallet.ui.base.BindBaseFragment
import io.camlcase.smartwallet.ui.custom.CustomTypefaceSpan
import io.camlcase.smartwallet.ui.custom.DividerItemDecoration
import io.camlcase.smartwallet.ui.custom.ScrollOffsetListener
import io.reactivex.rxjava3.core.BackpressureStrategy
import io.reactivex.rxjava3.core.Flowable
import kotlinx.android.synthetic.main.view_collapsing_toolbar.*
import java.util.concurrent.TimeUnit

/**
 * Avoids rapid succession of taps on a View.
 * @return
 */
fun View?.throttledClick(millisBetweenEvents: Int = MILLIS_BETWEEN_EVENTS): Flowable<View> {
    this?.let {
        return it.clicks()
            .throttleFirst(millisBetweenEvents.toLong(), TimeUnit.MILLISECONDS)
            .toFlowable(BackpressureStrategy.BUFFER)
    } ?: return Flowable.never()
}

/**
 * Avoids rapid succession of taps on a View without the use of RxAndroid
 */
fun View?.throttledClick(millisBetweenEvents: Int = MILLIS_BETWEEN_EVENTS, action: () -> Unit) {
    this?.setOnClickListener(object : View.OnClickListener {
        private var lastClickTime: Long = 0

        override fun onClick(v: View) {
            if ((SystemClock.elapsedRealtime() - lastClickTime) < millisBetweenEvents) {
                return
            } else {
                action()
            }

            lastClickTime = SystemClock.elapsedRealtime()
        }
    })
}

fun View?.show() {
    if (this?.visibility != View.VISIBLE) {
        this?.visibility = View.VISIBLE
    }
}

fun View?.hide() {
    if (this?.visibility != View.GONE) {
        this?.visibility = View.GONE
    }
}

fun View?.isVisible(): Boolean {
    return this?.visibility == View.VISIBLE
}

fun View?.invisible() {
    if (this?.visibility != View.INVISIBLE) {
        this?.visibility = View.INVISIBLE
    }
}

/**
 * Extracts a styleable reference to a drawable
 */
fun TypedArray?.findDrawableAttribute(context: Context, styleId: Int): Drawable? {
    this?.apply {
        val drawableResId = getResourceId(styleId, -1)
        if (drawableResId > 0) {
            return ContextCompat.getDrawable(context, drawableResId)
        }
    }
    return null
}

/**
 * Spans [fontFamily] into [spannedItem] inside a [fullText]
 */
fun Context?.spanFontType(
    fullText: SpannableString,
    @FontRes fontFamily: Int,
    spannedItem: String
): SpannableString {
    var spannable = fullText
    return this?.let {
        val indexStart = fullText.indexOf(spannedItem)
        val indexEnd = indexStart + spannedItem.length

        // Set font to regularItem
        val regularTypeFace = ResourcesCompat.getFont(this, fontFamily)
        regularTypeFace?.let {
            val end = if (indexEnd == fullText.length) {
                indexEnd
            } else {
                indexEnd + 1
            }
            spannable.setSpan(CustomTypefaceSpan(it), indexStart, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
            spannable
        }
    } ?: spannable
}

fun Context?.span(
    fullText: SpannableString,
    span: UpdateAppearance,
    spannedItem: String
): SpannableString {
    val spannable = fullText
    return this?.let {
        val indexStart = fullText.indexOf(spannedItem)
        val indexEnd = indexStart + spannedItem.length

        val end = if (indexEnd == fullText.length) {
            indexEnd
        } else {
            indexEnd + 1
        }
        spannable.setSpan(span, indexStart, end, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        spannable
    } ?: spannable
}

/**
 * Loops in [fullText] and spans [normalItems] with [fontFamily] typeface
 */
fun Context?.spanFontType(fullText: String, @FontRes fontFamily: Int, spannedItems: List<String>): SpannableString {
    var spannable = SpannableString(fullText)
    for (item in spannedItems) {
        spannable = this?.spanFontType(spannable, fontFamily, item) ?: spannable
    }
    return spannable
}

fun TextInputEditText?.validateTezosAddress(inputLayout: TextInputLayout, onValid: () -> Unit, onInvalid: () -> Unit) {
    this?.doOnTextChanged { text, _, _, _ ->
        val input = text.toString()
        if (input.validTezosAddress()) {
            inputLayout.error = null
            onValid()
        } else {
            if (!text.isNullOrBlank()) {
                inputLayout.error = this.context.getString(R.string.error_invalid_address)
            } else {
                inputLayout.error = null
            }
            onInvalid()
        }
    }
}

fun Resources?.dpToPx(dp: Float): Float {
    return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, this?.displayMetrics)
}

/**
 * @return drawable resource of the symbol, the tezos logo if nothing found
 */
@DrawableRes
fun Context?.getTokenLogoImage(tokenSymbol: String): Int {
    @DrawableRes val res: Int = this.getDrawableResourceIdByName("token_${tokenSymbol}_logo")
    return if (res == 0) {
        R.drawable.token_xtz_logo
    } else {
        res
    }
}

/**
 * scroll.fullScroll(View.FOCUS_DOWN) will lead to the change of focus. That will bring some strange behavior when
 * there are more than one focusable views, e.g two EditText. Better to find last child and scroll there.
 * @see https://stackoverflow.com/a/34866634/4328579
 */
fun ScrollView?.scrollToBottom() {
    this?.postDelayed({
        val delta = this.getScrollDelta()
        this.smoothScrollBy(0, delta)
    }, 100)
}

fun NestedScrollView?.scrollToBottom() {
    this?.postDelayed({
        val delta = this.getScrollDelta()
        this.smoothScrollBy(0, delta)
    }, 100)
}

private fun ViewGroup.getScrollDelta(): Int {
    val lastChild = this.getChildAt(this.childCount - 1)
    val bottom = lastChild.bottom + this.paddingBottom
    val sy = this.scrollY
    val sh = this.height
    return bottom - (sy + sh)
}

fun RecyclerView?.addLineDivider() {
    this?.also {
        it.addItemDecoration(DividerItemDecoration())
    }
}

fun RecyclerView?.init(activity: Activity?) {
    this?.apply {
        layoutManager = LinearLayoutManager(activity)
        setHasFixedSize(true)
    }
}

fun EditText?.listenToChanges(onTextChanged: (String) -> Unit) {
    this?.addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
            val input = s.toString()
            onTextChanged(input)
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            // Nothing to do
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            // Nothing to do
        }
    })
}

fun View?.setMarginTop(@DimenRes margin: Int) {
    this?.apply {
        val dimensionPixelSize = this.context.resources.getDimensionPixelSize(margin)

        if (this.layoutParams is LinearLayout.LayoutParams) {
            this.setLinearLayoutMargin(top = dimensionPixelSize)
        }
    }
}

/**
 * Margins should be already resolved as pixel size. See [setMarginTop].
 */
fun View?.setLinearLayoutMargin(left: Int = 0, top: Int = 0, right: Int = 0, bottom: Int = 0) {
    this?.apply {
        try {
            val params: LinearLayout.LayoutParams = this.layoutParams as LinearLayout.LayoutParams
            params.setMargins(left, top, right, bottom)
            this.layoutParams = params
        } catch (e: ClassCastException) {
            // Nothing to do
        }
    }
}

fun TextView?.addEndExternalLink(@StringRes textId: Int = -1) {
    this?.apply {
        val textToAppend = if (textId == -1) {
            text.toString()
        } else {
            context.getString(textId)
        } + "   "
        val builder = SpannableStringBuilder(textToAppend)
        val imageSpan = ImageSpan(context, R.drawable.ic_external_link)
        builder.setSpan(imageSpan, textToAppend.length - 1, textToAppend.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        text = builder
    }
}

fun BindBaseFragment.slideInRight() {
    val inflater = TransitionInflater.from(requireContext())
    enterTransition = inflater.inflateTransition(R.transition.slide_right)
}

fun BindBaseFragment.setupAppBar(binding: ViewCollapsingToolbarBinding, title: String, backNavigation: Boolean) {
    setupToolbar(binding.toolbar, backNavigation)
    binding.setupAppBar(title)
}

fun ViewCollapsingToolbarBinding?.setupAppBar(title: String) {
    this?.apply {
        expandedTitle.text = title
        appBar.addOnOffsetChangedListener(
            ScrollOffsetListener(
                expandedTitle,
                toolbar,
                title
            )
        )
    }
}

fun ViewCollapsingToolbarSmallTitleBinding?.setupAppBar(title: String) {
    this?.apply {
        expandedTitle.text = title
        appBar.addOnOffsetChangedListener(
            ScrollOffsetListener(
                expandedTitle,
                toolbar,
                title
            )
        )
    }
}


