package io.camlcase.smartwallet.core.architecture

import androidx.lifecycle.ViewModel
import io.reactivex.rxjava3.disposables.CompositeDisposable

abstract class BaseViewModel : ViewModel() {

    val disposables = CompositeDisposable()

    /**
     * Subscriptions that use the Wallet should be cleared as soon as they finish.
     */
    var operationDisposables = CompositeDisposable()

    override fun onCleared() {
        disposables.clear()
        operationDisposables.clear()
    }

    internal fun clearOperationSubscription() {
        operationDisposables.clear()
        operationDisposables = CompositeDisposable()
    }
}
