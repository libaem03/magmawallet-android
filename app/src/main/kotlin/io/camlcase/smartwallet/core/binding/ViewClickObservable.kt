/*
 * Copyright (c) 2018 Jake Wharton
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package io.camlcase.smartwallet.core.binding

import android.os.Looper
import android.view.View
import android.view.View.OnClickListener
import androidx.annotation.CheckResult
import io.reactivex.rxjava3.android.MainThreadDisposable
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Observer
import io.reactivex.rxjava3.disposables.Disposable

/**
 * Create an observable which emits on `view` click events. The emitted value is
 * unspecified and should only be used as notification.
 *
 * *Warning:* The created observable keeps a strong reference to `view`. Unsubscribe
 * to free this reference.
 *
 * *Warning:* The created observable uses [View.setOnClickListener] to observe
 * clicks. Only one observable can be used for a view at a time.
 *
 * --------
 * Taken and modified from the RxBindings library to use RxJava3 methods:
 * @see https://github.com/JakeWharton/RxBinding/blob/master/rxbinding/src/main/java/com/jakewharton/rxbinding3/view/ViewClickObservable.kt
 */
@CheckResult
fun View.clicks(): Observable<View> {
    return ViewClickObservable(this)
}

private class ViewClickObservable(
    private val view: View
) : Observable<View>() {

    override fun subscribeActual(observer: Observer<in View>) {
        if (!checkMainThread(observer)) {
            return
        }
        val listener = Listener(view, observer)
        observer.onSubscribe(listener)
        view.setOnClickListener(listener)
    }

    private class Listener(
        private val view: View,
        private val observer: Observer<in View>
    ) : MainThreadDisposable(), OnClickListener {

        override fun onClick(v: View) {
            if (!isDisposed) {
                observer.onNext(v)
            }
        }

        override fun onDispose() {
            view.setOnClickListener(null)
        }
    }
}

fun checkMainThread(observer: Observer<*>): Boolean {
    if (Looper.myLooper() != Looper.getMainLooper()) {
        observer.onSubscribe(Disposable.empty())
        observer.onError(IllegalStateException(
            "Expected to be called on the main thread but was " + Thread.currentThread().name))
        return false
    }
    return true
}
