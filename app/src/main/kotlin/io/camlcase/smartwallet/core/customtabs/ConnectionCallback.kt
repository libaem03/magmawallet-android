/**
 * (c) @saurabharora90
 * Added from the CustomTabs-Kotlin library in Github as it was published on JCenter and not
 * available as a library anymore.
 * https://github.com/saurabharora90/CustomTabs-Kotlin
 */
package io.camlcase.smartwallet.core.customtabs

/**
 * A Callback for when the service is connected or disconnected. Use those callbacks to
 * handle UI changes when the service is connected or disconnected.
 */
interface ConnectionCallback {
    /**
     * Called when the service is connected.
     */
    fun onCustomTabsConnected()

    /**
     * Called when the service is disconnected.
     */
    fun onCustomTabsDisconnected()
}
