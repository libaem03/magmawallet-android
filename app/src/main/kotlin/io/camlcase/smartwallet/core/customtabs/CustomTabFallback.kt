/**
 * (c) @saurabharora90
 * Added from the CustomTabs-Kotlin library in Github as it was published on JCenter and not
 * available as a library anymore.
 * https://github.com/saurabharora90/CustomTabs-Kotlin
 */
package io.camlcase.smartwallet.core.customtabs

import android.app.Activity
import android.net.Uri

/**
 * To be used as a fallback to open the Uri when Custom Tabs is not available.
 */
interface CustomTabFallback {

    /**
     *
     * @param activity The Activity that wants to open the Uri.
     * @param uri The uri to be opened by the fallback.
     */
    fun openUri(activity: Activity, uri: Uri)
}
