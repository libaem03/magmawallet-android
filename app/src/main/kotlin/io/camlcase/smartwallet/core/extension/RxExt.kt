/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.core.extension

import io.camlcase.smartwallet.core.androidThread
import io.camlcase.smartwallet.data.core.DataRxThread.newThread
import io.camlcase.smartwallet.data.core.Logger
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.disposables.Disposable
import io.reactivex.rxjava3.functions.BiFunction
import java.util.concurrent.TimeUnit

fun <T> Flowable<T>.subscribeForUI(): Flowable<T> =
    this.subscribeOn(newThread())
        .observeOn(androidThread())

fun <T> Single<T>.subscribeForUI(): Single<T> =
    this.subscribeOn(newThread())
        .observeOn(androidThread())

fun <T> Observable<T>.subscribeForUI(): Observable<T> =
    this.subscribeOn(newThread())
        .observeOn(androidThread())

fun Disposable.addToDisposables(disposables: CompositeDisposable?) = disposables?.add(this)

/**
 * Will retry if [predicate] is true on a interval of [delayInMillis], as much as [maxRetry] times.
 *
 * See https://medium.com/@nicolas.duponchel/try-to-retry-with-rxkotlin-69906b2d181e
 */
fun <T> Observable<T>.retry(predicate: (Throwable) -> Boolean, maxRetry: Long, delayInMillis: Long): Observable<T> =
    retryWhen { errorObservable ->
        Observable.zip(
            errorObservable.map { if (predicate(it)) it else throw it },
            Observable.interval(delayInMillis, TimeUnit.MILLISECONDS),
            BiFunction { t1: Throwable, range: Long -> if (range >= maxRetry) throw t1 })
    }

/**
 * Will retry if [predicate] is true on a interval of [delayInMillis], as much as [maxRetry] times.
 */
fun <T> Flowable<T>.retry(predicate: (Throwable) -> Boolean, maxRetry: Long, delayInMillis: Long): Flowable<T> =
    retryWhen { errorObservable ->
        Flowable.zip(
            errorObservable.map {
                Logger.w("retry on $it")
                if (predicate(it)) {
                    it
                } else {
                    throw it
                }
            },
            Flowable.interval(delayInMillis, TimeUnit.MILLISECONDS),
            BiFunction { t1: Throwable, range: Long -> if (range >= maxRetry) throw t1 })
            .onBackpressureDrop()
    }

