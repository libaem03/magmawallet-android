package io.camlcase.smartwallet.core.extension

import android.widget.ImageView
import androidx.annotation.DrawableRes
import com.squareup.picasso.Picasso
import io.camlcase.smartwallet.R

fun ImageView?.showImage(url: String?, @DrawableRes placeholder: Int = R.drawable.circle_blue_bg) {
    this?.also {
        Picasso.get()
            .load(url)
            .placeholder(placeholder)
            .error(placeholder)
            .into(this)
    }
}
