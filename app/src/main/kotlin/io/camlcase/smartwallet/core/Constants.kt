/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.core

import io.camlcase.smartwallet.data.core.Exchange

/**
 * UI
 */

// Throttled clicks
const val SECONDS_BETWEEN_EVENTS = 1
const val MILLIS_BETWEEN_EVENTS = 1000

object Arguments {
    /**
     * Where did the screen come from. Usually an @IntDef annotation.
     * @see [ScreenSource]
     */
    const val ARG_SOURCE = "arg.screen.source"
}

object Exchange {

    /**
     * Decimal value of % of Slippage suggested to the user to be applied to the Dexter exchange.
     */
    val defaultSlippages: List<Double> = listOf(0.01, Exchange.DEFAULT_SLIPPAGE, 0.001)
}

object Migration {
    /**
     * Secure wallet generation was tightened on version 64 but we didn't really started saving this data until 66.
     */
    const val SECURE_WALLET_CREATION_VERSION = 65
}

object Onboarding {
    const val BIP44_PREFIX = "m/44\'/1729\'/"
    const val BIP44_DEFAULT = "0\'/0\'"
}

/**
 * URLS
 */

object URL {
    /**
     * @see https://baking-bad.org/docs/api
     */
    const val BAKER_PROVIDER = "https://baking-bad.org/"

    /**
     * Settings screen feedback external url
     */
    const val FEEDBACK_URL =
        "https://docs.google.com/forms/d/e/1FAIpQLSdynaHhnvnJ_lu_00O8bUYBjpvgNMMc3-AarfG9uBKULOCPrQ/viewform?usp=sf_link"

    const val OPERATION_DETAIL_URL = "https://tzkt.io/"
    const val OPERATION_DETAIL_TESTNET_URL = "https://granadanet.tzkt.io/"
    const val ABOUT_URL = "https://magmawallet.io/about/"
    const val PRIVACY_POLICY_URL = "https://magmawallet.io/android/privacy-policy/"
    const val TERMS_URL = "https://magmawallet.io/android/terms/"
    const val NEWSLETTER_URL = "https://camlcase.io/newsletter/"

    const val DEXTER_URL = "https://dexter.exchange/"

    /**
     * Web link for users interested in knowing more about using passphrase for their wallets
     */
    const val WALLET_SETUP_URL =
        "https://blog.trezor.io/5-reasons-why-you-should-use-a-passphrase-and-3-reasons-why-you-maybe-shouldnt-411c3935ac81"
}

object Currency {
    /**
     * Based on CoinGecko's supported exchange rates
     */
    val POPULAR_CURRENCIES = listOf("usd", "eur", "gbp", "jpy", "rub", "inr", "btc", "eth")
}


