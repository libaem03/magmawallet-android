/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.model.navigation

import androidx.annotation.IntDef

/**
 * Screen source for Main screen
 */
enum class MainSource {
    LAUNCHER,
    ONBOARDING,
    WELCOME_LOGIN,
    BUY_XTZ,
    DEFAULT
}

/**
 * Which tab on the main navigation to select
 */
object MainNavigation {
    @IntDef(TAB_WALLET, TAB_EXCHANGE, TAB_CONTACTS, TAB_ACTIVITY)
    @Retention(AnnotationRetention.SOURCE)
    annotation class Tab

    /**
     * Go to wallet
     */
    const val TAB_WALLET = 0

    /**
     * Go to Exchange
     */
    const val TAB_EXCHANGE = 1

    /**
     * Go to contacts list
     */
    const val TAB_CONTACTS = 2

    /**
     * Go to Activity
     */
    const val TAB_ACTIVITY = 3

    /**
     * Go to Settings
     */
    const val TAB_SETTINGS = 4
}

object WelcomeNavigation {
    @IntDef(SOURCE_LAUNCH, SOURCE_LOGIN)
    @Retention(AnnotationRetention.SOURCE)
    annotation class ScreenSource

    /**
     * User clicked FROM token
     */
    const val SOURCE_LAUNCH = 0

    /**
     * User clicked TO token
     */
    const val SOURCE_LOGIN = 1
}

/**
 * Screen source for SEND flow
 */
object SendNavigation {

    @IntDef(SOURCE_MAIN, SOURCE_TOKEN, SOURCE_MAIN_ONLY_XTZ, SOURCE_CONTACT_DETAIL)
    @Retention(AnnotationRetention.SOURCE)
    annotation class ScreenSource

    /**
     * User navigated from the WLT
     */
    const val SOURCE_MAIN = 0

    /**
     * User navigated from the Token detail screen
     */
    const val SOURCE_TOKEN = 1

    /**
     * User navigated from the WLT AND have multiple token types in their wallet (balance > 0)
     */
    const val SOURCE_MAIN_ONLY_XTZ = 3

    /**
     * User navigated from the CON Detail. We should skip Send TO.
     */
    const val SOURCE_CONTACT_DETAIL = 4
}

/**
 * Confirm the mnemonics shown previously
 */
object SeedPhraseConfirm {
    @IntDef(SOURCE_ONBOARDING, SOURCE_MIGRATION)
    @Retention(AnnotationRetention.SOURCE)
    annotation class ScreenSource

    /**
     * User is creating a new wallet
     */
    const val SOURCE_ONBOARDING = 0

    /**
     * User needs to type their migration mnemonic
     */
    const val SOURCE_MIGRATION = 1
}

/**
 * Set/Edit PIN screen source
 */
object PinNavigation {
    @IntDef(SOURCE_ONBOARDING, SOURCE_SETTINGS)
    @Retention(AnnotationRetention.SOURCE)
    annotation class ScreenSource

    /**
     * User comes from the onboarding, wants to SET pin
     */
    const val SOURCE_ONBOARDING = 0

    /**
     * User comes from Settings, wants to CHANGE pin
     */
    const val SOURCE_SETTINGS = 1
}

/**
 * Screen source for showing its seed phrase to user
 */
object SeedPhraseNavigation {
    @IntDef(SOURCE_ONBOARDING, SOURCE_SETTINGS, SOURCE_MIGRATION)
    @Retention(AnnotationRetention.SOURCE)
    annotation class ScreenSource

    /**
     * User navigated during the Onboarding
     */
    const val SOURCE_ONBOARDING = 0

    /**
     * User comes from Settings
     */
    const val SOURCE_SETTINGS = 1

    /**
     * User is on the Migration flow
     */
    const val SOURCE_MIGRATION = 2
}

object ExchangeChooseTokenNavigation {
    @IntDef(SOURCE_FROM, SOURCE_TO)
    @Retention(AnnotationRetention.SOURCE)
    annotation class ScreenSource

    /**
     * User clicked FROM token
     */
    const val SOURCE_FROM = 0

    /**
     * User clicked TO token
     */
    const val SOURCE_TO = 1
}

object AddContactNavigation {
    @IntDef(SOURCE_ADD, SOURCE_EDIT)
    @Retention(AnnotationRetention.SOURCE)
    annotation class ScreenSource

    /**
     * User wants to add
     */
    const val SOURCE_ADD = 0

    /**
     * User wants to edit
     */
    const val SOURCE_EDIT = 1
}
