package io.camlcase.smartwallet.ui.main.settings

import io.camlcase.smartwallet.core.Migration
import io.camlcase.smartwallet.data.core.Wallet
import io.camlcase.smartwallet.data.model.delegate.TezosBaker
import io.camlcase.smartwallet.data.model.error.AppError
import io.camlcase.smartwallet.data.model.error.ErrorType
import io.camlcase.smartwallet.data.source.local.AppPreferences
import io.camlcase.smartwallet.data.source.local.UserPreferences
import io.camlcase.smartwallet.data.usecase.LogOutUseCase
import io.camlcase.smartwallet.data.usecase.analytics.AnalyticEvent
import io.camlcase.smartwallet.data.usecase.analytics.SendAnalyticUseCase
import io.camlcase.smartwallet.data.usecase.delegate.GetDelegateUseCase
import io.camlcase.smartwallet.data.usecase.info.GetXTZCurrencyExchange
import io.camlcase.smartwallet.model.DataWrapper
import io.camlcase.smartwallet.test.RxTest
import io.mockk.*
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.core.Single
import junit.framework.TestCase.assertNotNull
import junit.framework.TestCase.assertTrue
import org.junit.After
import org.junit.Assert
import org.junit.Test

/**
 * @see [SettingsViewModel]
 */
class SettingsViewModelTest : RxTest() {

    private val useCase = mockk<LogOutUseCase>()
    private val delegateUseCase = mockk<GetDelegateUseCase>()
    private val appPreferences = mockk<AppPreferences>(relaxed = true)
    private val userPreferences = mockk<UserPreferences>(relaxed = true)
    private val analyticUseCase = mockk<SendAnalyticUseCase>(relaxed = true)
    private val currencyRatesUseCase = mockk<GetXTZCurrencyExchange>(relaxed = true)

    private val expectedSettings = 14
    private val expectedHeaders = 4

    private fun initViewModel(): SettingsViewModel {
        return spyk(
            SettingsViewModel(
                delegateUseCase,
                appPreferences,
                userPreferences,
                useCase,
                analyticUseCase,
                currencyRatesUseCase
            )
        )
    }

    @After
    fun after() {
        confirmVerified(useCase)
        confirmVerified(delegateUseCase)
        confirmVerified(appPreferences)
        confirmVerified(analyticUseCase)
        confirmVerified(currencyRatesUseCase)
    }

    @Test
    fun `Init settings with nothing`() {
        every { appPreferences.getBoolean(AppPreferences.BIOMETRIC_LOGIN_ENABLED) } returns false
        every { delegateUseCase.getDelegate() } returns Single.just(Result.failure(AppError(ErrorType.NO_BAKER)))
        every { delegateUseCase.listenToDelegate() } returns Flowable.never()
        every { userPreferences.versionCreated() } returns Migration.SECURE_WALLET_CREATION_VERSION
        every { currencyRatesUseCase.getCurrencyCode() } returns Wallet.DEFAULT_CURRENCY

        val viewModel = initViewModel()

        viewModel.initSettings(AppPreferences.BIOMETRIC_LOGIN_ENABLED)

        viewModel.data.value.apply {
            assertNotNull(this)
            assertTrue(this is DataWrapper.Success)
            val list = this!!.data!!
            rowAsserts(
                list,
                biometric = true,
                expectedChecked = false,
                expectedDelegate = null,
                expectedCurrency = Wallet.DEFAULT_CURRENCY
            )
        }

        validateSettingsCalls(viewModel)
    }

    private fun validateSettingsCalls(viewModel: SettingsViewModel){
        verifySequence {
            viewModel.initSettings(AppPreferences.BIOMETRIC_LOGIN_ENABLED)
            delegateUseCase.listenToDelegate()
            delegateUseCase.getDelegate()
            appPreferences.getBoolean(AppPreferences.BIOMETRIC_LOGIN_ENABLED)
            currencyRatesUseCase.getCurrencyCode()
            viewModel.disposables
            viewModel.disposables
            viewModel.data
        }
    }

    private fun rowAsserts(
        list: List<SettingsItem>,
        biometric: Boolean,
        expectedChecked: Boolean,
        expectedDelegate: String?,
        expectedCurrency: String,
        expectedSettingsRows: Int = expectedSettings
    ) {
        Assert.assertTrue(list.count { it is HeaderSettings } == expectedHeaders)
        Assert.assertTrue(list.count { it is SettingsForView } == expectedSettingsRows)

        if (biometric) {
            assertBiometricRow(list.firstOrNull { it.type == SettingType.BIOMETRIC_LOGIN } as SettingsForView,
                expectedChecked)
        }
        assertDelegateRow(list.firstOrNull { it.type == SettingType.DELEGATE } as SettingsForView, expectedDelegate)
        assertEditPinRow(list.firstOrNull { it.type == SettingType.EDIT_PIN } as SettingsForView)
        assertRecoveryPhraseRow(list.firstOrNull { it.type == SettingType.RECOVERY_PHRASE } as SettingsForView)
        assertFeedbackRow(list.firstOrNull { it.type == SettingType.FEEDBACK } as SettingsForView)
        assertDeleteWalletRow(list.firstOrNull { it.type == SettingType.DELETE_WALLET } as SettingsForView)
        assertVersion(list.firstOrNull { it.type == SettingType.VERSION } as SettingsForView)
        assertLocalCurrencyRow(list.firstOrNull { it.type == SettingType.LOCAL_CURRENCY } as SettingsForView, expectedCurrency)
    }

    private fun assertBiometricRow(view: SettingsForView, expectedChecked: Boolean = false) {
        assertTrue(view.type == SettingType.BIOMETRIC_LOGIN)
        assertTrue(view.detail == null)
        assertTrue(view.checked == expectedChecked)
    }

    private fun assertDelegateRow(view: SettingsForView, expectedDelegate: String?) {
        assertTrue(view.type == SettingType.DELEGATE)
        assertTrue(view.detail == expectedDelegate)
        assertTrue(!view.checked)
    }

    private fun assertRecoveryPhraseRow(view: SettingsForView) {
        assertTrue(view.type == SettingType.RECOVERY_PHRASE)
        assertTrue(view.detail == null)
        assertTrue(!view.checked)
    }

    private fun assertFeedbackRow(view: SettingsForView) {
        assertTrue(view.type == SettingType.FEEDBACK)
        assertTrue(view.detail == null)
        assertTrue(!view.checked)
    }

    private fun assertDeleteWalletRow(view: SettingsForView) {
        assertTrue(view.type == SettingType.DELETE_WALLET)
        assertTrue(view.detail == null)
        assertTrue(!view.checked)
    }

    private fun assertEditPinRow(view: SettingsForView) {
        assertTrue(view.type == SettingType.EDIT_PIN)
        assertTrue(view.detail == null)
        assertTrue(!view.checked)
    }

    private fun assertVersion(view: SettingsForView) {
        assertTrue(view.type == SettingType.VERSION)
        assertTrue(view.detail == null)
        assertTrue(!view.checked)
    }

    private fun assertLocalCurrencyRow(view: SettingsForView, currency: String) {
        assertTrue(view.type == SettingType.LOCAL_CURRENCY)
        assertTrue(view.detail == currency)
        assertTrue(!view.checked)
    }

    @Test
    fun `Init settings with delegator`() {
        val expected = "tz1Delegator_Address"

        every { appPreferences.getBoolean(AppPreferences.BIOMETRIC_LOGIN_ENABLED) } returns false
        every { delegateUseCase.getDelegate() } returns Single.just(Result.success(TezosBaker(null, expected)))
        every { delegateUseCase.listenToDelegate() } returns Flowable.never()
        every { userPreferences.versionCreated() } returns Migration.SECURE_WALLET_CREATION_VERSION
        every { currencyRatesUseCase.getCurrencyCode() } returns Wallet.DEFAULT_CURRENCY

        val viewModel = initViewModel()

        viewModel.initSettings(AppPreferences.BIOMETRIC_LOGIN_ENABLED)

        viewModel.data.value.apply {
            assertNotNull(this)
            assertTrue(this is DataWrapper.Success)
            val list = this!!.data!!
            rowAsserts(
                list, biometric = true, expectedChecked = false, expectedDelegate = expected,
                expectedCurrency = Wallet.DEFAULT_CURRENCY
            )
        }

        validateSettingsCalls(viewModel)
    }

    @Test
    fun `Init settings with biometric login enabled`() {
        every { appPreferences.getBoolean(AppPreferences.BIOMETRIC_LOGIN_ENABLED) } returns true
        every { delegateUseCase.getDelegate() } returns Single.just(Result.failure(AppError(ErrorType.NO_BAKER)))
        every { delegateUseCase.listenToDelegate() } returns Flowable.never()
        every { userPreferences.versionCreated() } returns Migration.SECURE_WALLET_CREATION_VERSION
        every { currencyRatesUseCase.getCurrencyCode() } returns Wallet.DEFAULT_CURRENCY

        val viewModel = initViewModel()

        viewModel.initSettings(AppPreferences.BIOMETRIC_LOGIN_ENABLED)

        viewModel.data.value.apply {
            assertNotNull(this)
            assertTrue(this is DataWrapper.Success)
            val list = this!!.data!!
            rowAsserts(list, biometric = true, expectedChecked = true, expectedDelegate = null, expectedCurrency = Wallet.DEFAULT_CURRENCY)
        }

        validateSettingsCalls(viewModel)
    }

    @Test
    fun `Init settings with delegator with name`() {
        val expected = "tz1Delegator_Address"

        every { appPreferences.getBoolean(AppPreferences.BIOMETRIC_LOGIN_ENABLED) } returns false
        every { delegateUseCase.getDelegate() } returns Single.just(
            Result.success(TezosBaker(expected, "tz1Something"))
        )
        every { delegateUseCase.listenToDelegate() } returns Flowable.never()
        every { userPreferences.versionCreated() } returns Migration.SECURE_WALLET_CREATION_VERSION
        every { currencyRatesUseCase.getCurrencyCode() } returns Wallet.DEFAULT_CURRENCY

        val viewModel = initViewModel()

        viewModel.initSettings(AppPreferences.BIOMETRIC_LOGIN_ENABLED)

        viewModel.data.value.apply {
            assertNotNull(this)
            assertTrue(this is DataWrapper.Success)
            val list = this!!.data!!
            rowAsserts(list, biometric = true, expectedChecked = false, expectedDelegate = expected, expectedCurrency = Wallet.DEFAULT_CURRENCY)
        }

        validateSettingsCalls(viewModel)
    }

    @Test
    fun `Test deleteWallet calls`() {
        every { useCase.logOut() } just Runs

        val viewModel = initViewModel()
        viewModel.deleteWallet {
            Assert.assertTrue(true)
        }

        verify {
            useCase.logOut()
            analyticUseCase.reportEvent(AnalyticEvent.WALLET_UNPAIRED)
        }
    }

    @Test
    fun `Test updateBiometricLogin true`() {
        every { appPreferences.store(AppPreferences.BIOMETRIC_LOGIN_ENABLED, true) } just Runs

        val viewModel = initViewModel()
        viewModel.updateBiometricLogin(true)

        verify {
            appPreferences.store(AppPreferences.BIOMETRIC_LOGIN_ENABLED, true)
        }
    }

    @Test
    fun `Test updateBiometricLogin false`() {
        every { appPreferences.store(AppPreferences.BIOMETRIC_LOGIN_ENABLED, false) } just Runs

        val viewModel = initViewModel()
        viewModel.updateBiometricLogin(false)

        verify {
            appPreferences.store(AppPreferences.BIOMETRIC_LOGIN_ENABLED, false)
        }
    }

    @Test
    fun `initSettings No biometric hardware`() {
        every { delegateUseCase.getDelegate() } returns Single.just(Result.failure(AppError(ErrorType.NO_BAKER)))
        every { delegateUseCase.listenToDelegate() } returns Flowable.never()
        every { userPreferences.versionCreated() } returns Migration.SECURE_WALLET_CREATION_VERSION
        every { currencyRatesUseCase.getCurrencyCode() } returns Wallet.DEFAULT_CURRENCY

        val viewModel = initViewModel()

        viewModel.initSettings()

        viewModel.data.value.apply {
            assertNotNull(this)
            assertTrue(this is DataWrapper.Success)
            val list = this!!.data!!
            rowAsserts(
                list,
                biometric = false,
                expectedChecked = false,
                expectedDelegate = null,
                 expectedCurrency = Wallet.DEFAULT_CURRENCY,
                expectedSettingsRows = (expectedSettings - 1)
            )
        }

        verifySequence {
            viewModel.initSettings()
            delegateUseCase.listenToDelegate()
            delegateUseCase.getDelegate()
            currencyRatesUseCase.getCurrencyCode()
            viewModel.disposables
            viewModel.disposables
            viewModel.data
        }
    }

    @Test
    fun `initSettings getCurrencyCode BTC`() {
        every { appPreferences.getBoolean(AppPreferences.BIOMETRIC_LOGIN_ENABLED) } returns false
        every { delegateUseCase.getDelegate() } returns Single.just(Result.failure(AppError(ErrorType.NO_BAKER)))
        every { delegateUseCase.listenToDelegate() } returns Flowable.never()
        every { userPreferences.versionCreated() } returns Migration.SECURE_WALLET_CREATION_VERSION
        every { currencyRatesUseCase.getCurrencyCode() } returns "BTC"

        val viewModel = initViewModel()

        viewModel.initSettings(AppPreferences.BIOMETRIC_LOGIN_ENABLED)

        viewModel.data.value.apply {
            assertNotNull(this)
            assertTrue(this is DataWrapper.Success)
            val list = this!!.data!!
            rowAsserts(
                list,
                biometric = true,
                expectedChecked = false,
                expectedDelegate = null,
                expectedCurrency = "BTC"
            )
        }

        validateSettingsCalls(viewModel)
    }
}
