package io.camlcase.smartwallet.ui.migration.funds

import io.camlcase.kotlintezos.model.Tez
import io.camlcase.smartwallet.data.core.Migration
import io.camlcase.smartwallet.data.core.Tokens
import io.camlcase.smartwallet.data.core.XTZ
import io.camlcase.smartwallet.data.model.amount.CoinBalance
import io.camlcase.smartwallet.data.model.amount.TokenBalance
import io.camlcase.smartwallet.data.model.exchange.DexterBalance
import io.camlcase.smartwallet.data.model.exchange.FATokenInfo
import io.camlcase.smartwallet.data.model.exchange.TokenInfoBundle
import io.camlcase.smartwallet.data.model.exchange.XTZTokenInfo
import io.camlcase.smartwallet.model.DataWrapper
import io.camlcase.smartwallet.test.RxTest
import io.mockk.spyk
import junit.framework.TestCase
import org.junit.Assert
import org.junit.Test
import java.math.BigInteger

/**
 * @see BalanceCheckViewModel
 */
class BalanceCheckViewModelTest : RxTest() {
    private fun initViewModel(): BalanceCheckViewModel {
        return spyk(BalanceCheckViewModel())
    }

    @Test
    fun `getBalanceCheck NO_TRANSFER`() {
        val viewModel = initViewModel()
        viewModel.getBalanceCheck(emptyList())

        viewModel.data.value.apply {
            TestCase.assertNotNull(this)
            TestCase.assertTrue(this is DataWrapper.Success)
            val item = this!!.data!!
            Assert.assertTrue(item.transferFundsType == MigrationTransferFundsType.NO_TRANSFER)
            Assert.assertTrue(item.neededXTZ == Tez.zero)
        }
    }

    @Test
    fun `getBalanceCheck SHOULD_TRANSFER 0 balance`() {
        val xtzBalance = Tez.zero
        val balances =
            listOf(
                TokenInfoBundle(XTZ, CoinBalance(xtzBalance), XTZTokenInfo(1.0)),
                TokenInfoBundle(
                    Tokens.Testnet.TZBTC, TokenBalance(BigInteger.ZERO, Tokens.Testnet.TZBTC.decimals),
                    FATokenInfo(Tokens.Testnet.TZBTC, 1.0, DexterBalance.empty)
                ),
                TokenInfoBundle(
                    Tokens.Testnet.USDTZ, TokenBalance(BigInteger.ZERO, Tokens.Testnet.USDTZ.decimals),
                    FATokenInfo(Tokens.Testnet.USDTZ, 1.0, DexterBalance.empty)
                )
            )

        val viewModel = initViewModel()
        viewModel.getBalanceCheck(balances)

        viewModel.data.value.apply {
            TestCase.assertNotNull(this)
            TestCase.assertTrue(this is DataWrapper.Success)
            val item = this!!.data!!
            Assert.assertTrue(item.transferFundsType == MigrationTransferFundsType.NO_TRANSFER)
            Assert.assertTrue(item.neededXTZ == Tez.zero)
        }
    }

    @Test
    fun `getBalanceCheck NO_XTZ_BALANCE 0-0`() {
        val balances =
            listOf(
                TokenInfoBundle(XTZ, CoinBalance(Tez(0.0)), XTZTokenInfo(1.0)),
                TokenInfoBundle(
                    Tokens.Testnet.TZBTC, TokenBalance(BigInteger("100"), Tokens.Testnet.TZBTC.decimals),
                    FATokenInfo(Tokens.Testnet.TZBTC, 1.0, DexterBalance.empty)
                ),
                TokenInfoBundle(
                    Tokens.Testnet.USDTZ, TokenBalance(BigInteger("200"), Tokens.Testnet.USDTZ.decimals),
                    FATokenInfo(Tokens.Testnet.USDTZ, 1.0, DexterBalance.empty)
                )
            )

        val viewModel = initViewModel()
        viewModel.getBalanceCheck(balances)

        viewModel.data.value.apply {
            TestCase.assertNotNull(this)
            TestCase.assertTrue(this is DataWrapper.Success)
            val item = this!!.data!!
            Assert.assertTrue(item.transferFundsType == MigrationTransferFundsType.NO_XTZ_BALANCE)
            Assert.assertTrue(item.neededXTZ == Migration.XTZ_BUFFER)
        }
    }

    @Test
    fun `getBalanceCheck NO_XTZ_BALANCE 0-3`() {
        val xtzBalance = Tez(0.3)
        val balances =
            listOf(
                TokenInfoBundle(XTZ, CoinBalance(xtzBalance), XTZTokenInfo(1.0)),
                TokenInfoBundle(
                    Tokens.Testnet.TZBTC, TokenBalance(BigInteger("100"), Tokens.Testnet.TZBTC.decimals),
                    FATokenInfo(Tokens.Testnet.TZBTC, 1.0, DexterBalance.empty)
                ),
                TokenInfoBundle(
                    Tokens.Testnet.USDTZ, TokenBalance(BigInteger("200"), Tokens.Testnet.USDTZ.decimals),
                    FATokenInfo(Tokens.Testnet.USDTZ, 1.0, DexterBalance.empty)
                )
            )

        val viewModel = initViewModel()
        viewModel.getBalanceCheck(balances)

        viewModel.data.value.apply {
            TestCase.assertNotNull(this)
            TestCase.assertTrue(this is DataWrapper.Success)
            val item = this!!.data!!
            Assert.assertTrue(item.transferFundsType == MigrationTransferFundsType.NO_XTZ_BALANCE)
            Assert.assertTrue(item.neededXTZ == (Migration.XTZ_BUFFER - xtzBalance))
        }
    }

    @Test
    fun `getBalanceCheck NO_XTZ_BALANCE 0-5`() {
        val xtzBalance = Tez(0.5)
        val balances =
            listOf(
                TokenInfoBundle(XTZ, CoinBalance(xtzBalance), XTZTokenInfo(1.0)),
                TokenInfoBundle(
                    Tokens.Testnet.TZBTC, TokenBalance(BigInteger("100"), Tokens.Testnet.TZBTC.decimals),
                    FATokenInfo(Tokens.Testnet.TZBTC, 1.0, DexterBalance.empty)
                ),
                TokenInfoBundle(
                    Tokens.Testnet.USDTZ, TokenBalance(BigInteger("200"), Tokens.Testnet.USDTZ.decimals),
                    FATokenInfo(Tokens.Testnet.USDTZ, 1.0, DexterBalance.empty)
                )
            )

        val viewModel = initViewModel()
        viewModel.getBalanceCheck(balances)

        viewModel.data.value.apply {
            TestCase.assertNotNull(this)
            TestCase.assertTrue(this is DataWrapper.Success)
            val item = this!!.data!!
            Assert.assertTrue(item.transferFundsType == MigrationTransferFundsType.NO_XTZ_BALANCE)
            Assert.assertTrue(item.neededXTZ == Tez.zero)
        }
    }

    @Test
    fun `getBalanceCheck SHOULD_TRANSFER 0-5-1 xtz`() {
        val xtzBalance = Tez(0.500_001)
        val balances =
            listOf(
                TokenInfoBundle(XTZ, CoinBalance(xtzBalance), XTZTokenInfo(1.0)),
                TokenInfoBundle(
                    Tokens.Testnet.TZBTC, TokenBalance(BigInteger("100"), Tokens.Testnet.TZBTC.decimals),
                    FATokenInfo(Tokens.Testnet.TZBTC, 1.0, DexterBalance.empty)
                ),
                TokenInfoBundle(
                    Tokens.Testnet.USDTZ, TokenBalance(BigInteger("200"), Tokens.Testnet.USDTZ.decimals),
                    FATokenInfo(Tokens.Testnet.USDTZ, 1.0, DexterBalance.empty)
                )
            )

        val viewModel = initViewModel()
        viewModel.getBalanceCheck(balances)

        viewModel.data.value.apply {
            TestCase.assertNotNull(this)
            TestCase.assertTrue(this is DataWrapper.Success)
            val item = this!!.data!!
            Assert.assertTrue(item.transferFundsType == MigrationTransferFundsType.SHOULD_TRANSFER)
            Assert.assertTrue(item.neededXTZ == Tez.zero)
        }
    }

    @Test
    fun `getBalanceCheck KO`() {
        val balances =
            listOf(
                TokenInfoBundle(XTZ, TokenBalance(BigInteger.TEN, 2), XTZTokenInfo(1.0)),
            )

        val viewModel = initViewModel()
        viewModel.getBalanceCheck(balances)

        viewModel.data.value.apply {
            TestCase.assertNotNull(this)
            TestCase.assertTrue(this is DataWrapper.Error)
            val item = this!!.error
            Assert.assertTrue(item is ClassCastException)
        }
    }
}
