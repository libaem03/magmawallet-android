package io.camlcase.smartwallet.ui.onboarding.seed_phrase

import io.camlcase.smartwallet.data.model.AppWallet
import io.camlcase.smartwallet.data.model.error.AppError
import io.camlcase.smartwallet.data.model.error.ErrorType
import io.camlcase.smartwallet.data.usecase.user.CreateWalletUseCase
import io.camlcase.smartwallet.data.usecase.user.GetSeedPhraseUseCase
import io.camlcase.smartwallet.model.DataWrapper
import io.camlcase.smartwallet.model.navigation.SeedPhraseNavigation
import io.camlcase.smartwallet.test.RxTest
import io.camlcase.smartwallet.test.mock.mainTestnetAddress
import io.mockk.*
import io.reactivex.rxjava3.core.Single
import junit.framework.TestCase
import org.junit.After
import org.junit.Assert
import org.junit.Test

/**
 * @see SeedPhraseViewModel
 */
class SeedPhraseViewModelTest : RxTest() {
    private val useCase = mockk<GetSeedPhraseUseCase>(relaxed = true)
    private val createWalletUseCase = mockk<CreateWalletUseCase>(relaxed = true)
    private val mnemonicList = listOf("phoenix", "tezos", "camlcase")

    @After
    fun after() {
        confirmVerified(useCase)
        confirmVerified(createWalletUseCase)
    }

    private fun initViewModel(): SeedPhraseViewModel {
        return spyk(SeedPhraseViewModel(useCase, createWalletUseCase))
    }

    @Test
    fun `Test getSeedPhrase even list`() {
        every { useCase.getSeedPhraseData() } returns Single.just(Pair(mnemonicList, null))

        val viewModel = initViewModel()
        viewModel.getSeedPhrase(SeedPhraseNavigation.SOURCE_SETTINGS)

        viewModel.data.value.apply {
            Assert.assertNotNull(this)
            TestCase.assertTrue(this is DataWrapper.Success)
            val result = this!!.data!!
            Assert.assertTrue(result.left + result.right == mnemonicList)
            Assert.assertTrue(result.middle == mnemonicList.size / 2)
            Assert.assertTrue(result.derivationPath == null)
        }

        verify {
            useCase.getSeedPhraseData()
            viewModel.clearOperationSubscription()
        }
    }

    @Test
    fun `getSeedPhrase DerivationPath`() {
        val derivationPath = "m/2/2/2"
        every { useCase.getSeedPhraseData() } returns Single.just(Pair(mnemonicList, derivationPath))

        val viewModel = initViewModel()
        viewModel.getSeedPhrase(SeedPhraseNavigation.SOURCE_SETTINGS)

        viewModel.data.value.apply {
            Assert.assertNotNull(this)
            TestCase.assertTrue(this is DataWrapper.Success)
            val result = this!!.data!!
            Assert.assertTrue(result.left + result.right == mnemonicList)
            Assert.assertTrue(result.middle == mnemonicList.size / 2)
            Assert.assertTrue(result.derivationPath == derivationPath)
        }

        verify {
            useCase.getSeedPhraseData()
            viewModel.clearOperationSubscription()
        }
    }

    @Test
    fun `Test getSeedPhrase KO`() {
        val magmaError = AppError(ErrorType.NO_WALLET)
        every { useCase.getSeedPhraseData() } returns Single.error(magmaError)

        val viewModel = initViewModel()
        viewModel.getSeedPhrase(SeedPhraseNavigation.SOURCE_SETTINGS)

        viewModel.data.value.apply {
            Assert.assertNotNull(this)
            TestCase.assertTrue(this is DataWrapper.Error)
            Assert.assertTrue(this!!.error == magmaError)
        }

        verify {
            useCase.getSeedPhraseData()
            viewModel.clearOperationSubscription()
        }
    }

    // Mocked to avoid using native library
    private var wallet = mockk<AppWallet>(relaxed = true) {
        every { mnemonic } returns mnemonicList
        every { address } returns mainTestnetAddress
    }

    @Test
    fun `getSeedPhrase Migration OK`() {
        every { createWalletUseCase.getMigrationWallet() } returns wallet

        val viewModel = initViewModel()
        viewModel.getSeedPhrase(SeedPhraseNavigation.SOURCE_MIGRATION)

        viewModel.data.value.apply {
            Assert.assertNotNull(this)
            TestCase.assertTrue(this is DataWrapper.Success)
            val result = this!!.data!!
            Assert.assertTrue(result.left + result.right == mnemonicList)
            Assert.assertTrue(result.middle == mnemonicList.size / 2)
        }

        verify {
            createWalletUseCase.getMigrationWallet()
            viewModel.clearOperationSubscription()
        }
    }

    @Test
    fun `getSeedPhrase Migration KO`() {
        val magmaError = AppError(ErrorType.NO_WALLET)
        every { createWalletUseCase.getMigrationWallet() } throws magmaError

        val viewModel = initViewModel()
        viewModel.getSeedPhrase(SeedPhraseNavigation.SOURCE_MIGRATION)

        viewModel.data.value.apply {
            Assert.assertNotNull(this)
            TestCase.assertTrue(this is DataWrapper.Error)
            Assert.assertTrue(this!!.error == magmaError)
        }

        verify {
            createWalletUseCase.getMigrationWallet()
            viewModel.clearOperationSubscription()
        }
    }

    // We behave like SOURCE_SETTINGS
    @Test
    fun `getSeedPhrase Origination OK`() {
        every { useCase.getSeedPhraseData() } returns Single.just(Pair(mnemonicList, null))

        val viewModel = initViewModel()
        viewModel.getSeedPhrase(SeedPhraseNavigation.SOURCE_ONBOARDING)

        viewModel.data.value.apply {
            Assert.assertNotNull(this)
            TestCase.assertTrue(this is DataWrapper.Success)
            val result = this!!.data!!
            Assert.assertTrue(result.left + result.right == mnemonicList)
            Assert.assertTrue(result.middle == mnemonicList.size / 2)
            Assert.assertTrue(result.derivationPath == null)
        }

        verify {
            useCase.getSeedPhraseData()
            viewModel.clearOperationSubscription()
        }
    }

    @Test
    fun `hasPassphrase TRUE OK`() {
        every { useCase.hasPassphrase() } returns Single.just(true)

        val viewModel = initViewModel()
        viewModel.hasPassphrase {
            Assert.assertTrue(it)
        }

        verify {
            useCase.hasPassphrase()
            viewModel.clearOperationSubscription()
        }
    }

    @Test
    fun `hasPassphrase FALSE OK`() {
        every { useCase.hasPassphrase() } returns Single.just(false)

        val viewModel = initViewModel()
        viewModel.hasPassphrase {
            Assert.assertFalse(it)
        }

        verify {
            useCase.hasPassphrase()
            viewModel.clearOperationSubscription()
        }
    }

    @Test
    fun `hasPassphrase KO`() {
        every { useCase.hasPassphrase() } returns Single.error(IllegalStateException())

        val viewModel = initViewModel()
        viewModel.hasPassphrase {
            Assert.assertFalse(it)
        }

        verify {
            useCase.hasPassphrase()
            viewModel.clearOperationSubscription()
        }
    }

    @Test
    fun `confirmMnemonic TRUE OK`() {
        every { useCase.confirmMnemonic(any(), any()) } returns Single.just(true)
        val viewModel = initViewModel()
        viewModel.confirmMnemonic(mnemonicList, null)

        viewModel.validation.value.apply {
            Assert.assertNotNull(this)
            TestCase.assertTrue(this is DataWrapper.Success)
            val result = this!!.data!!
            Assert.assertTrue(result)
        }

        verify {
            useCase.confirmMnemonic(any(), any())
            viewModel.clearOperationSubscription()
        }
    }

    @Test
    fun `confirmMnemonic KO`() {
        every { useCase.confirmMnemonic(any(), any()) } returns Single.error(AppError(ErrorType.WALLET_CREATE_ERROR))
        val viewModel = initViewModel()
        viewModel.confirmMnemonic(mnemonicList, null)

        viewModel.validation.value.apply {
            Assert.assertNotNull(this)
            TestCase.assertTrue(this is DataWrapper.Error)
            val result = this!!.error!!
            Assert.assertTrue(result is AppError)
        }

        verify {
            useCase.confirmMnemonic(any(), any())
            viewModel.clearOperationSubscription()
        }
    }
}
