package io.camlcase.smartwallet.ui.exchange.amount

import io.camlcase.kotlintezos.model.Tez
import io.camlcase.smartwallet.data.core.Exchange
import io.camlcase.smartwallet.data.core.Tokens
import io.camlcase.smartwallet.data.core.Tokens.testnetFATokens
import io.camlcase.smartwallet.data.core.Wallet
import io.camlcase.smartwallet.data.core.XTZ
import io.camlcase.smartwallet.data.model.Token
import io.camlcase.smartwallet.data.model.amount.CoinBalance
import io.camlcase.smartwallet.data.model.amount.EmptyBalance
import io.camlcase.smartwallet.data.model.amount.TokenBalance
import io.camlcase.smartwallet.data.model.exchange.DexterBalance
import io.camlcase.smartwallet.data.model.exchange.FATokenInfo
import io.camlcase.smartwallet.data.model.exchange.TokenInfoBundle
import io.camlcase.smartwallet.data.model.exchange.XTZTokenInfo
import io.camlcase.smartwallet.data.source.local.AppPreferences
import io.camlcase.smartwallet.data.usecase.info.GetBCDBalanceUseCase
import io.camlcase.smartwallet.data.usecase.operation.DexterCalculationsUseCase
import io.camlcase.smartwallet.model.DataWrapper
import io.camlcase.smartwallet.test.RxTest
import io.camlcase.smartwallet.test.mock.TEST_TOKEN
import io.camlcase.smartwallet.test.mock.tokenToXtzDexterExchange
import io.camlcase.smartwallet.test.mock.xtzToTokenDexterExchange
import io.camlcase.smartwallet.ui.exchange.amount.ChooseTokenViewModelTest.Companion.testToken2
import io.mockk.*
import io.reactivex.rxjava3.core.Single
import org.junit.After
import org.junit.Assert
import org.junit.Test
import java.io.IOException
import java.math.BigDecimal
import java.math.BigInteger

/**
 * @see ExchangeAmountViewModel
 */
class ExchangeAmountViewModelTest : RxTest() {
    private val useCase = mockk<GetBCDBalanceUseCase>()
    private val supportedFATokens = Tokens.testnetFATokens
    private val appPreference = mockk<AppPreferences>()
    private val calculationsUseCase = mockk<DexterCalculationsUseCase>(relaxed = true)

    private fun initViewModel(): ExchangeAmountViewModel {
        return spyk(ExchangeAmountViewModel(useCase, supportedFATokens, appPreference, calculationsUseCase))
    }

    @After
    fun after() {
        confirmVerified(useCase)
        confirmVerified(appPreference)
        confirmVerified(calculationsUseCase)
    }

    @Test
    fun `Test getTokenBalances empty`() {
        every { useCase.getCurrencyBundle() } returns Wallet.defaultCurrency
        val viewModel = spyk(ExchangeAmountViewModel(useCase, emptyList(), appPreference, calculationsUseCase))
        viewModel.getTokenBalances()

        viewModel.data.value.apply {
            Assert.assertNotNull(this)
            Assert.assertTrue(this is DataWrapper.Empty)
        }

        verify {
            useCase.getCurrencyBundle()
        }
    }

    @Test
    fun `Test getTokenBalances OK`() {
        every { useCase.getLocalBalances() } returns tokenBalances.map { it.getOrThrow() }
        every { useCase.getCurrencyBundle() } returns Wallet.defaultCurrency
        every { calculationsUseCase.calculateMarketRate(any(), any()) } returns Single.just(BigDecimal.ONE)

        val viewModel = initViewModel()
        viewModel.getTokenBalances()

        viewModel.data.value.apply {
            Assert.assertNotNull(this)
            Assert.assertTrue(this is DataWrapper.Success)
            val result = (this as DataWrapper.Success).data
            Assert.assertTrue(result!!.fromToken == XTZ)
            Assert.assertTrue(result!!.toToken == currentToken)
            Assert.assertTrue(result!!.fromBalance == "20")
        }

        verify {
            useCase.getLocalBalances()
            useCase.getCurrencyBundle()
            calculationsUseCase.calculateMarketRate(any(), any())
        }
    }

    @Test
    fun `Test getTokenBalances Failed`() {
        val expected = listOf(
            Result.success(
                TokenInfoBundle(
                    XTZ,
                    CoinBalance(Tez(20.0)), XTZTokenInfo(1.0)
                )
            ),
            Result.failure(IOException("ERROR"))
        )
        every { useCase.fallbackBalances() } returns Single.just(expected)
        every { useCase.getCurrencyBundle() } returns Wallet.defaultCurrency

        val viewModel = initViewModel()
        viewModel.getTokenBalances()

        viewModel.data.value.apply {
            println(this)
            Assert.assertNotNull(this)
            Assert.assertTrue(this is DataWrapper.Error)
        }

        verify {
            useCase.getLocalBalances()
            useCase.getCurrencyBundle()
        }
    }

    @Test
    fun `Test getTokenBalances KO`() {
        every { useCase.getLocalBalances() } throws IOException("ERROR")
        every { useCase.getCurrencyBundle() } returns Wallet.defaultCurrency

        val viewModel = initViewModel()
        viewModel.getTokenBalances()

        viewModel.data.value.apply {
            println(this)
            Assert.assertNotNull(this)
            Assert.assertTrue(this is DataWrapper.Error)
        }

        verify {
            useCase.getLocalBalances()
            useCase.getCurrencyBundle()
        }
    }

    @Test
    fun `Test oneIsXTZ OK`() {
        loadSampleData { viewModel ->
            Assert.assertTrue(viewModel.oneIsXTZ())
        }
    }

    @Test
    fun `Test setTokenFrom Avoid FA12-FA12`() {
        loadSampleData { viewModel ->
            // XTZ > FA
            // Change XTZ to FA12
            viewModel.setTokenFrom(currentToken)
            // Should change automatically to FA > XTZ
            Assert.assertTrue(viewModel.fromToken.token == currentToken)
            Assert.assertTrue(viewModel.toToken.token == XTZ)
            Assert.assertTrue(viewModel.getBalanceFrom() == "10")
            Assert.assertTrue(viewModel.oneIsXTZ())
        }
    }

    @Test
    fun `Test setTokenFrom Avoid XTZ-XTZ`() {
        loadSampleData { viewModel ->
            viewModel.switch()
            // FA > XTZ
            viewModel.setTokenFrom(XTZ)

            // Should have changed to XTZ > FA
            Assert.assertTrue(viewModel.fromToken.token == XTZ)
            Assert.assertTrue(viewModel.toToken.token == currentToken)
            Assert.assertTrue(viewModel.getBalanceFrom() == "20.000000")
            Assert.assertTrue(viewModel.oneIsXTZ())
        }
    }

    @Test
    fun `Test setTokenTo XTZ`() {
        loadSampleData { viewModel ->
            viewModel.setTokenTo(XTZ)

            println("viewModel.fromToken.token ${viewModel.fromToken.token}")
            Assert.assertTrue(viewModel.fromToken.token == currentToken)
            Assert.assertTrue(viewModel.getBalanceFrom() == "10")
            Assert.assertTrue(viewModel.oneIsXTZ())
        }
    }

    @Test
    fun `refreshTokenBalances clear TRUE`() {
        val clear = true
        // getBalances from refreshTokenBalances will always have forceRefresh = true
        every { useCase.getBalances() } returns Single.just(tokenBalances)
        every { calculationsUseCase.calculateMarketRate(any(), any()) } returns Single.just(BigDecimal.ONE)
        val viewModel = initViewModel()
        viewModel.refreshTokenBalances(clear)

        viewModel.data.value.apply {
            println(this)
            Assert.assertNotNull(this)
            Assert.assertTrue(this is DataWrapper.Success)

            val result = (this as DataWrapper.Success).data
            Assert.assertTrue(result!!.fromToken == XTZ)
            Assert.assertTrue(result!!.toToken == currentToken)
            Assert.assertTrue(result!!.fromBalance == "20")
        }

        verify {
            useCase.getBalances()
            calculationsUseCase.calculateMarketRate(any(), any())
        }
    }

    // Clear == false doesn't set From/To tokens
    @Test
    fun `refreshTokenBalances clear FALSE`() {
        // getBalances from refreshTokenBalances will always have forceRefresh = true
        every { useCase.getBalances() } returns Single.just(tokenBalances)
        val viewModel = initViewModel()
        viewModel.refreshTokenBalances()

        viewModel.data.value.apply {
            println(this)
            Assert.assertNotNull(this)
            Assert.assertTrue(this is DataWrapper.Success)

            val result = (this as DataWrapper.Success).data
            Assert.assertTrue(result!!.fromToken == XTZ)
            Assert.assertTrue(result!!.toToken == XTZ)
        }

        verify { useCase.getBalances() }
    }

    @Test
    fun `Test switch OK`() {
        loadSampleData { viewModel ->
            Assert.assertTrue(viewModel.fromToken.token == XTZ)
            Assert.assertTrue(viewModel.toToken.token == currentToken)

            viewModel.switch()

            Assert.assertTrue(viewModel.fromToken.token == currentToken)
            Assert.assertTrue(viewModel.toToken.token == XTZ)
        }
    }

    private fun loadSampleData(before: () -> Unit = {}, action: (ExchangeAmountViewModel) -> Unit) {
        every { useCase.getLocalBalances() } returns tokenBalances.map { it.getOrThrow() }
        every { useCase.getCurrencyBundle() } returns Wallet.defaultCurrency
        every { calculationsUseCase.calculateMarketRate(any(), any()) } returns Single.just(BigDecimal.ONE)
        before()

        val viewModel = initViewModel()
        viewModel.getTokenBalances()

        viewModel.data.value.apply {
            Assert.assertNotNull(this)
            Assert.assertTrue(this is DataWrapper.Success)

            action(viewModel)
        }

        verify {
            useCase.getLocalBalances()
            useCase.getCurrencyBundle()
            calculationsUseCase.calculateMarketRate(any(), any())
        }
    }

    @Test
    fun `Test onAmountFromChanged 0-00 XTZ`() {
        loadSampleData { viewModel ->

            viewModel.onAmountFromChanged(BigDecimal.ZERO) { result ->
                println(result)
                Assert.assertTrue(result.amountFromInCurrency == "${Wallet.DEFAULT_CURRENCY_SYMBOL}0.00")
                Assert.assertTrue(result.fromValidation == ExchangeAmountValidation.ZeroValidation(false))
                Assert.assertTrue(result.toValidation == ExchangeAmountValidation.ZeroValidation(false))
                Assert.assertTrue(!result.enableExchange)
            }
        }
    }

    @Test
    fun `Test onAmountFromChanged 1-005 XTZ`() {
        val input = 1.005.toBigDecimal()
        loadSampleData({
            every { calculationsUseCase.calculate(any(), any(), input) } returns Single.just(
                xtzToTokenDexterExchange
            )
        }) { viewModel ->
            viewModel.onAmountFromChanged(input) { result ->
                println(result)
                Assert.assertTrue(result.fromValidation == ExchangeAmountValidation.BaseValidation(true))
                Assert.assertTrue(result.toValidation == ExchangeAmountValidation.BaseValidation(true))
                Assert.assertTrue(result.enableExchange)
            }
            verify {
                calculationsUseCase.calculate(any(), any(), input)
            }
        }
    }

    @Test
    fun `Test onAmountFromChanged 12_345-005 XTZ`() {
        val input = 12_345.005.toBigDecimal()
        loadSampleData(
            {
                every { calculationsUseCase.calculate(any(), any(), input) } returns Single.just(
                    xtzToTokenDexterExchange
                )
            }
        ) { viewModel ->
            // Over XTZ balance
            viewModel.onAmountFromChanged(input) { result ->
                println(result)
                Assert.assertTrue(result.fromValidation == ExchangeAmountValidation.BaseValidation(false))
                Assert.assertTrue(result.toValidation == ExchangeAmountValidation.BaseValidation(true))
                Assert.assertTrue(!result.enableExchange)
            }

            verify {
                calculationsUseCase.calculate(any(), any(), input)
            }
        }
    }

    @Test
    fun `Test onAmountFromChanged 0-00 FA12`() {
        loadSampleData { viewModel ->
            viewModel.switch()

            viewModel.onAmountFromChanged(BigDecimal.ZERO) { result ->
                println(result)
                Assert.assertTrue(result.amountFromInCurrency == "${Wallet.DEFAULT_CURRENCY_SYMBOL}0.00")
                Assert.assertTrue(result.fromValidation == ExchangeAmountValidation.ZeroValidation(false))
                Assert.assertTrue(result.toValidation == ExchangeAmountValidation.ZeroValidation(false))
                Assert.assertTrue(!result.enableExchange)
            }
        }
    }

    @Test
    fun `Test onAmountFromChanged 0-0005 FA12`() {
        val input = 0.0005.toBigDecimal()
        loadSampleData(
            {
                every { calculationsUseCase.calculate(any(), any(), input) } returns Single.just(
                    tokenToXtzDexterExchange
                )
            }
        ) { viewModel ->
            viewModel.switch()
            verify { useCase.getLocalBalances() }
            // In reality it should never get to this point. We block FA1.2 tokens so they no decimals can be entered
            viewModel.onAmountFromChanged(input) { result ->
                println(result)
                Assert.assertTrue(result.fromValidation == ExchangeAmountValidation.BaseValidation(true))
                Assert.assertTrue(result.toValidation == ExchangeAmountValidation.BaseValidation(true))
                Assert.assertTrue(result.enableExchange)
            }

            verify {
                calculationsUseCase.calculate(any(), any(), input)
            }
        }
    }

    @Test
    fun `Test onAmountFromChanged 1-005 FA12`() {
        val input = 1.005.toBigDecimal()
        loadSampleData(
            {
                every { calculationsUseCase.calculate(any(), any(), input) } returns Single.just(
                    tokenToXtzDexterExchange
                )
            }
        ) { viewModel ->
            viewModel.switch()

            viewModel.onAmountFromChanged(1.005.toBigDecimal()) { result ->
                println(result)
                Assert.assertTrue(result.fromValidation == ExchangeAmountValidation.BaseValidation(true))
                Assert.assertTrue(result.toValidation == ExchangeAmountValidation.BaseValidation(true))
                Assert.assertTrue(result.enableExchange)
            }
            verify {
                calculationsUseCase.calculate(any(), any(), input)
            }
        }
    }

    @Test
    fun `Test onAmountFromChanged 12_234-005 FA12`() {
        val input = 12_234.005.toBigDecimal()
        loadSampleData(
            {
                every { calculationsUseCase.calculate(any(), any(), input) } returns Single.just(
                    tokenToXtzDexterExchange
                )
            }
        ) { viewModel ->
            viewModel.switch()

            viewModel.onAmountFromChanged(input) { result ->
                println(result)
                Assert.assertTrue(result.fromValidation == ExchangeAmountValidation.BaseValidation(false))
                Assert.assertTrue(result.toValidation == ExchangeAmountValidation.BaseValidation(true))
                Assert.assertTrue(!result.enableExchange)
            }

            verify {
                calculationsUseCase.calculate(any(), any(), input)
            }
        }
    }

    @Test
    fun `Test getRequest 1-00 XTZ`() {
        val input = 1.00.toBigDecimal()
        loadSampleData(
            {
                every { calculationsUseCase.calculateMarketRate(any(), any()) } returns Single.just(BigDecimal.ONE)
                every { calculationsUseCase.calculatePriceImpact(any(), any(), any()) } returns Single.just(BigDecimal.ONE)
            }
        ) { viewModel ->
            viewModel.getRequest(input)
            viewModel.operation.value.apply {
                println(this)
                Assert.assertNotNull(this)
                Assert.assertTrue(this is DataWrapper.Success)

                val result = (this as DataWrapper.Success).data!!
                Assert.assertTrue(result.tezToToken)
                Assert.assertTrue(result.fromToken == XTZ)
                Assert.assertTrue(result.toToken == currentToken)
                Assert.assertTrue(result.amountFrom == CoinBalance(Tez(input)))
                Assert.assertTrue(result.amountTo is EmptyBalance) // Will be calculated again in confirmation

                verify {
                    calculationsUseCase.calculateMarketRate(any(), any())
                    calculationsUseCase.calculatePriceImpact(any(), any(), any())
                }
            }
        }
    }

    @Test
    fun `Test getRequest 1-00 FA12`() {
        loadSampleData({
            every { calculationsUseCase.calculateMarketRate(any(), any()) } returns Single.just(BigDecimal.ONE)
            every { calculationsUseCase.calculatePriceImpact(any(), any(), any()) } returns Single.just(BigDecimal.ONE)
        }) { viewModel ->
            viewModel.switch()

            val input = 1.00.toBigDecimal()
            viewModel.getRequest(input)
            viewModel.operation.value.apply {
                println(this)
                Assert.assertNotNull(this)
                Assert.assertTrue(this is DataWrapper.Success)

                val result = (this as DataWrapper.Success).data!!
                Assert.assertTrue(result.tokenToTez)
                Assert.assertTrue(result.fromToken == currentToken)
                Assert.assertTrue(result.toToken == XTZ)
                Assert.assertTrue(result.amountFrom.bigDecimal == input)
                Assert.assertTrue(result.amountTo is EmptyBalance) // Will be calculated again in confirmation

                verify {
                    calculationsUseCase.calculateMarketRate(any(), any())
                    calculationsUseCase.calculatePriceImpact(any(), any(), any())
                }
            }
        }
    }

    @Test
    fun `init Slippage-timeout Stored in Preferences`() {
        val expectedSlippage = 0.15
        val expectedPriceImpact = 0.2.toBigDecimal()
        val expectedTimeout = 10
        every { useCase.getLocalBalances() } returns tokenBalances.map { it.getOrThrow() }
        every { useCase.getCurrencyBundle() } returns Wallet.defaultCurrency
        every { appPreference.getDouble(AppPreferences.EXCHANGE_SLIPPAGE) } returns expectedSlippage
        every { appPreference.getInt(AppPreferences.EXCHANGE_TIMEOUT) } returns expectedTimeout
        every { calculationsUseCase.calculateMarketRate(any(), any()) } returns Single.just(BigDecimal.ONE)
        every { calculationsUseCase.getExchangeRate(any(), any(), any()) } returns Single.just(BigDecimal.ONE)
        val input = 1.0.toBigDecimal()
        every { calculationsUseCase.calculate(any(), any(), input) } returns Single.just(
            xtzToTokenDexterExchange
        )
        every { calculationsUseCase.calculatePriceImpact(any(), any(), any()) } returns Single.just(expectedPriceImpact)

        val viewModel = initViewModel()
        viewModel.getTokenBalances()

        Assert.assertTrue(viewModel.selectedSlippage == Exchange.DEFAULT_SLIPPAGE)
        Assert.assertTrue(viewModel.timeoutMinutes == Exchange.DEFAULT_TIMEOUT_MINUTES)

        viewModel.getRequest(input)
        viewModel.operation.value.apply {
            println(this)
            Assert.assertNotNull(this)
            Assert.assertTrue(this is DataWrapper.Success)

            val result = (this as DataWrapper.Success).data!!
            Assert.assertTrue(result.selectedSlippage == Exchange.DEFAULT_SLIPPAGE)
            Assert.assertTrue(result.priceImpact == expectedPriceImpact)
            Assert.assertTrue(result.timeout == Exchange.DEFAULT_TIMEOUT_MINUTES)
        }

        viewModel.init()

        Assert.assertTrue(viewModel.selectedSlippage == expectedSlippage)
        Assert.assertTrue(viewModel.timeoutMinutes == expectedTimeout)

        viewModel.getRequest(input)
        viewModel.operation.value.apply {
            println(this)
            Assert.assertNotNull(this)
            Assert.assertTrue(this is DataWrapper.Success)

            val result = (this as DataWrapper.Success).data!!
            Assert.assertTrue(result.selectedSlippage == expectedSlippage)
            Assert.assertTrue(result.timeout == expectedTimeout)
        }

        verify {
            useCase.getLocalBalances()
            useCase.getCurrencyBundle()
            appPreference.getDouble(AppPreferences.EXCHANGE_SLIPPAGE)
            appPreference.getInt(AppPreferences.EXCHANGE_TIMEOUT)
            calculationsUseCase.calculateMarketRate(any(), any())
            calculationsUseCase.calculateMarketRate(any(), any())
            calculationsUseCase.calculatePriceImpact(any(), any(), any())
            calculationsUseCase.calculatePriceImpact(any(), any(), any())
        }
    }

    companion object {
        val testToken3 = Token(
            "TestToken 3",
            "tsTk3",
            0,
            "KT1_3",
            "KT1_3"
        )

        val dexterBalance = DexterBalance(Tez(100.0), BigInteger("1000"))

        val currentToken = testnetFATokens[0]
        val currentTokenBalance = TokenInfoBundle(
            currentToken,
            TokenBalance(BigInteger.TEN, 0),
            FATokenInfo(currentToken, 1.0, dexterBalance)
        )
        val tzgBalance = TokenInfoBundle(
            TEST_TOKEN,
            TokenBalance(BigInteger("1000000000"), TEST_TOKEN.decimals),
            FATokenInfo(TEST_TOKEN, 1.0, dexterBalance)
        )
        val tzsBalance = TokenInfoBundle(
            testToken2,
            TokenBalance(BigInteger.TEN, 0),
            FATokenInfo(testToken2, 1.0, dexterBalance)
        )
        val tztBalance = TokenInfoBundle(
            testToken3,
            TokenBalance(BigInteger.TEN, 0),
            FATokenInfo(testToken3, 1.0, dexterBalance)
        )
        val xtzBalance = TokenInfoBundle(
            XTZ,
            CoinBalance(Tez(20.0)), XTZTokenInfo(1.0)
        )
        val tokenBalances = listOf(
            Result.success(xtzBalance),
            Result.success(currentTokenBalance),
            Result.success(tzgBalance),
            Result.success(tzsBalance),
            Result.success(tztBalance)
        )
    }
}
