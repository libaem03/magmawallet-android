package io.camlcase.smartwallet.ui.main.settings.network

import io.camlcase.smartwallet.data.usecase.info.GetNetworkConfigUseCase
import io.mockk.mockk
import io.mockk.spyk
import org.junit.Assert
import org.junit.Test

/**
 * Test Android WEB_URL pattern
 */
class NetworkConfigViewModelURLTest {
    private val useCase = mockk<GetNetworkConfigUseCase>(relaxed = true)

    @Test
    fun test_ValidUrls() {
        val urls = listOf(
            "https://www.magmawallet.io",
            "https://www.magmawallet.io/about",
            "https://dexter.io?parameter=something&parameter2=100",
            "https://localhost:8080",
            "https://t.co",
            "https://www.t.co",
            "https://www.t.co",
            "https://aa.com#gibberish!\$%\$/()",
            "https://www.aa.com",
        )

        val viewModel = spyk(NetworkConfigViewModel(useCase))
        for (url in urls) {
            Assert.assertTrue(viewModel.validUrl(url))
        }
    }

    @Test
    fun test_InvalidUrls() {
        val urls = listOf(
            "http://www.magmawallet.io",
            "http://magmawallet.io",
            "www.magmawallet.io",
            "www.mp3.com",
            "www.t.co",
            "www.aa.com",
            "www.magmawallet-.io",
            "www.-magmawallet.io",
            "httpssh://magmawallet",
            "www.mp3#.com",
            "ssh://secure@magma",
            "dhh://secure@magma",
            "https://aa.com#gibberish!\$·%\$/()"
        )

        val viewModel = spyk(NetworkConfigViewModel(useCase))
        for (url in urls) {
            Assert.assertFalse(viewModel.validUrl(url))
        }
    }
}
