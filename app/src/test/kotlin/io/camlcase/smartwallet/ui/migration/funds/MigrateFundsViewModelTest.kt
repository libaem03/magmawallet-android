package io.camlcase.smartwallet.ui.migration.funds

import io.camlcase.kotlintezos.model.Tez
import io.camlcase.smartwallet.data.core.Tokens.Testnet.TZBTC
import io.camlcase.smartwallet.data.core.Tokens.Testnet.USDTZ
import io.camlcase.smartwallet.data.core.Wallet
import io.camlcase.smartwallet.data.core.XTZ
import io.camlcase.smartwallet.data.core.extension.format
import io.camlcase.smartwallet.data.model.amount.CoinBalance
import io.camlcase.smartwallet.data.model.amount.TokenBalance
import io.camlcase.smartwallet.data.model.exchange.DexterBalance
import io.camlcase.smartwallet.data.model.exchange.FATokenInfo
import io.camlcase.smartwallet.data.model.exchange.TokenInfoBundle
import io.camlcase.smartwallet.data.model.exchange.XTZTokenInfo
import io.camlcase.smartwallet.data.usecase.info.GetBCDBalanceUseCase
import io.camlcase.smartwallet.data.usecase.migration.MigrateFundsUseCase
import io.camlcase.smartwallet.model.DataWrapper
import io.camlcase.smartwallet.test.RxTest
import io.mockk.*
import io.reactivex.rxjava3.core.Single
import junit.framework.TestCase
import org.junit.After
import org.junit.Assert
import org.junit.Test
import java.io.IOException
import java.math.BigInteger

/**
 * @see MigrateFundsViewModel
 */
class MigrateFundsViewModelTest : RxTest() {
    private val useCase = mockk<GetBCDBalanceUseCase>(relaxed = true)
    private val migrateFundsUseCase = mockk<MigrateFundsUseCase>(relaxed = true)

    @After
    fun tearDown() {
        confirmVerified(useCase)
        confirmVerified(migrateFundsUseCase)
    }

    private fun initViewModel(): MigrateFundsViewModel {
        return spyk(MigrateFundsViewModel(useCase, migrateFundsUseCase))
    }

    @Test
    fun `getConfirmationDetails Only XTZ OK`() {
        every { useCase.getCurrencyBundle() } returns Wallet.defaultCurrency
        every { useCase.fallbackBalances() } returns Single.just(
            listOf(
                Result.success(TokenInfoBundle(XTZ, CoinBalance(Tez(1.0)), XTZTokenInfo(1.0)))
            )
        )
        val expectedFees = Tez(0.000_500)
        every { migrateFundsUseCase.calculateFees() } returns Single.just(expectedFees)

        val viewModel = initViewModel()
        viewModel.getConfirmationDetails()

        viewModel.data.value.apply {
            TestCase.assertNotNull(this)
            TestCase.assertTrue(this is DataWrapper.Success)
            val item = this!!.data!!
            Assert.assertTrue(item.error == null)
            Assert.assertTrue(item.fees == expectedFees.format(XTZ.symbol))
            Assert.assertTrue(item.toSend == Tez(1.0).format(XTZ.symbol))
        }

        verify {
            useCase.getCurrencyBundle()
            useCase.fallbackBalances()
            migrateFundsUseCase.calculateFees()
        }
    }

    @Test
    fun `getConfirmationDetails XTZ + FA12 OK`() {
        every { useCase.getCurrencyBundle() } returns Wallet.defaultCurrency
        every { useCase.fallbackBalances() } returns Single.just(
            listOf(
                Result.success(TokenInfoBundle(XTZ, CoinBalance(Tez(1.0)), XTZTokenInfo(1.0))),
                Result.success(
                    TokenInfoBundle(
                        TZBTC, TokenBalance(BigInteger("100"), TZBTC.decimals),
                        FATokenInfo(TZBTC, 1.0, DexterBalance.empty)
                    )
                ),
                Result.success(
                    TokenInfoBundle(
                        USDTZ, TokenBalance(BigInteger("200"), USDTZ.decimals),
                        FATokenInfo(USDTZ, 1.0, DexterBalance.empty)
                    )
                )
            )
        )
        val expectedFees = Tez(0.000_500)
        every { migrateFundsUseCase.calculateFees() } returns Single.just(expectedFees)

        val viewModel = initViewModel()
        viewModel.getConfirmationDetails()

        viewModel.data.value.apply {
            println(this)
            TestCase.assertNotNull(this)
            TestCase.assertTrue(this is DataWrapper.Success)
            val item = this!!.data!!
            Assert.assertTrue(item.error == null)
            Assert.assertTrue(item.fees == expectedFees.format(XTZ.symbol))
            Assert.assertTrue(
                item.toSend == "1 XTZ\n" +
                        "+ 0.000001 tzBTC\n" +
                        "+ 0.0002 USDtz"
            )
        }

        verify {
            useCase.getCurrencyBundle()
            useCase.fallbackBalances()
            migrateFundsUseCase.calculateFees()
        }
    }

    // XTZ Will never fail with a Result, but we test the case nonetheless
    @Test
    fun `getConfirmationDetails XTZ OK + FA12 KO`() {
        every { useCase.getCurrencyBundle() } returns Wallet.defaultCurrency
        every { useCase.fallbackBalances() } returns Single.just(
            listOf(
                Result.success(TokenInfoBundle(XTZ, CoinBalance(Tez(1.0)), XTZTokenInfo(1.0))),
                Result.failure(IOException("FAIL")),
                Result.success(
                    TokenInfoBundle(
                        USDTZ, TokenBalance(BigInteger("200"), USDTZ.decimals),
                        FATokenInfo(USDTZ, 1.0, DexterBalance.empty)
                    )
                )
            )
        )
        val expectedFees = Tez(0.000_500)
        every { migrateFundsUseCase.calculateFees() } returns Single.just(expectedFees)

        val viewModel = initViewModel()
        viewModel.getConfirmationDetails()

        viewModel.data.value.apply {
            println(this)
            TestCase.assertNotNull(this)
            TestCase.assertTrue(this is DataWrapper.Success)
            val item = this!!.data!!
            Assert.assertTrue(item.error is IOException)
            Assert.assertTrue(item.fees == expectedFees.format(XTZ.symbol))
            Assert.assertTrue(
                item.toSend == "1 XTZ\n" +
                        "+ 0.0002 USDtz"
            )
        }

        verify {
            useCase.getCurrencyBundle()
            useCase.fallbackBalances()
            migrateFundsUseCase.calculateFees()
        }
    }

    @Test
    fun `getConfirmationDetails XTZ KO + FA12 KO`() {
        every { useCase.getCurrencyBundle() } returns Wallet.defaultCurrency
        every { useCase.fallbackBalances() } returns Single.just(
            listOf(
                Result.failure(IllegalStateException("MAIN FAIL")),
                Result.failure(IOException("FAIL")),
                Result.success(
                    TokenInfoBundle(
                        USDTZ, TokenBalance(BigInteger("200"), USDTZ.decimals),
                        FATokenInfo(USDTZ, 1.0, DexterBalance.empty)
                    )
                )
            )
        )
        val expectedFees = Tez(0.000_500)
        every { migrateFundsUseCase.calculateFees() } returns Single.just(expectedFees)

        val viewModel = initViewModel()
        viewModel.getConfirmationDetails()

        viewModel.data.value.apply {
            println(this)
            TestCase.assertNotNull(this)
            TestCase.assertTrue(this is DataWrapper.Success)
            val item = this!!.data!!
            // We save the last error
            Assert.assertTrue(item.error is IOException)
            Assert.assertTrue(item.fees == expectedFees.format(XTZ.symbol))
            Assert.assertTrue(item.toSend == "0.0002 USDtz")
        }

        verify {
            useCase.getCurrencyBundle()
            useCase.fallbackBalances()
            migrateFundsUseCase.calculateFees()
        }
    }

    @Test
    fun `getConfirmationDetails XTZ KO`() {
        every { useCase.fallbackBalances() } returns Single.error(IOException("FAIL"))
        val expectedFees = Tez(0.000_500)
        every { migrateFundsUseCase.calculateFees() } returns Single.just(expectedFees)

        val viewModel = initViewModel()
        viewModel.getConfirmationDetails()

        viewModel.data.value.apply {
            TestCase.assertNotNull(this)
            TestCase.assertTrue(this is DataWrapper.Error)
            val item = this!!.error
            Assert.assertTrue(item is IOException)
        }

        verify {
            useCase.fallbackBalances()
            migrateFundsUseCase.calculateFees()
        }
    }

    @Test
    fun `getConfirmationDetails Fees KO`() {
        every { useCase.fallbackBalances() } returns Single.just(
            listOf(
                Result.success(TokenInfoBundle(XTZ, CoinBalance(Tez(1.0)), XTZTokenInfo(1.0)))
            )
        )
        every { migrateFundsUseCase.calculateFees() } returns Single.error(IOException("FAIL"))

        val viewModel = initViewModel()
        viewModel.getConfirmationDetails()

        viewModel.data.value.apply {
            TestCase.assertNotNull(this)
            TestCase.assertTrue(this is DataWrapper.Error)
            val item = this!!.error
            Assert.assertTrue(item is IOException)
        }

        verify {
            useCase.fallbackBalances()
            migrateFundsUseCase.calculateFees()
        }
    }

    @Test
    fun `migrateFunds OK`() {
        val operationHash = "oo1234566"
        every { migrateFundsUseCase.transferFunds(any()) } returns Single.just(operationHash)

        val viewModel = initViewModel()
        viewModel.migrateFunds()

        viewModel.operation.value.apply {
            TestCase.assertNotNull(this)
            TestCase.assertTrue(this is DataWrapper.Success)
            val item = this!!.data!!
            Assert.assertTrue(item == operationHash)
        }

        verify {
            migrateFundsUseCase.transferFunds(Tez.zero)
        }
    }

    @Test
    fun `migrateFunds KO`() {
        every { migrateFundsUseCase.transferFunds(any()) } returns Single.error(IOException("FAIL"))

        val viewModel = initViewModel()
        viewModel.migrateFunds()

        viewModel.operation.value.apply {
            TestCase.assertNotNull(this)
            TestCase.assertTrue(this is DataWrapper.Error)
            val item = this!!.error
            Assert.assertTrue(item is IOException)
        }

        verify {
            migrateFundsUseCase.transferFunds(Tez.zero)
        }
    }
}
