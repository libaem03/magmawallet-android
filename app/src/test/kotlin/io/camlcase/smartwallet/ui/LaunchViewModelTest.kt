package io.camlcase.smartwallet.ui

import io.camlcase.smartwallet.data.model.LoginStep
import io.camlcase.smartwallet.data.model.OnboardingStep
import io.camlcase.smartwallet.data.source.local.AppPreferences
import io.camlcase.smartwallet.data.source.local.UserPreferences
import io.camlcase.smartwallet.model.navigation.LaunchState
import io.camlcase.smartwallet.test.RxTest
import io.mockk.*
import org.junit.After
import org.junit.Assert
import org.junit.Test

/**
 * @see LaunchViewModel
 */
class LaunchViewModelTest : RxTest() {
    private val userPreferences = mockk<UserPreferences>(relaxed = true)
    private val appPreferences = mockk<AppPreferences>(relaxed = true)

    private fun initViewModel(): LaunchViewModel {
        return spyk(LaunchViewModel(userPreferences, appPreferences))
    }

    private fun mockUnloggedState() {
        every { userPreferences.userLoggedIn() } returns true
        every { appPreferences.getOnboardingStep() } returns OnboardingStep.COMPLETE
        every { appPreferences.getBoolean(AppPreferences.AUTH_CANCELLED) } returns true
    }

    @Test
    fun `Test getUserLaunchState UNLOGGED`() {
        mockUnloggedState()
        val viewModel = initViewModel()

        val result = viewModel.getUserLaunchState()

        Assert.assertEquals(LaunchState.UNLOGGED, result)

        verifyUnlogged()
    }

    @Test
    fun `Test getUserLaunchState ACTIVE`() {
        every { userPreferences.userLoggedIn() } returns true
        every { appPreferences.getOnboardingStep() } returns OnboardingStep.COMPLETE
        every { appPreferences.getBoolean(AppPreferences.AUTH_CANCELLED) } returns false
        val viewModel = initViewModel()

        val result = viewModel.getUserLaunchState()

        Assert.assertEquals(LaunchState.ACTIVE, result)

        verify {
            userPreferences.userLoggedIn()
            appPreferences.getOnboardingStep()
            appPreferences.getBoolean(AppPreferences.AUTH_CANCELLED)
        }
    }

    private fun verifyUnlogged() {
        verify {
            userPreferences.userLoggedIn()
            appPreferences.getOnboardingStep()
            appPreferences.getBoolean(AppPreferences.AUTH_CANCELLED)
        }
    }

    @Test
    fun `Test getUserLaunchState FIRST_TIME`() {
        every { userPreferences.userLoggedIn() } returns false
        every { appPreferences.getOnboardingStep() } returns OnboardingStep.CREATE_WALLET
        every { appPreferences.getLoginStep() } returns LoginStep.UNKNOWN
        val viewModel = initViewModel()

        val result = viewModel.getUserLaunchState()

        Assert.assertEquals(LaunchState.FIRST_TIME, result)

        verifyFirstTime()
    }

    private fun verifyFirstTime() {
        verify {
            userPreferences.userLoggedIn()
            appPreferences.getOnboardingStep()
            appPreferences.getLoginStep()
        }
    }

    private fun mockPendingOnboarding() {
        every { userPreferences.userLoggedIn() } returns false
        every { appPreferences.getLoginStep() } returns LoginStep.UNKNOWN
        every { appPreferences.getOnboardingStep() } returns OnboardingStep.SET_PIN
    }

    @Test
    fun `Test getUserLaunchState PENDING_ONBOARDING`() {
        mockPendingOnboarding()
        val viewModel = initViewModel()

        val result = viewModel.getUserLaunchState()

        Assert.assertEquals(LaunchState.PENDING_ONBOARDING, result)

        verifyPendingOnboarding()
    }

    private fun verifyPendingOnboarding() {
        verify {
            userPreferences.userLoggedIn()
            appPreferences.getOnboardingStep()
            appPreferences.getLoginStep()
        }
    }

    @Test
    fun `Test checkUserOnboardingStep UNLOGGED`() {
        mockUnloggedState()
        val viewModel = initViewModel()
        viewModel.checkUserOnboardingStep()

        verify {
            userPreferences.userLoggedIn()
            appPreferences.getOnboardingStep()
            appPreferences.getBoolean(AppPreferences.AUTH_CANCELLED)
            appPreferences.storeOnboardingStep(OnboardingStep.CREATE_WALLET)
        }
    }

    @Test
    fun `Test checkUserOnboardingStep PENDING_ONBOARDING`() {
        mockPendingOnboarding()
        val viewModel = initViewModel()
        viewModel.checkUserOnboardingStep()

        verify {
            userPreferences.userLoggedIn()
            appPreferences.getOnboardingStep()
            appPreferences.getLoginStep()
        }
    }

    @Test
    fun `Test showPairingWarning FIRST_TIME`() {
        every { userPreferences.userLoggedIn() } returns false
        every { appPreferences.getOnboardingStep() } returns OnboardingStep.CREATE_WALLET
        every { appPreferences.getLoginStep() } returns LoginStep.UNKNOWN
        val viewModel = initViewModel()

        val result = viewModel.showPairingWarning()
        Assert.assertFalse(result)

        verifyFirstTime()
    }

    @Test
    fun `Test showPairingWarning UNLOGGED`() {
        mockUnloggedState()
        val viewModel = initViewModel()

        val result = viewModel.showPairingWarning()
        Assert.assertTrue(result)

        verifyUnlogged()
    }

    @Test
    fun `Test showPairingWarning PENDING_ONBOARDING`() {
        mockPendingOnboarding()
        val viewModel = initViewModel()

        val result = viewModel.showPairingWarning()
        Assert.assertFalse(result)

        verifyPendingOnboarding()
    }

    @After
    fun after() {
        confirmVerified(userPreferences)
        confirmVerified(appPreferences)
    }

    @Test
    fun `getUserLaunchState MIGRATION`() {
        every { userPreferences.userLoggedIn() } returns false
        every { appPreferences.getLoginStep() } returns LoginStep.MIGRATION

        val viewModel = initViewModel()
        val result = viewModel.getUserLaunchState()

        Assert.assertEquals(LaunchState.PENDING_LOGIN, result)

        verify {
            userPreferences.userLoggedIn()
            appPreferences.getLoginStep()
        }
    }

    @Test
    fun `activeUser Import+SET_PIN`() {
        every { userPreferences.userLoggedIn() } returns true
        every { appPreferences.getOnboardingStep() } returns OnboardingStep.SET_PIN
        every { appPreferences.getLoginStep() } returns LoginStep.UNKNOWN

        val viewModel = initViewModel()
        val result = viewModel.activeUser()

        Assert.assertFalse(result)

        verify {
            userPreferences.userLoggedIn()
            appPreferences.getOnboardingStep()
        }
    }
}
