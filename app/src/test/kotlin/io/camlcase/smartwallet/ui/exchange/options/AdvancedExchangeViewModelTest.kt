package io.camlcase.smartwallet.ui.exchange.options

import io.camlcase.smartwallet.core.Exchange.defaultSlippages
import io.camlcase.smartwallet.data.core.Exchange
import io.camlcase.smartwallet.data.core.Exchange.DEFAULT_TIMEOUT_MINUTES
import io.camlcase.smartwallet.data.source.local.AppPreferences
import io.mockk.*
import org.junit.After
import org.junit.Assert
import org.junit.Test
import kotlin.math.absoluteValue
import kotlin.random.Random

class AdvancedExchangeViewModelTest {
    private val appPreferences = mockk<AppPreferences>(relaxed = true)
    private fun initViewModel(): AdvancedExchangeViewModel {
        return spyk(AdvancedExchangeViewModel(appPreferences))
    }

    @After
    fun tearDown() {
        confirmVerified(appPreferences)
    }

    @Test
    fun `Test parseSlippage 0-00`() {
        val input = "0.00"
        val viewModel = initViewModel()
        viewModel.validateSlippage(input,
            valid = {
                Assert.assertEquals(0.00, it, 0.0)
            },
            invalid = {
                Assert.fail()
            })
    }

    @Test
    fun `Test parseSlippage 0-01`() {
        val input = "0.01"
        val viewModel = initViewModel()
        viewModel.validateSlippage(input,
            valid = {
                Assert.assertEquals(0.0001, it, 0.0)
            },
            invalid = {
                Assert.fail()
            })
    }

    @Test
    fun `Test parseSlippage 45-00`() {
        val input = "45.00"
        val viewModel = initViewModel()
        viewModel.validateSlippage(input,
            valid = {
                Assert.assertEquals(0.45, it, 0.0)
            },
            invalid = {
                Assert.fail()
            })
    }

    @Test
    fun `Test parseSlippage 2-00`() {
        val input = "2.00"
        val viewModel = initViewModel()
        viewModel.validateSlippage(input,
            valid = {
                Assert.assertEquals(0.02, it, 0.0)
            },
            invalid = {
                Assert.fail()
            })
    }

    @Test
    fun `Test parseSlippage 0-50`() {
        val input = "0.50"
        val viewModel = initViewModel()
        viewModel.validateSlippage(input,
            valid = {
                Assert.assertEquals(0.005, it, 0.0)
            },
            invalid = {
                Assert.fail()
            })
    }

    @Test
    fun `Test parseSlippage 50-00`() {
        val input = "50.00"
        val viewModel = initViewModel()
        viewModel.validateSlippage(input,
            valid = {
                Assert.fail()
            },
            invalid = {
                Assert.assertTrue(true)
            })
    }

    @Test
    fun `Test updateInputSlippage`() {
        every { appPreferences.getDouble(AppPreferences.EXCHANGE_SLIPPAGE) } returns -1.0
        val viewModel = initViewModel()

        viewModel.initInputSlippage(
            onCustom = {
                println(it)
                Assert.fail()
            },
            onDefault = {
                Assert.assertTrue(it == Exchange.DEFAULT_SLIPPAGE)
            })
        verify {
            appPreferences.getDouble(AppPreferences.EXCHANGE_SLIPPAGE)
        }
    }

    @Test
    fun `Test updateInputSlippage 0-00`() {
        val input = 0.00
        every { appPreferences.getDouble(AppPreferences.EXCHANGE_SLIPPAGE) } returns input
        val viewModel = initViewModel()

        viewModel.initInputSlippage(
            onCustom = {
                println(it)
                Assert.assertEquals("0", it)
            },
            {
                println(it)
                Assert.fail()
            })
        verify {
            appPreferences.getDouble(AppPreferences.EXCHANGE_SLIPPAGE)
        }
    }

    @Test
    fun `Test updateInputSlippage 0-01`() {
        val input = 0.01
        every { appPreferences.getDouble(AppPreferences.EXCHANGE_SLIPPAGE) } returns input
        val viewModel = initViewModel()

        viewModel.initInputSlippage(
            onCustom = {
                println(it)
                Assert.fail()
            },
            {
                Assert.assertTrue(it == input)
            })
        verify {
            appPreferences.getDouble(AppPreferences.EXCHANGE_SLIPPAGE)
        }
    }

    @Test
    fun `Test updateInputSlippage 0-50`() {
        val input = 0.50
        every { appPreferences.getDouble(AppPreferences.EXCHANGE_SLIPPAGE) } returns input
        val viewModel = initViewModel()

        viewModel.initInputSlippage(onCustom = {
            println(it)
            Assert.assertEquals("50", it)
        },
            {
                println(it)
                Assert.fail()
            })
        verify {
            appPreferences.getDouble(AppPreferences.EXCHANGE_SLIPPAGE)
        }
    }

    @Test
    fun `Test updateInputSlippage 1-20`() {
        val input = 1.20
        every { appPreferences.getDouble(AppPreferences.EXCHANGE_SLIPPAGE) } returns input
        val viewModel = initViewModel()

        viewModel.initInputSlippage(
            onCustom = {
                println(it)
                Assert.assertEquals("120", it)
            },
            {
                println(it)
                Assert.fail()
            })

        verify {
            appPreferences.getDouble(AppPreferences.EXCHANGE_SLIPPAGE)
        }
    }

    @Test
    fun `validTimeout KO`() {
        val viewModel = initViewModel()

        invalidTimeoutInputs.forEach {
            val result = viewModel.validTimeout(it)
            Assert.assertFalse(result)
        }
    }

    @Test
    fun `validTimeout OK`() {
        val viewModel = initViewModel()

        validTimeoutInputs.forEach {
            val result = viewModel.validTimeout(it)
            Assert.assertTrue(result)
        }
    }

    @Test
    fun `parseTimeout KO`() {
        val viewModel = initViewModel()

        invalidTimeoutInputs.forEach {
            val result = viewModel.parseTimeout(it)
            Assert.assertEquals(Exchange.DEFAULT_TIMEOUT_MINUTES, result)
            verify {
                appPreferences.store(AppPreferences.EXCHANGE_TIMEOUT, DEFAULT_TIMEOUT_MINUTES)
            }
        }
    }

    @Test
    fun `parseTimeout OK`() {
        val viewModel = initViewModel()

        validTimeoutInputs.forEach {
            val result = viewModel.parseTimeout(it)
            Assert.assertEquals(it.toInt(), result)
            verify {
                appPreferences.store(AppPreferences.EXCHANGE_TIMEOUT, result)
            }
        }
    }

    @Test
    fun `parseSlippage OK input`() {
        val viewModel = initViewModel()

        validSlippages.forEach {
            val parseSlippage = viewModel.parseSlippage(it, Random.nextDouble())
            Assert.assertEquals(convertToSlippage(it), parseSlippage, 0.0)
            verify {
                appPreferences.store(AppPreferences.EXCHANGE_SLIPPAGE, parseSlippage)
            }
        }
    }

    @Test
    fun `parseSlippage KO input-selected`() {
        val viewModel = initViewModel()

        invalidSlippages.forEach {
            val parseSlippage = viewModel.parseSlippage(it, 1.0)
            Assert.assertEquals(defaultSlippages[1], parseSlippage, 0.0)
            verify {
                appPreferences.store(AppPreferences.EXCHANGE_SLIPPAGE, parseSlippage)
            }
        }
    }

    @Test
    fun `parseSlippage OK selected`() {
        val viewModel = initViewModel()

        validSelectedSlippages.forEach {
            val parseSlippage = viewModel.parseSlippage("", it)
            Assert.assertEquals(it, parseSlippage, 0.0)
            verify {
                appPreferences.store(AppPreferences.EXCHANGE_SLIPPAGE, parseSlippage)
            }
        }

        defaultSlippages.forEach {
            val parseSlippage = viewModel.parseSlippage("", it)
            Assert.assertEquals(it, parseSlippage, 0.0)
            verify {
                appPreferences.store(AppPreferences.EXCHANGE_SLIPPAGE, parseSlippage)
            }
        }
    }

    @Test
    fun `parseSlippage KO selected`() {
        val viewModel = initViewModel()

        invalidSelectedSlippages.forEach {
            val parseSlippage = viewModel.parseSlippage("", it)
            Assert.assertEquals(Exchange.DEFAULT_SLIPPAGE, parseSlippage, 0.0)
            verify {
                appPreferences.store(AppPreferences.EXCHANGE_SLIPPAGE, parseSlippage)
            }
        }
    }

    private fun convertToSlippage(input: String): Double {
        val convertedInput = input.toDouble().absoluteValue
        return convertedInput / 100
    }

    companion object {
        // decimal values of %
        val invalidSelectedSlippages = listOf(
            0.0, 0.5, 50.0, 12928373734636363.00000272626252525
        )

        // decimal values of %
        val validSelectedSlippages = listOf(
            0.000001, 0.4999, 0.2, 0.1004
        )
        val validSlippages = listOf(
            "0", "1", "0.01", "45", "49.99", "0.5", "2.00", "0.000000001", "-5"
        )

        val invalidSlippages = listOf(
            "50", "51.0", "", "null",
            "Lorem ipsum",
            "✨",
            "\uD83E\uDDDD \uD83E\uDDDD\u200D♂️ \uD83E\uDDDD\u200D♀️"
        )

        val invalidTimeoutInputs = listOf(
            "0",
            "",
            "null",
            "1.0",
            "1.440",
            "1,440",
            "1441",
            "1,441",
            "Lorem ipsum",
            "✨",
            "\uD83E\uDDDD \uD83E\uDDDD\u200D♂️ \uD83E\uDDDD\u200D♀️",
            "-5"
        )

        val validTimeoutInputs = listOf(
            "1",
            "5",
            "1440",
            "10",
            "1399",
        )
    }
}
