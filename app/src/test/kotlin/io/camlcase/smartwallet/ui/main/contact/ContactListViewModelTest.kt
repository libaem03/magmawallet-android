package io.camlcase.smartwallet.ui.main.contact

import io.camlcase.kotlintezos.model.TezosNetwork
import io.camlcase.smartwallet.data.model.contact.ContactTezosAddress
import io.camlcase.smartwallet.data.model.contact.MagmaContact
import io.camlcase.smartwallet.data.usecase.contact.ListContactsUseCase
import io.camlcase.smartwallet.model.DataWrapper
import io.camlcase.smartwallet.test.RxTest
import io.mockk.*
import junit.framework.TestCase
import org.junit.After
import org.junit.Assert
import org.junit.Test

class ContactListViewModelTest : RxTest() {
    private val useCase = mockk<ListContactsUseCase>(relaxed = true)

    private fun initViewModel(): ContactListViewModel {
        return spyk(ContactListViewModel(useCase, TezosNetwork.CARTHAGENET))
    }

    @After
    fun after() {
        confirmVerified(useCase)
    }

    /**
     * - Has no read permissions
     */
    @Test
    fun `getContacts No permissions`() {
        val viewModel = initViewModel()
        viewModel.getContacts(false)

        viewModel.data.value.apply {
            TestCase.assertNotNull(this)
            TestCase.assertTrue(this is DataWrapper.Empty)
        }
    }

    /**
     * - Has read permissions
     * - No contacts with tz1 address
     */
    @Test
    fun `getContacts Empty`() {
        every { useCase.getMagmaContacts() } returns emptyList()

        val viewModel = initViewModel()
        viewModel.getContacts(true)

        viewModel.data.value.apply {
            TestCase.assertNotNull(this)
            TestCase.assertTrue(this is DataWrapper.Empty)
        }

        verify {
            useCase.getMagmaContacts()
        }
    }

    /**
     * - Has read permissions
     * - Error when reading contacts
     */
    @Test
    fun `getContacts Error`() {
        val exception = IllegalStateException("Oops")
        every { useCase.getMagmaContacts() } throws exception

        val viewModel = initViewModel()
        viewModel.getContacts(true)

        viewModel.data.value.apply {
            TestCase.assertNotNull(this)
            TestCase.assertTrue(this is DataWrapper.Error)
            Assert.assertTrue(this!!.error == exception)
        }

        verify {
            useCase.getMagmaContacts()
        }
    }

    /**
     * - Has read permissions
     * - Some contacts with tz1 address
     */
    @Test
    fun `getContacts Some`() {
        val contacts = listOf(
            MagmaContact(0, "Test Name", listOf(ContactTezosAddress("tz1", TezosNetwork.CARTHAGENET))),
            MagmaContact(1, "Test Name 2", listOf(ContactTezosAddress("tz1_2", TezosNetwork.CARTHAGENET))),
            MagmaContact(2, "Test Name 3", listOf(ContactTezosAddress("tz1_3", TezosNetwork.CARTHAGENET)))
        )

        every { useCase.getMagmaContacts() } returns contacts

        val viewModel = initViewModel()
        viewModel.getContacts(true)

        viewModel.data.value.apply {
            TestCase.assertNotNull(this)
            TestCase.assertTrue(this is DataWrapper.Success)

            val list = this!!.data!!
            Assert.assertTrue(list == contacts)
        }

        verify {
            useCase.getMagmaContacts()
        }
    }

    @Test
    fun `createMagmaContact OK`() {
        val viewModel = initViewModel()
        val input = "Anything"
        val result = viewModel.createMagmaContact(input)

        Assert.assertTrue(result.id == 0L)
        Assert.assertTrue(result.name == "")
        Assert.assertTrue(result.address == input)
        Assert.assertTrue(result.tezosAddresses.isNotEmpty())
    }

    @Test
    fun `existsInContacts`() {
        val contacts = listOf(
            MagmaContact(0, "Test Name", listOf(ContactTezosAddress("tz1", TezosNetwork.CARTHAGENET))),
            MagmaContact(1, "Test Name 2", listOf(ContactTezosAddress("tz1_2", TezosNetwork.CARTHAGENET))),
            MagmaContact(2, "Test Name 3", listOf(ContactTezosAddress("tz1_3", TezosNetwork.CARTHAGENET)))
        )

        every { useCase.getMagmaContacts() } returns contacts

        val viewModel = initViewModel()
        viewModel.getContacts(true)

        Assert.assertTrue(viewModel.existsInContacts("tz1") != null)
        Assert.assertTrue(viewModel.existsInContacts("tz1_4") == null)

        verify {
            useCase.getMagmaContacts()
        }
    }
}
