package io.camlcase.smartwallet.ui.onboarding.wallet

import io.camlcase.smartwallet.data.model.AppWallet
import io.camlcase.smartwallet.data.model.error.AppError
import io.camlcase.smartwallet.data.model.error.ErrorType
import io.camlcase.smartwallet.data.source.user.WalletLocalSource
import io.camlcase.smartwallet.data.usecase.analytics.AnalyticEvent
import io.camlcase.smartwallet.data.usecase.analytics.SendAnalyticUseCase
import io.camlcase.smartwallet.data.usecase.analytics.StateEvent
import io.camlcase.smartwallet.data.usecase.user.CreateWalletUseCase
import io.camlcase.smartwallet.model.DataWrapper
import io.camlcase.smartwallet.test.RxTest
import io.camlcase.smartwallet.test.mock.mainTestnetAddress
import io.mockk.*
import junit.framework.TestCase
import org.junit.After
import org.junit.Assert
import org.junit.Test

class CreateWalletViewModelTest : RxTest() {
    private val mnemonicList = listOf(
        "phoenix",
        "tezos",
        "camlcase",
        "magma",
        "dexter",
        "caldera",
        "phoenix",
        "tezos",
        "camlcase",
        "magma",
        "dexter",
        "caldera"
    )

    // Mocked to avoid using native library
    private var wallet = mockk<AppWallet>(relaxed = true) {
        every { mnemonic } returns mnemonicList
        every { address } returns mainTestnetAddress
    }

    private var provider = mockk<CreateWalletUseCase>(relaxed = true)
    private var analytics = mockk<SendAnalyticUseCase>(relaxed = true)
    private var localSource = mockk<WalletLocalSource>(relaxed = true)

    private fun initViewModel(): CreateWalletViewModel {
        return spyk(CreateWalletViewModel(provider, analytics))
    }

    @After
    fun after() {
        confirmVerified(localSource)
        confirmVerified(provider)
        confirmVerified(analytics)
    }

    @Test
    fun testInit() {
        Assert.assertNotNull(wallet)
        Assert.assertNotNull(localSource)
    }

    @Test
    fun `createWallet NoPassphrase OK`() {
        every { provider.createNewWallet(any()) } returns wallet

        val viewModel = initViewModel()
        viewModel.createWallet(null)

        viewModel.data.value.apply {
            Assert.assertNotNull(this)
            TestCase.assertTrue(this is DataWrapper.Success)
            Assert.assertEquals(mainTestnetAddress, this!!.data)
        }

        verify {
            provider.createNewWallet(any())
            analytics.reportEvent(AnalyticEvent.WALLET_CREATED)
        }
    }

    @Test
    fun `createWallet EmptyPassphrase OK`() {
        every { provider.createNewWallet(any()) } returns wallet

        val viewModel = initViewModel()
        viewModel.createWallet("    ")

        viewModel.data.value.apply {
            Assert.assertNotNull(this)
            TestCase.assertTrue(this is DataWrapper.Success)
            Assert.assertEquals(mainTestnetAddress, this!!.data)
        }

        verify {
            provider.createNewWallet(null)
            analytics.reportEvent(AnalyticEvent.WALLET_CREATED)
        }
    }

    @Test
    fun `createWallet UntrimmedPassphrase OK`() {
        every { provider.createNewWallet(any()) } returns wallet

        val viewModel = initViewModel()
        viewModel.createWallet("    hello world     ")

        viewModel.data.value.apply {
            Assert.assertNotNull(this)
            TestCase.assertTrue(this is DataWrapper.Success)
            Assert.assertEquals(mainTestnetAddress, this!!.data)
        }

        verify {
            provider.createNewWallet("hello world")
            analytics.reportEvent(AnalyticEvent.WALLET_CREATED)
            analytics.reportState(StateEvent.ONB_WALLET_PASSPHRASE)
        }
    }

    @Test
    fun `createWallet KO`() {
        every { provider.createNewWallet(any()) } throws AppError(ErrorType.WALLET_CREATE_ERROR)

        val viewModel = initViewModel()
        viewModel.createWallet(null)

        viewModel.data.value.apply {
            Assert.assertNotNull(this)
            TestCase.assertTrue(this is DataWrapper.Error)
            Assert.assertTrue(this!!.error is AppError)
            Assert.assertTrue((this!!.error as AppError).type == ErrorType.WALLET_CREATE_ERROR)
        }

        verify {
            provider.createNewWallet(null)
        }
    }

    @Test
    fun `createWallet Invalid KO`() {
        every { provider.createNewWallet(any()) } returns mockk<AppWallet>(relaxed = true) {
            every { mnemonic } returns listOf("magma", "camlcase")
            every { address } returns mainTestnetAddress
        }

        val viewModel = initViewModel()
        viewModel.createWallet(null)

        viewModel.data.value.apply {
            Assert.assertNotNull(this)
            TestCase.assertTrue(this is DataWrapper.Error)
            Assert.assertTrue(this!!.error is IllegalStateException)
        }

        verify {
            provider.createNewWallet(null)
        }
    }
}
