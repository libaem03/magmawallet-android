package io.camlcase.smartwallet.ui.main.activity

import io.camlcase.kotlintezos.model.OperationResultStatus
import io.camlcase.kotlintezos.model.SmartContractCall
import io.camlcase.kotlintezos.model.Tez
import io.camlcase.kotlintezos.model.TezosNetwork
import io.camlcase.kotlintezos.model.operation.fees.ExtraFees
import io.camlcase.kotlintezos.model.operation.fees.OperationFees
import io.camlcase.kotlintezos.model.tzkt.TzKtDelegation
import io.camlcase.kotlintezos.model.tzkt.TzKtTransaction
import io.camlcase.kotlintezos.model.tzkt.dto.TzKtAlias
import io.camlcase.kotlintezos.smartcontract.michelson.NoneMichelsonParameter
import io.camlcase.smartwallet.core.extension.ellipsize
import io.camlcase.smartwallet.data.core.Tokens
import io.camlcase.smartwallet.data.core.XTZ
import io.camlcase.smartwallet.data.model.amount.CoinBalance
import io.camlcase.smartwallet.data.model.amount.TokenBalance
import io.camlcase.smartwallet.data.model.contact.ContactTezosAddress
import io.camlcase.smartwallet.data.model.contact.MagmaContact
import io.camlcase.smartwallet.data.model.delegate.TezosBakerResponse
import io.camlcase.smartwallet.data.model.operation.DelegateOperation
import io.camlcase.smartwallet.data.model.operation.SwapOperation
import io.camlcase.smartwallet.data.model.operation.TezosOperation
import org.junit.Assert
import org.junit.Test
import java.math.BigDecimal

class OperationForViewParserTest {

    @Test
    fun `parse Undelegation`() {
        val parser = OperationForViewParser(emptyList(), null)

        val prevDelegate = "tz1Ydpp4ibTrGSPn2i6cqQZ6arWieUbcfWVn"
        val parseOperation = parser.parseOperation(
            DelegateOperation(
                null,
                TzKtDelegation(
                    "",
                    296463,
                    "ooAz5xnXhEwJr2xvR8ALRrsH938GGLDdYruyEkMM2GeWGkQJEE5",
                    "2020-12-22T14:32:25Z",
                    "BL1Bvpu6CqXZrBNBKuLBPovnfxK7fRKGmerzjw85KrgvZ9FMB6y",
                    OperationResultStatus.APPLIED,
                    OperationFees.simulationFees,
                    null,
                    source = TzKtAlias(address = "tz1"),
                    previousDelegate = TzKtAlias(address = prevDelegate),
                    newDelegate = null,
                    Tez.zero
                )
            )
        )

        Assert.assertTrue(parseOperation.type == TezosOperation.Type.DELEGATE)
        Assert.assertTrue(parseOperation.destination == "")
        Assert.assertTrue(parseOperation.detail == prevDelegate.ellipsize())
    }

    /**
     * SwapOperation(tokenFrom=Token(name=USDTez, symbol=USDtz, decimals=6, contractAddress=KT1FCMQk44tEP9fm9n5JJEhkSk1TW3XQdaWH, dexterAddress=KT1RfTPvrfAQDGAJ7wB71EtwxLQgjmfz59kE), token=Token(name=Tezos, symbol=XTZ, decimals=6, contractAddress=null, dexterAddress=null), amountFrom=TokenBalance(value=20, decimals=6), amountTo=CoinBalance(value=Tez(integerAmount=6, decimalAmount=246503)),
     *
     * tzKtOperations=[
     * TzKtTransaction(id=676419, level=57718, timestamp=2021-03-04T09:58:17Z, block=BLruJSNZEmZh53y6C5Mh5VowiUwmq5JSy8KqrsmckB83BDQzwzT, hash=opXSHzXzzpvRxRs7qPSrr6Qq4J4jBfTDMGSN3qnUJJwV3ALuJgQ, status=APPLIED, fees=OperationFees(fee=Tez(integerAmount=0, decimalAmount=0), gasLimit=0, storageLimit=0, extraFees=ExtraFees(map={})), errors=null, amount=Tez(integerAmount=6, decimalAmount=246503), sender=TzKtAlias(alias=null, address=KT1RfTPvrfAQDGAJ7wB71EtwxLQgjmfz59kE), destination=TzKtAlias(alias=null, address=tz1YCYVCQqFCtjQAQH5sTkkCkPkdMBQTfEn5), smartContractCall=null),
     * TzKtTransaction(id=676418, level=57718, timestamp=2021-03-04T09:58:17Z, block=BLruJSNZEmZh53y6C5Mh5VowiUwmq5JSy8KqrsmckB83BDQzwzT, hash=opXSHzXzzpvRxRs7qPSrr6Qq4J4jBfTDMGSN3qnUJJwV3ALuJgQ, status=APPLIED, fees=OperationFees(fee=Tez(integerAmount=0, decimalAmount=0), gasLimit=0, storageLimit=0, extraFees=ExtraFees(map={})), errors=null, amount=Tez(integerAmount=0, decimalAmount=0), sender=TzKtAlias(alias=null, address=KT1RfTPvrfAQDGAJ7wB71EtwxLQgjmfz59kE), destination=TzKtAlias(alias=null, address=KT1FCMQk44tEP9fm9n5JJEhkSk1TW3XQdaWH),
     *              smartContractCall=SmartContractCall(entrypoint=transfer, parameters=PairMichelsonParameter(left=BytesMichelsonParameter(value=[0, 0, -119, -57, -80, -5, -76, -43, 93, -68, -67, 91, -128, -55, -29, 30, -36, 73, 36, -69, -125, 82], annotations=null), right=PairMichelsonParameter(left=BytesMichelsonParameter(value=[1, -69, 93, 13, 9, 123, 43, -4, 90, 61, 123, -75, 125, -86, -121, -106, -7, 66, -39, 22, 107, 0], annotations=null), right=BigIntegerMichelsonParameter(value=20000000, annotations=null), annotations=null), annotations=null))),
     * TzKtTransaction(id=676417, level=57718, timestamp=2021-03-04T09:58:17Z, block=BLruJSNZEmZh53y6C5Mh5VowiUwmq5JSy8KqrsmckB83BDQzwzT, hash=opXSHzXzzpvRxRs7qPSrr6Qq4J4jBfTDMGSN3qnUJJwV3ALuJgQ, status=APPLIED, fees=OperationFees(fee=Tez(integerAmount=0, decimalAmount=11406), gasLimit=108371, storageLimit=257, extraFees=ExtraFees(map={})), errors=null, amount=Tez(integerAmount=0, decimalAmount=0), sender=TzKtAlias(alias=null, address=tz1YCYVCQqFCtjQAQH5sTkkCkPkdMBQTfEn5), destination=TzKtAlias(alias=null, address=KT1RfTPvrfAQDGAJ7wB71EtwxLQgjmfz59kE),
     *              smartContractCall=SmartContractCall(entrypoint=tokenToXtz, parameters=PairMichelsonParameter(left=PairMichelsonParameter(left=StringMichelsonParameter(value=tz1YCYVCQqFCtjQAQH5sTkkCkPkdMBQTfEn5, annotations=null), right=StringMichelsonParameter(value=tz1YCYVCQqFCtjQAQH5sTkkCkPkdMBQTfEn5, annotations=null), annotations=null), right=PairMichelsonParameter(left=BigIntegerMichelsonParameter(value=20000000, annotations=null), right=PairMichelsonParameter(left=BigIntegerMichelsonParameter(value=6215270, annotations=null), right=StringMichelsonParameter(value=2021-03-04T10:17:24Z, annotations=null), annotations=null), annotations=null), annotations=null))),
     * TzKtTransaction(id=676416, level=57718, timestamp=2021-03-04T09:58:17Z, block=BLruJSNZEmZh53y6C5Mh5VowiUwmq5JSy8KqrsmckB83BDQzwzT, hash=opXSHzXzzpvRxRs7qPSrr6Qq4J4jBfTDMGSN3qnUJJwV3ALuJgQ, status=APPLIED, fees=OperationFees(fee=Tez(integerAmount=0, decimalAmount=3191), gasLimit=26228, storageLimit=257, extraFees=ExtraFees(map={})), errors=null, amount=Tez(integerAmount=0, decimalAmount=0), sender=TzKtAlias(alias=null, address=tz1YCYVCQqFCtjQAQH5sTkkCkPkdMBQTfEn5), destination=TzKtAlias(alias=null, address=KT1FCMQk44tEP9fm9n5JJEhkSk1TW3XQdaWH),
     *              smartContractCall=SmartContractCall(entrypoint=approve, parameters=PairMichelsonParameter(left=StringMichelsonParameter(value=KT1RfTPvrfAQDGAJ7wB71EtwxLQgjmfz59kE, annotations=null), right=BigIntegerMichelsonParameter(value=20000000, annotations=null), annotations=null)))])
     */
    @Test
    fun `parse tokenToXTZ Exchange`() {
        val tokenToXTZ = SwapOperation(
            Tokens.Testnet.USDTZ,
            XTZ,
            TokenBalance(BigDecimal(20.0), Tokens.Testnet.USDTZ.decimals),
            CoinBalance(Tez(6.246503)),
            listOf(
                TzKtTransaction(
                    id = "676419",
                    level = 57718,
                    timestamp = "2021-03-04T09:58:17Z",
                    block = "BLruJSNZEmZh53y6C5Mh5VowiUwmq5JSy8KqrsmckB83BDQzwzT",
                    hash = "opXSHzXzzpvRxRs7qPSrr6Qq4J4jBfTDMGSN3qnUJJwV3ALuJgQ",
                    status = OperationResultStatus.APPLIED,
                    fees = OperationFees.empty,
                    errors = null,
                    amount = Tez(6.246503),
                    sender = TzKtAlias(alias = null, address = "KT1RfTPvrfAQDGAJ7wB71EtwxLQgjmfz59kE"),
                    destination = TzKtAlias(
                        alias = null,
                        address = "tz1YCYVCQqFCtjQAQH5sTkkCkPkdMBQTfEn5"
                    ),
                    smartContractCall = null,
                    hasInternals = false
                ),
                TzKtTransaction(
                    id = "676418",
                    level = 57718,
                    timestamp = "2021-03-04T09:58:17Z",
                    block = "BLruJSNZEmZh53y6C5Mh5VowiUwmq5JSy8KqrsmckB83BDQzwzT",
                    hash = "opXSHzXzzpvRxRs7qPSrr6Qq4J4jBfTDMGSN3qnUJJwV3ALuJgQ",
                    status = OperationResultStatus.APPLIED,
                    fees = OperationFees.empty,
                    errors = null,
                    amount = Tez.zero,
                    sender = TzKtAlias(alias = null, address = "KT1RfTPvrfAQDGAJ7wB71EtwxLQgjmfz59kE"),
                    destination = TzKtAlias(alias = null, address = "KT1FCMQk44tEP9fm9n5JJEhkSk1TW3XQdaWH"),
                    smartContractCall = SmartContractCall(
                        entrypoint = "transfer",
                        parameters = NoneMichelsonParameter()
                    ),
                    hasInternals = false
                ),
                TzKtTransaction(
                    id = "676417",
                    level = 57718,
                    timestamp = "2021-03-04T09:58:17Z",
                    block = "BLruJSNZEmZh53y6C5Mh5VowiUwmq5JSy8KqrsmckB83BDQzwzT",
                    hash = "opXSHzXzzpvRxRs7qPSrr6Qq4J4jBfTDMGSN3qnUJJwV3ALuJgQ",
                    status = OperationResultStatus.APPLIED,
                    fees = OperationFees(
                        fee = Tez("11406"),
                        gasLimit = 108371,
                        storageLimit = 257,
                        extraFees = ExtraFees()
                    ),
                    errors = null,
                    amount = Tez.zero,
                    sender = TzKtAlias(alias = null, address = "tz1YCYVCQqFCtjQAQH5sTkkCkPkdMBQTfEn5"),
                    destination = TzKtAlias(alias = null, address = "KT1RfTPvrfAQDGAJ7wB71EtwxLQgjmfz59kE"),
                    smartContractCall = SmartContractCall(
                        entrypoint = "tokenToXtz",
                        parameters = NoneMichelsonParameter()
                    ),
                    hasInternals = false
                ),
                TzKtTransaction(
                    id = "676416",
                    level = 57718,
                    timestamp = "2021-03-04T09:58:17Z",
                    block = "BLruJSNZEmZh53y6C5Mh5VowiUwmq5JSy8KqrsmckB83BDQzwzT",
                    hash = "opXSHzXzzpvRxRs7qPSrr6Qq4J4jBfTDMGSN3qnUJJwV3ALuJgQ",
                    status = OperationResultStatus.APPLIED,
                    fees = OperationFees(
                        fee = Tez("3191"),
                        gasLimit = 26228,
                        storageLimit = 257,
                        extraFees = ExtraFees()
                    ),
                    errors = null,
                    amount = Tez.zero,
                    sender = TzKtAlias(alias = null, address = "tz1YCYVCQqFCtjQAQH5sTkkCkPkdMBQTfEn5"),
                    destination = TzKtAlias(alias = null, address = "KT1FCMQk44tEP9fm9n5JJEhkSk1TW3XQdaWH"),
                    smartContractCall = SmartContractCall(entrypoint = "approve", parameters = NoneMichelsonParameter()),
                    hasInternals = false
                )
            )
        )

        val parser = OperationForViewParser(emptyList(), null)
        val parseOperation = parser.parseOperation(tokenToXTZ)
        Assert.assertTrue(parseOperation.type == TezosOperation.Type.SWAP)
        Assert.assertTrue(parseOperation.timestamp == 1614851897000)
        Assert.assertTrue(parseOperation.formattedAmount == "- 20.00 USDtz")
        Assert.assertTrue(parseOperation.formattedAmountExtra == "+ 6.246503 XTZ")
        Assert.assertTrue(parseOperation.formattedFees == "0.014597 XTZ")
        Assert.assertTrue(parseOperation.destination == "KT1RfTPvrfAQDG...fz59kE")
        Assert.assertTrue(parseOperation.source == "tz1YCYVCQqFCtj...QTfEn5")
        Assert.assertTrue(parseOperation.detail == "1 USDtz ↔︎ 0.312325 XTZ")
    }

    /**
     * SwapOperation(tokenFrom=Token(name=Tezos, symbol=XTZ, decimals=6, contractAddress=null, dexterAddress=null), token=Token(name=Wrapped BTC, symbol=tzBTC, decimals=8, contractAddress=KT1CUg39jQF8mV6nTMqZxjUUZFuz1KXowv3K, dexterAddress=KT1BYYLfMjufYwqFtTSYJND7bzKNyK7mjrjM), amountFrom=CoinBalance(value=Tez(integerAmount=1, decimalAmount=0)), amountTo=TokenBalance(value=0.00091571, decimals=8),
     *
     * tzKtOperations=[
     * TzKtTransaction(id=676210, level=57705, timestamp=2021-03-04T09:51:03Z, block=BLNQweKTKVA2AfGd1awXsMC1TdaQVNiBJUzW6y3MS3CEWQ5UU5q, hash=op7DfBJXpuBLoUgYNraef8QFaEjfBqg2LHbXMWcF9LKmyLDMYQq, status=APPLIED, fees=OperationFees(fee=Tez(integerAmount=0, decimalAmount=0), gasLimit=0, storageLimit=0, extraFees=ExtraFees(map={})), errors=null, amount=Tez(integerAmount=0, decimalAmount=0), sender=TzKtAlias(alias=null, address=KT1BYYLfMjufYwqFtTSYJND7bzKNyK7mjrjM), destination=TzKtAlias(alias=null, address=KT1CUg39jQF8mV6nTMqZxjUUZFuz1KXowv3K),
     *          smartContractCall=SmartContractCall(entrypoint=transfer, parameters=PairMichelsonParameter(left=BytesMichelsonParameter(value=[1, 32, 124, -127, 70, -40, -26, -42, -29, -102, -29, 25, -120, -7, -105, -108, 62, -10, -59, 65, 51, 0], annotations=null), right=PairMichelsonParameter(left=BytesMichelsonParameter(value=[0, 0, -119, -57, -80, -5, -76, -43, 93, -68, -67, 91, -128, -55, -29, 30, -36, 73, 36, -69, -125, 82], annotations=null), right=BigIntegerMichelsonParameter(value=91571, annotations=null), annotations=null), annotations=null))),
     * TzKtTransaction(id=676209, level=57705, timestamp=2021-03-04T09:51:03Z, block=BLNQweKTKVA2AfGd1awXsMC1TdaQVNiBJUzW6y3MS3CEWQ5UU5q, hash=op7DfBJXpuBLoUgYNraef8QFaEjfBqg2LHbXMWcF9LKmyLDMYQq, status=APPLIED, fees=OperationFees(fee=Tez(integerAmount=0, decimalAmount=10958), gasLimit=105543, storageLimit=257, extraFees=ExtraFees(map={})), errors=null, amount=Tez(integerAmount=1, decimalAmount=0), sender=TzKtAlias(alias=null, address=tz1YCYVCQqFCtjQAQH5sTkkCkPkdMBQTfEn5), destination=TzKtAlias(alias=null, address=KT1BYYLfMjufYwqFtTSYJND7bzKNyK7mjrjM),
     *          smartContractCall=SmartContractCall(entrypoint=xtzToToken, parameters=PairMichelsonParameter(left=StringMichelsonParameter(value=tz1YCYVCQqFCtjQAQH5sTkkCkPkdMBQTfEn5, annotations=null), right=PairMichelsonParameter(left=BigIntegerMichelsonParameter(value=91113, annotations=null), right=StringMichelsonParameter(value=2021-03-04T10:10:37Z, annotations=null), annotations=null), annotations=null)))])
     */
    @Test
    fun `parse xtzToToken Exchange`() {
        val xtzToToken = SwapOperation(
            XTZ,
            Tokens.Testnet.TZBTC,
            CoinBalance(Tez(1.0)),
            TokenBalance(BigDecimal(0.00091571), Tokens.Testnet.TZBTC.decimals),
            listOf(
                TzKtTransaction(
                    id = "676210",
                    level = 57705,
                    timestamp = "2021-03-04T09:51:03Z",
                    block = "BLNQweKTKVA2AfGd1awXsMC1TdaQVNiBJUzW6y3MS3CEWQ5UU5q",
                    hash = "op7DfBJXpuBLoUgYNraef8QFaEjfBqg2LHbXMWcF9LKmyLDMYQq",
                    status = OperationResultStatus.APPLIED,
                    fees = OperationFees.empty,
                    errors = null,
                    amount = Tez.zero,
                    sender = TzKtAlias(alias = null, address = "KT1BYYLfMjufYwqFtTSYJND7bzKNyK7mjrjM"),
                    destination = TzKtAlias(alias = null, address = "KT1CUg39jQF8mV6nTMqZxjUUZFuz1KXowv3K"),
                    smartContractCall = SmartContractCall(
                        entrypoint = "transfer",
                        parameters = NoneMichelsonParameter()
                    ),
                    hasInternals = false
                ),
                TzKtTransaction(
                    id = "676209",
                    level = 57705,
                    timestamp = "2021-03-04T09:51:03Z",
                    block = "BLNQweKTKVA2AfGd1awXsMC1TdaQVNiBJUzW6y3MS3CEWQ5UU5q",
                    hash = "op7DfBJXpuBLoUgYNraef8QFaEjfBqg2LHbXMWcF9LKmyLDMYQq",
                    status = OperationResultStatus.APPLIED,
                    fees = OperationFees(
                        fee = Tez("10958"),
                        gasLimit = 105543,
                        storageLimit = 257,
                        extraFees = ExtraFees()
                    ),
                    errors = null,
                    amount = Tez(1.0),
                    sender = TzKtAlias(alias = null, address = "tz1YCYVCQqFCtjQAQH5sTkkCkPkdMBQTfEn5"),
                    destination = TzKtAlias(alias = null, address = "KT1BYYLfMjufYwqFtTSYJND7bzKNyK7mjrjM"),
                    smartContractCall = SmartContractCall(
                        entrypoint = "xtzToToken",
                        parameters = NoneMichelsonParameter()
                    ),
                    hasInternals = false
                )
            )
        )
        val parser = OperationForViewParser(emptyList(), null)
        val parseOperation = parser.parseOperation(xtzToToken)
        Assert.assertTrue(parseOperation.type == TezosOperation.Type.SWAP)
        Assert.assertTrue(parseOperation.timestamp == 1614851463000)
        Assert.assertTrue(parseOperation.formattedAmount == "- 1 XTZ")
        Assert.assertTrue(parseOperation.formattedAmountExtra == "+ 0.00091571 tzBTC")
        Assert.assertTrue(parseOperation.formattedFees == "0.010958 XTZ")
        Assert.assertTrue(parseOperation.destination == "KT1BYYLfMjufYw...7mjrjM")
        Assert.assertTrue(parseOperation.source == "tz1YCYVCQqFCtj...QTfEn5")
        Assert.assertTrue(parseOperation.detail == "1 XTZ ↔︎ 0.000916 tzBTC")
    }
    
    @Test
    fun `parse UNDelegation on Contacts`() {
        val delegate = DelegateOperation(
            delegate = null,
            tzKtOperation = TzKtDelegation(
                id = "679561",
                level = 57916,
                hash = "ooBnLzUnWEGHG3c3tQ4n6ZERmCP2pPPoiJCCEm1iaAYGNw157nR",
                timestamp = "2021-03-04T11:42:17Z",
                block = "BLMAssTLdBQPaNqUt4CdHabSf32G7Jc32rKufgMCESRFLJnjvoA",
                status = OperationResultStatus.APPLIED,
                fees = OperationFees(fee = Tez("428"), gasLimit = 1400, storageLimit = 0, extraFees = null),
                errors = null,
                source = TzKtAlias(alias = null, address = "tz1Ydpp4ibTrGSPn2i6cqQZ6arWieUbcfWVn"),
                previousDelegate = TzKtAlias(alias = null, address = "tz1YCYVCQqFCtjQAQH5sTkkCkPkdMBQTfEn5"),
                newDelegate = null,
                delegatedAmount = Tez(979.540861)
            )
        )

        val parser = OperationForViewParser(contacts, supportedDelegates)
        val parseOperation = parser.parseOperation(delegate)
        Assert.assertTrue(parseOperation.type == TezosOperation.Type.DELEGATE)
        Assert.assertTrue(parseOperation.timestamp == 1614858137000)
        Assert.assertTrue(parseOperation.formattedAmount == null)
        Assert.assertTrue(parseOperation.formattedAmountExtra == null)
        Assert.assertTrue(parseOperation.formattedFees == "0.000428 XTZ")
        Assert.assertTrue(parseOperation.destination == "")
        Assert.assertTrue(parseOperation.source == "")
        Assert.assertTrue(parseOperation.detail == contacts[1].name)
    }

    companion object {
        val supportedDelegates = listOf(
            TezosBakerResponse(
                name = "Baking Team",
                address = "tz1aWXP237BLwNHJcCD4b3DutCevhqq2T1Z9",
                fee = 0.02,
                logo = null,
                estimatedRoi = 0.060335,
                serviceHealth = "active",
                payoutTiming = "stable",
                payoutAccuracy = "precise",
                openForDelegation = true,
                minDelegation = 1000.0
            ),
            TezosBakerResponse(
                name = "Citadel.one",
                address = "tz1VpvtSaSxKvykrqajFJTZqCXgoVJ5cKaM1",
                fee = 0.03,
                logo = null,
                estimatedRoi = 0.059388,
                serviceHealth = "active",
                payoutTiming = "stable",
                payoutAccuracy = "precise",
                openForDelegation = true,
                minDelegation = 10.0
            ),
            TezosBakerResponse(
                name = "PosDog",
                address = "tz1PirboZKFVqkfE45hVLpkpXaZtLk3mqC17",
                fee = 0.05,
                logo = null,
                estimatedRoi = 0.05882,
                serviceHealth = "active",
                payoutTiming = "stable",
                payoutAccuracy = "precise",
                openForDelegation = true,
                minDelegation = 1000.0
            ),
            TezosBakerResponse(
                name = "BakeTz",
                address = "tz1cXeGHP8Urj2pQRwpAkCdPGbCdqFUPsQwU",
                fee = 0.05,
                logo = null,
                estimatedRoi = 0.058758,
                serviceHealth = "active",
                payoutTiming = "stable",
                payoutAccuracy = "precise",
                openForDelegation = true,
                minDelegation = 0.0
            ),
            TezosBakerResponse(
                name = "Paradigm Bakery",
                address = "tz1YSzTPwEUpMrRxeTTHNo6RVxfo2TMN633b",
                fee = 0.0499,
                logo = null,
                estimatedRoi = 0.058722,
                serviceHealth = "active",
                payoutTiming = "stable",
                payoutAccuracy = "precise",
                openForDelegation = true,
                minDelegation = 10.0
            ),
            TezosBakerResponse(
                name = "DNAG Delegation",
                address = "tz1R55a2HQbXUAzWKJYE5bJp3UvvawwCm9Pr",
                fee = 0.05,
                logo = null,
                estimatedRoi = 0.058681,
                serviceHealth = "active",
                payoutTiming = "stable",
                payoutAccuracy = "precise",
                openForDelegation = true,
                minDelegation = 10.0
            )
        )

        val contacts = listOf(
            MagmaContact(
                id = 1,
                name = "Magma Contact",
                tezosAddresses = listOf(
                    ContactTezosAddress(
                        address = "tz1WHGdvZ46SFqijoF3neTGanozLgCivpvxh",
                        network = TezosNetwork.FLORENCENET
                    )
                )
            ),
            MagmaContact(
                id = 14,
                name = "Testnet Main Baker",
                tezosAddresses = listOf(
                    ContactTezosAddress(
                        address = "tz1YCYVCQqFCtjQAQH5sTkkCkPkdMBQTfEn5",
                        network = TezosNetwork.FLORENCENET
                    )
                )
            ),
            MagmaContact(
                id = 2,
                name = "Testnet UI All tokens",
                tezosAddresses = listOf(
                    ContactTezosAddress(
                        address = "tz1XFFEs45qUZR7W4y5C4dTFnoh11iUaqEvB",
                        network = TezosNetwork.FLORENCENET
                    )
                )
            ),
            MagmaContact(
                id = 18,
                name = "Testnet UI Empty",
                tezosAddresses = listOf(
                    ContactTezosAddress(
                        address = "tz1ZebN9N7fB5y7CQpSFw3xf1KpaX1auVF7Z",
                        network = TezosNetwork.FLORENCENET
                    )
                )
            )
        )
    }
}
