package io.camlcase.smartwallet.ui.main.token

import io.camlcase.kotlintezos.model.Tez
import io.camlcase.smartwallet.data.core.XTZ
import io.camlcase.smartwallet.data.model.amount.CoinBalance
import io.camlcase.smartwallet.data.model.exchange.TokenInfoBundle
import io.camlcase.smartwallet.data.model.exchange.XTZTokenInfo
import io.camlcase.smartwallet.data.usecase.info.GetBCDBalanceUseCase
import io.camlcase.smartwallet.model.DataWrapper
import io.camlcase.smartwallet.test.RxTest
import io.camlcase.smartwallet.ui.base.ErrorMessageParser
import io.mockk.*
import io.reactivex.rxjava3.core.Single
import junit.framework.TestCase
import org.junit.After
import org.junit.Assert
import org.junit.Test

/**
 * @see TokenListViewModel
 */
class TokenListViewModelTest : RxTest() {
    private val useCase = mockk<GetBCDBalanceUseCase>(relaxed = true)
    private val errorMessageParser = mockk<ErrorMessageParser>(relaxed = true)

    @After
    fun after() {
        confirmVerified(useCase)
    }

    private fun initViewModel(): TokenListViewModel {
        return spyk(TokenListViewModel(useCase, errorMessageParser))
    }

    // TODO Flaky test?
//    @Test
//    fun `Test getTokens 0 Balance XTZ OK`() {
//        every { useCase.getBalances() } returns Single.just(listOf(Result.success(TokenInfoBundle.empty)))
//
//        val viewModel = initViewModel()
//
//        viewModel.getTokens()
//
//        viewModel.balances.value.apply {
//            println("?? $this")
//            TestCase.assertNotNull(this)
//            TestCase.assertTrue(this is DataWrapper.Success)
//            val item = this!!.data!!
//            Assert.assertTrue(item.faTokens.isEmpty())
//        }
//
//        verify {
//            useCase.toString()
//            useCase.getBalances()
//        }
//    }

    @Test
    fun `Test getTokenInfoBundle with no tokens`() {
        val input = XTZ
        val result = initViewModel().getTokenInfoBundle(input)
        Assert.assertNull(result)
    }

    @Test
    fun `fallbackTokens OK`() {
        every { useCase.fallbackBalances() } returns Single.just(
            listOf(
                Result.success(TokenInfoBundle(XTZ, CoinBalance(Tez(1.0)), XTZTokenInfo(1.0)))
            )
        )

        val viewModel = initViewModel()
        viewModel.fallbackTokens()

        viewModel.balances.value.apply {
            println("?? $this")
            TestCase.assertNotNull(this)
            TestCase.assertTrue(this is DataWrapper.Success)
            val item = this!!.data!!
            Assert.assertTrue(item.xtz.exchange!!.contains("1.00"))
            Assert.assertTrue(item.xtz.formattedCurrencyBalance!!.contains("1.00"))
            Assert.assertTrue(item.xtz.balance is CoinBalance)
            Assert.assertTrue(item.faTokens.isEmpty())
        }

        verify {
            useCase.getCurrencyBundle()
            useCase.fallbackBalances()
        }
    }

    @Test
    fun `fallbackTokens KO`() {
        every { useCase.fallbackBalances() } returns Single.error(IllegalStateException("ERROR"))

        val viewModel = initViewModel()
        viewModel.fallbackTokens()

        viewModel.balances.value.apply {
            TestCase.assertNotNull(this)
            TestCase.assertTrue(this is DataWrapper.Error)
            val item = this!!.error
            Assert.assertTrue(item is IllegalStateException)
        }

        verify {
            useCase.getCurrencyBundle()
            useCase.fallbackBalances()
        }
    }
}
