package io.camlcase.smartwallet.ui.money

import org.junit.Test

class BuyTezFragmentTest{

    @Test
    fun testRedirect(){
        val (id, status) = splitThings("https://www.myurl.com/?transactionId=465c2g57-591d-5418-8787-e5d92d2f2f28&transactionStatus=pending")
    }


    fun splitThings(redirectUrl: String): Pair<String, String> {
        val txArgs = redirectUrl.split("transactionId=", "&transactionStatus=")
        if (txArgs.size > 2) {
            val txId = txArgs[1]
            val txStatus = txArgs[2]
            println("id = $txId && status $txStatus")
            return Pair(txId, txStatus)
        }
        return Pair("", "")
    }
}
