package io.camlcase.smartwallet.ui.security.lock

import io.camlcase.smartwallet.data.model.error.ErrorType
import io.camlcase.smartwallet.data.model.error.AppError
import io.camlcase.smartwallet.data.source.local.AppPreferences
import io.camlcase.smartwallet.data.source.user.PinLocalSource
import io.camlcase.smartwallet.test.RxTest
import io.mockk.*
import org.junit.After
import org.junit.Assert
import org.junit.Test

class AuthLoginViewModelTest : RxTest() {
    private val pinLocalSource = mockk<PinLocalSource>()
    private val appPreferences = mockk<AppPreferences>()

    private fun initViewModel(): AuthLoginViewModel {
        return spyk(AuthLoginViewModel(pinLocalSource, appPreferences))
    }

    @After
    fun after() {
        confirmVerified(pinLocalSource)
        confirmVerified(appPreferences)
    }

    @Test
    fun `Test validPin OK`() {
        every { pinLocalSource.comparePin(any()) } returns true
        val result = initViewModel().validPin("")

        Assert.assertTrue(result == AuthLoginViewModel.VALID_PIN)
        verify {
            pinLocalSource.comparePin(any())
        }
    }

    @Test
    fun `Test validPin NO`() {
        every { pinLocalSource.comparePin(any()) } returns false
        val result = initViewModel().validPin("")

        Assert.assertTrue(result == AuthLoginViewModel.INVALID_PIN)
        verify {
            pinLocalSource.comparePin(any())
        }
    }

    @Test
    fun `Test validPin KO`() {
        every { pinLocalSource.comparePin(any()) } throws AppError(ErrorType.PIN_ALGORITHM_ERROR)
        val result = initViewModel().validPin("")

        Assert.assertTrue(result == AuthLoginViewModel.PIN_ERROR)
        verify {
            pinLocalSource.comparePin(any())
        }
    }

    @Test
    fun `Test biometricEnabled`() {
        every { appPreferences.getBoolean(AppPreferences.BIOMETRIC_LOGIN_ENABLED) } returns true

        val result = initViewModel().biometricEnabled()

        Assert.assertTrue(result)
        verify {
            appPreferences.getBoolean(AppPreferences.BIOMETRIC_LOGIN_ENABLED)
        }
    }

    @Test
    fun `Test onValidAuthentication`() {
        every { appPreferences.store(AppPreferences.AUTH_TIMESTAMP, any()) } just Runs
        every { appPreferences.store(AppPreferences.AUTH_CANCELLED, false) } just Runs

        initViewModel().onValidAuthentication()

        verify {
            appPreferences.store(AppPreferences.AUTH_TIMESTAMP, any())
            appPreferences.store(AppPreferences.AUTH_CANCELLED, false)
        }
    }

    @Test
    fun `Test unloggedUser`() {
        every { appPreferences.store(AppPreferences.AUTH_CANCELLED, true) } just Runs

        initViewModel().unloggedUser()

        verify {
            appPreferences.store(AppPreferences.AUTH_CANCELLED, true)
        }
    }
}
