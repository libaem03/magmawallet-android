package io.camlcase.smartwallet.ui.send

import io.camlcase.smartwallet.data.core.Wallet
import io.camlcase.smartwallet.data.core.XTZ
import io.camlcase.smartwallet.data.model.exchange.TokenInfoBundle
import io.camlcase.smartwallet.data.usecase.info.GetBCDBalanceUseCase
import io.camlcase.smartwallet.model.DataWrapper
import io.camlcase.smartwallet.test.RxTest
import io.camlcase.smartwallet.test.getCurrencySymbol
import io.camlcase.smartwallet.ui.exchange.amount.ExchangeAmountViewModelTest
import io.camlcase.smartwallet.ui.exchange.amount.ExchangeAmountViewModelTest.Companion.currentTokenBalance
import io.camlcase.smartwallet.ui.exchange.amount.ExchangeAmountViewModelTest.Companion.xtzBalance
import io.camlcase.smartwallet.ui.send.SendAmountViewModel.Companion.IS_ZERO
import io.mockk.*
import io.reactivex.rxjava3.core.Single
import org.junit.After
import org.junit.Assert
import org.junit.Test
import java.math.BigDecimal

/**
 * @see SendAmountViewModel
 */
class SendAmountViewModelTest : RxTest() {
    private val useCase = mockk<GetBCDBalanceUseCase>(relaxed = true)

    private fun initViewModel(): SendAmountViewModel {
        return spyk(SendAmountViewModel(useCase))
    }

    @After
    fun after() {
        confirmVerified(useCase)
    }

    @Test
    fun `Test getBalanceInfo no token`() {
        sampleXTZBalance { viewModel, wrapper ->
            val result = (wrapper as DataWrapper.Success).data!!
            Assert.assertTrue(result.token == XTZ)
            Assert.assertTrue(result.balance == xtzBalance.balance)
        }
    }

    private fun sampleXTZBalance(action: (SendAmountViewModel, DataWrapper<TokenInfoBundle>?) -> Unit) {
        every { useCase.getTokenInfo(XTZ) } returns Single.just(xtzBalance)
        val viewModel = initViewModel()
        viewModel.getBalanceInfo(null)

        viewModel.balance.value.apply {
            Assert.assertNotNull(this)
            Assert.assertTrue(this is DataWrapper.Success)
            action(viewModel, this)
        }

        verify {
            useCase.getTokenInfo(XTZ)
        }
    }

    @Test
    fun `Test getBalanceInfo FA12 token`() {
        val viewModel = initViewModel()
        viewModel.getBalanceInfo(currentTokenBalance)

        viewModel.balance.value.apply {
            Assert.assertNotNull(this)
            Assert.assertTrue(this is DataWrapper.Success)
            val result = (this as DataWrapper.Success).data!!
            Assert.assertTrue(result.token == ExchangeAmountViewModelTest.currentToken)
            Assert.assertTrue(result.balance == currentTokenBalance.balance)
        }
    }

    @Test
    fun `Test validAmount 0-00`() {
        val input = BigDecimal.ZERO
        val result = initViewModel().validAmount(input)

        Assert.assertTrue(result == IS_ZERO)
    }

    @Test
    fun `Test validAmount 1-00 no balance`() {
        val input = 1.0.toBigDecimal()
        val result = initViewModel().validAmount(input)

        Assert.assertTrue(result == SendAmountViewModel.OVER_BALANCE)
    }

    @Test
    fun `Test validAmount OVER_BALANCE true`() {
        sampleXTZBalance { sendAmountViewModel, dataWrapper ->
            val input = 100.0.toBigDecimal()
            val result = sendAmountViewModel.validAmount(input)

            Assert.assertTrue(result == SendAmountViewModel.OVER_BALANCE)
        }
    }

    @Test
    fun `Test validAmount CORRECT`() {
        sampleXTZBalance { sendAmountViewModel, dataWrapper ->
            val input = 10.0.toBigDecimal()
            val result = sendAmountViewModel.validAmount(input)
            Assert.assertTrue(result == SendAmountViewModel.CORRECT)
        }
    }

    @Test
    fun `Test convertToCurrency no balance`() {
        every { useCase.getCurrencyBundle() } returns Wallet.defaultCurrency
        val input = 1.0.toBigDecimal()
        val result = initViewModel().convertToCurrency(input)
        Assert.assertTrue(result == "${getCurrencySymbol()}1.00")

        verify {
            useCase.getCurrencyBundle()
        }
    }

    @Test
    fun `Test convertToCurrency`() {
        every { useCase.getCurrencyBundle() } returns Wallet.defaultCurrency
        sampleXTZBalance { sendAmountViewModel, dataWrapper ->
            val input = 1.0.toBigDecimal()
            val result = sendAmountViewModel.convertToCurrency(input)
            Assert.assertTrue(result == "${getCurrencySymbol()}1.00")

            verify {
                useCase.getCurrencyBundle()
            }
        }
    }
}
