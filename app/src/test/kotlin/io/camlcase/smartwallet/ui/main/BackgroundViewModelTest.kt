package io.camlcase.smartwallet.ui.main

import io.camlcase.smartwallet.data.source.local.AppPreferences
import io.camlcase.smartwallet.test.RxTest
import io.camlcase.smartwallet.ui.base.navigate.background.BackgroundMode
import io.mockk.confirmVerified
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.After
import org.junit.Assert
import org.junit.Test
import java.util.*

/**
 * @see BackgroundViewModel
 */
class BackgroundViewModelTest : RxTest() {
    private val appPreferences = mockk<AppPreferences>(relaxed = true)

    @Test
    fun `Test showAuthentication TRUE`() {
        every { appPreferences.getLong(AppPreferences.AUTH_TIMESTAMP) } returns 0L

        val result = BackgroundViewModel(appPreferences).showAuthentication(BackgroundMode.COME_FROM_BACKGROUND)
        Assert.assertTrue(result)

        verify {
            appPreferences.getLong(AppPreferences.AUTH_TIMESTAMP)
        }
    }

    @Test
    fun `Test showAuthentication FALSE`() {
        every { appPreferences.getLong(AppPreferences.AUTH_TIMESTAMP) } returns Date().time

        val result = BackgroundViewModel(appPreferences).showAuthentication(BackgroundMode.COME_FROM_BACKGROUND)
        Assert.assertFalse(result)

        verify {
            appPreferences.getLong(AppPreferences.AUTH_TIMESTAMP)
        }
    }

    @After
    fun after() {
        confirmVerified(appPreferences)
    }
}
