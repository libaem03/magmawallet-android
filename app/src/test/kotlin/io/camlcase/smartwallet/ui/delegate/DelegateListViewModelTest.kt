package io.camlcase.smartwallet.ui.delegate

import io.camlcase.smartwallet.core.extension.isMainnet
import io.camlcase.smartwallet.data.model.delegate.TezosBaker
import io.camlcase.smartwallet.data.model.delegate.TezosBakerResponse
import io.camlcase.smartwallet.data.model.error.AppError
import io.camlcase.smartwallet.data.model.error.ErrorType
import io.camlcase.smartwallet.data.source.delegate.BakerLocalSource
import io.camlcase.smartwallet.data.source.local.UserPreferences
import io.camlcase.smartwallet.data.usecase.delegate.DelegateStream
import io.camlcase.smartwallet.data.usecase.delegate.GetDelegateUseCase
import io.camlcase.smartwallet.model.DataWrapper
import io.camlcase.smartwallet.test.RxTest
import io.camlcase.smartwallet.test.blockingObserve
import io.mockk.*
import io.reactivex.rxjava3.core.Single
import junit.framework.TestCase
import org.junit.After
import org.junit.Assert
import org.junit.Test
import java.io.IOException

/**
 * [DelegateListViewModel]
 */
class DelegateListViewModelTest : RxTest() {

    internal val carthageBakers = listOf(
        TezosBakerResponse("Hannibal", "tz1KxFpyYzkcgq5CAok8fNvszrRcD2MUFMXy"),
        TezosBakerResponse("Bomilcar", "tz1YH2LE6p7Sj16vF6irfHX92QV45XAZYHnX"),
        TezosBakerResponse("Himilco", "tz1aWXP237BLwNHJcCD4b3DutCevhqq2T1Z9"),
        TezosBakerResponse("Mago", "tz1RomaiWJV3NFDZWTMVR2aEeHknsn3iF5Gi")
    )

    private val useCase = mockk<GetDelegateUseCase>(relaxed = true)

    private fun initViewModel(): DelegateListViewModel {
        return spyk(DelegateListViewModel(useCase))
    }

    @After
    fun after() {
        confirmVerified(useCase)
    }

    @Test
    fun `getDelegates +No current`() {
        every { useCase.supportedDelegates() } returns Single.just(carthageBakers)
        every { useCase.getDelegate() } returns Single.just(Result.failure(AppError(ErrorType.NO_BAKER)))

        val viewModel = initViewModel()
        viewModel.getDelegates(false)

        val wrapper = viewModel.data.blockingObserve()

        Assert.assertNotNull(wrapper)
        TestCase.assertTrue(wrapper is DataWrapper.Success)
        Assert.assertEquals(carthageBakers.size, (wrapper!!.data as DelegateData).delegates.size)
        Assert.assertEquals(isMainnet(), (wrapper!!.data as DelegateData).isMainnet)

        verifySequence {
            useCase.supportedDelegates()
            useCase.getDelegate()
        }
    }

    @Test
    fun `getDelegates +Supported +No current`() {
        val localSource = mockk<BakerLocalSource>(relaxed = true) {
            every { get() } returns Single.just(ArrayList(carthageBakers))
        }
        val userPreferences = mockk<UserPreferences>() {
            every { getString(UserPreferences.DELEGATE_ADDRESS) } returns null
        }
        val infoUseCase = spyk(
            GetDelegateUseCase(
                mockk(),
                userPreferences,
                mockk(),
                mockk(),
                mockk(),
                localSource,
                listOf(""),
                false,
                DelegateStream()
            )
        )

        val viewModel = DelegateListViewModel(infoUseCase)
        viewModel.getDelegates(false)

        val wrapper = viewModel.data.blockingObserve()

        Assert.assertNotNull(wrapper)
        TestCase.assertTrue(wrapper is DataWrapper.Success)
        Assert.assertEquals(carthageBakers.size, (wrapper!!.data as DelegateData).delegates.size)
        Assert.assertEquals(isMainnet(), (wrapper!!.data as DelegateData).isMainnet)

        verifySequence {
            infoUseCase.supportedDelegates()
            localSource.get()
            infoUseCase.getDelegate()
        }
    }

    @Test
    fun `getDelegates +Supported KO`() {
        every { useCase.supportedDelegates() } returns Single.error(expectedException)
        val viewModel = initViewModel()
        viewModel.getDelegates(false)

        viewModel.data.value.apply {
            Assert.assertNotNull(this)
            TestCase.assertTrue(this is DataWrapper.Error)
            Assert.assertTrue((this as DataWrapper.Error).error == expectedException)
        }

        verify {
            useCase.supportedDelegates()
            useCase.getDelegate()
        }
    }

    @Test
    fun `Test getBaker existing in bakers`() {
        every { useCase.supportedDelegates() } returns Single.just(carthageBakers)
        every { useCase.getDelegate() } returns Single.just(Result.failure(AppError(ErrorType.NO_BAKER)))

        val viewModel = initViewModel()
        viewModel.getDelegates(false)

        val result = viewModel.getBaker(carthageBakers[0].address, carthageBakers[0].name)
        Assert.assertEquals(carthageBakers[0], result)

        verify {
            useCase.supportedDelegates()
            useCase.getDelegate()
        }
    }

    @Test
    fun `getDelegates +current exists in supported`() {
        every { useCase.supportedDelegates() } returns Single.just(carthageBakers)
        every { useCase.getDelegate() } returns Single.just(
            Result.success(
                TezosBaker(
                    carthageBakers[0].name,
                    carthageBakers[0].address
                )
            )
        )

        val viewModel = initViewModel()
        viewModel.getDelegates(false)

        viewModel.data.value.apply {
            Assert.assertNotNull(this)
            TestCase.assertTrue(this is DataWrapper.Success)
            val item = this!!.data!!
            Assert.assertTrue(item.delegates.size == carthageBakers.size)
            Assert.assertTrue(item.delegates[0].current)
            Assert.assertFalse(item.delegates[0].removed)
            Assert.assertTrue(item.delegates[0].removable)
        }

        verify {
            useCase.supportedDelegates()
            useCase.getDelegate()
        }
    }

    @Test
    fun `getDelegates +current exists in supported +Removed`() {
        every { useCase.supportedDelegates() } returns Single.just(carthageBakers)
        every { useCase.getDelegate() } returns Single.just(
            Result.success(
                TezosBaker(
                    carthageBakers[0].name,
                    carthageBakers[0].address
                )
            )
        )

        val viewModel = initViewModel()
        viewModel.getDelegates(true)

        viewModel.data.value.apply {
            Assert.assertNotNull(this)
            TestCase.assertTrue(this is DataWrapper.Success)
            val item = this!!.data!!
            Assert.assertTrue(item.delegates.size == carthageBakers.size)
            Assert.assertTrue(item.delegates[0].current)
            Assert.assertTrue(item.delegates[0].removed)
        }

        verify {
            useCase.supportedDelegates()
            useCase.getDelegate()
        }
    }

    @Test
    fun `getDelegates +current NOT exists in supported`() {
        every { useCase.supportedDelegates() } returns Single.just(carthageBakers)
        val expected = "tz1_DELEGATE"
        every { useCase.getDelegate() } returns Single.just(Result.success(TezosBaker(null, address = expected)))

        val viewModel = initViewModel()
        viewModel.getDelegates(false)

        viewModel.data.value.apply {
            Assert.assertNotNull(this)
            TestCase.assertTrue(this is DataWrapper.Success)
            val item = this!!.data!!
            Assert.assertTrue(item.delegates.size == carthageBakers.size + 1)
            Assert.assertTrue(item.delegates[0].current)
            Assert.assertTrue(item.delegates[0].name == null)
            Assert.assertTrue(item.delegates[0].address == expected)
            Assert.assertFalse(item.delegates[0].removed)
            Assert.assertTrue(item.delegates[0].removable)
        }

        verify {
            useCase.supportedDelegates()
            useCase.getDelegate()
        }
    }

    @Test
    fun `getDelegates +current NOT exists in supported +Removed`() {
        every { useCase.supportedDelegates() } returns Single.just(carthageBakers)
        val expected = "tz1_DELEGATE"
        every { useCase.getDelegate() } returns Single.just(Result.success(TezosBaker(null, address = expected)))

        val viewModel = initViewModel()
        viewModel.getDelegates(true)

        viewModel.data.value.apply {
            Assert.assertNotNull(this)
            TestCase.assertTrue(this is DataWrapper.Success)
            val item = this!!.data!!
            Assert.assertTrue(item.delegates.size == carthageBakers.size + 1)
            Assert.assertTrue(item.delegates[0].current)
            Assert.assertTrue(item.delegates[0].name == null)
            Assert.assertTrue(item.delegates[0].address == expected)
            Assert.assertTrue(item.delegates[0].removed)
            Assert.assertFalse(item.delegates[0].removable)
        }

        verify {
            useCase.supportedDelegates()
            useCase.getDelegate()
        }
    }

    @Test
    fun `Test getBaker existing in bakers with no name`() {
        every { useCase.supportedDelegates() } returns Single.just(carthageBakers)

        val viewModel = initViewModel()
        viewModel.getDelegates(false)

        val result = viewModel.getBaker(carthageBakers[0].address)
        Assert.assertEquals(carthageBakers[0].address, result.address)
        Assert.assertTrue(result.name.isEmpty())

        verify {
            useCase.supportedDelegates()
            useCase.getDelegate()
        }
    }

    @Test
    fun `Test getBaker no existing in bakers`() {
        every { useCase.supportedDelegates() } returns Single.just(carthageBakers)

        val viewModel = initViewModel()
        viewModel.getDelegates(false)

        val result = viewModel.getBaker("tz1BLABLABLA", "BLABLABLA")
        Assert.assertEquals("tz1BLABLABLA", result.address)
        Assert.assertTrue(result.name.isEmpty())

        verify {
            useCase.supportedDelegates()
            useCase.getDelegate()
        }
    }

    companion object {
        private val expectedException = IOException("ERROR")
    }
}
