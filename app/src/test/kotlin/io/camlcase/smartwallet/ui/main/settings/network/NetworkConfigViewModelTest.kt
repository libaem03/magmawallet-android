package io.camlcase.smartwallet.ui.main.settings.network

import io.camlcase.smartwallet.data.model.NetworkProviders
import io.camlcase.smartwallet.data.usecase.info.GetNetworkConfigUseCase
import io.camlcase.smartwallet.model.DataWrapper
import io.camlcase.smartwallet.test.RxTest
import io.mockk.*
import io.reactivex.rxjava3.core.Single
import junit.framework.TestCase
import org.junit.After
import org.junit.Assert
import org.junit.Test

class NetworkConfigViewModelTest : RxTest() {
    private val useCase = mockk<GetNetworkConfigUseCase>(relaxed = true)
    private lateinit var viewModel: NetworkConfigViewModel

    fun init(): NetworkConfigViewModel {
        viewModel = spyk(NetworkConfigViewModel(useCase))
        return viewModel
    }

    @After
    fun tearDown() {
        confirmVerified(useCase)
        confirmVerified(viewModel)
    }

    @Test
    fun `getNetworkConfig OK`() {
        val expected = NetworkProviders(false)
        every { useCase.getNetworkConfig() } returns expected
        val result = init().getNetworkConfig()
        Assert.assertTrue(result == expected)

        // Check internal data
        val validationResult = viewModel.validateUrls(
            expected.nodeUrl,
            expected.verifierNodeUrl,
            expected.tzktUrl,
            expected.bcdUrl,
        )
        // Nothing changed
        Assert.assertFalse(validationResult)

        verify {
            viewModel.getNetworkConfig()
            useCase.getNetworkConfig()

            viewModel.validateUrls(
                expected.nodeUrl,
                expected.verifierNodeUrl,
                expected.tzktUrl,
                expected.bcdUrl,
            )
            viewModel.validUrl(expected.nodeUrl)
            viewModel.validUrl(expected.verifierNodeUrl)
            viewModel.validUrl(expected.tzktUrl)
            viewModel.validUrl(expected.bcdUrl)
        }
    }

    @Test
    fun `getDefaultNetworkConfig OK`() {
        val expected = NetworkProviders(
            "a",
            "b",
            "c",
            "d"
        )
        every { useCase.getDefaultNetworkConfig() } returns expected
        val result = init().getDefaultNetworkConfig()
        Assert.assertTrue(result == expected)

        verify {
            viewModel.getDefaultNetworkConfig()
            useCase.getDefaultNetworkConfig()
        }
    }

    @Test
    fun `changeProviders OK`() {
        every { useCase.setNetworkConfig(any()) } just Runs
        every { useCase.checkUrls(any()) } returns Single.just(true)
        init().changeProviders(
            "a",
            "b",
            "c",
            "d"
        )

        viewModel.operation.value.apply {
            TestCase.assertNotNull(this)
            TestCase.assertTrue(this is DataWrapper.Success)
            val result = this!!.data!!
            Assert.assertTrue(result)

            verify {
                viewModel.changeProviders(
                    "a",
                    "b",
                    "c",
                    "d"
                )
                useCase.checkUrls(any())
                useCase.setNetworkConfig(
                    NetworkProviders(
                        "a",
                        "b",
                        "c",
                        "d"
                    )
                )
                viewModel.disposables
                viewModel.operation
            }
        }
    }

    @Test
    fun `changeProviders SET KO`() {
        every { useCase.checkUrls(any()) } returns Single.just(true)
        every { useCase.setNetworkConfig(any()) } throws IllegalStateException("ERROR")
        init().changeProviders(
            "a",
            "b",
            "c",
            "d"
        )

        viewModel.operation.value.apply {
            TestCase.assertNotNull(this)
            TestCase.assertTrue(this is DataWrapper.Error)
            Assert.assertTrue(this!!.error!! is IllegalStateException)

            verify {
                viewModel.changeProviders(
                    "a",
                    "b",
                    "c",
                    "d"
                )
                useCase.checkUrls(any())
                useCase.setNetworkConfig(
                    NetworkProviders(
                        "a",
                        "b",
                        "c",
                        "d"
                    )
                )
                viewModel.disposables
                viewModel.operation
            }
        }
    }

    @Test
    fun `changeProviders Check KO`() {
        every { useCase.checkUrls(any()) } throws IllegalStateException("ERROR")
        every { useCase.setNetworkConfig(any()) } just Runs
        init().changeProviders(
            "a",
            "b",
            "c",
            "d"
        )

        viewModel.operation.value.apply {
            TestCase.assertNotNull(this)
            TestCase.assertTrue(this is DataWrapper.Error)
            Assert.assertTrue(this!!.error!! is IllegalStateException)

            verify {
                viewModel.changeProviders(
                    "a",
                    "b",
                    "c",
                    "d"
                )
                useCase.checkUrls(any())
                viewModel.disposables
                viewModel.operation
            }
        }
    }
}
