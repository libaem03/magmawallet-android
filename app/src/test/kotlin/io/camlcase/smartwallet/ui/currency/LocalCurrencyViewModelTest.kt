package io.camlcase.smartwallet.ui.currency

import io.camlcase.smartwallet.data.core.extension.toLowerCaseUS
import io.camlcase.smartwallet.data.model.exchange.CoinGeckoRate
import io.camlcase.smartwallet.data.model.exchange.CurrencyBundle
import io.camlcase.smartwallet.data.model.exchange.CurrencyType
import io.camlcase.smartwallet.data.usecase.info.GetCurrencyRatesUseCase
import io.camlcase.smartwallet.data.usecase.info.GetXTZCurrencyExchange
import io.camlcase.smartwallet.model.DataWrapper
import io.camlcase.smartwallet.test.RxTest
import io.mockk.*
import io.reactivex.rxjava3.core.Single
import junit.framework.TestCase
import org.junit.After
import org.junit.Assert
import org.junit.Test
import java.io.IOException

class LocalCurrencyViewModelTest : RxTest() {
    private val useCase = mockk<GetCurrencyRatesUseCase>(relaxed = true)
    private val changeUseCase = mockk<GetXTZCurrencyExchange>(relaxed = true)
    private lateinit var viewModel: LocalCurrencyViewModel

    private fun initViewModel(): LocalCurrencyViewModel {
        return spyk(LocalCurrencyViewModel(useCase, changeUseCase))
    }

    @After
    fun tearDown() {
        confirmVerified(useCase)
        confirmVerified(changeUseCase)
        confirmVerified(viewModel)
    }

    @Test
    fun `getAvailableCurrencies KO`() {
        val expected = IOException("Error")
        every { useCase.getAvailableCurrencies() } returns Single.error(expected)

        viewModel = initViewModel()
        viewModel.getAvailableCurrencies()

        viewModel.data.value.apply {
            println("$this")
            TestCase.assertNotNull(this)
            TestCase.assertTrue(this is DataWrapper.Error)
            val item = this!!.error!!
            Assert.assertTrue(item == expected) // Empty
        }

        verify {
            useCase.getAvailableCurrencies()

            viewModel.getAvailableCurrencies()
            viewModel.disposables
            viewModel.data
        }
    }

    @Test
    fun `getAvailableCurrencies Empty`() {
        val expected = mapOf<String, CoinGeckoRate>()
        every { useCase.getAvailableCurrencies() } returns Single.just(expected)

        viewModel = initViewModel()
        viewModel.getAvailableCurrencies()

        viewModel.data.value.apply {
            println("$this")
            TestCase.assertNotNull(this)
            TestCase.assertTrue(this is DataWrapper.Success)
            val item = this!!.data!!
            Assert.assertTrue(item.size == 2) // Empty
        }

        verify {
            useCase.getAvailableCurrencies()

            viewModel.getAvailableCurrencies()
            viewModel.disposables
            viewModel.data
        }
    }

    @Test
    fun `getAvailableCurrencies OK`() {
        val expected = mapOf<String, CoinGeckoRate>(
            "btc" to CoinGeckoRate("BTC", "BTC", 1.5, CurrencyType.CRYPTO.name.toLowerCaseUS()),
            "eur" to CoinGeckoRate("EUR", "EUR", 1.2, CurrencyType.FIAT.name.toLowerCaseUS()),
            "jpy" to CoinGeckoRate("JPY", "JPY", 1.8, CurrencyType.FIAT.name.toLowerCaseUS())
        )
        every { useCase.getAvailableCurrencies() } returns Single.just(expected)

        viewModel = initViewModel()
        viewModel.getAvailableCurrencies()

        viewModel.data.value.apply {
            println("$this")
            TestCase.assertNotNull(this)
            TestCase.assertTrue(this is DataWrapper.Success)
            val item = this!!.data!!
            Assert.assertTrue(item.size == 8)
            Assert.assertTrue(item[0] is Header) // Popular
            Assert.assertTrue((item[1] as CurrencyForView).name == "EUR")
            Assert.assertTrue((item[2] as CurrencyForView).name == "JPY")
            Assert.assertTrue((item[3] as CurrencyForView).name == "BTC")
            Assert.assertTrue(item[4] is Header) // All, alphabetically
            Assert.assertTrue((item[5] as CurrencyForView).name == "BTC")
            Assert.assertTrue((item[6] as CurrencyForView).name == "EUR")
            Assert.assertTrue((item[7] as CurrencyForView).name == "JPY")
        }

        verify {
            useCase.getAvailableCurrencies()

            viewModel.getAvailableCurrencies()
            viewModel.disposables
            viewModel.data
        }
    }

    @Test
    fun `changeCurrency OK`() {
        val change = CurrencyBundle("EUR", CurrencyType.FIAT, "€")
        every { changeUseCase.updateCurrencyFor(change) } returns Single.just(1.5)
        viewModel = initViewModel()
        viewModel.changeCurrency(change)

        viewModel.operation.value.apply {
            println("$this")
            TestCase.assertNotNull(this)
            TestCase.assertTrue(this is DataWrapper.Success)
            val item = this!!.data!!
            Assert.assertTrue(item == 1.5)
        }

        verify {
            changeUseCase.updateCurrencyFor(change)

            viewModel.changeCurrency(change)
            viewModel.disposables
            viewModel.operation
        }
    }

    @Test
    fun `changeCurrency KO`() {
        val expected = IOException("Error")
        val change = CurrencyBundle("EUR", CurrencyType.FIAT, "€")
        every { changeUseCase.updateCurrencyFor(change) } returns Single.error(expected)
        viewModel = initViewModel()
        viewModel.changeCurrency(change)

        viewModel.operation.value.apply {
            println("$this")
            TestCase.assertNotNull(this)
            TestCase.assertTrue(this is DataWrapper.Error)
            val item = this!!.error!!
            Assert.assertTrue(item == expected)
        }

        verify {
            changeUseCase.updateCurrencyFor(change)

            viewModel.changeCurrency(change)
            viewModel.disposables
            viewModel.operation
        }
    }
}
