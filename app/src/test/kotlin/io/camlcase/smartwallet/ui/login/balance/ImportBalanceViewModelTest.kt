package io.camlcase.smartwallet.ui.login.balance

import io.camlcase.kotlintezos.model.Tez
import io.camlcase.kotlintezos.model.bcd.BCDAccount
import io.camlcase.kotlintezos.model.bcd.BCDAccountToken
import io.camlcase.smartwallet.data.model.error.AppError
import io.camlcase.smartwallet.data.model.error.ErrorType
import io.camlcase.smartwallet.data.usecase.LogOutUseCase
import io.camlcase.smartwallet.data.usecase.info.GetBCDBalanceUseCase
import io.camlcase.smartwallet.data.usecase.user.ChooseWalletUseCase
import io.camlcase.smartwallet.model.DataWrapper
import io.camlcase.smartwallet.test.RxTest
import io.mockk.*
import io.reactivex.rxjava3.core.Single
import org.junit.After
import org.junit.Assert
import org.junit.Test
import java.io.IOException
import java.math.BigInteger
import java.util.*

class ImportBalanceViewModelTest : RxTest() {
    private val useCase = mockk<ChooseWalletUseCase>(relaxed = true)
    private val balanceUseCase = mockk<GetBCDBalanceUseCase>(relaxed = true)
    private val logOutUseCase = mockk<LogOutUseCase>(relaxed = true)
    private lateinit var viewModel: ImportBalanceViewModel

    @After
    fun tearDown() {
        confirmVerified(useCase)
        confirmVerified(balanceUseCase)
        confirmVerified(logOutUseCase)
        confirmVerified(viewModel)
    }

    private fun init(): ImportBalanceViewModel {
        viewModel = spyk(ImportBalanceViewModel(useCase, balanceUseCase, logOutUseCase))
        return viewModel
    }

    @Test
    fun `getBalances NoAddresses NoTokens OK`() {
        every { useCase.getAddressPair() } returns Single.just(Pair("tz1", "tz2"))
        every { balanceUseCase.getBalanceInfo(any()) } returns Single.just(
            BCDAccount(
                "tz1",
                null,
                Tez(10.0),
                Date(),
                emptyList()
            )
        )

        init().getBalances(null, null)
        viewModel.data.value.apply {
            Assert.assertNotNull(this)
            Assert.assertTrue(this is DataWrapper.Success)

            val result = (this as DataWrapper.Success).data!!
            Assert.assertTrue(result.size == 4)
            Assert.assertTrue((result[1] as ImportBalance).address == "tz1")
            Assert.assertTrue((result[1] as ImportBalance).xtzBalance == "10 XTZ")
            Assert.assertTrue((result[1] as ImportBalance).faTokenBalance == false)
            Assert.assertTrue((result[3] as ImportBalance).address == "tz1")
            Assert.assertTrue((result[3] as ImportBalance).xtzBalance == "10 XTZ")
            Assert.assertTrue((result[3] as ImportBalance).faTokenBalance == false)
        }

        verify {
            useCase.getAddressPair()
            balanceUseCase.getBalanceInfo("tz1")
            balanceUseCase.getBalanceInfo("tz2")
            viewModel.getBalances(null, null)
            viewModel.operationDisposables
            viewModel.data
            viewModel.clearOperationSubscription()
        }
    }

    @Test
    fun `getBalances OneAddress Tokens OK`() {
        every { useCase.getAddressPair() } returns Single.just(Pair("tz1", null))
        every { balanceUseCase.getBalanceInfo(any()) } returns Single.just(
            BCDAccount(
                "tz1",
                null,
                Tez(10.0),
                Date(),
                listOf(
                    BCDAccountToken(
                        1,
                        "",
                        BigInteger.TEN,
                        2,
                        "",
                        ""
                    )
                )
            )
        )

        init().getBalances(null, null)
        viewModel.data.value.apply {
            Assert.assertNotNull(this)
            Assert.assertTrue(this is DataWrapper.Success)

            val result = (this as DataWrapper.Success).data!!
            Assert.assertTrue(result.size == 2)
            Assert.assertTrue((result[1] as ImportBalance).address == "tz1")
            Assert.assertTrue((result[1] as ImportBalance).xtzBalance == "10 XTZ")
            Assert.assertTrue((result[1] as ImportBalance).faTokenBalance == true)
        }

        verify {
            useCase.getAddressPair()
            balanceUseCase.getBalanceInfo("tz1")
            viewModel.getBalances(null, null)
            viewModel.operationDisposables
            viewModel.data
            viewModel.clearOperationSubscription()
        }
    }

    @Test
    fun `getBalances KO`() {
        every { useCase.getAddressPair() } returns Single.error(AppError(ErrorType.NO_WALLET))

        init().getBalances(null, null)
        viewModel.data.value.apply {
            Assert.assertNotNull(this)
            Assert.assertTrue(this is DataWrapper.Error)

            val result = this!!.error
            Assert.assertTrue(result is AppError)
            Assert.assertTrue(this.data == null)
        }

        verify {
            useCase.getAddressPair()
            viewModel.getBalances(null, null)
            viewModel.operationDisposables
            viewModel.data
            viewModel.clearOperationSubscription()
            logOutUseCase.logOut()
        }
    }

    @Test
    fun `getBalances Addresses KO`() {
        every { balanceUseCase.getBalanceInfo(any()) } returns Single.error(IOException())

        val hdAddress = "tz1"
        val sdAddress = "tz1_2"
        init().getBalances(hdAddress, sdAddress)
        viewModel.data.value.apply {
            Assert.assertNotNull(this)
            Assert.assertTrue(this is DataWrapper.Error)

            val result = this!!.error
            Assert.assertTrue(result is IOException)

            Assert.assertTrue(data!!.size == 4)
            Assert.assertTrue((data!![1] as ImportBalance).address == hdAddress)
            Assert.assertTrue((data!![1] as ImportBalance).xtzBalance == null)
            Assert.assertTrue((data!![1] as ImportBalance).faTokenBalance == null)
            Assert.assertTrue((data!![3] as ImportBalance).address == sdAddress)
            Assert.assertTrue((data!![3] as ImportBalance).xtzBalance == null)
            Assert.assertTrue((data!![3] as ImportBalance).faTokenBalance == null)
        }

        verify {
            viewModel.getBalances(hdAddress, sdAddress)
            balanceUseCase.getBalanceInfo(hdAddress)
            balanceUseCase.getBalanceInfo(sdAddress)
            viewModel.operationDisposables
            viewModel.data
            viewModel.clearOperationSubscription()
        }
    }
}
