package io.camlcase.smartwallet.ui.exchange.confirmation

import io.camlcase.kotlintezos.model.Tez
import io.camlcase.kotlintezos.model.operation.SmartContractCallOperation
import io.camlcase.kotlintezos.model.operation.fees.OperationFees
import io.camlcase.smartwallet.data.core.Wallet
import io.camlcase.smartwallet.data.core.XTZ
import io.camlcase.smartwallet.data.core.extension.format
import io.camlcase.smartwallet.data.model.amount.Balance
import io.camlcase.smartwallet.data.model.amount.CoinBalance
import io.camlcase.smartwallet.data.model.amount.EmptyBalance
import io.camlcase.smartwallet.data.model.amount.TokenBalance
import io.camlcase.smartwallet.data.model.error.AppError
import io.camlcase.smartwallet.data.model.swap.SwapRequest
import io.camlcase.smartwallet.data.usecase.OperationBundle
import io.camlcase.smartwallet.data.usecase.info.GetBCDBalanceUseCase
import io.camlcase.smartwallet.data.usecase.operation.DexterCalculationsUseCase
import io.camlcase.smartwallet.data.usecase.operation.ExchangeTokensUseCase
import io.camlcase.smartwallet.model.DataWrapper
import io.camlcase.smartwallet.test.RxTest
import io.camlcase.smartwallet.test.mock.TEST_TOKEN
import io.camlcase.smartwallet.test.mock.tokenToXtzDexterExchange
import io.camlcase.smartwallet.test.mock.xtzToTokenDexterExchange
import io.camlcase.smartwallet.ui.exchange.amount.ExchangeAmountViewModelTest.Companion.currentToken
import io.camlcase.smartwallet.ui.exchange.amount.ExchangeAmountViewModelTest.Companion.currentTokenBalance
import io.camlcase.smartwallet.ui.exchange.amount.ExchangeAmountViewModelTest.Companion.tzgBalance
import io.camlcase.smartwallet.ui.exchange.amount.ExchangeAmountViewModelTest.Companion.xtzBalance
import io.mockk.*
import io.reactivex.rxjava3.core.Single
import junit.framework.TestCase
import org.junit.After
import org.junit.Assert
import org.junit.Test
import java.io.IOException
import java.math.BigDecimal
import java.math.BigInteger

/**
 * @see SwapConfirmationViewModel
 */
class SwapConfirmationViewModelTest : RxTest() {
    private val useCase = mockk<ExchangeTokensUseCase>(relaxed = true)
    private val tokensUseCase = mockk<GetBCDBalanceUseCase>(relaxed = true)
    private val calculationsUseCase = mockk<DexterCalculationsUseCase>(relaxed = true)

    private fun initViewModel(): SwapConfirmationViewModel {
        return spyk(SwapConfirmationViewModel(useCase, tokensUseCase, calculationsUseCase))
    }

    @Test
    fun `getConfirmationDetails xtzToToken OK`() {
        sampleXtzToTokenCall(Tez(5.0)) { viewModel, wrapper ->
            val result = wrapper!!.data!!

            Assert.assertTrue(result.formattedFromValue == operations[0].amount.format(null) + " XTZ")
            Assert.assertEquals("0.10 ${currentToken.symbol}", result.formattedToValue)
            Assert.assertEquals("1 XTZ = 1 ${currentToken.symbol}", result.exchangeRate)
            Assert.assertEquals("0 XTZ", result.formattedLiquidityFee)
            Assert.assertEquals("0%", result.priceImpact)
        }
    }

    private fun sampleXtzToTokenCall(
        xtzAmount: Tez,
        action: (SwapConfirmationViewModel, DataWrapper<SwapConfirmationForView>?) -> Unit
    ) {
        val amountFrom = CoinBalance(xtzAmount)
        val request = SwapRequest(
            XTZ,
            currentToken,
            amountFrom,
            TokenBalance(BigInteger.ONE, 0),
            0.4,
            BigDecimal.ZERO
        )
        every { tokensUseCase.getCurrencyBundle() } returns Wallet.defaultCurrency
        every { useCase.createSwapOperation(any()) } returns Single.just(bundle)

        every { tokensUseCase.getBalances() } returns Single.just(
            listOf(
                Result.success(xtzBalance),
                Result.success(currentTokenBalance)
            )
        )
//        every { tokensUseCase.getTokenInfo(XTZ) } returns Single.just(xtzBalance)
//        every { tokensUseCase.getTokenInfo(currentToken) } returns Single.just(currentTokenBalance)

        every { calculationsUseCase.calculate(any(), any(), any<Balance<*>>()) } returns Single.just(
            xtzToTokenDexterExchange
        )

        val viewModel = initViewModel()

        viewModel.getConfirmationDetails(request)

        viewModel.data.value.apply {
            Assert.assertNotNull(this)
            Assert.assertTrue(this is DataWrapper.Success)
            action(viewModel, this)
        }

        verify {
            tokensUseCase.getCurrencyBundle()
            tokensUseCase.getBalances()
            useCase.createSwapOperation(any())
            viewModel.clearOperationSubscription()
            calculationsUseCase.calculate(any(), any(), any<Balance<*>>())
        }
    }

    @Test
    fun `Test getConfirmationDetails xtzToToken Balance KO`() {
        val amountFrom = CoinBalance(Tez(5.0))
        val request = SwapRequest(
            XTZ,
            TEST_TOKEN,
            amountFrom,
            TokenBalance(BigInteger.ONE, 0),
            1.0,
            BigDecimal.ZERO
        )
        every { useCase.createSwapOperation(any()) } returns Single.just(bundle)

        every { tokensUseCase.getCurrencyBundle() } returns Wallet.defaultCurrency
        every { tokensUseCase.getBalances() } returns Single.just(
            listOf(
                Result.success(xtzBalance),
                Result.failure(exception)
            )
        )
        val viewModel = initViewModel()

        viewModel.getConfirmationDetails(request)

        viewModel.data.value.apply {
            println(this)
            Assert.assertNotNull(this)
            Assert.assertTrue(this is DataWrapper.Error)
            Assert.assertTrue(this!!.error!! is AppError)
            Assert.assertTrue((this!!.error!! as AppError).exception == exception)
            Assert.assertNull(this!!.data)
        }

        verify {
            tokensUseCase.getCurrencyBundle()
            tokensUseCase.getBalances()
            viewModel.clearOperationSubscription()
        }
    }

    @Test
    fun `getConfirmationDetails xtzToToken Fees KO`() {
        val amountFrom = CoinBalance(Tez(5.0))
        val request = SwapRequest(
            XTZ,
            currentToken,
            amountFrom,
            TokenBalance(BigInteger.ONE, 0),
            1.0,
            BigDecimal.ZERO
        )
        every { useCase.createSwapOperation(any()) } returns Single.error(exception)
        every { tokensUseCase.getCurrencyBundle() } returns Wallet.defaultCurrency
        every { tokensUseCase.getBalances() } returns Single.just(
            listOf(
                Result.success(xtzBalance),
                Result.success(currentTokenBalance)
            )
        )

        val viewModel = initViewModel()

        viewModel.getConfirmationDetails(request)

        viewModel.data.value.apply {
            Assert.assertNotNull(this)
            TestCase.assertTrue(this is DataWrapper.Error)
            val result = this!!.data
//            println(result)
//            Assert.assertTrue(result.formattedFromValue == amountFrom.formattedValue + " XTZ")
//            Assert.assertEquals("0.000000000000000047 ${currentToken.symbol}", this.data!!.formattedToValue)
//            Assert.assertEquals("1 XTZ = 0 ${currentToken.symbol}", this.data!!.exchangeRate)
//            Assert.assertEquals("0.015 XTZ", this.data!!.formattedLiquidityFee)
//            Assert.assertEquals("100%", this.data!!.slippage)
            Assert.assertNull(result)
        }

        verify {
            tokensUseCase.getCurrencyBundle()
            tokensUseCase.getBalances()
            useCase.createSwapOperation(any())
            viewModel.clearOperationSubscription()
        }
    }

    @Test
    fun `Test xtzToToken swap OK`() {
        val hash = "ooo000"
        every { useCase.createSwapOperation(any()) } returns Single.just(bundle)
        every { useCase.swap(bundle.operations) } returns Single.just(hash)

        val viewModel = initViewModel()
        viewModel.swap()

        viewModel.operation.value.apply {
            Assert.assertNotNull(this)
            Assert.assertTrue(this is DataWrapper.Success)
            Assert.assertTrue(this!!.data == hash)

        }

        verify {
            useCase.createSwapOperation(any())
            useCase.swap(any())
            viewModel.clearOperationSubscription()
        }
    }

    @Test
    fun `Test getTokens defaults`() {
        val result = initViewModel().getTokens()
        Assert.assertTrue(result.first == "XTZ")
        Assert.assertTrue(result.second == "XTZ")
    }

    @Test
    fun `Test getConfirmationDetails tokenToXtz OK`() {
        val amountFrom = TokenBalance(BigInteger.TEN, 0)
        sampleTokenToXtzCall(amountFrom) { viewModel, wrapper ->
            val result = wrapper!!.data!!
            Assert.assertTrue(result.formattedFromValue == amountFrom.formattedValue + " ${TEST_TOKEN.symbol}")
        }
    }

    private fun sampleTokenToXtzCall(
        tokenAmount: TokenBalance,
        action: (SwapConfirmationViewModel, DataWrapper<SwapConfirmationForView>?) -> Unit
    ) {
        val request = SwapRequest(
            TEST_TOKEN,
            XTZ,
            tokenAmount,
            EmptyBalance(),
            1.0,
            BigDecimal.ZERO
        )
        every { useCase.createSwapOperation(any()) } returns Single.just(
            OperationBundle(
                operations,
                OperationFees(Tez(0.00567), 0, 0)
            )
        )
        every { tokensUseCase.getCurrencyBundle() } returns Wallet.defaultCurrency
        every { tokensUseCase.getBalances() } returns Single.just(
            listOf(
                Result.success(xtzBalance),
                Result.success(tzgBalance)
            )
        )
        every { calculationsUseCase.calculate(any(), any(), any<Balance<*>>()) } returns Single.just(
            tokenToXtzDexterExchange
        )

        val viewModel = initViewModel()

        viewModel.getConfirmationDetails(request)

        viewModel.data.value.apply {
            Assert.assertNotNull(this)
            TestCase.assertTrue(this is DataWrapper.Success)
            action(viewModel, this)
        }

        verify {
            tokensUseCase.getCurrencyBundle()
            tokensUseCase.getBalances()
            useCase.createSwapOperation(any())
            viewModel.clearOperationSubscription()
            calculationsUseCase.calculate(any(), any(), any<Balance<*>>())
        }
    }

    @Test
    fun `Test showHelpOption xtzToToken with amount less balance`() {
        sampleXtzToTokenCall(Tez(5.0)) { viewModel, wrapper ->
            val showHelpOption = viewModel.showHelpOption()
            Assert.assertFalse(showHelpOption)
        }
    }

    @Test
    fun `Test showHelpOption xtzToToken with amount eq balance`() {
        sampleXtzToTokenCall(xtzBalance.balance.value as Tez) { viewModel, wrapper ->
            val showHelpOption = viewModel.showHelpOption()
            Assert.assertTrue(showHelpOption)
        }
    }

    @Test
    fun `Test showHelpOption tokenToXtz`() {
        val amountFrom = TokenBalance(BigInteger.TEN, 0)
        sampleTokenToXtzCall(amountFrom) { viewModel, wrapper ->
            val showHelpOption = viewModel.showHelpOption()
            Assert.assertFalse(showHelpOption)
        }
    }

    @After
    fun after() {
        confirmVerified(useCase)
        confirmVerified(tokensUseCase)
        confirmVerified(calculationsUseCase)
    }

    companion object {
        val exception = IOException("ERROR")

        private const val TO = "tz1Address2"
        val operations = listOf(
            SmartContractCallOperation(Tez(1.340), TO, TO, OperationFees.simulationFees)
        )
        val bundle = OperationBundle(
            operations,
            OperationFees.empty
        )
    }
}
