package io.camlcase.smartwallet.ui.main.token

import io.camlcase.smartwallet.data.usecase.delegate.GetDelegateUseCase
import io.camlcase.smartwallet.data.usecase.info.GetCurrencyRatesUseCase
import io.camlcase.smartwallet.data.usecase.money.GetMoonPayDataUseCase
import io.camlcase.smartwallet.test.RxTest
import io.mockk.*
import io.reactivex.rxjava3.core.Single
import org.junit.After
import org.junit.Assert
import org.junit.Test
import java.io.IOException

class SideDataViewModelTest : RxTest() {
    private val delegateUseCase = mockk<GetDelegateUseCase>(relaxed = true)
    private val currencyExchangeUseCase = mockk<GetCurrencyRatesUseCase>(relaxed = true)
    private val moonPayDataUseCase = mockk<GetMoonPayDataUseCase>(relaxed = true)

    private fun init(): SideDataViewModel {
        return spyk(SideDataViewModel(delegateUseCase, currencyExchangeUseCase, moonPayDataUseCase))
    }

    @After
    fun tearDown() {
        confirmVerified(delegateUseCase)
        confirmVerified(currencyExchangeUseCase)
        confirmVerified(moonPayDataUseCase)
    }

    @Test
    fun `getData with updateDelegates false`() {
        every { currencyExchangeUseCase.updateAvailableCurrencies() } returns Single.just(true)
        every { delegateUseCase.updateDelegates() } returns Single.just(false)
        every { delegateUseCase.fetchDelegate() } returns Single.error(NullPointerException())
        every { moonPayDataUseCase.updateMoonPayXTZData() } returns Single.just(false)

        call {
            // Never called
            Assert.assertNull(it)
        }
    }

    private fun call(bakerAction: (Boolean?) -> Unit) {
        val viewModel = init()
        viewModel.getData()

        viewModel.bakersUpdate.value.apply {
            bakerAction(this)
        }

        verify {
            currencyExchangeUseCase.updateAvailableCurrencies()
            delegateUseCase.updateDelegates()
            delegateUseCase.fetchDelegate()
            moonPayDataUseCase.updateMoonPayXTZData()
        }
    }

    @Test
    fun `getData with updateDelegates true`() {
        every { currencyExchangeUseCase.updateAvailableCurrencies() } returns Single.just(true)
        every { delegateUseCase.updateDelegates() } returns Single.just(true)
        every { delegateUseCase.fetchDelegate() } returns Single.error(NullPointerException())
        every { moonPayDataUseCase.updateMoonPayXTZData() } returns Single.just(false)

        call {
            Assert.assertTrue(it!!)
        }
    }

    @Test
    fun `getData with KOs `() {
        every { currencyExchangeUseCase.updateAvailableCurrencies() } returns Single.error(IOException())
        every { delegateUseCase.updateDelegates() } returns Single.error(IOException())
        every { delegateUseCase.fetchDelegate() } returns Single.error(NullPointerException())
        every { moonPayDataUseCase.updateMoonPayXTZData() } returns Single.just(false)

        call {
            // Never called
            Assert.assertNull(it)
        }
    }
}
