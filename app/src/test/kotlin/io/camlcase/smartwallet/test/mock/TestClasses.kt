/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.test.mock

import io.camlcase.kotlintezos.model.Tez
import io.camlcase.kotlintezos.wallet.HDWallet
import io.camlcase.smartwallet.data.model.AppWallet
import io.camlcase.smartwallet.data.model.Token
import io.camlcase.smartwallet.data.model.amount.CoinBalance
import io.camlcase.smartwallet.data.model.amount.TokenBalance
import io.camlcase.smartwallet.data.model.exchange.DexterExchange
import io.mockk.every
import io.mockk.mockk
import java.math.BigDecimal
import java.math.BigInteger

val TEST_TOKEN by lazy {
    Token(
        "TestToken",
        "TTz",
        0,
        contractAddress = "KT1N8A78V9fSiyGwqBpAU2ZQ6S446C7ZwRoD",
        dexterAddress = "KT1QwuYp1g9uvii5CPXivNQd4RC7VGvLPS4e"
    )
}

/**
 * Faucet address to send everything to.
 * Should also be registered as baker
 */
const val mainTestnetAddress = "tz1YCYVCQqFCtjQAQH5sTkkCkPkdMBQTfEn5"

/**
 * Testnet address that is used for regular testing
 */
const val secondaryTestnetAddress = "tz1becfeFKJy1ZM9SdH2Hq8NM6PXgFm5TFTt"

val xtzToTokenDexterExchange = DexterExchange(
    BigDecimal.ONE,
    BigDecimal.TEN,
    BigDecimal.ZERO,
    BigDecimal.ONE,
    CoinBalance(Tez("1")),
    TokenBalance(BigInteger.TEN, 2)
)

val tokenToXtzDexterExchange = DexterExchange(
    BigDecimal.TEN,
    BigDecimal.ONE,
    BigDecimal.ZERO,
    BigDecimal.ONE,
    TokenBalance(BigInteger.TEN, 2),
    CoinBalance(Tez("1")),
)

val mnemonicList = listOf(
    "phoenix",
    "tezos",
    "camlcase",
    "magma",
    "dexter",
    "caldera",
    "phoenix",
    "tezos",
    "camlcase",
    "magma",
    "dexter",
    "caldera"
)

// Mocked to avoid using native library
val testWallet = mockk<AppWallet>(relaxed = true) {
    every { mnemonic } returns mnemonicList
    every { address } returns mainTestnetAddress
    every { wallet } returns mockk<HDWallet>(relaxed = true) {
        every { mnemonic } returns mnemonicList
        every { linearAddress } returns mainTestnetAddress
    }
}
