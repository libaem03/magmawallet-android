package io.camlcase.smartwallet.core.extension

import org.junit.Assert
import org.junit.Test
import java.math.BigDecimal

class FormatExtKtTest {

    @Test
    fun `Test format exchange with thousand`() {
        val input = BigDecimal("1200")
        val result = input.formatExchangeRate()
        Assert.assertEquals("1,200", result)
    }

    @Test
    fun `Test format exchange with thousands`() {
        val input = BigDecimal("1200100")
        val result = input.formatExchangeRate()
        Assert.assertEquals("1,200,100", result)
    }

    @Test
    fun `Test format exchange with thousands and 2 decimals`() {
        val input = BigDecimal("1200.0100")
        val result = input.formatExchangeRate()
        Assert.assertEquals("1,200.01", result)
    }

    @Test
    fun `Test format exchange with thousands and 7 decimals`() {
        val input = BigDecimal("1200.0100123")
        val result = input.formatExchangeRate()
        Assert.assertEquals("1,200.010012", result)
    }


    /**
     * TRUNCATE EXCHANGE
     */

    @Test
    fun `Test formatAndTruncate 0-00`() {
        val input = 0.00
        val result = input.formatAndTruncate()
        Assert.assertEquals("0.00", result)
    }

    @Test
    fun `Test formatAndTruncate 0-2345`() {
        val input = 0.2345
        val result = input.formatAndTruncate()
        Assert.assertEquals("0.23", result)
    }

    @Test
    fun `Test formatAndTruncate 0-001`() {
        val input = 0.001
        val result = input.formatAndTruncate()
        Assert.assertEquals("0.001", result)
    }

    @Test
    fun `Test formatAndTruncate 0-000_1`() {
        val input = 0.000_1
        val result = input.formatAndTruncate()
        Assert.assertEquals("0.0001", result)
    }

    @Test
    fun `Test formatAndTruncate 0-000_01`() {
        val input = 0.000_01
        val result = input.formatAndTruncate()
        Assert.assertEquals("0.00001", result)
    }

    @Test
    fun `Test formatAndTruncate 0-000_001`() {
        val input = 0.000_001
        val result = input.formatAndTruncate()
        Assert.assertEquals("0.000001", result)
    }

    @Test
    fun `Test formatAndTruncate 0-000_000_1`() {
        val input = 0.000_000_1
        val result = input.formatAndTruncate()
        Assert.assertEquals("0.00", result)
    }

    @Test
    fun `Test formatAndTruncate 1-000_000_1`() {
        val input = 1.000_000_1
        val result = input.formatAndTruncate()
        Assert.assertEquals("1.00", result)
    }

    @Test
    fun `Test formatAndTruncate 1_999-000_000_1`() {
        val input = 1_999.000_000_1
        val result = input.formatAndTruncate()
        Assert.assertEquals("1,999.00", result)
    }
}
