/**
 * # Released under MIT License
 *
 * Copyright (c) 2021 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.usecase.info

import io.camlcase.kotlintezos.BCDClient
import io.camlcase.kotlintezos.ClientWrapper
import io.camlcase.kotlintezos.TzKtClient
import io.camlcase.kotlintezos.model.TezosNetwork
import io.camlcase.kotlintezos.model.bcd.BCDAccount
import io.camlcase.kotlintezos.model.tzkt.TzKtAccount
import io.camlcase.kotlintezos.network.DefaultNetworkClient
import io.camlcase.smartwallet.data.core.EmitterSource
import io.camlcase.smartwallet.data.core.Logger
import io.camlcase.smartwallet.data.core.RxEmitter
import io.camlcase.smartwallet.data.model.NetworkProviders
import io.camlcase.smartwallet.data.source.local.AppPreferences
import io.camlcase.smartwallet.data.source.local.UserPreferences
import io.camlcase.smartwallet.data.tezos.TezosNodeClientFactory
import io.reactivex.rxjava3.core.Single
import okhttp3.OkHttpClient
import javax.inject.Inject

class GetNetworkConfigUseCase @Inject constructor(
    private val appPreferences: AppPreferences,
    private val userPreferences: UserPreferences,
    private val isMainnet: Boolean,
    private val clientWrapper: ClientWrapper<OkHttpClient>,
    private val emitter: RxEmitter,
    private val tezosNetwork: TezosNetwork
) {
    fun getNetworkConfig(): NetworkProviders {
        return appPreferences.getNetworkConfig() ?: NetworkProviders(isMainnet)
    }

    fun getDefaultNetworkConfig(): NetworkProviders {
        return NetworkProviders(isMainnet)
    }

    fun setNetworkConfig(providers: NetworkProviders) {
        appPreferences.storeNetworkConfig(providers)
    }

    /**
     * Make a sanity check to the urls provided
     */
    fun checkUrls(providers: NetworkProviders): Single<Boolean> {
        val saved = getNetworkConfig()
        return checkTezosNodeUrl(saved.nodeUrl, providers.nodeUrl, providers.verifierNodeUrl)
            .flatMap { checkTezosNodeUrl(saved.verifierNodeUrl, providers.verifierNodeUrl, providers.nodeUrl) }
            .flatMap { checkTzKtUrl(saved.tzktUrl, providers.tzktUrl) }
            .flatMap { checkBCDUrl(saved.bcdUrl, providers.bcdUrl) }
            .map { true }
    }

    /**
     * Only checks the url if it has changed
     */
    private fun checkTezosNodeUrl(savedUrl: String, newUrl: String, verifierUrl: String): Single<Map<String, Any?>> {
        return if (savedUrl != newUrl) {
            val nodeUrlClient =
                TezosNodeClientFactory(newUrl, verifierUrl, clientWrapper).createTezosNodeClient()
            Single.create<Map<String, Any?>> {
                nodeUrlClient.getHead(emitter.emitCallback(it, EmitterSource.NETWORK_CONFIG_CHECK))
            }
        } else {
            Single.just(emptyMap())
        }
    }

    private fun checkTzKtUrl(savedUrl: String, newUrl: String): Single<Boolean> {
        return if (savedUrl != newUrl) {
            val client = DefaultNetworkClient(newUrl, emptyList(), clientWrapper)
            Single.create<TzKtAccount> {
                TzKtClient(client).getAccount(
                    userPreferences.getUserAddress().mainAddress,
                    emitter.emitCallback(it, EmitterSource.NETWORK_CONFIG_CHECK)
                )
            }.map { true }
        } else {
            Single.just(true)
        }
    }

    private fun checkBCDUrl(savedUrl: String, newUrl: String): Single<Boolean> {
        return if (savedUrl != newUrl) {
            val client = DefaultNetworkClient(newUrl, emptyList(), clientWrapper)
            Single.create<BCDAccount> {
                BCDClient(client).getAccount(
                    userPreferences.getUserAddress().mainAddress,
                    tezosNetwork,
                    emitter.emitCallback(it, EmitterSource.NETWORK_CONFIG_CHECK)
                )
            }.map { true }
        } else {
            Single.just(true)
        }
    }
}
