/**
 * # Released under MIT License
 *
 * Copyright (c) 2021 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.usecase.user

import io.camlcase.smartwallet.data.core.Wallet
import io.camlcase.smartwallet.data.model.AppWallet
import io.camlcase.smartwallet.data.model.error.AppError
import io.camlcase.smartwallet.data.model.error.ErrorType
import io.camlcase.smartwallet.data.source.local.UserPreferences
import io.camlcase.smartwallet.data.source.user.WalletLocalSource
import io.camlcase.smartwallet.data.usecase.LogOutUseCase
import javax.inject.Inject

class CreateWalletUseCase @Inject constructor(
    private val localSource: WalletLocalSource,
    private val logOutUseCase: LogOutUseCase,
    private val userPreferences: UserPreferences,
    private val versionCode: Int
) {

    /**
     * Create wallet: A BIPP44 index 0 wallet
     */
    fun createNewWallet(passphrase: String?): AppWallet {
        return createWallet(
            null,
            passphrase,
            Wallet.DEFAULT_BIP44_DERIVATION_PATH,
            true
        )
    }

    /**
     * Import a BIP44 wallet
     *
     * @param derivationPath In case user wants another index
     */
    fun recoverHDWallet(mnemonic: List<String>, passphrase: String? = null, derivationPath: String): AppWallet {
        return createWallet(mnemonic, passphrase, derivationPath, true)
    }

    /**
     * Import a linear wallet
     */
    fun recoverSDWallet(mnemonic: List<String>, passphrase: String?, isMigrated: Boolean): AppWallet {
        return createWallet(mnemonic, passphrase, null, isMigrated)
    }

    /**
     * Creates a wallet.
     *
     * @param derivationPath Will be saved to restore the wallet afterwards
     * @param isMigrated If the wallet is a migration
     * @param createWallet To choose between creating an HD or an SD wallet. Params: mnemonic, passphrase, derivation path
     */
    private fun createWallet(
        mnemonic: List<String>?,
        passphrase: String?,
        derivationPath: String?,
        isMigrated: Boolean
    ): AppWallet {
        // Clean everything, just in case user was UNLOGGED and wants to reset session
        logOutUseCase.logOut()

        val appWallet = try {
            AppWallet(mnemonic, passphrase, derivationPath)
        } catch (e: Throwable) {
            throw AppError(ErrorType.WALLET_CREATE_ERROR, exception = e)
        }

        if (mnemonic.isNullOrEmpty() || isMigrated) {
            // Two situations where we know it doesn't need to show the migration flow
            // - Newly created wallets
            // - Imported wallets where the user has explicitly said so
            userPreferences.updateVersionCreated(versionCode)
        }
        localSource.storeWallet(appWallet)
        return appWallet
    }

    /**
     * The migration wallet is the future new wallet of a user that needs to migrate. We save it separately until all
     * the funds of the user has been transferred and then substitute this newly created wallet with the old one.
     */
    fun getMigrationWallet(): AppWallet {
        var migrationWallet = localSource.getMigrationWallet()
        if (migrationWallet != null) {
            // Use always the same mnemonic
            return migrationWallet
        }

        migrationWallet = try {
            AppWallet(null, null, Wallet.DEFAULT_BIP44_DERIVATION_PATH)
        } catch (e: Throwable) {
            throw AppError(ErrorType.WALLET_CREATE_ERROR, exception = e)
        }
        if (migrationWallet == null) {
            throw AppError(ErrorType.WALLET_CREATE_ERROR)
        } else {
            localSource.storeMigrationWallet(migrationWallet)
        }
        return migrationWallet
    }

    fun swapMigrationWalletToStandardWallet() {
        localSource.swapMigrationWalletToStandardWallet()
        userPreferences.updateVersionCreated(versionCode)
    }
}
