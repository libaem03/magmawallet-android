/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.usecase

import io.camlcase.kotlintezos.TezosNodeClient
import io.camlcase.kotlintezos.model.RPCErrorResponse
import io.camlcase.kotlintezos.model.operation.Operation
import io.camlcase.kotlintezos.model.operation.fees.OperationFees
import io.camlcase.kotlintezos.operation.OperationFeesPolicy
import io.camlcase.smartwallet.data.core.RxEmitter
import io.camlcase.smartwallet.data.core.extension.bundleFees
import io.camlcase.smartwallet.data.core.extension.run
import io.camlcase.smartwallet.data.core.extension.tezos.getAccumulatedFee
import io.camlcase.smartwallet.data.source.user.WalletLocalSource
import io.reactivex.rxjava3.core.Single

interface OperationUseCase {
    val walletLocalSource: WalletLocalSource
    val tezosNodeClient: TezosNodeClient
    val emitter: RxEmitter

    fun run(operations: List<Operation>): Single<String> {
        return walletLocalSource.getWalletOrDie()
            .flatMap { tezosNodeClient.run(operations, it.address, it.wallet, emitter) }
    }

    fun bundleFees(
        operations: List<Operation>,
        feesPolicy: OperationFeesPolicy = OperationFeesPolicy.Estimate()
    ): Single<OperationBundle> {
        return walletLocalSource.getWalletOrDie()
            .flatMap { tezosNodeClient.bundleFees(operations, it.wallet.mainAddress, it.wallet, feesPolicy, emitter) }
            .map {
                val accumulatedFee = it.operations.getAccumulatedFee()
                OperationBundle(it.operations, accumulatedFee, it.simulationErrors)
            }
    }
}

/**
 * @param operations Transaction operations with calculated fees
 * @param accumulatedFee Accumulated Tez fee of all operations
 * @param internalErrors If there were any non-fatal errors while simulating
 */
data class OperationBundle(
    val operations: List<Operation>,
    var accumulatedFee: OperationFees,
    val internalErrors: List<RPCErrorResponse>? = null
)
