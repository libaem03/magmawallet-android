/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.model.exchange

import android.os.Parcel
import android.os.Parcelable
import io.camlcase.kotlintezos.model.Tez
import io.camlcase.smartwallet.data.model.Token
import java.math.BigDecimal
import java.math.BigInteger

data class DexterBalance(
    val balance: Tez,
    val tokens: BigInteger
) : Parcelable {
    fun getTokenPool(token: Token): BigDecimal {
        return tokens.moveComma(token.decimals)
    }

    constructor(parcel: Parcel) : this(
        Tez(parcel.readString() ?: "0"),
        BigInteger(parcel.readString() ?: "0")
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(balance.stringRepresentation)
        parcel.writeString(tokens.toString())
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<DexterBalance> {
        override fun createFromParcel(parcel: Parcel): DexterBalance {
            return DexterBalance(parcel)
        }

        override fun newArray(size: Int): Array<DexterBalance?> {
            return arrayOfNulls(size)
        }

        val empty = DexterBalance(Tez.zero, BigInteger.ZERO)
    }
}
