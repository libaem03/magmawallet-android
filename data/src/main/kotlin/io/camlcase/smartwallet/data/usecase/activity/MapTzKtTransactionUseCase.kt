/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.usecase.activity

import io.camlcase.kotlintezos.exchange.DexterEntrypoint
import io.camlcase.kotlintezos.model.Address
import io.camlcase.kotlintezos.model.operation.SmartContractCallOperation
import io.camlcase.kotlintezos.model.tzkt.TzKtOperation
import io.camlcase.kotlintezos.model.tzkt.TzKtTransaction
import io.camlcase.smartwallet.data.core.XTZ
import io.camlcase.smartwallet.data.model.Token
import io.camlcase.smartwallet.data.model.operation.ReceiveOperation
import io.camlcase.smartwallet.data.model.operation.SendOperation
import io.camlcase.smartwallet.data.model.operation.TezosOperation
import io.camlcase.smartwallet.data.model.operation.UnknownOperation
import io.camlcase.smartwallet.data.source.local.UserPreferences
import javax.inject.Inject
import javax.inject.Named

/**
 * Handles transactions and exchanges. Part of [MapTzKtOperationUseCase].
 */
class MapTzKtTransactionUseCase @Inject constructor(
    private val userPreferences: UserPreferences,
    @Named("SupportedFATokens")
    private val supportedFATokens: List<Token>
) {
    private val address
        get() = userPreferences.getUserAddress().mainAddress

    private val tokenTransferMapper =
        MapTokenTransferUseCase(supportedFATokens, { mapSmartContractTransaction(it) }, { received(it) })
    private val swapMapper = MapSwapUseCase(supportedFATokens)

    fun map(
        transaction: TzKtTransaction,
        operations: List<TzKtOperation>
    ): TezosOperation {
        return transaction.smartContractCall?.let { smartContract ->
            return when {
                smartContract.entrypoint == DexterEntrypoint.TOKEN_TO_TEZ.entrypoint -> {
                    swapMapper.mapTokenToTez(transaction, smartContract, operations)
                }
                smartContract.entrypoint == DexterEntrypoint.TEZ_TO_TOKEN.entrypoint -> {
                    swapMapper.mapTezToToken(transaction, smartContract, operations)
                }
                isTokenSendOperation(smartContract.entrypoint, transaction) -> {
                    tokenTransferMapper.mapTokenSend(transaction, smartContract, operations)
                }
                else -> mapSmartContractTransaction(transaction)
            }
        } ?: mapCommonTransaction(transaction)
    }

    private fun mapCommonTransaction(transaction: TzKtTransaction): TezosOperation {
        return if (received(transaction.destination.address)) {
            ReceiveOperation(XTZ, transaction)
        } else {
            SendOperation(XTZ, transaction)
        }
    }

    private fun received(destination: Address): Boolean {
        return destination == address
    }

    private fun isTokenSendOperation(
        entrypoint: String,
        transaction: TzKtTransaction
    ): Boolean {
        if (entrypoint == SmartContractCallOperation.SMART_CONTRACT_ENTRYPOINT_TRANSFER) {
            return true
        }

        // Some sends are done with the 'default' entrypoints
        if (entrypoint == SmartContractCallOperation.SMART_CONTRACT_ENTRYPOINT_DEFAULT) {
            val token =
                supportedFATokens.firstOrNull {
                    it.contractAddress == transaction.destination.address
                            // Dexter dApp can send a second user the result of an exchange
                            || it.dexterAddress == transaction.destination.address
                }
            return token != null
        }

        return false
    }

    private fun mapSmartContractTransaction(
        transaction: TzKtOperation
    ): TezosOperation {
        return UnknownOperation(listOf(transaction))
    }
}
