/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.model.error

interface OperationError: MagmaError

/**
 * Sending to correctly-formatted but nonexistent address
 */
class InvalidAddressError : OperationError, IllegalStateException()

/**
 * insufficient XTZ to pay network fees
 */
class InsufficientXTZ : OperationError, IllegalStateException()

/**
 * Cannot run current operation because there's another one waiting to be injected
 */
class OperationInProgress : OperationError, IllegalStateException()

/**
 * When user is registered as baker, they cannot delegate to another address
 */
class BakerCantDelegate : OperationError, IllegalStateException()

/**
 * Contract already delegated to the given delegate
 */
class UnchangedDelegate : OperationError, IllegalStateException()

/**
 * Dexter error: User tried to proceed with a trade that results in zero tokens returned.
 */
class ExchangeZeroTokens : OperationError, IllegalStateException()

/**
 * Dexter error
 */
class ExchangeInvalidTokens : OperationError, IllegalStateException()

/**
 * Dexter error: Timeout set up is too short
 */
class ExchangeDeadlineTimeout: OperationError, IllegalStateException()

/**
 * Dexter generic error
 */
class ExchangeError(cause: String?) : OperationError, IllegalStateException(cause)

/**
 * When waiting for an operation to be injected, force the process to fail if we need to retry because it's not visible yet.
 */
class OperationNotInjectedYet : MagmaError, Throwable()
