/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.core.extension

import java.util.*
import java.util.regex.Pattern

fun String.stripNonDigits(): String {
    return this.replace(Regex("[^\\d]"), "")
}

/**
 * Cleans a given input so it can be read as a list of words separated by spaces
 */
fun String.sanitizeSeedPhrase(): String {
    return this.replace("\n", " ")
        .replace(",", "")
        .replace(".", "")
        .replace("\\s+".toRegex(), " ").trim()
}

fun String.isOnlyLettersAndSpaces(): Boolean {
    val pattern = Pattern.compile(("^[a-zA-Z\\s]*$"))
    val matcher = pattern.matcher(this)
    return matcher.matches() && !this.contains("\n")
}

/**
 * Avoid adding an Android dependency by wrapping it up in an extension function.
 * To be called in classes where context is not available.
 */
fun String.toLowerCaseUS(): String {
    return this.toLowerCase(Locale.US)
}

/**
 * Avoid adding an Android dependency by wrapping it up in an extension function.
 * To be called in classes where context is not available.
 */
fun String.toUpperCaseUS(): String {
    return this.toUpperCase(Locale.US)
}
