/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.core

import io.camlcase.kotlintezos.TezosNodeClient
import io.camlcase.kotlintezos.TzKtClient
import io.camlcase.kotlintezos.model.Tez
import io.camlcase.kotlintezos.wallet.HDWallet
import io.camlcase.smartwallet.data.model.Token
import io.camlcase.smartwallet.data.model.exchange.CurrencyBundle
import io.camlcase.smartwallet.data.model.exchange.CurrencyType
import java.math.BigDecimal

const val MAINNET_FLAVOR = "mainnet"

val XTZ by lazy {
    Token(
        "Tezos",
        "XTZ",
        6,
        contractAddress = null,
        dexterAddress = null
    )
}

object Tokens {

    object Testnet {
        val TZBTC by lazy {
            Token(
                "Wrapped BTC",
                "tzBTC",
                8,
                contractAddress = "KT1BVwiXfDdaXsvcmvSmBkpZt4vbGVhLmhBh",
                dexterAddress = "KT1CUVF6urJ3zD2BjztL5vvneW1YiRfB2VdL"
            )
        }
        val USDTZ by lazy {
            Token(
                "USDTez",
                "USDtz",
                6,
                contractAddress = "KT1AuVKbQZTAwA7mdS7TZSPZD9ZH1Eb62JFf",
                dexterAddress = "KT1F77ZFv4cXu5nENdNvQVS42bw6ippkL4NR"
            )
        }
    }

    /**
     * Supported FA1.2 Tokens in Mainnet
     */
    object Mainnet {
        val TZBTC by lazy {
            Token(
                "Wrapped BTC",
                "tzBTC",
                8,
                contractAddress = "KT1PWx2mnDueood7fEmfbBDKx1D9BAnnXitn",
                dexterAddress = "KT1BGQR7t4izzKZ7eRodKWTodAsM23P38v7N"
            )
        }
        val USDTZ by lazy {
            Token(
                "USDTez",
                "USDtz",
                6,
                contractAddress = "KT1LN4LPSqTMS7Sd2CJw4bbDGRkMv2t68Fy9",
                dexterAddress = "KT1Tr2eG3eVmPRbymrbU2UppUmKjFPXomGG9"
            )
        }
        val WXTZ by lazy {
            Token(
                "Wrapped Tezos",
                "wXTZ",
                6,
                contractAddress = "KT1VYsVfmobT7rsMVivvZ4J8i3bPiqz12NaH",
                dexterAddress = "KT1D56HQfMmwdopmFLTwNHFJSs6Dsg2didFo"
            )
        }

        val ETHTZ by lazy {
            Token(
                "ETH Tez",
                "ETHtz",
                18,
                contractAddress = "KT19at7rQUvyjxnZ2fBv7D9zc8rkyG7gAoU8",
                dexterAddress = "KT1PDrBE59Zmxnb8vXRgRAG1XmvTMTs5EDHU"
            )
        }

        val KUSD by lazy {
            Token(
                "Kolibri",
                "kUSD",
                18,
                contractAddress = "KT1K9gCRgaLRFKTErYt1wVxA3Frb9FjasjTV",
                dexterAddress = "KT1AbYeDbjjcAnV1QK7EZUUdqku77CdkTuv6"
            )
        }
    }

    val unknownToken by lazy {
        Token(
            "N/A",
            "N/A",
            contractAddress = "",
            dexterAddress = ""
        )
    }

    val testnetFATokens: List<Token> = listOf(
        Testnet.TZBTC,
        Testnet.USDTZ
    )

    val mainnetFATokens: List<Token> = listOf(
        Mainnet.ETHTZ,
        Mainnet.KUSD,
        Mainnet.TZBTC,
        Mainnet.USDTZ,
        Mainnet.WXTZ,
    )

    val testnetSupportedTokens: List<Token> = listOf(XTZ).plus(testnetFATokens)
    val supportedTokens: List<Token> = listOf(XTZ).plus(mainnetFATokens)
}

object Delegate {
    const val BAKER_MINIMUM_FEE = 0.16
    const val BAKER_MINIMUM_ROI = 0.05

    /**
     * Randomly choosen Testent bakers to substitute Mainnet addresses on Test environment.
     */
    val testnetBakers = listOf(
        "tz1aWXP237BLwNHJcCD4b3DutCevhqq2T1Z9",
        "tz1VpvtSaSxKvykrqajFJTZqCXgoVJ5cKaM1",
        "tz1PirboZKFVqkfE45hVLpkpXaZtLk3mqC17",
        "tz1cXeGHP8Urj2pQRwpAkCdPGbCdqFUPsQwU",
        "tz1YSzTPwEUpMrRxeTTHNo6RVxfo2TMN633b",
        "tz1R55a2HQbXUAzWKJYE5bJp3UvvawwCm9Pr",
        "tz1VWasoyFGAWZt5K2qZRzP3cWzv3z7MMhP8",
        "tz1T8UYSbVuRm6CdhjvwCfXsKXb4yL9ai9Q3",
        "tz1f8Ybid6x2pMvJGauz3Zi8enkABSSkGPn8",
        "tz1fyYJwgV1ozj6RyjtU1hLTBeoqQvQmRjVv",
        "tz1RR6wETy9BeXG3Fjk25YmkSMGHxTtKkhpX",
        "tz1dAfFc4QAre74yrPU2jFBLcgaAs9MLHryD",
        "tz3TimAbQEubgxRKezDKyE5RwvHq1rR9H5tt",
        "tz1LbSsDSmekew3prdDGx1nS22ie6jjBN6B3",
        "tz1fyYJwgV1ozj6RyjtU1hLTBeoqQvQmRjVv",
        "tz3QrTBHrGRPD8f94QzpF6enwZUZiS8SSVxo",
        "tz1NhfcnynGwyYuJxuYBfYAa3pNnD85Ttaqv",
        "tz1ecKvfDKUgmqLcAbSVWrEUxZTwHREYDcUU",
        "tz1hW5PzkbvDwWBtoFMmM1dJuwL272HitKZB",
        "tz1NGS7FEGGfEFp5XRVkHBqVqSQ8VuumF9j3"
    )
}

object Wallet {
    const val DEFAULT_CURRENCY = "USD"
    const val DEFAULT_CURRENCY_SYMBOL = "$"
    val defaultCurrency = CurrencyBundle(DEFAULT_CURRENCY, CurrencyType.FIAT, DEFAULT_CURRENCY_SYMBOL)

    const val DEFAULT_BIP44_DERIVATION_PATH = HDWallet.DEFAULT_TEZOS_BIP44_DERIVATION_PATH

    fun validDerivationPath(path: String): Boolean = HDWallet.validDerivationPath(path)
}

object Exchange {
    /**
     * Slippage default limit: 0.5%
     */
    const val DEFAULT_SLIPPAGE = 0.005

    /**
     * Cap operations with a slippage > 50%
     */
    const val INVALID_SLIPPAGE_THRESHOLD = 0.5

    /**
     * Warn operations with a price impact of 50% or greater
     */
    const val WARNING_PRICE_IMPACT_THRESHOLD = 0.01

    /**
     * Cap operations with a price impact of 50% or greater
     */
    const val INVALID_PRICE_IMPACT_THRESHOLD = 0.5

    /**
     * Warn operations with a price impact 1% < 3% (not inclusive of 3%)
     */
    val noticePriceImpactRange = 0.01..0.03

    /**
     * Warn operations with a price impact 3% < 5% (not inclusive of 5%)
     */
    val warningPriceImpactRange = 0.03..0.05

    /**
     * Warn operations with a price impact 5% < 50% (not inclusive of 50%)
     */
    val bigWarningPriceImpactRange = 0.05..INVALID_PRICE_IMPACT_THRESHOLD

    /**
     * Exchange operations consume gas, storage and burn fees. We need a minimum XTZ balance to handle those
     * and we should cap those who leave the user with less than [minimumXTZBalance].
     */
    val minimumXTZBalance = Tez(0.5)

    /**
     * Deadline for reverting a Dexter swap
     */
    const val DEFAULT_TIMEOUT_MINUTES = 20
    const val MIN_TIMEOUT_MINUTES = 1
    const val MAX_TIMEOUT_MINUTES = 1_440
}

object Api {
    const val TIMEOUT_SECONDS = 70L
    const val BASIC_AUTH_HEADER = "Authorization"
}

object Urls {
    object Mainnet {
        const val NODE_URL = "https://api.tez.ie/rpc/mainnet"
        const val VERIFIER_NODE_URL = TezosNodeClient.MAINNET_SMARTPY_NODE
        const val TZKT_URL = TzKtClient.MAINNET_TZKT_URL
    }

    object Testnet {
        const val NODE_URL = TezosNodeClient.GRANADANET_ECAD_NODE
        const val VERIFIER_NODE_URL = TezosNodeClient.GRANADANET_SMARTPY_NODE
        const val TZKT_URL = TzKtClient.GRANADANET_TZKT_URL
    }

    const val BCD_URL = "https://api.better-call.dev/v1"
}

object Contacts {
    const val GOOGLE_ACCOUNT_TYPE = "com.google"
}

object Migration {

    /**
     * To transfer funds to the new wallet, we'd need a minimum buffer of XTZ
     */
    val XTZ_BUFFER = Tez(0.5)
    private val xtzBufferSubstract = XTZ_BUFFER.signedDecimalRepresentation.divide(
        2.toBigDecimal(),
        6,
        BigDecimal.ROUND_HALF_UP
    )

    /**
     * Fees change depending on amount so we should only substract a part of the minimum buffer to avoid failure
     */
    val XTZ_BUFFER_SUBSTRACT = Tez(xtzBufferSubstract)
}
