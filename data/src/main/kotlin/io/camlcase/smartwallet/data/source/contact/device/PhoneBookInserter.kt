/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.source.contact.device

import android.content.ContentProviderOperation
import android.content.ContentProviderResult
import android.content.ContentResolver
import android.provider.ContactsContract
import io.camlcase.smartwallet.data.core.Contacts
import io.camlcase.smartwallet.data.core.Logger
import io.camlcase.smartwallet.data.model.contact.ContactTezosAddress
import io.camlcase.smartwallet.data.model.contact.MagmaContact
import io.camlcase.smartwallet.data.source.contact.PhoneBookLocalSource
import java.io.IOException
import javax.inject.Inject

/**
 * INSERT operations for the Android Contacts database
 */
internal class PhoneBookInserter @Inject constructor(
    private val contentResolver: ContentResolver
) {
    /**
     * @return RAW_Contact_ID
     */
    fun addContact(account: String, contact: MagmaContact): Long {
        val operations = ArrayList<ContentProviderOperation>()

        operations.add(
            ContentProviderOperation.newInsert(ContactsContract.RawContacts.CONTENT_URI)
                .withValue(ContactsContract.RawContacts.ACCOUNT_TYPE, Contacts.GOOGLE_ACCOUNT_TYPE)
                .withValue(ContactsContract.RawContacts.ACCOUNT_NAME, account)
                .build()
        )
        // NAME
        operations.add(
            ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                .withValueBackReference(ContactsContract.RawContacts.Data.RAW_CONTACT_ID, 0)
                .withValue(
                    ContactsContract.RawContacts.Data.MIMETYPE,
                    ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE
                )
                .withValue(ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME, contact.name)
                .build()
        )
        // TEZOS ADDRESS
        val tezosAddress = contact.tezosAddresses[0]
        operations.add(
            ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
                .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Im.CONTENT_ITEM_TYPE)
                .withValue(ContactsContract.CommonDataKinds.Im.DATA, tezosAddress.formatted)
                .withValue(ContactsContract.CommonDataKinds.Im.TYPE, ContactsContract.CommonDataKinds.Im.TYPE_OTHER)
                .withValue(
                    ContactsContract.CommonDataKinds.Im.PROTOCOL,
                    ContactsContract.CommonDataKinds.Im.PROTOCOL_CUSTOM
                )
                .withValue(ContactsContract.CommonDataKinds.Im.CUSTOM_PROTOCOL, PhoneBookLocalSource.TEZOS_CONTACT)
                .build()
        )

        val results: Array<ContentProviderResult> = contentResolver.applyBatch(ContactsContract.AUTHORITY, operations)
        if (results.isEmpty() || results[0].uri?.lastPathSegment == null) {
            throw IOException("Contact couldn't be inserted.\n$contact")
        }
        // Insert will return raw_contact_id and we need contact_id
        return results[0].uri!!.lastPathSegment!!.toLong()
    }

    fun insertTezosAddress(
        id: Long,
        tezosAddress: ContactTezosAddress
    ): Boolean {
        val operations = ArrayList<ContentProviderOperation>()
        operations.add(tezosAddressOperation(id, tezosAddress))
        val results: Array<ContentProviderResult> = contentResolver.applyBatch(ContactsContract.AUTHORITY, operations)
        return !(results.isEmpty() || results[0].uri == null)
    }

    private fun tezosAddressOperation(
        id: Long = 0,
        tezosAddress: ContactTezosAddress
    ): ContentProviderOperation {
        return ContentProviderOperation.newInsert(ContactsContract.Data.CONTENT_URI)
            .withValue(ContactsContract.Data.RAW_CONTACT_ID, id)
            .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Im.CONTENT_ITEM_TYPE)
            .withValue(ContactsContract.CommonDataKinds.Im.DATA, tezosAddress.formatted)
            .withValue(ContactsContract.CommonDataKinds.Im.TYPE, ContactsContract.CommonDataKinds.Im.TYPE_OTHER)
            .withValue(
                ContactsContract.CommonDataKinds.Im.PROTOCOL,
                ContactsContract.CommonDataKinds.Im.PROTOCOL_CUSTOM
            )
            .withValue(ContactsContract.CommonDataKinds.Im.CUSTOM_PROTOCOL, PhoneBookLocalSource.TEZOS_CONTACT)
            .build()
    }
}
