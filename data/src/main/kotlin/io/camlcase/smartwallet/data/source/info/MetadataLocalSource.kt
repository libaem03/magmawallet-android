/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.source.info

import io.camlcase.kotlintezos.model.blockchain.BlockchainMetadata
import io.camlcase.smartwallet.data.core.db.MagmaDatabase
import io.camlcase.smartwallet.data.core.db.entity.MetadataEntity
import io.camlcase.smartwallet.data.core.db.entity.toEntity
import io.camlcase.smartwallet.data.core.db.entity.toTezosModel
import io.camlcase.smartwallet.data.source.local.UserPreferences
import io.camlcase.smartwallet.data.source.local.UserPreferences.Companion.BLOCKCHAIN_COUNTER
import io.reactivex.Single
import javax.inject.Inject

class MetadataLocalSource @Inject constructor(
    private val database: MagmaDatabase,
    private val counterLocalSource: CounterLocalSource
) {
    fun store(metadata: BlockchainMetadata) {
        val entity = metadata.toEntity()
        store(metadata.counter)
        database.metadataDao().insert(entity)
    }

    fun store(counter: Int) {
        counterLocalSource.saveCounter(counter)
    }

    fun get(): Single<BlockchainMetadata> {
        val counter = counterLocalSource.getCounter()
        return database.metadataDao()
            .query()
            .map { it.toTezosModel(counter) }
    }

    fun getSync(): BlockchainMetadata? {
        val counter = counterLocalSource.getCounter()
        val local: MetadataEntity? = database.metadataDao().querySync()
        return local?.toTezosModel(counter)
    }
}

class CounterLocalSource @Inject constructor(
    private val userPreferences: UserPreferences
) {
    fun getCounter(): Int {
        return userPreferences.getInt(BLOCKCHAIN_COUNTER)
    }

    fun saveCounter(counter: Int) {
        userPreferences.store(BLOCKCHAIN_COUNTER, counter)
    }
}
