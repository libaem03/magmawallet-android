/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.model

import io.camlcase.kotlintezos.model.Address
import io.camlcase.smartwallet.data.analytic.report
import org.json.JSONObject

data class UserTezosAddress(
    val address: Address
) : Serializable<String> {
    /**
     * Account where balance should be read from
     */
    val mainAddress: Address = address

    override fun serialize(): String {
        val jsonObject = JSONObject()
        jsonObject.put(ARG_ADDRESS, address)
        return jsonObject.toString()
    }

    companion object : Deserializer<UserTezosAddress, String> {
        const val ARG_ADDRESS = "address"

        val empty = UserTezosAddress("")

        override fun deserialize(serialized: String): UserTezosAddress? {
            return try {
                val jsonObject = JSONObject(serialized)
                val address = jsonObject[ARG_ADDRESS] as String
                return UserTezosAddress(address)
            } catch (e: Exception) {
                e.report("deserialize UserTezosAddress ($serialized)")
                null
            }
        }
    }
}

