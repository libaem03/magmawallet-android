/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.usecase

import io.camlcase.kotlintezos.core.ext.isTypeOfError
import io.camlcase.kotlintezos.core.ext.listCauses
import io.camlcase.kotlintezos.model.RPCErrorCause
import io.camlcase.kotlintezos.model.TezosError
import io.camlcase.kotlintezos.model.TezosErrorType
import io.camlcase.smartwallet.data.model.error.*

object ErrorParser {
    fun parse(error: Throwable): MagmaError {
        return if (error is TezosError) {
            parse(error)
        } else {
            parseGeneralError(error)
        }
    }

    fun parse(error: TezosError): MagmaError {
        if (error.type == TezosErrorType.COUNTER) {
            return OperationInProgress()
        }

        if (error.rpcErrors.isNullOrEmpty()) {
            return parseGeneralTezosError(error, null)
        } else {
            val causes = error.rpcErrors.listCauses()
            val parsedError = parseCauses(causes)
            return if (parsedError == null) {
                val contractErrors = ArrayList<MagmaError>()
                for (rpcError in error.rpcErrors!!) {
                    rpcError.contractArgs?.forEach { _, value ->
                        if (value is String && dexterErrors.contains(value)) {
                            contractErrors.add(ExchangeError(value))
                        }
                    }
                }

                if (contractErrors.isEmpty()) {
                    parseGeneralTezosError(error, causes)
                } else {
                    contractErrors[0]
                }
            } else {
                parsedError
            }
        }
    }

    fun parseGeneralTezosError(error: TezosError, causes: List<String>?): MagmaError {
        val cause: String = if (!causes.isNullOrEmpty()) {
            causes[0]
        } else if (error.exception != null) {
            error.exception!!.message ?: ""
        } else if (error.code != null) {
            error.code.toString()
        } else {
            error.type.name
        }
        return AppError(ErrorType.TEZOS_ERROR, cause, error.exception)
    }

    /**
     * TODO Detect more cases
     */
    private fun parseGeneralError(error: Throwable): MagmaError {
        if (error is okio.IOException) {
            return AppError(ErrorType.CONNECTION_FAIL, exception = error)
        } else {
            return AppError(ErrorType.UNKNOWN, exception = error)
        }
    }

    fun parseCauses(causes: List<String>): MagmaError? {
        return when {
            causes.isTypeOfError(RPCErrorCause.INSUFFICIENT_XTZ_BALANCE) -> {
                InsufficientXTZ()
            }
            causes.isTypeOfError(RPCErrorCause.INVALID_CONTRACT)
                    || causes.isTypeOfError(RPCErrorCause.INVALID_DELEGATE) -> {
                InvalidAddressError()
            }
            causes.isTypeOfError(RPCErrorCause.BAKER_CANT_DELEGATE)
                    || causes.isTypeOfError(RPCErrorCause.INVALID_DELEGATE) -> {
                BakerCantDelegate()
            }
            causes.isTypeOfError(RPCErrorCause.UNCHANGED_DELEGATED)
                    || causes.isTypeOfError(RPCErrorCause.INVALID_DELEGATE) -> {
                UnchangedDelegate()
            }
            causes.isTypeOfError(RPCErrorCause.UNCHANGED_DELEGATED)
                    || causes.isTypeOfError(RPCErrorCause.INVALID_DELEGATE) -> {
                UnchangedDelegate()
            }
            causes.isTypeOfError(RPCErrorCause.EXCHANGE_ZERO_TOKENS) -> {
                ExchangeZeroTokens()
            }
            causes.isTypeOfError(RPCErrorCause.EXCHANGE_INVALID_SWAP) -> {
                ExchangeInvalidTokens()
            }
            causes.isTypeOfError(RPCErrorCause.EXCHANGE_INVALID_DEADLINE) -> {
                ExchangeDeadlineTimeout()
            }
            else -> {
                parseDexterErrors(causes)
            }
        }
    }

    fun parseDexterErrors(causes: List<String>): ExchangeError? {
        for (dexterError in dexterErrors) {
            for (cause in causes) {
                if (cause.contains(dexterError)) {
                    return ExchangeError(cause)
                }
            }
        }
        return null
    }
}
