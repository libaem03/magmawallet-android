/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.model.money

import kotlinx.serialization.Serializable

@Serializable
data class MoonPayCurrency(
    val id: String,
    /**
     * Returned as an ISO 8601 string.
     * Example: "2018-09-28T10:47:49.801Z"
     */
    val createdAt: String,
    val updatedAt: String,
    @Serializable(with = MoonPayCurrencyTypeSerializer::class)
    val type: MoonPayCurrencyType,
    val name: String,
    val code: String,
    /**
     * The currency's precision (number of digits after decimal point).
     */
    val precision: Int,
    /**
     * The minimum transaction amount when using this currency as a base currency.
     */
    val minAmount: Double?,
    /**
     * The maximum transaction amount when using this currency as a base currency.
     */
    val maxAmount: Double?,
    /**
     * Whether the currency supports test mode.
     */
    val supportsTestMode: Boolean = false,
    /**
     * Whether purchases for this currency are suspended. If the currency is suspended, exchange rates may not be available and it is not possible to create a transaction with this currency.
     */
    val isSuspended: Boolean = false,
    /**
     * Whether purchases for this currency are supported in the US.
     */
    val isSupportedInUS: Boolean = false,
    /**
     * Whether sales for this currency are supported.
     */
    val isSellSupported: Boolean = false,
    /**
     * A list with all the US states for this currency that are not supported.
     * Example:  ["CT","NY"]
     */
    val notAllowedUSStates: List<String>? = null
)

enum class MoonPayCurrencyType {
    CRYPTO,
    FIAT
}
