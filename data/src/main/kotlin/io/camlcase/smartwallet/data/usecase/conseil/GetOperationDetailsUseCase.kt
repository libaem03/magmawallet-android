/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.usecase.conseil

import io.camlcase.kotlintezos.BCDClient
import io.camlcase.kotlintezos.TzKtClient
import io.camlcase.kotlintezos.model.OperationResultStatus
import io.camlcase.kotlintezos.model.bcd.BCDOperation
import io.camlcase.kotlintezos.model.operation.OperationType
import io.camlcase.kotlintezos.model.tzkt.TzKtOperation
import io.camlcase.smartwallet.data.core.EmitterSource
import io.camlcase.smartwallet.data.core.RxEmitter
import io.camlcase.smartwallet.data.model.error.AppError
import io.camlcase.smartwallet.data.model.error.ErrorType
import io.camlcase.smartwallet.data.model.error.MagmaError
import io.camlcase.smartwallet.data.model.error.OperationNotInjectedYet
import io.camlcase.smartwallet.data.model.operation.MagmaOperationResult
import io.camlcase.smartwallet.data.usecase.ErrorParser
import io.reactivex.rxjava3.core.BackpressureStrategy
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

/**
 * Fetch the details of an operation
 */
class GetOperationDetailsUseCase @Inject constructor(
    val emitter: RxEmitter,
    val client: TzKtClient,
    val bcdClient: BCDClient
) {

    fun get(operationHash: String): Flowable<List<TzKtOperation>> {
        return Flowable.create<List<TzKtOperation>>({
            client.getOperationDetails(operationHash, emitter.emitCallback(it, EmitterSource.WAIT_INJECTION))
        }, BackpressureStrategy.DROP)
    }

    fun getOperationDetail(operationHash: String): Flowable<MagmaOperationResult> {
        return get(operationHash)
            .flatMap {
                if (it.isEmpty()) {
                    throw OperationNotInjectedYet()
                }
                mapTzKtToOperationDetail(operationHash, it).toFlowable()
            }
    }

    /**
     * It will fetch all errors and get last operation status
     */
    private fun mapTzKtToOperationDetail(
        operationHash: String,
        list: List<TzKtOperation>
    ): Single<MagmaOperationResult> {
        val errors = ArrayList<String>()
        var status = OperationResultStatus.APPLIED
        var type = OperationType.TRANSACTION

        for (operation in list) {
            operation.errors?.map { it.type }?.apply {
                errors.addAll(this)
            }

            status = operation.status
            type = operation.kind
        }

        return if (errors.isEmpty()) {
            Single.just(MagmaOperationResult(type, status))
        } else {
            parseErrors(operationHash, errors)
                .map {
                    MagmaOperationResult(type, status, it)
                }
        }
    }

    private fun parseErrors(operationHash: String, errors: List<String>): Single<MagmaError> {
        val parsedError = ErrorParser.parseCauses(errors)
        return if (parsedError == null) {
            getExtraOperationDetail(operationHash)
                .map { list ->
                    val bcdErrors: List<String> = list.fold(ArrayList<String>()) { list, operation ->
                        operation.errors?.map {
                            list.add(it.contractMessage ?: it.description)
                        }
                        list
                    }
                    val bcdParsedError = ErrorParser.parseCauses(bcdErrors)
                    val fallbackError: String? = if (bcdErrors.isNotEmpty()) {
                        bcdErrors[0]
                    } else if (errors.isNotEmpty()) {
                        errors[0]
                    }else{
                        ""
                    }
                    bcdParsedError ?: AppError(ErrorType.TEZOS_ERROR, fallbackError)
                }
        } else {
            Single.just(parsedError)
        }
    }

    /**
     * Second opinion on errors
     */
    fun getExtraOperationDetail(operationHash: String): Single<List<BCDOperation>> {
        return Single.create<List<BCDOperation>> {
            bcdClient.getOperationDetails(operationHash, emitter.emitCallback(it, EmitterSource.GET_BCD_DETAILS))
        }.onErrorReturn {
            emptyList()
        }
    }
}
