/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.model.money

import kotlinx.serialization.Serializable

/**
 * Country objects represent the countries supported by MoonPay. If the isAllowed flag is set to false, it means that MoonPay accepts citizens of this country but not residents.
 * [API docs](https://www.moonpay.com/dashboard/api_reference/client_side_api#countries)
 */
@Serializable
data class MoonPaySupportedCountry(
    /**
     * The country's ISO 3166-1 alpha-2 code.
     */
    val alpha2: String,
    /**
     * The country's ISO 3166-1 alpha-3 code.
     */
    val alpha3: String,
    /**
     * Whether residents of this country can use the service.
     */
    val isAllowed: Boolean,
    /**
     * Whether residents of this country can buy cryptocurrencies.
     */
    val isBuyAllowed: Boolean,
    /**
     * Whether residents of this country can sell cryptocurrencies.
     */
    val isSellAllowed: Boolean,
    val name: String,
    /**
     * A list of supported identity documents for the country.
     * Example: ["passport","national_identity_card","driving_licence"]
     */
    val supportedDocuments: List<String>
)
