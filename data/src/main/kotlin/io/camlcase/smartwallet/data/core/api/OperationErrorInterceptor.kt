/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.core.api

import io.camlcase.kotlintezos.model.RPCErrorResponse
import io.camlcase.smartwallet.data.analytic.CrashReporter
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response
import okio.Buffer
import okio.IOException
import javax.inject.Inject

/**
 * Users can help diagnosing node errors by toggling a Settings flag that sends network information as analytics every time there's an operation error.
 */
class OperationErrorInterceptor @Inject constructor() : Interceptor {
    private val parser = OperationErrorParser()
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val response = chain.proceed(request)

        val body = response.peekBody(Long.MAX_VALUE).string()
        val errors: List<RPCErrorResponse>? = try {
            parser.parse(body)
        } catch (e: Throwable) {
            null
        }

        if (!errors.isNullOrEmpty()) {
//            reportError(request, body, errors)
        }
        return response
    }

    private fun reportError(request: Request, responseBody: String?, errors: List<RPCErrorResponse>) {
        CrashReporter.reportRPCError(
            request = request.stringifyRequestBody(),
            response = responseBody,
            errors = errors,
            url = request.url.toString()
        )
    }

    private fun Request.stringifyRequestBody(): String? {
        return try {
            val copy = this.newBuilder().build()
            val buffer = Buffer()
            copy.body?.writeTo(buffer)
            buffer.readUtf8()
        } catch (e: IOException) {
            this.body?.toString()
        }
    }
}
