/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.source.contact.device

import android.content.ContentResolver
import android.database.Cursor
import android.net.Uri
import android.provider.ContactsContract.*
import io.camlcase.kotlintezos.model.TezosNetwork
import io.camlcase.kotlintezos.model.validTezosAddress
import io.camlcase.smartwallet.data.core.Logger
import io.camlcase.smartwallet.data.model.WrappedUri
import io.camlcase.smartwallet.data.model.contact.ContactTezosAddress
import io.camlcase.smartwallet.data.model.contact.MagmaContact
import io.camlcase.smartwallet.data.source.contact.PhoneBookLocalSource
import io.camlcase.smartwallet.data.source.contact.PhoneBookLocalSource.Companion.TEZOS_CONTACT
import io.camlcase.smartwallet.data.source.contact.PhoneBookLocalSource.Companion.addressNetworkFormat
import javax.inject.Inject

/**
 * GET operations for the Android Contacts database
 */
internal class PhoneBookGetter @Inject constructor(
    private val contentResolver: ContentResolver,
    private val network: TezosNetwork
) {

    /**
     * Queries device database to fetch only those with IM/[TEZOS_CONTACT]/[TezosNetwork] values
     */
    fun getContacts(): ArrayList<MagmaContact> {
        val magmaContacts = ArrayList<MagmaContact>()
        val projection =
            arrayOf(
                Data.RAW_CONTACT_ID,
                Data.LOOKUP_KEY,
                Contacts.DISPLAY_NAME_PRIMARY,
                Data.DATA1,
                Data.DATA3,
                Data.MIMETYPE
            )

        contentResolver.query(
            Data.CONTENT_URI,
            projection,
            PhoneBookLocalSource.selectionByTezosAddress(network),
            null,
            "lower(" + Data.DISPLAY_NAME_PRIMARY + ")"
        )?.apply {
            while (this.moveToNext()) {
                val id = this.getLong(0)
                val permId = this.getLong(1)
                val name = this.getString(2)
                val address = this.getString(3)
                val tezosNetwork = this.getString(4) ?: TezosNetwork.MAINNET.name

                val tezosAddress = ContactTezosAddress.parse(address)
                val list = if (tezosAddress == null) {
                    emptyList()
                } else {
                    listOf(tezosAddress)
                }
                magmaContacts.add(
                    MagmaContact(
                        id,
                        name,
                        list
                    )
                )
            }

            this.close()
        }

        return magmaContacts
    }

    /**
     * Fetches raw_contact_id from the RawContact uri inserted.
     */
    fun getRawId(uri: Uri): Long {
        val projection = arrayOf(Data.RAW_CONTACT_ID)
        val cursor: Cursor? = contentResolver.query(uri, projection, null, null, null)
        cursor?.moveToNext()
        val contactId: Long = cursor?.getLong(0) ?: 0
        cursor?.close()
        return contactId
    }

    /**
     * Fetches contact_id
     */
    fun getContactId(rawId: Long): Long {
        val projection = arrayOf(Data.CONTACT_ID)
        val selection =  Data.RAW_CONTACT_ID + " = '" + rawId + "'"
        val cursor: Cursor? = contentResolver.query(Data.CONTENT_URI, projection, selection, null, null)
        cursor?.moveToNext()
        val contactId: Long = cursor?.getLong(0) ?: 0
        cursor?.close()
        return contactId
    }

    /**
     * Given a raw_contacts query uri, fetch the [TEZOS_CONTACT] data.
     * @return For now, a list with only the first element
     */
    fun queryTezosAddresses(uri: Uri): ArrayList<ContactTezosAddress> {
        val addresses = ArrayList<ContactTezosAddress>()
        val projection =
            arrayOf(
                Data.DATA1,
                Data.DATA3,
                Data.MIMETYPE
            )
        contentResolver.query(uri, projection, PhoneBookLocalSource.selectionByTezosAddress(network), null, null)
            ?.apply {
                if (this.moveToFirst()) {
                    val data = this.getString(0)

                    val tezosAddress = ContactTezosAddress.parse(data)
                    // User can edit the field from the outside. Check everything is correct
                    if (tezosAddress != null && tezosAddress.address.validTezosAddress() && tezosAddress.network == network) {
                        addresses.add(tezosAddress)
                    }
                }
                this.close()
            }
        return addresses
    }

    /**
     * Given an uri from the Contacts picker, fetch its related values and build a [MagmaContact] object.
     */
    fun getContactFromUri(bundle: WrappedUri): MagmaContact? {
        var contact: MagmaContact? = null
        bundle.uri?.let { uri ->
            contentResolver.query(uri, null, null, null, null)?.let {
                if (it.moveToFirst()) {
                    var idx = it.getColumnIndex(Contacts._ID)
                    val _id = it.getLong(idx)
                    idx = it.getColumnIndex(Contacts.DISPLAY_NAME_PRIMARY)
                    val name = it.getString(idx)

                    // Once we get the _id we can query again for the Tezos Address, if there's any
                    val b = Uri.withAppendedPath(Contacts.CONTENT_URI, _id.toString()).buildUpon()
                    b.appendPath(Contacts.Entity.CONTENT_DIRECTORY)
                    val contactUri: Uri = b.build()

                    val id = getRawId(contactUri)
                    val addresses = queryTezosAddresses(contactUri)

                    contact = MagmaContact(id, name, addresses)
                }
                it.close()
            }
        }
        return contact
    }

    /**
     * Check if the contact has other mime types apart from [TEZOS_CONTACT]-[TezosNetwork]
     */
    fun getContactExtraTypes(contact: MagmaContact): List<String> {
        val projection =
            arrayOf(
                Data.RAW_CONTACT_ID,
                Contacts.DISPLAY_NAME_PRIMARY,
                Data.MIMETYPE
            )

        // Check, for that id, that there's something different than the current Tezos Network. This should return entries such as:
        // - Any other mimeTypes the user has: phone, email, address
        // - Any other IM mimeTypes the user has
        // - Any other TEZOS_CONTACT types that aren't the current network
        val selection =
            Data.RAW_CONTACT_ID + " = '" + contact.id + "' " +
                    // When we create a Magma contact in a Google account it automatically adds it to "My contacts" group
                    "AND " + Data.MIMETYPE + " NOT LIKE '" + CommonDataKinds.GroupMembership.CONTENT_ITEM_TYPE + "' " +
                    // We should ignore the Name field
                    "AND " + Data.MIMETYPE + " NOT LIKE '" + CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE + "' " +
                    "AND " + CommonDataKinds.Im.DATA + " NOT LIKE " + addressNetworkFormat(network)

        val mimeTypes = ArrayList<String>()
        contentResolver.query(
            Data.CONTENT_URI,
            projection,
            selection,
            null,
            null
        )?.apply {
            while (this.moveToNext()) {
                val id = this.getLong(0)
                val mimeType = this.getString(2)

                mimeTypes.add(mimeType)
            }
            this.close()
        }
        return mimeTypes
    }
}
