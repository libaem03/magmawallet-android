/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.source.local

import android.content.Context
import android.content.SharedPreferences
import io.camlcase.smartwallet.data.core.AppContext

/**
 * Status of the user agreement to permissions
 */
class PermissionPreferences(
    override val context: AppContext,
    override val fileName: String = "permission_preferences"
) : Preferences {
    override val preferences: SharedPreferences? = context.get()?.getSharedPreferences(fileName, Context.MODE_PRIVATE)

    fun getPermissionStatus(permission: String): PermissionStatus {
        val savedStatus = getString(permission)
        return savedStatus?.let { PermissionStatus.valueOf(it) } ?: PermissionStatus.FIRST_DIALOG
    }

    fun storePermissionStatus(permission: String, status: PermissionStatus) {
        store(permission, status.name)
    }

    companion object {
        const val CAMERA_PERMISSION = "user.permission.camera"
        const val WRITE_CONTACTS_PERMISSION = "user.permission.contacts.write"
        const val READ_CONTACTS_PERMISSION = "user.permission.contacts.read"
    }
}

enum class PermissionStatus {
    FIRST_DIALOG,
    SECOND_DIALOG,
    DONT_REMIND_DIALOG,
    PERMISSION_GIVEN
}
