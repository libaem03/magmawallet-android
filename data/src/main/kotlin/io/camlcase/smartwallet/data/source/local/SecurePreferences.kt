/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.source.local

import android.content.SharedPreferences
import androidx.security.crypto.EncryptedSharedPreferences
import androidx.security.crypto.MasterKey
import androidx.security.crypto.MasterKey.KeyScheme
import io.camlcase.smartwallet.data.core.AppContext
import javax.inject.Inject

/**
 * Wraps the SharedPreferences class and automatically encrypts keys and values using a master key stored using
 * the Android Keystore system.
 */
class SecurePreferences @Inject constructor(
    override val context: AppContext,
    override val fileName: String = "encrypted_preferences"
) : Preferences {

    // In case MasterKey.Builder.buildOnM ever gets deleted
//    private val keyGenParameterSpec = KeyGenParameterSpec.Builder(
//        MasterKey.DEFAULT_MASTER_KEY_ALIAS,
//        (KeyProperties.PURPOSE_ENCRYPT or KeyProperties.PURPOSE_DECRYPT)
//    )
//        .setBlockModes(KeyProperties.BLOCK_MODE_GCM)
//        .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_NONE)
//        .setKeySize(DEFAULT_AES_GCM_MASTER_KEY_SIZE).build()

    private val masterKey = MasterKey.Builder(context.get()!!, MasterKey.DEFAULT_MASTER_KEY_ALIAS)
        .setKeyScheme(KeyScheme.AES256_GCM)
        .build()

    override val preferences: SharedPreferences? by lazy {
        context.get()?.let {
            EncryptedSharedPreferences
                .create(
                    context.get()!!,
                    fileName,
                    masterKey,
                    EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
                    EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
                )
        }
    }

    /**
     * Workaround to a known bug: https://issuetracker.google.com/issues/138314232
     */
    override fun clear() {
        preferences?.apply {
            val editor = edit()
            for (item in all.keys) {
                editor.remove(item)
            }
            editor.clear()
            editor.apply()
        }
    }

    companion object {
        const val KEY_WALLET = "smart.wallet"
        const val KEY_WALLET_MIGRATION = "smart.wallet.new"
        const val KEY_PUBLIC_KEY = "smart.wallet.public_key"
        const val KEY_PIN = "app.pin"
        const val KEY_PIN_SALT = "app.pin.salt"
    }
}



