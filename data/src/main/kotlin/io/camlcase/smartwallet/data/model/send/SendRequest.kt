/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.model.send

import android.os.Parcel
import android.os.Parcelable
import io.camlcase.kotlintezos.model.Address
import io.camlcase.kotlintezos.model.Tez
import io.camlcase.smartwallet.data.core.XTZ
import io.camlcase.smartwallet.data.model.Token
import io.camlcase.smartwallet.data.model.amount.*
import io.camlcase.smartwallet.data.model.isXTZ
import java.math.BigInteger

data class SendRequest(
    val amount: Balance<*>,
    val to: Address,
    val token: Token = XTZ
) : Parcelable {

    var balance: Balance<*> = EmptyBalance()
    var delegated: Boolean = false
    val transactionAmount: Balance<*>
        get() {
            return if (maxXTZ && delegated) {
                // You cannot empty a delegated empty account. Either leave 1 mutez or undelegate.
                val maxAmountForDelegated = (amount as CoinBalance) - Tez("1")
                CoinBalance(maxAmountForDelegated)
            } else {
                amount
            }
        }

    val maxXTZ: Boolean
        get() {
            return token.isXTZ() && balance.isEqualTo(amount)
        }

    val xtzValue: Tez
        get() {
            return (transactionAmount as? CoinBalance)?.value ?: Tez.zero
        }

    val tokenValue: BigInteger
        get() {
            return (amount as? TokenBalance)?.rpcRepresentation ?: BigInteger.ZERO
        }

    constructor(parcel: Parcel) : this(
        parcel.readParcelable(Balance::class.java.classLoader) ?: EmptyBalance(),
        parcel.readString() ?: "",
        parcel.readParcelable(Token::class.java.classLoader) ?: XTZ
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeParcelable(amount, flags)
        parcel.writeString(to)
        parcel.writeParcelable(token, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    fun copy(newAmount: Balance<*>): SendRequest {
        val sendRequest = SendRequest(newAmount, to, token)
        sendRequest.balance = balance
        sendRequest.delegated = delegated
        return sendRequest
    }

    companion object CREATOR : Parcelable.Creator<SendRequest> {
        override fun createFromParcel(parcel: Parcel): SendRequest {
            return SendRequest(parcel)
        }

        override fun newArray(size: Int): Array<SendRequest?> {
            return arrayOfNulls(size)
        }

        val empty = SendRequest(EmptyBalance(), "")
    }
}
