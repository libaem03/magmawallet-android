/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.usecase.delegate

import io.camlcase.kotlintezos.TezosNodeClient
import io.camlcase.kotlintezos.model.Address
import io.camlcase.smartwallet.data.core.Delegate
import io.camlcase.smartwallet.data.core.EmitterSource
import io.camlcase.smartwallet.data.core.Logger
import io.camlcase.smartwallet.data.core.RxEmitter
import io.camlcase.smartwallet.data.model.delegate.TezosBaker
import io.camlcase.smartwallet.data.model.delegate.TezosBakerResponse
import io.camlcase.smartwallet.data.model.error.AppError
import io.camlcase.smartwallet.data.model.error.ErrorType
import io.camlcase.smartwallet.data.source.delegate.BakerLocalSource
import io.camlcase.smartwallet.data.source.delegate.BakerRemoteSource
import io.camlcase.smartwallet.data.source.local.UserPreferences
import io.camlcase.smartwallet.data.usecase.user.ShouldUpdateUseCase
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

class GetDelegateUseCase @Inject constructor(
    private val emitter: RxEmitter,
    private val userPreferences: UserPreferences,
    private val updateUseCase: ShouldUpdateUseCase,
    private val tezosNodeClient: TezosNodeClient,
    private val bakerRemoteSource: BakerRemoteSource,
    private val bakerLocalSource: BakerLocalSource,
    private val testnetBakers: List<String>,
    private val isMainnet: Boolean,
    private val delegateStream: DelegateStream
) {

    fun listenToDelegate(): Flowable<Result<TezosBaker>> {
        return delegateStream.stream
            .doOnNext { Logger.i("** Delegate change to: $it") }
    }

    /**
     * Fetch blockchain delegate and store it
     */
    fun fetchDelegate(): Single<String> {
        val address = userPreferences.getUserAddress().mainAddress
        return Single.create<String> {
            tezosNodeClient.getDelegate(address, emitter.emitCallback(it, EmitterSource.GET_DELEGATE))
        }
            .flatMap { matchDelegate(it) }
            .map {
                val bakerAddress = it.address
                userPreferences.store(UserPreferences.DELEGATE_ADDRESS, bakerAddress)
                delegateStream.onDelegate(it)
                bakerAddress
            }
    }

    fun getDelegate(): Single<Result<TezosBaker>> {
        val currentBaker = userPreferences.getString(UserPreferences.DELEGATE_ADDRESS)
        return if (currentBaker == null) {
            Single.just(Result.failure(AppError(ErrorType.NO_BAKER)))
        } else {
            matchDelegate(currentBaker)
                .map { Result.success(it) }
        }
    }

    fun pingLocalDelegate(): Single<Result<TezosBaker>> {
        return getDelegate()
            .map {
                delegateStream.onDelegate(it)
                it
            }
    }

    private fun matchDelegate(currentBaker: Address): Single<TezosBaker> {
        return supportedDelegates()
            .map { list ->
                list.firstOrNull { it.address == currentBaker }?.let {
                    TezosBaker(it.name, it.address)
                } ?: TezosBaker(null, currentBaker)
            }
    }

    /**
     * Will return locally stored bakers.
     * If local is empty, will try to fetch them.
     */
    fun supportedDelegates(): Single<List<TezosBakerResponse>> {
        return bakerLocalSource.get()
            .flatMap { localList ->
                if (localList.isNullOrEmpty()) {
                    remoteSupportedDelegates()
                } else {
                    Single.just(localList)
                }
            }
    }

    private fun remoteSupportedDelegates(): Single<List<TezosBakerResponse>> {
        return remoteDelegates()
    }

    fun updateDelegates(): Single<Boolean> {
        return if (updateUseCase.shouldUpdateBakers()) {
            remoteSupportedDelegates()
                .map {
                    updateUseCase.updateBakersTimestamp()
                    true
                }
        } else {
            Single.just(false)
        }
    }

    private fun remoteDelegates(): Single<List<TezosBakerResponse>> {
        return bakerRemoteSource.getListOfSupportedDelegates()
            .map { list ->
                var filtered = filterDelegates(list)
                if (!isMainnet) {
                    // Use Carthage baker addresses as placeholders so the delegating works
                    filtered = filtered.take(testnetBakers.size)
                        .mapIndexed { index, response -> response.copy(testnetBakers[index]) }
                }
                filtered

            }
            .flatMap { filtered ->
                bakerLocalSource.store(filtered)
                    .map { filtered }
            }
    }

    /**
     * Don't show all, filter for usability purposes.
     */
    private fun filterDelegates(delegates: List<TezosBakerResponse>): List<TezosBakerResponse> {
        return delegates.filter {
            it.openForDelegation
                    && it.estimatedRoi >= Delegate.BAKER_MINIMUM_ROI
                    && it.fee < Delegate.BAKER_MINIMUM_FEE
//             Already filtered on query
//                    && it.serviceHealth == BakerHealth.ACTIVE.name
//                    && it.payoutTiming == BakerTiming.STABLE.name
//                    && it.payoutAccuracy == BakerAccuracy.PRECISE.name
        }
    }
}

