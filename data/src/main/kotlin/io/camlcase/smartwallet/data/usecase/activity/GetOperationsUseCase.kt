/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.usecase.activity

import io.camlcase.kotlintezos.TzKtClient
import io.camlcase.kotlintezos.data.tzkt.GetOperationsByAddressRPC
import io.camlcase.kotlintezos.data.tzkt.GetOperationsParameter
import io.camlcase.kotlintezos.data.tzkt.GetTransactionsByParameter
import io.camlcase.kotlintezos.model.tzkt.TzKtOperation
import io.camlcase.kotlintezos.model.tzkt.TzKtTransaction
import io.camlcase.smartwallet.data.core.EmitterSource
import io.camlcase.smartwallet.data.core.RxEmitter
import io.camlcase.smartwallet.data.core.extension.tezos.getTzktDate
import io.camlcase.smartwallet.data.model.contact.ContactTezosAddress
import io.camlcase.smartwallet.data.model.operation.ReceiveOperation
import io.camlcase.smartwallet.data.model.operation.SendOperation
import io.camlcase.smartwallet.data.model.operation.TezosOperation
import io.camlcase.smartwallet.data.source.local.UserPreferences
import io.camlcase.smartwallet.data.source.operation.OperationsLocalSource
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

class GetOperationsUseCase @Inject constructor(
    private val emitter: RxEmitter,
    private val userPreferences: UserPreferences,
    private val tzKtClient: TzKtClient,
    private val localSource: OperationsLocalSource,
    private val mapUseCase: MapTzKtOperationUseCase
) {

    /**
     * Fetches either last 100 or latest since [OperationsLocalSource.timestamp] operations.
     */
    fun operations(): Single<List<TezosOperation>> {
        val address = userPreferences.getUserAddress().mainAddress

        return Single.create<List<TzKtOperation>> {
            tzKtClient.operations(
                address,
                filters = getFilters(),
                filtersForTokenReceive = getTokenReceiveFilters(),
                callback = emitter.emitCallback(it, EmitterSource.LIST_OPERATIONS)
            )
        }.map {
            val operations = mapUseCase.map(it)
            localSource.store(operations)
            localSource.operations
        }
    }

    private fun getFilters(): Map<GetOperationsParameter, String> {
        val lastTimestamp = if (localSource.timestamp > 0L) {
            localSource.timestamp.getTzktDate()
        } else {
            null
        }

        val map = HashMap<GetOperationsParameter, String>()
        map[GetOperationsParameter.TYPE] = GetOperationsByAddressRPC.QUERY_PARAMS_TYPE_VALUE
        if (lastTimestamp != null) {
            map[GetOperationsParameter.TIMESTAMP_GT] = lastTimestamp
        }
        return map
    }

    private fun getTokenReceiveFilters(): Map<GetTransactionsByParameter, String> {
        val lastTimestamp = if (localSource.timestamp > 0L) {
            localSource.timestamp.getTzktDate()
        } else {
            null
        }
        return if (lastTimestamp != null) {
            mapOf(
                GetTransactionsByParameter.TIMESTAMP_GT to lastTimestamp
            )
        } else {
            emptyMap()
        }
    }

    /**
     * Given a list of possible source/destination, filter the local operations stored by them.
     */
    fun filterOperations(tezosAddresses: List<ContactTezosAddress>): List<TezosOperation> {
        val operations = localSource.get()
        val filtered: List<TezosOperation> = operations.filter {
            if (it is SendOperation) {
                tezosAddresses.forEach { address ->
                    if (address.address == it.destination.address) {
                        return@filter true
                    }
                }
            }

            if (it is ReceiveOperation) {
                val source = (it.tzKtOperations[0] as TzKtTransaction).sender.address
                tezosAddresses.forEach { address ->
                    if (address.address == source) {
                        return@filter true
                    }
                }
            }
            return@filter false

        }

        return filtered
    }

    fun localOperations(): List<TezosOperation> {
        return localSource.get()
    }

    fun clear() {
        localSource.clear()
    }
}
