/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.model.delegate

import io.camlcase.kotlintezos.model.Address
import io.camlcase.smartwallet.data.core.Delegate
import java.io.Serializable

/**
 * Public baker info, optionally including additional information such as configs, insurance and contribution
 * [See api](https://baking-bad.org/docs/api#get-bakers).
 */
@kotlinx.serialization.Serializable
data class TezosBakerResponse(
    val name: String,
    val address: Address,
    val fee: Double, // In decimals
    val logo: String? = null, // URL
    val estimatedRoi: Double,
    val serviceHealth: String?, // health=active,closed,dead
    val payoutTiming: String?, // timing=stable,unstable,suspicious,no_data.
    val payoutAccuracy: String?, // accuracy=precise,inaccurate,suspicious,no_data
    val openForDelegation: Boolean,
    /**
     * Minimal amount to delegate.
     */
    val minDelegation: Double
) : Serializable {

    /**
     * For constructing testnet bakers
     */
    constructor(name: String, address: Address) : this(
        name,
        address,
        Delegate.BAKER_MINIMUM_FEE,
        null,
        Delegate.BAKER_MINIMUM_ROI,
        null,
        null,
        null,
        true,
        0.0
    )

    fun copy(address: Address): TezosBakerResponse {
        return TezosBakerResponse(
            this.name,
            address,
            this.fee,
            this.logo,
            this.estimatedRoi,
            this.serviceHealth,
            this.payoutTiming,
            this.payoutAccuracy,
            this.openForDelegation,
            this.minDelegation
        )
    }
}
