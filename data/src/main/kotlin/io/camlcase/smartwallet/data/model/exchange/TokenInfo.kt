/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.model.exchange

import android.os.Parcel
import android.os.Parcelable
import io.camlcase.kotlintezos.model.Tez
import io.camlcase.smartwallet.data.core.XTZ
import io.camlcase.smartwallet.data.core.extension.formatAsMoney
import io.camlcase.smartwallet.data.model.Token
import io.camlcase.smartwallet.data.model.amount.Balance
import io.camlcase.smartwallet.data.model.amount.EmptyBalance
import io.camlcase.smartwallet.data.model.showExchange
import java.math.BigInteger

data class TokenInfoBundle(
    val token: Token,
    val balance: Balance<*>,
    val exchange: TokenInfo
) : Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readParcelable(Token::class.java.classLoader) ?: XTZ,
        parcel.readParcelable(Balance::class.java.classLoader) ?: EmptyBalance(),
        parcel.readParcelable(TokenInfo::class.java.classLoader) ?: XTZTokenInfo.empty
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeParcelable(token, flags)
        parcel.writeParcelable(balance, flags)
        parcel.writeParcelable(exchange, flags)

    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<TokenInfoBundle> {
        override fun createFromParcel(parcel: Parcel): TokenInfoBundle {
            return TokenInfoBundle(parcel)
        }

        override fun newArray(size: Int): Array<TokenInfoBundle?> {
            return arrayOfNulls(size)
        }

        val empty = TokenInfoBundle(XTZ, EmptyBalance(), XTZTokenInfo.empty)
    }
}

/**
 * Decimal/Integer formatted + token symbol
 */
fun TokenInfoBundle.formattedBalance(): String {
    return "${this.balance.formattedValue} ${this.token.symbol}"
}

fun TokenInfoBundle.formattedCurrencyBalance(bundle: CurrencyBundle): String {
    return this.balance.inCurrency(this.exchange.currencyExchange).formatAsMoney(bundle)
}

/**
 * Show for exchange if:
 * - The token *is* in Dexter
 * - The token has tez/token pool to exchange with
 */
fun TokenInfoBundle.showInExchange(): Boolean {
    val dexterBalance = (this.exchange as? FATokenInfo)?.dexterBalance
    val emptyDexterPool = dexterBalance?.let {
        it.balance == Tez.zero || it.tokens.compareTo(BigInteger.ZERO) == 0
    } ?: false
    return this.token.showExchange() && !emptyDexterPool
}

/**
 * Information regarding a certain Tezos blockhain Token
 */
interface TokenInfo : Parcelable {
    val token: Token

    /**
     * Price in the user currency of 1 value of that token.
     */
    val currencyExchange: Double
}

data class XTZTokenInfo(
    override val currencyExchange: Double
) : TokenInfo, Parcelable {
    override val token: Token = XTZ

    constructor(parcel: Parcel) : this(
        parcel.readDouble()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeDouble(currencyExchange)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<XTZTokenInfo> {
        override fun createFromParcel(parcel: Parcel): XTZTokenInfo {
            return XTZTokenInfo(parcel)
        }

        override fun newArray(size: Int): Array<XTZTokenInfo?> {
            return arrayOfNulls(size)
        }

        val empty = XTZTokenInfo(1.0)
    }
}

/**
 * Exchange Info of a certain FA1.2 token
 *
 * @param currencyExchange The price in currency of 1 Token
 * @param dexterBalance Tez/Token pool
 */
data class FATokenInfo(
    override val token: Token,
    override val currencyExchange: Double,
    val dexterBalance: DexterBalance
) : TokenInfo, Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readParcelable(Token::class.java.classLoader) ?: XTZ,
        parcel.readDouble(),
        parcel.readParcelable(DexterBalance::class.java.classLoader) ?: DexterBalance.empty
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeParcelable(token, flags)
        parcel.writeDouble(currencyExchange)
        parcel.writeParcelable(dexterBalance, flags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<FATokenInfo> {
        override fun createFromParcel(parcel: Parcel): FATokenInfo {
            return FATokenInfo(parcel)
        }

        override fun newArray(size: Int): Array<FATokenInfo?> {
            return arrayOfNulls(size)
        }
    }
}
