/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.model.exchange

import android.os.Parcel
import android.os.Parcelable
import io.camlcase.smartwallet.data.core.Wallet

data class CurrencyBundle(
    /**
     * 3-word representation of the currency
     */
    val code: String,
    /**
     * Will be formatted differently depending on type
     */
    val type: CurrencyType,
    /**
     * For [CurrencyType.FIAT] types, the symbol. For [CurrencyType.CRYPTO], same as [code]
     */
    val symbol: String
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString() ?: Wallet.DEFAULT_CURRENCY,
        CurrencyType.valueOf(parcel.readString() ?: CurrencyType.FIAT.name),
        parcel.readString() ?: Wallet.DEFAULT_CURRENCY_SYMBOL
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(code)
        parcel.writeString(type.name)
        parcel.writeString(symbol)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<CurrencyBundle> {
        override fun createFromParcel(parcel: Parcel): CurrencyBundle {
            return CurrencyBundle(parcel)
        }

        override fun newArray(size: Int): Array<CurrencyBundle?> {
            return arrayOfNulls(size)
        }
    }
}
