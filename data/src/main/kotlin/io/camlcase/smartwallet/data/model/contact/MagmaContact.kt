/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.model.contact

import android.os.Parcel
import android.os.Parcelable
import io.camlcase.kotlintezos.model.Address
import io.camlcase.kotlintezos.model.TezosNetwork
import io.camlcase.smartwallet.data.source.contact.PhoneBookLocalSource
import java.util.*

/**
 * @param id In the device database as raw_contact_id
 * @param tezosAddresses 1-to-many relation
 */
data class MagmaContact(
    val id: Long = 0,
    val name: String,
    val tezosAddresses: List<ContactTezosAddress>
) : Parcelable {
    val address: Address?
        get() {
            return if (tezosAddresses.isEmpty()) {
                null
            } else {
                tezosAddresses[0].address
            }
        }

    constructor(parcel: Parcel) : this(
        parcel.readLong(),
        parcel.readString() ?: "",
        parcel.createTypedArrayList(ContactTezosAddress) ?: emptyList()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeLong(id)
        parcel.writeString(name)
        parcel.writeTypedList(tezosAddresses)
    }

    fun copy(id: Long): MagmaContact {
        return MagmaContact(id, name, tezosAddresses)
    }

    fun copy(tezosAddresses: List<ContactTezosAddress>): MagmaContact {
        return MagmaContact(id, name, tezosAddresses)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<MagmaContact> {
        override fun createFromParcel(parcel: Parcel): MagmaContact {
            return MagmaContact(parcel)
        }

        override fun newArray(size: Int): Array<MagmaContact?> {
            return arrayOfNulls(size)
        }
    }
}

data class ContactTezosAddress(
    val address: Address,
    val network: TezosNetwork
) : Parcelable {
    /**
     * E.g: mainnet//tz1...
     */
    val formatted = network.name.toLowerCase(Locale.ROOT) + PhoneBookLocalSource.SEPARATOR + address

    constructor(parcel: Parcel) : this(
        parcel.readString() ?: "",
        TezosNetwork.valueOf(parcel.readString() ?: TezosNetwork.MAINNET.name)
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(address)
        parcel.writeString(network.name)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ContactTezosAddress> {
        override fun createFromParcel(parcel: Parcel): ContactTezosAddress {
            return ContactTezosAddress(parcel)
        }

        override fun newArray(size: Int): Array<ContactTezosAddress?> {
            return arrayOfNulls(size)
        }

        fun parse(formatted: String): ContactTezosAddress? {
            val split = formatted.split("//")
            return if (split.size == 2) {
                try {
                    val network = TezosNetwork.valueOf(split[0].toUpperCase(Locale.ROOT))
                    ContactTezosAddress(split[1], network)
                } catch (e: IllegalArgumentException) {
                    // The valueOf() method throws an IllegalArgumentException if the specified name does not match any
                    // of the enum constants defined in the class.
                    null
                }
            } else {
                null
            }
        }
    }
}
