/**
 * # Released under MIT License
 *
 * Copyright (c) 2021 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.analytic

import android.content.Context
import dagger.Reusable
import io.camlcase.kotlintezos.model.TezosError
import io.camlcase.smartwallet.data.core.AppContext
import io.camlcase.smartwallet.data.core.EmitterSource

/**
 * @see DataSourceModule
 */
@Reusable
class AnalyticsTracker constructor(
    private val appContext: AppContext,
    private val isMainnet: Boolean,
    private val sentryApiKey: String,
//    private val mixpanelToken: String
) : Reportable {
    private val crashReporter: CrashReporter = CrashReporter(appContext, sentryApiKey)
//    private lateinit var mixpanel: MixpanelAPI

    init {
        appContext.get()?.apply {
            initExternalTrackers(this)
        }
    }

    private fun initExternalTrackers(context: Context) {
//        if (!isMainnet) {
//            mixpanel = MixpanelAPI.getInstance(context, mixpanelToken)
//        }
    }

    fun unbind() {
//        if (!isMainnet) {
//            mixpanel.flush()
//        }
    }

    /**
     * Handle reset of session
     */
    fun logout() {
//        if (!isMainnet) {
//            mixpanel.reset()
//        }
    }

    override fun reportEvent(value: String, extra: String?) {
//        crashReporter.reportEvent(value, extra)
//        if (!isMainnet) {
//            mixpanel.track(value)
//        }
    }

    override fun reportEvent(value: String, params: Map<String, String>) {
//        if (!isMainnet) {
//            val props = JSONObject()
//            params.forEach { (key, value) ->
//                props.put(key, value)
//            }
//            mixpanel.track(value, props)
//        }
    }

    override fun addTrail(value: String) {
//        crashReporter.addTrail(value)
    }

    override fun reportException(e: Throwable) {
//        crashReporter.reportException(e)
    }

    fun reportTezosError(error: TezosError, source: EmitterSource) {
//        CrashReporter.reportErrorEvent(error, source)
    }
}
