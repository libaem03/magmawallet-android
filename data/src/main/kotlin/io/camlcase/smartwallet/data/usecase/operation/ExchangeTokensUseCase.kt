/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.usecase.operation

import io.camlcase.kotlintezos.TezosNodeClient
import io.camlcase.kotlintezos.exchange.DexterEntrypoint
import io.camlcase.kotlintezos.model.Address
import io.camlcase.kotlintezos.model.Tez
import io.camlcase.kotlintezos.model.TezosError
import io.camlcase.kotlintezos.model.TezosErrorType
import io.camlcase.kotlintezos.model.operation.Operation
import io.camlcase.kotlintezos.model.operation.SmartContractCallOperation
import io.camlcase.kotlintezos.model.operation.TokenOperationParams
import io.camlcase.kotlintezos.model.operation.TokenOperationType
import io.camlcase.kotlintezos.model.operation.fees.OperationFees
import io.camlcase.kotlintezos.operation.OperationFactory
import io.camlcase.kotlintezos.operation.OperationFeesPolicy
import io.camlcase.kotlintezos.smartcontract.michelson.BigIntegerMichelsonParameter
import io.camlcase.kotlintezos.smartcontract.michelson.IntegerMichelsonParameter
import io.camlcase.kotlintezos.smartcontract.michelson.PairMichelsonParameter
import io.camlcase.kotlintezos.smartcontract.michelson.StringMichelsonParameter
import io.camlcase.smartwallet.data.core.RxEmitter
import io.camlcase.smartwallet.data.core.extension.tezos.getTotalFees
import io.camlcase.smartwallet.data.model.Token
import io.camlcase.smartwallet.data.model.amount.CoinBalance
import io.camlcase.smartwallet.data.model.amount.TokenBalance
import io.camlcase.smartwallet.data.model.error.AppError
import io.camlcase.smartwallet.data.model.error.ErrorType
import io.camlcase.smartwallet.data.model.error.InsufficientXTZ
import io.camlcase.smartwallet.data.model.isXTZ
import io.camlcase.smartwallet.data.model.swap.SwapBundleRequest
import io.camlcase.smartwallet.data.source.local.UserPreferences
import io.camlcase.smartwallet.data.source.user.WalletLocalSource
import io.camlcase.smartwallet.data.usecase.OperationBundle
import io.camlcase.smartwallet.data.usecase.OperationUseCase
import io.camlcase.smartwallet.data.usecase.analytics.AnalyticEvent
import io.camlcase.smartwallet.data.usecase.analytics.SendAnalyticUseCase
import io.reactivex.rxjava3.core.Single
import java.math.BigDecimal
import java.math.BigInteger
import java.util.*
import javax.inject.Inject

class ExchangeTokensUseCase @Inject constructor(
    override val emitter: RxEmitter,
    private val userPreferences: UserPreferences,
    override val walletLocalSource: WalletLocalSource,
    override val tezosNodeClient: TezosNodeClient,
    private val analytics: SendAnalyticUseCase,
    private val calculationsUseCase: DexterCalculationsUseCase,
) : OperationUseCase {

    fun swap(operations: List<Operation>): Single<String> {
        return run(operations)
            .doOnError { analytics.reportEvent(AnalyticEvent.EXCHANGE_ERROR) }
    }

    fun createSwapOperation(request: SwapBundleRequest): Single<OperationBundle> {
        val feesPolicy = if (request.maxXTZ()) {
            OperationFeesPolicy.Default()
        } else {
            OperationFeesPolicy.Estimate()
        }
        return createSwapOperation(request, feesPolicy)
    }

    /**
     * Depending if smart contract created or not, will:
     * - Create operations needed to send xtz/fa1.2 token
     * - Simulate fees
     * _ Bundle operations with fees
     */
    fun createSwapOperation(
        request: SwapBundleRequest,
        feesPolicy: OperationFeesPolicy
    ): Single<OperationBundle> {
        val fromToken = request.fromToken.token
        val toToken = request.toToken.token

        val user = userPreferences.getUserAddress()
        val swapType = SwapType.get(fromToken)

        return calculationsUseCase.getMinimumOut(
            request.fromAmount,
            request.fromToken,
            request.toToken,
            request.slippage
        )
            .map { amountTo ->
                val tezAmount: Tez
                val tokenAmount: BigInteger
                val tokenContractAddress: Address
                val dexterAddress: Address
                if (fromToken.isXTZ()) {
                    tezAmount = (request.fromAmount as CoinBalance).value
                    tokenAmount = (amountTo as TokenBalance).rpcRepresentation
                    dexterAddress = toToken.dexterAddress!!
                    tokenContractAddress = toToken.contractAddress!!
                } else {
                    tezAmount = (amountTo as CoinBalance).value
                    tokenAmount = (request.fromAmount as TokenBalance).rpcRepresentation
                    dexterAddress = fromToken.dexterAddress!!
                    tokenContractAddress = fromToken.contractAddress!!
                }

                createOperations(
                    swapType,
                    dexterAddress,
                    user.address,
                    tokenContractAddress,
                    tezAmount,
                    tokenAmount,
                    request.timeout
                )
            }
            .flatMap { list ->
                bundleFees(list, feesPolicy)
                    .flatMap {
                        if (request.maxXTZ()) {
                            // Recalculate with fees substracted from amount
                            recalculateXtzToTokenAmount(request, it)
                        } else {
                            if (swapType == SwapType.TOKEN_TO_XTZ && overXTZFunds(it.accumulatedFee.getTotalFees())) {
                                throw AppError(ErrorType.BLOCKED_OPERATION, exception = InsufficientXTZ())
                            }
                            Single.just(it)
                        }
                    }
            }
    }

    private fun overXTZFunds(fee: Tez): Boolean {
        return fee > userPreferences.getTezBalance()
    }

    /**
     * Tries to do an exchange with amount - (fees + padding)
     */
    private fun recalculateXtzToTokenAmount(
        request: SwapBundleRequest,
        bundle: OperationBundle
    ): Single<OperationBundle> {
        request.operationFee = bundle.accumulatedFee
        val substractFeesFromAmount = request.substractFeesFromAmount
        return if (substractFeesFromAmount.bigDecimal > BigDecimal.ZERO) {
            val excRequest = request.copy(substractFeesFromAmount)
            createSwapOperation(excRequest, OperationFeesPolicy.Estimate())
        } else {
            throw AppError(ErrorType.BLOCKED_OPERATION, exception = InsufficientXTZ())
        }
    }

    private fun createOperations(
        swapType: SwapType,
        dexterContractAddress: Address,
        from: Address, // tz1 manager
        tokenContractAddress: Address,
        tezAmount: Tez,
        tokenAmount: BigInteger,
        timeout: Int,
        fees: OperationFees = OperationFees.simulationFees
    ): List<Operation> {
        return when (swapType) {
            SwapType.XTZ_TO_TOKEN -> {
                val params = TokenOperationParams.TezToToken(
                    dexterContractAddress,
                    from,
                    from,
                    tezAmount,
                    tokenAmount,
                    swapDeadline(timeout)
                )
                OperationFactory.createTokenOperation(TokenOperationType.TEZ_TO_TOKEN, params, fees)!!
            }
            SwapType.TOKEN_TO_XTZ -> {
                val params = TokenOperationParams.TokenToTez(
                    dexterContractAddress,
                    tokenContractAddress,
                    from,
                    from,
                    from,
                    tezAmount,
                    tokenAmount,
                    swapDeadline(timeout),
                    BigInteger.ZERO // Won't be used
                )
                createTokenToTezOperation(params, fees)
            }
        }
    }

    /**
     * Operation for [TokenOperationType.TOKEN_TO_TEZ]
     * We don't use [OperationFactory] as we force it to have allowance.
     */
    private fun createTokenToTezOperation(
        params: TokenOperationParams.TokenToTez,
        fees: OperationFees
    ): List<SmartContractCallOperation> {
        val tezAmount: BigInteger = try {
            params.amount.stringRepresentation.toBigInteger()
        } catch (e: NumberFormatException) {
            throw TezosError(TezosErrorType.INTERNAL_ERROR, exception = e)
        }

        val parameter = PairMichelsonParameter(
            PairMichelsonParameter(
                StringMichelsonParameter(params.owner),
                StringMichelsonParameter(params.destination)
            ),
            PairMichelsonParameter(
                BigIntegerMichelsonParameter(params.tokenAmount),
                PairMichelsonParameter(
                    BigIntegerMichelsonParameter(tezAmount),
                    StringMichelsonParameter(params.deadline)
                )
            )
        )

        val operation = SmartContractCallOperation(
            Tez.zero,
            params.from,
            params.dexterAddress,
            fees,
            parameter,
            DexterEntrypoint.TOKEN_TO_TEZ.entrypoint
        )

        val approveOperation =
            createApproveOperations(
                params.from,
                params.tokenAddress,
                params.dexterAddress,
                params.tokenAmount,
                fees
            )
        val operations = approveOperation.toMutableList()
        operations.add(operation)
        return operations
    }

    /**
     * List all the operations needed to approve the transfer of [tokenAmount] through DEXter.
     * Assumes allowance is always != 0 to avoid fetching it from Indexter.
     *
     * @param tokenContract Tezos address for that FA1.2 token
     * @param dexterContract Tezos address for the Dexter smart contract of [tokenContract]
     */
    private fun createApproveOperations(
        from: Address,
        tokenContract: Address,
        dexterContract: Address,
        tokenAmount: BigInteger,
        fees: OperationFees
    ): List<SmartContractCallOperation> {
        val approveParameter = PairMichelsonParameter(
            StringMichelsonParameter(dexterContract),
            BigIntegerMichelsonParameter(tokenAmount)
        )
        val approval = SmartContractCallOperation(
            Tez.zero,
            from,
            tokenContract,
            fees,
            approveParameter,
            SmartContractCallOperation.SMART_CONTRACT_ENTRYPOINT_APPROVE
        )

        // Since allowance is != 0
        // when the approval is not zero, set it to zero first, then the target value
        val resetParameter = PairMichelsonParameter(
            StringMichelsonParameter(dexterContract),
            IntegerMichelsonParameter(0)
        )

        val resetApproval = SmartContractCallOperation(
            Tez.zero,
            from,
            tokenContract,
            fees,
            resetParameter,
            SmartContractCallOperation.SMART_CONTRACT_ENTRYPOINT_APPROVE
        )
        return listOf(resetApproval, approval)
    }

    companion object {
        private fun swapDeadline(timeout: Int): Date {
            val now = Calendar.getInstance()
            now.add(Calendar.MINUTE, timeout)
            return now.time
        }
    }
}

enum class SwapType {
    XTZ_TO_TOKEN,
    TOKEN_TO_XTZ;

    companion object {
        /**
         * @param fromToken Token to start the trade
         */
        fun get(
            fromToken: Token
        ): SwapType {
            val tezToToken = fromToken.isXTZ()

            return if (tezToToken) {
                XTZ_TO_TOKEN
            } else {
                TOKEN_TO_XTZ
            }
        }

        fun deadline(minutes: Int): Date {
            val now = Calendar.getInstance()
            now.add(Calendar.MINUTE, minutes)
            return now.time
        }
    }
}
