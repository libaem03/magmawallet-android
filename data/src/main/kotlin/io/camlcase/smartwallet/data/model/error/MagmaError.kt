/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.model.error

/**
 * Base class for all errors
 */
interface MagmaError

enum class ErrorType {
    /**
     * Something changed outside the scope of the app
     * (E.g. balance is lower now and you cannot send the amount)
     */
    EXPIRED_DATA,

    /**
     * Critical data has been compromised. Delete and logout.
     */
    NO_WALLET,

    /**
     * Critical KT1 data not stored
     */
    NO_MAGMA_CONTRACT,

    /**
     * [Result] says success but no data retrieved
     */
    INVALID_RESULT,

    /**
     * An operation couldn't be executed.
     * (E.g. Transfering tz1 funds and these are lower than the buffer)
     *
     * @see InvalidAddressError, [InsufficientXTZ]
     */
    BLOCKED_OPERATION,


    /**
     * The operation returned null and cannot be passed in a Rx thread
     * (E.g.: Get delegate returns null if no delegate set)
     */
    EMPTY_DATA,

    /**
     * Something went wrong when encrypting a PIN.
     * @see CryptoUtils
     */
    PIN_ALGORITHM_ERROR,

    /**
     * User has no delegate stored
     */
    NO_BAKER,

    /**
     * Something went wrong with core library
     */
    WALLET_CREATE_ERROR,

    /**
     * Error returned by KotlinTezos. Use always with [AppError.message].
     */
    TEZOS_ERROR,

    /**
     * Okhttp error
     */
    CONNECTION_FAIL,

    UNKNOWN
}

/**
 * Dexter specific errors
 */
val dexterErrors = listOf(
    /**
     * Approve operation for token doesn't have the correct value of allowance.
     */
    "UnsafeAllowanceChange",
)


