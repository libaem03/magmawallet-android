/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.usecase.activity

import io.camlcase.kotlintezos.model.Address
import io.camlcase.kotlintezos.model.SmartContractCall
import io.camlcase.kotlintezos.model.operation.SmartContractCallOperation
import io.camlcase.kotlintezos.model.tzkt.TzKtOperation
import io.camlcase.kotlintezos.model.tzkt.TzKtTransaction
import io.camlcase.kotlintezos.model.tzkt.dto.TzKtAlias
import io.camlcase.kotlintezos.smartcontract.michelson.*
import io.camlcase.smartwallet.data.core.Tokens
import io.camlcase.smartwallet.data.core.extension.multiLet
import io.camlcase.smartwallet.data.model.Token
import io.camlcase.smartwallet.data.model.amount.TokenBalance
import io.camlcase.smartwallet.data.model.operation.ReceiveOperation
import io.camlcase.smartwallet.data.model.operation.SendOperation
import io.camlcase.smartwallet.data.model.operation.TezosOperation
import io.camlcase.smartwallet.data.model.operation.TransferOperation
import java.math.BigInteger

/**
 * Handles token transfers mapping.
 */
internal class MapTokenTransferUseCase(
    private val supportedFATokens: List<Token>,
    private val mapSmartContractTransaction: (transaction: TzKtOperation) -> TezosOperation,
    private val received: (Address) -> Boolean
) {
    fun mapTokenSend(
        transaction: TzKtTransaction,
        smartContract: SmartContractCall,
        operations: List<TzKtOperation>
    ): TezosOperation {
        val token =
            supportedFATokens.firstOrNull {
                it.contractAddress == transaction.destination.address
                        // Dexter dApp can send a second user the result of an exchange
                        || it.dexterAddress == transaction.destination.address
            } ?: Tokens.unknownToken

        return if (smartContract.entrypoint == SmartContractCallOperation.SMART_CONTRACT_ENTRYPOINT_TRANSFER) {
            mapTokenSendTransfer(token, transaction, smartContract, operations)
        } else {
            mapTokenSendDefault(token, transaction, smartContract, operations)
        }
    }

    private fun mapTokenSendTransfer(
        token: Token,
        transaction: TzKtTransaction,
        smartContract: SmartContractCall,
        operations: List<TzKtOperation>
    ): TezosOperation {
        smartContract.parameters?.apply {
            // Pair \"SOURCE\" (Pair \"DESTINATION\" AMOUNT)
            val parentPair = this as PairMichelsonParameter
            val from = (parentPair.left as? StringMichelsonParameter)?.value
            val to = ((parentPair.right as? PairMichelsonParameter)?.left as? StringMichelsonParameter)?.value
            val amount = ((parentPair.right as? PairMichelsonParameter)?.right as? BigIntegerMichelsonParameter)?.value

            return multiLet(from, to, amount) { source, destination, tokenAmount ->
                mapTokenSendParameters(token, operations, source, destination, tokenAmount)
            } ?: mapSmartContractTransaction(transaction)
        }
        return mapSmartContractTransaction(transaction)
    }

    private fun mapTokenSendDefault(
        token: Token,
        transaction: TzKtTransaction,
        smartContract: SmartContractCall,
        operations: List<TzKtOperation>
    ): TezosOperation {
        // Left (Left (Left (Pair \"SOURCE\" (Pair \"DESTINATION\" AMOUNT))))"
        var triple: Triple<String?, String?, BigInteger?>? = smartContract.parameters?.let {
            val pairMichelsonParameter =
                (((it as? LeftMichelsonParameter)?.value as? LeftMichelsonParameter)?.value as? LeftMichelsonParameter)?.value as? PairMichelsonParameter
            val from = (pairMichelsonParameter?.left as? StringMichelsonParameter)?.value
            val to =
                ((pairMichelsonParameter?.right as? PairMichelsonParameter)?.left as? StringMichelsonParameter)?.value
            val amount =
                ((pairMichelsonParameter?.right as? PairMichelsonParameter)?.right as? BigIntegerMichelsonParameter)?.value

            Triple(from, to, amount)
        }
        var operation = multiLet(triple?.first, triple?.second, triple?.third) { source, destination, tokenAmount ->
            mapTokenSendParameters(token, operations, source, destination, tokenAmount)
        }
        if (operation != null) {
            return operation
        }

        // OR
        // Left (Left (Right (Pair "DESTINATION" 1000000)))
        val pair: Pair<String?, BigInteger?>? = smartContract.parameters?.let {
            val pairMichelsonParameter =
                (((it as? LeftMichelsonParameter)?.value as? LeftMichelsonParameter)?.value as? RightMichelsonParameter)?.value as? PairMichelsonParameter
            val to =
                ((pairMichelsonParameter?.left as? PairMichelsonParameter)?.left as? StringMichelsonParameter)?.value
            val amount =
                ((pairMichelsonParameter?.left as? PairMichelsonParameter)?.right as? BigIntegerMichelsonParameter)?.value

            Pair(to, amount)
        }
        operation = multiLet(pair?.first, pair?.second) { destination, tokenAmount ->
            mapTokenSendParameters(token, operations, transaction.sender.address, destination, tokenAmount)
        }
        if (operation != null) {
            return operation
        }

        // OR
        // Pair (Pair \"SOURCE\" \"DESTINATION\") AMOUNT
        triple = smartContract.parameters?.let {
            val pairMichelsonParameter =
                ((it as? PairMichelsonParameter)?.left as? PairMichelsonParameter)
            val from = (pairMichelsonParameter?.left as? StringMichelsonParameter)?.value
            val to =
                ((pairMichelsonParameter?.left as? PairMichelsonParameter)?.right as? StringMichelsonParameter)?.value
            val amount = ((it as? PairMichelsonParameter)?.right as? BigIntegerMichelsonParameter)?.value

            Triple(from, to, amount)
        }
        operation = multiLet(triple?.first, triple?.second, triple?.third) { source, destination, tokenAmount ->
            mapTokenSendParameters(token, operations, source, destination, tokenAmount)
        }
        if (operation != null) {
            return operation
        }

        return mapSmartContractTransaction(transaction)
    }

    private fun mapTokenSendParameters(
        token: Token,
        operations: List<TzKtOperation>,
        source: Address,
        destination: Address,
        tokenAmount: BigInteger
    ): TransferOperation {
        return if (received(destination)) {
            ReceiveOperation(
                token,
                operations,
                TzKtAlias(address = source),
                TokenBalance(tokenAmount, token.decimals)
            )
        } else {
            SendOperation(
                token,
                operations,
                TzKtAlias(address = destination),
                TokenBalance(tokenAmount, token.decimals)
            )
        }
    }
}
