/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.core

import io.camlcase.kotlintezos.model.TezosCallback
import io.camlcase.kotlintezos.model.TezosError
import io.camlcase.smartwallet.data.analytic.report
import io.reactivex.rxjava3.core.Scheduler
import io.reactivex.rxjava3.core.SingleEmitter
import io.reactivex.rxjava3.schedulers.Schedulers

object DataRxThread {
    fun ioThread(): Scheduler {
        return Schedulers.io()
    }

    fun newThread(): Scheduler {
        return Schedulers.newThread()
    }

    fun singleThread(): Scheduler {
        return Schedulers.single()
    }
}

/**
 * Converts a [TezosCallback] into a Single emitter
 */
fun <T> SingleEmitter<T>?.emitTezosCallback(report: Boolean = true): TezosCallback<T> {
    val emitter = this
    return object : TezosCallback<T> {
        override fun onFailure(error: TezosError) {
            Logger.e(error.toString() + " | ${emitter?.isDisposed}", "TezosCallback ERROR:")
            if (emitter?.isDisposed == false) {
                emitter.onError(error)
            }
            if (report) {
                error.report()
            }
        }

        override fun onSuccess(item: T?) {
            Logger.e(item?.toString() ?: "null", "TezosCallback SUCCESS:")
            if (emitter?.isDisposed == false) {
                emitter.onSuccess(item)
            }
        }
    }
}
