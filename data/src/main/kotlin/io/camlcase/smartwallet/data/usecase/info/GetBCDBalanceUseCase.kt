/**
 * # Released under MIT License
 *
 * Copyright (c) 2021 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.usecase.info

import io.camlcase.kotlintezos.BCDClient
import io.camlcase.kotlintezos.model.Address
import io.camlcase.kotlintezos.model.TezosNetwork
import io.camlcase.kotlintezos.model.bcd.BCDAccount
import io.camlcase.kotlintezos.model.bcd.BCDAccountToken
import io.camlcase.smartwallet.data.core.EmitterSource
import io.camlcase.smartwallet.data.core.RxEmitter
import io.camlcase.smartwallet.data.core.XTZ
import io.camlcase.smartwallet.data.model.Token
import io.camlcase.smartwallet.data.model.UserTezosAddress
import io.camlcase.smartwallet.data.model.amount.CoinBalance
import io.camlcase.smartwallet.data.model.amount.TokenBalance
import io.camlcase.smartwallet.data.model.error.AppError
import io.camlcase.smartwallet.data.model.error.ErrorType
import io.camlcase.smartwallet.data.model.exchange.*
import io.camlcase.smartwallet.data.source.local.UserPreferences
import io.camlcase.smartwallet.data.source.token.TokenBalanceLocalSource
import io.camlcase.smartwallet.data.usecase.GetExchangeUseCase
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.core.Single
import java.math.BigInteger
import javax.inject.Inject
import javax.inject.Named

/**
 * Use Better-call.dev API to fetch balance information. Merge them with Dexter and Coingecko info.
 */
class GetBCDBalanceUseCase @Inject constructor(
    @Named("SupportedFATokens")
    private val supportedFATokens: List<Token>,
    private val network: TezosNetwork,
    private val emitter: RxEmitter,
    private val client: BCDClient,
    private val tokenLocalSource: TokenBalanceLocalSource,
    private val userPreferences: UserPreferences,
    private val xtzCurrencyUseCase: GetXTZCurrencyExchange,
    private val exchangeUseCase: GetExchangeUseCase,
) {
    private val user: UserTezosAddress
        get() {
            return userPreferences.getUserAddress()
        }

    fun getCurrencyBundle(): CurrencyBundle {
        return xtzCurrencyUseCase.getCurrencyBundle()
    }

    fun fallbackBalances(): Single<List<Result<TokenInfoBundle>>> {
        val local = getLocalBalances()
        return if (local.isEmpty()) {
            getBalances()
        } else {
            Single.fromCallable { local.map { Result.success(it) } }
        }
    }

    fun getLocalBalances(): List<TokenInfoBundle> {
        return tokenLocalSource.get()
    }

    fun getBalances(): Single<List<Result<TokenInfoBundle>>> {
        // Query BCD for all balances
        return getBalanceInfo(user.mainAddress)
            .flatMap { parseBalances(it) }
    }

    /**
     * A single token information
     * @throws AppError EMPTY_DATA If no balance for that [token] could be fetched
     */
    fun getTokenInfo(token: Token): Single<TokenInfoBundle> {
        return getBalances()
            .map { list ->
                val result = list.firstOrNull { it.getOrNull()?.token == token }
                result?.getOrNull() ?: throw AppError(
                    ErrorType.EMPTY_DATA,
                    exception = list.firstOrNull { it.exceptionOrNull() != null }?.exceptionOrNull()
                )
            }
    }

    private fun parseBalances(bcdAccount: BCDAccount): Single<List<Result<TokenInfoBundle>>> {
        return getXTZInfo(bcdAccount)
            .flatMap { xtzInfo ->
                getTokensInfo(bcdAccount, xtzInfo)
                    .map {
                        val all = ArrayList(it)
                        all.add(Result.success(xtzInfo))
                        all
                    }
            }
    }

    /**
     * @param xtzInfo To Calculate the currency-to-token rate depending on Dexter pools + currency-to-xtz rate
     */
    private fun getTokensInfo(bcdAccount: BCDAccount, xtzInfo: TokenInfoBundle): Single<List<Result<TokenInfoBundle>>> {
        val dexterTokens = HashMap<String, TokenBalance>()
        val balances = HashMap<Token, TokenBalance>()
        bcdAccount.tokens.forEach { accountToken ->
            // Do we support it or is a N/A Token
            val token = supportedFATokens.firstOrNull { it.contractAddress == accountToken.contract }
                ?: accountToken.mapToToken()
            // Tokens with uri relate to NFT and we don't support them at the moment
            if (accountToken.displayUri == null) {
                userPreferences.storeTokenBalance(token, accountToken.balance)
                val balance = TokenBalance(accountToken.balance, token.decimals)
                balances[token] = balance
                token.dexterAddress?.apply {
                    dexterTokens[token.symbol] = balance
                }
            }
        }

        // BCD will only return tokens we have balance of, we need to query Dexter for ALL supported tokens
        supportedFATokens.forEach {
            if (!dexterTokens.containsKey(it.symbol)) {
                dexterTokens[it.symbol] = TokenBalance(BigInteger.ZERO, it.decimals)
            }
        }

        return getTokensDexterData(dexterTokens, xtzInfo)
            .map { dexterList ->
                // We only pass the tokens who have a dexter contract. Will need to populate the rest afterwards.
                if (dexterTokens.size < balances.size) {
                    val allList = ArrayList<Result<TokenInfoBundle>>(dexterList)
                    balances.forEach {
                        if (!dexterTokens.containsKey(it.key.symbol)) {
                            val bundle =
                                TokenInfoBundle(it.key, it.value, FATokenInfo(it.key, 1.0, DexterBalance.empty))
                            allList.add(Result.success(bundle))
                        }
                    }
                    allList
                } else {
                    dexterList
                }
            }
    }

    /**
     * Fetch [BCDAccount] remotely for a certain [address]
     */
    fun getBalanceInfo(address: Address): Single<BCDAccount> {
        return Single.create<BCDAccount> {
            client.getAccountBalance(address, network, emitter.emitCallback(it, EmitterSource.BCD_BALANCE))
        }
    }

    private fun getXTZInfo(bcdAccount: BCDAccount): Single<TokenInfoBundle> {
        val xtz = bcdAccount.balance
        userPreferences.storeTezBalance(xtz)
        val balance = CoinBalance(xtz)
        return getXTZExchange()
            .map {
                val xtzBundle = TokenInfoBundle(XTZ, balance, it)
                tokenLocalSource.store(xtzBundle)
                xtzBundle
            }
    }

    fun getXTZExchange(): Single<XTZTokenInfo> {
        return xtzCurrencyUseCase.getXTZExchange()
    }

    /**
     * @param balances List of balances of all supported tokens. Needs to be same tokens as [supportedFATokens].
     * @return Dexter pools of all supported FA1.2 token data
     * @throws NoSuchElementException If tokens returned is different from [supportedFATokens]
     */
    private fun getTokensDexterData(
        balances: Map<String, TokenBalance>,
        xtzInfo: TokenInfoBundle
    ): Single<List<Result<TokenInfoBundle>>> {
        fun getBalance(token: Token): TokenBalance {
            return balances[token.symbol] ?: throw IllegalStateException("Error fetching ${token.symbol} balance")
        }

        return Flowable.fromIterable(supportedFATokens)
            .flatMap { getTokenInfo(it, getBalance(it), xtzInfo.exchange.currencyExchange).toFlowable() }
            // Wrap it all together
            .toList()
    }

    /**
     * - Finds the  Dexter balance/token pool of [token]
     * - Calculates the XTZ to FA1.2 exchange with the XTZ/USD
     * - Stores it locally
     *
     * @return Result so it can bundled up in a list of responses
     */
    private fun getTokenInfo(
        token: Token,
        balance: TokenBalance,
        xtzCurrencyExchange: Double
    ): Single<Result<TokenInfoBundle>> {
        return getTokenExchange(token, xtzCurrencyExchange)
            .map {
                val info = TokenInfoBundle(token, balance, it)
                tokenLocalSource.store(info)
                info
            }
            .map { Result.success(it) }
            .onErrorReturn { Result.failure(it) }
    }

    private fun getTokenExchange(token: Token, xtzCurrencyExchange: Double): Single<FATokenInfo> {
        return if (token.existsOnDexter) {
            exchangeUseCase.getTokenExchange(token, xtzCurrencyExchange)
        } else {
            // For tokens who are not in Dexter (yet) but want to show balance
            Single.just(FATokenInfo(token, 1.0, DexterBalance.empty))
        }
    }
}

fun BCDAccountToken.mapToToken(): Token {
    return Token(
        this.name ?: "N/A",
        this.symbol ?: "N/A",
        this.decimals ?: 0,
        this.contract,
        null
    )
}
