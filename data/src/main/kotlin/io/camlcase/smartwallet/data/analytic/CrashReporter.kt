/**
 * # Released under MIT License
 *
 * Copyright (c) 2021 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.analytic

import io.camlcase.kotlintezos.model.RPCErrorResponse
import io.camlcase.kotlintezos.model.TezosError
import io.camlcase.smartwallet.data.core.AppContext
import io.camlcase.smartwallet.data.core.EmitterSource
import io.camlcase.smartwallet.data.core.Logger
import io.camlcase.smartwallet.data.model.error.AppError
import io.sentry.Breadcrumb
import io.sentry.Sentry
import io.sentry.SentryEvent
import io.sentry.SentryLevel
import io.sentry.protocol.Contexts
import io.sentry.protocol.SentryException

/**
 * [See docs](https://docs.sentry.io/platforms/android/)
 * [See clean user info issue](https://github.com/getsentry/sentry-java/issues/1218)
 */
internal class CrashReporter(
    private val appContext: AppContext,
    private val sentryApiKey: String
) : Reportable {

//    init {
//        appContext.get()?.apply {
//            if (!BuildConfig.DEBUG) {
//                SentryAndroid.init(this) { options ->
//                    options.dsn = sentryApiKey
//                    /**
//                     * Remove identifying information Sentry might have recovered during the session before sending it.
//                     */
//                    options.setBeforeSend { event, _ ->
//                        event.contexts.sanitize()
//                        event.user = null
//                        event
//                    }
//                }
//            }
//        }
//    }

    override fun reportEvent(value: String, extra: String?) {
        if (extra != null) {
            Sentry.addBreadcrumb(extra)
        }
        Sentry.captureMessage(value)
    }

    override fun reportEvent(value: String, params: Map<String, String>) {
        reportEvent(value, null)
    }

    override fun addTrail(value: String) {
        val crumb = Breadcrumb(value)
        Sentry.addBreadcrumb(crumb)
    }

    override fun reportException(e: Throwable) {
        Sentry.captureException(e)
    }

    companion object {
        fun reportErrorEvent(e: Throwable) {
            if (e is TezosError) {
                reportErrorEvent(e)
            } else {
                val type = e.javaClass.simpleName
                val value = e.message ?: e.toString()
                reportErrorEvent(type, value)
            }
        }

        fun reportErrorEvent(e: TezosError, source: EmitterSource) {
            val type = e.type.name
            val value = if (e.rpcErrors.isNullOrEmpty()) {
                source.name + " : " + e.toString()
            } else {
                // Avoid adding the contract so only generic information is passed onto Sentry
                fun parseRPCErrors(list: List<RPCErrorResponse>): String {
                    var chainedErrors = ""
                    list.forEach {
                        chainedErrors += " [${it.type}, ${it.cause}, ${it.message}]"
                    }
                    return chainedErrors
                }
                parseRPCErrors(e.rpcErrors!!)
            }
            reportErrorEvent(type, value)
        }

        /**
         * Error events have a WARNING level.
         */
        private fun reportErrorEvent(type: String, value: String) {
            val exception = SentryException()
            exception.type = type
            exception.value = value
            val event = SentryEvent()
            event.level = SentryLevel.WARNING
            event.exceptions = listOf(exception)

            Sentry.captureEvent(event)
        }

        fun reportRPCError(request: String?, response: String?, errors: List<RPCErrorResponse>, url: String) {
            val event = SentryEvent()
            event.level = SentryLevel.WARNING
            val sentryException = SentryException()
            sentryException.value = "Enhanced Error Log"
            event.exceptions = listOf(sentryException)
            event.setExtras(
                mapOf(
                    "request" to (request ?: ""),
                    "response" to (response ?: ""),
                    "errors" to errors.toString(),
                    "url" to url,
                )
            )
            Sentry.captureEvent(event)
        }
    }
}

/**
 * Remove identifying "device_app_hash" and "device.id" value of Contexts entity
 */
fun Contexts.sanitize(): Contexts {
    this.app?.let { app ->
        app.deviceAppHash = null
        this.setApp(app)
    }
    this.device?.let { device ->
        device.id = null
        this.setDevice(device)
    }
    return this
}

/**
 * Send error information to reporter.
 */
fun Throwable?.report() {
    this?.apply {
//        CrashReporter.reportErrorEvent(this)
    }
}

fun Throwable?.report(source: String) {
    Logger.e("ERROR on $source: $this")
    this?.apply {
//        CrashReporter.reportErrorEvent(this)
    }
}

fun Throwable?.getCause(): String {
    return this?.let {
        when (it) {
            is AppError -> {
                it.exception?.message ?: it.type.name
            }
            is TezosError -> {
                if (it.rpcErrors?.isNotEmpty() == true) {
                    it.rpcErrors?.get(0)?.cause ?: "ERROR"
                } else {
                    it.exception?.message ?: it.type.name
                }
            }
            else -> it.message ?: it.javaClass.name
        }
    } ?: "ERROR"
}
