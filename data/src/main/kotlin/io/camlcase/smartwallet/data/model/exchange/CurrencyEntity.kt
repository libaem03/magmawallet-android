/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.model.exchange

import kotlinx.serialization.KSerializer
import kotlinx.serialization.Serializable
import kotlinx.serialization.descriptors.PrimitiveKind
import kotlinx.serialization.descriptors.PrimitiveSerialDescriptor
import kotlinx.serialization.descriptors.SerialDescriptor
import kotlinx.serialization.encoding.Decoder
import kotlinx.serialization.encoding.Encoder
import java.util.*

/**
 * @param timestamp Last updated
 */
@Serializable
data class CurrencyEntity(
    val code: String,
    @Serializable(with = CurrencyTypeSerializer::class)
    val type: CurrencyType,
    val symbol: String,
    val price: Double,
    val timestamp: Long
) {
    constructor(bundle: CurrencyBundle, price: Double) : this(
        bundle.code,
        bundle.type,
        bundle.symbol,
        price,
        Date().time
    )

    fun toBundle(): CurrencyBundle {
        return CurrencyBundle(code, type, symbol)
    }

    companion object {
        val empty = CurrencyEntity("", CurrencyType.FIAT, "", 0.0, 0L)
    }
}

enum class CurrencyType {
    FIAT,
    CRYPTO;

    companion object {
        fun get(type: String): CurrencyType {
            return values().firstOrNull { it.name.equals(type, true) } ?: FIAT
        }
    }
}

/**
 * Custom serializer for [CurrencyType]
 */
object CurrencyTypeSerializer : KSerializer<CurrencyType> {
    override val descriptor: SerialDescriptor
        get() = PrimitiveSerialDescriptor("CurrencyEntity", PrimitiveKind.STRING)

    override fun deserialize(decoder: Decoder): CurrencyType {
        return CurrencyType.valueOf(decoder.decodeString())
    }

    override fun serialize(encoder: Encoder, value: CurrencyType) {
        encoder.encodeString(value.name)
    }
}
