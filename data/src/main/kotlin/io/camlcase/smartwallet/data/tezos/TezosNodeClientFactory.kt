/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.tezos

import io.camlcase.kotlintezos.ClientWrapper
import io.camlcase.kotlintezos.TezosNodeClient
import io.camlcase.kotlintezos.data.Header
import io.camlcase.kotlintezos.network.DefaultNetworkClient
import okhttp3.OkHttpClient
import java.util.concurrent.Executors

/**
 * Bundled initialization of different Tezos clients
 */
class TezosNodeClientFactory(
    private val tezosNodeUrl: String,
    private val verifierNodeUrl: String,
    private val clientWrapper: ClientWrapper<OkHttpClient>
) {

    private val headers = emptyList<Header>()
    val client = DefaultNetworkClient(tezosNodeUrl, headers, clientWrapper)
    private val verifierClient = DefaultNetworkClient(verifierNodeUrl, headers, clientWrapper)
    private val executorService = Executors.newCachedThreadPool()

    /**
     * A [TezosNodeClient] which will need its metadata to be retrieved remotely often
     */
    fun createTezosNodeClient(): TezosNodeClient {
        return TezosNodeClient(client, verifierClient, executorService)
    }
}
