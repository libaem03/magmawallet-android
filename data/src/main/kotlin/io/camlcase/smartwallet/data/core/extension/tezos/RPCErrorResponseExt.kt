/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.core.extension.tezos

import io.camlcase.kotlintezos.core.ext.isTypeOfError
import io.camlcase.kotlintezos.core.ext.listCauses
import io.camlcase.kotlintezos.core.ext.stripUnhandledErrorCause
import io.camlcase.kotlintezos.model.RPCErrorCause
import io.camlcase.kotlintezos.model.RPCErrorResponse
import io.camlcase.kotlintezos.model.TezosError
import io.camlcase.kotlintezos.model.TezosErrorType
import io.camlcase.smartwallet.data.model.error.*

/**
 * Parses an RPC error as user-friendly as possible.
 *
 * - Strips cause from a possible verbose "Unhandled error" message. See [stripUnhandledErrorCause]
 * - Message has a user-readable error cause
 * - If nothing of the above: Show type and cause.
 */
fun RPCErrorResponse?.parseMessage(): String {
    return this?.let {
        this.stripUnhandledErrorCause()
    }
        ?: this?.message
        ?: "(${this?.type}) ${this?.cause}"
}

/**
 * Find [RPCErrorCause] errors
 *
 * @return [AppError] if found, input error if not.
 * @see [InsufficientXTZ], [InvalidAddressError]
 */
fun Throwable?.parseError(): Throwable? {
    if (this is TezosError) {
        val causes = rpcErrors.listCauses()
        when {
            this.type == TezosErrorType.COUNTER -> {
                return AppError(ErrorType.BLOCKED_OPERATION, exception = OperationInProgress())
            }
            causes.isTypeOfError(RPCErrorCause.INSUFFICIENT_XTZ_BALANCE) -> {
                return AppError(ErrorType.BLOCKED_OPERATION, exception = InsufficientXTZ())
            }
            causes.isTypeOfError(RPCErrorCause.INVALID_CONTRACT)
                    || causes.isTypeOfError(RPCErrorCause.INVALID_DELEGATE) -> {
                return AppError(ErrorType.BLOCKED_OPERATION, exception = InvalidAddressError())
            }
            causes.isTypeOfError(RPCErrorCause.BAKER_CANT_DELEGATE)
                    || causes.isTypeOfError(RPCErrorCause.INVALID_DELEGATE) -> {
                return AppError(ErrorType.BLOCKED_OPERATION, exception = BakerCantDelegate())
            }
            causes.isTypeOfError(RPCErrorCause.UNCHANGED_DELEGATED)
                    || causes.isTypeOfError(RPCErrorCause.INVALID_DELEGATE) -> {
                return AppError(ErrorType.BLOCKED_OPERATION, exception = UnchangedDelegate())
            }
            causes.isTypeOfError(RPCErrorCause.UNCHANGED_DELEGATED)
                    || causes.isTypeOfError(RPCErrorCause.INVALID_DELEGATE) -> {
                return AppError(ErrorType.BLOCKED_OPERATION, exception = UnchangedDelegate())
            }
            causes.isTypeOfError(RPCErrorCause.EXCHANGE_ZERO_TOKENS) -> {
                return AppError(ErrorType.BLOCKED_OPERATION, exception = ExchangeZeroTokens())
            }
            causes.isTypeOfError(RPCErrorCause.EXCHANGE_INVALID_SWAP) -> {
                return AppError(ErrorType.BLOCKED_OPERATION, exception = ExchangeInvalidTokens())
            }
        }
    }

    return this
}
