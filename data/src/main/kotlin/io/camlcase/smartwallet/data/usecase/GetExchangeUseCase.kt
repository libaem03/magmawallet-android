/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.usecase

import io.camlcase.kotlintezos.data.dexter.DexterPool
import io.camlcase.kotlintezos.exchange.DexterTokenClient
import io.camlcase.kotlintezos.model.Address
import io.camlcase.smartwallet.data.core.EmitterSource
import io.camlcase.smartwallet.data.core.RxEmitter
import io.camlcase.smartwallet.data.core.extension.tezos.getDexterClientOrDie
import io.camlcase.smartwallet.data.model.Token
import io.camlcase.smartwallet.data.model.exchange.DexterBalance
import io.camlcase.smartwallet.data.model.exchange.FATokenInfo
import io.camlcase.smartwallet.data.usecase.operation.DexterCalculationsUseCase
import io.reactivex.rxjava3.core.Single
import java.math.BigDecimal
import java.math.BigInteger
import java.math.RoundingMode
import java.util.*
import javax.inject.Inject

/**
 * Get Token to Currency exchange values
 */
class GetExchangeUseCase @Inject constructor(
    private val emitter: RxEmitter,
    private val dexterContractClients: LinkedHashMap<Address, DexterTokenClient>,
    private val calculationsUseCase: DexterCalculationsUseCase
) {

    /**
     * - Fetch Dexter Balance
     * - Find XTZ to FA1.2 token exchange rate
     *
     * @returns [FATokenInfo] with both information
     */
    fun getTokenExchange(token: Token, xtzCurrencyExchange: Double): Single<FATokenInfo> {
        return getTokenDexterBalance(token)
            .flatMap { calculateTokenExchange(token, it, xtzCurrencyExchange) }
    }

    private fun calculateTokenExchange(
        token: Token,
        dexterBalance: DexterBalance,
        xtzCurrencyExchange: Double
    ): Single<FATokenInfo> {
        // Market rate
        return if (dexterBalance.tokens == BigInteger.ZERO) {
            Single.just(BigDecimal.ZERO)
        } else {
            calculationsUseCase.getTokenToXtzMarketRate(token, dexterBalance)
        }.map { marketRate ->
            val tokenCurrencyExchange = BigDecimal(xtzCurrencyExchange).multiply(marketRate)
                .setScale(scale, RoundingMode.DOWN)

            FATokenInfo(
                token,
                tokenCurrencyExchange.toDouble(),
                dexterBalance
            )
        }
    }

    /**
     * Fetch Dexter xtz/token pool for a [token]
     */
    private fun getTokenDexterBalance(token: Token): Single<DexterBalance> {
        return dexterContractClients.getDexterClientOrDie<Single<DexterBalance>>(token.dexterAddress) { client ->
            Single.create<DexterPool> {
                client.getLiquidityPool(emitter.emitCallback(it, EmitterSource.DEXTER_POOL))
            }.map { DexterBalance(it.xtzPool, it.tokenPool) }
        }
    }

    companion object {
        private const val scale = 6
    }
}
