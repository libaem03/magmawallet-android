/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.usecase.migration

import io.camlcase.kotlintezos.TezosNodeClient
import io.camlcase.kotlintezos.model.Address
import io.camlcase.kotlintezos.model.Tez
import io.camlcase.kotlintezos.model.operation.*
import io.camlcase.kotlintezos.model.operation.fees.CalculatedFees
import io.camlcase.kotlintezos.model.operation.fees.OperationFees
import io.camlcase.kotlintezos.operation.OperationFeesPolicy
import io.camlcase.kotlintezos.operation.RevealService
import io.camlcase.kotlintezos.wallet.PublicKey
import io.camlcase.kotlintezos.wallet.SimulatedSignatureProvider
import io.camlcase.kotlintezos.wallet.Wallet
import io.camlcase.smartwallet.data.core.EmitterSource
import io.camlcase.smartwallet.data.core.Migration
import io.camlcase.smartwallet.data.core.RxEmitter
import io.camlcase.smartwallet.data.core.extension.singleObserve
import io.camlcase.smartwallet.data.core.extension.tezos.toBigDecimal
import io.camlcase.smartwallet.data.model.Token
import io.camlcase.smartwallet.data.model.error.AppError
import io.camlcase.smartwallet.data.model.error.ErrorType
import io.camlcase.smartwallet.data.model.error.InsufficientXTZ
import io.camlcase.smartwallet.data.source.local.UserPreferences
import io.camlcase.smartwallet.data.source.user.WalletLocalSource
import io.camlcase.smartwallet.data.usecase.OperationUseCase
import io.camlcase.smartwallet.data.usecase.operation.SendType.Companion.createSendTokenOperation
import io.reactivex.rxjava3.annotations.NonNull
import io.reactivex.rxjava3.core.Single
import java.math.BigDecimal
import java.math.BigInteger
import javax.inject.Inject
import javax.inject.Named

class MigrateFundsUseCase @Inject constructor(
    override val emitter: RxEmitter,
    override val walletLocalSource: WalletLocalSource,
    override val tezosNodeClient: TezosNodeClient,
    private val userPreferences: UserPreferences,
    @Named("SupportedFATokens")
    private val supportedFATokens: List<Token>,
    private val revealService: RevealService
) : OperationUseCase {

    /**
     * @throws InsufficientXTZ If not enough XTZ to pay
     * @throws AppError NO_WALLET if getMigrationWallet was not created
     */
    fun calculateFees(): Single<Tez> {
        val tezBalance = userPreferences.getTezBalance()
        if (tezBalance <= Migration.XTZ_BUFFER) {
            return Single.error(InsufficientXTZ())
        }
        walletLocalSource.getMigrationWallet()?.address?.let { newWalletAddress ->
            val (oldAddress, tokenBalances, delegate) = extractData()
            val bufferedXTZBalance = tezBalance - Migration.XTZ_BUFFER_SUBSTRACT
//            Logger.i("calculateFees: $tezBalance - ${Migration.XTZ_BUFFER_SUBSTRACT} = $bufferedXTZBalance")

            return walletLocalSource.getWalletOrDie()
                .flatMap { appWallet ->
                    createOperations(oldAddress, newWalletAddress, bufferedXTZBalance, tokenBalances, delegate)
                        .flatMap { calculateFees(appWallet.wallet, it) }
                        .map { getTotalFees(it) }

                }
        }
        return Single.error(AppError(ErrorType.NO_WALLET))
    }

    private fun extractData(): Triple<Address, Map<String, BigInteger>, Address?> {
        val user = userPreferences.getUserAddress()
        val tokenBalances: Map<String, BigInteger> = userPreferences.getTokenBalance()
        return Triple(user.address, tokenBalances, userPreferences.getString(UserPreferences.DELEGATE_ADDRESS))
    }

    /**
     * @throws AppError NO_WALLET if getMigrationWallet was not created
     */
    fun transferFunds(totalFee: Tez): Single<String> {
        walletLocalSource.getMigrationWallet()?.address?.let { newWalletAddress ->
            return walletLocalSource.getWalletOrDie()
                .flatMap { appWallet ->
                    val (oldAddress, tokenBalances, delegate) = extractData()
                    val tezBalance = userPreferences.getTezBalance()

                    val xtzToSend = substractTezValues(totalFee, tezBalance)
                    createOperations(oldAddress, newWalletAddress, xtzToSend, tokenBalances, delegate)
                        .flatMap { transferFunds(appWallet.wallet, it) }
                }
        }
        return Single.error(AppError(ErrorType.NO_WALLET))
    }

    private fun createOperations(
        source: Address,
        destination: Address,
        tezBalance: Tez,
        tokenBalances: Map<String, BigInteger>,
        delegate: Address?
    ): Single<List<Operation>> {
        val operations: ArrayList<Operation> = ArrayList()
        delegate?.apply {
            operations.add(DelegationOperation(source, null, OperationFees.simulationFees))
        }
        operations.addAll(getTokenTransferOperations(source, destination, tokenBalances))
        getXTZTransferOperation(source, destination, tezBalance)?.apply {
            operations.add(this)
        }
        return revealService.needsReveal(source, operations).singleObserve()
            .map {
                if (it) {
                    val publicKey: PublicKey =
                        walletLocalSource.getPublicKey() ?: SimulatedSignatureProvider().publicKey
                    operations.add(
                        0,
                        RevealOperation(
                            source,
                            publicKey,
                            OperationFees.simulationFees
                        )
                    )
                }
                operations
            }
    }

    private fun calculateFees(
        wallet: Wallet,
        operations: List<Operation>,
    ): @NonNull Single<CalculatedFees> {
        return Single.create<CalculatedFees> {
            tezosNodeClient.calculateFees(
                operations,
                wallet.mainAddress,
                wallet,
                OperationFeesPolicy.Estimate(),
                callback = emitter.emitCallback(it, EmitterSource.CALCULATE_MIGRATION_FEES)
            )
        }
    }

    private fun getTotalFees(calculatedFees: CalculatedFees): Tez {
        var extraFee = Tez.zero
        calculatedFees.accumulatedFees.extraFees?.asList?.map {
            extraFee += it.fee
        }
        return calculatedFees.accumulatedFees.fee + extraFee
    }

    private fun substractTezValues(totalFee: Tez, tezBalance: Tez): Tez {
        if (totalFee > tezBalance) {
            throw InsufficientXTZ()
        }
        val leftOverXTZ = tezBalance - totalFee
//        Logger.i("substractTezValues: $tezBalance - $totalFee = $leftOverXTZ")
        return leftOverXTZ
    }

    /**
     * Simple transaction operation
     * @return TransactionOperation if enough balance, null if not
     */
    private fun getXTZTransferOperation(
        source: Address,
        destination: Address,
        amount: Tez
    ): TransactionOperation? {
        val finalAmount = amount.toBigDecimal()
        return if (finalAmount > BigDecimal.ZERO) {
            TransactionOperation(
                Tez(finalAmount),
                source,
                destination,
                OperationFees.simulationFees
            )
        } else {
            // Nothing to send
            null
        }
    }

    /**
     * @return List of operations needed to send Tokens. If balance == 0, the token is skipped and the list is empty.
     */
    private fun getTokenTransferOperations(
        address: Address,
        destination: Address,
        balances: Map<String, BigInteger>
    ): List<SmartContractCallOperation> {
        val list: ArrayList<SmartContractCallOperation> = ArrayList()
        for (entry in balances) {
            // Don't transfer if there's no balance
            val balance = entry.value
            val token = supportedFATokens.firstOrNull { it.symbol == entry.key }
            val validToken = token != null && !token.contractAddress.isNullOrEmpty()

            if (balance > BigInteger.ZERO && validToken) {
                createSendTokenOperation(
                    token!!,
                    balance,
                    address,
                    destination

                )?.apply {
                    list.addAll(this)
                }
            }
        }
        return list
    }

    /**
     * Send a list of operations for transfering funds
     * @see transferFunds
     */
    private fun transferFunds(
        wallet: Wallet,
        operations: List<Operation>
    ): Single<String> {
        return Single.create<String> {
            tezosNodeClient.runOperations(
                operations,
                wallet.mainAddress,
                wallet,
                OperationFeesPolicy.Estimate(),
                callback = emitter.emitCallback(it, EmitterSource.MIGRATION_TRANSFER)
            )
        }
    }
}
