/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.source.local

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import io.camlcase.kotlintezos.model.Tez
import io.camlcase.smartwallet.data.analytic.report
import io.camlcase.smartwallet.data.core.AppContext
import io.camlcase.smartwallet.data.model.Token
import io.camlcase.smartwallet.data.model.UserTezosAddress
import io.camlcase.smartwallet.data.model.exchange.CoinGeckoRate
import io.camlcase.smartwallet.data.model.money.MoonPayCurrency
import io.camlcase.smartwallet.data.model.money.MoonPaySupportedCountry
import kotlinx.serialization.builtins.ListSerializer
import kotlinx.serialization.builtins.MapSerializer
import kotlinx.serialization.builtins.serializer
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import java.math.BigInteger

/**
 * Android SharedPreferences for user info that is read sparingly.
 */
class UserPreferences(
    override val context: AppContext,
    override val fileName: String = "preferences"
) : Preferences {
    override val preferences: SharedPreferences?
        get() = context.get()?.getSharedPreferences(fileName, Context.MODE_PRIVATE)

    fun userLoggedIn(): Boolean {
        val address = preferences?.getString(WALLET_PUBLIC_ADDRESS, null)
        return !address.isNullOrBlank()
    }

    /**
     * Don't use it to check if user logged in! Will return a placeholder UserTezosAddress (for streams) if not stored.
     * @see [userLoggedIn]
     */
    fun getUserAddress(): UserTezosAddress {
        return preferences?.getString(WALLET_PUBLIC_ADDRESS, null)?.let {
            UserTezosAddress.deserialize(it)
        } ?: UserTezosAddress.empty
    }

    fun storeTezBalance(balance: Tez) {
        store(BALANCE_TEZ, balance.stringRepresentation)
    }

    fun getTezBalance(): Tez {
        return preferences?.getString(BALANCE_TEZ, null)?.let {
            Tez(it)
        } ?: Tez.zero
    }

    fun storeTokenBalance(token: Token, balance: BigInteger) {
        val balanceMap = getTokenBalance().toMutableMap()
        balanceMap[token.symbol] = balance
        store(BALANCE_TOKENS, Gson().toJson(balanceMap))
    }

    /**
     * @return Map of <Token Symbol, Integer (no decimals) value of balance>
     */
    fun getTokenBalance(): Map<String, BigInteger> {
        var balanceMap: HashMap<String, BigInteger> = HashMap()
        preferences?.getString(BALANCE_TOKENS, null)?.let {
            try {
                balanceMap = Gson().fromJson(it, object : TypeToken<HashMap<String, BigInteger>>() {}.type)
            } catch (e: Exception) {
                // Nothing to do, just save a new map
                e.report("getTokenBalance")
            }
        }
        return balanceMap
    }

    fun storeManagerBalance(balance: Tez) {
        store(BALANCE_MANAGER_TEZ, balance.stringRepresentation)
    }

    /**
     * @return null if no KT1 originated
     */
    fun getManagerBalance(): Tez? {
        return preferences?.getString(BALANCE_MANAGER_TEZ, null)?.let {
            Tez(it)
        }
    }

    /**
     * @returns null if limit has not been set, 0 if it's unlimited, >0 if it's limited
     */
    fun getWithdrawalLimit(): Tez? {
        return preferences?.getString(WITHDRAWAL_LIMIT, null)?.let { Tez(it) }
    }

    fun updateVersionCreated(versionCode: Int) {
        store(WALLET_VERSION_CREATED, versionCode)
    }

    fun versionCreated(): Int {
        return getInt(WALLET_VERSION_CREATED)
    }

    fun getMoonPaySupportedCountries(): List<MoonPaySupportedCountry>? {
        return getString(MOONPAY_COUNTRIES)?.let {
            Json.decodeFromString(ListSerializer(MoonPaySupportedCountry.serializer()), it)
        }
    }

    fun updateMoonPaySupportedCountries(list: List<MoonPaySupportedCountry>) {
        val serialisedList = Json.encodeToString(ListSerializer(MoonPaySupportedCountry.serializer()), list)
        store(MOONPAY_COUNTRIES, serialisedList)
    }

    fun updateMoonPayXTZCurrency(xtz: MoonPayCurrency) {
        val serialised = Json.encodeToString(MoonPayCurrency.serializer(), xtz)
        store(MOONPAY_XTZ_CURRENCY, serialised)
    }

    fun getMoonPayXTZCurrency(): MoonPayCurrency? {
        return getString(MOONPAY_XTZ_CURRENCY)?.let {
            Json.decodeFromString(it)
        }
    }

    fun updateAvailableCurrencies(value: Map<String, CoinGeckoRate>) {
        val serialisedList = Json.encodeToString(MapSerializer(String.serializer(), CoinGeckoRate.serializer()), value)
        store(COINGECKO_RATES, serialisedList)
    }

    /**
     * @return Map <currency code, rate object>
     */
    fun getAvailableCurrencies(): Map<String, CoinGeckoRate>? {
        return getString(COINGECKO_RATES)?.let {
            Json.decodeFromString(MapSerializer(String.serializer(), CoinGeckoRate.serializer()), it)
        }
    }

    companion object {
        const val WALLET_PUBLIC_ADDRESS = "user.wallet.address"  // UserTezosAddress serialized
        const val WALLET_VERSION_CREATED = "user.wallet.version_created"  // Version the wallet was created
        const val BALANCE_TOKENS = "user.balance.tokens"  // List of Token balances
        const val BALANCE_TEZ = "user.balance.tez"  // String representation of Tez
        const val BALANCE_MANAGER_TEZ = "user.balance.manager.tez"  // If KT1 originated, manager's balance
        const val DELEGATE_ADDRESS = "user.delegate.address"  // String tezos address
        const val WITHDRAWAL_LIMIT = "user.withdrawal_limit"  // String tezos value
        const val CURRENCY_EXCHANGE = "user.currency" // Currency class
        const val COINGECKO_RATES = "api.coingecko.rates" // Available currencies
        const val MOONPAY_COUNTRIES = "user.moonpay.supported_countries" // To show buy button
        const val MOONPAY_XTZ_CURRENCY = "user.moonpay.xtz_currency" // To show buy button

        const val BLOCKCHAIN_COUNTER = "user.blockchain.counter"
    }
}

