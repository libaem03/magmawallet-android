/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.source.info.exchange

import io.camlcase.kotlintezos.data.Parser
import io.camlcase.kotlintezos.data.RPC
import io.camlcase.smartwallet.data.core.extension.toLowerCaseUS
import io.camlcase.smartwallet.data.model.exchange.CoinGeckoRate
import io.camlcase.smartwallet.data.model.exchange.CoinGeckoRates
import io.camlcase.smartwallet.data.source.RemoteSource
import io.camlcase.smartwallet.data.source.info.exchange.CoinGeckoRemoteSource.Companion.COINGECKO_API_VERSION
import io.camlcase.smartwallet.data.source.info.exchange.CoinGeckoRemoteSource.Companion.COINGECKO_EXCHANGE_RATES_ENDPOINT
import io.camlcase.smartwallet.data.source.info.exchange.CoinGeckoRemoteSource.Companion.COINGECKO_PRICE_ENDPOINT
import io.reactivex.rxjava3.core.Single
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.jsonObject
import kotlinx.serialization.json.jsonPrimitive

/**
 * @see DataSourceModel
 */
class CoinGeckoRemoteSource(
    debug: Boolean = false
) : RemoteSource(COINGECKO_URL, null, debug) {

    fun getXTZPriceFor(currencyCode: String): Single<Double> {
        return Single.fromFuture(networkClient.send(GetXTZPrice(currencyCode)))
    }

    fun getBTCToCurrencyRates(): Single<Map<String, CoinGeckoRate>> {
        return Single.fromFuture(networkClient.send(GetBTCtoCurrencyRates()))
    }

    companion object {
        internal const val COINGECKO_URL = "https://api.coingecko.com/api"
        internal const val COINGECKO_API_VERSION = "/v3"
        internal const val COINGECKO_PRICE_ENDPOINT = "/simple/price?ids=tezos&vs_currencies="
        internal const val COINGECKO_EXCHANGE_RATES_ENDPOINT = "/exchange_rates"
    }
}

class GetXTZPrice(
    currencyCode: String
) : RPC<Double>(
    endpoint = COINGECKO_API_VERSION + COINGECKO_PRICE_ENDPOINT + currencyCode.toLowerCaseUS(),
    header = listOf(RemoteSource.headerAcceptJson),
    parser = CoinGeckoPriceParser(currencyCode.toLowerCaseUS())
)

class CoinGeckoPriceParser(
    private val currencyCode: String
) : Parser<Double> {
    val format = Json {
        isLenient = true
        ignoreUnknownKeys = true
    }

    override fun parse(jsonData: String): Double {
        val parseToJsonElement = format.parseToJsonElement(jsonData)
        val jsonElement = parseToJsonElement.jsonObject["tezos"]?.jsonObject?.get(currencyCode)
        return jsonElement?.jsonPrimitive?.content?.toDouble() ?: 1.0
    }
}

/**
 * Get BTC-to-Currency exchange rates
 */
internal class GetBTCtoCurrencyRates : RPC<Map<String, CoinGeckoRate>>(
    endpoint = COINGECKO_API_VERSION + COINGECKO_EXCHANGE_RATES_ENDPOINT,
    header = listOf(RemoteSource.headerAcceptJson),
    parser = CoinGeckoRatesParser()
)

class CoinGeckoRatesParser : Parser<Map<String, CoinGeckoRate>> {
    val format = Json {
        isLenient = true
        ignoreUnknownKeys = true
    }

    override fun parse(jsonData: String): Map<String, CoinGeckoRate> {
        return format.decodeFromString(CoinGeckoRates.serializer(), jsonData).rates
    }
}
