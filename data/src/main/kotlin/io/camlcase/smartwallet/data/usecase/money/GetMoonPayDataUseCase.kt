/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.usecase.money

import io.camlcase.smartwallet.data.core.XTZ
import io.camlcase.smartwallet.data.model.error.AppError
import io.camlcase.smartwallet.data.model.error.ErrorType
import io.camlcase.smartwallet.data.model.money.MoonPayCurrency
import io.camlcase.smartwallet.data.model.money.MoonPaySupportedCountry
import io.camlcase.smartwallet.data.source.local.UserPreferences
import io.camlcase.smartwallet.data.source.money.MoonPayRemoteSource
import io.camlcase.smartwallet.data.usecase.user.ShouldUpdateUseCase
import io.reactivex.rxjava3.core.Single
import java.util.*
import javax.inject.Inject

class GetMoonPayDataUseCase @Inject constructor(
    private val remoteSource: MoonPayRemoteSource,
    private val prefsUpdater: ShouldUpdateUseCase,
    private val userPreferences: UserPreferences
) {

    fun updateMoonPayXTZData(): Single<Boolean> {
        val shouldUpdate = prefsUpdater.shouldUpdateMoonPaySupportedCountries()
        val localList = userPreferences.getMoonPaySupportedCountries()
        if (localList == null || shouldUpdate) {
            return Single.zip(
                updateMoonPaySupportedCountries(),
                updatetMoonPayXTZ(),
                { _, _ -> true }
            )
        }
        return Single.just(false)
    }

    /**
     * Check our saved moonpay data (xtz currency & supported countries) against the user's country code and see if
     * xtz are available for buying.
     */
    fun moonPaySupported(countryCode: String): Boolean {
        val moonpayXtz = userPreferences.getMoonPayXTZCurrency()
        return if (countryCode == Locale.US.country) {
            moonpayXtz?.isSupportedInUS == true
        } else {
            val moonpayCountry = userPreferences.getMoonPaySupportedCountries()
                ?.firstOrNull { it.alpha2 == countryCode || it.alpha3 == countryCode }
            moonpayCountry?.isBuyAllowed == true
        }
    }

    private fun updatetMoonPayXTZ(): Single<MoonPayCurrency> {
        return remoteSource.getSupportedCurrencies()
            .map { list ->
                val xtz = list.firstOrNull { it.code == XTZ.symbol.toLowerCase(Locale.US) }
                    ?: throw AppError(ErrorType.INVALID_RESULT, "MoonPay didn't return XTZ")
                userPreferences.updateMoonPayXTZCurrency(xtz)
                xtz
            }
    }

    private fun updateMoonPaySupportedCountries(): Single<List<MoonPaySupportedCountry>> {
        return remoteSource.getSupportedCountries()
            .map {
                userPreferences.updateMoonPaySupportedCountries(it)
                prefsUpdater.updateMoonPaySupportedCountriesTimestamp()
                it
            }
    }
}
