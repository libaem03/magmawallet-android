/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.model.amount

import android.os.Parcelable
import io.camlcase.kotlintezos.model.Tez
import io.camlcase.smartwallet.data.model.Token
import java.math.BigDecimal

/**
 * Value of a user's amount of T
 */
interface Balance<T> : Comparable<T>, Parcelable {
    val value: T
    val formattedValue: String
    val isZero: Boolean
    val bigDecimal: BigDecimal

    fun inCurrency(currencyPrice: Double): BigDecimal

    /**
     * User friendly formatted representation of the amount of token
     *
     * @param token So we can show its symbol
     */
    fun format(token: Token): String
    fun compareTo(double: Double): Int
    fun compareTo(other: Tez): Int
    operator fun minus(other: T): T
    operator fun plus(other: T): T
}

fun Balance<*>.inverseWith(value: BigDecimal, decimals: Int): Balance<*> {
    return when (this) {
        is CoinBalance -> TokenBalance(
            value,
            decimals
        )
        is TokenBalance -> CoinBalance(
            Tez(value)
        )
        else -> EmptyBalance()
    }
}

fun Balance<*>.isEqualTo(other: Balance<*>): Boolean {
    return this.compareTo(other) == 0
}

fun Balance<*>.compareTo(other: Balance<*>): Int {
    return this.bigDecimal.compareTo(other.bigDecimal)
}
