/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.model.operation

import io.camlcase.kotlintezos.model.Tez
import io.camlcase.kotlintezos.model.conseil.ConseilOrigination
import io.camlcase.kotlintezos.model.tzkt.*
import io.camlcase.kotlintezos.model.tzkt.dto.TzKtAlias
import io.camlcase.smartwallet.data.core.XTZ
import io.camlcase.smartwallet.data.model.Token
import io.camlcase.smartwallet.data.model.amount.Balance
import io.camlcase.smartwallet.data.model.amount.CoinBalance

/**
 * User is source
 */
data class SendOperation(
    override val token: Token,
    override val tzKtOperations: List<TzKtOperation>,
    override val destination: TzKtAlias,
    override val amount: Balance<*>
) : TransferOperation {
    override val type: TezosOperation.Type = TezosOperation.Type.SEND
    override val fees: Tez? = tzKtOperations.groupFees()

    constructor(
        token: Token,
        tzKtOperation: TzKtTransaction,
        destination: TzKtAlias = tzKtOperation.destination,
        amount: Balance<*> = CoinBalance(tzKtOperation.amount)
    ) : this(token, listOf(tzKtOperation), destination, amount)
}

/**
 * User is destination
 */
data class ReceiveOperation(
    override val token: Token,
    override val tzKtOperations: List<TzKtOperation>,
    override val destination: TzKtAlias,
    override val amount: Balance<*>
) : TransferOperation {
    override val type: TezosOperation.Type = TezosOperation.Type.RECEIVE
    override val fees: Tez? = null

    constructor(
        token: Token,
        tzKtOperation: TzKtTransaction,
        destination: TzKtAlias = tzKtOperation.sender,
        amount: Balance<*> = CoinBalance(tzKtOperation.amount)
    ) : this(token, listOf(tzKtOperation), destination, amount)
}

/**
 * XTZ ⟷ FA1.2 Tokens
 */
data class SwapOperation(
    val tokenFrom: Token,
    override val token: Token, // Token TO
    val amountFrom: Balance<*>,
    val amountTo: Balance<*>,
    override val tzKtOperations: List<TzKtOperation>
) : TezosOperation {
    override val type: TezosOperation.Type = TezosOperation.Type.SWAP
    override val fees: Tez? = tzKtOperations.groupFees()
}

/**
 * User delegates baking to a [delegate]
 *
 * @param delegate Null if it's an undelegation
 */
data class DelegateOperation(
    val delegate: TzKtAlias?,
    val tzKtOperation: TzKtDelegation
) : TezosOperation {
    override val token: Token = XTZ
    override val type: TezosOperation.Type = TezosOperation.Type.DELEGATE
    override val tzKtOperations: List<TzKtOperation> = listOf(tzKtOperation)
    override val fees: Tez? = tzKtOperations.groupFees()
}

/**
 * User has originated a smart contract.
 * Contract address is in [ConseilOrigination] in [tzKtOperations]
 */
data class OriginateOperation(
    val tzKtOperation: TzKtOrigination
) : TezosOperation {
    override val token: Token = XTZ
    override val type: TezosOperation.Type = TezosOperation.Type.ORIGINATION
    override val tzKtOperations: List<TzKtOperation> = listOf(tzKtOperation)
    override val fees: Tez? = tzKtOperations.groupFees()
}

/**
 * User has revealed their address. No public key to show.
 */
data class RevealOperation(
    val tzKtOperation: TzKtReveal
) : TezosOperation {
    override val token: Token = XTZ
    override val type: TezosOperation.Type = TezosOperation.Type.REVEAL
    override val tzKtOperations: List<TzKtOperation> = listOf(tzKtOperation)
    override val fees: Tez? = tzKtOperations.groupFees()
}

/**
 * Smart contract transactions that are not supported in Magma
 */
data class UnknownOperation(
    override val tzKtOperations: List<TzKtOperation>
) : TezosOperation {
    override val token: Token = XTZ
    override val type: TezosOperation.Type = TezosOperation.Type.UNKNOWN
    override val fees: Tez? = tzKtOperations.groupFees()
}
