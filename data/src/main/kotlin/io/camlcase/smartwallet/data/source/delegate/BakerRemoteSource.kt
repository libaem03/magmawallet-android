/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.source.delegate

import io.camlcase.kotlintezos.data.Parser
import io.camlcase.kotlintezos.data.RPC
import io.camlcase.smartwallet.data.analytic.report
import io.camlcase.smartwallet.data.core.Logger
import io.camlcase.smartwallet.data.model.delegate.TezosBakerResponse
import io.camlcase.smartwallet.data.source.RemoteSource
import io.camlcase.smartwallet.data.source.RemoteSource.Companion.headerAcceptJson
import io.reactivex.rxjava3.core.Single
import kotlinx.serialization.builtins.ListSerializer
import kotlinx.serialization.json.Json

/**
 * Tezos bakers information
 */
class BakerRemoteSource(
    debug: Boolean
) : RemoteSource("https://api.baking-bad.org/v2", null, debug) {
    fun getListOfSupportedDelegates(): Single<List<TezosBakerResponse>> {
        val rpc = GetTezosBakersRPC()
        return Single.fromFuture(networkClient.send(rpc))
    }
}

private class GetTezosBakersRPC : RPC<List<TezosBakerResponse>>(
    endpoint = LIST_ENDPOINT + FILTERS,
    header = listOf(headerAcceptJson),
    parser = BakerListParser()
) {
    companion object {
        private const val LIST_ENDPOINT = "/bakers"
        private const val FILTERS = "?accuracy=precise&timing=stable&health=active"
    }
}

internal class BakerListParser : Parser<List<TezosBakerResponse>> {
    val format = Json {
        isLenient = true
        ignoreUnknownKeys = true
    }

    override fun parse(jsonData: String): List<TezosBakerResponse> {
        try {
            return format.decodeFromString(ListSerializer(TezosBakerResponse.serializer()), jsonData)
        } catch (e: Exception) {
            e.printStackTrace()
            Logger.e("*** ERROR: Parsing $jsonData\n$e")
            e.report()
        }
        return emptyList()
    }
}
