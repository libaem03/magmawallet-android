/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.model.amount

import android.os.Parcel
import android.os.Parcelable
import io.camlcase.kotlintezos.model.Tez
import io.camlcase.smartwallet.data.core.extension.formatAsToken
import io.camlcase.smartwallet.data.model.Token
import io.camlcase.smartwallet.data.model.exchange.moveComma
import java.math.BigDecimal
import java.math.BigInteger
import java.math.MathContext
import java.math.RoundingMode
import kotlin.math.pow

/**
 * FA1.2 tokens are arbitrary precision, 0 to infinity, never negative
 */
data class TokenBalance(
    override val value: BigDecimal,
    val decimals: Int
) : Balance<BigDecimal> {
    constructor(balance: BigInteger, decimals: Int) : this(
        balance.moveComma(decimals),
        decimals
    )

    constructor(balance: Double, decimals: Int) : this(
        BigDecimal.valueOf(balance),
        decimals
    )

    override val formattedValue: String
        get() = if (decimals > 0) {
            value.formatAsToken(decimals)
        } else {
            value.toBigInteger().formatAsToken()
        }

    val rpcRepresentation: BigInteger = if (decimals > 0) {
        value.multiply(BigDecimal(10.0.pow(decimals)), MathContext(0, RoundingMode.HALF_DOWN)).toBigInteger()
    } else {
        value.toBigInteger()
    }

    override val isZero: Boolean = value.compareTo(BigDecimal.ZERO) == 0

    override fun format(token: Token): String {
        return value.formatAsToken(token)
    }

    override fun inCurrency(currencyPrice: Double): BigDecimal {
        return value * currencyPrice.toBigDecimal()
    }

    override fun compareTo(other: BigDecimal): Int {
        return value.compareTo(other)
    }

    override fun compareTo(double: Double): Int {
        return value.compareTo(double.toBigDecimal())
    }

    override fun compareTo(other: Tez): Int {
        return value.compareTo(other.signedDecimalRepresentation)
    }

    override val bigDecimal: BigDecimal
        get() = value

    constructor(parcel: Parcel) : this(
        BigDecimal(parcel.readString()),
        parcel.readInt()
    )

    override fun plus(other: BigDecimal): BigDecimal {
        return value.add(other)
    }

    override fun minus(other: BigDecimal): BigDecimal {
        return value.subtract(other).abs()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(value.toPlainString())
        parcel.writeInt(decimals)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<TokenBalance> {
        override fun createFromParcel(parcel: Parcel): TokenBalance {
            return TokenBalance(parcel)
        }

        override fun newArray(size: Int): Array<TokenBalance?> {
            return arrayOfNulls(size)
        }
    }
}
