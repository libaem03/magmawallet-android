/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.core.api

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import dagger.Reusable
import io.camlcase.smartwallet.data.core.AppContext
import io.camlcase.smartwallet.data.core.api.ConnectionInterceptor.NoConnectivityException
import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException
import java.net.InetSocketAddress
import java.net.Socket
import javax.inject.Inject

/**
 * OkHttp Interceptor to run before calls and ensure we have connection.
 *
 * @throws NoConnectivityException If connectivity could not be detected
 * (IOException, the only type OkHttp won't crash with)
 * @see https://medium.com/@elye.project/android-intercept-on-no-internet-connection-acb91d305357
 */
@Reusable
class ConnectionInterceptor @Inject constructor(
    private val appContext: AppContext
) : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        return if (!isConnectionOn()) {
            throw NoConnectivityException()
        } else {
            chain.proceed(chain.request())
        }
    }

    /**
     * Check if either WIFI or CELLULLAR DATA is ON
     */
    private fun isConnectionOn(): Boolean {
        return appContext.get()?.let { context ->
            val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

            val network = connectivityManager.activeNetwork
            val connection = connectivityManager.getNetworkCapabilities(network)

            return connection?.let {
                it.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)
                        || it.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)
            } ?: false
        } ?: false
    }

    /**
     * Even if WIFI/CELLULLAR is ON, we might not have internet.
     * Perform a quick special connection to Google Public DNS (i.e. 8.8.8.8) to check.
     */
    @Deprecated("Slow connections trigger this false")
    private fun isInternetAvailable(): Boolean {
        return try {
            val timeoutMs = 1500
            val sock = Socket()
            val sockaddr = InetSocketAddress("8.8.8.8", 53)

            sock.connect(sockaddr, timeoutMs)
            sock.close()

            true
        } catch (e: IOException) {
            false
        }
    }

    class NoConnectivityException : IOException() {
        override val message: String
            get() = "No network available"
    }

    class NoInternetException : IOException() {
        override val message: String
            get() = "No internet available"
    }
}
