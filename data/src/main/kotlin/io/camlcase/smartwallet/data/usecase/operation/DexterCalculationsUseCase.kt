/**
 * # Released under MIT License
 *
 * Copyright (c) 2021 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.usecase.operation

import dagger.Reusable
import io.camlcase.kotlintezos.exchange.calculations.AndroidJSDexterCalculations
import io.camlcase.kotlintezos.model.Tez
import io.camlcase.smartwallet.data.core.DataRxThread.singleThread
import io.camlcase.smartwallet.data.core.XTZ
import io.camlcase.smartwallet.data.model.Token
import io.camlcase.smartwallet.data.model.amount.*
import io.camlcase.smartwallet.data.model.exchange.*
import io.camlcase.smartwallet.data.model.isXTZ
import io.reactivex.rxjava3.core.Single
import java.math.BigDecimal
import java.math.BigInteger
import javax.inject.Inject
import kotlin.math.absoluteValue

/**
 * Use the same instance of the usecase so [ApplicationObserver] is responsible of calling [init] and [close].
 * This way we ensure the JS file is loaded once per session and all calls are made using the same object.
 *
 * IMPORTANT: The library used doesn't support multithreading, so all calls to it must be done in [singleThread].
 */
@Reusable
class DexterCalculationsUseCase @Inject constructor(
    private val calculator: AndroidJSDexterCalculations,
) {

    fun init(): Single<Unit> {
        return Single.fromCallable {
            calculator.initReading()
        }.subscribeOn(singleThread())
    }

    fun close(): Single<Unit> {
        return Single.fromCallable {
            calculator.closeReading()
        }.subscribeOn(singleThread())
    }

    fun calculate(
        fromToken: TokenInfoBundle,
        toToken: TokenInfoBundle,
        amountFrom: Balance<*>
    ): Single<DexterExchange> {
        return Single.fromCallable {
            if (fromToken.token.isXTZ()) {
                // XTZ -> FA1.2 Token
                calculateXTZToToken(amountFrom as CoinBalance, toToken.exchange as FATokenInfo)
            } else {
                // FA1.2 Token -> XTZ
                calculateTokenToXTZ(amountFrom as TokenBalance, fromToken.exchange as FATokenInfo)
            }
        }.subscribeOn(singleThread())
    }

    fun calculate(fromToken: TokenInfoBundle, toToken: TokenInfoBundle, amount: BigDecimal): Single<DexterExchange> {
        return Single.fromCallable {
            when (fromToken.balance) {
                is TokenBalance -> TokenBalance(amount, fromToken.token.decimals)
                is CoinBalance -> CoinBalance(Tez(amount))
                else -> EmptyBalance()
            }
        }.flatMap {
            calculate(fromToken, toToken, it)
        }.subscribeOn(singleThread())
    }

    private fun calculateXTZToToken(amountFrom: CoinBalance, tokenInfo: FATokenInfo): DexterExchange {
        val tokenPool = tokenInfo.dexterBalance.tokens
        val tezPool = tokenInfo.dexterBalance.balance
        val amount = amountFrom.value

        val valueToBuy: BigInteger = calculator.xtzToTokenTokenOutput(amount, tezPool, tokenPool)
        val amountTo = TokenBalance(valueToBuy, tokenInfo.token.decimals)
        val conversionRate =
            calculator.xtzToTokenExchangeRate(amount, xtzPool = tezPool, tokenPool = tokenPool)
                .scaleDownToToken(XTZ.decimals, tokenInfo.token.decimals)
        val liquidityFee = amount.signedDecimalRepresentation.multiply(liquidityFee)
        return DexterExchange(
            amountFrom.bigDecimal,
            amountTo.value,
            liquidityFee,
            conversionRate,
            amountFrom,
            amountTo
        )
    }

    private fun calculateTokenToXTZ(amountFrom: TokenBalance, tokenInfo: FATokenInfo): DexterExchange {
        val tokenPool = tokenInfo.dexterBalance.tokens
        val tezPool = tokenInfo.dexterBalance.balance
        val amount = amountFrom.value

        val valueToBuyInTez = calculator.tokenToXtzXtzOutput(amountFrom.rpcRepresentation, tezPool, tokenPool)
        val valueToBuy = valueToBuyInTez.signedDecimalRepresentation

        val amountTo = CoinBalance(valueToBuyInTez)
        val conversionRate = calculator.tokenToXtzExchangeRate(
            amountFrom.rpcRepresentation,
            xtzPool = tezPool,
            tokenPool = tokenPool
        ).scaleUpToToken(tokenInfo.token.decimals, XTZ.decimals)
        val liquidityFee = amount.multiply(liquidityFee)
        return DexterExchange(
            amountFrom.bigDecimal,
            valueToBuy,
            liquidityFee,
            conversionRate,
            amountFrom,
            amountTo
        )
    }

    /**
     * ?? {token_from} = ?? {token_to}
     *
     * @throws ArithmeticException
     */
    fun getExchangeRate(
        amount: BigDecimal = BigDecimal.ONE,
        fromToken: TokenInfoBundle,
        toToken: TokenInfoBundle
    ): Single<BigDecimal> {
        return Single.fromCallable {
            val balance: DexterBalance
            if (fromToken.token.isXTZ()) {
                // XTZ -> FA1.2 Token
                balance = (toToken.exchange as FATokenInfo).dexterBalance
                calculator.xtzToTokenExchangeRate(
                    Tez(amount),
                    xtzPool = balance.balance,
                    tokenPool = balance.tokens
                ).scaleDownToToken(fromToken.token.decimals, toToken.token.decimals)
            } else {
                // FA1.2 Token -> XTZ
                val tokenAmount = TokenBalance(amount, fromToken.token.decimals)
                balance = (fromToken.exchange as FATokenInfo).dexterBalance
                calculator.tokenToXtzExchangeRate(
                    tokenAmount.rpcRepresentation,
                    xtzPool = balance.balance,
                    tokenPool = balance.tokens
                ).scaleUpToToken(fromToken.token.decimals, toToken.token.decimals)
            }
        }.subscribeOn(singleThread())
    }

    fun calculateMarketRate(
        fromToken: TokenInfoBundle,
        toToken: TokenInfoBundle
    ): Single<BigDecimal> {
        return Single.fromCallable {
            if (fromToken.token.isXTZ()) {
                val dexterBalance = (toToken.exchange as FATokenInfo).dexterBalance
                calculator.xtzToTokenMarketRate(
                    xtzPool = dexterBalance.balance,
                    tokenPool = dexterBalance.tokens,
                    toToken.token.decimals
                )
            } else {
                val dexterBalance =
                    (fromToken.exchange as FATokenInfo).dexterBalance
                calculator.tokenToXtzMarketRate(
                    xtzPool = dexterBalance.balance,
                    tokenPool = dexterBalance.tokens,
                    fromToken.token.decimals
                )
            }
        }.subscribeOn(singleThread())
    }

    fun getMinimumOut(
        fromAmount: Balance<*>,
        fromToken: TokenInfoBundle,
        toToken: TokenInfoBundle,
        allowedSlippage: Double
    ): Single<Balance<*>> {
        return calculate(fromToken, toToken, fromAmount)
            .map<Balance<*>> {
                val amountTo: Balance<*> = it.amountTo
                if (fromToken.token.isXTZ()) {
                    val valueToBuy = (amountTo as TokenBalance).rpcRepresentation
                    val out = calculator.xtzToTokenMinimumTokenOutput(valueToBuy, allowedSlippage)
                    TokenBalance(out, fromToken.token.decimals)
                } else {
                    val valueToBuy = (amountTo as CoinBalance).value
                    val out = calculator.tokenToXtzMinimumXtzOutput(valueToBuy, allowedSlippage)
                    CoinBalance(out)
                }
            }.subscribeOn(singleThread())
    }

    fun getTokenToXtzMarketRate(token: Token, balance: DexterBalance): Single<BigDecimal> {
        return Single.fromCallable {
            calculator.tokenToXtzMarketRate(balance.balance, balance.tokens, token.decimals)
        }.subscribeOn(singleThread())
    }

    /**
     * Price impact is measure of how much a trade will alter the price.
     */
    fun calculatePriceImpact(
        amount: BigDecimal,
        fromToken: TokenInfoBundle,
        toToken: TokenInfoBundle
    ): Single<BigDecimal> {
        return Single.fromCallable {
            if (fromToken.token.isXTZ()) {
                val dexterBalance = (toToken.exchange as FATokenInfo).dexterBalance
                calculator.xtzToTokenPriceImpact(
                    Tez(amount),
                    dexterBalance.balance,
                    dexterBalance.tokens
                )
            } else {
                val dexterBalance =
                    (fromToken.exchange as FATokenInfo).dexterBalance
                val amountTo = TokenBalance(amount, fromToken.token.decimals)
                calculator.tokenToXtzPriceImpact(
                    amountTo.rpcRepresentation,
                    dexterBalance.balance,
                    dexterBalance.tokens
                )
            }
        }.subscribeOn(singleThread())
    }

    private fun BigDecimal.scaleUpToToken(fromTokenDecimals: Int, toTokenDecimals: Int): BigDecimal {
        val scale = (toTokenDecimals - fromTokenDecimals).absoluteValue
        return this.moveComma(scale * -1)
    }

    private fun BigDecimal.scaleDownToToken(fromTokenDecimals: Int, toTokenDecimals: Int): BigDecimal {
        val scale = (toTokenDecimals - fromTokenDecimals).absoluteValue
        return this.moveComma(scale)
    }

    companion object {
        private val liquidityFee = BigDecimal("0.003")
        private const val scale: Int = 8
        private const val roundingMode: Int = BigDecimal.ROUND_DOWN
        private const val roundingModeToken: Int = BigDecimal.ROUND_HALF_UP
    }
}

