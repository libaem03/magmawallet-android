/**
 * # Released under MIT License
 *
 * Copyright (c) 2021 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.model

import android.os.Parcel
import android.os.Parcelable
import io.camlcase.smartwallet.data.core.Urls
import kotlinx.serialization.Serializable

@Serializable
data class NetworkProviders(
    val nodeUrl: String,
    val verifierNodeUrl: String,
    val tzktUrl: String,
    val bcdUrl: String
) : Parcelable {
    constructor(isMainnet: Boolean) : this(
        if (isMainnet) Urls.Mainnet.NODE_URL else Urls.Testnet.NODE_URL,
        if (isMainnet) Urls.Mainnet.VERIFIER_NODE_URL else Urls.Testnet.VERIFIER_NODE_URL,
        if (isMainnet) Urls.Mainnet.TZKT_URL else Urls.Testnet.TZKT_URL,
        Urls.BCD_URL
    )

    constructor(parcel: Parcel) : this(
        parcel.readString() ?: Urls.Mainnet.NODE_URL,
        parcel.readString() ?: Urls.Mainnet.VERIFIER_NODE_URL,
        parcel.readString() ?: Urls.Mainnet.TZKT_URL,
        parcel.readString() ?: Urls.BCD_URL
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(nodeUrl)
        parcel.writeString(verifierNodeUrl)
        parcel.writeString(tzktUrl)
        parcel.writeString(bcdUrl)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<NetworkProviders> {
        override fun createFromParcel(parcel: Parcel): NetworkProviders {
            return NetworkProviders(parcel)
        }

        override fun newArray(size: Int): Array<NetworkProviders?> {
            return arrayOfNulls(size)
        }
    }
}
