/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.source.contact

import android.content.ContentProviderOperation
import android.content.ContentProviderResult
import android.content.ContentResolver
import android.provider.ContactsContract.*
import android.provider.ContactsContract.CommonDataKinds.Im
import dagger.Reusable
import io.camlcase.kotlintezos.model.TezosNetwork
import io.camlcase.smartwallet.data.core.AppContext
import io.camlcase.smartwallet.data.model.WrappedUri
import io.camlcase.smartwallet.data.model.contact.ContactTezosAddress
import io.camlcase.smartwallet.data.model.contact.MagmaContact
import io.camlcase.smartwallet.data.source.contact.device.PhoneBookGetter
import io.camlcase.smartwallet.data.source.contact.device.PhoneBookInserter
import io.camlcase.smartwallet.data.usecase.analytics.SendAnalyticUseCase
import java.io.IOException
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList

/**
 * Reads/Writes the device Contacts provider
 *
 * Has Android dependency
 */
@Reusable
class PhoneBookLocalSource @Inject constructor(
    private val appContext: AppContext,
    private val network: TezosNetwork,
    private val analyticUseCase: SendAnalyticUseCase
) {
    private val inserter = PhoneBookInserter(contentResolver())
    private val getter = PhoneBookGetter(contentResolver(), network)

    fun get(): List<MagmaContact> {
        return getter.getContacts()
    }

    /**
     * @param account Gmail account which to save the contact on
     * @return Contact with updated ID
     */
    @Throws(IllegalStateException::class, IOException::class)
    fun add(account: String, contact: MagmaContact): MagmaContact {
        val insertedId = inserter.addContact(account, contact)
        return contact.copy(insertedId)
    }

    fun insertTezosAddress(
        id: Long,
        tezosAddress: ContactTezosAddress
    ): Boolean {
        return inserter.insertTezosAddress(id, tezosAddress)
    }

    fun updateTezosAddress(
        contact: MagmaContact,
        tezosAddress: ContactTezosAddress
    ): Boolean {
        return withContentResolver {
            updateTezosAddress(it, contact, tezosAddress)
        }
    }

    /**
     * Update GIVEN_NAME
     */
    fun updateName(
        contact: MagmaContact,
        name: String
    ): Boolean {
        return withContentResolver {
            updateName(it, contact, name)
        }
    }

    private fun updateName(
        contentResolver: ContentResolver,
        contact: MagmaContact,
        name: String
    ): Boolean {
        val selection = Data.RAW_CONTACT_ID + " = '" + contact.id + "' " +
                "AND " + RawContacts.Data.MIMETYPE + " = '" + CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE + "' "

        val operations = ArrayList<ContentProviderOperation>()
        operations.add(
            ContentProviderOperation.newUpdate(Data.CONTENT_URI)
                .withSelection(
                    selection,
                    null
                )
                .withValue(CommonDataKinds.StructuredName.GIVEN_NAME, name)
                .build()
        )
        val results: Array<ContentProviderResult> = contentResolver.applyBatch(AUTHORITY, operations)
        return results.isNotEmpty() && results[0].count == 1
    }

    /**
     * Overwrites the first element of [MagmaContact.tezosAddresses] with [tezosAddress]
     */
    private fun updateTezosAddress(
        contentResolver: ContentResolver,
        contact: MagmaContact,
        tezosAddress: ContactTezosAddress
    ): Boolean {
        val operations = ArrayList<ContentProviderOperation>()
        operations.add(
            ContentProviderOperation.newUpdate(Data.CONTENT_URI)
                .withSelection(
                    selectionByIdAndTezosAddress(contact),
                    null
                )
                .withValue(Im.DATA, tezosAddress.formatted)
                .withValue(Im.LABEL, tezosAddress.network.name)
                .withValue(Im.TYPE, Im.TYPE_OTHER)
                .withValue(Im.PROTOCOL, Im.PROTOCOL_CUSTOM)
                .withValue(Im.CUSTOM_PROTOCOL, TEZOS_CONTACT)
                .build()
        )
        val results: Array<ContentProviderResult> = contentResolver.applyBatch(AUTHORITY, operations)
        return results.isNotEmpty() && results[0].count == 1
    }

    fun getContactFromUri(bundle: WrappedUri): MagmaContact? {
        return getter.getContactFromUri(bundle)
    }

    private fun <T> withContentResolver(block: (ContentResolver) -> T): T {
        appContext.get()?.let {
            val contentResolver = it.contentResolver
            return block(contentResolver)

        } ?: throw IllegalStateException("Context of the application is null | PhoneBookLocalSource")
    }

    private fun contentResolver(): ContentResolver {
        appContext.get()?.let {
            return it.contentResolver
        } ?: throw IllegalStateException("Context of the application is null | PhoneBookLocalSource")
    }

    /**
     * Fully deletes the whole contract
     */
    fun delete(contact: MagmaContact): Boolean {
        return withContentResolver {
            val contactId = getter.getContactId(contact.id)
            deleteContact(it, contactId)
        }
    }

    private fun deleteContact(contentResolver: ContentResolver, id: Long): Boolean {
        val operations = ArrayList<ContentProviderOperation>()
        val selection = CommonDataKinds.Phone.CONTACT_ID + " = '" + id + "'"
        operations.add(
            ContentProviderOperation
                .newDelete(RawContacts.CONTENT_URI)
                .withSelection(selection, null)
                .build()
        )
        val results: Array<ContentProviderResult> = contentResolver.applyBatch(AUTHORITY, operations)
        return results.isNotEmpty() && results[0].count == 1
    }

    /**
     * Only deletes the [TEZOS_CONTACT] type that matches the tz1 address
     */
    fun deleteData(contact: MagmaContact): Boolean {
        return withContentResolver { deleteTezosAddress(it, contact) }
    }

    private fun deleteTezosAddress(contentResolver: ContentResolver, contact: MagmaContact): Boolean {
        val operations = ArrayList<ContentProviderOperation>()
        val selectPhone: String = selectionByIdAndTezosAddress(contact)

        operations.add(
            ContentProviderOperation.newDelete(Data.CONTENT_URI)
                .withSelection(selectPhone, null)
                .build()
        )
        val results: Array<ContentProviderResult> = contentResolver.applyBatch(AUTHORITY, operations)
        return results.isNotEmpty() && results[0].count == 1
    }

    fun hasExtraData(contact: MagmaContact): Boolean {
        val mimeTypes = getter.getContactExtraTypes(contact)
        return mimeTypes.isNotEmpty()
    }

    companion object {
        /**
         * Label to be shown at the top of the Address on the Contact screen
         */
        internal const val TEZOS_CONTACT = "Tezos Address"
        internal const val SEPARATOR = "//"

        internal fun selectionByTezosAddress(network: TezosNetwork): String =
            Data.MIMETYPE + " = '" + Im.CONTENT_ITEM_TYPE + "' " +
                    "AND " + Im.CUSTOM_PROTOCOL + " = '" + TEZOS_CONTACT + "' " +
                    "AND " + Im.DATA + " LIKE " + addressNetworkFormat(network)

        internal fun selectionByIdAndTezosAddress(contact: MagmaContact): String =
            Data.RAW_CONTACT_ID + " = '" + contact.id + "' " +
                    "AND " + Data.MIMETYPE + " = '" + Im.CONTENT_ITEM_TYPE + "' " +
                    "AND " + Im.CUSTOM_PROTOCOL + " = '" + TEZOS_CONTACT + "' " +
                    "AND " + Im.DATA + " = '" + contact.tezosAddresses[0].formatted + "'"

        internal fun addressNetworkFormat(network: TezosNetwork): String =
            "'" + network.name.toLowerCase(Locale.ROOT) + "//%'"
    }
}


