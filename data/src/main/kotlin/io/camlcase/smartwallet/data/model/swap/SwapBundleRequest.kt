/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.model.swap

import io.camlcase.kotlintezos.model.Tez
import io.camlcase.kotlintezos.model.operation.fees.OperationFees
import io.camlcase.smartwallet.data.core.Exchange
import io.camlcase.smartwallet.data.core.extension.tezos.getTotalFees
import io.camlcase.smartwallet.data.model.amount.Balance
import io.camlcase.smartwallet.data.model.amount.CoinBalance
import io.camlcase.smartwallet.data.model.amount.EmptyBalance
import io.camlcase.smartwallet.data.model.exchange.TokenInfoBundle
import io.camlcase.smartwallet.data.model.isXTZ

data class SwapBundleRequest(
    val fromToken: TokenInfoBundle,
    val toToken: TokenInfoBundle,
    val fromAmount: Balance<*>,
    /**
     * Allowed slippage set by the user (if user accepted the warning it should be the calculated one)
     */
    val slippage: Double,
    val timeout: Int = Exchange.DEFAULT_TIMEOUT_MINUTES
) {
    var operationFee: OperationFees = OperationFees.empty

    val accumulatedFees: Tez
        get() = operationFee.getTotalFees()

    val substractFeesFromAmount: Balance<*>
        get() {
            return if (maxXTZ()) {
                val amount = (fromAmount as CoinBalance).value
                val fees = if (accumulatedFees > Exchange.minimumXTZBalance) {
                    accumulatedFees
                } else {
                    Exchange.minimumXTZBalance
                }

                val newAmount = amount - fees
                CoinBalance(newAmount)
            } else {
                fromAmount
            }
        }

    fun maxXTZ(): Boolean {
        if (fromToken.token.isXTZ()) {
            // Check if XTZ value is MAX
            val totalBalance = (fromToken.balance as CoinBalance).value
            val paddedBalance = totalBalance - Exchange.minimumXTZBalance
            val amount = (fromAmount as CoinBalance).value
            // Fees will be substracted of the amount
            if (amount > paddedBalance) {
                return true
            }
        }
        return false
    }

    fun copy(fromAmount: Balance<*>): SwapBundleRequest {
        val bundle = SwapBundleRequest(fromToken, toToken, fromAmount, slippage)
        bundle.operationFee = operationFee
        return bundle
    }

    companion object {
        val empty =
            SwapBundleRequest(TokenInfoBundle.empty, TokenInfoBundle.empty, EmptyBalance(), Exchange.DEFAULT_SLIPPAGE)
    }
}
