/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.source.user

import io.camlcase.kotlintezos.wallet.PublicKey
import io.camlcase.smartwallet.data.core.AppContext
import io.camlcase.smartwallet.data.model.AppWallet
import io.camlcase.smartwallet.data.model.UserTezosAddress
import io.camlcase.smartwallet.data.model.error.AppError
import io.camlcase.smartwallet.data.model.error.ErrorType
import io.camlcase.smartwallet.data.source.local.SecurePreferences
import io.camlcase.smartwallet.data.source.local.UserPreferences
import io.reactivex.rxjava3.core.Single

/**
 * Stores an [AppWallet]
 */
class WalletLocalSource(
    private val securePreferences: SecurePreferences,
    private val flagPreferences: UserPreferences

) {
    fun storeWallet(wallet: AppWallet) {
        securePreferences.store(SecurePreferences.KEY_WALLET, wallet.serialize())
        securePreferences.store(SecurePreferences.KEY_PUBLIC_KEY, wallet.wallet.publicKey.bytes)
        flagPreferences.store(UserPreferences.WALLET_PUBLIC_ADDRESS, UserTezosAddress(wallet.address).serialize())
    }

    fun getWallet(): AppWallet? {
        return securePreferences.getString(SecurePreferences.KEY_WALLET)?.let { AppWallet.deserialize(it) }
    }

    /**
     * @throws AppError if Wallet is not saved
     */
    fun getWalletOrDie(): Single<AppWallet> {
        return Single.fromCallable { getWallet() ?: throw AppError(ErrorType.NO_WALLET) }
    }

    fun getPublicKey(): PublicKey? {
        return securePreferences.getByteArray(SecurePreferences.KEY_PUBLIC_KEY)?.let { PublicKey(it) }
    }

    /**
     * @throws AppError if PublicKey is not saved
     */
    fun getPublicKeyOrDie(): Single<PublicKey> {
        return Single.fromCallable { getPublicKey() ?: throw AppError(ErrorType.NO_WALLET) }
    }

    fun deleteWallet() {
        securePreferences.clear()
        flagPreferences.clear()
    }

    fun storeMigrationWallet(wallet: AppWallet) {
        securePreferences.store(SecurePreferences.KEY_WALLET_MIGRATION, wallet.serialize())
    }

    fun getMigrationWallet(): AppWallet? {
        return securePreferences.getString(SecurePreferences.KEY_WALLET_MIGRATION)?.let { AppWallet.deserialize(it) }
    }

    /**
     * Once all the migration has been done succesfully, swap to the newly created wallet and delete the temporary key.
     */
    fun swapMigrationWalletToStandardWallet() {
        getMigrationWallet()?.let {
            // First clean up delegate, limits and other data
            flagPreferences.clear()
            // Store migration wallet in both secure and user preferences
            storeWallet(it)
            // Delete the temporary key we had for it
            securePreferences.preferences?.edit()?.apply {
                this.remove(SecurePreferences.KEY_WALLET_MIGRATION)
                this.apply()
            }
        }
    }

    companion object {
        operator fun invoke(appContext: AppContext) = run {
            WalletLocalSource(SecurePreferences(appContext), UserPreferences(appContext))
        }
    }
}
