/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.source.user

import io.camlcase.smartwallet.data.core.AppContext
import io.camlcase.smartwallet.data.core.security.CryptoUtils
import io.camlcase.smartwallet.data.source.local.SecurePreferences

/**
 * Stores a hash of the pin
 */
class PinLocalSource(
    private val securePreferences: SecurePreferences
) {
    /**
     * @throws MagmaError
     */
    fun storePin(rawPin: String) {
        val hashedPin = CryptoUtils.encryptWithSalt(rawPin, getSalt())
        securePreferences.store(SecurePreferences.KEY_PIN, hashedPin)
    }

    /**
     * @throws MagmaError
     */
    fun comparePin(typedPin: String): Boolean {
        val salt = getSalt()
        val hashedPin = CryptoUtils.encryptWithSalt(typedPin, salt)
        val storedPin = securePreferences.getString(SecurePreferences.KEY_PIN)

        return hashedPin == storedPin
    }

    private fun getSalt(): String {
        var salt = securePreferences.getString(SecurePreferences.KEY_PIN_SALT)
        if (salt == null) {
            salt = CryptoUtils.generateSalt()
            securePreferences.store(SecurePreferences.KEY_PIN_SALT, salt)
        }
        return salt
    }

    companion object {
        operator fun invoke(appContext: AppContext) = run {
            PinLocalSource(SecurePreferences(appContext))
        }
    }
}
