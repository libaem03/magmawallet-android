/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.core

import dagger.Reusable
import io.camlcase.kotlintezos.model.TezosCallback
import io.camlcase.kotlintezos.model.TezosError
import io.camlcase.smartwallet.data.analytic.AnalyticsTracker
import io.camlcase.smartwallet.data.model.error.AppError
import io.camlcase.smartwallet.data.model.error.ErrorType
import io.reactivex.rxjava3.core.FlowableEmitter
import io.reactivex.rxjava3.core.SingleEmitter
import javax.inject.Inject

/**
 * Helper class to bridge the reactive world with the callback-style world.
 */
@Reusable
class RxEmitter @Inject constructor(
    val analyticsDelegate: AnalyticsTracker
) {

    inline fun <reified T> emitCallback(emitter: SingleEmitter<T>?, source: EmitterSource): TezosCallback<T> {
        analyticsDelegate.addTrail("Source: ${source.name}")
        return tezosCallback(emitter) { defaultErrorReport(it, source) }
    }

    inline fun <reified T> emitCallback(emitter: FlowableEmitter<T>?, source: EmitterSource): TezosCallback<T> {
        analyticsDelegate.addTrail("Source: ${source.name}")
        return tezosCallback(emitter) { defaultErrorReport(it, source) }
    }

    /**
     * For infrastructure errors, it's important to know which is the [source]
     */
    fun defaultErrorReport(error: TezosError, source: EmitterSource) {
        if (error.rpcErrors.isNullOrEmpty()) {
            // If it has rpcErrors, these will be reported on OperationErrorInterceptor
            analyticsDelegate.reportTezosError(error, source)
        }
    }

    /**
     * Converts a [TezosCallback] into a Single emitter
     */
    fun <T> tezosCallback(emitter: SingleEmitter<T>?, reportError: (TezosError) -> Unit): TezosCallback<T> {
        return object : TezosCallback<T> {
            override fun onFailure(error: TezosError) {
                Logger.e(error.toString() + " | ${emitter?.isDisposed}", "TezosCallback ERROR:")
                reportError(error)

                if (emitter != null && !emitter.isDisposed) {
                    emitter.onError(error)
                }
            }

            override fun onSuccess(item: T?) {
                Logger.e(item?.toString() ?: "null", "TezosCallback SUCCESS:")
                if (emitter != null && !emitter.isDisposed) {
                    if (item == null) {
                        emitter.onError(AppError(ErrorType.EMPTY_DATA))
                    } else {
                        emitter.onSuccess(item)
                    }
                }
            }
        }
    }

    /**
     * Converts a [TezosCallback] into a Flowable emitter
     */
    fun <T> tezosCallback(emitter: FlowableEmitter<T>?, reportError: (TezosError) -> Unit): TezosCallback<T> {
        return object : TezosCallback<T> {
            override fun onFailure(error: TezosError) {
                Logger.e(error.toString() + " | ${emitter?.isCancelled}", "TezosCallback ERROR:")
                if (emitter?.isCancelled == false) {
                    emitter.onError(error)
                }
                reportError(error)
            }

            override fun onSuccess(item: T?) {
                Logger.e(item?.toString() ?: "null", "TezosCallback SUCCESS:")
                if (emitter?.isCancelled == false) {
                    emitter.onNext(item)
                }
            }
        }
    }
}

/**
 * Add the latest call source so we have a better understanding of the user situation before crash reports.
 */
enum class EmitterSource {
    XTZ_BALANCE,
    TOKEN_BALANCE,
    DEXTER_POOL,
    LIST_OPERATIONS,
    GET_DELEGATE,
    DELEGATE,
    UNDELEGATE,
    BUNDLE_FEES,
    RUN_OPERATION,
    WAIT_INJECTION,
    GET_BCD_DETAILS,
    MIGRATION_BALANCE,
    CALCULATE_MIGRATION_FEES,
    MIGRATION_TRANSFER,
    BCD_BALANCE,
    NETWORK_CONFIG_CHECK
}

