/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.source.local

import android.content.Context
import android.content.SharedPreferences
import io.camlcase.smartwallet.data.core.AppContext
import io.camlcase.smartwallet.data.model.LoginStep
import io.camlcase.smartwallet.data.model.NetworkProviders
import io.camlcase.smartwallet.data.model.OnboardingStep
import kotlinx.serialization.json.Json

/**
 * Android SharedPreferences for app flags and info that is read sparingly.
 */
class AppPreferences(
    override val context: AppContext,
    override val fileName: String = "app_preferences"
) : Preferences {
    override val preferences: SharedPreferences?
        get() = context.get()?.getSharedPreferences(fileName, Context.MODE_PRIVATE)

    fun storeOnboardingStep(step: OnboardingStep) {
        store(ONBOARDING_STEP, step.name)
    }

    fun getOnboardingStep(): OnboardingStep {
        return getString(ONBOARDING_STEP)?.let {
            OnboardingStep.valueOf(it)
        } ?: OnboardingStep.CREATE_WALLET
    }

    fun storeLoginStep(step: LoginStep) {
        store(LOGIN_STEP, step.name)
    }

    fun getLoginStep(): LoginStep {
        return getString(LOGIN_STEP)?.let {
            LoginStep.valueOf(it)
        } ?: LoginStep.UNKNOWN
    }

    fun getNetworkConfig(): NetworkProviders? {
        return getString(NETWORK_CONFIG)?.let {
            Json.decodeFromString(NetworkProviders.serializer(), it)
        }
    }

    fun storeNetworkConfig(providers: NetworkProviders) {
        val serialised = Json.encodeToString(NetworkProviders.serializer(), providers)
        store(NETWORK_CONFIG, serialised)
    }

    companion object {
        const val ONBOARDING_STEP = "app.onboarding.step"  // Screen to show in the onboarding
        const val LOGIN_STEP = "app.login.step"  // Screen to show in the import
        const val AUTH_TIMESTAMP = "app.auth.timestamp" // Last authentication, in millis
        const val BIOMETRIC_LOGIN_ENABLED = "app.biometric_login.enabled" // Boolean
        const val AUTH_CANCELLED = "app.auth.cancelled" // User cancelled authentication

        const val BAKERS_DATA_TIMESTAMP = "app.bakers.timestamp" // Milliseconds since last update
        const val META_DATA_TIMESTAMP = "app.metadata.timestamp" // Milliseconds since last update
        const val COINGECKO_RATES_TIMESTAMP = "app.coingecko.rates.timestamp" // Milliseconds since last update
        const val MOONPAY_COUNTRIES_TIMESTAMP = "app.moonpay_countries.timestamp" // Milliseconds since last update

        const val EXCHANGE_SLIPPAGE = "app.exchange.slippage" // Decimal percentage of the preferred slippage
        const val EXCHANGE_TIMEOUT = "app.exchange.timeout" // Minutes for a Dexter deadline

        const val NETWORK_CONFIG = "app.network.urls" // Endpoints to fetch data
    }
}
