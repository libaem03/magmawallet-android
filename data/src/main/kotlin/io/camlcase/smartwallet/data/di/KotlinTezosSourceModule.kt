/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.di

import dagger.Module
import dagger.Provides
import dagger.Reusable
import io.camlcase.kotlintezos.BCDClient
import io.camlcase.kotlintezos.ClientWrapper
import io.camlcase.kotlintezos.TezosNodeClient
import io.camlcase.kotlintezos.TzKtClient
import io.camlcase.kotlintezos.exchange.DexterTokenClient
import io.camlcase.kotlintezos.exchange.IndexterService
import io.camlcase.kotlintezos.model.Address
import io.camlcase.kotlintezos.model.TezosNetwork
import io.camlcase.kotlintezos.model.TezosProtocol
import io.camlcase.kotlintezos.network.DefaultNetworkClient
import io.camlcase.kotlintezos.operation.RevealService
import io.camlcase.kotlintezos.operation.metadata.BlockchainMetadataService
import io.camlcase.kotlintezos.token.TokenContractClient
import io.camlcase.kotlintezos.token.balance.TokenBalanceService
import io.camlcase.smartwallet.data.core.api.ConnectionInterceptor
import io.camlcase.smartwallet.data.core.api.MagmaClientWrapper
import io.camlcase.smartwallet.data.core.api.OperationErrorInterceptor
import io.camlcase.smartwallet.data.core.extension.tezos.isMainnet
import io.camlcase.smartwallet.data.model.NetworkProviders
import io.camlcase.smartwallet.data.model.Token
import io.camlcase.smartwallet.data.source.local.AppPreferences
import io.camlcase.smartwallet.data.tezos.TezosNodeClientFactory
import okhttp3.OkHttpClient
import java.util.concurrent.Executors
import javax.inject.Named

/**
 * KotlinTezos data providers
 */
@Module
open class KotlinTezosSourceModule(
    val debug: Boolean,
    val flavor: String,
) {

    @Provides
    @Reusable
    open fun provideTezosProtocol(): TezosProtocol {
        return TezosProtocol.GRANADA
    }

    @Provides
    @Reusable
    open fun provideNetworkClient(
        interceptor: ConnectionInterceptor,
        operationErrorInterceptor: OperationErrorInterceptor
    ): ClientWrapper<OkHttpClient> {
        return MagmaClientWrapper(debug, interceptor, operationErrorInterceptor)
    }

    /**
     * Helper class to build a certain [TezosNodeClient]
     */
    @Provides
    @Reusable
    fun provideTezosNodeClientFactory(
        clientWrapper: ClientWrapper<OkHttpClient>,
        appPreferences: AppPreferences,
        isMainnet: Boolean
    ): TezosNodeClientFactory {
        val providers = appPreferences.getNetworkConfig() ?: NetworkProviders(isMainnet)
        return TezosNodeClientFactory(
            providers.nodeUrl,
            providers.verifierNodeUrl,
            clientWrapper
        )
    }

    @Provides
    @Reusable
    open fun provideTezosNodeClient(factory: TezosNodeClientFactory): TezosNodeClient {
        return factory.createTezosNodeClient()
    }

    @Provides
    @Reusable
    open fun provideTezosNetwork(): TezosNetwork {
        return if (flavor.isMainnet()) {
            TezosNetwork.MAINNET
        } else {
            TezosNetwork.GRANADANET
        }
    }

    @Provides
    @Reusable
    fun provideSupportedTokenContracts(
        tezosNodeClient: TezosNodeClient,
        @Named("SupportedFATokens") supportedFATokens: List<Token>,
        balanceService: TokenBalanceService
    ): LinkedHashMap<Address, TokenContractClient> {
        val map = LinkedHashMap<String, TokenContractClient>()
        for (supported in supportedFATokens) {
            val contractAddress = supported.contractAddress
            if (!contractAddress.isNullOrBlank()) {
                map[contractAddress] =
                    TokenContractClient(contractAddress, tezosNodeClient, balanceService)
            }
        }
        return map
    }

    @Provides
    @Reusable
    fun provideDexterTokenContracts(
        tezosNodeClient: TezosNodeClient,
        @Named("SupportedFATokens") supportedFATokens: List<Token>,
        tezosNetwork: TezosNetwork,
        clientWrapper: ClientWrapper<OkHttpClient>,
    ): LinkedHashMap<Address, DexterTokenClient> {
        // Fake Indexter service. We won't be using allowances/liquidity
        val fakeIndexterClient = DefaultNetworkClient("https://indexter.dexter.io", emptyList(), clientWrapper)
        val fakeIndexterService = IndexterService(fakeIndexterClient, Executors.newCachedThreadPool())

        val map = LinkedHashMap<String, DexterTokenClient>()
        for (supported in supportedFATokens) {
            val dexterAddress = supported.dexterAddress
            if (!dexterAddress.isNullOrBlank()) {
                map[dexterAddress] =
                    DexterTokenClient(
                        tezosNodeClient,
                        tezosNetwork,
                        dexterAddress,
                        supported.contractAddress!!,
                        fakeIndexterService
                    )
            }
        }
        return map
    }

    @Provides
    @Reusable
    fun provideRevealService(factory: TezosNodeClientFactory): RevealService {
        val executorService = Executors.newCachedThreadPool()
        return RevealService(
            BlockchainMetadataService(factory.client, executorService),
            executorService
        )
    }

    @Provides
    @Reusable
    fun provideTzKtClient(
        clientWrapper: ClientWrapper<OkHttpClient>,
        appPreferences: AppPreferences,
        isMainnet: Boolean
    ): TzKtClient {
        val providers = appPreferences.getNetworkConfig() ?: NetworkProviders(isMainnet)
        val client = DefaultNetworkClient(providers.tzktUrl, emptyList(), clientWrapper)
        return TzKtClient(client)
    }

    @Provides
    @Reusable
    fun provideBCDClient(
        clientWrapper: ClientWrapper<OkHttpClient>,
        appPreferences: AppPreferences,
        isMainnet: Boolean
    ): BCDClient {
        val providers = appPreferences.getNetworkConfig() ?: NetworkProviders(isMainnet)
        val executorService = Executors.newCachedThreadPool()
        val client = DefaultNetworkClient(providers.bcdUrl, emptyList(), clientWrapper)
        return BCDClient(client, executorService)
    }
}


