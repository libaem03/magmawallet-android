/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.model.amount

import android.os.Parcel
import android.os.Parcelable
import io.camlcase.kotlintezos.model.Tez
import io.camlcase.smartwallet.data.model.Token
import java.math.BigDecimal

data class EmptyBalance(
    override val value: Int = 0
) : Balance<Int> {
    override val formattedValue: String = "0"
    override val isZero: Boolean = true
    override fun inCurrency(currencyPrice: Double): BigDecimal {
        return BigDecimal.ZERO
    }

    override fun compareTo(other: Int): Int {
        return 0
    }

    override fun compareTo(double: Double): Int {
        return 0.0.compareTo(double)
    }

    override fun compareTo(other: Tez): Int {
        return Tez.zero.compareTo(other)
    }

    override fun minus(other: Int): Int {
        return 0
    }

    override val bigDecimal: BigDecimal
        get() = BigDecimal.ZERO

    override fun format(token: Token): String {
        return ""
    }

    constructor(parcel: Parcel) : this(parcel.readInt())

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(value)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<EmptyBalance> {
        override fun createFromParcel(parcel: Parcel): EmptyBalance {
            return EmptyBalance(parcel)
        }

        override fun newArray(size: Int): Array<EmptyBalance?> {
            return arrayOfNulls(size)
        }
    }

    override fun plus(other: Int): Int {
        return 0
    }
}



