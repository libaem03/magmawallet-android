/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.usecase.user

import io.camlcase.smartwallet.data.core.Wallet
import io.camlcase.smartwallet.data.model.AppWallet
import io.camlcase.smartwallet.data.model.error.AppError
import io.camlcase.smartwallet.data.model.error.ErrorType
import io.camlcase.smartwallet.data.model.valid
import io.camlcase.smartwallet.data.source.user.WalletLocalSource
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

class GetSeedPhraseUseCase @Inject constructor(
    private val walletLocalSource: WalletLocalSource
) {
    /**
     * - 12 words in plain text
     * - derivation path (if any)
     */
    fun getSeedPhraseData(): Single<Pair<List<String>, String?>> {
        return walletLocalSource.getWalletOrDie()
            .map {
                val mnemonics = it.mnemonic
                if (!it.valid()) {
                    throw AppError(ErrorType.NO_WALLET)
                }
                Pair(mnemonics, it.derivationPath)
            }
    }

    fun hasPassphrase(): Single<Boolean> {
        return walletLocalSource.getWalletOrDie()
            .map { !it.passphrase.isNullOrBlank() }
    }

    /**
     * @throws AppError with [ErrorType.WALLET_CREATE_ERROR] if [mnemonic] is invalid.
     */
    fun confirmMnemonic(mnemonic: List<String>, passphrase: String?): Single<Boolean> {
        return walletLocalSource.getWalletOrDie()
            .map {
                val confirmWallet = try {
                    AppWallet(mnemonic, passphrase, Wallet.DEFAULT_BIP44_DERIVATION_PATH)
                } catch (e: Throwable) {
                    throw AppError(ErrorType.WALLET_CREATE_ERROR, exception = e)
                }
                if (confirmWallet.valid()) {
                    it.mnemonic == confirmWallet.mnemonic
                            && it.passphrase == confirmWallet.passphrase
                            && it.address == confirmWallet.address
                } else {
                    throw AppError(ErrorType.WALLET_CREATE_ERROR)
                }
            }
    }

    /**
     * @return true if [mnemonic] and generated address match with migration wallet
     * @throws AppError with [ErrorType.WALLET_CREATE_ERROR] If migration wallet could not be recovered or if [mnemonic] is not valid
     */
    fun confirmMigrationMnemonic(mnemonic: List<String>): Boolean {
        val migrationWallet: AppWallet =
            walletLocalSource.getMigrationWallet() ?: throw AppError(ErrorType.WALLET_CREATE_ERROR)
        val confirmWallet = try {
            AppWallet(mnemonic, null, migrationWallet.derivationPath)
        } catch (e: Throwable) {
            throw AppError(ErrorType.WALLET_CREATE_ERROR, exception = e)
        }
        return if (confirmWallet.valid()) {
            migrationWallet.mnemonic == confirmWallet.mnemonic && migrationWallet.address == confirmWallet.address
        } else {
            throw AppError(ErrorType.WALLET_CREATE_ERROR)
        }
    }
}
