/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.di

import dagger.Module
import dagger.Provides
import dagger.Reusable
import io.camlcase.smartwallet.data.analytic.AnalyticsTracker
import io.camlcase.smartwallet.data.core.AppContext
import io.camlcase.smartwallet.data.core.Delegate
import io.camlcase.smartwallet.data.core.Tokens
import io.camlcase.smartwallet.data.core.db.MagmaDatabase
import io.camlcase.smartwallet.data.model.Token
import io.camlcase.smartwallet.data.source.delegate.BakerRemoteSource
import io.camlcase.smartwallet.data.source.info.exchange.CoinGeckoRemoteSource
import io.camlcase.smartwallet.data.source.local.UserPreferences
import io.camlcase.smartwallet.data.source.money.MoonPayRemoteSource
import io.camlcase.smartwallet.data.usecase.delegate.DelegateStream
import io.camlcase.smartwallet.data.usecase.money.BuyTezDelegate
import javax.inject.Named

/**
 * Inject data sources.
 * Since api keys and urls will depend on product flavours, we delegate the retrieval to the APP module.
 */
@Module
open class DataSourceModule(
    val debug: Boolean = false,
    val isMainnet: Boolean,
    val database: MagmaDatabase,
    val moonPayApiUrl: String,
    val moonPayApiKey: String,
    val versionCode: Int,
    val sentryApiKey: String,
) {

    @Provides
    @Reusable
    open fun provideDelegateListRemoteSource(): BakerRemoteSource {
        return BakerRemoteSource(debug)
    }

    @Provides
    @Reusable
    open fun provideTestnetBakers(): List<String> {
        return Delegate.testnetBakers
    }

    @Provides
    @Reusable
    open fun provideIsMainnet(): Boolean {
        return isMainnet
    }

    @Provides
    @Reusable
    @Named("SupportedFATokens")
    open fun provideSupportedFATokens(): List<Token> {
        return if (isMainnet) {
            Tokens.mainnetFATokens
        } else {
            Tokens.testnetFATokens
        }
    }

    @Provides
    @Reusable
    @Named("SupportedTokens")
    open fun provideSupportedTokens(): List<Token> {
        return if (isMainnet) {
            Tokens.supportedTokens
        } else {
            Tokens.testnetSupportedTokens
        }
    }

    @Provides
    @Reusable
    open fun provideDelegateStream(): DelegateStream {
        return DelegateStream()
    }

    @Provides
    fun provideMagmaDatabase(): MagmaDatabase {
        return database
    }

    @Provides
    @Reusable
    fun provideBuyTezDelegate(userPreferences: UserPreferences): BuyTezDelegate {
        return BuyTezDelegate(isMainnet, moonPayApiKey, userPreferences)
    }

    @Provides
    fun provideVersionCode(): Int {
        return versionCode
    }

    @Provides
    @Reusable
    fun provideCoinGeckoRemoteSource(): CoinGeckoRemoteSource {
        return CoinGeckoRemoteSource(debug)
    }

    @Provides
    @Reusable
    fun provideMoonPayRemoteSource(): MoonPayRemoteSource {
        return MoonPayRemoteSource(debug, moonPayApiUrl, moonPayApiKey)
    }

    @Provides
    @Reusable
    fun provideAnalyticsTracker(appContext: AppContext, isMainnet: Boolean): AnalyticsTracker {
        return AnalyticsTracker(appContext, isMainnet, sentryApiKey)
    }
}
