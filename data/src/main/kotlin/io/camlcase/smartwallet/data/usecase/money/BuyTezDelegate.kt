/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.usecase.money

import io.camlcase.kotlintezos.model.Address
import io.camlcase.smartwallet.data.core.extension.toUpperCaseUS
import io.camlcase.smartwallet.data.model.money.MoonPayTransactionStatus
import io.camlcase.smartwallet.data.source.local.UserPreferences

/**
 * Integration with a PCI-compliant Fiat-to-XTZ provider.
 * @see [MoonPay](https://www.moonpay.io/)
 */
class BuyTezDelegate(
    private val isMainnet: Boolean,
    private val apiKey: String,
    private val userPreferences: UserPreferences
) {

    /**
     * @param languageCode The ISO 639-1 standard language code representing the language the widget should use
     */
    fun getBuyUrl(languageCode: String): String {
        val baseUrl = if (isMainnet) {
            MOONPAY_URL
        } else {
            MOONPAY_URL_STAGING
        }

        val destinationAddress = getUserAddress()

        var buyUrl = "$baseUrl?apiKey=$apiKey"
        getUrlArguments(languageCode, destinationAddress).forEach { (key, value) -> buyUrl += "&$key=$value" }
        return buyUrl
    }

    fun getUserAddress(): Address {
        return if (isMainnet) {
            userPreferences.getUserAddress().mainAddress
        } else {
            TESTNET_ACCOUNT
        }
    }

    /**
     * @param userAddress Not used for now
     */
    private fun getUrlArguments(languageCode: String, userAddress: Address): Map<String, String> {
        val map = HashMap(constantUrlArguments)
        if (!isMainnet) {
            map["currencyCode"] = "eth"
            // map["walletAddress"] = TESTNET_ACCOUNT
        }
        // The ISO 639-1 standard language code representing the language the widget should use
        map["language"] = languageCode
        // An identifier you would like to associate with the customer. This identifier will be present whenever we
        // pass you customer data, allowing you to match our data with your own existing customer data.
        map["externalCustomerId"] = userAddress

        return map
    }

    companion object {
        /**
         * Moonpay doesn't have XTZ as a testing currency. We need to test the flow in ETH.
         */
        private const val TESTNET_ACCOUNT = "0x87dc2e57a2b3d072e1658c3a400b748b15ddf7e9"

        private const val MOONPAY_URL = "https://buy.moonpay.io"
        private const val MOONPAY_URL_STAGING = "https://buy-staging.moonpay.io"

        private const val REDIRECT_URL = "magmawallet://wallet"

        val constantUrlArguments: Map<String, String> = mapOf(
            Pair("enabledPaymentMethods", "credit_debit_card,sepa_bank_transfer,gbp_bank_transfer,gbp_open_banking_payment,samsung_pay,google_pay"),
            //  If you pass a currencyCode, the currency will be selected by default and the customer won't be able to select another currency.
            Pair("currencyCode", "xtz"),
            // We will append the transaction's ID and status as query parameters to your URL.
            Pair("redirectURL", REDIRECT_URL),
            // The customer will be able to see the valid wallet address, but will not be able to change it.
//            Pair("showWalletAddressForm", "true"),
            Pair("colorCode", "%23992871")
        )

        fun extractTransactionDetails(redirectUrl: String): Pair<String?, MoonPayTransactionStatus?> {
            if (redirectUrl.startsWith(REDIRECT_URL)) {
                val txArgs = redirectUrl.split("transactionId=", "&transactionStatus=")
                if (txArgs.size > 2) {
                    val txId = txArgs[1]
                    val txStatus = txArgs[2]?.let {
                        MoonPayTransactionStatus.valueOf(it.toUpperCaseUS())
                    }

                    return Pair(txId, txStatus)
                }
            }
            return Pair(null, null)
        }
    }
}
