package io.camlcase.smartwallet.data.usecase.conseil

import io.camlcase.kotlintezos.BCDClient
import io.camlcase.kotlintezos.TzKtClient
import io.camlcase.kotlintezos.data.parser.bcd.BCDOperationListParser
import io.camlcase.kotlintezos.data.parser.tzkt.TzKtOperationListParser
import io.camlcase.kotlintezos.model.OperationResultStatus
import io.camlcase.kotlintezos.model.bcd.BCDOperation
import io.camlcase.kotlintezos.model.operation.OperationType
import io.camlcase.kotlintezos.model.tzkt.TzKtOperation
import io.camlcase.kotlintezos.model.tzkt.TzKtTransaction
import io.camlcase.smartwallet.data.core.RxEmitter
import io.camlcase.smartwallet.data.model.error.ExchangeDeadlineTimeout
import io.camlcase.smartwallet.data.model.error.ExchangeInvalidTokens
import io.camlcase.smartwallet.data.model.operation.MagmaOperationResult
import io.camlcase.smartwallet.data.test.RxTest
import io.mockk.*
import io.reactivex.rxjava3.core.FlowableEmitter
import io.reactivex.rxjava3.core.SingleEmitter
import io.reactivex.rxjava3.observers.TestObserver
import org.junit.After
import org.junit.Assert
import org.junit.Test
import java.io.IOException

/**
 * @see [GetOperationDetailsUseCase]
 */
class GetOperationDetailsUseCaseTest : RxTest() {
    private val emitter = mockk<RxEmitter>(relaxed = true)
    private val tzKtClient = mockk<TzKtClient>(relaxed = true)
    private val bcdClient = mockk<BCDClient>(relaxed = true)

    private fun initUseCase(): GetOperationDetailsUseCase {
        return spyk(GetOperationDetailsUseCase(emitter, tzKtClient, bcdClient))
    }

    @Test
    fun `Test valid response`() {
        val expected = listOf(
            mockk<TzKtTransaction>() {
                every { status } returns OperationResultStatus.APPLIED
                every { kind } returns OperationType.TRANSACTION
                every { errors } returns null
            }
        )
        every { emitter.tezosCallback(any<FlowableEmitter<List<TzKtOperation>>>(), any()) } answers {
            firstArg<FlowableEmitter<List<TzKtOperation>>>().onNext(expected)
            mockk()
        }
        val useCase = initUseCase()

        useCase.getOperationDetail("")
            .test()
            .assertNoErrors()
            .assertValue {
                Assert.assertEquals(OperationType.TRANSACTION, it.type)
                Assert.assertEquals(OperationResultStatus.APPLIED, it.status)
                Assert.assertEquals(null, it.parsedError)
                true
            }


        verify {
            tzKtClient.getOperationDetails("", any())
        }
    }

    /**
     * We cannot pass null through an rx flow
     */
    @Test
    fun `Test empty response`() {
        every { emitter.tezosCallback(any<FlowableEmitter<List<TzKtOperation>>>(), any()) } answers {
            firstArg<FlowableEmitter<List<TzKtOperation>>>().onNext(null)
            mockk()
        }
        val useCase = initUseCase()
        val testSubscriber = TestObserver<MagmaOperationResult>()
        useCase.getOperationDetail("")
            .singleOrError()
            .subscribe(testSubscriber)

        testSubscriber.assertFailure(NullPointerException::class.java)
        testSubscriber.dispose()

        verify {
            tzKtClient.getOperationDetails(any(), any())
        }
    }

    @Test
    fun `Test error response`() {
        val error = IOException()
        every { emitter.tezosCallback(any<FlowableEmitter<List<TzKtOperation>>>(), any()) } answers {
            firstArg<FlowableEmitter<List<TzKtOperation>>>().onError(error)
            mockk()
        }
        val useCase = initUseCase()
        val testSubscriber = TestObserver<MagmaOperationResult>()
        useCase.getOperationDetail("")
            .singleOrError()
            .subscribe(testSubscriber)

        testSubscriber.assertError(error)
        testSubscriber.dispose()

        verify {
            tzKtClient.getOperationDetails(any(), any())
        }
    }

    @After
    fun after() {
        confirmVerified(tzKtClient)
        confirmVerified(bcdClient)
    }

    @Test
    fun `getOperationDetail with unknown errors`() {
        val json =
            "[{\"type\":\"transaction\",\"id\":34616631,\"level\":1190194,\"timestamp\":\"2020-10-28T00:40:28Z\",\"block\":\"BL4AgfjH9Mk6ZknQc7Ygf66CX2nbUH3E2w2mN3tZ9nnVq7zHpwu\",\"hash\":\"oo5XsmdPjxvBAbCyL9kh3x5irUmkWNwUFfi2rfiKqJGKA6Sxjzf\",\"counter\":7388570,\"sender\":{\"address\":\"tz1PMiUhz8HQ8KmKTYt1M6smQVWBb8w5QGrQ\"},\"gasLimit\":613968,\"gasUsed\":0,\"storageLimit\":257,\"storageUsed\":0,\"bakerFee\":61961,\"storageFee\":0,\"allocationFee\":0,\"target\":{\"alias\":\"Dexter tzBTC/XTZ\",\"address\":\"KT1DrJV8vhkdLEj76h1H9Q4irZDqAkMPo1Qf\"},\"amount\":1000000,\"parameters\":\"{\\\"entrypoint\\\":\\\"xtzToToken\\\",\\\"value\\\":{\\\"prim\\\":\\\"Pair\\\",\\\"args\\\":[{\\\"string\\\":\\\"tz1PMiUhz8HQ8KmKTYt1M6smQVWBb8w5QGrQ\\\"},{\\\"prim\\\":\\\"Pair\\\",\\\"args\\\":[{\\\"int\\\":\\\"15308\\\"},{\\\"string\\\":\\\"2020-10-28T00:40:16Z\\\"}]}]}}\",\"status\":\"failed\",\"errors\":[{\"type\":\"michelson_v1.runtime_error\"},{\"type\":\"michelson_v1.script_rejected\"}],\"hasInternals\":false}]"
        val expected = TzKtOperationListParser().parse(json)
        every { emitter.tezosCallback(any<FlowableEmitter<List<TzKtTransaction>>>(), any()) } answers {
            firstArg<FlowableEmitter<List<TzKtOperation>>>().onNext(expected)
            mockk()
        }

        val bcdJson =
            "[{\"id\":\"31f3c660f750446f98d6525961b72a3f\",\"protocol\":\"PsCARTHAGazKbHtnKfLzQg3kms52kSRpgnDY982a9oYsSXRLQEb\",\"hash\":\"oo5XsmdPjxvBAbCyL9kh3x5irUmkWNwUFfi2rfiKqJGKA6Sxjzf\",\"internal\":false,\"network\":\"mainnet\",\"timestamp\":\"2020-10-28T00:40:28Z\",\"level\":1190194,\"kind\":\"transaction\",\"source\":\"tz1PMiUhz8HQ8KmKTYt1M6smQVWBb8w5QGrQ\",\"fee\":61961,\"counter\":7388570,\"gas_limit\":613968,\"storage_limit\":257,\"amount\":1000000,\"destination\":\"KT1DrJV8vhkdLEj76h1H9Q4irZDqAkMPo1Qf\",\"destination_alias\":\"Dexter tzBTC/XTZ\",\"status\":\"failed\",\"entrypoint\":\"xtzToToken\",\"errors\":[{\"id\":\"proto.006-PsCARTHA.michelson_v1.runtime_error\",\"title\":\"Script runtime error\",\"descr\":\"Toplevel error for all runtime script errors\",\"kind\":\"temporary\"},{\"id\":\"proto.006-PsCARTHA.michelson_v1.script_rejected\",\"title\":\"Script failed\",\"descr\":\"A FAILWITH instruction was reached\",\"kind\":\"temporary\",\"location\":1843,\"with\":\"\\\"NOW is greater than deadline.\\\"\"}],\"result\":{},\"parameters\":{\"prim\":\"pair\",\"type\":\"namedtuple\",\"children\":[{\"prim\":\"address\",\"type\":\"address\",\"name\":\"to\",\"value\":\"tz1PMiUhz8HQ8KmKTYt1M6smQVWBb8w5QGrQ\"},{\"prim\":\"nat\",\"type\":\"nat\",\"name\":\"minTokensBought\",\"value\":\"15308\"},{\"prim\":\"timestamp\",\"type\":\"timestamp\",\"name\":\"deadline\",\"value\":\"2020-10-28 00:40:16 +0000 UTC\"}]},\"mempool\":false,\"content_index\":0,\"rawMempool\":null}]"
        val expectedBCD = BCDOperationListParser().parse(bcdJson)
        every { emitter.tezosCallback(any<SingleEmitter<List<BCDOperation>>>(), any()) } answers {
            firstArg<SingleEmitter<List<BCDOperation>>>().onSuccess(expectedBCD)
            mockk()
        }

        val useCase = initUseCase()

        useCase.getOperationDetail("")
            .test()
            .assertNoErrors()
            .assertValue {
                println(it)
                Assert.assertNotNull(it)
                Assert.assertTrue(it.parsedError is ExchangeDeadlineTimeout)
                true
            }

        verify {
            tzKtClient.getOperationDetails(any(), any())
            bcdClient.getOperationDetails(any(), any())
        }
    }

    @Test
    fun `getOperationDetail with Dexter tokens errors`() {
        val json =
            "[{\"type\":\"transaction\",\"id\":36142032,\"level\":1229065,\"timestamp\":\"2020-11-24T10:04:15Z\",\"block\":\"BKwWtn3hWDUXAN78XXoprkQGSPjW6AjhqWWANrn97pykpKFnkBb\",\"hash\":\"op6ZMjPtuUd5HF8vDza2W76LxZtvBC43WetEV143VnS2ev7oVBR\",\"counter\":8266254,\"sender\":{\"address\":\"tz1PUgGvY15NWURR1iS5xLz12Byw6kTdqKu9\"},\"gasLimit\":135820,\"gasUsed\":0,\"storageLimit\":257,\"storageUsed\":0,\"bakerFee\":13984,\"storageFee\":0,\"allocationFee\":0,\"target\":{\"address\":\"KT1DrJV8vhkdLEj76h1H9Q4irZDqAkMPo1Qf\"},\"amount\":500,\"parameters\":\"{\\\"entrypoint\\\":\\\"xtzToToken\\\",\\\"value\\\":{\\\"prim\\\":\\\"Pair\\\",\\\"args\\\":[{\\\"string\\\":\\\"tz1PUgGvY15NWURR1iS5xLz12Byw6kTdqKu9\\\"},{\\\"prim\\\":\\\"Pair\\\",\\\"args\\\":[{\\\"int\\\":\\\"7\\\"},{\\\"string\\\":\\\"2020-11-24T10:23:12Z\\\"}]}]}}\",\"status\":\"failed\",\"errors\":[{\"type\":\"michelson_v1.runtime_error\"},{\"type\":\"michelson_v1.script_rejected\"}],\"hasInternals\":false}]"
        val expected = TzKtOperationListParser().parse(json)
        every { emitter.tezosCallback(any<FlowableEmitter<List<TzKtTransaction>>>(), any()) } answers {
            firstArg<FlowableEmitter<List<TzKtOperation>>>().onNext(expected)
            mockk()
        }

        val bcdJson =
            "[{\"id\":\"c97cf1093d1d44d1b7a1bc4209058beb\",\"protocol\":\"PsDELPH1Kxsxt8f9eWbxQeRxkjfbxoqM52jvs5Y5fBxWWh4ifpo\",\"hash\":\"op6ZMjPtuUd5HF8vDza2W76LxZtvBC43WetEV143VnS2ev7oVBR\",\"internal\":false,\"network\":\"mainnet\",\"timestamp\":\"2020-11-24T10:04:15Z\",\"level\":1229065,\"kind\":\"transaction\",\"source\":\"tz1PUgGvY15NWURR1iS5xLz12Byw6kTdqKu9\",\"fee\":13984,\"counter\":8266254,\"gas_limit\":135820,\"storage_limit\":257,\"amount\":500,\"destination\":\"KT1DrJV8vhkdLEj76h1H9Q4irZDqAkMPo1Qf\",\"destination_alias\":\"Dexter tzBTC/XTZ\",\"status\":\"failed\",\"entrypoint\":\"xtzToToken\",\"errors\":[{\"id\":\"proto.007-PsDELPH1.michelson_v1.runtime_error\",\"title\":\"Script runtime error\",\"descr\":\"Toplevel error for all runtime script errors\",\"kind\":\"temporary\"},{\"id\":\"proto.007-PsDELPH1.michelson_v1.script_rejected\",\"title\":\"Script failed\",\"descr\":\"A FAILWITH instruction was reached\",\"kind\":\"temporary\",\"location\":1999,\"with\":\"\\\"tokensBought is less than minTokensBought.\\\"\"}],\"result\":{},\"parameters\":{\"prim\":\"pair\",\"type\":\"namedtuple\",\"children\":[{\"prim\":\"address\",\"type\":\"address\",\"name\":\"to\",\"value\":\"tz1PUgGvY15NWURR1iS5xLz12Byw6kTdqKu9\"},{\"prim\":\"nat\",\"type\":\"nat\",\"name\":\"minTokensBought\",\"value\":\"7\"},{\"prim\":\"timestamp\",\"type\":\"timestamp\",\"name\":\"deadline\",\"value\":\"2020-11-24 10:23:12 +0000 UTC\"}]},\"mempool\":false,\"content_index\":0,\"rawMempool\":null}]"
        val expectedBCD = BCDOperationListParser().parse(bcdJson)
        every { emitter.tezosCallback(any<SingleEmitter<List<BCDOperation>>>(), any()) } answers {
            firstArg<SingleEmitter<List<BCDOperation>>>().onSuccess(expectedBCD)
            mockk()
        }

        val useCase = initUseCase()

        useCase.getOperationDetail("")
            .test()
            .assertNoErrors()
            .assertValue {
                println(it)
                Assert.assertNotNull(it)
                Assert.assertTrue(it.parsedError is ExchangeInvalidTokens)
                true
            }

        verify {
            tzKtClient.getOperationDetails(any(), any())
            bcdClient.getOperationDetails(any(), any())
        }
    }
}
