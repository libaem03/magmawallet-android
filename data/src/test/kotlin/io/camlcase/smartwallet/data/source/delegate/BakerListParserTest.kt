package io.camlcase.smartwallet.data.source.delegate

import io.camlcase.smartwallet.data.model.delegate.BakerHealth
import io.camlcase.smartwallet.data.test.JSONTestLoader
import org.junit.Assert
import org.junit.Test

class BakerListParserTest {

    @Test
    fun `Parse a normal response`() {
        val json = JSONTestLoader.getStringFromFile("get_tezos_bakers.json")
        val result = BakerListParser().parse(json)

        Assert.assertNotNull(result)
        Assert.assertEquals(90, result!!.size)
        Assert.assertEquals(0.061274, result[0].estimatedRoi, 0.0)
        Assert.assertEquals(BakerHealth.ACTIVE.name.toLowerCase(), result[0].serviceHealth)
    }

    @Test
    fun `Parse empty json`() {
        val json = ""
        val result = BakerListParser().parse(json)

        Assert.assertNotNull(result)
        Assert.assertEquals(0, result!!.size)
    }

    @Test
    fun `Parse empty response`() {
        val json = "{}"
        val result = BakerListParser().parse(json)

        Assert.assertNotNull(result)
        Assert.assertEquals(0, result!!.size)
    }

    @Test
    fun `Parse empty bakers list`() {
        val json = "[]"
        val result = BakerListParser().parse(json)

        Assert.assertNotNull(result)
        Assert.assertEquals(0, result!!.size)
    }
}
