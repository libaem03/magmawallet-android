package io.camlcase.smartwallet.data.core.extension

import org.junit.Assert
import org.junit.Test

class KotlinExtKtTest {

    @Test
    fun `Mltilet of 2 nulls is all null`() {
        val input1: String? = null
        val input2: String? = null
        val expected = "All null"
        val result = multiLet(input1, input2) { val1, val2 ->
            val1 + val2
        } ?: expected

        Assert.assertEquals(expected, result)
    }

    @Test
    fun `Multilet of 1 left null is all null`() {
        val inputLeft: String? = null
        val inputRight: String? = "RIGHT"
        val nullValue = "All null"
        val result = multiLet(inputLeft, inputRight) { val1, val2 ->
            val1 + val2
        } ?: nullValue

        Assert.assertEquals(nullValue, result)
    }

    @Test
    fun `Multilet of 1 right null is all null`() {
        val inputLeft: String? = "LEFT"
        val inputRight: String? = null
        val nullValue = "All null"
        val result = multiLet(inputLeft, inputRight) { val1, val2 ->
            val1 + val2
        } ?: nullValue

        Assert.assertEquals(nullValue, result)
    }

    @Test
    fun `Multilet of 2 non null is result`() {
        val inputLeft: String? = "LEFT"
        val inputRight: String? = "RIGHT"
        val nullValue = "All null"
        val result = multiLet(inputLeft, inputRight) { val1, val2 ->
            val1 + val2
        } ?: nullValue

        Assert.assertEquals(inputLeft + inputRight, result)
    }

    @Test
    fun `Multilet of 3 with one null is null`() {
        val inputLeft: String? = "LEFT"
        val inputCenter: String? = "CENTER"
        val inputRight: String? = null
        val nullValue = "All null"
        val result = multiLet(inputLeft, inputCenter, inputRight) { val1, val2, val3 ->
            val1 + val2 + val3
        } ?: nullValue

        Assert.assertEquals(nullValue, result)
    }

    @Test
    fun `Multilet of 3 non null is result`() {
        val inputLeft: String? = "LEFT"
        val inputCenter: String? = "CENTER"
        val inputRight: String? = "RIGHT"
        val nullValue = "All null"
        val result = multiLet(inputLeft, inputCenter, inputRight) { val1, val2, val3 ->
            val1 + val2 + val3
        } ?: nullValue

        Assert.assertEquals(inputLeft + inputCenter + inputRight, result)
    }

    @Test
    fun `Multilet of 4 with one null is null`() {
        val inputLeft: String? = "LEFT"
        val inputCenter: String? = "CENTER"
        val inputCenter2: String? = "CENTER2"
        val inputRight: String? = null
        val nullValue = "All null"
        val result = multiLet(inputLeft, inputCenter, inputCenter2, inputRight) { val1, val2, val3, val4 ->
            val1 + val2 + val3 + val4
        } ?: nullValue

        Assert.assertEquals(nullValue, result)
    }

    @Test
    fun `Multilet of 4 non null is result`() {
        val inputLeft: String? = "LEFT"
        val inputCenter: String? = "CENTER"
        val inputCenter2: String? = "CENTER2"
        val inputRight: String? = "RIGHT"
        val nullValue = "All null"
        val result = multiLet(inputLeft, inputCenter, inputCenter2, inputRight) { val1, val2, val3, val4 ->
            val1 + val2 + val3 + val4
        } ?: nullValue

        Assert.assertEquals(inputLeft + inputCenter + inputCenter2 + inputRight, result)
    }
}
