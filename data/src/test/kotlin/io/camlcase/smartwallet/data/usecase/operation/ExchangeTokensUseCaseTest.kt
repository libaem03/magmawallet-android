package io.camlcase.smartwallet.data.usecase.operation

import io.camlcase.kotlintezos.TezosNodeClient
import io.camlcase.kotlintezos.exchange.IndexterService
import io.camlcase.kotlintezos.model.Tez
import io.camlcase.kotlintezos.model.TezosNetwork
import io.camlcase.kotlintezos.model.operation.BundledFees
import io.camlcase.kotlintezos.model.operation.SmartContractCallOperation
import io.camlcase.kotlintezos.model.operation.fees.OperationFees
import io.camlcase.smartwallet.data.core.Exchange
import io.camlcase.smartwallet.data.core.RxEmitter
import io.camlcase.smartwallet.data.core.Tokens.Testnet.USDTZ
import io.camlcase.smartwallet.data.core.XTZ
import io.camlcase.smartwallet.data.model.amount.CoinBalance
import io.camlcase.smartwallet.data.model.amount.TokenBalance
import io.camlcase.smartwallet.data.model.exchange.DexterBalance
import io.camlcase.smartwallet.data.model.exchange.FATokenInfo
import io.camlcase.smartwallet.data.model.exchange.TokenInfoBundle
import io.camlcase.smartwallet.data.model.exchange.XTZTokenInfo
import io.camlcase.smartwallet.data.model.swap.SwapBundleRequest
import io.camlcase.smartwallet.data.test.RxTest
import io.camlcase.smartwallet.data.test.mock.ADDRESS
import io.camlcase.smartwallet.data.test.mock.mockUserPreferences
import io.camlcase.smartwallet.data.test.mock.mockWalletLocalSource
import io.camlcase.smartwallet.data.usecase.analytics.SendAnalyticUseCase
import io.github.vjames19.futures.jdk8.Future
import io.mockk.confirmVerified
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.core.SingleEmitter
import org.junit.After
import org.junit.Assert
import org.junit.Test
import java.math.BigDecimal
import java.math.BigInteger

/**
 * @see ExchangeTokensUseCase
 */
class ExchangeTokensUseCaseTest : RxTest() {
    private val emitter = mockk<RxEmitter>(relaxed = true)
    private val userPreferences = mockUserPreferences()
    private val walletLocalSource = mockWalletLocalSource()
    private val tezosNodeClient = mockk<TezosNodeClient>(relaxed = true)
    private val analytics = mockk<SendAnalyticUseCase>(relaxed = true)
    private val calculationsUseCase = mockk<DexterCalculationsUseCase>(relaxed = true)

    private fun initUseCase(): ExchangeTokensUseCase {
        return ExchangeTokensUseCase(
            emitter,
            userPreferences,
            walletLocalSource,
            tezosNodeClient,
            analytics,
            calculationsUseCase,
        )
    }

    @Test
    fun `Test xtzToToken createSwapOperation OK`() {
        every { calculationsUseCase.getMinimumOut(any(), any(), any(), any()) } returns Single.just(
            TokenBalance(
                BigInteger.TEN,
                USDTZ.decimals
            )
        )
        every { emitter.tezosCallback(any<SingleEmitter<*>>(), any()) } answers {
            firstArg<SingleEmitter<BundledFees>>().onSuccess(bundled)
            mockk()
        }

        val request = SwapBundleRequest(
            fromToken = TokenInfoBundle(XTZ, CoinBalance(Tez(10.0)), XTZTokenInfo.empty),
            toToken = TokenInfoBundle(
                USDTZ,
                TokenBalance(BigDecimal.ONE, 0),
                FATokenInfo(USDTZ, 2.0, DexterBalance.empty)
            ),
            fromAmount = CoinBalance(Tez(1.340)),
            slippage = Exchange.DEFAULT_SLIPPAGE
        )
        val useCase = initUseCase()
        useCase.createSwapOperation(request)
            .test()
            .assertNoErrors()
            .assertValue {
                Assert.assertNotNull(it)
                Assert.assertTrue(it.accumulatedFee.fee == OperationFees.simulationFees.fee)
                Assert.assertTrue(it.accumulatedFee.gasLimit == OperationFees.simulationFees.gasLimit)
                Assert.assertTrue(it.accumulatedFee.storageLimit == OperationFees.simulationFees.storageLimit)
                Assert.assertTrue(it.operations == operations)
                true
            }

        verify {
            calculationsUseCase.getMinimumOut(any(), any(), any(), any())
            walletLocalSource.getWalletOrDie()
            userPreferences.getUserAddress()
            tezosNodeClient.bundleFees(any(), any(), any(), any(), any())
        }
    }

    @Test
    fun `Test tokenToXtz createSwapOperation OK`() {
        every {
            calculationsUseCase.getMinimumOut(
                any(),
                any(),
                any(),
                any()
            )
        } returns Single.just(CoinBalance(Tez(1.0)))
        every { userPreferences.getTezBalance() } returns Tez(10.0)
        every { emitter.tezosCallback(any<SingleEmitter<*>>(), any()) } answers {
            firstArg<SingleEmitter<BundledFees>>().onSuccess(bundled)
            mockk()
        }

        val request = SwapBundleRequest(
            fromToken = TokenInfoBundle(
                USDTZ,
                TokenBalance(BigDecimal("12345"), 0),
                FATokenInfo(USDTZ, 2.0, DexterBalance(Tez(12345.0), BigInteger("12345")))
            ),
            toToken = TokenInfoBundle(XTZ, CoinBalance(Tez(10.0)), XTZTokenInfo.empty),
            fromAmount = TokenBalance(BigInteger("12"), 0),
            slippage = Exchange.DEFAULT_SLIPPAGE
        )
        val useCase = initUseCase()
        useCase.createSwapOperation(request)
            .test()
            .assertNoErrors()
            .assertValue {
                Assert.assertNotNull(it)
                Assert.assertTrue(it.accumulatedFee.fee == OperationFees.simulationFees.fee)
                Assert.assertTrue(it.accumulatedFee.gasLimit == OperationFees.simulationFees.gasLimit)
                Assert.assertTrue(it.accumulatedFee.storageLimit == OperationFees.simulationFees.storageLimit)
                Assert.assertTrue(it.operations == operations)
                true
            }

        verify {
            walletLocalSource.getWalletOrDie()
            calculationsUseCase.getMinimumOut(any(), any(), any(), any())
            userPreferences.getUserAddress()
            userPreferences.getTezBalance()
            tezosNodeClient.bundleFees(any(), any(), any(), any(), any())
        }
    }

    @Test
    fun `Test exchange xtzToToken OK`() {
        val hash = "oo000"
        every { emitter.tezosCallback(any<SingleEmitter<*>>(), any()) } answers {
            firstArg<SingleEmitter<String>>().onSuccess(hash)
            mockk()
        }

        val useCase = initUseCase()
        useCase.swap(SendTokenUseCaseTest.operations)
            .test()
            .assertNoErrors()
            .assertValue { it == hash }

        verify {
            walletLocalSource.getWalletOrDie()
            tezosNodeClient.runOperations(SendTokenUseCaseTest.operations, SendTokenUseCaseTest.TO, any(), any())
        }
    }

    @After
    fun after() {
        confirmVerified(userPreferences)
        confirmVerified(walletLocalSource)
        confirmVerified(tezosNodeClient)
        confirmVerified(analytics)
        confirmVerified(calculationsUseCase)
    }

    companion object {
        private const val TO = "tz1Address2"
        val operations = listOf(
            SmartContractCallOperation(Tez(1.340), ADDRESS, TO, OperationFees.simulationFees)
        )
        val bundled = BundledFees(operations, null)
    }
}
