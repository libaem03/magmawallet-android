/**
 * # Released under MIT License
 *
 * Copyright (c) 2021 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.model.operation

import io.camlcase.kotlintezos.data.parser.tzkt.TzKtOperationListParser
import io.camlcase.kotlintezos.model.Tez
import org.junit.Assert
import org.junit.Test

class TezosOperationTest {

    @Test
    fun `groupFees +BurnFee`() {
        val json =
            "[{\"type\":\"transaction\",\"id\":2455834,\"level\":186174,\"timestamp\":\"2021-02-04T17:49:36Z\",\"block\":\"BLHsAiNmF4pL4P2TEfVzCoQoBdj6bATKYmAjNNsypVVTFy8sZ7Y\",\"hash\":\"ooJNwFGED3ogQreiMfGsT2wqC95gi93Ltcpcd3vmSthmnd9PxdG\",\"counter\":247767,\"initiator\":{\"address\":\"tz1Ydpp4ibTrGSPn2i6cqQZ6arWieUbcfWVn\"},\"sender\":{\"address\":\"KT1NA4sggYrgggK66j53jGxV88726HaqEjcB\"},\"nonce\":1,\"gasLimit\":0,\"gasUsed\":27029,\"storageLimit\":0,\"storageUsed\":0,\"bakerFee\":0,\"storageFee\":0,\"allocationFee\":0,\"target\":{\"address\":\"KT1Dj8A3CFakuzTaqUsehdtFmM85rAJyFgNh\"},\"amount\":0,\"parameters\":\"{\\\"entrypoint\\\":\\\"transfer\\\",\\\"value\\\":{\\\"prim\\\":\\\"Pair\\\",\\\"args\\\":[{\\\"bytes\\\":\\\"00008e8fb7d2b514711b03303c3f76e0228b1d644439\\\"},{\\\"prim\\\":\\\"Pair\\\",\\\"args\\\":[{\\\"bytes\\\":\\\"0194e5cf8b46377e10fe6a932b4aeaf0adf8cbc56900\\\"},{\\\"int\\\":\\\"16367\\\"}]}]}}\",\"status\":\"applied\",\"hasInternals\":false},{\"type\":\"transaction\",\"id\":2455833,\"level\":186174,\"timestamp\":\"2021-02-04T17:49:36Z\",\"block\":\"BLHsAiNmF4pL4P2TEfVzCoQoBdj6bATKYmAjNNsypVVTFy8sZ7Y\",\"hash\":\"ooJNwFGED3ogQreiMfGsT2wqC95gi93Ltcpcd3vmSthmnd9PxdG\",\"counter\":247767,\"initiator\":{\"address\":\"tz1Ydpp4ibTrGSPn2i6cqQZ6arWieUbcfWVn\"},\"sender\":{\"address\":\"KT1NA4sggYrgggK66j53jGxV88726HaqEjcB\"},\"nonce\":0,\"gasLimit\":0,\"gasUsed\":1427,\"storageLimit\":0,\"storageUsed\":0,\"bakerFee\":0,\"storageFee\":0,\"allocationFee\":0,\"target\":{\"address\":\"tz1Ydpp4ibTrGSPn2i6cqQZ6arWieUbcfWVn\"},\"amount\":4842112,\"status\":\"applied\",\"hasInternals\":false},{\"type\":\"transaction\",\"id\":2455832,\"level\":186174,\"timestamp\":\"2021-02-04T17:49:36Z\",\"block\":\"BLHsAiNmF4pL4P2TEfVzCoQoBdj6bATKYmAjNNsypVVTFy8sZ7Y\",\"hash\":\"opera\",\"counter\":247767,\"sender\":{\"address\":\"tz1Ydpp4ibTrGSPn2i6cqQZ6arWieUbcfWVn\"},\"gasLimit\":106302,\"gasUsed\":77446,\"storageLimit\":257,\"storageUsed\":0,\"bakerFee\":11199,\"storageFee\":0,\"allocationFee\":0,\"target\":{\"address\":\"KT1NA4sggYrgggK66j53jGxV88726HaqEjcB\"},\"amount\":0,\"parameters\":\"{\\\"entrypoint\\\":\\\"tokenToXtz\\\",\\\"value\\\":{\\\"prim\\\":\\\"Pair\\\",\\\"args\\\":[{\\\"prim\\\":\\\"Pair\\\",\\\"args\\\":[{\\\"string\\\":\\\"tz1Ydpp4ibTrGSPn2i6cqQZ6arWieUbcfWVn\\\"},{\\\"string\\\":\\\"tz1Ydpp4ibTrGSPn2i6cqQZ6arWieUbcfWVn\\\"}]},{\\\"prim\\\":\\\"Pair\\\",\\\"args\\\":[{\\\"int\\\":\\\"16367\\\"},{\\\"prim\\\":\\\"Pair\\\",\\\"args\\\":[{\\\"int\\\":\\\"4817901\\\"},{\\\"string\\\":\\\"2021-02-04T18:09:23Z\\\"}]}]}]}}\",\"status\":\"applied\",\"hasInternals\":true},{\"type\":\"transaction\",\"id\":2455831,\"level\":186174,\"timestamp\":\"2021-02-04T17:49:36Z\",\"block\":\"BLHsAiNmF4pL4P2TEfVzCoQoBdj6bATKYmAjNNsypVVTFy8sZ7Y\",\"hash\":\"ooJNwFGED3ogQreiMfGsT2wqC95gi93Ltcpcd3vmSthmnd9PxdG\",\"counter\":247766,\"sender\":{\"address\":\"tz1Ydpp4ibTrGSPn2i6cqQZ6arWieUbcfWVn\"},\"gasLimit\":26228,\"gasUsed\":25828,\"storageLimit\":258,\"storageUsed\":1,\"bakerFee\":3191,\"storageFee\":250,\"allocationFee\":0,\"target\":{\"address\":\"KT1Dj8A3CFakuzTaqUsehdtFmM85rAJyFgNh\"},\"amount\":0,\"parameters\":\"{\\\"entrypoint\\\":\\\"approve\\\",\\\"value\\\":{\\\"prim\\\":\\\"Pair\\\",\\\"args\\\":[{\\\"string\\\":\\\"KT1NA4sggYrgggK66j53jGxV88726HaqEjcB\\\"},{\\\"int\\\":\\\"16367\\\"}]}}\",\"status\":\"applied\",\"hasInternals\":false}]"
        val list = TzKtOperationListParser().parse(json)
        val fees = list!!.groupFees()

        val expected = Tez("14640")
        Assert.assertEquals(expected, fees)
    }

    @Test
    fun `groupFees +AllocationFee`() {
        val json =
            "[{\"type\":\"transaction\",\"id\":2481461,\"level\":188127,\"timestamp\":\"2021-02-05T10:20:06Z\",\"block\":\"BL5gcPgh6E7dsyVEbYiuqQbNA3MQWe5uTE21QX6JfKGjkb6owZt\",\"hash\":\"opJh5NNW3eMkbHiedRZhPuBWpPmsNvUWukN6xrqGcy3aHw2Efze\",\"counter\":247768,\"initiator\":{\"address\":\"tz1Ydpp4ibTrGSPn2i6cqQZ6arWieUbcfWVn\"},\"sender\":{\"address\":\"KT1NA4sggYrgggK66j53jGxV88726HaqEjcB\"},\"nonce\":0,\"gasLimit\":0,\"gasUsed\":25972,\"storageLimit\":0,\"storageUsed\":42,\"bakerFee\":0,\"storageFee\":0,\"allocationFee\":10500,\"target\":{\"address\":\"KT1Dj8A3CFakuzTaqUsehdtFmM85rAJyFgNh\"},\"amount\":0,\"parameters\":\"{\\\"entrypoint\\\":\\\"transfer\\\",\\\"value\\\":{\\\"prim\\\":\\\"Pair\\\",\\\"args\\\":[{\\\"bytes\\\":\\\"0194e5cf8b46377e10fe6a932b4aeaf0adf8cbc56900\\\"},{\\\"prim\\\":\\\"Pair\\\",\\\"args\\\":[{\\\"bytes\\\":\\\"00008e8fb7d2b514711b03303c3f76e0228b1d644439\\\"},{\\\"int\\\":\\\"6706\\\"}]}]}}\",\"status\":\"applied\",\"hasInternals\":false},{\"type\":\"transaction\",\"id\":2481460,\"level\":188127,\"timestamp\":\"2021-02-05T10:20:06Z\",\"block\":\"BL5gcPgh6E7dsyVEbYiuqQbNA3MQWe5uTE21QX6JfKGjkb6owZt\",\"hash\":\"opJh5NNW3eMkbHiedRZhPuBWpPmsNvUWukN6xrqGcy3aHw2Efze\",\"counter\":247768,\"sender\":{\"address\":\"tz1Ydpp4ibTrGSPn2i6cqQZ6arWieUbcfWVn\"},\"gasLimit\":103234,\"gasUsed\":76862,\"storageLimit\":299,\"storageUsed\":0,\"bakerFee\":10727,\"storageFee\":0,\"allocationFee\":0,\"target\":{\"address\":\"KT1NA4sggYrgggK66j53jGxV88726HaqEjcB\"},\"amount\":2000000,\"parameters\":\"{\\\"entrypoint\\\":\\\"xtzToToken\\\",\\\"value\\\":{\\\"prim\\\":\\\"Pair\\\",\\\"args\\\":[{\\\"string\\\":\\\"tz1Ydpp4ibTrGSPn2i6cqQZ6arWieUbcfWVn\\\"},{\\\"prim\\\":\\\"Pair\\\",\\\"args\\\":[{\\\"int\\\":\\\"6672\\\"},{\\\"string\\\":\\\"2021-02-05T10:39:37Z\\\"}]}]}}\",\"status\":\"applied\",\"hasInternals\":true}]"
        val list = TzKtOperationListParser().parse(json)
        val fees = list!!.groupFees()

        val expected = Tez("21227")
        Assert.assertEquals(expected, fees)
    }
}
