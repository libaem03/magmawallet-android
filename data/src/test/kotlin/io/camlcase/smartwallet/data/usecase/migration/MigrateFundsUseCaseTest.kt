package io.camlcase.smartwallet.data.usecase.migration

import io.camlcase.kotlintezos.TezosNodeClient
import io.camlcase.kotlintezos.model.*
import io.camlcase.kotlintezos.model.operation.fees.BurnFee
import io.camlcase.kotlintezos.model.operation.fees.CalculatedFees
import io.camlcase.kotlintezos.model.operation.fees.ExtraFees
import io.camlcase.kotlintezos.model.operation.fees.OperationFees
import io.camlcase.kotlintezos.operation.RevealService
import io.camlcase.smartwallet.data.core.RxEmitter
import io.camlcase.smartwallet.data.core.Tokens
import io.camlcase.smartwallet.data.core.Tokens.testnetSupportedTokens
import io.camlcase.smartwallet.data.model.AppWallet
import io.camlcase.smartwallet.data.model.Token
import io.camlcase.smartwallet.data.model.UserTezosAddress
import io.camlcase.smartwallet.data.model.error.AppError
import io.camlcase.smartwallet.data.model.error.ErrorType
import io.camlcase.smartwallet.data.model.error.InsufficientXTZ
import io.camlcase.smartwallet.data.source.local.UserPreferences
import io.camlcase.smartwallet.data.test.RxTest
import io.camlcase.smartwallet.data.test.mock.mockWalletLocalSource
import io.github.vjames19.futures.jdk8.Future
import io.mockk.*
import io.reactivex.rxjava3.core.SingleEmitter
import org.junit.After
import org.junit.Assert
import org.junit.Test
import java.math.BigInteger

/**
 * @see [MigrateFundsUseCase]
 */
class MigrateFundsUseCaseTest : RxTest() {
    private val walletLocalSource = mockWalletLocalSource()
    private val emitter = mockk<RxEmitter>(relaxed = true)
    private val tezosNodeClient = mockk<TezosNodeClient>(relaxed = true)
    private val userPreferences = mockk<UserPreferences>(relaxed = true)
    private val supportedFATokens: List<Token> = spyk(testnetSupportedTokens)
    private val revealService = mockk<RevealService>(relaxed = true)

    private fun initUseCase(): MigrateFundsUseCase {
        return spyk(
            MigrateFundsUseCase(
                emitter,
                walletLocalSource,
                tezosNodeClient,
                userPreferences,
                supportedFATokens,
                revealService
            )
        )
    }

    @After
    fun tearDown() {
        confirmVerified(walletLocalSource)
        confirmVerified(tezosNodeClient)
        confirmVerified(userPreferences)
        confirmVerified(supportedFATokens)
        confirmVerified(revealService)
    }

    private fun loadSampleData(
        expectedXTZBalance: Tez = Tez("234555005"),
        migrationWallet: AppWallet = mockk<AppWallet>() {
            every { address } returns "tz1"
        },
        calculatedFeeValue: Tez = Tez("25000"),
        totalFees: CalculatedFees = CalculatedFees(
            listOf(
                OperationFees(calculatedFeeValue, 6000, 10000)
            )
        ),
        delegate: String? = null,
        tokenBalances: Map<String, BigInteger> = emptyMap(),
        needsReveal: Boolean = false
    ) {
        every { walletLocalSource.getMigrationWallet() } returns migrationWallet
        every { emitter.tezosCallback(any<SingleEmitter<CalculatedFees>>(), any()) } answers {
            firstArg<SingleEmitter<CalculatedFees>>().onSuccess(totalFees)
            mockk()
        }
        every { revealService.needsReveal(any(), any()) } returns Future { needsReveal }
        // GIVEN
        every { userPreferences.getTezBalance() } returns expectedXTZBalance
        every { userPreferences.getUserAddress() } returns SOURCE_ADDRESS
        every { userPreferences.getTokenBalance() } returns tokenBalances
        every { userPreferences.getString(UserPreferences.DELEGATE_ADDRESS) } returns delegate
    }

    private fun verifyCalculateFees() {

        verify {
            walletLocalSource.getMigrationWallet()
            walletLocalSource.getWalletOrDie()
            userPreferences.getTezBalance()
            userPreferences.getUserAddress()
            userPreferences.getTokenBalance()
            userPreferences.getString(UserPreferences.DELEGATE_ADDRESS)
            tezosNodeClient.calculateFees(any(), any(), any(), any(), any())
            revealService.needsReveal(any(), any())
        }
    }

    private fun verifyTransferFees() {

        verify {
            walletLocalSource.getMigrationWallet()
            walletLocalSource.getWalletOrDie()
            userPreferences.getTezBalance()
            userPreferences.getUserAddress()
            userPreferences.getTokenBalance()
            userPreferences.getString(UserPreferences.DELEGATE_ADDRESS)
            tezosNodeClient.runOperations(any(), any(), any(), any(), any())
            revealService.needsReveal(any(), any())
        }
    }

    @Test
    fun `calculateFees Only XTZ`() {
        val fee = Tez("25000")
        loadSampleData(
            calculatedFeeValue = fee,
            delegate = null
        )

        // DO
        val useCase = initUseCase()
        useCase.calculateFees()
            .test()
            .assertNoErrors()
            .assertValue {
                Assert.assertTrue(it == fee)
                true
            }

        verifyCalculateFees()
    }

    @Test
    fun `calculateFees XTZ + Known FA12`() {
        val fee = Tez("25000")
        val balances = mutableMapOf(
            Tokens.Testnet.USDTZ.symbol to BigInteger("12345"),
            Tokens.Testnet.TZBTC.symbol to BigInteger("54321")
        )
        loadSampleData(
            tokenBalances = balances,
            delegate = null
        )

        // DO
        val useCase = initUseCase()
        useCase.calculateFees()
            .test()
            .assertNoErrors()
            .assertValue {
                Assert.assertTrue(it == fee)
                true
            }

        verifyCalculateFees()
        verify {
            supportedFATokens.iterator()
            supportedFATokens.iterator()
        }
    }

    @Test
    fun `calculateFees XTZ + FA12`() {
        val fee = Tez("25000")
        val balances = mutableMapOf(
            Tokens.Testnet.USDTZ.symbol to BigInteger("12345"),
            Tokens.Testnet.TZBTC.symbol to BigInteger("54321"),
            "FakeToken" to BigInteger("1")
        )
        loadSampleData(
            tokenBalances = balances,
            delegate = null
        )

        // DO
        val useCase = initUseCase()
        useCase.calculateFees()
            .test()
            .assertNoErrors()
            .assertValue {
                Assert.assertTrue(it == fee)
                true
            }

        verifyCalculateFees()
        verify {
            supportedFATokens.iterator()
            supportedFATokens.iterator()
        }
    }

    /**
     * It will create two operations to calculate: XTZ + undelegate
     */
    @Test
    fun `calculateFees XTZ-Delegate`() {
        val fee = Tez("25000")
        loadSampleData(
            calculatedFeeValue = fee,
            delegate = "tz1_SomeDelegate"
        )
        // DO
        val useCase = initUseCase()
        useCase.calculateFees()
            .test()
            .assertNoErrors()
            .assertValue {
                Assert.assertTrue(it == fee)
                true
            }

        verifyCalculateFees()
    }

    @Test
    fun `calculateFees 0-5 XTZ`() {
        loadSampleData(
            expectedXTZBalance = Tez(0.5)
        )
        val useCase = initUseCase()
        useCase.calculateFees()
            .test()
            .assertError {
                Assert.assertTrue(it is InsufficientXTZ)
                true
            }

        verify {
            userPreferences.getTezBalance()
        }
    }

    @Test
    fun `calculateFees 0-5_1 XTZ`() {
        val fee = Tez("25000")
        loadSampleData(
            calculatedFeeValue = fee,
            expectedXTZBalance = Tez(0.500_001)
        )
        val useCase = initUseCase()
        useCase.calculateFees()
            .test()
            .assertNoErrors()
            .assertValue {
                Assert.assertTrue(it == fee)
                true
            }

        verifyCalculateFees()
    }

    @Test
    fun `calculateFees No Wallet`() {
        val expectedXTZBalance = Tez(0.500_001)
        every { walletLocalSource.getMigrationWallet() } returns null
        every { userPreferences.getTezBalance() } returns expectedXTZBalance

        val useCase = initUseCase()
        useCase.calculateFees()
            .test()
            .assertError {
                Assert.assertTrue(it is AppError)
                Assert.assertTrue((it as AppError).type == ErrorType.NO_WALLET)
                true
            }

        verify {
            userPreferences.getTezBalance()
            walletLocalSource.getMigrationWallet()
        }
    }

    @Test
    fun `calculateFees Burn Fees`() {
        val fee = Tez("25000")
        val burnFee = BurnFee(Tez("250"))
        val extraFees = ExtraFees()
        extraFees.add(burnFee)
        val totalFees = CalculatedFees(listOf(OperationFees(fee, 6000, 10000, extraFees)))
        loadSampleData(
            calculatedFeeValue = fee,
            totalFees = totalFees,
            delegate = null
        )

        // DO
        val useCase = initUseCase()
        useCase.calculateFees()
            .test()
            .assertNoErrors()
            .assertValue {
                Assert.assertTrue(it == (fee + burnFee.fee))
                true
            }

        verifyCalculateFees()
    }

    // XTZ to send will be 0 but will try to send the other operations
    // EDGE CASE: User only has XTZ and is equal to fees. It will try to send an empty list and will get an error
    // This shouldn't happen. Fees are not gonna be >= 0.5 XTZ.
    @Test
    fun `transferFunds Fees EQUAL Tz Balance`() {
        val fee = Tez(0.5)
        val tezBalance = Tez(0.5)
        loadSampleData(
            expectedXTZBalance = tezBalance
        )
        val operationHash = "oo1234"
        every { emitter.tezosCallback(any<SingleEmitter<String>>(), any()) } answers {
            firstArg<SingleEmitter<String>>().onError(TezosError(TezosErrorType.RPC_ERROR, listOf(RPCErrorResponse(RPCErrorType.TEMPORARY, "Something something"))))
            mockk()
        }

        // DO
        val useCase = initUseCase()
        useCase.transferFunds(fee)
            .test()
            .assertError {
                Assert.assertTrue(it is TezosError)
                true
            }

       verifyTransferFees()
    }

    @Test
    fun `transferFunds Fees NO Migration Wallet`() {
        every { walletLocalSource.getMigrationWallet() } returns null
        val fee = Tez(0.5)

        // DO
        val useCase = initUseCase()
        useCase.transferFunds(fee)
            .test()
            .assertError {
                Assert.assertTrue(it is AppError)
                Assert.assertTrue((it as AppError).type == ErrorType.NO_WALLET)
                true
            }

        verify {
            walletLocalSource.getMigrationWallet()
        }
    }

    @Test
    fun `transferFunds Fees NO User Wallet`() {
        every { walletLocalSource.getMigrationWallet() } returns mockk<AppWallet>() {
            every { address } returns "tz1"
        }
        every { walletLocalSource.getWalletOrDie() } throws AppError(ErrorType.NO_WALLET)
        val fee = Tez(0.5)

        // DO
        val useCase = initUseCase()
        try {
            useCase.transferFunds(fee)
                .test()
        }catch (e: AppError){
            Assert.assertTrue(e.type == ErrorType.NO_WALLET)
        }

        verify {
            walletLocalSource.getMigrationWallet()
            walletLocalSource.getWalletOrDie()
        }
    }

    @Test
    fun `transferFunds Fees MORE Tz Balance`() {
        val fee = Tez(0.500_001)
        val tezBalance = Tez(0.5)
        loadSampleData(
            expectedXTZBalance = tezBalance
        )

        // DO
        val useCase = initUseCase()
        useCase.transferFunds(fee)
            .test()
            .assertError {
                Assert.assertTrue(it is InsufficientXTZ)
                true
            }

        verify {
            walletLocalSource.getMigrationWallet()
            walletLocalSource.getWalletOrDie()
            userPreferences.getTezBalance()
            userPreferences.getUserAddress()
            userPreferences.getTokenBalance()
            userPreferences.getString(UserPreferences.DELEGATE_ADDRESS)
        }
    }

    @Test
    fun `transferFunds Fees Only XTZ`() {
        val fee = Tez(0.000_500)
        val tezBalance = Tez(0.5)
        loadSampleData(
            expectedXTZBalance = tezBalance
        )
        val operationHash = "oo1234"
        every { emitter.tezosCallback(any<SingleEmitter<String>>(), any()) } answers {
            firstArg<SingleEmitter<String>>().onSuccess(operationHash)
            mockk()
        }

        // DO
        val useCase = initUseCase()
        useCase.transferFunds(fee)
            .test()
            .assertNoErrors()
            .assertValue {
                Assert.assertTrue(it == operationHash)
                true
            }

        verifyTransferFees()
    }

    companion object {
        val SOURCE_ADDRESS = UserTezosAddress("tz1_OldAddress")
    }
}
