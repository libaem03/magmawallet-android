///**
// * # Released under MIT License
// *
// * Copyright (c) 2020 camlCase
// *
// * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
// * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
// * permit persons to whom the Software is furnished to do so, subject to the following conditions:
// *
// * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
// * the Software.
// *
// * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
// * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
// * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// * SOFTWARE.
// */
//package io.camlcase.smartwallet.data.model
//
//import io.camlcase.kotlintezos.model.Tez
//import junit.framework.TestCase.assertEquals
//import org.junit.Test
//
//class CoinBalanceTest {
//
//    @Test
//    fun `Multiply balance 0x0-12345`() {
//        val input = 0.0
//        val multiplier = 0.12345
//        multiplyTest(input, multiplier)
//    }
//
//    @Test
//    fun `Multiply balance 0-567x0-12345`() {
//        val input = 0.567
//        val multiplier = 0.12345
//        multiplyTest(input, multiplier)
//    }
//
//    @Test
//    fun `Multiply balance 1x0-12345`() {
//        val input = 1.0
//        val multiplier = 0.12345
//        multiplyTest(input, multiplier)
//    }
//
//    @Test
//    fun `Multiply balance 2x0-12345`() {
//        val input = 2.0
//        val multiplier = 0.12345
//        multiplyTest(input, multiplier)
//    }
//
//    @Test
//    fun `Multiply balance 2x3-12345`() {
//        val input = 2.0
//        val multiplier = 3.12345
//        multiplyTest(input, multiplier)
//    }
//
//    @Test
//    fun `Multiply balance 2-8976x3-12345`() {
//        val input = 2.8976
//        val multiplier = 3.12345
//        multiplyTest(input, multiplier)
//    }
//
//    @Test
//    fun `Multiply balance 1_209-098976x3-123458`() {
//        val input = 1_209.098976
//        val multiplier = 3.123458
//        multiplyTest(input, multiplier)
//    }
//
//    private fun multiplyTest(input: Double, multiplier: Double){
//        val expected = input * multiplier
//
//        val balance = CoinBalance(Tez(input))
//        val result = balance * multiplier
//        assertEquals(Tez(expected), result)
//    }
//}
