package io.camlcase.smartwallet.data.usecase.user

import io.camlcase.kotlintezos.wallet.HDWallet
import io.camlcase.smartwallet.data.model.AppWallet
import io.camlcase.smartwallet.data.model.error.AppError
import io.camlcase.smartwallet.data.model.error.ErrorType
import io.camlcase.smartwallet.data.test.mock.mnemonics
import io.camlcase.smartwallet.data.test.mock.mockWalletLocalSource
import io.mockk.*
import io.reactivex.rxjava3.core.Single
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class GetSeedPhraseUseCaseTest {
    private val walletLocalSource = mockWalletLocalSource()
    private lateinit var useCase: GetSeedPhraseUseCase

    private fun init(): GetSeedPhraseUseCase {
        useCase = spyk(GetSeedPhraseUseCase(walletLocalSource))
        return useCase
    }

    @Before
    fun before() {
        MockKAnnotations.init(this)
        mockkConstructor(AppWallet::class)
    }

    @After
    fun tearDown() {
        confirmVerified(walletLocalSource)
        confirmVerified(useCase)
    }

    @Test
    fun `hasPassphrase TRUE`() {
        every { walletLocalSource.getWalletOrDie() } returns Single.just(
            mockk<AppWallet>(relaxed = true) {
                every { mnemonic } returns mnemonics
                every { passphrase } returns "magma"
                every { wallet } returns mockk<HDWallet>(relaxed = true)
            }
        )

        init().hasPassphrase()
            .test()
            .assertNoErrors()
            .assertValue {
                Assert.assertTrue(it)

                verify {
                    walletLocalSource.getWalletOrDie()
                    useCase.hasPassphrase()
                }

                true
            }
    }

    @Test
    fun `hasPassphrase FALSE`() {
        every { walletLocalSource.getWalletOrDie() } returns Single.just(
            mockk<AppWallet>(relaxed = true) {
                every { mnemonic } returns mnemonics
                every { passphrase } returns null
                every { wallet } returns mockk<HDWallet>(relaxed = true)
            }
        )

        init().hasPassphrase()
            .test()
            .assertNoErrors()
            .assertValue {
                Assert.assertFalse(it)

                verify {
                    walletLocalSource.getWalletOrDie()
                    useCase.hasPassphrase()
                }

                true
            }
    }

    @Test
    fun `hasPassphrase KO`() {
        every { walletLocalSource.getWalletOrDie() } returns Single.error(AppError(ErrorType.NO_WALLET))

        init().hasPassphrase()
            .test()
            .assertNoValues()
            .assertError {
                Assert.assertTrue(it is AppError)

                verify {
                    walletLocalSource.getWalletOrDie()
                    useCase.hasPassphrase()
                }

                true
            }
    }
}
