package io.camlcase.smartwallet.data.usecase.operation

import io.camlcase.kotlintezos.exchange.calculations.AndroidJSDexterCalculations
import io.camlcase.smartwallet.data.test.RxTest
import io.mockk.*
import org.junit.After
import org.junit.Test
import java.io.IOException

class DexterCalculationsUseCaseTest : RxTest() {
    private val jsDexterCalculations = mockk<AndroidJSDexterCalculations>(relaxed = true)
    private lateinit var useCase: DexterCalculationsUseCase

    private fun initUseCase() {
        useCase = spyk(DexterCalculationsUseCase(jsDexterCalculations))
    }

    @After
    fun tearDown() {
        confirmVerified(jsDexterCalculations)
        confirmVerified(useCase)
    }

    @Test
    fun `init OK`() {
        initUseCase()
        useCase.init()
            .test()
            .assertNoErrors()
        verify {
            useCase.init()
            jsDexterCalculations.initReading()
        }
    }

    @Test
    fun `init KO`() {
        every { jsDexterCalculations.initReading() } throws IOException("")
        initUseCase()
        useCase.init()
            .test()
            .assertError {
                it is IOException
            }
        verify {
            useCase.init()
            jsDexterCalculations.initReading()
        }
    }
}
