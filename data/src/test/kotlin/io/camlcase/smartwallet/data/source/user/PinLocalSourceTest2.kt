package io.camlcase.smartwallet.data.source.user

import io.camlcase.smartwallet.data.core.security.CryptoUtils
import io.camlcase.smartwallet.data.model.error.AppError
import io.camlcase.smartwallet.data.source.local.SecurePreferences
import io.mockk.every
import io.mockk.mockk
import io.mockk.mockkStatic
import io.mockk.spyk
import org.junit.Before
import org.junit.Test
import java.security.NoSuchAlgorithmException
import javax.crypto.SecretKeyFactory.getInstance

/**
 * Same as [PinLocalSourceTest] but the javax.crypto giving errors
 */
class PinLocalSourceTest2 {
    @Before
    fun mockInteractions() {
        mockkStatic("javax.crypto.SecretKeyFactory")
        every { getInstance(any()) } throws NoSuchAlgorithmException()
    }

    @Test(expected = AppError::class)
    fun test_NoPassword() {
        val securePreferences = mockk<SecurePreferences>(relaxed = true) {
            every { getString(SecurePreferences.KEY_PIN) } returns null
            every { getString(SecurePreferences.KEY_PIN_SALT) } returns "" // to avoid generating the salt
        }
        val input = "blabla"
        val pinLocalSource = spyk(PinLocalSource(securePreferences))

        pinLocalSource.comparePin(input)
    }

    @Test(expected = AppError::class)
    fun test_FilledPassword_NoMatch() {
        val salt = CryptoUtils.generateSalt()
        val hashedPin = CryptoUtils.encryptWithSalt("storedPin", salt)

        val securePreferences = mockk<SecurePreferences>(relaxed = true) {
            every { getString(SecurePreferences.KEY_PIN) } returns hashedPin
            every { getString(SecurePreferences.KEY_PIN_SALT) } returns salt
        }
        val input = "storedPin1"
        val pinLocalSource = PinLocalSource(securePreferences)

        pinLocalSource.comparePin(input)

    }

    @Test(expected = AppError::class)
    fun test_FilledPassword_Match() {
        val salt = CryptoUtils.generateSalt()
        val hashedPin = CryptoUtils.encryptWithSalt("storedPin", salt)

        val securePreferences = mockk<SecurePreferences>(relaxed = true) {
            every { getString(SecurePreferences.KEY_PIN) } returns hashedPin
            every { getString(SecurePreferences.KEY_PIN_SALT) } returns salt
        }
        val input = "storedPin"
        val pinLocalSource = PinLocalSource(securePreferences)
        pinLocalSource.comparePin(input)
    }
}
