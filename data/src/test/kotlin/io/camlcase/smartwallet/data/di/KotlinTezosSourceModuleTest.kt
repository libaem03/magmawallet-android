package io.camlcase.smartwallet.data.di

import io.camlcase.smartwallet.data.core.api.ConnectionInterceptor
import io.camlcase.smartwallet.data.source.local.AppPreferences
import io.mockk.*
import org.junit.After
import org.junit.Assert
import org.junit.Test

class KotlinTezosSourceModuleTest {
    var indexterUrl: String = "http://localhost:8080"
    var debug: Boolean = false
    var flavor: String = "mainnet"
    val appPreferences = mockk<AppPreferences>(relaxed = true) {
        every { getNetworkConfig() } returns null
    }

    @After
    fun tearDown() {
        confirmVerified(appPreferences)
    }

    fun createModule(): KotlinTezosSourceModule {
        return spyk(
            KotlinTezosSourceModule(
                debug,
                flavor
            )
        )
    }

    @Test
    fun `Constructor OK`() {
        val result = createModule()
        Assert.assertNotNull(result)
    }

    @Test
    fun `provideTzKtClient OK`() {
        val module = createModule()
        val client = module.provideNetworkClient(mockk<ConnectionInterceptor>(), mockk())
        val result = module.provideTzKtClient(client, appPreferences, true)
        Assert.assertNotNull(result)

        verify {
            appPreferences.getNetworkConfig()
        }
    }

    @Test
    fun `provideBCDClient OK`() {
        val module = createModule()
        val client = module.provideNetworkClient(mockk<ConnectionInterceptor>(), mockk())
        val result = module.provideBCDClient(client, appPreferences, true)
        Assert.assertNotNull(result)
        verify {
            appPreferences.getNetworkConfig()
        }
    }
}
