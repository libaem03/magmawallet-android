package io.camlcase.smartwallet.data.core.extension

import io.camlcase.kotlintezos.model.Tez
import io.camlcase.smartwallet.data.core.extension.tezos.convertToCurrency
import junit.framework.TestCase.assertEquals
import org.junit.Test
import java.math.BigDecimal

class ConvertToUSDTest {

    @Test
    fun `Convert from 0 XTZ to 0 USD`() {
        val expected = BigDecimal("00.0000000")
        val inputTez = Tez(0.0)
        val priceDollar = 0.0

        val result = inputTez.convertToCurrency(priceDollar)
        assertEquals(expected, result)
    }

    @Test
    fun `Convert from 0 XTZ to 1 USD`() {
        val expected = BigDecimal("00.0000000")
        val inputTez = Tez(0.0)
        val priceDollar = 1.0

        val result = inputTez.convertToCurrency(priceDollar)
        assertEquals(expected, result)
    }

    @Test
    fun `Convert from null XTZ to 1 USD`() {
        val expected = BigDecimal(0)
        val inputTez: Tez? = null
        val priceDollar = 1.0

        val result = inputTez.convertToCurrency(priceDollar)
        assertEquals(expected, result)
    }

    @Test
    fun `Convert from 0-000123 XTZ to 1-00897 USD`() {
        val expected = BigDecimal("0.00012410331")
        val inputTez = Tez("123") // 0.000123 XTC
        val priceDollar = 1.00897

        val result = inputTez.convertToCurrency(priceDollar)
        assertEquals(expected, result)
    }

    @Test
    fun `Convert from 0-000001 XTZ to 1-00897 USD`() {
        val expected = BigDecimal("0.00000100897")
        val inputTez = Tez("1") // 0.000001 XTC
        val priceDollar = 1.00897

        val result = inputTez.convertToCurrency(priceDollar)
        assertEquals(expected, result)
    }

    @Test
    fun `Convert from 1_239_789-09 XTZ to 1-9159307801919423 USD`() {
        val expected = BigDecimal("2375350.0784771581694495070000")
        val inputTez = Tez(1_239_789.09)
        val priceDollar = 1.9159307801919423

        val result = inputTez.convertToCurrency(priceDollar)
        assertEquals(expected, result)
    }
}
