package io.camlcase.smartwallet.data.usecase.operation

import io.camlcase.kotlintezos.TezosNodeClient
import io.camlcase.kotlintezos.model.TezosProtocol
import io.camlcase.smartwallet.data.core.RxEmitter
import io.camlcase.smartwallet.data.source.local.UserPreferences
import io.camlcase.smartwallet.data.test.RxTest
import io.camlcase.smartwallet.data.test.mock.mockUserPreferences
import io.camlcase.smartwallet.data.test.mock.mockWalletLocalSource
import io.camlcase.smartwallet.data.usecase.analytics.AnalyticEvent
import io.camlcase.smartwallet.data.usecase.analytics.SendAnalyticUseCase
import io.mockk.confirmVerified
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.reactivex.rxjava3.core.SingleEmitter
import org.junit.After
import org.junit.Assert
import org.junit.Test
import java.io.IOException

/**
 * @see DelegateUseCase
 */

class DelegateUseCaseTest : RxTest() {
    private val walletLocalSource = mockWalletLocalSource()
    private val emitter = mockk<RxEmitter>(relaxed = true)
    private val tezosNodeClient = mockk<TezosNodeClient>(relaxed = true)
    private val userPreferences = mockUserPreferences()
    private val tezosProtocol = TezosProtocol.CARTHAGE
    private val analytics = mockk<SendAnalyticUseCase>(relaxed = true)

    private fun initUseCase(): DelegateUseCase {
        return DelegateUseCase(
            emitter,
            walletLocalSource,
            tezosNodeClient,
            userPreferences,
            tezosProtocol,
            analytics
        )
    }

    @Test
    fun `Test delegate OK`() {
        val expectedOperationHash = "ooo1234"
        val expectedBaker = "tz1"
        every { emitter.tezosCallback(any<SingleEmitter<*>>(), any()) } answers {
            firstArg<SingleEmitter<String>>().onSuccess(expectedOperationHash)
            mockk()
        }
        val useCase = initUseCase()
        useCase.delegate(expectedBaker)
            .test()
            .assertNoErrors()
            .assertValue {
                Assert.assertTrue(it == expectedOperationHash)
                true
            }

        verify {
            walletLocalSource.getWalletOrDie()
            tezosNodeClient.delegate(any(), any(), any(), any(), any())
            userPreferences.store(UserPreferences.DELEGATE_ADDRESS, expectedBaker)
        }
    }

    @Test
    fun `delegate KO`() {
        val expected = "tz1BAKER"
        val expectedError = IOException("Error")
        every { emitter.tezosCallback(any<SingleEmitter<*>>(), any()) } answers {
            firstArg<SingleEmitter<String>>().onError(expectedError)
            mockk()
        }

        val useCase = initUseCase()
        useCase.delegate(expected)
            .test()
            .assertError { error ->
                Assert.assertTrue(error == expectedError)
                true
            }

        verify {
            walletLocalSource.getWalletOrDie()
            tezosNodeClient.delegate(any(), any(), any(), any(), any())
            analytics.reportEvent(AnalyticEvent.BAKER_DELEGATED_ERROR)
        }
    }

    @Test
    fun `undelegate OK`() {
        val expectedOperationHash = "ooo1234"
        every { emitter.tezosCallback(any<SingleEmitter<*>>(), any()) } answers {
            firstArg<SingleEmitter<String>>().onSuccess(expectedOperationHash)
            mockk()
        }
        val useCase = initUseCase()
        useCase.undelegate()
            .test()
            .assertNoErrors()
            .assertValue {
                Assert.assertTrue(it == expectedOperationHash)
                true
            }

        verify {
            walletLocalSource.getWalletOrDie()
            tezosNodeClient.undelegate(any(), any(), any(), any())
            userPreferences.clearPreference(UserPreferences.DELEGATE_ADDRESS)
        }
    }

    @Test
    fun `undelegate KO`() {
        val error = IOException("Error")
        every { emitter.tezosCallback(any<SingleEmitter<*>>(), any()) } answers {
            firstArg<SingleEmitter<String>>().onError(error)
            mockk()
        }
        val useCase = initUseCase()
        useCase.undelegate()
            .test()
            .assertError {
                Assert.assertTrue(it == error)
                true
            }

        verify {
            walletLocalSource.getWalletOrDie()
            tezosNodeClient.undelegate(any(), any(), any(), any())
            analytics.reportEvent(AnalyticEvent.BAKER_UNDELEGATED_ERROR)
        }
    }

    @After
    fun after() {
        confirmVerified(walletLocalSource)
        confirmVerified(tezosNodeClient)
        confirmVerified(userPreferences)
        confirmVerified(analytics)
    }
}
