package io.camlcase.smartwallet.data.core.api

import io.camlcase.smartwallet.data.test.JSONTestLoader
import org.junit.Assert
import org.junit.Test

class OperationErrorParserTest {

    @Test
    fun `parse General Error`() {
        val json =
            "[{\"kind\":\"temporary\",\"id\":\"proto.007-PsDELPH1.contract.balance_too_low\",\"contract\":\"tz1crghTXjGHpecfMsoMcvV8FA3JqUkBnDEn\",\"balance\":\"500\",\"amount\":\"1268\"}]"
        val result = OperationErrorParser().parse(json)
        println(result)
        Assert.assertTrue(result?.size == 1)
    }

    @Test
    fun `parse Counter error`() {
        val input =
            "[{\"kind\":\"temporary\",\"id\":\"failure\",\"msg\":\"Error while applying operation oopQDQ9HumjwcPmtQMU75u5NiL13bE5aCFpxK85TfH8gdLDVZES:\\nbranch refused (Error:\\n                  Counter 561557 already used for contract tz1iuj52evxucSGhhyUq1S5bPN3uax1E9BNQ (expected 561558)\\n)\"}]"
        val result = OperationErrorParser().parse(input)
        println(result)
        Assert.assertTrue(result?.size == 1)
        Assert.assertTrue(result!![0].message!!.startsWith("Error while applying operation"))
    }

    @Test
    fun `parse Exhausted gas`() {
        val input =
            "{\"contents\":[{\"kind\":\"transaction\",\"source\":\"tz1XVJ8bZUXs7r5NV8dHvuiBhzECvLRLR3jW\",\"fee\":\"1\",\"counter\":\"31127\",\"gas_limit\":\"0\",\"storage_limit\":\"10000\",\"amount\":\"10000000000000000\",\"destination\":\"KT1D5jmrBD7bDa3jCpgzo32FMYmRDdK2ihka\",\"metadata\":{\"balance_updates\":[{\"kind\":\"contract\",\"contract\":\"tz1XVJ8bZUXs7r5NV8dHvuiBhzECvLRLR3jW\",\"change\":\"-1\"},{\"kind\":\"freezer\",\"category\":\"fees\",\"delegate\":\"tz1Ke2h7sDdakHJQh8WX4Z372du1KChsksyU\",\"cycle\":284,\"change\":\"1\"}],\"operation_result\":{\"status\":\"failed\",\"errors\":[{\"kind\":\"temporary\",\"id\":\"proto.004-Pt24m4xi.gas_exhausted.operation\"}]}}}]}"
        val result = OperationErrorParser().parse(input)
        println(result)
        Assert.assertTrue(result?.size == 1)
        Assert.assertTrue(result!![0].cause == "proto.004-Pt24m4xi.gas_exhausted.operation")
    }

    @Test
    fun `parse Internal operations fail`() {
        val input = JSONTestLoader.getStringFromFile("token_balance_run_operation_simulation.json")

        val result = OperationErrorParser().parse(input)
        println(result)
        Assert.assertTrue(result?.size == 2)
        Assert.assertTrue(result!![0].cause == "proto.006-PsCARTHA.michelson_v1.runtime_error")
        Assert.assertTrue(result!![1].cause == "proto.006-PsCARTHA.michelson_v1.script_rejected")
        Assert.assertTrue(result!![1].contractArgs!!.size == 2)
    }

    @Test
    fun `parse Correct response`() {
        val json =
            "[{\"type\":\"transaction\",\"id\":4039024,\"level\":301741,\"timestamp\":\"2020-12-24T13:28:06Z\",\"block\":\"BLGrDJeb39qv3Q2tei4LePNYxeHUhAGDkDmKKLk7kEFLc3EvYha\",\"hash\":\"ooYfX4TTeLTL8UBbEQ7eJgxL19hkbHnvBstu59cxGAoJQSrPhn3\",\"counter\":780758,\"sender\":{\"address\":\"tz1Ydpp4ibTrGSPn2i6cqQZ6arWieUbcfWVn\"},\"gasLimit\":1827,\"gasUsed\":1427,\"storageLimit\":257,\"storageUsed\":0,\"bakerFee\":496,\"storageFee\":0,\"allocationFee\":64250,\"target\":{\"address\":\"tz1crghTXjGHpecfMsoMcvV8FA3JqUkBnDEn\"},\"amount\":500,\"status\":\"applied\",\"hasInternals\":false},{\"type\":\"transaction\",\"id\":2586770,\"level\":198540,\"timestamp\":\"2020-11-16T17:32:39Z\",\"block\":\"BMYTTT6qXgVtY1nCyZLrAHd1bNxQsETBa6u8HvRGmSoqk7e9pzz\",\"hash\":\"ooyFCMpjDcHVfomCnHixf3fArfSTg81nMuA5f3CpEcv8uXv38Hv\",\"counter\":783709,\"sender\":{\"address\":\"tz1crghTXjGHpecfMsoMcvV8FA3JqUkBnDEn\"},\"gasLimit\":1827,\"gasUsed\":1427,\"storageLimit\":257,\"storageUsed\":0,\"bakerFee\":1621,\"storageFee\":0,\"allocationFee\":64250,\"target\":{\"address\":\"tz1MBB3wYuwunkC3Mtw6FpVzc689q4bLrvNP\"},\"amount\":1126879,\"status\":\"applied\",\"hasInternals\":false},{\"type\":\"transaction\",\"id\":2586625,\"level\":198528,\"timestamp\":\"2020-11-16T17:26:39Z\",\"block\":\"BKqt6VP186zBwobNKnDEYPp2Xr3puS3c5oZ84GPjGbkMMKhXb3G\",\"hash\":\"onxWbibbyA8jtPnmUZYk7XD8M1auqemahe7eDVdxXaAzDrmdjLx\",\"counter\":780687,\"sender\":{\"address\":\"tz1YCYVCQqFCtjQAQH5sTkkCkPkdMBQTfEn5\"},\"gasLimit\":1527,\"gasUsed\":1427,\"storageLimit\":257,\"storageUsed\":0,\"bakerFee\":1591,\"storageFee\":0,\"allocationFee\":0,\"target\":{\"address\":\"tz1crghTXjGHpecfMsoMcvV8FA3JqUkBnDEn\"},\"amount\":1000000,\"status\":\"applied\",\"hasInternals\":false},{\"type\":\"transaction\",\"id\":2496149,\"level\":190559,\"timestamp\":\"2020-11-13T19:12:53Z\",\"block\":\"BL41hJYT9TEK7aaBmQq5utmXtGeJjxB6hM21evUEbrUpanr5DzL\",\"hash\":\"ooaGwC2dTA86XrWWQDH15aaybmYjPiKgUHcMU8ibucLaaPfSDHo\",\"counter\":783708,\"sender\":{\"address\":\"tz1crghTXjGHpecfMsoMcvV8FA3JqUkBnDEn\"},\"gasLimit\":1527,\"gasUsed\":1427,\"storageLimit\":257,\"storageUsed\":0,\"bakerFee\":1657,\"storageFee\":0,\"allocationFee\":64250,\"target\":{\"address\":\"tz1VoVgZNeK4CYN2K4J1VmrB5sQtWQwxkD5N\"},\"amount\":28305659,\"status\":\"applied\",\"hasInternals\":false},{\"type\":\"reveal\",\"id\":2496148,\"level\":190559,\"timestamp\":\"2020-11-13T19:12:53Z\",\"block\":\"BL41hJYT9TEK7aaBmQq5utmXtGeJjxB6hM21evUEbrUpanr5DzL\",\"hash\":\"ooaGwC2dTA86XrWWQDH15aaybmYjPiKgUHcMU8ibucLaaPfSDHo\",\"sender\":{\"address\":\"tz1crghTXjGHpecfMsoMcvV8FA3JqUkBnDEn\"},\"counter\":783707,\"gasLimit\":1100,\"gasUsed\":1000,\"bakerFee\":1598,\"status\":\"applied\"},{\"type\":\"transaction\",\"id\":2493707,\"level\":190376,\"timestamp\":\"2020-11-13T17:32:07Z\",\"block\":\"BMQ3BjGkjo3mD8MCHxiSdcPDibwTh6MvXMLzVeu4jbaxAVkxduB\",\"hash\":\"opAU6xrDxzBvUXCtkbosHSVkj2tc9fcQMEJHrERWF6hrD9mgWjL\",\"counter\":780747,\"sender\":{\"address\":\"tz1Ydpp4ibTrGSPn2i6cqQZ6arWieUbcfWVn\"},\"gasLimit\":1527,\"gasUsed\":1427,\"storageLimit\":257,\"storageUsed\":0,\"bakerFee\":1625,\"storageFee\":0,\"allocationFee\":64250,\"target\":{\"address\":\"tz1crghTXjGHpecfMsoMcvV8FA3JqUkBnDEn\"},\"amount\":28565914,\"status\":\"applied\",\"hasInternals\":false}]"
        val result = OperationErrorParser().parse(json)
        println(result)
        Assert.assertTrue(result?.size == 0)
    }

    @Test
    fun `parse Not json`() {
        val json =
            "0bb0b3468dfc9df33600a4c1d9b35a17ac9b32d25d456a4d4ea550185f1ad0956b00bcdedb052d67e209d32a4caf333f806f21a681caf409cab83c904e0000d60ef79535a8a7a4591e444d15f1c5d0fcf8ba7f955aad5e3b69decda932a5e46c00bcdedb052d67e209d32a4caf333f806f21a681ca00cbb83c80ea30e0d40305000089c7b0fbb4d55dbcbd5b80c9e31edc4924bb835200"
        val result = OperationErrorParser().parse(json)
        println(result)
        Assert.assertTrue(result == null)
    }
}
