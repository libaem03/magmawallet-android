package io.camlcase.smartwallet.data.usecase.info

import io.camlcase.kotlintezos.BCDClient
import io.camlcase.kotlintezos.data.parser.bcd.BCDAccountTokenBalanceResponseParser
import io.camlcase.kotlintezos.model.Tez
import io.camlcase.kotlintezos.model.TezosNetwork
import io.camlcase.kotlintezos.model.bcd.BCDAccount
import io.camlcase.kotlintezos.model.bcd.dto.map
import io.camlcase.smartwallet.data.core.RxEmitter
import io.camlcase.smartwallet.data.core.Tokens
import io.camlcase.smartwallet.data.core.Tokens.testnetFATokens
import io.camlcase.smartwallet.data.core.XTZ
import io.camlcase.smartwallet.data.model.UserTezosAddress
import io.camlcase.smartwallet.data.model.exchange.DexterBalance
import io.camlcase.smartwallet.data.model.exchange.FATokenInfo
import io.camlcase.smartwallet.data.model.exchange.TokenInfoBundle
import io.camlcase.smartwallet.data.model.exchange.XTZTokenInfo
import io.camlcase.smartwallet.data.source.local.UserPreferences
import io.camlcase.smartwallet.data.source.token.TokenBalanceLocalSource
import io.camlcase.smartwallet.data.test.RxTest
import io.camlcase.smartwallet.data.test.mock.ADDRESS
import io.camlcase.smartwallet.data.usecase.GetExchangeUseCase
import io.mockk.*
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.core.SingleEmitter
import org.junit.After
import org.junit.Assert
import org.junit.Test
import java.util.*

class GetBCDBalanceUseCaseTest : RxTest() {
    private val supportedFATokens = testnetFATokens
    private val network = TezosNetwork.EDONET
    private val emitter = mockk<RxEmitter>(relaxed = true)
    private val client = mockk<BCDClient>(relaxed = true)
    private val tokenLocalSource = mockk<TokenBalanceLocalSource>(relaxed = true)
    private val userPreferences = mockk<UserPreferences>(relaxed = true)
    private val xtzCurrencyUseCase = mockk<GetXTZCurrencyExchange>(relaxed = true)
    private val exchangeUseCase = mockk<GetExchangeUseCase>(relaxed = true)

    private lateinit var useCase: GetBCDBalanceUseCase

    fun init(): GetBCDBalanceUseCase {
        useCase = spyk(
            GetBCDBalanceUseCase(
                supportedFATokens,
                network,
                emitter,
                client,
                tokenLocalSource,
                userPreferences,
                xtzCurrencyUseCase,
                exchangeUseCase
            )
        )
        return useCase
    }

    @After
    fun tearDown() {
        confirmVerified(emitter)
        confirmVerified(client)
        confirmVerified(tokenLocalSource)
        confirmVerified(userPreferences)
        confirmVerified(xtzCurrencyUseCase)
        confirmVerified(exchangeUseCase)
    }

    @Test
    fun `getBalances filter NFT`() {
        // 5 tokens
        val jsonTokenInput =
            "{\"balances\":[{\"contract\":\"KT1PrNd3sy1pLAqGtft47dzG4v8KizqPJntT\",\"network\":\"edo2net\",\"level\":69539,\"token_id\":12,\"name\":\"Mystery Map: Piece 12\",\"decimals\":0,\"description\":\"A piece to a most mysterious puzzle. Where it leads is uncertain, but an adventure lies ahead.\",\"display_uri\":\"ipfs://QmbduhWkxhTQmPTj1sZCABnCbzPTug5mF5JVXRmPmKAhtH\",\"thumbnail_uri\":\"ipfs://QmbduhWkxhTQmPTj1sZCABnCbzPTug5mF5JVXRmPmKAhtH\",\"is_transferable\":true,\"balance\":\"1\"},{\"contract\":\"KT1RUSCZ7pJ3WNTuXFD44UpStmNRjA459guZ\",\"network\":\"edo2net\",\"level\":105565,\"token_id\":6,\"name\":\"Mystery Map Test: Piece 6\",\"decimals\":0,\"description\":\"A piece to a most mysterious puzzle. Where it leads is uncertain, but an adventure lies ahead.\",\"display_uri\":\"ipfs://QmVDGHNm5ju9hNvbguutAnNuUVVUxaz5BxxgsUSc2NdBwr\",\"thumbnail_uri\":\"ipfs://QmVDGHNm5ju9hNvbguutAnNuUVVUxaz5BxxgsUSc2NdBwr\",\"is_transferable\":true,\"balance\":\"1\"},{\"contract\":\"KT1PrNd3sy1pLAqGtft47dzG4v8KizqPJntT\",\"network\":\"edo2net\",\"level\":69539,\"token_id\":5,\"name\":\"Mystery Map: Piece 5\",\"decimals\":0,\"description\":\"A piece to a most mysterious puzzle. Where it leads is uncertain, but an adventure lies ahead.\",\"display_uri\":\"ipfs://QmPHGj4qgiapAGK5BuWin1JNWETQskdprb46JYmifkG7Gf\",\"thumbnail_uri\":\"ipfs://QmPHGj4qgiapAGK5BuWin1JNWETQskdprb46JYmifkG7Gf\",\"is_transferable\":true,\"balance\":\"1\"},{\"contract\":\"KT1FCMQk44tEP9fm9n5JJEhkSk1TW3XQdaWH\",\"network\":\"edo2net\",\"token_id\":0,\"balance\":\"171123304\"},{\"contract\":\"KT1CUg39jQF8mV6nTMqZxjUUZFuz1KXowv3K\",\"network\":\"edo2net\",\"token_id\":0,\"balance\":\"10798895\"}],\"total\":5}"
        val bcdAccounts = BCDAccountTokenBalanceResponseParser().parse(jsonTokenInput)
        val expectedBalance = BCDAccount(
            ADDRESS,
            null,
            Tez(10.0),
            Date(),
            bcdAccounts.balances.map {
                it.map()
            }
        )
        every { emitter.tezosCallback(any<SingleEmitter<*>>(), any()) } answers {
            firstArg<SingleEmitter<BCDAccount>>().onSuccess(expectedBalance)
            mockk()
        }
        every { userPreferences.getUserAddress() } returns UserTezosAddress.empty
        every { xtzCurrencyUseCase.getXTZExchange() } returns Single.just(XTZTokenInfo.empty)
        every { exchangeUseCase.getTokenExchange(any(), any()) } returns Single.just(
            FATokenInfo(
                Tokens.Testnet.USDTZ,
                1.0,
                DexterBalance.empty
            )
        )

        init().getBalances()
            .test()
            .assertNoErrors()
            .assertValue {
                Assert.assertTrue(it[0].getOrNull()!!.token == Tokens.Testnet.TZBTC)
                Assert.assertTrue(it[1].getOrNull()!!.token == Tokens.Testnet.USDTZ)
                Assert.assertTrue(it[2].getOrNull()!!.token == XTZ)
                true
            }

        verify {
            useCase.getBalances()
            emitter.analyticsDelegate
            emitter.tezosCallback(any<SingleEmitter<*>>(), any())
            client.getAccountBalance(any(), any(), any())
            xtzCurrencyUseCase.getXTZExchange()
            userPreferences.getUserAddress()
            userPreferences.storeTezBalance(any())
            tokenLocalSource.store(any<TokenInfoBundle>())
            userPreferences.storeTokenBalance(any(), any())
            userPreferences.storeTokenBalance(any(), any())
            exchangeUseCase.getTokenExchange(any(), any())
            exchangeUseCase.getTokenExchange(any(), any())
        }
    }
}
