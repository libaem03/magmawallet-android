/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.test

import java.io.BufferedReader
import java.io.IOException
import java.io.InputStream
import java.io.InputStreamReader

class JSONTestLoader {
    companion object {
        private const val STRING_ENCODING = "UTF-8"

        fun getStringFromFile(path: String): String {
            try {
                val resourceAsStream: InputStream? = JSONTestLoader::class.java.classLoader!!.getResourceAsStream(path)
                return resourceAsStream?.let {
                    getStringFromStream(resourceAsStream)
                } ?: ""
            } catch (exception: IOException) {
                throw RuntimeException(exception)
            }
        }

        @Throws(IOException::class)
        fun getStringFromStream(input: InputStream): String {
            val inputStreamReader = InputStreamReader(
                input,
                STRING_ENCODING
            )
            val bufferedReader = BufferedReader(inputStreamReader)
            val stringBuilder = StringBuilder()
            var line: String? = bufferedReader.readLine()

            do {
                stringBuilder.append(line).append("\n")
                line = bufferedReader.readLine()
            } while (line != null)

            bufferedReader.close()
            inputStreamReader.close()
            return stringBuilder.toString()
        }
    }
}
