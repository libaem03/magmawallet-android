package io.camlcase.smartwallet.data.usecase.conseil

import io.camlcase.kotlintezos.TzKtClient
import io.camlcase.kotlintezos.data.parser.tzkt.TzKtOperationListParser
import io.camlcase.kotlintezos.data.tzkt.GetOperationsByAddressRPC
import io.camlcase.kotlintezos.data.tzkt.GetOperationsParameter
import io.camlcase.kotlintezos.data.tzkt.GetTransactionsByParameter
import io.camlcase.kotlintezos.model.tzkt.TzKtOperation
import io.camlcase.smartwallet.data.core.RxEmitter
import io.camlcase.smartwallet.data.core.Tokens
import io.camlcase.smartwallet.data.core.extension.tezos.getTzktDate
import io.camlcase.smartwallet.data.model.operation.*
import io.camlcase.smartwallet.data.source.operation.OperationsLocalSource
import io.camlcase.smartwallet.data.test.JSONTestLoader
import io.camlcase.smartwallet.data.test.RxTest
import io.camlcase.smartwallet.data.test.mock.ADDRESS
import io.camlcase.smartwallet.data.test.mock.mockUserPreferences
import io.camlcase.smartwallet.data.usecase.activity.GetOperationsUseCase
import io.camlcase.smartwallet.data.usecase.activity.MapTzKtOperationUseCase
import io.camlcase.smartwallet.data.usecase.activity.MapTzKtTransactionUseCase
import io.mockk.*
import io.reactivex.rxjava3.core.SingleEmitter
import org.junit.After
import org.junit.Assert
import org.junit.Test
import java.util.*
import kotlin.collections.ArrayList

/**
 * @see GetOperationsUseCase
 */
class GetOperationsUseCaseTest : RxTest() {
    private val emitter = mockk<RxEmitter>(relaxed = true)
    private val userPreferences = mockUserPreferences()
    private val tzKtClient = mockk<TzKtClient>(relaxed = true) {
        every { operations(any(), any(), any(), any()) } just Runs
    }
    private val mapTxUseCase = spyk(MapTzKtTransactionUseCase(userPreferences, Tokens.testnetFATokens))
    private val mapUseCase = spyk(MapTzKtOperationUseCase(mapTxUseCase))
    private val localSource = mockk<OperationsLocalSource>(relaxed = true)

    private fun initUseCase(): GetOperationsUseCase {
        return GetOperationsUseCase(
            emitter, userPreferences, tzKtClient, localSource, mapUseCase
        )
    }

    @Test
    fun `Test get operations +Timestamp0`() {
        val operations: List<TzKtOperation>? =
            TzKtOperationListParser().parse(JSONTestLoader.getStringFromFile("get_tzkt_operations.json"))

        every { localSource.timestamp } returns 0L
        every { localSource.operations } returns ArrayList(mapUseCase.map(operations!!))
        every { emitter.tezosCallback(any<SingleEmitter<*>>(), any()) } answers {
            firstArg<SingleEmitter<List<TzKtOperation>>>().onSuccess(operations)
            mockk()
        }

        val useCase = initUseCase()
        useCase.operations()
            .test()
            .assertNoErrors()
            .assertValue {
                Assert.assertNotNull(it)
                Assert.assertTrue(it.size == 39)
                Assert.assertTrue(it.count { it is SwapOperation } == 4)
                Assert.assertTrue(it.count { it is RevealOperation } == 0)
                Assert.assertTrue(it.count { it is DelegateOperation } == 10)
                Assert.assertTrue(it.count { it is SendOperation } == 23)
                Assert.assertTrue(it.count { it is ReceiveOperation } == 0)
                true
            }

        verify {
            userPreferences.getUserAddress()
            localSource.timestamp
            localSource.timestamp
            tzKtClient.operations(
                ADDRESS,
                mapOf(GetOperationsParameter.TYPE to GetOperationsByAddressRPC.QUERY_PARAMS_TYPE_VALUE),
                emptyMap(),
                any()
            )
            localSource.store(any())
            localSource.operations
        }
    }

    @Test
    fun `Test get operations +Timestamp`() {
        val operations: List<TzKtOperation>? =
            TzKtOperationListParser().parse(
                "[{\"type\":\"transaction\",\"id\":198682,\"level\":20821,\"timestamp\":\"2021-02-19T11:22:31Z\",\"block\":\"BM56gq7eSFQSd3tA7VLejYb1CvhSZk4guWYg2WpCRmSAE1UZBj7\",\"hash\":\"onhrpGvd6HiYWVe4fBtpBdRsazsKN2mKWadfQZTKtdqFZC7pjzT\",\"counter\":14796,\"sender\":{\"address\":\"tz1XJMJzVWcpb7qKYvneJRUJb7DNckxrDiPY\"},\"gasLimit\":1827,\"gasUsed\":1427,\"storageLimit\":257,\"storageUsed\":0,\"bakerFee\":495,\"storageFee\":0,\"allocationFee\":0,\"target\":{\"address\":\"tz1YCYVCQqFCtjQAQH5sTkkCkPkdMBQTfEn5\"},\"amount\":375,\"status\":\"applied\",\"hasInternals\":false},{\"type\":\"transaction\",\"id\":198497,\"level\":20805,\"timestamp\":\"2021-02-19T11:14:11Z\",\"block\":\"BKn92bjcu2QE52k4rtAHaUXGdXTyfjotQCgpBbciJs5YSi5G2a7\",\"hash\":\"op7CXbVCEsx7Ga4hUYW4Jy5L6mVaKNzbiDJvqoAA2UkzrnfpQhp\",\"counter\":14795,\"sender\":{\"address\":\"tz1XJMJzVWcpb7qKYvneJRUJb7DNckxrDiPY\"},\"gasLimit\":1827,\"gasUsed\":1427,\"storageLimit\":257,\"storageUsed\":0,\"bakerFee\":494,\"storageFee\":0,\"allocationFee\":0,\"target\":{\"address\":\"tz1YCYVCQqFCtjQAQH5sTkkCkPkdMBQTfEn5\"},\"amount\":52,\"status\":\"applied\",\"hasInternals\":false},{\"type\":\"transaction\",\"id\":131800,\"level\":15174,\"timestamp\":\"2021-02-17T10:55:01Z\",\"block\":\"BMGz4bZUpAbiKpguUiBMrxxDdWKuNjX75Pb98qGjphTUdU9WTNs\",\"hash\":\"ooY14svb8woC2CzwRvWpqKCuduXE9EGT3CJRSm2fH92gBRzw8AY\",\"counter\":14715,\"sender\":{\"address\":\"tz1YCYVCQqFCtjQAQH5sTkkCkPkdMBQTfEn5\"},\"gasLimit\":1827,\"gasUsed\":1427,\"storageLimit\":257,\"storageUsed\":0,\"bakerFee\":496,\"storageFee\":0,\"allocationFee\":64250,\"target\":{\"address\":\"tz1VUATLkAa7B8r3FAx5Hpxa2EBjM1SXgRXP\"},\"amount\":500000,\"status\":\"applied\",\"hasInternals\":false},{\"type\":\"transaction\",\"id\":131779,\"level\":15172,\"timestamp\":\"2021-02-17T10:54:01Z\",\"block\":\"BMRmnxPrKH2H6UUmFvHUC3LCDEHqye4sM5hVd8rF6Wt2v6Xksxg\",\"hash\":\"ooQNVyYvyhzUs49Liw8NZyiF5g1TxVrhEjfPYneEBd377WR9N8F\",\"counter\":14714,\"sender\":{\"address\":\"tz1YCYVCQqFCtjQAQH5sTkkCkPkdMBQTfEn5\"},\"gasLimit\":1827,\"gasUsed\":1427,\"storageLimit\":257,\"storageUsed\":0,\"bakerFee\":497,\"storageFee\":0,\"allocationFee\":64250,\"target\":{\"address\":\"tz1XFFEs45qUZR7W4y5C4dTFnoh11iUaqEvB\"},\"amount\":100000000,\"status\":\"applied\",\"hasInternals\":false},{\"type\":\"transaction\",\"id\":131757,\"level\":15170,\"timestamp\":\"2021-02-17T10:53:01Z\",\"block\":\"BMLdzrJTBqZEcBja2i3g8tL3TCWTbVFcJyVmMbM38SHvREHhvod\",\"hash\":\"ooLqxy1oxAk7ZjRuNuHfbbpu5hBX5xqWWVryp4QFsQp3NPwUQ5a\",\"counter\":14713,\"sender\":{\"address\":\"tz1YCYVCQqFCtjQAQH5sTkkCkPkdMBQTfEn5\"},\"gasLimit\":26326,\"gasUsed\":25926,\"storageLimit\":333,\"storageUsed\":76,\"bakerFee\":3048,\"storageFee\":19000,\"allocationFee\":0,\"target\":{\"address\":\"KT1CUg39jQF8mV6nTMqZxjUUZFuz1KXowv3K\"},\"amount\":0,\"parameter\":{\"entrypoint\":\"transfer\",\"value\":{\"to\":\"tz1XFFEs45qUZR7W4y5C4dTFnoh11iUaqEvB\",\"from\":\"tz1YCYVCQqFCtjQAQH5sTkkCkPkdMBQTfEn5\",\"value\":\"50000\"}},\"status\":\"applied\",\"hasInternals\":false,\"parameters\":\"{\\\"entrypoint\\\":\\\"transfer\\\",\\\"value\\\":{\\\"prim\\\":\\\"Pair\\\",\\\"args\\\":[{\\\"string\\\":\\\"tz1YCYVCQqFCtjQAQH5sTkkCkPkdMBQTfEn5\\\"},{\\\"prim\\\":\\\"Pair\\\",\\\"args\\\":[{\\\"string\\\":\\\"tz1XFFEs45qUZR7W4y5C4dTFnoh11iUaqEvB\\\"},{\\\"int\\\":\\\"50000\\\"}]}]}}\"},{\"type\":\"transaction\",\"id\":131724,\"level\":15167,\"timestamp\":\"2021-02-17T10:51:31Z\",\"block\":\"BLZFddxpPzYth16WPv2AzWkGm3bx23JVvR6WdGR4CfFa85mXL7X\",\"hash\":\"opFhYeLjvJR2KSm9J4MJ2HPqMdJi9e5jRWCz3i35UnoBGb8TPkC\",\"counter\":14712,\"sender\":{\"address\":\"tz1YCYVCQqFCtjQAQH5sTkkCkPkdMBQTfEn5\"},\"gasLimit\":1827,\"gasUsed\":1427,\"storageLimit\":257,\"storageUsed\":0,\"bakerFee\":497,\"storageFee\":0,\"allocationFee\":64250,\"target\":{\"address\":\"tz1XJMJzVWcpb7qKYvneJRUJb7DNckxrDiPY\"},\"amount\":100000000,\"status\":\"applied\",\"hasInternals\":false},{\"type\":\"transaction\",\"id\":131691,\"level\":15164,\"timestamp\":\"2021-02-17T10:50:01Z\",\"block\":\"BLdR3LtMbXGiNoQTdzF6Pv1jWbL6zJMmP6mCYuW2hUtZXxkxeyS\",\"hash\":\"opWPPkFe4gB4RRJrd7EyhLhSuU95DAsnh8wsFiYjKh98tddd2tm\",\"counter\":14711,\"sender\":{\"address\":\"tz1YCYVCQqFCtjQAQH5sTkkCkPkdMBQTfEn5\"},\"gasLimit\":1827,\"gasUsed\":1427,\"storageLimit\":257,\"storageUsed\":0,\"bakerFee\":495,\"storageFee\":0,\"allocationFee\":64250,\"target\":{\"address\":\"tz1crghTXjGHpecfMsoMcvV8FA3JqUkBnDEn\"},\"amount\":500,\"status\":\"applied\",\"hasInternals\":false},{\"type\":\"transaction\",\"id\":131647,\"level\":15160,\"timestamp\":\"2021-02-17T10:48:01Z\",\"block\":\"BLxehtc3AmF4q8HPcd5mjP2t5adkykk73eiJnNvjscfdkxmPbGY\",\"hash\":\"onppBdZZPpgMaqn5TptGqVxHxXxgE6hbhCTDgS21t8MDmp5fyfT\",\"counter\":14710,\"initiator\":{\"address\":\"tz1YCYVCQqFCtjQAQH5sTkkCkPkdMBQTfEn5\"},\"sender\":{\"address\":\"KT1GDLrXLAoYg3wxMvkxzyKH8zpPvxF4C68G\"},\"nonce\":1,\"gasLimit\":0,\"gasUsed\":25972,\"storageLimit\":0,\"storageUsed\":76,\"bakerFee\":0,\"storageFee\":19000,\"allocationFee\":0,\"target\":{\"address\":\"KT1CUg39jQF8mV6nTMqZxjUUZFuz1KXowv3K\"},\"amount\":0,\"parameter\":{\"entrypoint\":\"transfer\",\"value\":{\"to\":\"tz1YCYVCQqFCtjQAQH5sTkkCkPkdMBQTfEn5\",\"from\":\"KT1GDLrXLAoYg3wxMvkxzyKH8zpPvxF4C68G\",\"value\":\"8250245\"}},\"status\":\"applied\",\"hasInternals\":false,\"parameters\":\"{\\\"entrypoint\\\":\\\"transfer\\\",\\\"value\\\":{\\\"prim\\\":\\\"Pair\\\",\\\"args\\\":[{\\\"bytes\\\":\\\"0153b3a4b05d9c53161c08b8ee1be72885c3120f6800\\\"},{\\\"prim\\\":\\\"Pair\\\",\\\"args\\\":[{\\\"bytes\\\":\\\"000089c7b0fbb4d55dbcbd5b80c9e31edc4924bb8352\\\"},{\\\"int\\\":\\\"8250245\\\"}]}]}}\"},{\"type\":\"transaction\",\"id\":131646,\"level\":15160,\"timestamp\":\"2021-02-17T10:48:01Z\",\"block\":\"BLxehtc3AmF4q8HPcd5mjP2t5adkykk73eiJnNvjscfdkxmPbGY\",\"hash\":\"onppBdZZPpgMaqn5TptGqVxHxXxgE6hbhCTDgS21t8MDmp5fyfT\",\"counter\":14710,\"sender\":{\"address\":\"tz1YCYVCQqFCtjQAQH5sTkkCkPkdMBQTfEn5\"},\"gasLimit\":103234,\"gasUsed\":76862,\"storageLimit\":333,\"storageUsed\":0,\"bakerFee\":10730,\"storageFee\":0,\"allocationFee\":0,\"target\":{\"address\":\"KT1GDLrXLAoYg3wxMvkxzyKH8zpPvxF4C68G\"},\"amount\":1000000000,\"parameter\":{\"entrypoint\":\"xtzToToken\",\"value\":{\"to\":\"tz1YCYVCQqFCtjQAQH5sTkkCkPkdMBQTfEn5\",\"deadline\":\"2021-02-17T11:07:32Z\",\"minTokensBought\":\"7743019\"}},\"status\":\"applied\",\"hasInternals\":true,\"parameters\":\"{\\\"entrypoint\\\":\\\"xtzToToken\\\",\\\"value\\\":{\\\"prim\\\":\\\"Pair\\\",\\\"args\\\":[{\\\"string\\\":\\\"tz1YCYVCQqFCtjQAQH5sTkkCkPkdMBQTfEn5\\\"},{\\\"prim\\\":\\\"Pair\\\",\\\"args\\\":[{\\\"int\\\":\\\"7743019\\\"},{\\\"string\\\":\\\"2021-02-17T11:07:32Z\\\"}]}]}}\"},{\"type\":\"transaction\",\"id\":131626,\"level\":15158,\"timestamp\":\"2021-02-17T10:47:01Z\",\"block\":\"BLASWjSeDTiUgXBPuvp1aH7pHupLdyKvAEj5jy5K2RDY5ijdgkm\",\"hash\":\"oniBDhWJc33zNCRirPTqDQksgCtZEAy4qJCcLc6h1er61npGGn7\",\"counter\":14709,\"sender\":{\"address\":\"tz1YCYVCQqFCtjQAQH5sTkkCkPkdMBQTfEn5\"},\"gasLimit\":1827,\"gasUsed\":1427,\"storageLimit\":257,\"storageUsed\":0,\"bakerFee\":498,\"storageFee\":0,\"allocationFee\":64250,\"target\":{\"address\":\"tz1Ydpp4ibTrGSPn2i6cqQZ6arWieUbcfWVn\"},\"amount\":985000000,\"status\":\"applied\",\"hasInternals\":false},{\"type\":\"delegation\",\"id\":131110,\"level\":15105,\"timestamp\":\"2021-02-17T10:19:51Z\",\"block\":\"BL8NQwvaMM5Nzk9Mj8eTx4LV65KHCTJUSTVMWdn9WCK8uHnQTCe\",\"hash\":\"oonpSbCTUodM7oaWDcPrcgXbGQi538fx4VGTjGmMXPfSUu8ZSvU\",\"counter\":14708,\"sender\":{\"address\":\"tz1YCYVCQqFCtjQAQH5sTkkCkPkdMBQTfEn5\"},\"gasLimit\":10000,\"gasUsed\":1000,\"bakerFee\":1257,\"amount\":24151997475,\"newDelegate\":{\"address\":\"tz1YCYVCQqFCtjQAQH5sTkkCkPkdMBQTfEn5\"},\"status\":\"applied\"},{\"type\":\"reveal\",\"id\":131109,\"level\":15105,\"timestamp\":\"2021-02-17T10:19:51Z\",\"block\":\"BL8NQwvaMM5Nzk9Mj8eTx4LV65KHCTJUSTVMWdn9WCK8uHnQTCe\",\"hash\":\"oonpSbCTUodM7oaWDcPrcgXbGQi538fx4VGTjGmMXPfSUu8ZSvU\",\"sender\":{\"address\":\"tz1YCYVCQqFCtjQAQH5sTkkCkPkdMBQTfEn5\"},\"counter\":14707,\"gasLimit\":10000,\"gasUsed\":1000,\"bakerFee\":1268,\"status\":\"applied\"},{\"type\":\"transaction\",\"id\":131090,\"level\":15103,\"timestamp\":\"2021-02-17T10:18:51Z\",\"block\":\"BLMcftFitGdwPmBxoPa9esmDYHGSGJNeFWt8wM9SWt5ZCsfZXMa\",\"hash\":\"ooMhcP7zkrR3gXW2Kre2ZRnPv64wiYZEepVDyofAodjmN4ZHBzL\",\"counter\":14717,\"sender\":{\"address\":\"tz1XKiNSh28xvVcQGyup7dBd8aetEAn5WD4H\"},\"gasLimit\":3227,\"gasUsed\":1427,\"storageLimit\":514,\"storageUsed\":0,\"bakerFee\":1077,\"storageFee\":0,\"allocationFee\":0,\"target\":{\"address\":\"tz1YCYVCQqFCtjQAQH5sTkkCkPkdMBQTfEn5\"},\"amount\":19608500000,\"status\":\"applied\",\"hasInternals\":false},{\"type\":\"transaction\",\"id\":131008,\"level\":15095,\"timestamp\":\"2021-02-17T10:14:31Z\",\"block\":\"BM8R2DXqzgTshy3rJuUSKJTWU3qArFpS1bdVm3SEW8P5YabDq5e\",\"hash\":\"oopWXNijeerz8EPi6m6Ff9uLcrqwMPmbz1GAECsy11X4vSANPBU\",\"counter\":14700,\"sender\":{\"address\":\"tz1XC7ENuCiwP2amgLdSwwURUwaFeJgHzHjH\"},\"gasLimit\":3227,\"gasUsed\":1427,\"storageLimit\":514,\"storageUsed\":0,\"bakerFee\":1077,\"storageFee\":0,\"allocationFee\":64250,\"target\":{\"address\":\"tz1YCYVCQqFCtjQAQH5sTkkCkPkdMBQTfEn5\"},\"amount\":4543500000,\"status\":\"applied\",\"hasInternals\":false}]"
            )
        val operationsNew: List<TzKtOperation>? =
            TzKtOperationListParser().parse(
                "[{\"type\":\"transaction\",\"id\":598782,\"level\":52868,\"timestamp\":\"2021-03-02T17:25:29Z\",\"block\":\"BLzRQmVsfLx5HWgnPNVCrC89awzU95m38tsbDKqQxFJc4RSrNou\",\"hash\":\"oooWGkK1Eqy1Dj89biBtK5sz7a9auYTbMwuu8SaLnpE2sw2Z5Bb\",\"counter\":14724,\"initiator\":{\"address\":\"tz1YCYVCQqFCtjQAQH5sTkkCkPkdMBQTfEn5\"},\"sender\":{\"address\":\"KT1BYYLfMjufYwqFtTSYJND7bzKNyK7mjrjM\"},\"nonce\":0,\"gasLimit\":0,\"gasUsed\":26244,\"storageLimit\":0,\"storageUsed\":0,\"bakerFee\":0,\"storageFee\":0,\"allocationFee\":0,\"target\":{\"address\":\"KT1CUg39jQF8mV6nTMqZxjUUZFuz1KXowv3K\"},\"amount\":0,\"parameter\":{\"entrypoint\":\"transfer\",\"value\":{\"to\":\"tz1YCYVCQqFCtjQAQH5sTkkCkPkdMBQTfEn5\",\"from\":\"KT1BYYLfMjufYwqFtTSYJND7bzKNyK7mjrjM\",\"value\":\"579313\"}},\"status\":\"applied\",\"hasInternals\":false,\"parameters\":\"{\\\"entrypoint\\\":\\\"transfer\\\",\\\"value\\\":{\\\"prim\\\":\\\"Pair\\\",\\\"args\\\":[{\\\"bytes\\\":\\\"01207c8146d8e6d6e39ae31988f997943ef6c5413300\\\"},{\\\"prim\\\":\\\"Pair\\\",\\\"args\\\":[{\\\"bytes\\\":\\\"000089c7b0fbb4d55dbcbd5b80c9e31edc4924bb8352\\\"},{\\\"int\\\":\\\"579313\\\"}]}]}}\"},{\"type\":\"transaction\",\"id\":592217,\"level\":52341,\"timestamp\":\"2021-03-02T12:59:59Z\",\"block\":\"BM83EYUG8tBEZGWvKQ9Ew4GUxorrbe5dFUiCEdnWLDSRbR9ZNsD\",\"hash\":\"ooSGjEN83hrnkk68Tip3mUSofQAnrx4dhAK8G7MtBvAEAs5Vu3r\",\"counter\":57815,\"sender\":{\"address\":\"tz1ix7Q9WMyHyo5qAsskfs9zRT5ELEuPCgJP\"},\"gasLimit\":26598,\"gasUsed\":26198,\"storageLimit\":257,\"storageUsed\":0,\"bakerFee\":3076,\"storageFee\":0,\"allocationFee\":0,\"target\":{\"address\":\"KT1FCMQk44tEP9fm9n5JJEhkSk1TW3XQdaWH\"},\"amount\":0,\"parameter\":{\"entrypoint\":\"transfer\",\"value\":{\"to\":\"tz1YCYVCQqFCtjQAQH5sTkkCkPkdMBQTfEn5\",\"from\":\"tz1ix7Q9WMyHyo5qAsskfs9zRT5ELEuPCgJP\",\"value\":\"200000\"}},\"status\":\"applied\",\"hasInternals\":false,\"parameters\":\"{\\\"entrypoint\\\":\\\"transfer\\\",\\\"value\\\":{\\\"prim\\\":\\\"Pair\\\",\\\"args\\\":[{\\\"string\\\":\\\"tz1ix7Q9WMyHyo5qAsskfs9zRT5ELEuPCgJP\\\"},{\\\"prim\\\":\\\"Pair\\\",\\\"args\\\":[{\\\"string\\\":\\\"tz1YCYVCQqFCtjQAQH5sTkkCkPkdMBQTfEn5\\\"},{\\\"int\\\":\\\"200000\\\"}]}]}}\"}]"
            )

        val list = ArrayList<TzKtOperation>(operations)
        list.addAll(operationsNew!!)
        val mappedOperations = mapUseCase.map(list)

        val time = Date().time
        val tzktDate = time.getTzktDate()!!
        every { localSource.timestamp } returns time
        every { localSource.operations } returns ArrayList(mappedOperations)
        every { emitter.tezosCallback(any<SingleEmitter<*>>(), any()) } answers {
            firstArg<SingleEmitter<List<TzKtOperation>>>().onSuccess(operationsNew)
            mockk()
        }

        val useCase = initUseCase()
        useCase.operations()
            .test()
            .assertNoErrors()
            .assertValue {
                Assert.assertNotNull(it)
                Assert.assertTrue(it.size == 15)
                Assert.assertTrue(it.count { it is SwapOperation } == 1)
                Assert.assertTrue(it.count { it is RevealOperation } == 1)
                Assert.assertTrue(it.count { it is DelegateOperation } == 1)
                Assert.assertTrue(it.count { it is SendOperation } == 11)
                Assert.assertTrue(it.count { it is ReceiveOperation } == 0)
                true
            }

        verify {
            userPreferences.getUserAddress()
            localSource.timestamp
            localSource.timestamp
            tzKtClient.operations(
                ADDRESS,
                mapOf(
                    GetOperationsParameter.TYPE to GetOperationsByAddressRPC.QUERY_PARAMS_TYPE_VALUE,
                    GetOperationsParameter.TIMESTAMP_GT to tzktDate
                ),
                mapOf(
                    GetTransactionsByParameter.TIMESTAMP_GT to tzktDate
                ),
                any()
            )
            localSource.store(any())
            localSource.operations
        }
    }

    @After
    fun after() {
        confirmVerified(tzKtClient)
        confirmVerified(userPreferences)
        confirmVerified(localSource)
    }

    companion object {}
}
