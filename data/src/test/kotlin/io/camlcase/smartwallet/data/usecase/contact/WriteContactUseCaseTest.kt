package io.camlcase.smartwallet.data.usecase.contact

import io.camlcase.kotlintezos.model.TezosNetwork
import io.camlcase.smartwallet.data.model.contact.ContactTezosAddress
import io.camlcase.smartwallet.data.model.contact.MagmaContact
import io.camlcase.smartwallet.data.source.contact.PhoneBookLocalSource
import io.camlcase.smartwallet.data.test.RxTest
import io.mockk.*
import org.junit.After
import org.junit.Assert
import org.junit.Test

class WriteContactUseCaseTest : RxTest() {
    private val phoneBookLocalSource = mockk<PhoneBookLocalSource>(relaxed = true)

    fun init(network: TezosNetwork = TezosNetwork.CARTHAGENET): WriteContactUseCase {
        return spyk(WriteContactUseCase(network, phoneBookLocalSource))
    }

    @After
    fun after() {
        confirmVerified(phoneBookLocalSource)
    }

    @Test
    fun `addContact OK`() {
        val input = TEST_CONTACT
        every { phoneBookLocalSource.add(any(), any()) } returns input.copy(1)

        val useCase = init()
        val account = "some@account.com"
        useCase.addContact(account, input.name, input.tezosAddresses[0].address)

        verify {
            phoneBookLocalSource.add(account, input)
        }
    }

    @Test
    fun `pickContact OK`() {
        every { phoneBookLocalSource.getContactFromUri(any()) } returns TEST_CONTACT

        val useCase = init()
        val result = useCase.pickContact(mockk())

        Assert.assertTrue(result == TEST_CONTACT)

        verify {
            phoneBookLocalSource.getContactFromUri(any())
        }
    }

    @Test
    fun `editContactAddress Existent contact with no previous tz1 address`() {
        every { phoneBookLocalSource.insertTezosAddress(any(), any()) } returns true

        val input = MagmaContact(
            0,
            "Test Name",
            emptyList()
        )
        val useCase = init()
        val result = useCase.editContactAddress(input, ADDRESS2)

        Assert.assertTrue(result)

        verify {
            phoneBookLocalSource.insertTezosAddress(input.id, ContactTezosAddress(ADDRESS2, TezosNetwork.CARTHAGENET))
        }
    }

    @Test
    fun `editContactAddress Existent contact with tz1 address`() {
        every { phoneBookLocalSource.updateTezosAddress(any(), any()) } returns true

        val useCase = init()
        val result = useCase.editContactAddress(TEST_CONTACT, ADDRESS2)

        Assert.assertTrue(result)

        verify {
            phoneBookLocalSource.updateTezosAddress(
                TEST_CONTACT,
                ContactTezosAddress(ADDRESS2, TezosNetwork.CARTHAGENET)
            )
        }
    }

    @Test
    fun `editContactName OK`() {
        every { phoneBookLocalSource.updateName(any(), any()) } returns true

        val useCase = init()
        val name = "Updated name"
        val result = useCase.editContactName(TEST_CONTACT, name)

        Assert.assertTrue(result)

        verify {
            phoneBookLocalSource.updateName(
                TEST_CONTACT,
                name
            )
        }
    }

    @Test
    fun `deleteContact Existent contact with extra data`() {
        every { phoneBookLocalSource.hasExtraData(any()) } returns true
        every { phoneBookLocalSource.deleteData(any()) } returns true

        val useCase = init()
        val result = useCase.deleteContact(TEST_CONTACT)

        Assert.assertTrue(result)

        verify {
            phoneBookLocalSource.hasExtraData(TEST_CONTACT)
            phoneBookLocalSource.deleteData(TEST_CONTACT)
        }
    }

    // Will delete the full contact
    @Test
    fun `deleteContact Magma contact`() {
        every { phoneBookLocalSource.hasExtraData(any()) } returns false
        every { phoneBookLocalSource.delete(any()) } returns true

        val useCase = init()
        val result = useCase.deleteContact(TEST_CONTACT)

        Assert.assertTrue(result)

        verify {
            phoneBookLocalSource.hasExtraData(TEST_CONTACT)
            phoneBookLocalSource.delete(TEST_CONTACT)
        }
    }

    /**
     * EDIT
     */

    @Test
    fun `editContact Same name`() {
        every { phoneBookLocalSource.updateTezosAddress(any(), any()) } returns true

        val updatedName = TEST_CONTACT.name
        val updatedAddress = ADDRESS2

        val useCase = init()
        val result = useCase.editContact(TEST_CONTACT, Pair(updatedName, updatedAddress))

        Assert.assertTrue(result.first)
        Assert.assertTrue(result.second.name == TEST_CONTACT.name)
        Assert.assertTrue(result.second.tezosAddresses[0].address == updatedAddress)

        verify {
            phoneBookLocalSource.updateTezosAddress(
                TEST_CONTACT,
                ContactTezosAddress(ADDRESS2, TezosNetwork.CARTHAGENET)
            )
        }
    }

    @Test
    fun `editContact Same data`() {
        val updatedName = TEST_CONTACT.name
        val updatedAddress = TEST_CONTACT.tezosAddresses[0].address

        val useCase = init()
        val result = useCase.editContact(TEST_CONTACT, Pair(updatedName, updatedAddress))

        Assert.assertFalse(result.first)
        Assert.assertTrue(result.second == TEST_CONTACT)
    }

    @Test
    fun `editContact Same address`() {
        every { phoneBookLocalSource.updateName(any(), any()) } returns true

        val updatedName = "Updated name"
        val updatedAddress = ADDRESS1

        val useCase = init()
        val result = useCase.editContact(TEST_CONTACT, Pair(updatedName, updatedAddress))

        Assert.assertTrue(result.first)
        Assert.assertTrue(result.second.name == updatedName)
        Assert.assertTrue(result.second.tezosAddresses[0].address == updatedAddress)

        verify {
            phoneBookLocalSource.updateName(
                TEST_CONTACT,
                updatedName
            )
        }
    }

    @Test
    fun `editContact Both changes`() {
        every { phoneBookLocalSource.updateTezosAddress(any(), any()) } returns true
        every { phoneBookLocalSource.updateName(any(), any()) } returns true

        val updatedName = "Updated name"
        val updatedAddress = ADDRESS2

        val useCase = init()
        val result = useCase.editContact(TEST_CONTACT, Pair(updatedName, updatedAddress))

        Assert.assertTrue(result.first)
        Assert.assertTrue(result.second.name == updatedName)
        Assert.assertTrue(result.second.tezosAddresses[0].address == updatedAddress)

        verify {
            phoneBookLocalSource.updateName(
                TEST_CONTACT,
                updatedName
            )
            phoneBookLocalSource.updateTezosAddress(
                TEST_CONTACT,
                ContactTezosAddress(ADDRESS2, TezosNetwork.CARTHAGENET)
            )
        }
    }

    companion object {
        private const val ADDRESS1 = "tz1XSXBeWqdupm73qWAJkxJkxRzH16y77i1B"
        private const val ADDRESS2 = "tz1eZAGXmXxwkXUBUxuSk5XkJ5UZ5Q25Baja"
        private val TEST_CONTACT_TEZOS_ADDRESS = ContactTezosAddress(
            ADDRESS1,
            TezosNetwork.CARTHAGENET
        )
        val TEST_CONTACT = MagmaContact(
            0,
            "Test Name",
            listOf(
                TEST_CONTACT_TEZOS_ADDRESS
            )
        )
    }
}
