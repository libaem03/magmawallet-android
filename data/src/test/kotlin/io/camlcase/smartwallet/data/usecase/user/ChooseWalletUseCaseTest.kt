package io.camlcase.smartwallet.data.usecase.user

import io.camlcase.kotlintezos.wallet.HDWallet
import io.camlcase.smartwallet.data.model.AppWallet
import io.camlcase.smartwallet.data.test.mock.ADDRESS
import io.camlcase.smartwallet.data.test.mock.mnemonics
import io.camlcase.smartwallet.data.test.mock.mockWalletLocalSource
import io.mockk.*
import io.reactivex.rxjava3.core.Single
import org.junit.After
import org.junit.Assert
import org.junit.Test

class ChooseWalletUseCaseTest {
    private val walletLocalSource = mockWalletLocalSource()
    private val createWalletUseCase = mockk<CreateWalletUseCase>(relaxed = true)
    private lateinit var useCase: ChooseWalletUseCase

    private fun init(): ChooseWalletUseCase {
        useCase = spyk(ChooseWalletUseCase(walletLocalSource, createWalletUseCase))
        return useCase
    }

    @After
    fun tearDown() {
        confirmVerified(walletLocalSource)
        confirmVerified(createWalletUseCase)
        confirmVerified(useCase)
    }

    @Test
    fun `getAddressPair Two addresses OK`() {
        val wallet = mockk<AppWallet>(relaxed = true) {
            every { mnemonic } returns mnemonics
            every { address } returns ADDRESS
            every { wallet } returns mockk<HDWallet>(relaxed = true) {
                every { linearAddress } returns "tz1_2"
            }
        }
        every { walletLocalSource.getWalletOrDie() } returns Single.just(wallet)

        init().getAddressPair()
            .test()
            .assertNoErrors()
            .assertValue {
                Assert.assertTrue(it.first == ADDRESS)
                Assert.assertTrue(it.second == "tz1_2")
                true
            }

        verify {
            walletLocalSource.getWalletOrDie()
            useCase.getAddressPair()
        }
    }

    @Test
    fun `getAddressPair One address OK`() {
        val wallet = mockk<AppWallet>(relaxed = true) {
            every { mnemonic } returns mnemonics
            every { address } returns ADDRESS
            every { wallet } returns mockk<HDWallet>(relaxed = true) {
                every { linearAddress } returns null
            }
        }
        every { walletLocalSource.getWalletOrDie() } returns Single.just(wallet)

        init().getAddressPair()
            .test()
            .assertNoErrors()
            .assertValue {
                Assert.assertTrue(it.first == ADDRESS)
                Assert.assertTrue(it.second == null)
                true
            }

        verify {
            walletLocalSource.getWalletOrDie()
            useCase.getAddressPair()
        }
    }

    @Test
    fun `getAddressPair KO`() {
        every { walletLocalSource.getWalletOrDie() } returns Single.error(IllegalStateException())

        init().getAddressPair()
            .test()
            .assertError {
                Assert.assertTrue(it is IllegalStateException)
                true
            }

        verify {
            walletLocalSource.getWalletOrDie()
            useCase.getAddressPair()
        }
    }

    @Test
    fun `chooseWallet HD OK`() {
        val wallet = mockk<AppWallet>(relaxed = true) {
            every { mnemonic } returns mnemonics
            every { address } returns ADDRESS
            every { wallet } returns mockk<HDWallet>(relaxed = true) {
                every { linearAddress } returns "tz1_2"
            }
        }
        every { walletLocalSource.getWallet() } returns wallet

        val result = init().chooseWallet(ADDRESS)
        Assert.assertTrue(!result)

        verify {
            walletLocalSource.getWallet()
            useCase.chooseWallet(ADDRESS)
        }
    }

    @Test
    fun `chooseWallet SD OK`() {
        val wallet = mockk<AppWallet>(relaxed = true) {
            every { mnemonic } returns mnemonics
            every { passphrase } returns "password"
            every { address } returns ADDRESS
            every { wallet } returns mockk<HDWallet>(relaxed = true) {
                every { linearAddress } returns "tz1_2"
            }
        }
        every { walletLocalSource.getWallet() } returns wallet

        val result = init().chooseWallet("tz1_2")
        Assert.assertTrue(result)

        verify {
            walletLocalSource.getWallet()
            createWalletUseCase.recoverSDWallet(mnemonics, "password", false)
            useCase.chooseWallet("tz1_2")
        }
    }

    @Test
    fun `chooseWallet KO`() {
        every { walletLocalSource.getWallet() } throws IllegalStateException()

        try {
            val result = init().chooseWallet("tz1_2")
        } catch (e: java.lang.IllegalStateException) {
            verify {
                walletLocalSource.getWallet()
                useCase.chooseWallet("tz1_2")
            }
        }
    }
}
