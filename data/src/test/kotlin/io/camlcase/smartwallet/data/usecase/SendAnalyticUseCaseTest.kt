package io.camlcase.smartwallet.data.usecase

import io.camlcase.smartwallet.data.analytic.AnalyticsTracker
import io.camlcase.smartwallet.data.usecase.analytics.AnalyticEvent
import io.camlcase.smartwallet.data.usecase.analytics.SendAnalyticUseCase
import io.mockk.confirmVerified
import io.mockk.mockk
import io.mockk.verify
import org.junit.After
import org.junit.Test

/**
 * @see SendAnalyticUseCase
 */
class SendAnalyticUseCaseTest {
    private val delegate = mockk<AnalyticsTracker>(relaxed = true)

    @After
    fun after() {
        confirmVerified(delegate)
    }

    @Test
    fun `Test report AnalyticEvents with no args OK`() {
        val useCase = SendAnalyticUseCase(delegate)

        for (event in AnalyticEvent.values()) {
            useCase.reportEvent(event)

            verify {
                delegate.reportEvent(event.message)
            }
        }
    }

    // TODO Uncomment when we have Events with %s
//    @Test
//    fun `Test report info event with 1 args OK`() {
//        val useCase = SendAnalyticUseCase(delegate)
//        val event = AnalyticEvent.SEND_SUCCESS
//        useCase.reportEvent(event, "XTZ")
//
//        verify {
//            delegate.reportMessage(event.message)
//        }
//    }
//
//    @Test
//    fun `Test report info event with 2 args OK`() {
//        val useCase = SendAnalyticUseCase(delegate)
//        val event = AnalyticEvent.SEND_SUCCESS
//        useCase.reportEvent(event, "XTZ", "YZG")
//
//        verify {
//            delegate.reportMessage(event.message)
//        }
//    }
//
//    @Test
//    fun `Test report info event with expected but no args OK`() {
//        val useCase = SendAnalyticUseCase(delegate)
//        val event = AnalyticEvent.SEND_SUCCESS
//        useCase.reportEvent(event)
//
//        verify {
//            delegate.reportMessage(event.message)
//        }
//    }
}
