package io.camlcase.smartwallet.data.core.extension

import org.junit.Assert
import org.junit.Test

class StringExtKtTest {
    @Test
    fun `Sanitize words separated by line breaks`() {
        val input = "mnemonic\nanother\nmore\nwords"
        val result = input.sanitizeSeedPhrase()
        Assert.assertEquals("mnemonic another more words", result)
    }

    @Test
    fun `Sanitize words separated by line breaks and spaces`() {
        val input = "mnemonic\n another\n more\nwords "
        val result = input.sanitizeSeedPhrase()
        Assert.assertEquals("mnemonic another more words", result)
    }

    @Test
    fun `Sanitize words with numbers`() {
        val input = "mnemonic2 another\n more3\nwor5ds "
        val result = input.sanitizeSeedPhrase()
        Assert.assertEquals("mnemonic2 another more3 wor5ds", result)
    }

    @Test
    fun `Sanitize words separated with commas`() {
        val input = "mnemonic, another\n more3\nwor5ds, "
        val result = input.sanitizeSeedPhrase()
        Assert.assertEquals("mnemonic another more3 wor5ds", result)
    }

    @Test
    fun `Sanitize words with symbols`() {
        val input = "mn+emonic, anot.her\n more3\nwor5ds, "
        val result = input.sanitizeSeedPhrase()
        Assert.assertEquals("mn+emonic another more3 wor5ds", result)
    }

    @Test
    fun `Check isOnlyLettersAndSpaces OK`() {
        val input = "aa bb ccc"
        val result = input.isOnlyLettersAndSpaces()
        Assert.assertTrue(result)
    }

    @Test
    fun `Check isOnlyLettersAndSpaces with $`() {
        val input = "aaaa bbb $"
        val result = input.isOnlyLettersAndSpaces()
        Assert.assertFalse(result)
    }

    @Test
    fun `Check isOnlyLettersAndSpaces with line break regex`() {
        val input = "aa\n"
        val result = input.isOnlyLettersAndSpaces()
        Assert.assertFalse(result)
    }

    @Test
    fun `Check isOnlyLettersAndSpaces with ""`() {
        val input = "aa\"\""
        val result = input.isOnlyLettersAndSpaces()
        Assert.assertFalse(result)
    }

    @Test
    fun `Check isOnlyLettersAndSpaces with numbers`() {
        val input = "aa999 "
        val result = input.isOnlyLettersAndSpaces()
        Assert.assertFalse(result)
    }

    @Test
    fun `Check isOnlyLettersAndSpaces with comma`() {
        val input = "aa,"
        val result = input.isOnlyLettersAndSpaces()
        Assert.assertFalse(result)
    }

    @Test
    fun `Check isOnlyLettersAndSpaces with emoji`() {
        val input = "aa😄"
        val result = input.isOnlyLettersAndSpaces()
        Assert.assertFalse(result)
    }
}
