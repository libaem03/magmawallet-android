package io.camlcase.smartwallet.data.usecase.analytics

import io.camlcase.smartwallet.data.analytic.AnalyticsTracker
import io.mockk.confirmVerified
import io.mockk.mockk
import io.mockk.spyk
import io.mockk.verify
import org.junit.After
import org.junit.Test

class SendAnalyticUseCaseTest {
    private val delegate = mockk<AnalyticsTracker>(relaxed = true)
    private lateinit var useCase: SendAnalyticUseCase

    private fun init(): SendAnalyticUseCase {
        useCase = spyk(SendAnalyticUseCase(delegate))
        return useCase
    }

    @After
    fun tearDown() {
        confirmVerified(delegate)
        confirmVerified(useCase)
    }

    @Test
    fun `reportState NoFormat NoArgs`() {
        val event = StateEvent.ONB_RECOVERY_ENTRY_DERIVATION_PATH
        init().reportState(event, null)

        verify {
            useCase.reportState(event, null)
            delegate.reportEvent(
                event.eventName + " : " + event.argument,
                emptyMap()
            )
        }
    }

    @Test
    fun `reportState Format NoArgs`() {
        val event = StateEvent.ONB_RECOVERY_ENTRY_FAILED
        init().reportState(event, null)

        verify {
            useCase.reportState(event, null)
            delegate.reportEvent(
                event.eventName + " : " + event.argument,
                emptyMap()
            )
        }
    }

    @Test
    fun `reportState NoFormat Args`() {
        val event = StateEvent.ONB_RECOVERY_ENTRY_DERIVATION_PATH
        val args = "Magma Android"
        init().reportState(event, args)

        verify {
            useCase.reportState(event, args)
            delegate.reportEvent(
                event.eventName + " : " + event.argument,
                emptyMap()
            )
        }
    }

    @Test
    fun `reportState Format Args`() {
        val event = StateEvent.ONB_RECOVERY_ENTRY_FAILED
        val args = "Magma Android"
        init().reportState(event, args)

        verify {
            useCase.reportState(event, args)
            delegate.reportEvent(
                event.eventName + " : " + String.format(event.argument, args),
                emptyMap()
            )
        }
    }

    @Test
    fun `reportScreen OK`() {
        val event = ScreenEvent.LOGIN_AUTH_BIOMETRIC
        init().reportScreen(event)
        verify {
            useCase.reportScreen(event)
            delegate.reportEvent(event.eventName, emptyMap())
        }
    }
}
