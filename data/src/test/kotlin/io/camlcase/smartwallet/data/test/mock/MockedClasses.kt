/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.smartwallet.data.test.mock

import io.camlcase.kotlintezos.wallet.SDWallet
import io.camlcase.smartwallet.data.model.AppWallet
import io.camlcase.smartwallet.data.model.Token
import io.camlcase.smartwallet.data.model.UserTezosAddress
import io.camlcase.smartwallet.data.source.local.UserPreferences
import io.camlcase.smartwallet.data.source.user.WalletLocalSource
import io.mockk.every
import io.mockk.mockk
import io.reactivex.rxjava3.core.Single

fun mockWalletLocalSource(): WalletLocalSource {
    val wallet = mockk<AppWallet>(relaxed = true) {
        every { mnemonic } returns mnemonics
        every { address } returns ADDRESS
        every { wallet } returns mockk<SDWallet>(relaxed = true)
    }
    return mockk<WalletLocalSource>(relaxed = true) {
        every { getWalletOrDie() } returns Single.just(wallet)
    }
}

fun mockUserPreferences(): UserPreferences {
    return mockk<UserPreferences>(relaxed = true) {
        every { getUserAddress() } returns UserTezosAddress(ADDRESS)
    }
}


val mnemonics = listOf("magma", "wallet")
const val ADDRESS = "tz1YCYVCQqFCtjQAQH5sTkkCkPkdMBQTABCD"


val TEST_TOKEN by lazy {
    Token(
        "TestToken",
        "TTz",
        0,
        contractAddress = "KT1N8A78V9fSiyGwqBpAU2ZQ6S446C7ZwRoD",
        dexterAddress = "KT1QwuYp1g9uvii5CPXivNQd4RC7VGvLPS4e"
    )
}
