package io.camlcase.smartwallet.data.source.user

import io.camlcase.smartwallet.data.core.security.CryptoUtils
import io.camlcase.smartwallet.data.model.error.AppError
import io.camlcase.smartwallet.data.source.local.SecurePreferences
import io.mockk.every
import io.mockk.mockk
import io.mockk.spyk
import io.mockk.verify
import org.junit.Assert
import org.junit.Test

class PinLocalSourceTest {

    @Test(expected = AppError::class)
    fun `Test comparePin with empty salt and no pin stored`() {
        val securePreferences = mockk<SecurePreferences>(relaxed = true) {
            every { getString(SecurePreferences.KEY_PIN) } returns null
            every { getString(SecurePreferences.KEY_PIN_SALT) } returns "" // to avoid generating the salt
        }
        val input = "blabla"
        val pinLocalSource = spyk(PinLocalSource(securePreferences))

        pinLocalSource.comparePin(input)
    }

    @Test
    fun `Test comparePin with no pin stored`() {
        val securePreferences = mockk<SecurePreferences>(relaxed = true) {
            every { getString(SecurePreferences.KEY_PIN) } returns null
            every { getString(SecurePreferences.KEY_PIN_SALT) } returns "abcd1234" // to avoid generating the salt
        }
        val input = "blabla"
        val expected = false
        val pinLocalSource = spyk(PinLocalSource(securePreferences))
        val result = pinLocalSource.comparePin(input)
        Assert.assertEquals(expected, result)

        verify {
            securePreferences.getString(SecurePreferences.KEY_PIN)
            securePreferences.getString(SecurePreferences.KEY_PIN_SALT)
        }
    }

    @Test
    fun test_FilledPassword_NoMatch() {
        val salt = CryptoUtils.generateSalt()
        val hashedPin = CryptoUtils.encryptWithSalt("storedPin", salt)

        val securePreferences = mockk<SecurePreferences>(relaxed = true) {
            every { getString(SecurePreferences.KEY_PIN) } returns hashedPin
            every { getString(SecurePreferences.KEY_PIN_SALT) } returns salt
        }
        val input = "storedPin1"
        val expected = false
        val pinLocalSource = PinLocalSource(securePreferences)
        val result = pinLocalSource.comparePin(input)
        Assert.assertEquals(expected, result)

        verify {
            securePreferences.getString(SecurePreferences.KEY_PIN)
            securePreferences.getString(SecurePreferences.KEY_PIN_SALT)
        }
    }

    @Test
    fun test_FilledPassword_Match() {
        val salt = CryptoUtils.generateSalt()
        val hashedPin = CryptoUtils.encryptWithSalt("storedPin", salt)

        val securePreferences = mockk<SecurePreferences>(relaxed = true) {
            every { getString(SecurePreferences.KEY_PIN) } returns hashedPin
            every { getString(SecurePreferences.KEY_PIN_SALT) } returns salt
        }
        val input = "storedPin"
        val expected = true
        val pinLocalSource = PinLocalSource(securePreferences)
        val result = pinLocalSource.comparePin(input)
        Assert.assertEquals(expected, result)

        verify {
            securePreferences.getString(SecurePreferences.KEY_PIN)
            securePreferences.getString(SecurePreferences.KEY_PIN_SALT)
        }
    }

}
