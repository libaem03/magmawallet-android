package io.camlcase.smartwallet.data.usecase.migration

import io.camlcase.kotlintezos.TezosNodeClient
import io.camlcase.kotlintezos.model.Tez
import io.camlcase.kotlintezos.model.TezosError
import io.camlcase.kotlintezos.model.TezosErrorType
import io.camlcase.smartwallet.data.core.RxEmitter
import io.camlcase.smartwallet.data.model.AppWallet
import io.camlcase.smartwallet.data.model.error.AppError
import io.camlcase.smartwallet.data.model.error.ErrorType
import io.camlcase.smartwallet.data.source.user.WalletLocalSource
import io.camlcase.smartwallet.data.test.mock.mockWalletLocalSource
import io.mockk.*
import io.reactivex.rxjava3.core.SingleEmitter
import org.junit.After
import org.junit.Assert
import org.junit.Test

/**
 * @see GetMigrationWalletBalance
 */
class GetMigrationWalletBalanceTest {
    private val emitter: RxEmitter = mockk<RxEmitter>(relaxed = true)
    private val tezosNodeClient: TezosNodeClient = mockk<TezosNodeClient>(relaxed = true)
    private val walletLocalSource: WalletLocalSource = mockWalletLocalSource()

    private val migrationWallet: AppWallet? = mockk<AppWallet>() {
        every { address } returns "tz1"
    }

    private fun initUseCase(): GetMigrationWalletBalance {
        return spyk(
            GetMigrationWalletBalance(
                emitter, tezosNodeClient, walletLocalSource
            )
        )
    }

    @After
    fun tearDown() {
        confirmVerified(walletLocalSource)
        confirmVerified(tezosNodeClient)
    }

    private fun loadSampleData(
        migrationWallet: AppWallet? = this.migrationWallet,
        expectedTezBalance: Tez = Tez(12345.6789)
    ) {
        every { walletLocalSource.getMigrationWallet() } returns migrationWallet
        every { emitter.tezosCallback(any<SingleEmitter<Tez>>(), any()) } answers {
            firstArg<SingleEmitter<Tez>>().onSuccess(expectedTezBalance)
            mockk()
        }
    }

    private fun verifyGetBalance() {
        verify {
            walletLocalSource.getMigrationWallet()
            tezosNodeClient.getBalance(any(), any())
        }
    }

    @Test
    fun `getBalance 0 xtz`() {
        val balance = Tez.zero
        loadSampleData(
            expectedTezBalance = balance
        )

        initUseCase().getBalance()
            .test()
            .assertNoErrors()
            .assertValue {
                Assert.assertTrue(it == balance)
                true
            }

        verifyGetBalance()
    }

    @Test
    fun `getBalance Node error`() {
        every { walletLocalSource.getMigrationWallet() } returns migrationWallet
        every { emitter.tezosCallback(any<SingleEmitter<Tez>>(), any()) } answers {
            firstArg<SingleEmitter<Tez>>().onError(TezosError(TezosErrorType.RPC_ERROR))
            mockk()
        }

        initUseCase().getBalance()
            .test()
            .assertError {
                Assert.assertTrue(it is TezosError)
                Assert.assertTrue((it as TezosError).type == TezosErrorType.RPC_ERROR)
                true
            }
        verifyGetBalance()
    }

    @Test
    fun `getBalance No Wallet`() {
        val balance = Tez.zero
        loadSampleData(
            migrationWallet = null,
            expectedTezBalance = balance
        )

        initUseCase().getBalance()
            .test()
            .assertError {
                Assert.assertTrue(it is AppError)
                Assert.assertTrue((it as AppError).type == ErrorType.NO_WALLET)
                true
            }

        verify {
            walletLocalSource.getMigrationWallet()
        }
    }
}
