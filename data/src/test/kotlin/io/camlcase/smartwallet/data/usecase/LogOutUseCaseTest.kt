package io.camlcase.smartwallet.data.usecase

import io.camlcase.smartwallet.data.analytic.AnalyticsTracker
import io.camlcase.smartwallet.data.core.db.MagmaDatabase
import io.camlcase.smartwallet.data.source.local.AppPreferences
import io.camlcase.smartwallet.data.source.local.PermissionPreferences
import io.camlcase.smartwallet.data.source.operation.OperationsLocalSource
import io.camlcase.smartwallet.data.source.token.TokenBalanceLocalSource
import io.camlcase.smartwallet.data.source.user.WalletLocalSource
import io.mockk.confirmVerified
import io.mockk.mockk
import io.mockk.verify
import org.junit.After
import org.junit.Test

/**
 * @see LogOutUseCase
 */
class LogOutUseCaseTest {
    private val permissionPreferences = mockk<PermissionPreferences>(relaxed = true)
    private val appPreferences = mockk<AppPreferences>(relaxed = true)
    private val walletLocalSource = mockk<WalletLocalSource>(relaxed = true)
    private val tokenBalanceLocalSource = mockk<TokenBalanceLocalSource>(relaxed = true)
    private val operationsLocalSource = mockk<OperationsLocalSource>(relaxed = true)
    private val database = mockk<MagmaDatabase>(relaxed = true)
    private val analyticsTracker = mockk<AnalyticsTracker>(relaxed = true)

    private fun initUseCase(): LogOutUseCase {
        return LogOutUseCase(
            permissionPreferences,
            appPreferences,
            walletLocalSource,
            tokenBalanceLocalSource,
            operationsLocalSource,
            database,
            analyticsTracker
        )
    }

    @Test
    fun `Test logOut OK`() {
        val usecase = initUseCase()
        usecase.logOut()

        verify {
            permissionPreferences.clear()
            appPreferences.clear()
            walletLocalSource.deleteWallet()
            tokenBalanceLocalSource.clear()
            operationsLocalSource.clear()
            database.clearAllTables()
            analyticsTracker.logout()
        }
    }

    @After
    fun after() {
        confirmVerified(permissionPreferences)
        confirmVerified(appPreferences)
        confirmVerified(walletLocalSource)
        confirmVerified(tokenBalanceLocalSource)
        confirmVerified(operationsLocalSource)
        confirmVerified(database)
        confirmVerified(analyticsTracker)
    }
}
